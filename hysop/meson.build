# ---
# --- Generate __init__.py file from __init__.py_tmpl file ---
# ---
message('Generate __init__.py file.')

conf_data = configuration_data({
	'HYSOP_VERSION'    : hysop_version,
        'USE_MPI'          : use_mpi,
        'WITH_GPU'         : with_opencl,          # WARNING TODO !!!!
        'WITH_FFTW'        : with_fftw,
        'WITH_SCALES'      : with_scales,
        'H5PY_PARALLEL_COMPRESSION_ENABLED': with_parallel_compressed_hdf5,
        'VERBOSE'          : verbose,
        'DEBUG'            : debug,
        'PROFILE'          : profile,
        'ENABLE_LONG_TESTS': enable_long_tests,
        'WITH_OPENCL'      : with_opencl,
        'OPENCL_DEFAULT_OPENCL_PLATFORM_ID': platform_id,
        'OPENCL_DEFAULT_OPENCL_DEVICE_ID'  : device_id,
	})

configure_file(input         : '__init__.py_tmpl',
               output        : '__init__.py',
               configuration : conf_data)

# ---
# --- Generate constants.py file from constants.py_tmpl file ---
# ---
message('Generate constants.py file.')

# set precision for real numbers
# used to generate constant.py
if doubleprec == 'ON'
  # -- Variables required in python files --
  python_prec     = 'np.float64'
  mpi_python_prec = 'MPI.DOUBLE'
else
  python_prec     = 'np.float32'
  mpi_python_prec = 'MPI.FLOAT'
endif

# set data layout ('fortran' order or 'C' order) in python (numpy).
if fortran_layout == 'ON'
  data_layout='\'F\''
else
  data_layout='\'C\''
endif

conf_data = configuration_data({
	'PYTHON_PREC'      : python_prec,
        'DATA_LAYOUT'      : data_layout,
        'MPI_PYTHON_PREC'  : mpi_python_prec,
	})

configure_file(input         : 'constants.py_tmpl',
               output        : 'constants.py',
               configuration : conf_data)

# ---
# --- Hysop python source files installation
# ---
src_python  = files(meson_build_dir+'/hysop/__init__.py')
src_python += files(meson_build_dir+'/hysop/constants.py')
src_files = ['defaults.py',
             'testsenv.py',
             'problem.py',
             'simulation.py',
             'operators.py',
             'methods.py',
             'iterative_method.py']
foreach f : src_files
  src_python += files(f)
endforeach

py.install_sources(
     src_python,
     subdir: 'hysop'
)

# -
# - Check parallel hdf5 availability
#
if with_parallel_compressed_hdf5 == 'ON'

  # get hdf5 lib version
  hdf5_lib_version = run_command(py,
  ['-c', 'import h5py; print(\'.\'.join(str(_) for _ in h5py.h5.get_libversion()))'],
  check: false).stdout().strip()

  # get python h5py version
  h5py_version_bool = run_command(py,
  ['-c', 'import h5py; print(h5py.h5.get_libversion() >= (1,10,2) )'],
  check: false).stdout().strip()

  if h5py_version_bool == 'false'
    message('''
            WARNING: Your hdf5 library is too old to support
                      parallel compression support.
                      Minimal version is 1.10.2 but h5py was linked to version
            ''',
            hdf5_lib_version,
            'Parallel HDF5 compression will be disabled.')
    h5py_parallel_compression_enabled = 'OFF'
  else
    message('Found h5py linked against libhdf5 version', hdf5_lib_version,
            'Parallel HDF5 compression will be enabled.')
    h5py_parallel_compression_enabled = 'ON'
  endif

else
  h5py_parallel_compression_enabled = 'OFF'
endif


# ---
# --- applied meson.build in upper directories
# ---

subdir('backend')

subdir('common_f')

subdir('numerics')

if with_scales == 'ON'
  subdir('scales_f')
endif

subdir('core')
subdir('domain')
subdir('fields')
subdir('mesh')
subdir('operator')
subdir('parameters')
subdir('symbolic')
subdir('tools')
subdir('topology')
