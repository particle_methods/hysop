# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Set some functions and variables useful to run tests.
"""
import contextlib
import os
import shutil

import numpy as np
import pytest

from hysop import (
    __DEFAULT_DEVICE_ID__,
    __DEFAULT_PLATFORM_ID__,
    __ENABLE_LONG_TESTS__,
    __FFTW_ENABLED__,
    __SCALES_ENABLED__,
    __TEST_ALL_OPENCL_PLATFORMS__,
)
from hysop.backend import __HAS_OPENCL_BACKEND__
from hysop.core.mpi import default_mpi_params, main_rank
from hysop.tools.contexts import printoptions
from hysop.tools.decorators import static_vars
from hysop.tools.io_utils import IO
from hysop.tools.numpywrappers import npw
from hysop.tools.htypes import check_instance, first_not_None, to_tuple

# accept failing tests when fft is not enabled
if __FFTW_ENABLED__:

    def fftw_failed(f):
        """For fftw tests that must not fail"""
        return f

else:
    fftw_failed = pytest.mark.xfail


# accept failing tests when scales is not enabled
if __SCALES_ENABLED__:

    def scales_failed(f):
        """For scales tests that must not fail"""
        return f

else:
    scales_failed = pytest.mark.xfail


@contextlib.contextmanager
def test_context():
    with printoptions(
        threshold=10000,
        linewidth=1000,
        nanstr="nan",
        infstr="inf",
        formatter={"float": lambda x: f"{x:>6.2f}"},
    ):
        yield


def domain_boundary_iterator(dim):
    import itertools as it

    from hysop.constants import BoxBoundaryCondition

    choices = (BoxBoundaryCondition.OUTFLOW, BoxBoundaryCondition.SYMMETRIC)
    choices = tuple(it.product(choices, repeat=2))
    for i in range(dim, -1, -1):
        bd0 = ((BoxBoundaryCondition.PERIODIC, BoxBoundaryCondition.PERIODIC),) * i
        for bd1 in it.product(choices, repeat=dim - i):
            bd = bd0 + bd1
            lbd = np.asarray([x[0] for x in bd])
            rbd = np.asarray([x[1] for x in bd])
            yield (lbd, rbd)


# accept failing tests when opencl is not present
if __HAS_OPENCL_BACKEND__:
    from hysop.backend.device.opencl import cl
    from hysop.backend.device.opencl.opencl_tools import get_or_create_opencl_env

    def opencl_failed(f):
        """For opencl tests that must not fail"""
        return f

    @static_vars(cl_environments={})
    def iter_clenv(device_type=None, all_platforms=None, **kwds):
        """
        Iterate over all platforms and device and yield OpenClEnvironments.
        If __ENABLE_LONG_TESTS__ is False, just yield the default OpenCl
        environment.
        """
        all_platforms = first_not_None(all_platforms, __TEST_ALL_OPENCL_PLATFORMS__)
        if isinstance(device_type, str):
            if device_type == "cpu":
                cl_device_type = cl.device_type.CPU
            elif device_type == "gpu":
                cl_device_type = cl.device_type.GPU
            else:
                raise NotImplementedError(device_type)
        else:
            cl_device_type = device_type

        cl_environments = iter_clenv.cl_environments
        if cl_device_type not in cl_environments:
            cl_environments[cl_device_type] = []
            if cl_device_type is None:
                mpi_params = default_mpi_params()
                for i, plat in enumerate(cl.get_platforms()):
                    for j, dev in enumerate(plat.get_devices()):
                        cl_env = get_or_create_opencl_env(
                            platform_id=i, device_id=j, mpi_params=mpi_params, **kwds
                        )
                        if (i == __DEFAULT_PLATFORM_ID__) and (
                            j == __DEFAULT_DEVICE_ID__
                        ):
                            cl_environments[None].insert(0, cl_env)
                        else:
                            cl_environments[None].append(cl_env)
            else:
                for cl_env in iter_clenv(cl_device_type=None, all_platforms=True):
                    if cl_env.device.type & cl_device_type:
                        cl_environments[cl_device_type].append(cl_env)

        if len(cl_environments[cl_device_type]) == 0:
            msg = "     |Could not generate any opencl environment for device type {}."
            msg = msg.format(device_type)
            if cl_device_type == None:
                raise RuntimeError(msg)
            else:
                print(msg)
        for cl_env in cl_environments[cl_device_type]:
            yield cl_env
            if not all_platforms:
                return

else:
    opencl_failed = pytest.mark.xfail
    iter_clenv = None

from hysop.backend import __HAS_CUDA_BACKEND__

if __HAS_CUDA_BACKEND__:

    def cuda_failed(f):
        """For cuda tests that must not fail"""
        return f

else:
    cuda_failed = pytest.mark.xfail

hysop_failed = pytest.mark.xfail
"""Use this decorator for tests that must fail"""


class postclean:
    """A decorator to remove files in default path and working dir
    at the end of the calling function.
    """

    def __init__(self, working_dir=None):
        """A decorator to remove files in default path and working dir
        at the end of the calling function.

        Usage:

        @postclean
        def test_name()

        or

        @postclean(working_dir)
        def test_name()

        working_dir = current test working directory.
        """
        if working_dir is not None:
            if not os.path.exists(working_dir):
                working_dir = None
        self.working_dir = working_dir

    def __call__(self, f):
        """Apply decorator"""

        def wrapped_f(*args):
            """return wrapped function + post exec"""
            f(*args)
            print("RM ...", self.working_dir, IO.default_path(), main_rank)
            if main_rank == 0:
                if os.path.exists(IO.default_path()):
                    shutil.rmtree(IO.default_path())
                if self.working_dir is not None:
                    if os.path.exists(self.working_dir):
                        shutil.rmtree(self.working_dir)

        return wrapped_f


class FakeCartesianField:
    r"""
    Generate data layouts like cartesian fields (only for testing purposes).

    This is usefull to test operators internals without the need
    to create real cartesian discrete fields.

    /!\ warning: grid size is not the same as grid shape:
        grid_shape = grid_size[::-1]
          3d size  id (X, Y, Z)
          3d shape is (Z, Y, X)
        shape is the underlying np.ndarray data shape
    """

    def __init__(
        self,
        name,
        dim,
        dtype,
        nb_components,
        compute_grid_size,
        min_ghosts=None,
        extra_ghosts=None,
        init=0,
        ghosts_init=npw.inf,
        extra_ghosts_init=npw.nan,
    ):

        check_instance(name, str)
        check_instance(dim, int)
        check_instance(nb_components, int)
        check_instance(compute_grid_size, (tuple, list), size=dim)
        check_instance(min_ghosts, (tuple, list), size=dim, allow_none=True)
        check_instance(extra_ghosts, (tuple, list), size=dim, allow_none=True)

        self.name = name
        self.dim = dim
        self.dtype = dtype
        self.nb_components = nb_components
        self.compute_grid_size = npw.asintarray(compute_grid_size)
        self.min_ghosts = npw.asintarray(first_not_None(min_ghosts, (0,) * dim))
        self.extra_ghosts = npw.asintarray(first_not_None(extra_ghosts, (0,) * dim))
        assert (self.min_ghosts >= 0).all()
        assert (self.extra_ghosts >= 0).all()

        self.data = tuple(
            self._alloc_array(init, ghosts_init, extra_ghosts_init)
            for i in range(nb_components)
        )

    def _alloc_array(self, init, ghosts_init, extra_ghosts_init):

        view0, view1 = self.view(self.dim)
        data = npw.empty(shape=self.grid_shape, dtype=self.dtype)
        self._init_array(data[...], extra_ghosts_init)
        self._init_array(data[view0], ghosts_init)
        self._init_array(data[view0][view1], init)
        return data

    def _init_array(self, a, vals):
        if isinstance(vals, tuple):
            check_instance(vals, tuple, (int, float, npw.number), size=2)
            (amin, amax) = vals
            a[...] = (amax - amin) * npw.random.rand(*a.shape) + amin
        else:
            check_instance(vals, (int, float, npw.number))
            a[...] = vals

    def view(self, dim):
        assert 1 <= dim <= self.dim

        compute_grid_size = self.compute_grid_size
        min_ghosts, extra_ghosts, ghosts = (
            self.min_ghosts,
            self.extra_ghosts,
            self.ghosts,
        )

        field_view = (
            []
        )  # view of the computational grid with minimal ghosts in the full grid
        cg_view = (
            []
        )  # view of the computational grid without ghosts in the field_view subgrid
        for i in range(self.dim):
            j = self.dim - i - 1
            if j < dim:
                field_sl = slice(
                    ghosts[j] - min_ghosts[j],
                    compute_grid_size[j] + ghosts[j] + min_ghosts[j],
                )
                cg_sl = slice(min_ghosts[j], compute_grid_size[j] + min_ghosts[j])
                field_view.append(field_sl)
                cg_view.append(cg_sl)
            else:
                field_view.append(ghosts[j])
        return tuple(field_view), tuple(cg_view)

    @property
    def ghosts(self):
        return self.min_ghosts + self.extra_ghosts

    @property
    def min_grid_size(self):
        return self.compute_grid_size + 2 * self.min_ghosts

    @property
    def grid_size(self):
        return self.compute_grid_size + 2 * self.ghosts

    @property
    def min_grid_shape(self):
        return self.min_grid_size[::-1]

    @property
    def compute_grid_shape(self):
        return self.compute_grid_size[::-1]

    @property
    def grid_shape(self):
        return self.grid_size[::-1]

    def fancy_print(self, raw_data=False, element_width=1, **print_opts):
        _formatter = {
            object: lambda x: "{:^{width}}".format(x, width=element_width)[
                :element_width
            ],
            float: lambda x: "{:{width}.2f}".format(x, width=element_width),
        }
        _print_opts = dict(
            threshold=10000,
            linewidth=1000,
            formatter={
                "object": lambda x: _formatter.get(type(x), _formatter[object])(x)
            },
        )
        _print_opts.update(print_opts)

        strarr = npw.empty_like(self.data[0], dtype=object)
        if raw_data:
            strarr[...] = self.data[0]
        else:
            view0, view1 = self.view(self.dim)
            strarr[...] = "E"
            strarr[view0] = "X"
            strarr[view0][view1] = "."

        with printoptions(**_print_opts):
            print(strarr)

    def __str__(self):
        ss = """FakeCartesianField {}:
    dtype:             {}
    compute_grid_size: {}
    min_ghosts:        {}
    min_grid_size:     {}
    extra_ghosts:      {}
    grid_size:         {}""".format(
            self.name,
            self.dtype.__name__,
            self.compute_grid_size,
            self.min_ghosts,
            self.min_grid_size,
            self.extra_ghosts,
            self.grid_size,
        )
        return ss


if __name__ == "__main__":
    a = FakeCartesianField(
        "A", 2, npw.int32, 1, [16, 8], [2, 1], [1, 1], (0, 10), -1, -2
    )
    print(a)
    print()
    a.fancy_print()
    print()
    print(a.data[0])
