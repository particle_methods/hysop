# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop import hprint
from hysop.backend.hardware.pci_ids import PCIIds
from hysop.backend.hardware.hwinfo import Topology

from hysop.core.mpi import shm_rank, shm_size

# Parse all PCI vendors and device data
hprint("*Detecting hardware and software environment...")
hprint("*Parsing pci database...")
pciids = PCIIds()

# Use lstopo xml output (hwinfo) to get local hardware topology
hprint("*Parsing hardware topology...")
topology = Topology.parse(pciids)
