# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import re, fractions
import numpy as np

from hysop.tools.htypes import check_instance
from hysop import vprint
from hysop.backend.device.opencl import cl, cl_api
from hysop.constants import DeviceType, CacheType, MemoryType, FpConfig
from hysop.tools.units import bytes2str, freq2str, time2str
from hysop.backend.device.logical_device import LogicalDevice, UnknownDeviceAttribute
from hysop.tools.string_utils import prepend
from hysop.backend.device.opencl.clpeak import ClPeakInfo
from hysop.tools.units import bdw2str, flops2str, iops2str
from hysop.backend.hardware.hwinfo import HardwareStatistics


def opencl_version_atleast(major, minor, returned=UnknownDeviceAttribute()):
    def decorator(f):
        def wrap(*args, **kargs):
            self = args[0]
            (version_major, version_minor) = self._cl_version
            if (version_major > major) or (
                version_major == major and version_minor >= minor
            ):
                return f(*args, **kargs)
            else:
                return returned

        return wrap

    return decorator


def cl2hysop_device_type(cl_dev_type):
    entries = {
        cl.device_type.ACCELERATOR: DeviceType.ACCELERATOR,
        cl.device_type.ALL: DeviceType.ALL,
        cl.device_type.CPU: DeviceType.CPU,
        cl.device_type.DEFAULT: DeviceType.DEFAULT,
        cl.device_type.GPU: DeviceType.GPU,
    }
    if hasattr(cl.device_type, "CUSTOM"):
        entries[cl.device_type.CUSTOM] = DeviceType.CUSTOM

    if cl_dev_type not in entries:
        msg = f"Unknown opencl device type {cl_dev_type}."
        raise ValueError(msg)
    return entries[cl_dev_type]


def cl2hysop_mem_type(cl_mem_type):
    if cl_mem_type == cl.device_local_mem_type.LOCAL:
        return MemoryType.LOCAL_MEMORY
    elif cl_mem_type == cl.device_local_mem_type.GLOBAL:
        return MemoryType.GLOBAL_MEMORY
    else:
        msg = f"Unknown memory type {cl_mem_type}."
        raise ValueError(msg)


def cl2hysop_cache_type(cl_cache_type):
    ct = cl.device_mem_cache_type
    if cl_cache_type == ct.NONE:
        return CacheType.NONE
    elif cl_cache_type == ct.READ_ONLY_CACHE:
        return CacheType.READ_ONLY_CACHE
    elif cl_cache_type == ct.READ_WRITE_CACHE:
        return CacheType.READ_WRITE_CACHE
    else:
        msg = f"Unknown cache type {cl_cache_type}."
        raise ValueError(msg)


def cl2hysop_fpconfig(cl_fpconfig):
    entries = {
        FpConfig.DENORM: bool(cl_fpconfig & 0x01),
        FpConfig.INF_NAN: bool(cl_fpconfig & 0x02),
        FpConfig.ROUND_TO_NEAREST: bool(cl_fpconfig & 0x04),
        FpConfig.ROUND_TO_ZERO: bool(cl_fpconfig & 0x08),
        FpConfig.ROUND_TO_INF: bool(cl_fpconfig & 0x10),
        FpConfig.FMA: bool(cl_fpconfig & 0x20),
        FpConfig.SOFT_FLOAT: bool(cl_fpconfig & 0x40),
        FpConfig.CORRECTLY_ROUNDED_DIVIDE_SQRT: bool(cl_fpconfig & 0x80),
    }
    if cl_fpconfig > 0xFF:
        msg = f"Unknown bit field entry '{cl_fpconfig:04x}'."
        msg += "\nSee opencl header to add missing entries (cl_device_fp_config)."
        raise ValueError(msg)
    return entries


class OpenClDevice(LogicalDevice):

    __attrs = (
        "version",
        "opencl_c_version",
        "spir_versions",
        "driver_version",
        "il_version",
        "name",
        "type",
        "vendor",
        "vendor_id",
        "extensions",
        "built_in_kernels",
        "available",
        "compiler_available",
        "linker_available",
        "host_unified_memory",
        "svm_capabilities",
        "partition_max_sub_devices",
        "partition_properties",
        "partition_affinity_domain",
        "partition_type",
        "pipe_max_active_reservations",
        "pipe_max_packet_size",
        "max_pipe_args",
        "queue_properties",
        "queue_on_device_max_size",
        "queue_on_device_preferred_size",
        "queue_on_host_properties",
        "queue_on_device_properties",
        "max_on_device_events",
        "max_on_device_queues",
        "max_global_variable_size",
        "max_parameter_size",
        "max_clock_frequency",
        "sub_group_independent_forward_progress",
        "max_num_sub_groups",
        "address_bits",
        "endian_little",
        "error_correction_support",
        "execution_capabilities",
        "max_work_item_dimensions",
        "max_work_item_sizes",
        "max_work_group_size",
        "mem_base_addr_align",
        "max_constant_args",
        "max_constant_buffer_size",
        "global_mem_size",
        "global_mem_cache_size",
        "global_mem_cacheline_size",
        "global_mem_cache_type",
        "max_mem_alloc_size",
        "local_mem_size",
        "local_mem_type",
        "half_fp_config",
        "single_fp_config",
        "double_fp_config",
        "image_support",
        "max_samplers",
        "image_max_array_size",
        "image_max_buffer_size",
        "max_read_image_args",
        "max_write_image_args",
        "max_read_write_image_args",
        "image2d_max_width",
        "image2d_max_height",
        "image3d_max_depth",
        "image3d_max_height",
        "image3d_max_width",
        "image_max_buffer_size",
        "preferred_platform_atomic_alignment",
        "preferred_local_atomic_alignment",
        "preferred_global_atomic_alignment",
        "preferred_interop_user_sync",
        "profiling_timer_resolution",
        "printf_buffer_size",
        "min_data_type_align_size",
        "native_vector_width_char",
        "native_vector_width_short",
        "native_vector_width_int",
        "native_vector_width_long",
        "native_vector_width_float",
        "native_vector_width_double",
        "native_vector_width_half",
        "preferred_vector_width_char",
        "preferred_vector_width_short",
        "preferred_vector_width_int",
        "preferred_vector_width_long",
        "preferred_vector_width_half",
        "preferred_vector_width_float",
        "preferred_vector_width_double",
        "pci_bus_id_nv",
        "pci_slot_id_nv",
        "attribute_async_engine_count_nv",
        "compute_capability_major_nv",
        "compute_capability_minor_nv",
        "gpu_overlap_nv",
        "integrated_memory_nv",
        "kernel_exec_timeout_nv",
        "register_per_block_nv",
        "warp_size_nv",
        "available_async_queues_amd",
        "board_name_amd",
        "global_free_memory_amd",
        "global_mem_channel_banks_amd",
        "global_mem_channel_bank_width_amd",
        "local_mem_banks_amd",
        "local_mem_size_per_compute_unit_amd",
        "gfxip_major_amd",
        "gfxip_minor_amd",
        "profiling_timer_offset_amd",
        "simd_instruction_width_amd",
        "simd_per_compute_unit_amd",
        "simd_width_amd",
        "thread_trace_supported_amd",
        "topology_amd",
        "wavefront_width_amd",
        "core_temperature_altera",
        "ext_mem_padding_in_bytes_qcom",
        "page_size_qcom",
        "me_version_intel",
        "num_simultaneous_interops_intel",
        "simultaneous_interops_intel",
        "max_atomic_counters_ext",
    )

    __all_attrs = __attrs + ("_simd_lane_size", "_usable_local_mem_size", "_real_name")

    def __init__(
        self, platform, platform_handle, device_id, device_handle, hardware_topo, **kwds
    ):

        # we do not keep a reference to platform_handle  or device_handle as we
        # need to pickle this object
        self._cl_version = self._extract_cl_version(device_handle)
        _not_found = ()
        for attr in self.__attrs:
            try:
                val = getattr(device_handle, attr)
                if isinstance(val, str):
                    val = val.strip()
                setattr(self, "_" + attr, val)
            except (cl_api.LogicError, AttributeError):
                _not_found += (attr,)
                setattr(self, "_" + attr, UnknownDeviceAttribute())
            except RuntimeError as e:
                if str(e) == "cannot use string() on <cdata 'char *' NULL>":
                    setattr(self, "_" + attr, "")
                else:
                    raise

        self._simd_lane_size = cl.characterize.get_simd_group_size(
            device_handle, np.int32
        )
        self._usable_local_mem_size = cl.characterize.usable_local_mem_size(
            device_handle
        )
        self._real_name = device_handle.name

        super().__init__(
            platform=platform,
            platform_handle=platform_handle,
            device_id=device_id,
            device_handle=device_handle,
            hardware_topo=hardware_topo,
            **kwds,
        )

    def _determine_performance_and_affinity(self, hardware_topo):

        # test device bandwidth using clpeak global memory bandwidth statistics
        platform = self.platform

        info = ClPeakInfo(
            platform_name=platform._real_name,
            platform_id=platform.platform_id,
            device_name=self._real_name,
            device_id=self.device_id,
            is_cpu=(self.type() == DeviceType.CPU),
        )
        self._clpeak_info = info

    def _extract_cl_version(self, device_handle):
        version = device_handle.version
        regexp = re.compile(r"OpenCL ([0-9]+)\.([0-9]+)")
        match = re.match(regexp, version)
        assert match, f"Could not match opencl version from '{version}'."
        (major, minor) = int(match.group(1)), int(match.group(2))
        return (major, minor)

    def _match_physical_devices(self, hardware_topo):
        if self.type() == DeviceType.CPU:
            return hardware_topo.cpu_packages()

        # if it is not a device of type CPU, it may be a pci device
        # first we match only by vendor id
        pci_devices = hardware_topo.pci_devices(vendor_id=self.vendor_id())

        # if there is only one such device we are lucky
        if len(pci_devices) == 1:
            device = pci_devices[0]
            msg = "Matched opencl device '{}' by vendor id to pci bus id {}"
            msg = msg.format(self.name(), device.pci_busid())
            vprint(msg)
            return device
        # else we may try to look to pci bus id
        # at this time, this is only available for nvidia and amd via opencl extensions
        else:
            pci_bus_id = self.pci_bus_id()
            pci_devices = dict(zip([dev.pci_busid for dev in pci_devices], pci_devices))
            if not isinstance(pci_bus_id, str):
                msg = "Could not get opencl pci device bus id."
                vprint(msg)
                return None
            elif pci_bus_id not in pci_devices.keys():
                msg = "Could get opencl pci device bus id ({}), but it did not match "
                msg = "any in hardware topology."
                msg = msg.format(pci_bus_id)
                vprint(msg)
                return None
            else:
                msg = f"Matched opencl device '{self.name}' to pci bus id {pci_bus_id}"
                vprint(msg)
                return pci_devices[pci_bus_id]

    def short_device_summary(self, indent=0, increment=2):
        ind = " " * indent
        inc = " " * increment
        msg = """{ind}{inc}|name:                     {}{}
{ind}{inc}|vendor:                   {} [0x{:04x}]
{ind}{inc}|device type:              {}
{ind}{inc}|OpenCL   version:         {}
{ind}{inc}|OpenCL C version:         {}
{ind}{inc}|global memory size:       {}{}
{ind}{inc}|global max alloc size:    {}
{ind}{inc}|local memory size:        {}
{ind}{inc}|device splitting:         {}
{ind}{inc}|physical device:          {}""".format(
            self.name(),
            self.hardware_device_id_str(),
            self.vendor(),
            self.vendor_id(),
            self.type(),
            self.version(),
            self.opencl_c_version(),
            bytes2str(self.global_mem_size()),
            " (ECC)" if self.error_correction_support() else "",
            bytes2str(self.max_global_alloc_size()),
            bytes2str(self.local_mem_size(), decimal=False),
            (
                f"yes, up to {self.max_subdevices()} subdevices"
                if self.has_device_partition_support()
                else "no"
            ),
            "no match",
            ind=ind,
            inc=inc,
        )
        msg += "\n" + self.clpeak_info_summary(indent=indent, increment=increment)
        return msg

    def clpeak_info_summary(self, indent, increment):
        ind = " " * indent
        inc = " " * increment
        info = self._clpeak_info
        ss = [f"{ind}{inc}|CLPEAK BENCHMARK:"]
        failed = True
        if info.has_memory_bandwidth:
            ss += [
                "{{s}}global memory bandwidth:         {}".format(
                    bdw2str(info.mean_global_bdw)
                )
            ]
            failed = False
        if info.has_transfer_bandwdith:
            otbv = info.optimal_transfer_bandwidth_values
            read = otbv["enqueuereadbuffer"]
            write = otbv["enqueuewritebuffer"]
            mapped_read = otbv["memcpy_from_mapped_ptr"]
            mapped_write = otbv["memcpy_to_mapped_ptr"]
            ss += [f"{{s}}read  memory bandwidth:          {bdw2str(read)}"]
            ss += [f"{{s}}write memory bandwidth:          {bdw2str(write)}"]
            ss += [f"{{s}}read  memory bandwidth (mapped): {bdw2str(mapped_read)}"]
            ss += [f"{{s}}write memory bandwidth (mapped): {bdw2str(mapped_write)}"]
            failed = False
        if info.has_integer_compute:
            ss += [
                "{{s}}integer compute:                 {}".format(
                    iops2str(info.max_int_compute)
                )
            ]
            failed = False
        if info.has_single_precision_compute:
            ss += [
                "{{s}}single precision compute:        {} (SP)".format(
                    flops2str(info.max_sp_compute)
                )
            ]
            failed = False
        if info.has_double_precision_compute:
            ss += [
                "{{s}}double precision compute:        {} (DP)".format(
                    flops2str(info.max_dp_compute)
                )
            ]
            failed = False
        if info.has_single_precision_compute and info.has_double_precision_compute:
            ratio = info.max_dp_compute / float(info.max_sp_compute)
            ratio = fractions.Fraction(ratio).limit_denominator(64)
            ss += [f"{{s}}double to float compute ratio:   {ratio} (DP/SP)"]
            failed = False
        if failed:
            ss += ["{s}>Error: All clpeak commands failed."]
        return "\n".join(ss).format(s=(ind + inc + inc + " |-"))

    def device_summary(self, indent=0, increment=2):
        ind = " " * indent
        inc = " " * increment
        msg = """{ind}{inc}General informations:
{ind}{inc}{inc}*name:                     {} {}
{ind}{inc}{inc}*vendor:                   {} [0x{:04x}]
{ind}{inc}{inc}*device type:              {}
{ind}{inc}{inc}*opencl   version:         {}
{ind}{inc}{inc}*opencl C version:         {}

{ind}{inc}{inc}*address bits:             {}bit
{ind}{inc}{inc}*little endian:            {}
{ind}{inc}{inc}*ECC enabled:              {}
{ind}{inc}{inc}*max clock frequency:      {}

{ind}{inc}{inc}*device available:         {}
{ind}{inc}{inc}*compiler available:       {}
{ind}{inc}{inc}*linker available:         {}
{ind}{inc}{inc}*spir support:             {}
""".format(
            self.name(),
            self.hardware_device_id_str(),
            self.vendor(),
            self.vendor_id(),
            self.type(),
            self.version(),
            self.opencl_c_version(),
            self.address_bits(),
            self.little_endian(),
            self.error_correction_support(),
            freq2str(self.max_clock_frequency)(),
            self.available(),
            self.compiler_available(),
            self.linker_available(),
            self.has_spir_support(),
            ind=ind,
            inc=inc,
        )
        return msg

    def memory_summary(self, indent=0, increment=2):
        ind = " " * indent
        inc = " " * increment
        msg = """
{ind}{inc}Global memory:
{ind}{inc}{inc}*size:                     {}
{ind}{inc}{inc}*max alloc size:           {}
{ind}{inc}{inc}*cache size:               {}
{ind}{inc}{inc}*cacheline size:           {}
{ind}{inc}{inc}*cache type:               {}

{ind}{inc}Local memory
{ind}{inc}{inc}*total size:               {}
{ind}{inc}{inc}*usable size:              {}
{ind}{inc}{inc}*cache type:               {}
""".format(
            bytes2str(self.global_mem_size()),
            bytes2str(self.max_global_alloc_size()),
            bytes2str(self.global_mem_cache_size(), decimal=False),
            bytes2str(self.global_mem_cacheline_size(), decimal=False),
            self.global_mem_cache_type(),
            bytes2str(self.local_mem_size(), decimal=False),
            bytes2str(self.usable_local_mem_size(), decimal=False),
            self.local_mem_type(),
            ind=ind,
            inc=inc,
        )
        return msg

    def kernel_summary(self, indent=0, increment=2):
        ind = " " * indent
        inc = " " * increment
        msg = """
{ind}{inc}Kernel execution capabilities:
{ind}{inc}{inc}*max grid  size:           {}
{ind}{inc}{inc}*max block size:           {} (dim={})
{ind}{inc}{inc}*max threads:              {}

{ind}{inc}{inc}*simd lane size:           {}
{ind}{inc}{inc}*max constant args:        {}
{ind}{inc}{inc}*max constant buffer size: {}
{ind}{inc}{inc}*max global variable size: {}
""".format(
            self.max_grid_size(),
            self.max_block_size(),
            self.max_block_dim(),
            self.max_threads_per_block(),
            self.simd_lane_size(),
            self.max_constant_args(),
            bytes2str(self.max_constant_buffer_size(), decimal=False),
            bytes2str(self.max_global_variable_size(), decimal=False),
            ind=ind,
            inc=inc,
        )
        return msg

    def fp_support_summary(self, indent=0, increment=2):
        ind = " " * indent
        inc = " " * increment

        def fmt(bits, has_fp, get_fp_flags):
            if not has_fp:
                support = "\n{ind}{inc}{inc}*fp{} support: None".format(
                    bits, inc=inc, ind=ind
                )
            else:
                support = "\n{ind}{inc}{inc}*fp{} support:".format(
                    bits, inc=inc, ind=ind
                )
                flags = get_fp_flags()
                sep = "\n{ind}{inc}{inc}{inc}".format(ind=ind, inc=inc)
                support += sep + sep.join(
                    sorted(str(k) for k in flags.keys() if flags[k])
                )
            support += "\n"

            return support

        msg = """
{ind}{inc}Floating point support:{}{}{}
""".format(
            fmt(16, self.has_fp16(), self.fp16_config),
            fmt(32, self.has_fp32(), self.fp32_config),
            fmt(64, self.has_fp64(), self.fp64_config),
            ind=ind,
            inc=inc,
        )
        return msg[:-1]

    def vectors_summary(self, indent=0, increment=2):
        ind = " " * indent
        inc = " " * increment
        has_native = self.opencl_version()[0] > 1 or self.opencl_version()[1] >= 1

        def fmt(string):
            return "/" + str(string) if has_native else ""

        msg = """
{ind}{inc}Vector sizes (preferred{}):
{ind}{inc}{inc}*char:                     {}{}
{ind}{inc}{inc}*short:                    {}{}
{ind}{inc}{inc}*int:                      {}{}
{ind}{inc}{inc}*long:                     {}{}

{ind}{inc}{inc}*half:                     {}{}
{ind}{inc}{inc}*float:                    {}{}
{ind}{inc}{inc}*double:                   {}{}
""".format(
            fmt("native"),
            self.preferred_vector_width_char(),
            fmt(self.native_vector_width_char()),
            self.preferred_vector_width_short(),
            fmt(self.native_vector_width_short()),
            self.preferred_vector_width_int(),
            fmt(self.native_vector_width_int()),
            self.preferred_vector_width_long(),
            fmt(self.native_vector_width_long()),
            self.preferred_vector_width_half(),
            fmt(self.native_vector_width_half()),
            self.preferred_vector_width_float(),
            fmt(self.native_vector_width_float()),
            self.preferred_vector_width_double(),
            fmt(self.native_vector_width_double()),
            ind=ind,
            inc=inc,
        )
        return msg

    def image_summary(self, indent=0, increment=2):
        ind = " " * indent
        inc = " " * increment
        if not self.has_image_support:
            return f"\n{ind}{inc}Image support: None\n"

        def fmt(has_read_support, has_write_support, max_size_fn):
            if not has_read_support:
                return "unsupported"
            if has_write_support:
                mode = "read/write"
            else:
                mode = "read only"
            max_size = max_size_fn
            return f"{tuple(max_size):<20} {mode}"

        def fmt_array(has_support, max_size_fn):
            if not has_support:
                return "unsupported"
            else:
                max_size = max_size_fn
                return f"{tuple(max_size)}"

        msg = """
{ind}{inc}Image support:
{ind}{inc}{inc}*max image args:           {}
{ind}{inc}{inc}*max write image args:     {}
{ind}{inc}{inc}*max samplers args:        {}

{ind}{inc}{inc}*1D image:                 {}
{ind}{inc}{inc}*2D image:                 {}
{ind}{inc}{inc}*3D image:                 {}

{ind}{inc}{inc}*1D image array:           {}
{ind}{inc}{inc}*2D image array:           {}

{ind}{inc}{inc}*1D image from buffer:     {}
{ind}{inc}{inc}*2D image from buffer:     {}
{ind}{inc}{inc}*3D image from buffer:     {}
""".format(
            self.max_image_args(),
            self.max_write_image_args(),
            self.max_samplers(),
            fmt(
                self.has_1d_image_support(),
                self.has_1d_image_write_support(),
                self.max_1d_image_size(),
            ),
            fmt(
                self.has_2d_image_support(),
                self.has_2d_image_write_support(),
                self.max_2d_image_size(),
            ),
            fmt(
                self.has_3d_image_support(),
                self.has_3d_image_write_support(),
                self.max_3d_image_size(),
            ),
            fmt_array(
                self.has_1d_image_array_support(), self.max_1d_image_array_size()
            ),
            fmt_array(
                self.has_2d_image_array_support(), self.max_2d_image_array_size()
            ),
            self.has_1d_image_from_buffer_support(),
            self.has_2d_image_from_buffer_support(),
            self.has_3d_image_from_buffer_support(),
            ind=ind,
            inc=inc,
        )
        return msg

    def atomics_summary(self, indent=0, increment=2):
        ind = " " * indent
        inc = " " * increment

        def fmt(glob, local, mixed):
            return f"{int(glob)}/{int(local)}/{int(mixed)}"

        msg = """
{ind}{inc}Atomics capabilities (global/local/mixed support):
{ind}{inc}{inc}*int32/uint32:             {}
{ind}{inc}{inc}*int64/uint64:             {}
{ind}{inc}{inc}*float32:                  {}
{ind}{inc}{inc}*float64:                  {}

{ind}{inc}{inc}*int32 hardware counters:  {}
{ind}{inc}{inc}*int64 hardware counters:  {}
""".format(
            fmt(
                self.has_global_int32_atomics(),
                self.has_local_int32_atomics(),
                self.has_mixed_int32_atomics(),
            ),
            fmt(
                self.has_global_int64_atomics(),
                self.has_local_int64_atomics(),
                self.has_mixed_int64_atomics(),
            ),
            fmt(
                self.has_global_float32_atomics(),
                self.has_local_float32_atomics(),
                self.has_mixed_float32_atomics(),
            ),
            fmt(
                self.has_global_float64_atomics(),
                self.has_local_float64_atomics(),
                self.has_mixed_float64_atomics(),
            ),
            self.has_int32_hardware_atomic_counters(),
            self.has_int64_hardware_atomic_counters(),
            ind=ind,
            inc=inc,
        )
        return msg

    def misc_summary(self, indent=0, increment=2):
        ind = " " * indent
        inc = " " * increment
        if self.has_printf_support:
            pbs = self.printf_buffer_size()
            pbs = (
                f"up to {bytes2str(pbs)}"
                if isinstance(pbs, int)
                else "unknown buffer size"
            )
        msg = """
{ind}{inc}Miscellaneous:
{ind}{inc}{inc}*device splitting:         {}
{ind}{inc}{inc}*printf:                   {}
{ind}{inc}{inc}*profiling support:        {}
{ind}{inc}{inc}*queue priority:           {}
""".format(
            (
                f"yes, up to {self.max_subdevices()} subdevices"
                if self.has_device_partition_support()
                else "no"
            ),
            f"yes, {pbs}" if self.has_printf_support() else "no",
            f"yes, resolution is {time2str(self.profiling_time_resolution()).strip()}",
            "yes" if self.has_queue_priority_support() else "no",
            ind=ind,
            inc=inc,
        )
        return msg

    def extensions_summary(self, indent=0, increment=2):
        ind = " " * indent
        inc = " " * increment
        sep = "\n{ind}{inc}{inc}*".format(ind=ind, inc=inc)
        return f"\n{ind}{inc}Extensions:{sep}" + sep.join(sorted(self.extensions()))

    def to_string(self, indent=0, increment=2, short=False):
        new_indent = indent + increment
        ind = " " * indent
        inc = " " * increment

        if short:
            return """{ind}*Device {}:\n{}""".format(
                self.device_id,
                self.short_device_summary(indent=indent, increment=increment),
                ind=ind,
            )
        else:
            return """
    {ind}>Device {}: {}
    {}{}{}{}{}{}{}{}{}
    """.format(
                self.device_id,
                self.name(),
                self.device_summary(indent=indent, increment=increment),
                self.memory_summary(indent=indent, increment=increment),
                self.kernel_summary(indent=indent, increment=increment),
                self.fp_support_summary(indent=indent, increment=increment),
                self.vectors_summary(indent=indent, increment=increment),
                self.image_summary(indent=indent, increment=increment),
                self.atomics_summary(indent=indent, increment=increment),
                self.misc_summary(indent=indent, increment=increment),
                self.extensions_summary(indent=indent, increment=increment),
                ind=ind,
            )

    # OPENCL DEVICE SPECIFIC
    def version(self):
        return self._version

    def opencl_version(self):
        return self._cl_version

    def has_extension(self, extension):
        return extension in self.extensions()

    def has_spir_support(self):
        return self.has_extension("cl_khr_spir")

    def pci_bus_id(self):
        """
        Return the PCI bus id of this device if possible.
        Format is '0000:bus:device.function' 8+5+3 = 16 bits
        Example: 0000:01:00.0
        """
        if self.type == DeviceType.CPU:
            return None
        else:
            if self.has_extension("cl_nv_device_attribute_query"):
                bus_id = self._pci_bus_id_nv
                slot_id = self._pci_slot_id_nv
                dev_id = slot_id >> 3
                fn_id = slot_id & 0x07
                bus_id0 = bus_id >> 8  # not sure if usefull
                bus_id1 = bus_id & 0xFF
                return f"{bus_id0:04x}:{bus_id1:02x}:{dev_id:02x}.{fn_id:01x}"
            elif self.has_extension("cl_amd_device_topology"):
                topo = self._topology_amd
                bus_id = topo.pcie.bus
                dev_id = topo.pcie.device
                fn_id = topo.pcie.function
                bus_id0 = bus_id >> 8  # not sure if usefull
                bus_id1 = bus_id & 0xFF
                return f"{bus_id0:04x}:{bus_id1:02x}:{dev_id:02x}.{fn_id:01x}"
            else:
                return UnknownDeviceAttribute()

    def extensions(self):
        return [ext.strip() for ext in self._extensions.split(" ") if ext.strip() != ""]

    def available(self):
        return bool(self._available)

    def compiler_available(self):
        return bool(self._compiler_available)

    def host_unified_memory(self):
        return self._host_unified_memory

    def opencl_c_version(self):
        return self._opencl_c_version.strip()

    @opencl_version_atleast(1, 1)
    def native_vector_width_char(self):
        return self._native_vector_width_char

    @opencl_version_atleast(1, 1)
    def native_vector_width_short(self):
        return self._native_vector_width_short

    @opencl_version_atleast(1, 1)
    def native_vector_width_int(self):
        return self._native_vector_width_int

    @opencl_version_atleast(1, 1)
    def native_vector_width_long(self):
        return self._native_vector_width_long

    @opencl_version_atleast(1, 1)
    def native_vector_width_float(self):
        return self._native_vector_width_float

    @opencl_version_atleast(1, 1)
    def native_vector_width_double(self):
        return self._native_vector_width_double

    @opencl_version_atleast(1, 1)
    def native_vector_width_half(self):
        return self._native_vector_width_half

    @opencl_version_atleast(1, 2)
    def built_in_kernels(self):
        return self._built_in_kernels

    @opencl_version_atleast(1, 2)
    def linker_available(self):
        return bool(self._linker_available)

    @opencl_version_atleast(1, 2)
    def partition_properties(self):
        return self._partition_properties

    @opencl_version_atleast(1, 2)
    def partition_affinity_domain(self):
        return self._partition_affinity_domain

    @opencl_version_atleast(1, 2)
    def partition_type(self):
        return self._partition_type

    @opencl_version_atleast(2, 0)
    def pipe_max_active_reservations(self):
        return self._pipe_max_active_reservations

    @opencl_version_atleast(2, 0)
    def pipe_max_packet_size(self):
        return self._pipe_max_packet_size

    @opencl_version_atleast(2, 0)
    def queue_on_device_max_size(self):
        return self._queue_on_device_max_size

    @opencl_version_atleast(2, 0)
    def queue_on_device_preferred_size(self):
        return self._queue_on_device_preferred_size

    @opencl_version_atleast(2, 0)
    def queue_on_host_properties(self):
        return self._queue_on_host_properties

    @opencl_version_atleast(2, 0)
    def spir_versions(self):
        return self._spir_versions

    @opencl_version_atleast(2, 0)
    def svm_capabilities(self):
        return self._svm_capabilities

    @opencl_version_atleast(2, 0)
    def max_global_variable_size(self):
        return self._max_global_variable_size

    @opencl_version_atleast(2, 1)
    def sub_group_independent_forward_progress(self):
        return self._sub_group_independent_forward_progress

    # DEVICE
    def name(self):
        return self._name

    def platform_name(self):
        return self._platform.name

    def type(self):
        return cl2hysop_device_type(self._type)

    def vendor(self):
        return self._vendor

    def vendor_id(self):
        return self._vendor_id

    def max_clock_frequency(self):
        return self._max_clock_frequency * 1e6

    def address_bits(self):
        return self._address_bits

    def little_endian(self):
        return bool(self._endian_little)

    def error_correction_support(self):
        return bool(self._error_correction_support)

    # KERNEL
    def max_grid_dim(self):
        return self._max_work_item_dimensions

    def max_grid_size(self):
        return UnknownDeviceAttribute()

    def max_block_dim(self):
        return self._max_work_item_dimensions

    def max_block_size(self):
        return np.asarray(self._max_work_item_sizes)

    def max_threads_per_block(self):
        return self._max_work_group_size

    def simd_lane_size(self):
        return self._simd_lane_size

    def max_constant_args(self):
        return self._max_constant_args

    def max_constant_buffer_size(self):
        return self._max_constant_buffer_size

    # MEMORY
    def global_mem_size(self):
        return self._global_mem_size

    def global_mem_cache_size(self):
        return self._global_mem_cache_size

    def global_mem_cacheline_size(self):
        return self._global_mem_cacheline_size

    def global_mem_cache_type(self):
        return cl2hysop_cache_type(self._global_mem_cache_type)

    def max_global_alloc_size(self):
        return self._max_mem_alloc_size

    def mem_base_addr_align(self):
        return self._mem_base_addr_align

    def local_mem_size(self):
        return self._local_mem_size

    def local_mem_type(self):
        return cl2hysop_mem_type(self._local_mem_type)

    def usable_local_mem_size(self):
        return self._usable_local_mem_size

    # DEVICE SPLITTING
    def has_device_partition_support(self):
        cl_version = self.opencl_version()
        if cl_version == (1, 0) or cl_version == (1, 1):
            return self.has_extension("cl_ext_device_fission")
        else:
            return self.max_subdevices > 1

    def max_subdevices(self):
        return self._partition_max_sub_devices

    # QUEUES
    @opencl_version_atleast(2, 0, False)
    def has_queue_priority_support(self):
        return True

    # FP SUPPORT
    def has_fp16(self):
        return self.has_extension("cl_khr_fp16")

    def has_fp32(self):
        return True

    def has_fp64(self):
        return self.has_extension("cl_khr_fp64") or self.has_extension("cl_amd_fp64")

    def fp16_config(self):
        assert self.has_fp16
        return cl2hysop_fpconfig(self._half_fp_config)

    def fp32_config(self):
        assert self.has_fp32
        return cl2hysop_fpconfig(self._single_fp_config)

    def fp64_config(self):
        assert self.has_fp64
        return cl2hysop_fpconfig(self._double_fp_config)

    # IMAGES
    def has_image_support(self):
        return bool(self._image_support)

    def max_image_args(self):
        assert self.has_image_support
        return self._max_read_image_args

    def max_read_image_args(self):
        assert self.has_image_support
        return self._max_read_image_args

    def max_write_image_args(self):
        assert self.has_image_support
        return self._max_write_image_args

    def max_samplers(self):
        assert self.has_image_support
        return self._max_samplers

    def has_1d_image_support(self):
        return self.has_image_support

    def has_2d_image_support(self):
        return self.has_image_support

    def has_3d_image_support(self):
        return self.has_image_support

    def has_1d_image_write_support(self):
        return self.max_write_image_args > 0

    def has_2d_image_write_support(self):
        return self.max_write_image_args > 0

    def has_3d_image_write_support(self):
        return (self.max_write_image_args > 0) and self.has_extension(
            "cl_khr_3d_image_writes"
        )

    @opencl_version_atleast(1, 2, False)
    def has_1d_image_array_support(self):
        return self.has_image_support

    @opencl_version_atleast(1, 2, False)
    def has_2d_image_array_support(self):
        return self.has_image_support

    def image_max_array_size(self):
        assert self.has_1d_image_array_support or self.has_2d_image_array_support
        return self._image_max_array_size

    def max_1d_image_size(self):
        assert self.has_1d_image_support
        return np.asarray([self._image2d_max_width])

    def max_1d_image_array_size(self):
        assert self.has_1d_image_array_support
        return np.asarray([self.image_max_array_size(), self._image2d_max_width])

    def max_2d_image_size(self):
        assert self.has_2d_image_support
        return np.asarray([self._image2d_max_height, self._image2d_max_width])

    def max_2d_image_array_size(self):
        assert self.has_2d_image_array_support
        return np.asarray(
            [
                self.image_max_array_size(),
                self._image2d_max_height,
                self._image2d_max_width,
            ]
        )

    def max_3d_image_size(self):
        assert self.has_3d_image_support
        return np.asarray(
            [self._image3d_max_depth, self._image3d_max_height, self._image3d_max_width]
        )

    @opencl_version_atleast(1, 2, False)
    def has_1d_image_from_buffer_support(self):
        return self.has_image_support()

    def has_2d_image_from_buffer_support(self):
        if self.opencl_version()[0] >= 2:
            return self.has_image_support()
        else:
            return self.has_extension("cl_khr_image2d_from_buffer")

    def has_3d_image_from_buffer_support(self):
        return False

    @opencl_version_atleast(2, 0, False)
    def has_2d_image_from_image_support(self):
        return self.has_image_support

    def image_max_buffer_size(self):
        assert (
            self.has_1d_image_from_buffer_support()
            or self.has_2d_image_from_buffer_support()
        )
        return self._image_max_buffer_size

    def image_pitch_aligment(self):
        assert self.has_2d_image_from_buffer_support()
        return self._image_pitch_alignment

    # ATOMICS
    def has_global_int32_atomics(self):
        if self.opencl_version() == (1, 0):
            return self.has_extension("cl_khr_global_int32_base_atomics")
        else:
            return True

    def has_global_int64_atomics(self):
        return self.has_extension("cl_khr_int64_base_atomics")

    def has_global_float32_atomics(self):
        return self.opencl_version()[0] >= 2

    def has_global_float64_atomics(self):
        return self.opencl_version()[0] >= 2

    def has_local_int32_atomics(self):
        if self.opencl_version() == (1, 0):
            return self.has_extension("cl_khr_local_int32_base_atomics")
        else:
            return True

    def has_local_int64_atomics(self):
        return self.has_extension("cl_khr_int64_base_atomics")

    def has_local_float32_atomics(self):
        return self.opencl_version()[0] >= 2

    def has_local_float64_atomics(self):
        return self.opencl_version()[0] >= 2

    def has_mixed_int32_atomics(self):
        return False

    def has_mixed_int64_atomics(self):
        return False

    def has_mixed_float32_atomics(self):
        return False

    def has_mixed_float64_atomics(self):
        return False

    def has_int32_hardware_atomic_counters(self):
        return self.has_extension("cl_ext_atomic_counters_32")

    def has_int64_hardware_atomic_counters(self):
        return self.has_extension("cl_ext_atomic_counters_64")

    @opencl_version_atleast(2, 0, returned=1)
    def preferred_platform_atomic_alignment(self):
        return self._preferred_platform_atomic_alignment

    @opencl_version_atleast(2, 0, returned=1)
    def preferred_local_atomic_alignment(self):
        return self._preferred_local_atomic_alignment

    @opencl_version_atleast(2, 0, returned=1)
    def preferred_global_atomic_alignment(self):
        return self._preferred_global_atomic_alignment

    # PROFILING
    def has_profiling_support(self):
        return True

    def profiling_time_resolution(self):
        assert self.has_profiling_support
        if self._profiling_timer_resolution == 0:
            return "unknown"
        else:
            return self._profiling_timer_resolution * 1e-9

    # PRINTF
    @opencl_version_atleast(1, 2, returned=False)
    def has_printf_support(self):
        return True

    def printf_buffer_size(self):
        assert self.has_printf_support
        try:
            return self._printf_buffer_size
        except:
            return UnknownDeviceAttribute()

    # VECTORS
    def preferred_vector_width_char(self):
        return self._preferred_vector_width_char

    def preferred_vector_width_short(self):
        return self._preferred_vector_width_short

    def preferred_vector_width_int(self):
        return self._preferred_vector_width_int

    def preferred_vector_width_long(self):
        return self._preferred_vector_width_long

    def preferred_vector_width_half(self):
        return self._preferred_vector_width_half

    def preferred_vector_width_float(self):
        return self._preferred_vector_width_float

    def preferred_vector_width_double(self):
        return self._preferred_vector_width_double

    # GRAPHIC API INTEROPS
    def has_gl_sharing(self):
        return self.has_extension("cl_khr_gl_sharing")

    def has_gl_event_sharing(self):
        return self.has_extension("cl_khr_gl_event")

    def has_gl_msaa_sharing(self):
        return self.has_extension("cl_khr_gl_msaa_sharing")

    def has_dx9_sharing(self):
        return self.has_extension("cl_khr_dx9_media_sharing")

    def has_dx10_sharing(self):
        return self.has_extension("cl_khr_d3d10_sharing")

    def has_dx11_sharing(self):
        return self.has_extension("cl_khr_d3d11_sharing")

    def stats(self):
        return OpenClDeviceStatistics(self)


class OpenClDeviceStatistics(HardwareStatistics):
    def __init__(self, device):
        self._name = None
        self._counter = 0
        self._global_mem_size = []
        self._type = []
        self._clpeak_stats = None
        if device is not None:
            check_instance(device, OpenClDevice)
            self._name = device._name
            self._counter += 1
            self._global_mem_size += [device._global_mem_size]
            self._type = device.type()
            self._clpeak_stats = device._clpeak_info.stats()

    def __iadd__(self, other):
        if other is None:
            return self
        if isinstance(other, OpenClDevice):
            other = other.stats()
        if not isinstance(other, OpenClDeviceStatistics):
            msg = "Unknown type {}, expected OpenClDevice or OpenClDeviceStatistics."
            msg = msg.format(type(other))
            raise TypeError(msg)
        if self._counter == 0:
            assert self._name is None
            self._name = other._name
            self._type = other._type
            self._clpeak_stats = other._clpeak_stats
        else:
            assert self._name == other._name
            self._clpeak_stats += other._clpeak_stats
        self._counter += other._counter
        self._global_mem_size += other._global_mem_size
        return self

    def to_string(self, indent=0, increment=2):
        ind = " " * indent
        inc = " " * increment
        ss = [
            "{{ind}}{:^4} {:^10} {:^10} {} {}".format(
                self._counter,
                self._type,
                bytes2str(np.mean(self._global_mem_size)),
                self._clpeak_stats,
                self._name,
            )
        ]
        return "\n".join(s.format(ind=ind, inc=inc) for s in ss)
