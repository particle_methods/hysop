# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import warnings

from hysop.tools.numpywrappers import npw
from hysop.tools.htypes import check_instance
from hysop.tools.numerics import is_complex
from hysop.tools.misc import upper_pow2_or_3
from hysop.constants import AutotunerFlags, BoundaryCondition, SymbolicExpressionKind
from hysop.numerics.remesh.remesh import RemeshKernel
from hysop.fields.cartesian_discrete_field import CartesianDiscreteScalarFieldView
from hysop.symbolic.array import (
    OpenClSymbolicArray,
    OpenClSymbolicBuffer,
    OpenClSymbolicNdBuffer,
)
from hysop.fields.discrete_field import DiscreteScalarFieldView

from hysop.backend.device.codegen import CodeGeneratorWarning
from hysop.backend.device.codegen.kernels.custom_symbolic import (
    CustomSymbolicKernelGenerator,
)

from hysop.backend.device.opencl import cl, clTools
from hysop.backend.device.opencl.opencl_array import OpenClArray
from hysop.backend.device.opencl.opencl_autotunable_kernel import (
    OpenClAutotunableKernel,
)
from hysop.backend.device.kernel_autotuner import KernelGenerationError


class OpenClAutotunableCustomSymbolicKernel(OpenClAutotunableKernel):
    """Autotunable interface for directional remeshing kernel code generators."""

    @classmethod
    def sort_key_by_name(cls, iterator):
        """Utility to sort a dictionary by key name."""
        # We sort objects because this has an influence on generated
        # argument order (and for autotuner hashing)
        return sorted(iterator, key=lambda x: x[0].name)

    def autotune(
        self,
        expr_info,
        hardcode_arrays=True,
        has_complex=False,
        disable_vectorization=False,
        debug=False,
        **kwds,
    ):
        """Autotune this kernel with specified configuration.

        hardcode_arrays means that array offset and strides can be hardcoded
        into the kernels as constants.
        """
        check_instance(
            tuple(expr_info.input_dfields.values()),
            tuple,
            values=CartesianDiscreteScalarFieldView,
        )
        check_instance(
            tuple(expr_info.output_dfields.values()),
            tuple,
            values=CartesianDiscreteScalarFieldView,
        )

        granularity = expr_info.compute_granularity

        dim = expr_info.dim
        direction = expr_info.direction

        precision = self.typegen.dtype
        ftype = clTools.dtype_to_ctype(precision)

        if (dim < 1) or (dim > 16):
            msg = "Dimension should be between 1 and 16."
            raise ValueError(dim)
        if not issubclass(precision, npw.floating):
            msg = f"Precision is not a npw.floating subtype, got {precision}."
            raise TypeError(msg)

        cshape = expr_info.compute_resolution
        array_dim = len(cshape)
        iter_shape = cshape[:granularity]
        work_size = npw.asarray(cshape, dtype=npw.int32).copy()[granularity:][::-1]
        kernel_dim = work_size.size

        work_dim = min(kernel_dim, self.max_device_work_dim())
        work_size = work_size[:work_dim]

        DEBUG = False
        if DEBUG:
            print(
                """
                dim:    {}
                dir:    {}
                prec:   {}
                ftype:  {}
                cshape: {}

                array_dim:  {}
                iter_shape: {}
                work_size:  {}
                kernel_dim: {}
            """.format(
                    dim,
                    direction,
                    precision,
                    ftype,
                    cshape,
                    array_dim,
                    iter_shape,
                    work_size,
                    work_dim,
                )
            )

        min_ghosts = npw.dim_zeros(dim)
        for mg in expr_info.min_ghosts.values():
            min_ghosts = npw.maximum(min_ghosts, mg)
        assert (min_ghosts >= 0).all(), min_ghosts
        min_wg_size = 2 * min_ghosts[dim - 1 - direction] + 1
        assert min_wg_size >= 1, min_wg_size

        name = expr_info.name
        for dfields in (expr_info.input_dfields, expr_info.output_dfields):
            for field, dfield in dfields.items():
                if (dfield.compute_resolution != cshape).any():
                    msg = "Resolution mismatch between discrete fields, "
                    msg += "got {} but cshape={}, cannot generate kernel."
                    msg = msg.format(dfield.compute_resolution, cshape)
                    raise ValueError(msg)
        for field, dfield in expr_info.input_dfields.items():
            if (dfield.dfield.ghosts < expr_info.min_ghosts[field]).any():
                msg = "Min ghosts condition not met for discrete field {}:\n"
                msg += " expected {} but got only {}."
                msg = msg.format(
                    dfield.name, expr_info.min_ghosts[field], dfield.ghosts
                )
                raise ValueError(msg)

        for mem_objects in (
            expr_info.input_arrays.values(),
            expr_info.output_arrays.values(),
            expr_info.input_buffers.values(),
            expr_info.output_buffers.values(),
        ):
            for mem_obj in mem_objects:
                if not mem_obj.is_bound:
                    msg = "Memory Object {}::{} has not been bound to any memory "
                    msg += "prior to code generation."
                    msg = msg.format(type(mem_obj).__name__, mem_obj.name)
                    raise RuntimeError(msg)

        for arrays in (
            expr_info.input_arrays.values(),
            expr_info.output_arrays.values(),
        ):
            for array in arrays:
                if not npw.array_equal(array.shape, cshape):
                    msg = "Resolution mismatch between discrete fields, "
                    msg += "got {} but cshape={}, cannot generate kernel."
                    msg = msg.format(dfield.compute_resolution, cshape)
                    raise ValueError(msg)

        make_offset, offset_dtype = self.make_array_offset()
        make_strides, strides_dtype = self.make_array_strides(
            array_dim, hardcode_arrays=hardcode_arrays
        )
        parameter_dtypes, parameter_make = {}, {}
        (
            make_gidx,
            gidx_dtype,
        ) = self.make_array_granularity_index(granularity)

        kernel_args = {}
        known_args = {}
        isolation_params = {}
        mesh_info_vars = {}
        target_stride_args = known_args if hardcode_arrays else kernel_args
        target_offset_args = known_args if hardcode_arrays else kernel_args

        # read-only input fields (opencl buffers can be marked read-only in discrete field views)
        # read-only input arrays and input buffers
        di = expr_info.discretization_info
        for obj, counts in self.sort_key_by_name(di.read_counter.items()):
            if isinstance(obj, di.IndexedCounterTypes):
                assert isinstance(obj, DiscreteScalarFieldView)
                dfield = expr_info.input_dfields[obj._field]
                mesh_info_name = f"{dfield.var_name}_mesh_info"
                mesh_info_vars[mesh_info_name] = self.mesh_info(
                    mesh_info_name, dfield.mesh
                )
                for i, count in enumerate(counts):
                    if count == 0:
                        continue
                    if (dfield in di.write_counter) and (
                        di.write_counter[dfield][i] > 0
                    ):
                        continue
                    vname = dfield.var_name + "_" + str(i)
                    kernel_args[vname + "_base"] = dfield.data[i].base_data
                    target_stride_args[vname + "_strides"] = make_strides(
                        dfield.data[i].strides, dfield.dtype
                    )
                    target_offset_args[vname + "_offset"] = make_offset(
                        dfield.data[i].offset, dfield.dtype
                    )
                    isolation_params[vname + "_base"] = dict(
                        count=dfield.npoints, dtype=dfield.dtype, fill=i
                    )
            elif isinstance(obj, di.SimpleCounterTypes):
                assert isinstance(
                    obj,
                    (OpenClSymbolicArray, OpenClSymbolicBuffer, OpenClSymbolicNdBuffer),
                ), type(obj)
                if counts == 0:
                    continue
                if (obj in di.write_counter) and (di.write_counter[obj] > 0):
                    continue
                vname = obj.varname
                kernel_args[vname + "_base"] = obj.base_data
                target_stride_args[vname + "_strides"] = make_strides(
                    obj.strides, obj.dtype
                )
                target_offset_args[vname + "_offset"] = make_offset(
                    obj.offset, obj.dtype
                )
                isolation_params[vname + "_base"] = dict(
                    count=obj.size, dtype=obj.dtype, fill=0
                )
            else:
                msg = f"Unsupported type {type(obj)}."
                raise TypeError(msg)
            has_complex |= is_complex(obj.dtype)

        # output fields, output arrays (output buffers are not supported yet)
        for obj, counts in self.sort_key_by_name(di.write_counter.items()):
            if isinstance(obj, di.IndexedCounterTypes):
                assert isinstance(obj, DiscreteScalarFieldView)
                dfield = expr_info.output_dfields[obj._field]
                mesh_info_name = f"{dfield.var_name}_mesh_info"
                mesh_info_vars[mesh_info_name] = self.mesh_info(
                    mesh_info_name, dfield.mesh
                )
                for i, count in enumerate(counts):
                    if count == 0:
                        continue
                    vname = dfield.var_name + "_" + str(i)
                    kernel_args[vname + "_base"] = dfield.data[i].base_data
                    target_stride_args[vname + "_strides"] = make_strides(
                        dfield.data[i].strides, dfield.dtype
                    )
                    target_offset_args[vname + "_offset"] = make_offset(
                        dfield.data[i].offset, dfield.dtype
                    )
                    isolation_params[vname + "_base"] = dict(
                        count=dfield.npoints, dtype=dfield.dtype, fill=i
                    )
            elif isinstance(obj, di.SimpleCounterTypes):
                assert isinstance(
                    obj,
                    (OpenClSymbolicArray, OpenClSymbolicBuffer, OpenClSymbolicNdBuffer),
                ), type(obj)
                vname = obj.varname
                kernel_args[vname + "_base"] = obj.base_data
                target_stride_args[vname + "_strides"] = make_strides(
                    obj.strides, obj.dtype
                )
                target_offset_args[vname + "_offset"] = make_offset(
                    obj.offset, obj.dtype
                )
                isolation_params[vname + "_base"] = dict(
                    count=obj.size, dtype=obj.dtype, fill=0
                )
                if counts == 0:
                    continue
            else:
                msg = f"Unsupported type {type(obj)}."
                raise TypeError(msg)
            has_complex |= is_complex(obj.dtype)

        # read-only input parameters
        for pname, param in expr_info.input_params.items():
            if pname in expr_info.output_params:
                continue
            has_complex |= is_complex(param.dtype)
            make_param, param_dtype = self.make_parameter(param)
            parameter_dtypes[pname] = param_dtype
            if param.const:
                known_args[pname] = param.value
                assert param.value.dtype == param_dtype
            else:
                kernel_args[pname] = make_param()
                parameter_make[pname] = make_param
                assert make_param().dtype == param_dtype

        # output parameters
        for pname, param in expr_info.output_params.items():
            has_complex |= is_complex(param.dtype)
            if param.const:
                msg = "A constant parameter cannot be set as output parameter."
                raise RuntimeError(msg)
            raise NotImplementedError(
                "Output parameters are not implemented in the backend OpenCL."
            )

        # granularity argument
        if granularity > 0:
            kernel_args["gidx"] = make_gidx(
                [
                    0,
                ]
                * granularity
            )

        return super().autotune(
            name=name,
            expr_info=expr_info,
            kernel_dim=kernel_dim,
            compute_shape=cshape,
            precision=precision,
            ftype=ftype,
            mesh_info_vars=mesh_info_vars,
            work_dim=work_dim,
            work_size=work_size,
            min_wg_size=min_wg_size,
            known_args=known_args,
            kernel_args=kernel_args,
            hardcode_arrays=hardcode_arrays,
            granularity=granularity,
            iter_shape=iter_shape,
            gidx_dtype=gidx_dtype,
            make_gidx=make_gidx,
            offset_dtype=offset_dtype,
            strides_dtype=strides_dtype,
            parameter_dtypes=parameter_dtypes,
            parameter_make=parameter_make,
            isolation_params=isolation_params,
            has_complex=has_complex,
            disable_vectorization=disable_vectorization,
            debug=debug,
            **kwds,
        )

    def compute_args_mapping(self, extra_kwds, extra_parameters):
        """
        Return arguments mapping which is a dictionnary
        with arguments names as keys and tuples a values.

        Tuples should contain (arg_position, arg_type(s)) with
        arg_position being an int and arg_type(s) a type or
        tuple of types which will be checked against.
        """

        strides_dtype = extra_kwds["strides_dtype"]
        offset_dtype = extra_kwds["offset_dtype"]
        gidx_dtype = extra_kwds["gidx_dtype"]
        hardcode_arrays = extra_kwds["hardcode_arrays"]
        granularity = extra_kwds["granularity"]
        expr_info = extra_kwds["expr_info"]
        parameter_dtypes = extra_kwds["parameter_dtypes"]

        args_mapping = {}
        arg_index = 0

        # read-only input fields, input arrays, input buffers
        di = expr_info.discretization_info
        for obj, counts in self.sort_key_by_name(di.read_counter.items()):
            if isinstance(obj, di.IndexedCounterTypes):
                assert isinstance(obj, DiscreteScalarFieldView)
                dfield = obj
                for i, count in enumerate(counts):
                    if count == 0:
                        continue
                    if (dfield in di.write_counter) and (
                        di.write_counter[dfield][i] > 0
                    ):
                        continue
                    vname = dfield.var_name + "_" + str(i)
                    args_mapping[vname + "_base"] = (arg_index, cl.MemoryObjectHolder)
                    arg_index += 1
                    if not hardcode_arrays:
                        args_mapping[vname + "_strides"] = (arg_index, strides_dtype)
                        arg_index += 1
                        args_mapping[vname + "_offset"] = (arg_index, offset_dtype)
                        arg_index += 1
            elif isinstance(obj, di.SimpleCounterTypes):
                assert isinstance(
                    obj,
                    (OpenClSymbolicArray, OpenClSymbolicBuffer, OpenClSymbolicNdBuffer),
                ), type(obj)
                if counts == 0:
                    continue
                if (obj in di.write_counter) and (di.write_counter[obj] > 0):
                    continue
                vname = obj.varname
                args_mapping[vname + "_base"] = (arg_index, cl.MemoryObjectHolder)
                arg_index += 1
                if not hardcode_arrays:
                    args_mapping[vname + "_strides"] = (arg_index, strides_dtype)
                    arg_index += 1
                    args_mapping[vname + "_offset"] = (arg_index, offset_dtype)
                    arg_index += 1
            else:
                msg = f"Unsupported type {type(obj)}."
                raise TypeError(msg)

        # output fields, arrays
        for obj, counts in self.sort_key_by_name(di.write_counter.items()):
            if isinstance(obj, di.IndexedCounterTypes):
                assert isinstance(obj, DiscreteScalarFieldView)
                dfield = obj
                for i, count in enumerate(counts):
                    if count == 0:
                        continue
                    vname = dfield.var_name + "_" + str(i)
                    args_mapping[vname + "_base"] = (arg_index, cl.MemoryObjectHolder)
                    arg_index += 1
                    if not hardcode_arrays:
                        args_mapping[vname + "_strides"] = (arg_index, strides_dtype)
                        arg_index += 1
                    if not hardcode_arrays:
                        args_mapping[vname + "_offset"] = (arg_index, offset_dtype)
                        arg_index += 1
            elif isinstance(obj, di.SimpleCounterTypes):
                assert isinstance(
                    obj,
                    (OpenClSymbolicArray, OpenClSymbolicBuffer, OpenClSymbolicNdBuffer),
                ), type(obj)
                if counts == 0:
                    continue
                vname = obj.varname
                args_mapping[vname + "_base"] = (arg_index, cl.MemoryObjectHolder)
                arg_index += 1
                if not hardcode_arrays:
                    args_mapping[vname + "_strides"] = (arg_index, strides_dtype)
                    arg_index += 1
                    args_mapping[vname + "_offset"] = (arg_index, offset_dtype)
                    arg_index += 1
            else:
                msg = f"Unsupported type {type(obj)}."
                raise TypeError(msg)

        # read-only input parameters
        for pname, param in expr_info.input_params.items():
            if (pname in expr_info.output_params) or param.const:
                continue
            args_mapping[pname] = (arg_index, parameter_dtypes[pname])
            arg_index += 1

        # output parameters
        for pname, param in expr_info.output_params.items():
            assert not param.const
            args_mapping[pname] = (arg_index, cl.MemoryObjectHolder)
            arg_index += 1

        if granularity > 0:
            args_mapping["gidx"] = (arg_index, gidx_dtype)
            arg_index += 1

        assert len(args_mapping) == arg_index
        return args_mapping

    def compute_parameters(self, extra_kwds):
        """Register extra parameters to optimize."""
        check_instance(extra_kwds, dict, keys=str)
        cshape = extra_kwds["compute_shape"]

        if extra_kwds["disable_vectorization"]:
            vectorization_options = [
                1,
            ]
        else:
            if extra_kwds["has_complex"]:
                vectorization_options = [1, 2, 4, 8]
            else:
                vectorization_options = [1, 2, 4, 8, 16]

        autotuner_flag = self.autotuner_config.autotuner_flag
        if autotuner_flag == AutotunerFlags.ESTIMATE:
            max_workitem_workload = [1, 1, 1]
        elif autotuner_flag == AutotunerFlags.MEASURE:
            max_workitem_workload = [1, 1, 4]
        elif autotuner_flag == AutotunerFlags.PATIENT:
            max_workitem_workload = [1, 1, 16]
        elif autotuner_flag == AutotunerFlags.EXHAUSTIVE:
            max_workitem_workload = [1, 1, 64]

        work_dim = extra_kwds["work_dim"]
        max_workitem_workload = npw.asarray(max_workitem_workload[:work_dim])
        extra_kwds["max_work_load"] = max_workitem_workload

        params = super().compute_parameters(extra_kwds=extra_kwds)
        params.register_extra_parameter("vectorization", vectorization_options)
        return params

    def compute_min_max_wg_size(
        self, work_bounds, work_load, global_work_size, extra_parameters, extra_kwds
    ):
        """Default min and max workgroup size."""
        _min_wg_size = extra_kwds["min_wg_size"]
        vectorization = extra_parameters["vectorization"]
        min_wg_size = npw.ones(shape=work_bounds.work_dim, dtype=npw.int32)
        max_wg_size = npw.ones(shape=work_bounds.work_dim, dtype=npw.int32)
        min_wg_size[0] = _min_wg_size
        max_wg_size[0] = max(
            (global_work_size[0] + vectorization - 1) // vectorization, _min_wg_size
        )
        return (min_wg_size, max_wg_size)

    def compute_global_work_size(
        self, work, local_work_size, extra_parameters, extra_kwds
    ):
        # contiguous axe is iterated fully by one work_group
        global_work_size = (
            (work.global_work_size + local_work_size - 1) // local_work_size
        ) * local_work_size
        global_work_size[0] = local_work_size[0]
        return global_work_size

    def generate_kernel_src(
        self,
        global_work_size,
        local_work_size,
        extra_parameters,
        extra_kwds,
        tuning_mode,
        dry_run,
    ):
        """Generate kernel name and source code"""

        ## Extract usefull variables
        ftype = extra_kwds["ftype"]
        expr_info = extra_kwds["expr_info"]
        kernel_dim = extra_kwds["kernel_dim"]
        work_dim = extra_kwds["work_dim"]
        known_args = extra_kwds["known_args"]
        mesh_info_vars = extra_kwds["mesh_info_vars"]
        granularity = extra_kwds["granularity"]
        debug = extra_kwds["debug"]

        if dry_run:
            assert local_work_size is None
            assert global_work_size is None
            global_work_size = npw.asintarray((32,) * work_dim)
            local_work_size = npw.asintarray((32,) + (1,) * (work_dim - 1))

        ## Extract and check parameters
        vectorization = extra_parameters["vectorization"]

        ## Get compile time OpenCL known variables
        known_vars = super().generate_kernel_src(
            global_work_size=global_work_size,
            local_work_size=local_work_size,
            extra_parameters=extra_parameters,
            extra_kwds=extra_kwds,
            tuning_mode=tuning_mode,
            dry_run=dry_run,
        )
        known_vars.update(mesh_info_vars)
        known_vars.update(known_args)

        ## Generate OpenCL source code
        codegen = CustomSymbolicKernelGenerator.create(
            expr_info,
            typegen=self.typegen,
            ftype=ftype,
            kernel_dim=kernel_dim,
            work_dim=work_dim,
            granularity=granularity,
            vectorization=vectorization,
            symbolic_mode=self.symbolic_mode,
            tuning_mode=tuning_mode,
            debug_mode=False,
            known_vars=known_vars,
        )

        if debug:
            print(
                f"User asked to debug kernel {codegen.name}, entering in edit mode and terminating program."
            )
            codegen.edit()
            codegen.test_compile()
            import sys

            sys.exit(1)

        kernel_name = codegen.name
        kernel_src = str(codegen)

        ## Check if cache would fit
        if local_work_size is not None:
            required_cache = codegen.required_workgroup_cache_size(local_work_size)
            if required_cache[1] > 0:
                msg = (
                    "Dynamic shared memory is not supported in custom symbolic kernels."
                )
                raise RuntimeError(msg)
            self.check_cache(required_cache[2])

        return (kernel_name, kernel_src)

    def hash_extra_kwds(self, extra_kwds):
        """Hash extra_kwds dictionnary for caching purposes."""
        kwds = (
            "kernel_dim",
            "work_dim",
            "ftype",
            "granularity",
            "known_args",
            "compute_shape",
        )
        return self.custom_hash(
            *tuple(extra_kwds[kwd] for kwd in kwds),
            mesh_info_vars=extra_kwds["mesh_info_vars"],
            expr_info=extra_kwds["expr_info"],
        )

    def format_best_candidate(self, **kwds):
        from hysop.backend.device.opencl.opencl_kernel import (
            OpenClKernelParameterYielder,
        )

        (kernel, args_dict) = super().format_best_candidate(**kwds)

        extra_kwds = kwds["extra_kwds"]
        iter_shape = extra_kwds["iter_shape"]
        make_gidx = extra_kwds["make_gidx"]
        granularity = extra_kwds["granularity"]
        parameter_make = extra_kwds["parameter_make"]

        if granularity > 0:

            def granularity_iterator():
                for idx in npw.ndindex(*iter_shape):
                    yield make_gidx(idx[::-1])

            args_dict["gidx"] = OpenClKernelParameterYielder(granularity_iterator)

        # pop non constant input parameters
        for pname in parameter_make.keys():
            args_dict.pop(pname)

        # return a method to update non constant input parameters
        def update_input_parameters():
            return {pname: pmake() for (pname, pmake) in parameter_make.items()}

        return (kernel, args_dict, update_input_parameters)
