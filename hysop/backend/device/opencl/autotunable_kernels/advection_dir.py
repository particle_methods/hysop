# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.numpywrappers import npw
from hysop.tools.htypes import check_instance
from hysop.constants import AutotunerFlags, DirectionLabels, BoundaryCondition
from hysop.numerics.odesolvers.runge_kutta import ExplicitRungeKutta

from hysop.backend.device.opencl import cl, clTools
from hysop.backend.device.codegen.kernels.directional_advection import (
    DirectionalAdvectionKernelGenerator,
)
from hysop.backend.device.opencl.opencl_array import OpenClArray
from hysop.backend.device.opencl.opencl_autotunable_kernel import (
    OpenClAutotunableKernel,
)


class OpenClAutotunableDirectionalAdvectionKernel(OpenClAutotunableKernel):
    """Autotunable interface for directional advection kernel code generators."""

    def autotune(
        self,
        direction,
        time_integrator,
        velocity_cfl,
        velocity,
        position,
        relative_velocity,
        hardcode_arrays,
        is_bilevel,
        **kwds,
    ):
        """Autotune this kernel with specified configuration."""

        precision = self.typegen.dtype

        dim = velocity.dim
        if not (1 <= dim <= 3):
            raise ValueError("Dimension mismatch.")
        if not (0 <= direction < dim):
            raise ValueError("Invalid direction.")
        if not issubclass(precision, npw.floating):
            raise TypeError("Precision is not a npw.floating subtype.")
        if not isinstance(time_integrator, ExplicitRungeKutta):
            msg = "Given time integrator is not an instance of ExplicitRungeKutta, "
            msg += f"got a '{time_integrator.__class__.__name__}'."
            raise ValueError(msg)
        if not isinstance(velocity_cfl, float) or velocity_cfl <= 0.0:
            raise ValueError("Invalid velocity_cfl value.")

        min_ghosts = DirectionalAdvectionKernelGenerator.min_ghosts(dim, velocity_cfl)
        cache_ghosts = min_ghosts[-1]
        self.check_cartesian_field(velocity, min_ghosts=min_ghosts, nb_components=1)
        if is_bilevel is not None:
            self.check_cartesian_field(
                position,
                nb_components=1,
                compute_resolution=position.compute_resolution,
            )
        else:
            self.check_cartesian_field(
                position,
                nb_components=1,
                compute_resolution=velocity.compute_resolution,
            )

        if (velocity.dtype != precision) or (position.dtype != precision):
            # TODO implement mixed precision kernels
            msg = "Array types (V={}, pos={}) do not match required operator precision {}."
            msg = msg.format(velocity.dtype, position.dtype, precision.__name__)
            raise NotImplementedError(msg)

        Vr = relative_velocity
        assert isinstance(Vr, (str, float))

        ftype = clTools.dtype_to_ctype(precision)
        name = "directional_advection{}_{}__{}_{}_{}g__{}".format(
            "" if (is_bilevel is None) else "_bilevel_linear",
            DirectionLabels[direction],
            time_integrator.name(),
            ftype,
            cache_ghosts,
            abs(hash(Vr)),
        )

        vboundaries = (velocity.global_lboundaries[-1], velocity.global_rboundaries[-1])

        eps = npw.finfo(precision).eps
        dt = velocity_cfl * velocity.space_step[-1]

        make_offset, offset_dtype = self.make_array_offset()
        make_strides, strides_dtype = self.make_array_strides(
            position.dim, hardcode_arrays=hardcode_arrays
        )

        kernel_args = {}
        known_args = {}
        isolation_params = {}
        mesh_info_vars = {}
        target_args = known_args if hardcode_arrays else kernel_args

        kernel_args["dt"] = precision(dt)

        kernel_args["V_base"] = velocity.sdata.base_data
        target_args["V_strides"] = make_strides(velocity.sdata.strides, velocity.dtype)
        target_args["V_offset"] = make_offset(velocity.sdata.offset, velocity.dtype)

        kernel_args["P_base"] = position.sdata.base_data
        target_args["P_strides"] = make_strides(position.sdata.strides, position.dtype)
        target_args["P_offset"] = make_offset(position.sdata.offset, position.dtype)

        isolation_params["V_base"] = dict(
            count=velocity.npoints, dtype=velocity.dtype, fill=1.00
        )
        isolation_params["P_base"] = dict(
            count=position.npoints, dtype=position.dtype, fill=0.00
        )

        mesh_info_vars["V_mesh_info"] = self.mesh_info("V_mesh_info", velocity.mesh)
        mesh_info_vars["P_mesh_info"] = self.mesh_info("P_mesh_info", position.mesh)

        return super().autotune(
            name=name,
            rk_scheme=time_integrator,
            kernel_args=kernel_args,
            cache_ghosts=cache_ghosts,
            vboundaries=vboundaries,
            precision=precision,
            ftype=ftype,
            mesh_info_vars=mesh_info_vars,
            work_dim=dim,
            work_size=position.compute_resolution[::-1],
            hardcode_arrays=hardcode_arrays,
            known_args=known_args,
            offset_dtype=offset_dtype,
            strides_dtype=strides_dtype,
            isolation_params=isolation_params,
            Vr=Vr,
            is_bilevel=is_bilevel,
            **kwds,
        )

    def compute_args_mapping(self, extra_kwds, extra_parameters):
        precision = extra_kwds["precision"]
        strides_dtype = extra_kwds["strides_dtype"]
        offset_dtype = extra_kwds["offset_dtype"]
        hardcode_arrays = extra_kwds["hardcode_arrays"]

        args_mapping = {}

        args_mapping["dt"] = (0, precision)

        args_mapping["V_base"] = (1, cl.MemoryObjectHolder)
        if not hardcode_arrays:
            args_mapping["V_strides"] = (2, strides_dtype)
            args_mapping["V_offset"] = (3, offset_dtype)
        i = 2 + 2 * (1 - hardcode_arrays)

        args_mapping["P_base"] = (i, cl.MemoryObjectHolder)
        if not hardcode_arrays:
            args_mapping["P_strides"] = (i + 1, strides_dtype)
            args_mapping["P_offset"] = (i + 2, offset_dtype)

        return args_mapping

    def compute_parameters(self, extra_kwds):
        """Register extra parameters to optimize."""
        check_instance(extra_kwds, dict, keys=str)
        work_dim = extra_kwds["work_dim"]

        ## Register extra parameters
        autotuner_flag = self.autotuner_config.autotuner_flag
        caching_options = [True]
        if autotuner_flag == AutotunerFlags.ESTIMATE:
            max_workitem_workload = [1, 1, 1]
            nparticles_options = [1]
        elif autotuner_flag == AutotunerFlags.MEASURE:
            max_workitem_workload = [1, 4, 1]
            nparticles_options = [1, 2, 4]
        elif autotuner_flag == AutotunerFlags.PATIENT:
            max_workitem_workload = [1, 8, 1]
            nparticles_options = [1, 2, 4, 8, 16]
        elif autotuner_flag == AutotunerFlags.EXHAUSTIVE:
            max_workitem_workload = [1, 8, 8]
            nparticles_options = [1, 2, 4, 8, 16]

        if extra_kwds["is_bilevel"] is not None:
            # Currently bilevel codegen is not vectorized
            nparticles_options = [1]
            max_workitem_workload = [1, 1, 1]
            caching_options = [True]

        extra_kwds["max_work_load"] = max_workitem_workload[:work_dim]

        params = super().compute_parameters(extra_kwds=extra_kwds)
        params.register_extra_parameter("is_cached", caching_options)
        params.register_extra_parameter("nparticles", nparticles_options)
        return params

    def compute_min_max_wg_size(
        self, work_bounds, work_load, global_work_size, extra_parameters, extra_kwds
    ):
        """Default min and max workgroup size."""
        cache_ghosts = extra_kwds["cache_ghosts"]
        min_wg_size = npw.ones(shape=work_bounds.work_dim, dtype=npw.int32)
        max_wg_size = npw.ones(shape=work_bounds.work_dim, dtype=npw.int32)
        min_wg_size[0] = 2 * cache_ghosts + 1
        if extra_kwds["is_bilevel"] is not None:
            min_wg_size[0] = extra_kwds["is_bilevel"][-1]
        max_wg_size[0] = max(global_work_size[0], min_wg_size[0])
        return (min_wg_size, max_wg_size)

    def compute_global_work_size(
        self, local_work_size, work, extra_parameters, extra_kwds
    ):
        gs = super().compute_global_work_size(
            local_work_size=local_work_size,
            work=work,
            extra_parameters=extra_parameters,
            extra_kwds=extra_kwds,
        )
        gs[0] = local_work_size[0]
        return gs

    def generate_kernel_src(
        self,
        global_work_size,
        local_work_size,
        extra_parameters,
        extra_kwds,
        tuning_mode,
        dry_run,
    ):
        """Generate kernel name and source code"""
        ## Extract usefull variables
        work_dim = extra_kwds["work_dim"]
        vboundaries = extra_kwds["vboundaries"]
        rk_scheme = extra_kwds["rk_scheme"]
        ftype = extra_kwds["ftype"]
        cache_ghosts = extra_kwds["cache_ghosts"]
        known_args = extra_kwds["known_args"]
        Vr = extra_kwds["Vr"]
        is_bilevel = extra_kwds["is_bilevel"]

        if (is_bilevel is not None) and (local_work_size is not None):
            local_work_size[0] = is_bilevel[-1]

        ## Get compile time OpenCL known variables
        known_vars = super().generate_kernel_src(
            global_work_size=global_work_size,
            local_work_size=local_work_size,
            extra_parameters=extra_parameters,
            extra_kwds=extra_kwds,
            tuning_mode=tuning_mode,
            dry_run=dry_run,
        )
        known_vars.update(extra_kwds["mesh_info_vars"])
        known_vars.update(known_args)

        # disable periodic-periodic because we exchange ghosts anyways
        if vboundaries == (BoundaryCondition.PERIODIC, BoundaryCondition.PERIODIC):
            vboundaries = (BoundaryCondition.NONE, BoundaryCondition.NONE)

        ## Generate OpenCL source code
        codegen = DirectionalAdvectionKernelGenerator(
            typegen=self.typegen,
            work_dim=work_dim,
            vboundary=vboundaries,
            relative_velocity=Vr,
            rk_scheme=rk_scheme,
            symbolic_mode=self.symbolic_mode,
            ftype=ftype,
            min_ghosts=cache_ghosts,
            tuning_mode=tuning_mode,
            is_bilevel=extra_kwds["is_bilevel"],
            known_vars=known_vars,
            **extra_parameters,
        )

        kernel_name = codegen.name
        kernel_src = str(codegen)

        ## Check if cache would fit
        if local_work_size is not None:
            self.check_cache(codegen.required_workgroup_cache_size(local_work_size)[2])
        if is_bilevel is not None:
            self.check_cache(codegen.required_workgroup_velocity_cache_size())

        return (kernel_name, kernel_src)

    def hash_extra_kwds(self, extra_kwds):
        """Hash extra_kwds dictionnary for caching purposes."""
        kwds = (
            "rk_scheme",
            "ftype",
            "work_dim",
            "Vr",
            "vboundaries",
            "cache_ghosts",
            "work_size",
            "known_args",
        )
        return self.custom_hash(
            *tuple(extra_kwds[kwd] for kwd in kwds),
            mesh_info_vars=extra_kwds["mesh_info_vars"],
        )
