# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import math
import itertools as it

from hysop.tools.numpywrappers import npw
from hysop.tools.htypes import check_instance
from hysop.tools.misc import upper_pow2, previous_pow2, upper_pow2_or_3
from hysop.tools.units import bytes2str
from hysop.constants import AutotunerFlags
from hysop.backend.device.opencl import cl, clTools, clArray
from hysop.backend.device.opencl.opencl_autotunable_kernel import (
    OpenClAutotunableKernel,
)
from hysop.backend.device.codegen.kernels.transpose import TransposeKernelGenerator
from hysop.backend.device.kernel_autotuner import KernelGenerationError


class OpenClAutotunableTransposeKernel(OpenClAutotunableKernel):
    """Autotunable interface for transpose kernel code generators."""

    def _max_tile_size(self, shape, dtype, tile_indexes, is_inplace):
        """Compute max tile size that will fit in device cache."""
        nbytes = dtype.itemsize
        factor = 2.0 if is_inplace else 1.0
        max_cache_elems = int(self.usable_cache_bytes_per_wg / (factor * nbytes))

        if len(tile_indexes) == 2:
            x = int(npw.sqrt(max_cache_elems))
            # while x*(x+1) > max_cache_elems:
            # x-=1
            # tile offsetting will just trigger the usual cache exception
            max_ts_cache = x
        else:
            # no cache is used
            max_ts_cache = npw.inf

        tile_shape = shape[tile_indexes]
        max_ts_shape = max(tile_shape)

        max_tile_size = min(max_ts_cache, max_ts_shape)
        return max_tile_size

    def autotune(
        self,
        is_inplace,
        input_buffer,
        output_buffer,
        axes,
        hardcode_arrays,
        name=None,
        **kwds,
    ):
        """Autotune this kernel with specified axes, inputs and outputs."""

        check_instance(axes, tuple, values=int)
        check_instance(is_inplace, bool)
        check_instance(input_buffer, clArray.Array)
        check_instance(output_buffer, clArray.Array)

        assert input_buffer.ndim == output_buffer.ndim
        assert input_buffer.size == output_buffer.size
        assert input_buffer.dtype == output_buffer.dtype

        dim = input_buffer.ndim
        size = input_buffer.size
        shape = npw.asintarray(input_buffer.shape[::-1])
        dtype = input_buffer.dtype
        ctype = clTools.dtype_to_ctype(dtype)

        # check if the permutation is valid
        assert dim >= 2
        assert len(axes) == dim
        assert set(axes) == set(range(dim))
        assert axes != tuple(range(dim))

        # check if is_inplace is allowed
        if is_inplace:
            can_compute_inplace = dim == 2
            can_compute_inplace &= all(shape[0] == shape)
            msg = (
                "Inplace was specified but this is only possible for 2D square arrays."
            )
            if not can_compute_inplace:
                raise ValueError(msg)
            assert input_buffer.data == output_buffer.data

        # get vector size for strides
        make_offset, offset_dtype = self.make_array_offset()
        make_strides, strides_dtype = self.make_array_strides(
            dim, hardcode_arrays=hardcode_arrays
        )

        kernel_args = {}
        known_args = {}
        isolation_params = {}
        target_args = known_args if hardcode_arrays else kernel_args

        if is_inplace:
            kernel_args["inout_base"] = output_buffer.base_data
            target_args["inout_strides"] = make_strides(
                output_buffer.strides, output_buffer.dtype
            )
            target_args["inout_offset"] = make_offset(
                output_buffer.offset, output_buffer.dtype
            )
            isolation_params["inout_base"] = dict(
                count=output_buffer.size,
                dtype=output_buffer.dtype,
                range=slice(output_buffer.size),
            )
        else:
            kernel_args["in_base"] = input_buffer.base_data
            target_args["in_strides"] = make_strides(
                input_buffer.strides, input_buffer.dtype
            )
            target_args["in_offset"] = make_offset(
                input_buffer.offset, input_buffer.dtype
            )
            isolation_params["in_base"] = dict(
                count=input_buffer.size,
                dtype=input_buffer.dtype,
                range=slice(input_buffer.size),
            )

            kernel_args["out_base"] = output_buffer.base_data
            target_args["out_strides"] = make_strides(
                output_buffer.strides, output_buffer.dtype
            )
            target_args["out_offset"] = make_offset(
                output_buffer.offset, output_buffer.dtype
            )
            isolation_params["out_base"] = dict(
                count=output_buffer.size, dtype=output_buffer.dtype, fill=0
            )

        if name is None:
            name = "transpose_{}_[{}]_{}".format(
                ctype,
                ",".join(str(a) for a in axes),
                "inplace" if is_inplace else "out_of_place",
            )

        # if last axe (contiguous axe) is permuted,
        # we need to use 2D tiles else we only need 1D tiles.
        (last_axe_permuted, work_dim, work_shape, tile_indices) = (
            TransposeKernelGenerator.characterize_permutation(
                shape, axes, self.max_device_work_dim()
            )
        )

        # keyword arguments will be agregated into extra_kwds dictionnary
        return super().autotune(
            name=name,
            kernel_args=kernel_args,
            known_args=known_args,
            hardcode_arrays=hardcode_arrays,
            offset_dtype=offset_dtype,
            strides_dtype=strides_dtype,
            axes=axes,
            dtype=dtype,
            ctype=ctype,
            shape=shape,
            tile_indices=tile_indices,
            work_dim=work_dim,
            work_size=work_shape,
            is_inplace=is_inplace,
            isolation_params=isolation_params,
            last_axe_permuted=last_axe_permuted,
            **kwds,
        )

    def compute_parameters(self, extra_kwds):
        """Register extra parameters to optimize."""
        check_instance(extra_kwds, dict, keys=str)
        params = super().compute_parameters(extra_kwds=extra_kwds)

        ## Register extra parameters
        # compute max tile fize from device cache
        tile_indices = extra_kwds["tile_indices"]
        dtype = extra_kwds["dtype"]
        shape = extra_kwds["shape"]
        is_inplace = extra_kwds["is_inplace"]
        last_axe_permuted = extra_kwds["last_axe_permuted"]

        flag = self.autotuner_config.autotuner_flag
        vectorization = (1,)
        use_diagonal_coordinates = (False,)
        if last_axe_permuted:
            use_diagonal_coordinates += (True,)
        tile_padding = (
            0,
            1,
        )

        max_tile_size = self._max_tile_size(shape, dtype, tile_indices, is_inplace)
        imax = int(math.log(max_tile_size, 2))
        jmax = (
            int(math.log(max_tile_size, 3))
            if flag in (AutotunerFlags.EXHAUSTIVE,)
            else 0
        )
        tile_sizes = tuple(
            int((2**i) * (3**j))
            for (i, j) in it.product(range(0, imax + 1), range(0, jmax + 1))
        )
        tile_sizes = (max_tile_size,) + tuple(sorted(tile_sizes, reverse=True))
        tile_sizes = tuple(
            filter(
                lambda x: (x >= max_tile_size // 8) and (x <= max_tile_size), tile_sizes
            )
        )

        params.register_extra_parameter("vectorization", vectorization)
        params.register_extra_parameter(
            "use_diagonal_coordinates", use_diagonal_coordinates
        )
        params.register_extra_parameter("tile_padding", tile_padding)
        params.register_extra_parameter("tile_size", tile_sizes)

        return params

    def compute_work_candidates(
        self, work_bounds, work_load, extra_parameters, extra_kwds
    ):
        """
        Configure work (global_size, local_size candidates) given a
        OpenClWorkBoundsConfiguration object and a work_load.

        Return a WorkConfiguration object.

        Notes
        -----
        global_work_size can be ignored if it depends on local_work_size and will be set
        in self.compute_global_work_size().
        """
        work = super().compute_work_candidates(
            work_bounds=work_bounds,
            work_load=work_load,
            extra_parameters=extra_parameters,
            extra_kwds=extra_kwds,
        )

        axes = extra_kwds["axes"]
        work_dim = extra_kwds["work_dim"]
        shape = extra_kwds["shape"]
        vectorization = extra_parameters["vectorization"]
        tile_size = extra_parameters["tile_size"]

        max_tile_work_size = TransposeKernelGenerator.max_local_worksize(
            axes=axes,
            shape=shape,
            work_dim=work_dim,
            tile_size=tile_size,
            vectorization=vectorization,
        )

        work.push_filter(
            "max_tile_worksize",
            work.max_wi_sizes_filter,
            max_work_item_sizes=max_tile_work_size,
        )

        return work

    def compute_global_work_size(
        self, local_work_size, work, extra_parameters, extra_kwds
    ):
        shape = extra_kwds["shape"]
        axes = extra_kwds["axes"]
        vectorization = extra_parameters["vectorization"]
        tile_size = extra_parameters["tile_size"]

        gs = TransposeKernelGenerator.compute_global_size(
            shape=shape,
            tile_size=tile_size,
            vectorization=vectorization,
            axes=axes,
            local_work_size=local_work_size,
            work_load=work.work_load,
        )
        return gs

    def generate_kernel_src(
        self,
        global_work_size,
        local_work_size,
        extra_parameters,
        extra_kwds,
        tuning_mode,
        dry_run,
        force_verbose=False,
        force_debug=False,
        return_codegen=False,
    ):
        """
        Generate kernel name and source code.
        """

        ## Extract usefull variables
        axes = extra_kwds["axes"]
        ctype = extra_kwds["ctype"]
        is_inplace = extra_kwds["is_inplace"]
        known_args = extra_kwds["known_args"]

        ## Get compile time OpenCL known variables
        known_vars = super().generate_kernel_src(
            global_work_size=global_work_size,
            local_work_size=local_work_size,
            extra_parameters=extra_parameters,
            extra_kwds=extra_kwds,
            tuning_mode=tuning_mode,
            dry_run=dry_run,
        )
        known_vars.update(known_args)
        known_vars["shape"] = self.to_vecn(extra_kwds["shape"], 0)

        ## Generate OpenCL source code
        codegen = TransposeKernelGenerator(
            axes=axes,
            typegen=self.typegen,
            ctype=ctype,
            is_inplace=is_inplace,
            symbolic_mode=self.symbolic_mode,
            known_vars=known_vars,
            debug_mode=force_debug,
            tuning_mode=tuning_mode,
            **extra_parameters,
        )

        kernel_name = codegen.name
        kernel_src = str(codegen)

        ## Check if cache would fit
        self.check_cache(codegen.required_workgroup_cache_size()[2])

        return (kernel_name, kernel_src)

    def compute_args_mapping(self, extra_kwds, extra_parameters):
        args_mapping = {}
        offset_dtype = extra_kwds["offset_dtype"]
        strides_dtype = extra_kwds["strides_dtype"]
        hardcode_arrays = extra_kwds["hardcode_arrays"]
        if extra_kwds["is_inplace"]:
            args_mapping["inout_base"] = (0, cl.MemoryObjectHolder)
            if not hardcode_arrays:
                args_mapping["inout_strides"] = (1, strides_dtype)
                args_mapping["inout_offset"] = (2, offset_dtype)
        else:
            args_mapping["in_base"] = (0, cl.MemoryObjectHolder)
            if not hardcode_arrays:
                args_mapping["in_strides"] = (1, strides_dtype)
                args_mapping["in_offset"] = (2, offset_dtype)
            args_mapping["out_base"] = (
                1 + 2 * (not hardcode_arrays),
                cl.MemoryObjectHolder,
            )
            if not hardcode_arrays:
                args_mapping["out_strides"] = (4, strides_dtype)
                args_mapping["out_offset"] = (5, offset_dtype)
        return args_mapping

    def hash_extra_kwds(self, extra_kwds):
        """Hash extra_kwds dictionnary for caching purposes."""
        return self.custom_hash(
            extra_kwds["ctype"],
            extra_kwds["axes"],
            extra_kwds["shape"],
            extra_kwds["is_inplace"],
            extra_kwds["known_args"],
        )
