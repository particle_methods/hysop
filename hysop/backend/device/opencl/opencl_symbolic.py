# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Abstract class providing a common interface to all
discrete operators working on the OpenCl backend
and using kernels generated on the fly from symbolic
expressions.

* :class:`~hysop.backend.device.opencl.opencl_operator.OpenClSymbolic` is an abstract class
    used to provide a common interface to all discrete operators working with the
    opencl backend and using kernels generated on the fly from symbolic expressions.
"""
import numpy as np
from abc import ABCMeta
from hysop.tools.decorators import debug
from hysop.tools.htypes import check_instance, first_not_None, InstanceOf, to_tuple
from hysop.tools.numpywrappers import npw
from hysop.fields.continuous_field import ScalarField, TensorField, Field
from hysop.parameters.tensor_parameter import Parameter, TensorParameter
from hysop.operator.base.custom_symbolic_operator import (
    ValidExpressions,
    SymbolicExpressionParser,
    CustomSymbolicOperatorBase,
)
from hysop.backend.device.opencl.opencl_operator import OpenClOperator
from hysop.constants import (
    ComputeGranularity,
    SpaceDiscretization,
    TranspositionState,
    DirectionLabels,
    SymbolicExpressionKind,
)
from hysop.numerics.interpolation.interpolation import MultiScaleInterpolation
from hysop.numerics.odesolvers.runge_kutta import TimeIntegrator
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.backend.device.opencl.autotunable_kernels.custom_symbolic import (
    OpenClAutotunableCustomSymbolicKernel,
)
from hysop.tools.sympy_utils import subscript, subscripts
from hysop.tools.parameters import MPIParams


class OpenClSymbolic(OpenClOperator):
    """
    Abstract class for discrete operators working on OpenCL backends
    that require custom symbolic kernels.
    """

    __default_method = (
        CustomSymbolicOperatorBase._CustomSymbolicOperatorBase__default_method
    )
    __available_methods = (
        CustomSymbolicOperatorBase._CustomSymbolicOperatorBase__available_methods
    )

    @classmethod
    def default_method(cls):
        dm = super().default_method()
        dm.update(cls.__default_method)
        return dm

    @classmethod
    def available_methods(cls):
        am = super().available_methods()
        am.update(cls.__available_methods)
        return am

    @debug
    def __new__(cls, **kwds):
        return super().__new__(cls, **kwds)

    @debug
    def __init__(self, **kwds):
        """
        Create an OpenClOperator with extra symbolic kernels.

        All input and output variable topologies should be of kind
        Backend.OPENCL and share the same OpenClEnvironment.

        Attributes
        ----------
        kwds: dict
            Base class keyword arguments.
        """
        super().__init__(**kwds)
        self.expressions = {}
        self.extra_kwds = {}
        self.expr_infos = {}
        self.symbolic_kernels = {}

    def require_symbolic_kernel(self, name, *exprs, **extra_kwds):
        if self.initialized:
            msg = (
                "{} has already been initialized, cannot require a new symbolic kernel."
            )
            msg = msg.format(self.name)
            raise RuntimeError(msg)
        check_instance(name, str)
        check_instance(exprs, tuple, values=ValidExpressions, minsize=1)
        self.expressions[name] = exprs
        self.extra_kwds[name] = extra_kwds

    @classmethod
    def __symbolic_variables(cls, *names, **kwds):
        scls = kwds.pop("scls")

        arrays = ()
        shape = to_tuple(kwds.get("shape", kwds.get("count", ())))
        if shape:
            assert len(names) == 1
            snames = tuple(
                names[0] + subscripts(idx, ",", disable_unicode=False)
                for idx in np.ndindex(*shape)
            )
            var_names = tuple(
                names[0] + subscripts(idx, "_", disable_unicode=True)
                for idx in np.ndindex(*shape)
            )
        else:
            snames = names
            var_names = names
            shape = len(names)
        for name, pname, var_name in zip(var_names, snames, var_names):
            assert "," not in name
            check_instance(name, str)
            arr = scls(name=name, pretty_name=pname, var_name=var_name, **kwds)
            arrays += (arr,)
        return np.asarray(arrays).reshape(shape)

    @classmethod
    def symbolic_ndbuffers(cls, *names, **kwds):
        from hysop.symbolic.array import OpenClSymbolicNdBuffer

        assert "memory_object" not in kwds
        assert "scls" not in kwds
        kwds["memory_object"] = None
        kwds["scls"] = OpenClSymbolicNdBuffer
        return cls.__symbolic_variables(*names, **kwds)

    @classmethod
    def symbolic_buffers(cls, *names, **kwds):
        from hysop.symbolic.array import OpenClSymbolicBuffer

        assert "memory_object" not in kwds
        assert "scls" not in kwds
        kwds["memory_object"] = None
        kwds["scls"] = OpenClSymbolicBuffer
        return cls.__symbolic_variables(*names, **kwds)

    @classmethod
    def symbolic_arrays(cls, *names, **kwds):
        from hysop.symbolic.array import OpenClSymbolicArray

        assert "memory_object" not in kwds
        assert "scls" not in kwds
        kwds["scls"] = OpenClSymbolicArray
        return cls.__symbolic_variables(*names, **kwds)

    @classmethod
    def symbolic_tmp_scalars(cls, *names, **kwds):
        from hysop.symbolic.tmp import TmpScalar

        assert "scls" not in kwds
        kwds["scls"] = TmpScalar
        return cls.__symbolic_variables(*names, **kwds)

    @classmethod
    def symbolic_constants(cls, *names, **kwds):
        from hysop.symbolic.constant import SymbolicConstant

        assert "scls" not in kwds
        kwds["scls"] = SymbolicConstant
        return cls.__symbolic_variables(*names, **kwds)

    @debug
    def handle_method(self, method):
        """
        Extract methods for symbolic kernels.
        """
        super().handle_method(method)

        self.cr = method.pop(ComputeGranularity)
        self.space_discretization = method.pop(SpaceDiscretization)
        self.time_integrator = method.pop(TimeIntegrator)
        self.interpolation = method.pop(MultiScaleInterpolation)
        assert 2 <= self.space_discretization, self.space_discretization
        assert self.space_discretization % 2 == 0, self.space_discretization

    def initialize(self, **kwds):
        super().initialize(**kwds)
        input_fields = self.input_fields
        output_fields = self.output_fields
        input_params = self.input_params
        output_params = self.output_params
        input_tensor_fields = self.input_tensor_fields
        output_tensor_fields = self.output_tensor_fields

        check_instance(
            input_fields, dict, keys=ScalarField, values=CartesianTopologyDescriptors
        )
        check_instance(
            output_fields, dict, keys=ScalarField, values=CartesianTopologyDescriptors
        )
        check_instance(input_params, dict, keys=Parameter, values=MPIParams)
        check_instance(output_params, dict, keys=Parameter, values=MPIParams)
        check_instance(input_tensor_fields, tuple, values=Field)
        check_instance(output_tensor_fields, tuple, values=Field)

        def _cmp(name, op_vars, expr_vars, exprs):
            for expr_var_key, expr_var_value in expr_vars.items():
                vname = (
                    expr_var_key if isinstance(expr_var_key, str) else expr_var_key.name
                )
                if expr_var_key not in op_vars:
                    msg = "{} has not been set as {} in {}.__init__() but is required "
                    msg += "by one of the following symbolic expressions:\n  *{}"
                    msg = msg.format(
                        expr_var_key,
                        name,
                        self.name,
                        "\n  *".join(str(x) for x in exprs),
                    )
                    raise RuntimeError(msg)
                if op_vars[expr_var_key] != expr_var_value:
                    msg = "{} has been set as {} in {}.__init__() but CartesianTopologyDescriptor "
                    msg += "value mismatch."
                    msg = msg.format(expr_var_key, name, self.name)
                    raise RuntimeError(msg)

        expr_infos = self.expr_infos
        variables = input_fields.copy()
        variables.update(output_fields)
        direction = None
        for name, exprs in self.expressions.items():
            expr_info = SymbolicExpressionParser.parse(name, variables, *exprs)
            if expr_info.has_direction:
                if direction is None:
                    direction = expr_info.direction
                elif expr_info.direction != direction:
                    print()
                    print("FATAL ERROR: Expressions cannot have different directions.")
                    print(expr_info)
                    print()
                    msg = f"{name} contains a directional expression."
                    raise RuntimeError(msg)
            _cmp("input_fields", input_fields, expr_info.input_fields, exprs)
            _cmp("output_fields", output_fields, expr_info.output_fields, exprs)
            _cmp(
                "input_params",
                dict((_.name, _) for _ in input_params.keys()),
                expr_info.input_params,
                exprs,
            )
            _cmp(
                "output_params",
                dict((_.name, _) for _ in output_params.keys()),
                expr_info.output_params,
                exprs,
            )
            assert 0 <= self.cr <= expr_info.max_granularity, self.cr
            expr_info.compute_granularity = self.cr
            expr_info.time_integrator = self.time_integrator
            expr_info.interpolation = self.interpolation
            expr_info.space_discretization = self.space_discretization
            expr_infos[name] = expr_info

    @debug
    def get_field_requirements(self):
        """Extract field requirements from first expression parsing stage."""
        requirements = super().get_field_requirements()

        for expr_info in self.expr_infos.values():
            expr_info.extract_obj_requirements()

            dim = expr_info.dim
            field_reqs = expr_info.field_requirements
            array_reqs = expr_info.array_requirements
            direction = expr_info.direction
            has_direction = expr_info.has_direction

            if has_direction:
                assert 0 <= direction < dim
                axes = TranspositionState[dim].filter_axes(
                    lambda axes: (axes[-1] == dim - 1 - direction)
                )
                axes = tuple(axes)

            min_ghosts_per_components = {}

            for fields, expr_info_fields, is_input, iter_requirements in zip(
                (self.input_fields, self.output_fields),
                (expr_info.input_fields, expr_info.output_fields),
                (True, False),
                (
                    requirements.iter_input_requirements,
                    requirements.iter_output_requirements,
                ),
            ):
                if not fields:
                    continue
                for field, td, req in iter_requirements():
                    if field not in expr_info_fields:
                        continue
                    min_ghosts = npw.int_zeros(shape=(field.nb_components, field.dim))
                    if has_direction:
                        req.axes = axes
                    for index in range(field.nb_components):
                        fname = f"{field.name}_{index}"
                        G = expr_info.min_ghosts_per_field_name.get(fname, 0)
                        if (field, index) in field_reqs:
                            fi_req = field_reqs[(field, index)]
                            if fi_req.axes:
                                if res.axes is not None:
                                    assert set(fi_req.axes).intersection(req.axes)
                                    req.axes = tuple(
                                        set(req.axes).intersection(fi_req.axes)
                                    )
                                else:
                                    req.axes = fi_req.axes
                            _min_ghosts = fi_req.min_ghosts.copy()
                            _max_ghosts = fi_req.max_ghosts.copy()
                            assert _min_ghosts[dim - 1 - direction] <= G
                            assert _max_ghosts[dim - 1 - direction] >= G
                            _min_ghosts[dim - 1 - direction] = G
                            req.min_ghosts = npw.maximum(_min_ghosts, req.min_ghosts)
                            req.max_ghosts = npw.minimum(_max_ghosts, req.max_ghosts)
                            min_ghosts[index] = _min_ghosts.copy()
                        else:
                            req.min_ghosts[dim - 1 - direction] = max(
                                G, req.min_ghosts[dim - 1 - direction]
                            )
                            min_ghosts[index][dim - 1 - direction] = G
                        assert req.min_ghosts[dim - 1 - direction] >= G
                        assert req.max_ghosts[dim - 1 - direction] >= G

                    if field not in min_ghosts_per_components:
                        min_ghosts_per_components[field] = min_ghosts

            expr_info.min_ghosts = {
                k: npw.max(v, axis=0) for (k, v) in min_ghosts_per_components.items()
            }
            expr_info.min_ghosts_per_components = {
                field: gpc[:, -1 - direction]
                for (field, gpc) in min_ghosts_per_components.items()
            }

            for array, reqs in array_reqs.items():
                expr_info.min_ghosts[array] = reqs.min_ghosts.copy()
                expr_info.min_ghosts_per_components[array] = reqs.min_ghosts[
                    -1 - direction
                ]
        return requirements

    @debug
    def discretize(self, force_symbolic_axes=None):
        """
        Discretize symbolic expressions.
        """
        if self.discretized:
            return
        super().discretize()
        for expr_info in self.expr_infos.values():
            expr_info.discretize_expressions(
                input_dfields=self.input_discrete_fields,
                output_dfields=self.output_discrete_fields,
                force_symbolic_axes=force_symbolic_axes,
            )

    @debug
    def setup(self, work):
        for expr_info in self.expr_infos.values():
            expr_info.setup_expressions(work)
            expr_info.check_arrays()
            expr_info.check_buffers()
        super().setup(work=work)
        self._collect_symbolic_kernels(work)

    def _collect_symbolic_kernels(self, work):
        cl_env = self.cl_env
        typegen = self.typegen
        autotuner_config = self.autotuner_config
        build_opts = self.build_options()

        kernel_autotuner = OpenClAutotunableCustomSymbolicKernel(
            cl_env=cl_env,
            typegen=typegen,
            build_opts=build_opts,
            autotuner_config=autotuner_config,
        )

        for name, expr_info in self.expr_infos.items():
            kernel, args_dict, update_input_parameters = kernel_autotuner.autotune(
                expr_info=expr_info, **self.extra_kwds[name]
            )
            kl = kernel.build_launcher(**args_dict)
            self.symbolic_kernels[name] = (kl, update_input_parameters)

    def _wrap_kernel(self, kernel_request_name):
        knl, knl_kwds = self.symbolic_kernels[kernel_request_name]

        def wrapped_kernel(knl=knl, knl_kwds=knl_kwds, queue=self.cl_env.default_queue):
            return knl(queue=queue, **knl_kwds())

        return wrapped_kernel
