# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.htypes import check_instance
from hysop.backend.hardware.hardware_backend import HardwareBackend
from hysop.backend.device.opencl import cl
from hysop.backend.device.opencl.opencl_platform import OpenClPlatform
from hysop.backend.hardware.hwinfo import HardwareStatistics


class OpenClBackend(HardwareBackend):

    def __init__(self, **kwds):
        super().__init__(**kwds)
        self._version = cl.VERSION
        self._version_status = cl.VERSION_STATUS
        self._version_text = cl.VERSION_TEXT
        self._cl_header_version = cl.get_cl_header_version()
        self._cl_header_version_text = ".".join(str(x) for x in self._cl_header_version)

    def _discover_platforms(self, hardware_topo):
        platforms = cl.get_platforms()
        for i, p in enumerate(platforms):
            plat = OpenClPlatform(
                hardware_topo=hardware_topo, platform_id=i, platform_handle=p
            )
            self._platforms[i] = plat

    def to_string(self, indent=0, increment=2):
        new_indent = indent + increment
        platforms = "\n".join(
            x.to_string(indent=new_indent, increment=increment)
            for x in self.platforms.values()
        )
        ss = """
{ind}::OpenClBackend::
{ind}{inc}Data collected using pyopencl {} compiled against OpenCL headers v{}.

{}""".format(
            self._version_text,
            self._cl_header_version_text,
            platforms,
            ind=" " * indent,
            inc=" " * increment,
        )
        return ss

    def stats(self):
        return OpenClBackendStatistics(self)

    def __str__(self):
        return self.to_string()


class OpenClBackendStatistics(HardwareStatistics):

    def __init__(self, backend=None):
        self._platform_statistics = {}
        if backend is not None:
            check_instance(backend, OpenClBackend)
            for pid, platform in backend._platforms.items():
                self._platform_statistics[platform._name] = platform.stats()

    @property
    def platform_statistics(self):
        return self._platform_statistics

    def __iadd__(self, other):
        if other is None:
            return self
        if isinstance(other, OpenClBackend):
            other = other.stats()
        if not isinstance(other, OpenClBackendStatistics):
            msg = "Unknown type {}, expected OpenClBackend or OpenClBackendStatistics."
            msg = msg.format(type(other))
            raise TypeError(msg)
        for pname, pstats in other._platform_statistics.items():
            if pname in self._platform_statistics:
                self._platform_statistics[pname] += pstats
            else:
                self._platform_statistics[pname] = pstats
        return self

    def to_string(self, indent=0, increment=2):
        ind = " " * indent
        inc = " " * increment
        ss = []
        for pname, plat in self._platform_statistics.items():
            ss += [f"{{ind}}Platform {pname}:"]
            ss += [plat.to_string(indent + increment, increment)]
        return "\n".join(s.format(ind=ind, inc=inc) for s in ss)
