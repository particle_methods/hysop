# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os, subprocess, sys
from abc import ABCMeta, abstractmethod

from hysop import __KERNEL_DEBUG__, vprint
from hysop.constants import Backend
from hysop.tools.numpywrappers import npw
from hysop.tools.htypes import check_instance, first_not_None, to_tuple, to_list
from hysop.tools.misc import upper_pow2_or_3
from hysop.tools.units import bytes2str
from hysop.tools.numerics import get_dtype
from hysop.core.mpi import main_rank

from hysop.backend.device.kernel_autotuner import KernelGenerationError
from hysop.backend.device.autotunable_kernel import (
    AutotunableKernel,
    AutotunerWorkConfiguration,
)
from hysop.backend.device.opencl import cl, clTools, clCharacterize, clArray
from hysop.backend.device.opencl.opencl_env import OpenClEnvironment
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.opencl.opencl_kernel import OpenClKernel
from hysop.backend.device.opencl.opencl_array import OpenClArray
from hysop.backend.device.opencl.opencl_kernel_statistics import OpenClKernelStatistics


class OpenClAutotunableKernel(AutotunableKernel):

    def __init__(self, cl_env, typegen, build_opts, autotuner_config, **kwds):
        super().__init__(
            autotuner_config=autotuner_config, build_opts=build_opts, **kwds
        )

        self.check_cl_env(cl_env, typegen)

        self.cl_env = cl_env
        self.typegen = typegen
        self.usable_cache_bytes_per_wg = clCharacterize.usable_local_mem_size(
            cl_env.device
        )

    def autotune(self, name, force_verbose=False, force_debug=False, **extra_kwds):
        from hysop.backend.device.opencl.opencl_kernel_autotuner import (
            OpenClKernelAutotuner,
        )

        autotuner = OpenClKernelAutotuner(name=name, tunable_kernel=self)

        best_candidate_results, file_basename, from_cache = autotuner.autotune(
            extra_kwds=extra_kwds, force_verbose=force_verbose, force_debug=force_debug
        )
        check_instance(best_candidate_results, dict)

        args_mapping = self.compute_args_mapping(
            extra_kwds=extra_kwds,
            extra_parameters=best_candidate_results["extra_parameters"],
        )
        args_list = autotuner._compute_args_list(
            args_mapping=args_mapping, **extra_kwds["kernel_args"]
        )

        return self.format_best_candidate(
            name=name,
            extra_kwds=extra_kwds,
            args_mapping=args_mapping,
            args_list=args_list,
            autotuner=autotuner,
            file_basename=file_basename,
            from_cache=from_cache,
            **best_candidate_results,
        )

    def compute_global_work_size(
        self, work, local_work_size, extra_parameters, extra_kwds
    ):
        """
        Compute aligned global_work_size from unaligned global_work_size
        and local_work_size.
        Input global_work_size may be None.
        """
        check_instance(work, AutotunerWorkConfiguration)
        check_instance(
            local_work_size, npw.ndarray, dtype=npw.int32, size=work.work_dim
        )
        check_instance(extra_parameters, dict, keys=str)
        check_instance(extra_kwds, dict, keys=str)
        global_work_size = work.global_work_size
        return (
            (global_work_size + local_work_size - 1) // local_work_size
        ) * local_work_size

    @abstractmethod
    def generate_kernel_src(
        self,
        global_work_size,
        local_work_size,
        extra_parameters,
        extra_kwds,
        tuning_mode,
        dry_run,
    ):
        """
        Generate kernel source code as a string.

        Returns opencl known arguments as a dictionnary
        for codegen capabilities.
        """
        known_vars = {}
        if (global_work_size is not None) and (local_work_size is not None):
            assert (
                (global_work_size % local_work_size) == 0
            ).all(), "global_works_size={} local_work_size={}".format(
                global_work_size, local_work_size
            )
            known_vars.update(
                {
                    "work_dim": int(global_work_size.size),
                    "global_size": global_work_size,
                    "local_size": local_work_size,
                    "num_groups": (global_work_size // local_work_size),
                }
            )
        return known_vars

    def format_best_candidate(
        self,
        autotuner,
        file_basename,
        from_cache,
        name,
        extra_kwds,
        extra_parameters,
        work_size,
        work_load,
        global_work_size,
        local_work_size,
        args_mapping,
        args_list,
        program,
        kernel,
        kernel_name,
        kernel_src,
        kernel_statistics,
        src_hash,
        extra_kwds_hash,
        extra_kwds_hash_logs,
    ):
        """
        Post treatment callback for autotuner results.
        Transform autotuner results in user friendly kernel wrappers.

        Return a OpenClKernel with default_queue and default_args set to None.
        Only default_global_size, default_local_size, and args_mapping are set.

        Use the build_launcher method to build OpenClKernelLauncher from this OpenClKernel.
        """
        check_instance(file_basename, str)
        check_instance(from_cache, bool)
        check_instance(extra_parameters, dict, keys=str)
        check_instance(extra_kwds, dict, keys=str)
        check_instance(work_load, tuple, values=npw.int32)
        check_instance(global_work_size, tuple, values=npw.int32)
        check_instance(local_work_size, tuple, values=npw.int32)
        check_instance(program, cl.Program)
        check_instance(kernel, cl.Kernel)
        check_instance(kernel_src, str)
        check_instance(kernel_name, str)
        check_instance(kernel_statistics, OpenClKernelStatistics)
        check_instance(src_hash, str)
        check_instance(extra_kwds_hash, str)
        check_instance(extra_kwds_hash_logs, str)

        autotuner_config = autotuner.autotuner_config
        if autotuner_config.filter_statistics(file_basename):
            kernel_hash_logs = self.generate_hash_logs(
                file_basename, extra_kwds_hash_logs
            )
            kernel_source = self.generate_source_file(file_basename, kernel_src)
            kernel_isolation = self.generate_oclgrind_isolation_file(
                kernel,
                file_basename,
                kernel_source,
                global_work_size,
                local_work_size,
                args_list,
                args_mapping,
                extra_kwds["isolation_params"],
            )

            if autotuner_config.postprocess_kernels:
                # /!\ REGENERATE KERNEL STATISTICS /!\
                # because of tuning_mode = false, kernel source code and performance may change
                del kernel_statistics
                for i, arg in enumerate(args_list):
                    kernel.set_arg(i, arg)
                kernel_statistics, _ = autotuner.bench_one_from_binary(
                    kernel=kernel,
                    target_nruns=autotuner_config.postprocess_nruns,
                    old_stats=None,
                    best_stats=None,
                    global_work_size=global_work_size,
                    local_work_size=local_work_size,
                )

                # execute command FILE_BASENAME FROM_CACHE
                # AUTOTUNER_DUMP_DIR  AUTOTUNER_NAME  KERNEL_NAME
                # MEAN_EXECUTION_TIME_NS  MIN_EXECUTION_TIME_NS  MAX_EXECUTION_TIME_NS
                # KERNEL_SOURCE_FILE  KERNEL_ISOLATION_FILE  KERNEL_HASH_LOGS_FILE
                # VENDOR_NAME  DEVICE_NAME
                # WORK_SIZE  WORK_LOAD
                # GLOBAL_WORK_SIZE  LOCAL_WORK_SIZE
                # EXTRA_PARAMETERS  EXTRA_KWDS_HASH  SRC_HASH
                command = [
                    str(autotuner_config.postprocess_kernels),
                    str(file_basename),
                    "1" if from_cache else "0",
                    str(autotuner_config.dump_folder),
                    str(autotuner.name),
                    str(kernel_name),
                    str(kernel_statistics.mean),
                    str(kernel_statistics.min),
                    str(kernel_statistics.max),
                    str(kernel_source),
                    str(kernel_isolation),
                    str(kernel_hash_logs),
                    str(kernel.context.devices[0].platform.name.strip()),
                    str(kernel.context.devices[0].name.strip()),
                    str(work_size),
                    str(work_load),
                    str(global_work_size),
                    str(local_work_size),
                    str(extra_parameters),
                    str(extra_kwds_hash),
                    str(src_hash),
                ]
                if autotuner_config.debug:
                    print(
                        f"POSTPROCESSING KERNEL {autotuner.name}:\n" + " ".join(command)
                    )
                try:
                    subprocess.check_call(command)
                except OSError as e:
                    msg = f"\nFATAL ERROR: Could not find or execute postprocessing script '{command[0]}'."
                    print(msg)
                    print()
                    raise
                except subprocess.CalledProcessError as e:
                    if e.returncode == 10:
                        msg = "Postprocessing script has requested to stop the simulation (return code 10), exiting."
                        vprint(msg)
                        sys.exit(0)
                    else:
                        msg = "\nFATAL ERROR: Failed to call autotuner postprocessing command.\n{}\n"
                        msg = msg.format(" ".join(command))
                        print(msg)
                        print()
                        raise

        kernel = OpenClKernel(
            name=autotuner.name,
            program=program,
            args_mapping=args_mapping,
            default_queue=None,
            default_global_work_size=global_work_size,
            default_local_work_size=local_work_size,
            default_args=None,
        )

        args_dict = extra_kwds["kernel_args"]

        return (kernel, args_dict)

    def generate_source_file(self, kernel_name, kernel_src, force=False):
        if (
            (not force)
            and (not self.autotuner_config.dump_kernels)
            and (not self.autotuner_config.generate_isolation_file)
        ):
            return None

        # dump the best kernel
        dump_folder = self.autotuner_config.dump_folder
        dump_file = dump_folder + "/{}__{}.cl".format(
            kernel_name.replace(" ", "_"), main_rank
        )
        if not os.path.exists(dump_folder) and (main_rank == 0):
            os.makedirs(dump_folder)
        with open(dump_file, "w+") as f:
            if self.autotuner_config.verbose:
                print(f"  >Saving OpenCL kernel source to '{dump_file}'.")
            f.write(kernel_src)
        return dump_file

    def generate_hash_logs(self, kernel_name, hash_logs, force=False):
        if (not force) and (not self.autotuner_config.dump_hash_logs):
            return None

        # dump the best kernel
        dump_folder = self.autotuner_config.dump_folder
        dump_file = dump_folder + "/{}__{}_hash_logs.txt".format(
            kernel_name.replace(" ", "_"), main_rank
        )
        if not os.path.exists(dump_folder) and (main_rank == 0):
            os.makedirs(dump_folder)
        with open(dump_file, "w+") as f:
            if self.autotuner_config.verbose:
                print(f"  >Saving hash logs to '{dump_file}'.")
            f.write(hash_logs)
        return dump_file

    def generate_oclgrind_isolation_file(
        self,
        kernel,
        kernel_name,
        kernel_source,
        global_work_size,
        local_work_size,
        args_list,
        args_mapping,
        isolation_params,
        force=False,
    ):

        if (not force) and (not self.autotuner_config.generate_isolation_file):
            return None
        if len(global_work_size) < 3:
            dim = len(global_work_size)
            global_work_size = tuple(x for x in global_work_size) + (1,) * (3 - dim)
            local_work_size = tuple(x for x in local_work_size) + (1,) * (3 - dim)

        sorted_args = tuple(sorted(args_mapping.items(), key=lambda x: x[1][0]))
        assert len(sorted_args) == len(args_list)

        dump_folder = self.autotuner_config.dump_folder
        dump_file = dump_folder + "/{}__{}.sim".format(
            kernel_name.replace(" ", "_"), main_rank
        )
        with open(dump_file, "w+") as f:
            msg = f"# Isolation configuration file for kernel {kernel_name}."
            msg += "\n# See https://github.com/jrprice/Oclgrind/wiki/Running-Kernels-in-Isolation "
            msg += "for more information."
            msg += "\n"
            msg += f"\n{kernel_source}"
            msg += f"\n{kernel.function_name}"
            msg += "\n{}".format(" ".join(str(x) for x in global_work_size))
            msg += "\n{}".format(" ".join(str(x) for x in local_work_size))
            msg += "\n"
            for (arg_name, (arg_pos, arg_types)), arg_value in zip(
                sorted_args, args_list
            ):
                if arg_name in isolation_params:
                    arg_isol = isolation_params[arg_name]
                elif isinstance(arg_value, npw.ndarray):
                    assert arg_value.dtype == arg_types
                    arg_isol = dict(count=arg_value.size, dtype=arg_value.dtype)
                elif isinstance(arg_value, npw.number):
                    arg_value = npw.asarray([arg_value], dtype=arg_types)
                    arg_isol = dict(count=1, dtype=arg_value.dtype)
                else:
                    msg = "Unknown isolation parameters for argument {} of type {}."
                    msg = msg.format(arg_name, arg_types)
                    raise RuntimeError(msg)
                try:
                    type_str = tuple(
                        x for x in type(arg_value).__mro__ if x in to_tuple(arg_types)
                    )[0].__name__
                except:
                    type_str = type(arg_types).__name__
                msg += f"\n# argument {arg_name} with type {type_str}\n"
                msg += self.format_oclgrind_isolation_argument(
                    arg_name, arg_isol, arg_value
                )
                msg += "\n"
            if self.autotuner_config.verbose:
                print(f"  >Saving oclgrind kernel isolation file to '{dump_file}'.")
            f.write(msg)
        return dump_file

    def format_oclgrind_isolation_argument(self, arg_name, arg_isol, arg_value):
        from pyopencl.cltypes import vec_types, vec_type_to_scalar_and_count
        from hysop.backend.device.opencl.opencl_types import (
            cl_vec_types,
            cl_vec_type_to_scalar_and_count,
        )

        check_instance(arg_isol, dict)
        assert "count" in arg_isol
        assert "dtype" in arg_isol
        dtype = get_dtype(arg_isol["dtype"])
        if dtype == npw.void:
            dtype = arg_value.dtype
        if dtype in vec_types:
            dtype, vect = vec_type_to_scalar_and_count(dtype)
            dtype = npw.dtype(get_dtype(dtype))
            if vect == 3:
                vect = 4
        elif dtype in cl_vec_types:
            dtype, vect = cl_vec_type_to_scalar_and_count(dtype)
            dtype = npw.dtype(get_dtype(dtype))
            if vect == 3:
                vect = 4
        elif dtype is npw.complex64:
            dtype, vect = npw.float32, 2
        elif dtype is npw.complex128:
            dtype, vect = npw.float64, 2
        else:
            dtype, vect = dtype, 1
        dtype = npw.dtype(get_dtype(dtype))
        itemsize = dtype.itemsize
        dtype = dtype.type
        count = arg_isol["count"]
        assert count >= 1
        assert vect >= 1
        count *= vect
        size = count * itemsize

        typemap = {
            npw.int8: "char",
            npw.int16: "short",
            npw.int32: "int",
            npw.int64: "long",
            npw.uint8: "uchar",
            npw.uint16: "ushort",
            npw.uint32: "uint",
            npw.uint64: "ulong",
            npw.float32: "float",
            npw.float64: "double",
        }
        arg = f"<size={size}"

        dump_data = False
        dump_hex_data = False
        if "noinit" in arg_isol:
            assert arg_isol["noinit"] is True
            arg += " noinit"
        elif "fill" in arg_isol:
            assert dtype in typemap.keys()
            arg += " fill={}".format(arg_isol["fill"])
            arg += f" {typemap[dtype]}"
        elif "range" in arg_isol:
            slices = arg_isol["range"]
            assert isinstance(slices, slice)
            assert dtype in typemap.keys(), dtype
            ranges = list(slices.indices(count))
            assert (ranges[1] - ranges[0]) // ranges[2] in (count, count // vect)
            if (ranges[1] - ranges[0]) // ranges[2] == count // vect:
                ranges[0] *= vect
                ranges[1] *= vect
            assert (ranges[1] - ranges[0]) // ranges[
                2
            ] == count, f"{(ranges[1]-ranges[0])//ranges[2]} != {count}"
            arg += f" range={ranges[0]}:{ranges[2]}:{ranges[1]-1}"
            arg += f" {typemap[dtype]}"
        else:
            if "arg_value" in arg_isol:
                arg_value = arg_isol["arg_value"]
            assert isinstance(arg_value, npw.ndarray), type(arg_value)
            if dtype in typemap:
                arg += f" {typemap[dtype]}"
                dump_data = True
            else:
                arg += " uint8 hex"
                dump_hex_data = True
        if ("dump" in arg_isol) and (arg_isol["dump"] is True):
            arg += " dump"
        arg += ">"

        if dump_data:
            arg += "\n" + " ".join(
                str(x).replace(",", "").replace("(", "").replace(")", "")
                for x in arg_value.flatten()
            )
        if dump_hex_data:
            view = arg_value.ravel().view(dtype=npw.uint8)
            arg += "\n" + " ".join(f"{ord(x):02x}" for x in arg_value.tobytes())
        return arg

    def max_device_work_dim(self):
        """Maximum dimensions that specify the global and local work-item IDs."""
        return self.cl_env.device.max_work_item_dimensions

    def max_device_work_group_size(self):
        """Return the maximum number of work items allowed by the device."""
        return self.cl_env.device.max_work_group_size

    def max_device_work_item_sizes(self):
        """
        Maximum number of work-items that can be specified in each dimension
        of the work-group.
        """
        return self.cl_env.device.max_work_item_sizes

    def check_cache(self, required_cache_size):
        """Check that required_cache_size bytes can fit in workgroup cache."""
        usable_cache_bytes_per_wg = self.usable_cache_bytes_per_wg
        if required_cache_size > usable_cache_bytes_per_wg:
            msg = "Insufficient device shared memory."
            msg += "\nKernel needs {} per workgroup but device '{}' only provides "
            msg += "{} of usable shared memory per workgroup."
            msg = msg.format(
                bytes2str(required_cache_size),
                self.cl_env.device.name,
                bytes2str(usable_cache_bytes_per_wg),
            )
            raise KernelGenerationError(msg)

    @classmethod
    def check_field(cls, field, *args, **kwds):
        """
        Extend AutotunableKernel.check_field() by checking
        that the field is defined on backend OPENCL.
        """
        super().check_field(field=field, *args, **kwds)
        if field.backend.kind != Backend.OPENCL:
            msg = "Domain backend is not Backend.OPENCL but {}."
            msg = msg.format(domain.backend.kind)
            raise RuntimeError(msg)

    @classmethod
    def check_fields(cls, *fields, **kwds):
        """
        Extend AutotunableKernel.check_fields() by checking
        that all fields are defined on backend OPENCL.
        """
        super().check_fields(*fields, **kwds)
        for field in fields:
            if field.backend.kind != Backend.OPENCL:
                msg = "Domain backend is not Backend.OPENCL but {}."
                msg = msg.format(domain.backend.kind)
                raise RuntimeError(msg)

    @classmethod
    def check_cl_env(cls, cl_env, typegen):
        """Check OpenClEnvironment."""
        if not isinstance(cl_env, OpenClEnvironment):
            raise ValueError("cl_env is not a OpenClEnvironment.")

        device = cl_env.device
        context = cl_env.context
        platform = cl_env.platform
        queue = cl_env.queue

        if not isinstance(typegen, OpenClTypeGen):
            raise ValueError("typegen is not an OpenClTypeGen.")
        if context is None:
            raise ValueError("context cannot be None.")
        if device is None:
            raise ValueError("device cannot be None.")
        if platform is None:
            raise ValueError("platform cannot be None.")
        if queue is None:
            raise ValueError("queue cannot be None.")
        if (typegen.platform != platform) or (typegen.device != device):
            raise ValueError("platform or device mismatch between typegen and cl_env.")

    @classmethod
    def to_vecn(cls, vec, extend):
        """
        Extend a npw.ndarray of size s<=16 to a compatible
        opencl vector size [1,2,3,4,8,16] with extra values.
        """
        check_instance(vec, npw.ndarray)
        msg = f"Bad vector {vec}."
        assert 0 < vec.size <= 16, msg
        vsize = upper_pow2_or_3(vec.size)
        res = npw.full(shape=(vsize,), dtype=vec.dtype, fill_value=extend)
        res[: vec.size] = vec
        return res

    def make_dt(self, dtype):
        assert dtype in (npw.float16, npw.float32, npw.float64)

        def _make_dt(value):
            return dtype(value)

        return _make_dt, dtype

    def make_parameter(self, param):
        from hysop.parameters.tensor_parameter import TensorParameter

        check_instance(param, TensorParameter)
        typegen = self.typegen
        shape = param.shape
        if len(shape) == 0:
            dtype = param.dtype

            def make_param():
                return dtype(param.value)

        elif (len(shape) == 1) and (shape[0] <= 16):
            vsize = upper_pow2_or_3(shape[0])
            ctype = param.ctype
            typen = typegen.typen(ctype, vsize)
            make_typen = typegen.make_typen(ctype)
            dtype = typen

            def make_param():
                return make_typen(param.value, n=vsize, dval=0)

        else:
            msg = "Array parameters have not been implemented yet."
            raise NotImplementedError(msg)
        return make_param, dtype

    def make_array_offset(self):
        offset_dtype = npw.uint64

        def make_offset(offset, dtype):
            """Build an offset in number of elements instead of bytes."""
            msg = "Unaligned offset {} for dtype {} (itemsize={}).".format(
                offset, dtype, dtype.itemsize
            )
            assert (offset % dtype.itemsize) == 0
            return offset_dtype(offset // dtype.itemsize)

        return make_offset, offset_dtype

    def make_array_strides(self, dim, hardcode_arrays):
        """Build array strides in number of elements instead of bytes."""
        typegen = self.typegen
        ndim = upper_pow2_or_3(dim)
        strides_dtype = typegen.uintn(ndim)

        def make_strides(bstrides, dtype):
            msg = "Invalid strides {} for dtype {} (itemsize={}).".format(
                bstrides, dtype.__class__.__name__, dtype.itemsize
            )
            assert (npw.mod(bstrides, dtype.itemsize) == 0).all(), msg
            data = typegen.make_uintn(
                vals=tuple(x // dtype.itemsize for x in bstrides[::-1]), n=ndim, dval=0
            )
            if hardcode_arrays:
                return to_list(data)[:ndim]
            else:
                return data

        return make_strides, strides_dtype

    def make_array_granularity_index(self, granularity):
        """Build array granularity index."""
        if granularity == 0:
            return (None, None)
        typegen = self.typegen
        ndim = upper_pow2_or_3(granularity)
        gidx_dtype = typegen.intn(ndim)

        def make_gidx(idx):
            data = typegen.make_intn(vals=idx, n=ndim, dval=0)
            return data

        return make_gidx, gidx_dtype

    def build_array_args(self, hardcode_arrays=False, **arrays):
        kernel_args = {}
        for name, data in arrays.items():
            check_instance(data, (OpenClArray, clArray.Array))
            base = f"{name}_base"
            kernel_args[base] = data.base_data
            if not hardcode_arrays:
                dim = data.ndim
                make_offset, _ = self.make_array_offset()
                make_strides, _ = self.make_array_strides(
                    dim=dim, hardcode_arrays=hardcode_arrays
                )
                offset = f"{name}_offset"
                strides = f"{name}_strides"
                kernel_args[offset] = make_offset(data.offset, data.dtype)
                kernel_args[strides] = make_strides(data.strides, data.dtype)
        return kernel_args
