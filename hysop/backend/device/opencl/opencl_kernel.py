# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop import vprint, dprint
from hysop.tools.decorators import debug
from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.numpywrappers import npw
from hysop.backend.device.opencl import cl
from hysop.backend.device.opencl.opencl_kernel_launcher import (
    OpenClKernelLauncher,
    OpenClParametrizedKernelLauncher,
    OpenClIterativeKernelLauncher,
    OpenClKernelParameterGenerator,
    OpenClKernelParameterYielder,
)


class OpenClKernel:
    """
    OpenCL program wrapper, used to generate OpenClKernelLauncher.
    Manage launching of one OpenCL program and manage its args_mapping as keyword args_mapping.
    """

    @debug
    def __init__(
        self,
        name,
        program,
        args_mapping,
        default_queue=None,
        default_global_work_size=None,
        default_local_work_size=None,
        default_args=None,
        **kwds,
    ):
        """
        Create a OpenClKernel.

        Parameters
        ----------
        name: str
            A name for this program (for logging purposes).
        program: cl.Program
            The precompiled program that contains the program to be launched.
            When a program is passed we can create a unique instance of a program
            that will have its args_mapping already set in KernelLaunchers.
        args_mapping: dict
            Dictionnary containing arg names as keys
            and tuple (position, type) as values.
        default_queue: cl.CommandQueue, optional
            Default queue to run the program.
        default_global_work_size: tuple of ints, optional
            Default global work size.
        default_local_work_size: tuple of ints, optional
            Default local work size.
        default_args: dict, optional
            Default program args_mapping.
        kwds: dict
            Base class args_mapping.
        """
        super().__init__(**kwds)
        check_instance(name, str)
        check_instance(program, cl.Program)
        check_instance(args_mapping, dict, keys=str, values=tuple)
        check_instance(default_queue, cl.CommandQueue, allow_none=True)
        check_instance(
            default_global_work_size, tuple, values=(int, npw.integer), allow_none=True
        )
        check_instance(
            default_local_work_size, tuple, values=(int, npw.integer), allow_none=True
        )
        check_instance(default_args, dict, keys=str, allow_none=True)

        self._name = name
        self._program = program
        self._args_mapping = args_mapping
        self._default_queue = default_queue
        self._default_args = default_args
        self._default_global_work_size = default_global_work_size
        self._default_local_work_size = default_local_work_size
        self._kid = 0

        assert len(program.all_kernels()) == 1
        self._kernel = program.all_kernels()[0]

        self.default_args = first_not_None(default_args, {})

    def _get_name(self):
        """Get the name of this program."""
        return self._name

    def _get_program(self):
        """Get the precompiled program to be launched."""
        return self._program

    def _get_args_mapping(self):
        """Dictionnary containing arg names as keys and tuple (position, type) as values."""
        return self._args_mapping

    def _get_default_queue(self):
        """Default queue to launch the program."""
        return self._default_queue

    def _get_default_global_work_size(self):
        """Default global work size to launch the program."""
        return self._default_global_work_size

    def _get_default_local_work_size(self):
        """Default default_local_work_size to launch the program."""
        return self._default_local_work_size

    def _get_default_args(self):
        """Default keyword args_mapping to launch the program."""
        return self._default_args

    def _set_default_args(self, default_args):
        """Set default arguments."""
        args_mapping = self._args_mapping
        nargs = len(args_mapping)
        positions = ()
        for argname in default_args:
            if argname not in args_mapping:
                msg = "Unkown default argument '{}', known ones are {}."
                msg = msg.format(
                    argname, ", ".join(f"'{a}'" for a in args_mapping.keys())
                )
                raise ValueError(msg)
        for argname, (argpos, argtype) in args_mapping.items():
            assert isinstance(argpos, int)
            if not isinstance(argtype, (type, npw.dtype)):
                check_instance(argtype, tuple, values=type)
            if argname in default_args:
                argval = default_args[argname]
                if not isinstance(argvalue, argtype):
                    msg = (
                        "Argument {} at position {} should be of type {} but got a {}."
                    )
                    msg = msg.format(argname, argpos, argtype, type(argval))
                    raise TypeError(msg)

            positions += (argpos,)

        msg = f"Ill-formed argument positions {positions}."
        assert set(positions) == set(range(nargs)), msg
        self._default_args = default_args

    name = property(_get_name)
    program = property(_get_program)
    args_mapping = property(_get_args_mapping)
    default_queue = property(_get_default_queue)
    default_args = property(_get_default_args, _set_default_args)
    default_global_work_size = property(_get_default_global_work_size)
    default_local_work_size = property(_get_default_local_work_size)

    def build_list_launcher(self, launcher_name=None, *args, **kwds):
        """
        Build a OpenClKernelLauncher and return it as a OpenClKernelListLauncher.
        See self.build_launcher() and OpenClKernelLauncher.as_list_launcher()
        """
        launcher_name = first_not_None(launcher_name, self.name)
        return self.build_launcher(*args, **kwds).as_list_launcher(name=launcher_name)

    def build_launcher(
        self,
        name=None,
        name_prefix=None,
        name_postfix=None,
        queue=None,
        local_work_size=None,
        global_work_size=None,
        **kwds,
    ):
        """
        Build an OpenClKernel with more default arguments bound.
        If all arguments are bound, return an OpenClKernelLauncher,
        else return an OpenClKernel.
        """
        name_prefix = first_not_None(name_prefix, "")
        name_postfix = first_not_None(name_postfix, f"__{self._kid}")
        name = first_not_None(name, f"{name_prefix}{self.name}{name_postfix}")

        queue = first_not_None(queue, self._default_queue)
        global_work_size = first_not_None(
            global_work_size, self._default_global_work_size
        )
        local_work_size = first_not_None(local_work_size, self._default_local_work_size)

        args_list, parameters_map, iterated_parameters = self._compute_args_list(**kwds)

        _kwds = dict(
            name=name,
            kernel=self._program,
            args_list=args_list,
            default_global_work_size=global_work_size,
            default_local_work_size=local_work_size,
            default_queue=queue,
        )

        if parameters_map:
            if not iterated_parameters:
                klauncher = OpenClParametrizedKernelLauncher(
                    parameters_map=parameters_map, **_kwds
                )
            else:
                klauncher = OpenClIterativeKernelLauncher(
                    parameters_map=parameters_map,
                    iterated_parameters=iterated_parameters,
                    **_kwds,
                )
        else:
            klauncher = OpenClKernelLauncher(**_kwds)

        self._kid += 1
        return klauncher

    def _compute_args_list(self, **kwds):
        """
        Compute argument list from default arguments and input keywords.
        If all arguments are not specified, also return a parameter_map
        which is args_mapping restricted to the missing arguments.
        """

        default_args = self.default_args
        args_mapping = self.args_mapping
        nargs = len(args_mapping)

        arguments = {k: w for (k, w) in default_args.items()}
        arguments.update(kwds)

        parameters_map = {
            k: v for (k, v) in args_mapping.items() if (k not in arguments)
        }
        iterated_parameters = {}

        args_list = {}
        for arg_name, arg_value in arguments.items():
            if arg_name not in args_mapping:
                msg = "Unknown argument {}, valid ones are {}."
                msg = msg.format(arg_name, ", ".join(args_mapping.keys()))
                raise ValueError(msg)
            (arg_index, arg_types) = args_mapping[arg_name]
            if isinstance(arg_value, OpenClKernelParameterGenerator):
                iterated_parameters[arg_name] = arg_value
                parameters_map[arg_name] = (arg_index, arg_types)
                continue
            elif isinstance(arg_types, npw.dtype):
                msg = None
                if not isinstance(arg_value, npw.ndarray):
                    msg = "Argument {} at position {} should be a np.ndarray, got a {}."
                    msg = msg.format(arg_name, arg_index, type(arg_value))
                elif not arg_value.dtype == arg_types:
                    msg = "Argument {} at position {} is a np.ndarray of wrong dtype, got a {}, expected a {}."
                    msg = msg.format(arg_name, arg_index, type(arg_value), arg_types)
                elif not arg_value.size == 1:
                    msg = "Argument {} at position {} is not a scalar np.ndarray, shape={}, size={}."
                    msg = msg.format(
                        arg_name, arg_index, arg_value.shape, arg_value.size
                    )
                if msg is not None:
                    raise ValueError(msg)
            elif not isinstance(arg_value, arg_types):
                msg = "Argument {} at position {} should be of type {} but got a {}."
                msg = msg.format(arg_name, arg_index, arg_types, type(arg_value))
                raise TypeError(msg)
            args_list[arg_index] = arg_value

        if parameters_map:
            args_list = tuple(args_list.items())
        else:
            args_list = tuple(args_list[i] for i in range(nargs))

        return args_list, parameters_map, iterated_parameters
