# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.io_utils import IO
from hysop.backend.device.kernel_autotuner_config import KernelAutotunerConfig
from hysop.backend.device.opencl import OPENCL_KERNEL_DUMP_FOLDER


class OpenClKernelAutotunerConfig(KernelAutotunerConfig):

    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)

    def default_dump_folder(self):
        default_path = IO.default_path()
        return f"{default_path}/{OPENCL_KERNEL_DUMP_FOLDER}"
