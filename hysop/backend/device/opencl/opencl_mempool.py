# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.htypes import check_instance
from hysop.backend.device.opencl.opencl_allocator import OpenClAllocator
from hysop.backend.device.opencl.opencl_buffer import OpenClPooledBuffer
from hysop.core.memory.mempool import MemoryPool


class OpenClMemoryPool(MemoryPool, OpenClAllocator):

    def __init__(self, allocator, **kwds):
        check_instance(allocator, OpenClAllocator)
        super().__init__(
            allocator=allocator, queue=allocator.queue, mem_flags=0, **kwds
        )

    def _wrap_buffer(self, buf, alloc_sz, size, alignment):
        return OpenClPooledBuffer(
            pool=self, buf=buf, alloc_sz=alloc_sz, size=size, alignment=alignment
        )

    def _allocate_impl(self):
        msg = "This should not have been called."
        raise RuntimeError(msg)

    def prefix(self):
        return "no prefix"
