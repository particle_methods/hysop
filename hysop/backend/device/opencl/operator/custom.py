# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.constants import DirectionLabels
from hysop.tools.decorators import debug
from hysop.core.memory.memory_request import MemoryRequest
from hysop.backend.device.opencl.opencl_operator import OpenClOperator, op_apply
from hysop.backend.device.opencl.autotunable_kernels.custom_symbolic import (
    OpenClAutotunableCustomSymbolicKernel,
)
from hysop.backend.device.opencl.opencl_kernel_launcher import OpenClKernelListLauncher
from hysop.backend.device.opencl.opencl_copy_kernel_launchers import (
    OpenClCopyBufferRectLauncher,
)
from hysop.operator.base.custom import CustomOperatorBase
from hysop.parameters.scalar_parameter import ScalarParameter

from hysop.backend.device.codegen.structs.mesh_info import (
    MeshBaseStruct,
    MeshInfoStruct,
)
from hysop.backend.device.codegen.base.variables import dtype_to_ctype
from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator

from pyopencl.elementwise import ElementwiseKernel
import pyopencl as cl


class OpenClCustomOperator(CustomOperatorBase, OpenClOperator):

    @debug
    def __new__(cls, **kwds):
        return super().__new__(cls, **kwds)

    @debug
    def __init__(self, **kwds):
        super().__init__(**kwds)

    @debug
    def setup(self, *args, **kwargs):
        super().setup(*args, **kwargs)
        dim = self.domain.dim
        cg = OpenClCodeGenerator("test_generator", self.typegen)
        mbs = MeshBaseStruct(
            self.typegen, typedef=f"{self.typegen.fbtype[0]}MeshBase{dim}D_s", vsize=dim
        )
        cg.require(
            "mis",
            MeshInfoStruct(
                self.typegen,
                typedef=f"{self.typegen.fbtype[0]}MeshInfo{dim}D_s",
                mbs_typedef=mbs.typedef,
                vsize=dim,
            ),
        )
        for f in self.discrete_fields:
            fn = f.continuous_fields()[0].name
            struct_value, struct_variable = MeshInfoStruct.create_from_mesh(
                fn + "_mesh",
                self.typegen,
                f.mesh,
                storage=OpenClCodeGenerator.default_keywords["constant"],
            )
            struct_variable.declare(cg)
            cg.append(
                f"""int3 get_{fn}i_xyz(int i) {{
              int iz = i/({fn}_mesh.local_mesh.resolution.x*{fn}_mesh.local_mesh.resolution.y);
              int iy = (i-({fn}_mesh.local_mesh.resolution.x*{fn}_mesh.local_mesh.resolution.y)*iz)/({fn}_mesh.local_mesh.resolution.x);
              return (int3)(i % {fn}_mesh.local_mesh.resolution.x,iy,iz);}}"""
            )
        kernel_args = []
        for f in self.dinvar:
            if f not in self.doutvar:
                fn = f.continuous_fields()[0].name
                kernel_args.append(f"const {self.typegen.fbtype} * {fn}")
        for p in self.dinparam:
            if isinstance(p, ScalarParameter):
                kernel_args.append(f"const {self.typegen.fbtype} {p.name}")
                self._cl_dinparam.append(p)
            else:
                kernel_args.append(f"const {self.typegen.fbtype} *{p.name}")
        for f in self.doutvar:
            fn = f.continuous_fields()[0].name
            kernel_args.append(f"{self.typegen.fbtype} * {fn}")
        for p in self.doutparam:
            kernel_args.append(f"{self.typegen.fbtype} *{p.name}")
        self.__elementwise = ElementwiseKernel(
            self.cl_env.context,
            ",".join(kernel_args),
            self.func,
            f"__{self.name}_elementwise",
            preamble=str(cg),
        )

        # Build testing:
        try:
            self.__elementwise.get_kernel(False)
        except cl.RuntimeError as e:
            print("USED KERNEL")
            print(",".join(kernel_args))
            print(str(cg) + self.func)
            raise e

    @op_apply
    def apply(self, **kwds):
        super().apply(**kwds)
        args = (
            tuple(_.sbuffer for _ in self.dinvar)
            + tuple(
                cl.array.to_device(self.cl_env.default_queue, _._value)
                for _ in self.dinparam
            )
            + tuple(_.sbuffer for _ in self.doutvar)
            + tuple(
                cl.array.to_device(self.cl_env.default_queue, _._value)
                for _ in self.doutparam
            )
            + tuple(
                cl.array.to_device(self.cl_env.default_queue, _._value)
                for _ in self.extra_args
            )
        )
        self.__elementwise(*args)
        for gh_exch in self.ghost_exchanger:
            gh_exch.exchange_ghosts()
