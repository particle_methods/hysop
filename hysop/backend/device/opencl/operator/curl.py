# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import primefac, functools
from hysop.tools.numerics import float_to_complex_dtype
from hysop.tools.numpywrappers import npw
from hysop.tools.decorators import debug
from hysop.tools.warning import HysopWarning
from hysop.operator.base.curl import SpectralCurlOperatorBase
from hysop.backend.device.opencl.opencl_symbolic import OpenClSymbolic
from hysop.core.graph.graph import op_apply
from hysop.core.memory.memory_request import MemoryRequest
from hysop.backend.device.opencl.opencl_fft import OpenClFFT
from hysop.backend.device.codegen.base.variables import dtype_to_ctype
from hysop.symbolic import local_indices_symbols
from hysop.symbolic.relational import Assignment
from hysop.symbolic.complex import ComplexMul
from hysop.symbolic.misc import Select


class OpenClSpectralCurl(SpectralCurlOperatorBase, OpenClSymbolic):
    """
    Compute the curl by using an OpenCL FFT backend.
    """

    @debug
    def __new__(cls, **kwds):
        return super().__new__(cls, **kwds)

    @debug
    def __init__(self, **kwds):
        super().__init__(**kwds)

        assert len(self.forward_transforms) % 2 == 0
        N = len(self.forward_transforms) // 2
        assert len(self.K) == 2 * N

        kernel_names = ()
        for i, (Ft, (tg, Ki)) in enumerate(zip(self.forward_transforms, self.K)):
            Fhs = Ft.output_symbolic_array(f"F{i}_hat")

            kname = f"filter_curl_{Fhs.dim}d_{i}"
            kernel_names += (kname,)

            is_complex = Ki.is_complex
            Ki = tg._indexed_wave_numbers[Ki]

            if is_complex:
                expr = ComplexMul(Ki, Fhs)
            else:
                expr = Ki * Fhs

            if i < N:
                expr = Assignment(Fhs, +expr)
            else:
                expr = Assignment(Fhs, -expr)

            self.require_symbolic_kernel(kname, expr)

        self._kernel_names = kernel_names

    @debug
    def setup(self, work):
        super().setup(work)
        curl_filters = ()
        for kname in self._kernel_names:
            kernel, _ = self.symbolic_kernels[kname]
            kernel = functools.partial(kernel, queue=self.cl_env.default_queue)
            curl_filters += (kernel,)
        self.curl_filters = curl_filters
        self.exchange_ghosts = self.dFout.exchange_ghosts(build_launcher=True)
        assert (
            len(self.forward_transforms)
            == len(self.backward_transforms)
            == len(curl_filters)
        )

    @op_apply
    def apply(self, **kwds):
        """Solve the Curl equation."""
        super().apply(**kwds)
        for Ft, filter_curl, Bt in zip(
            self.forward_transforms, self.curl_filters, self.backward_transforms
        ):
            evt = Ft()
            evt = filter_curl()
            evt = Bt()
        if self.exchange_ghosts is not None:
            evt = self.exchange_ghosts()
