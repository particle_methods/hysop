# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.decorators import debug
from hysop.backend.device.opencl.opencl_operator import OpenClOperator, op_apply
from hysop.operator.base.integrate import IntegrateBase
import pyopencl


class OpenClIntegrate(IntegrateBase, OpenClOperator):

    @debug
    def get_field_requirements(self):
        # force 0 ghosts for the reduction (pyopencl reduction kernel)
        requirements = super().get_field_requirements()
        topo, req = requirements.get_input_requirement(self.field)
        req.max_ghosts = (0,) * self.field.dim
        return requirements

    @debug
    def setup(self, work):
        super().setup(work)
        if self.expr is None:
            self.sum_kernels = tuple(
                self.dF.backend.sum(
                    a=self.dF.data[i], build_kernel_launcher=True, synchronize=False
                )
                for i in range(self.dF.nb_components)
            )
        else:
            from hysop.backend.device.codegen.base.variables import dtype_to_ctype

            self.sum_kernels = tuple(
                pyopencl.reduction.ReductionKernel(
                    self.cl_env.context,
                    self.dF.dtype,
                    neutral="0",
                    reduce_expr="a+b",
                    map_expr=self.expr,
                    arguments=f"__global {dtype_to_ctype(self.dF.dtype)} *x",
                )
                for i in range(self.dF.nb_components)
            )

    @op_apply
    def apply(self, **kwds):
        value = self.parameter._value.copy()
        queue = self.cl_env.default_queue
        evts = ()
        values = ()
        if self.expr is None:
            for knl in self.sum_kernels:
                evt = knl(queue=queue)
                evts += (evt,)
            for i, evt in enumerate(evts):
                evt.wait()
                values += (self.sum_kernels[i].out.get()[0],)
        else:
            outs = ()
            for i, knl in enumerate(self.sum_kernels):
                out, evt = knl(self.dF.buffers[i], queue=queue, return_event=True)
                evts += (evt,)
                outs += (out,)
            for i, evt in enumerate(evts):
                evt.wait()
                values += (outs[i].get(),)

        for i, Pi in enumerate(values):
            if self.scaling_coeff[i] is None:
                self.scaling_coeff[i] = 1.0 / Pi
            value[i] = self.scaling_coeff[i] * Pi
        # compute value from all processes
        self.parameter.value = self._collect(value)
