# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sympy as sm

from hysop.constants import DirectionLabels
from hysop.backend.device.opencl.opencl_array_backend import OpenClArrayBackend
from hysop.tools.decorators import debug
from hysop.tools.numpywrappers import npw
from hysop.core.memory.memory_request import MemoryRequest
from hysop.backend.device.opencl.opencl_operator import OpenClOperator, op_apply
from hysop.backend.device.opencl.opencl_symbolic import OpenClSymbolic
from hysop.backend.device.opencl.autotunable_kernels.custom_symbolic import (
    OpenClAutotunableCustomSymbolicKernel,
)
from hysop.backend.device.opencl.opencl_kernel_launcher import OpenClKernelListLauncher
from hysop.backend.device.opencl.opencl_copy_kernel_launchers import (
    OpenClCopyBufferRectLauncher,
)

from hysop.operator.base.enstrophy import EnstrophyBase
from hysop.operator.base.custom_symbolic_operator import SymbolicExpressionParser
from hysop.symbolic.relational import Assignment


class OpenClEnstrophy(EnstrophyBase, OpenClSymbolic):

    @debug
    def __new__(cls, **kwds):
        return super().__new__(cls, **kwds)

    @debug
    def __init__(self, **kwds):
        super().__init__(**kwds)

        rho = self.rho
        rhos = rho.s if (rho is not None) else sm.Integer(1)

        Ws = self.vorticity.s()
        WdotW = self.WdotW.s()

        expr = Assignment(WdotW, rhos * npw.dot(Ws, Ws))
        self.require_symbolic_kernel("WdotW", expr)

    @debug
    def get_field_requirements(self):
        # force 0 ghosts for the reduction (pyopencl reduction kernel)
        requirements = super().get_field_requirements()
        for is_input, (field, td, req) in requirements.iter_requirements():
            if field is self.WdotW:
                req.max_ghosts = (0,) * self.WdotW.dim
        return requirements

    @debug
    def discretize(self):
        super().discretize()
        assert (self.dWdotW.ghosts == 0).all()

    @debug
    def setup(self, work):
        super().setup(work)
        (self.WdotW_kernel, self.WdotW_update_parameters) = self.symbolic_kernels[
            "WdotW"
        ]
        self.sum_kernel = self.dWdotW.backend.sum(
            a=self.dWdotW.sdata, build_kernel_launcher=True, synchronize=False
        )

    @op_apply
    def apply(self, **kwds):
        queue = self.cl_env.default_queue
        evt = self.WdotW_kernel(queue=queue, **self.WdotW_update_parameters())
        evt = self.sum_kernel(queue=queue)
        evt.wait()
        local_enstrophy = self.coeff * self.sum_kernel.out.get()[0]

        # collect enstrophy from all processes
        self.enstrophy.value = self._collect(local_enstrophy)
