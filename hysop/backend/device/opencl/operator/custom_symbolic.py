# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.constants import DirectionLabels
from hysop.tools.decorators import debug
from hysop.operator.base.custom_symbolic_operator import CustomSymbolicOperatorBase
from hysop.core.memory.memory_request import MemoryRequest
from hysop.backend.device.opencl.opencl_operator import OpenClOperator, op_apply
from hysop.backend.device.opencl.autotunable_kernels.custom_symbolic import (
    OpenClAutotunableCustomSymbolicKernel,
)
from hysop.backend.device.opencl.opencl_kernel_launcher import OpenClKernelListLauncher
from hysop.backend.device.opencl.opencl_copy_kernel_launchers import (
    OpenClCopyBufferRectLauncher,
)


class OpenClCustomSymbolicOperator(CustomSymbolicOperatorBase, OpenClOperator):

    @debug
    def __new__(cls, **kwds):
        return super().__new__(cls, **kwds)

    @debug
    def __init__(self, **kwds):
        super().__init__(**kwds)

    @debug
    def setup(self, work):
        super().setup(work)
        self._collect_kernels()

    def _collect_kernels(self):
        kl = OpenClKernelListLauncher(name=self.name, profiler=self._profiler)
        kl += self._collect_symbolic_kernel()
        for sout in self.output_discrete_fields.values():
            kl += sout.exchange_ghosts(build_launcher=True)
        self.kl = kl

    def _collect_symbolic_kernel(self):
        cl_env = self.cl_env
        typegen = self.typegen

        autotuner_config = self.autotuner_config
        build_opts = self.build_options()

        kernel_autotuner = OpenClAutotunableCustomSymbolicKernel(
            cl_env=cl_env,
            typegen=typegen,
            build_opts=build_opts,
            autotuner_config=autotuner_config,
        )

        kernel, args_dict, update_input_parameters = kernel_autotuner.autotune(
            expr_info=self.expr_info
        )

        kl = kernel.build_launcher(**args_dict)

        self._symbolic_kernel_kl = kl
        self._update_input_params = update_input_parameters
        return kl

    @op_apply
    def apply(self, **kwds):
        queue = self.cl_env.default_queue
        evt = self.kl(queue=queue, **self._update_input_params())

    @classmethod
    def supports_mpi(cls):
        return True
