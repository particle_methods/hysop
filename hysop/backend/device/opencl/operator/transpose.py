# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.decorators import debug
from hysop.operator.base.transpose_operator import TransposeOperatorBase
from hysop.backend.device.opencl.opencl_operator import OpenClOperator, op_apply
from hysop.backend.device.opencl.autotunable_kernels.transpose import (
    OpenClAutotunableTransposeKernel,
)
from hysop.backend.device.opencl.opencl_kernel_launcher import OpenClKernelListLauncher


class OpenClTranspose(TransposeOperatorBase, OpenClOperator):

    @debug
    def setup(self, work):
        super().setup(work)
        self._collect_kernels()

    def _collect_kernels(self):
        self._collect_transpose_kernel()

    def _collect_transpose_kernel(self):
        cl_env = self.cl_env
        typegen = self.typegen
        axes = self.axes

        autotuner_config = self.autotuner_config
        build_opts = self.build_options()

        input_field = self.din
        output_field = self.dout
        is_inplace = self.is_inplace

        kernel = OpenClAutotunableTransposeKernel(
            cl_env, typegen, build_opts, autotuner_config
        )

        if is_inplace:
            # Only 2D square matrix inplace transposition is supported
            compute_inplace = input_field.dim == 2
            compute_inplace &= all(input_field.resolution[0] == input_field.resolution)
        else:
            compute_inplace = False

        hardcode_arrays = compute_inplace or not is_inplace
        transpose, _ = kernel.autotune(
            axes=axes,
            hardcode_arrays=hardcode_arrays,
            is_inplace=compute_inplace,
            input_buffer=input_field.sbuffer,
            output_buffer=output_field.sbuffer,
        )

        launcher = OpenClKernelListLauncher(
            name=transpose.name, profiler=self._profiler
        )
        for i in range(self.nb_components):
            if compute_inplace:
                assert hardcode_arrays
                launcher += transpose.build_launcher(
                    inout_base=input_field.data[i].base_data
                )
            elif is_inplace:
                assert not hardcode_arrays
                kernel_kargs = kernel.build_array_args(
                    **{"in": input_field.data[i], "out": self.dtmp}
                )
                launcher += transpose.build_launcher(**kernel_kargs)
                launcher.push_copy_device_to_device(
                    varname="tmp", src=self.dtmp, dst=input_field.data[i]
                )
            else:
                assert hardcode_arrays
                launcher += transpose.build_launcher(
                    in_base=input_field.data[i].base_data,
                    out_base=output_field.data[i].base_data,
                )
        self._kernel_launcher = launcher

    def enqueue_copy_kernel(self, _dst, _src, queue):
        pass

    @op_apply
    def apply(self, **kwds):
        queue = self.cl_env.default_queue

        kernel_launcher = self._kernel_launcher
        evt = kernel_launcher(queue=queue)

    @classmethod
    def supports_mpi(cls):
        return True
