# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.decorators import debug
from hysop.core.graph.graph import op_apply
from hysop.operator.base.min_max import (
    MinMaxFieldStatisticsBase,
    MinMaxDerivativeStatisticsBase,
)
from hysop.backend.device.opencl.opencl_operator import OpenClOperator
from hysop.backend.device.opencl.operator.derivative import (
    OpenClSpectralSpaceDerivative,
    OpenClFiniteDifferencesSpaceDerivative,
)


class OpenClMinMaxFieldStatistics(MinMaxFieldStatisticsBase, OpenClOperator):
    """OpenCl implementation backend of operator MinMaxFieldStatistics."""

    @op_apply
    def apply(self, **kwds):
        """See MinMaxFieldStatisticsBase.compute_statistics()."""
        super().apply(**kwds)
        self.compute_statistics(**kwds)


class OpenClMinMaxSpectralDerivativeStatistics(
    MinMaxDerivativeStatisticsBase, OpenClSpectralSpaceDerivative
):
    """OpenCl implementation backend of operator MinMaxSpectralDerivativeStatistics."""

    @op_apply
    def apply(self, **kwds):
        """Compute derivative and than statistics."""
        super().apply(**kwds)
        self.compute_statistics(**kwds)


class OpenClMinMaxFiniteDifferencesDerivativeStatistics(
    MinMaxDerivativeStatisticsBase, OpenClFiniteDifferencesSpaceDerivative
):
    """OpenCl implementation backend of operator MinMaxFiniteDifferencesDerivativeStatistics."""

    @op_apply
    def apply(self, **kwds):
        """Compute derivative and than statistics."""
        super().apply(**kwds)
        self.compute_statistics(**kwds)
