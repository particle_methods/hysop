# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sympy as sm

from hysop.tools.htypes import check_instance, first_not_None, to_tuple
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import ScalarField, Field
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.backend.device.opencl.operator.custom_symbolic import (
    OpenClCustomSymbolicOperator,
)
from hysop.symbolic.relational import Assignment


class OpenClAnalyticField(OpenClCustomSymbolicOperator):
    """
    Applies an analytic formula, given by user, on its field.
    Formula is given as one or more sympy expressions.
    """

    @debug
    def __new__(cls, field, formula, variables, **kwds):
        name = kwds.pop("name", f"analytic_{field.name}")
        return super().__new__(cls, name=None, exprs=None, variables=variables, **kwds)

    @debug
    def __init__(self, field, formula, variables, **kwds):
        """
        Initialize a Analytic operator on the python backend.

        Apply a user-defined formula onto a field, possibly
        dependent on space variables and external fields/parameters.

        Parameters
        ----------
        field: hysop.field.continuous_field.ScalarField
            Continuous field to be modified.
        formula : sm.Basic or array-like of sm.Basic
            field.nb_components symbolic expressions as a tuple.
        variables: dict
            Dictionary of fields as keys and topology descriptors as values.
        kwds: dict, optional
            Base class arguments.
        """
        formula = to_tuple(formula)
        check_instance(field, ScalarField)
        check_instance(
            formula, tuple, values=(type(None), sm.Basic), size=field.nb_components
        )
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)

        exprs = ()
        Fs = to_tuple(field.s())
        for lhs, rhs in zip(Fs, formula):
            if rhs is None:
                continue
            exprs += (Assignment(lhs, rhs),)

        if not exprs:
            msg = "All formulas are None. Give at least one expression."
            raise ValueError(msg)

        name = kwds.pop("name", f"analytic_{field.name}")

        super().__init__(name=name, exprs=exprs, variables=variables, **kwds)

    @classmethod
    def supports_mpi(cls):
        return True
