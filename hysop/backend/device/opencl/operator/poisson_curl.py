# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import primefac, functools
from hysop import vprint
from hysop.tools.numpywrappers import npw
from hysop.tools.decorators import debug
from hysop.tools.warning import HysopWarning
from hysop.tools.units import bytes2str
from hysop.tools.numerics import is_complex, find_common_dtype
from hysop.operator.base.poisson_curl import SpectralPoissonCurlOperatorBase
from hysop.backend.device.opencl.opencl_symbolic import OpenClSymbolic
from hysop.core.graph.graph import op_apply
from hysop.core.memory.memory_request import MemoryRequest
from hysop.backend.device.opencl.opencl_fft import OpenClFFT
from hysop.backend.device.codegen.base.variables import dtype_to_ctype
from hysop.constants import FieldProjection
from hysop.symbolic import local_indices_symbols
from hysop.symbolic.misc import Select, Expand
from hysop.symbolic.complex import ComplexMul
from hysop.symbolic.relational import Assignment, LogicalEQ, LogicalAND


class OpenClPoissonCurl(SpectralPoissonCurlOperatorBase, OpenClSymbolic):
    """
    Solves the poisson-rotational equation using clFFT.
    """

    def __new__(cls, **kwds):
        return super().__new__(cls, **kwds)

    def __init__(self, **kwds):
        super().__init__(**kwds)
        dim = self.dim
        wcomp = self.W.nb_components
        assert dim in (2, 3), dim

        # request the poisson rotational kernel

        W_Ft = self.W_forward_transforms
        U_Bt = self.U_backward_transforms
        W_Bt = self.W_backward_transforms
        kd1s = self.kd1s
        kd2s = self.kd2s

        Win = tuple(
            Ft.output_symbolic_array(f"{Ft.field.var_name}in_hat") for Ft in W_Ft
        )
        Wout = tuple(
            (
                Bt.input_symbolic_array(f"{Bt.field.var_name}out_hat")
                if (Bt is not None)
                else None
            )
            for Bt in W_Bt
        )
        Uout = tuple(
            Bt.input_symbolic_array(f"{Bt.field.var_name}out_hat") for Bt in U_Bt
        )
        K = tuple(
            tuple(self.tg._indexed_wave_numbers[kd] for kd in kd1[::-1]) for kd1 in kd1s
        )
        KK = tuple(
            tuple(self.tg._indexed_wave_numbers[kd] for kd in kd2[::-1]) for kd2 in kd2s
        )

        def mul(Ki, other):
            if Ki.Wn.is_complex:
                return ComplexMul(Ki, other)
            else:
                return Ki * other

        if self.should_diffuse:
            nu, dt = self.nu.s, self.dt.s
            for i, (win, wout, KKi) in enumerate(zip(Win, Wout, KK)):
                F = sum(KKi)
                expr = Assignment(wout, win / (1 - nu * dt * F))
                self.require_symbolic_kernel(f"diffusion_kernel__{i}", expr)
            Win = Wout

        indices = local_indices_symbols[:dim]
        cond = LogicalAND(*tuple(LogicalEQ(idx, 0) for idx in indices))

        if self.should_project:
            exprs = ()
            dtype = find_common_dtype(
                *tuple(Ft.output_dtype for Ft in self.W_forward_transforms)
            )
            Cs = self.symbolic_tmp_scalars("C", dtype=dtype, count=3)
            for i in range(3):
                expr = 0
                for j in range(3):
                    e = Win[j]
                    if i == j:
                        e = KK[j][j] * e
                    else:
                        e = (
                            ComplexMul(K[j][j], e)
                            if K[j][j].Wn.is_complex
                            else K[j][j] * e
                        )
                        e = (
                            ComplexMul(K[j][i], e)
                            if K[j][i].Wn.is_complex
                            else K[j][i] * e
                        )
                    expr += e
                expr /= sum(KK[i])
                expr = Select(expr, 0, cond)
                expr = Assignment(Cs[i], expr)
                exprs += (expr,)
            for i in range(3):
                expr = Assignment(Wout[i], Win[i] - Cs[i])
                exprs += (expr,)
            self.require_symbolic_kernel("projection_kernel", *exprs)
            Win = Wout

        exprs = ()
        for i in range(wcomp):
            F = sum(KK[i])
            expr = Assignment(Win[i], Select(Win[i] / F, 0, cond))
            self.require_symbolic_kernel(f"poisson_kernel__{i}", expr)

        if dim == 2:
            assert wcomp == 1
            e0 = Assignment(Uout[0], -mul(K[0][1], Win[0]))
            e1 = Assignment(Uout[1], +mul(K[0][0], Win[0]))
        elif dim == 3:
            assert wcomp == 3
            e0 = Assignment(Uout[0], mul(K[1][2], Win[1]) - mul(K[2][1], Win[2]))
            e1 = Assignment(Uout[1], mul(K[2][0], Win[2]) - mul(K[0][2], Win[0]))
            e2 = Assignment(Uout[2], mul(K[0][1], Win[0]) - mul(K[1][0], Win[1]))
        else:
            msg = f"dim={dim}"
            raise NotImplementedError(msg)
        self.require_symbolic_kernel("curl_kernel__0", e0)
        self.require_symbolic_kernel("curl_kernel__1", e1)
        if dim == 3:
            self.require_symbolic_kernel("curl_kernel__2", e2)

    @debug
    def setup(self, work):
        super().setup(work)
        self._build_diffusion_kernel()
        self._build_projection_kernel()
        self._build_poisson_curl_kernel()
        self._build_ghost_exchangers()

    def _build_diffusion_kernel(self):
        if self.should_diffuse:
            diffusion_filters = ()
            for i in range(self.W.nb_components):
                knl, knl_kwds = self.symbolic_kernels[f"diffusion_kernel__{i}"]
                knl = functools.partial(knl, queue=self.cl_env.default_queue)

                def F(knl=knl, knl_kwds=knl_kwds):
                    return knl(**knl_kwds())

                diffusion_filters += (F,)
            self.diffusion_filters = diffusion_filters
        else:
            self.diffusion_filters = None

    def _build_poisson_curl_kernel(self):
        poisson_filters = ()
        for i in range(self.W.nb_components):
            knl, __ = self.symbolic_kernels[f"poisson_kernel__{i}"]
            Fi = functools.partial(knl, queue=self.cl_env.default_queue)
            poisson_filters += (Fi,)

        curl_filters = ()
        for i in range(self.U.nb_components):
            knl, __ = self.symbolic_kernels[f"curl_kernel__{i}"]
            Fi = functools.partial(knl, queue=self.cl_env.default_queue)
            curl_filters += (Fi,)

        self.poisson_filters = poisson_filters
        self.curl_filters = curl_filters

    def _build_projection_kernel(self):
        if self.should_project:
            knl, _ = self.symbolic_kernels["projection_kernel"]
            self.filter_projection = functools.partial(
                knl, queue=self.cl_env.default_queue
            )
        else:
            self.filter_projection = None

    def _build_ghost_exchangers(self):
        def noop():
            pass

        exchange_U_ghosts = self.dU.exchange_ghosts(build_launcher=True)
        if exchange_U_ghosts is not None:
            self.exchange_U_ghosts = functools.partial(
                exchange_U_ghosts, queue=self.cl_env.default_queue
            )
        else:
            self.exchange_U_ghosts = noop

        if self.should_project or self.should_diffuse:
            exchange_W_ghosts = self.dW.exchange_ghosts(build_launcher=True)
            if exchange_W_ghosts is not None:
                self.exchange_W_ghosts = functools.partial(
                    exchange_W_ghosts, queue=self.cl_env.default_queue
                )
            else:
                self.exchange_W_ghosts = noop

    @op_apply
    def apply(self, simulation, **kwds):
        """Solve the PoissonCurl equation."""

        diffuse = self.should_diffuse
        project = self.do_project(simu=simulation)

        for Ft in self.W_forward_transforms:
            evt = Ft(simulation=simulation)
        if diffuse:
            for Fd in self.diffusion_filters:
                evt = Fd()
        if project:
            evt = self.filter_projection()
        if diffuse or project:
            for Bt in self.W_backward_transforms:
                evt = Bt(simulation=simulation)
            evt = self.exchange_W_ghosts()
        for Fp in self.poisson_filters:
            evt = Fp()
        for Fc, Bt in zip(self.curl_filters, self.U_backward_transforms):
            evt = Fc()
            evt = Bt(simulation=simulation)
        evt = self.exchange_U_ghosts()
        self.update_energy(simulation=simulation)
