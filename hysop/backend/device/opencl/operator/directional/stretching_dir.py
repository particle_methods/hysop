# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np

from hysop import Field, TopologyDescriptor
from hysop.tools.decorators import debug
from hysop.tools.htypes import check_instance
from hysop.core.graph.graph import (
    not_initialized,
    initialized,
    discretized,
    ready,
    op_apply,
)
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.constants import StretchingFormulation, BoundaryCondition

from hysop.backend.device.kernel_config import KernelConfig
from hysop.backend.device.opencl.operator.directional.opencl_directional_operator import (
    OpenClDirectionalOperator,
)

from hysop.tools.htypes import InstanceOf
from hysop.methods import Interpolation, TimeIntegrator, Remesh, SpaceDiscretization

from hysop.constants import StretchingFormulation
from hysop.numerics.odesolvers.runge_kutta import (
    ExplicitRungeKutta,
    Euler,
    RK2,
    RK3,
    RK4,
)

from hysop.backend.device.codegen.kernels.directional_stretching import (
    DirectionalStretchingKernel,
)


class OpenClDirectionalStretching(OpenClDirectionalOperator):

    __default_method = {
        # KernelConfig:          KernelConfig(),
        TimeIntegrator: Euler,
        StretchingFormulation: StretchingFormulation.GRAD_UW,
        SpaceDiscretization: SpaceDiscretization.FDC4,
    }

    __available_methods = {
        # KernelConfig:          InstanceOf(KernelConfig),
        TimeIntegrator: InstanceOf(ExplicitRungeKutta),
        StretchingFormulation: InstanceOf(StretchingFormulation),
        SpaceDiscretization: InstanceOf(SpaceDiscretization),
    }

    @debug
    def __new__(cls, velocity, vorticity, vorticity_out, variables, **kwds):
        return super().__new__(cls, input_fields=None, output_fields=None, **kwds)

    @debug
    def __init__(self, velocity, vorticity, vorticity_out, variables, **kwds):
        """
        Directionnal stretching of vorticity in a given direction
        on opencl backend.

        OpenCL kernels are build once per dimension in order to handle
        directional splitting with resolution non uniform in directions.

        Parameters
        ----------
        velocity: Field
            Continuous velocity field (all components)
        vorticity: Field
            Input vorticity field.
        vorticity_out: Field, optional
            Output vorticity field.
        variables: dict
            Dictionary of continuous fields as keys and topologies as values.
        kwds:
            Extra parameters passed to generated directional operators.

        Attributes
        ----------
        velocity: Field
            Continuous velocity field (all components)
        vorticity_in: list
            Input vorticity field.
        vorticity_out: list
            Output vorticity field.
        """

        check_instance(velocity, Field)
        check_instance(vorticity, Field)
        check_instance(vorticity_out, Field)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)

        input_fields = {velocity: variables[velocity], vorticity: variables[vorticity]}
        output_fields = {vorticity_out: variables[vorticity_out]}

        super().__init__(input_fields=input_fields, output_fields=output_fields, **kwds)

        self.velocity = velocity
        self.vorticity_in = vorticity
        self.vorticity_out = vorticity_out
        self.is_inplace = vorticity is vorticity_out

    @debug
    def handle_method(self, method):
        super().handle_method(method)

        self.space_discretization = method.pop(SpaceDiscretization)
        self.formulation = method.pop(StretchingFormulation)
        self.time_integrator = method.pop(TimeIntegrator)

        assert str(self.space_discretization)[:3] == "FDC"
        self.order = int(str(self.space_discretization)[3:])

    @debug
    def get_field_requirements(self):
        requirements = super().get_field_requirements()

        direction = self.splitting_direction
        is_inplace = self.is_inplace

        time_integrator = self.time_integrator
        formulation = self.formulation
        order = self.order

        velocity = self.velocity
        vorticity_in = self.vorticity_in
        vorticity_out = self.vorticity_out

        v_topo, v_requirements = requirements.get_input_requirement(velocity)
        win_topo, win_requirements = requirements.get_input_requirement(vorticity_in)
        wout_topo, wout_requirements = requirements.get_output_requirement(
            vorticity_out
        )

        if v_topo.mpi_params.size == 1:
            lboundary = v_topo.domain.lboundaries[-1]
            rboundary = v_topo.domain.rboundaries[-1]
        else:
            lboundary = BoundaryCondition.NONE
            rboundary = BoundaryCondition.NONE
        boundaries = (lboundary, rboundary)

        v_ghosts, w_ghosts = DirectionalStretchingKernel.min_ghosts(
            boundaries, formulation, order, time_integrator, direction
        )

        v_requirements.min_ghosts = v_ghosts
        win_requirements.min_ghosts = w_ghosts

        if is_inplace:
            wout_requirements.min_ghosts = w_ghosts

        return requirements

    @debug
    def discretize(self):
        super().discretize()
        dvelocity = self.input_discrete_fields[self.velocity]
        dvorticity_in = self.input_discrete_fields[self.vorticity_in]
        dvorticity_out = self.output_discrete_fields[self.vorticity_out]
        assert dvorticity_in.topology.topology is dvorticity_out.topology.topology

        vorticity_mesh_info_in = self.input_mesh_info[self.vorticity_in]
        vorticity_mesh_info_out = self.output_mesh_info[self.vorticity_out]

        self.dvelocity = dvelocity
        self.dvorticity_in = dvorticity_in
        self.dvorticity_out = dvorticity_out

        self.velocity_mesh_info = self.input_mesh_info[self.velocity]
        self.vorticity_mesh_info = vorticity_mesh_info_in

    @debug
    def setup(self, work):
        super().setup(work)
        self._collect_kernels()

    def _collect_kernels(self):
        self._collect_stretching_kernel()

    def _collect_stretching_kernel(self):

        velocity = self.dvelocity
        vorticity_in = self.dvorticity_in
        vorticity_out = self.dvorticity_out

        velocity_mesh_info = self.velocity_mesh_info
        vorticity_mesh_info = self.vorticity_mesh_info

        direction = self.splitting_direction
        fomulation = self.formulation
        discretization = self.space_discretization
        time_integrator = self.time_integrator

        cl_env = self.cl_env
        typegen = self.typegen
        build_options = self.build_options()
        autotuner_config = self.autotuner_config

        (
            kernel_launcher,
            kernel_args,
            kernel_args_mapping,
            total_work,
            per_work_statistic,
            cached_bytes,
        ) = DirectionalStretchingKernel.autotune(
            cl_env=cl_env,
            typegen=typegen,
            build_options=build_options,
            autotuner_config=autotuner_config,
            direction=direction,
            time_integrator=time_integrator,
            formulation=formulation,
            discretization=discretization,
            velocity=velocity,
            vorticity_in=vorticity_in,
            vorticity_out=vorticity_out,
            velocity_mesh_info=velocity_mesh_info,
            vorticity_mesh_info=vorticity_mesh_info,
        )
        kernel_launcher._profiler = self._profiler

    @op_apply
    def apply(self, **kargs):
        super().apply(**kargs)
        raise NotImplementedError()

    # Backend methods
    # ComputationalNode
    @classmethod
    def default_method(cls):
        return cls.__default_method

    @classmethod
    def available_methods(cls):
        return cls.__available_methods

    @classmethod
    def supports_multiple_topologies(cls):
        return False

    @classmethod
    def supports_mpi(cls):
        return False

    # DirectionalOperatorBase
    @classmethod
    def supported_dimensions(cls):
        return [3]

    # ComputationalGraphNode
    @classmethod
    def supports_multiscale(cls):
        return False

    def _do_compute(self, simulation, dt_coeff, **kargs):
        dt = simulation.time_step * dt_coeff
        self._do_compute_impl(dt=dt, **kargs)

    def _do_compute_monoscale(self, dt):
        raise NotImplementedError()

    def _do_compute_multiscale(self, dt):
        raise NotImplementedError()

    def _do_compute_monoscale_comm(self, dt):
        raise NotImplementedError()

    def _do_compute_multiscale_comm(self, dt):
        raise NotImplementedError()
