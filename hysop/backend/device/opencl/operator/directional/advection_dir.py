# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.decorators import debug
from hysop.constants import DirectionLabels
from hysop.backend.device.opencl.operator.directional.opencl_directional_operator import (
    OpenClDirectionalOperator,
    op_apply,
)

from hysop.operator.base.advection_dir import DirectionalAdvectionBase, MemoryRequest
from hysop.backend.device.opencl.autotunable_kernels.advection_dir import (
    OpenClAutotunableDirectionalAdvectionKernel,
)
from hysop.backend.device.opencl.autotunable_kernels.remesh_dir import (
    OpenClAutotunableDirectionalRemeshKernel,
)
from hysop.backend.device.opencl.opencl_copy_kernel_launchers import (
    OpenClCopyBufferRectLauncher,
)
from hysop.backend.device.opencl.opencl_kernel_launcher import OpenClKernelListLauncher


class OpenClDirectionalAdvection(DirectionalAdvectionBase, OpenClDirectionalOperator):

    DEBUG = False

    @debug
    def __new__(
        cls,
        force_atomics=False,
        relax_min_particles=False,
        remesh_criteria_eps=None,
        **kwds,
    ):
        return super().__new__(cls, **kwds)

    @debug
    def __init__(
        self,
        force_atomics=False,
        relax_min_particles=False,
        remesh_criteria_eps=None,
        **kwds,
    ):
        """
        Particular advection of field(s) in a given direction,
        on opencl backend, with remeshing.

        OpenCL kernels are build once per dimension in order to handle
        directional splitting with resolution non uniform in directions.

        Parameters
        ----------
        velocity: Field
            Continuous velocity field (all components)
        advected_fields: Field or array like of Fields
            Instance or list of continuous fields to be advected.
        advected_fields_out: Field or array like of Field
            Where input fields are remeshed
        variables: dict
            Dictionary of continuous fields as keys and topologies as values.
        velocity_cfl: float
            Velocity cfl in given direction.
        force_atomics: bool
            If set, only use atomic accumulators for remesh.
            Default: autotuner will automatically figure out the best strategy
            between using atomic operations and remeshing multiple particles at
            a time.
        relax_min_particles: bool
            Relax the minimum particles required for correct accumulation of remeshed
            quantities in cache when not using atomic operations.
            If this is set to True, the minimum number will be reset to 2.
            The minimum number of particles to be remeshed at a time,
            by a work_item is 2*ceil(scalar_cfl) because of maximum potential
            particles superposition after the advection step.
        remesh_criteria_eps: int
            Minimum number of epsilons (in specified precision) that will trigger
            remeshing. By default every non zero value is remeshed.
        kwds:
            Extra parameters passed to generated directional operators.

        Attributes
        ----------
        velocity: Field
            Continuous velocity field (all components)
        advected_fields_in: list
            List of continuous fields to be advected.
        advected_fields_out: list
            List of output continuous fields.
        """

        super().__init__(**kwds)
        self.force_atomics = force_atomics
        self.relax_min_particles = relax_min_particles
        self.remesh_criteria_eps = remesh_criteria_eps

        self._force_autotuner_verbose = None
        self._force_autotuner_debug = None

    @debug
    def setup(self, work):
        super().setup(work)
        self._collect_kernels()

    def _collect_kernels(self):
        kl = OpenClKernelListLauncher(name="advec_remesh", profiler=self._profiler)
        kl += self._collect_advection_kernel()
        kl += self._collect_remesh_kernels()
        kl += self._collect_redistribute_kernels()
        self.all_kernels = kl

    def _collect_advection_kernel(self):
        cl_env = self.cl_env
        typegen = self.typegen
        build_options = self.build_options()
        autotuner_config = self.autotuner_config

        kernel = OpenClAutotunableDirectionalAdvectionKernel(
            cl_env=cl_env,
            typegen=typegen,
            build_opts=build_options,
            autotuner_config=autotuner_config,
        )

        kwds = {}
        kwds["velocity"] = self.dvelocity
        kwds["position"] = self.dposition
        kwds["relative_velocity"] = self.relative_velocity

        if self.is_bilevel is None:
            kwds["is_bilevel"] = None
        else:
            kwds["is_bilevel"] = self.is_bilevel

        kwds["direction"] = self.splitting_direction
        kwds["velocity_cfl"] = self.velocity_cfl
        kwds["time_integrator"] = self.time_integrator

        (advec_kernel, args_dict) = kernel.autotune(
            force_verbose=self._force_autotuner_verbose,
            force_debug=self._force_autotuner_debug,
            hardcode_arrays=True,
            **kwds,
        )

        args_dict.pop("dt")
        advec_launcher = advec_kernel.build_launcher(**args_dict)

        assert "dt" in advec_launcher.parameters_map.keys()
        self.advection_kernel_launcher = advec_launcher
        return advec_launcher

    def _collect_remesh_kernels(self):
        cl_env = self.cl_env
        typegen = self.typegen
        build_options = self.build_options()
        autotuner_config = self.autotuner_config

        kernel = OpenClAutotunableDirectionalRemeshKernel(
            cl_env=cl_env,
            typegen=typegen,
            build_opts=build_options,
            autotuner_config=autotuner_config,
        )

        scalars_in = tuple(
            self.dadvected_fields_in[ifield] for ifield in self.advected_fields_in
        )
        scalars_out = tuple(
            self.dadvected_fields_out[ofield] for ofield in self.advected_fields_out
        )

        kwds = {}
        kwds["direction"] = self.splitting_direction
        kwds["scalar_cfl"] = self.scalar_cfl
        kwds["is_inplace"] = self.is_inplace

        kwds["position"] = self.dposition
        kwds["is_inplace"] = self.is_inplace

        kwds["remesh_kernel"] = self.remesh_kernel
        kwds["remesh_criteria_eps"] = self.remesh_criteria_eps
        kwds["force_atomics"] = self.force_atomics
        kwds["relax_min_particles"] = self.relax_min_particles

        assert len(scalars_in) == len(scalars_out)
        kl = OpenClKernelListLauncher(name="remesh")
        for Sin, Sout in zip(scalars_in, scalars_out):
            kwds["scalars_in"] = (Sin,)
            kwds["scalars_out"] = (Sout,)
            (remesh_kernel, args_dict) = kernel.autotune(
                force_verbose=self._force_autotuner_verbose,
                force_debug=self._force_autotuner_debug,
                hardcode_arrays=True,
                **kwds,
            )
            kl += remesh_kernel.build_launcher(**args_dict)
        self.remesh_kernel_launcher = kl
        return kl

    def _collect_redistribute_kernels(self):
        remesh_ghosts = self.remesh_ghosts
        dsoutputs = self.dadvected_fields_out
        kl = OpenClKernelListLauncher(name="accumulate_and_exchange_ghosts")
        for sout in dsoutputs.values():
            ghosts = tuple(sout.ghosts[:-1]) + (self.remesh_ghosts,)
            kl += sout.accumulate_ghosts(
                directions=sout.dim - 1, ghosts=ghosts, build_launcher=True
            )
            kl += sout.exchange_ghosts(build_launcher=True)
        self.accumulate_and_exchange = kl
        return kl

    @op_apply
    def apply(self, dbg=None, **kargs):
        queue = self.cl_env.default_queue
        dt = self.precision(self.dt() * self.dt_coeff)

        if self.DEBUG:
            queue.flush()
            queue.finish()
            dfin = tuple(self.dadvected_fields_in.values())
            dfout = tuple(self.dadvected_fields_out.values())
            print(f"OPENCL_DT= {dt}")
            self.advection_kernel_launcher(queue=queue, dt=dt).wait()
            print("OPENCL_P")
            self.dposition.print_with_ghosts()
            print("OPENCL_Sin (before remesh)")
            print(dfin[0].data[0].get(queue=queue))
            print("OPENCL_Sout (before remesh)")
            print(dfout.data[0].get(queue=queue))
            print()
            self.remesh_kernel_launcher(queue=queue).wait()
            print("OPENCL_Sout (before accumulation)")
            data = dfout[0].data[0]
            print(data.get(queue=queue))
            print(f"OPENCL_Sout (before accumulation, no ghosts)  ID={id(data)}")
            dfout[0].print_with_ghosts()
            self.accumulate_and_exchange(queue=queue).wait()
            print("OPENCL_Sout (after accumulation)")
            print(dfout[0].data[0])
            print("OPENCL_Sout (after accumulation, no ghosts)")
            dfout[0].print_with_ghosts()
        else:
            self.all_kernels(queue=queue, dt=dt)

    @classmethod
    def supports_mpi(cls):
        return True
