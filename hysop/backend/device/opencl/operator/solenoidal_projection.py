# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import primefac
import functools
from hysop import vprint
from hysop.tools.numpywrappers import npw
from hysop.tools.decorators import debug
from hysop.tools.units import bytes2str
from hysop.tools.numerics import is_complex, find_common_dtype
from hysop.operator.base.solenoidal_projection import SolenoidalProjectionOperatorBase
from hysop.backend.device.opencl.opencl_symbolic import OpenClSymbolic
from hysop.core.graph.graph import op_apply
from hysop.core.memory.memory_request import MemoryRequest
from hysop.backend.device.opencl.opencl_fft import OpenClFFT
from hysop.backend.device.codegen.base.variables import dtype_to_ctype
from hysop.backend.device.opencl.opencl_kernel_launcher import OpenClKernelListLauncher
from hysop.symbolic import local_indices_symbols
from hysop.symbolic.misc import Select, Expand
from hysop.symbolic.complex import ComplexMul
from hysop.symbolic.relational import Assignment, LogicalEQ, LogicalAND


class OpenClSolenoidalProjection(SolenoidalProjectionOperatorBase, OpenClSymbolic):
    """
    Solves the poisson rotational equation using clFFT and an OpenCL symbolic kernels.
    """

    def initialize(self, **kwds):
        # request the projection kernel if required
        Fin = tuple(
            Ft.output_symbolic_array(f"Fin{i}_hat")
            for (i, Ft) in enumerate(self.forward_transforms)
        )
        Fout = tuple(
            Bt.input_symbolic_array(f"Fout{i}_hat")
            for (i, Bt) in enumerate(self.backward_transforms)
        )
        K1s, K2s = (), ()
        for kd1 in self.kd1s:
            Ki = self.tg.indexed_wavenumbers(*kd1)[::-1]
            K1s += (Ki,)
        for kd2 in self.kd2s:
            Ki = self.tg.indexed_wavenumbers(*kd2)[::-1]
            K2s += (Ki,)

        dtype = find_common_dtype(
            *tuple(Ft.output_dtype for Ft in self.forward_transforms)
        )
        Cs = self.symbolic_tmp_scalars("C", dtype=dtype, count=3)

        I = local_indices_symbols[:3]
        cond = LogicalAND(*tuple(LogicalEQ(Ik, 0) for Ik in I))

        exprs = ()
        for i in range(3):
            expr = 0
            for j in range(3):
                e = Fin[j]
                if i == j:
                    e = K2s[j][j] * e
                else:
                    e = (
                        ComplexMul(K1s[j][j], e)
                        if K1s[j][j].Wn.is_complex
                        else K1s[j][j] * e
                    )
                    e = (
                        ComplexMul(K1s[j][i], e)
                        if K1s[j][i].Wn.is_complex
                        else K1s[j][i] * e
                    )
                expr += e
            expr /= sum(K2s[i])
            expr = Select(expr, 0, cond)
            expr = Assignment(Cs[i], expr)
            exprs += (expr,)
        for i in range(3):
            expr = Assignment(Fout[i], Fin[i] - Cs[i])
            exprs += (expr,)

        self.require_symbolic_kernel("solenoidal_projection", *exprs)

        if self.compute_divFin:
            divFin = self.backward_divFin_transform.input_symbolic_array("divFin")
            expr = sum(
                (
                    ComplexMul(K1s[j][j], Fin[j])
                    if K1s[j][j].Wn.is_complex
                    else K1s[j][j] * Fin[j]
                )
                for j in range(3)
            )
            expr = Assignment(divFin, expr)
            self.require_symbolic_kernel("compute_divFin", expr)

        if self.compute_divFout:
            expr = sum(
                (
                    ComplexMul(K1s[j][j], Fout[j])
                    if K1s[j][j].Wn.is_complex
                    else K1s[j][j] * Fout[j]
                )
                for j in range(3)
            )
            divFout = self.backward_divFout_transform.input_symbolic_array("divFout")
            expr = Assignment(divFout, expr)
            self.require_symbolic_kernel("compute_divFout", expr)

        super().initialize(**kwds)

    @debug
    def setup(self, work):
        super().setup(work)
        self._build_projection_kernel()
        self._build_divergence_kernels()
        self._build_ghost_exchangers()

    def _build_projection_kernel(self):
        knl, _ = self.symbolic_kernels["solenoidal_projection"]
        self.projection_kernel = functools.partial(knl, queue=self.cl_env.default_queue)

    def _build_divergence_kernels(self):
        if self.compute_divFin:
            knl, _ = self.symbolic_kernels["compute_divFin"]
            self.compute_divFin_kernel = functools.partial(
                knl, queue=self.cl_env.default_queue
            )
        if self.compute_divFout:
            knl, _ = self.symbolic_kernels["compute_divFout"]
            self.compute_divFout_kernel = functools.partial(
                knl, queue=self.cl_env.default_queue
            )

    def _build_ghost_exchangers(self):
        kl = OpenClKernelListLauncher(name="exchange_ghosts", profiler=self._profiler)
        kl += self.dFout.exchange_ghosts(build_launcher=True)
        if self.compute_divFin:
            kl += self.ddivFin.exchange_ghosts(build_launcher=True)
        if self.compute_divFout:
            kl += self.ddivFout.exchange_ghosts(build_launcher=True)
        self.exchange_ghost_kernels = functools.partial(
            kl, queue=self.cl_env.default_queue
        )

    @op_apply
    def apply(self, simulation=None, **kwds):
        """Solve the SolenoidalProjection."""
        super().apply(**kwds)

        for Ft in self.forward_transforms:
            evt = Ft()
        if self.compute_divFin:
            evt = self.compute_divFin_kernel()
            evt = self.backward_divFin_transform()
        evt = self.projection_kernel()
        if self.compute_divFout:
            evt = self.compute_divFout_kernel()
            evt = self.backward_divFout_transform()
        for Bt in self.backward_transforms:
            evt = Bt()
        evt = self.exchange_ghost_kernels()
