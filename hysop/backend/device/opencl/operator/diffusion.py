# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import primefac, functools
from hysop.tools.numerics import float_to_complex_dtype
from hysop.tools.numpywrappers import npw
from hysop.tools.decorators import debug
from hysop.tools.warning import HysopWarning
from hysop.operator.base.diffusion import DiffusionOperatorBase
from hysop.backend.device.opencl.opencl_symbolic import OpenClSymbolic
from hysop.core.graph.graph import op_apply
from hysop.core.memory.memory_request import MemoryRequest
from hysop.backend.device.opencl.opencl_fft import OpenClFFT
from hysop.backend.device.codegen.base.variables import dtype_to_ctype
from hysop.symbolic import local_indices_symbols
from hysop.symbolic.relational import LogicalAND, LogicalEQ, Assignment


class OpenClDiffusion(DiffusionOperatorBase, OpenClSymbolic):
    """
    Solves the diffusion equation using an OpenCL FFT backend.
    """

    @debug
    def __new__(cls, **kwds):
        return super().__new__(cls, **kwds)

    @debug
    def __init__(self, **kwds):
        super().__init__(**kwds)
        queue = self.cl_env.default_queue

        nu, dt = self.nu.s, self.dt.s

        kernel_names = ()
        for i, (Ft, Wn) in enumerate(zip(self.forward_transforms, self.wave_numbers)):
            Fhs = Ft.output_symbolic_array(f"F{i}_hat")
            indices = local_indices_symbols[: Fhs.dim]

            kname = f"filter_diffusion_{Fhs.dim}d_{i}"
            kernel_names += (kname,)

            F = 0
            for Wi in Wn:
                indexed_Wi = self.tg._indexed_wave_numbers[Wi]
                F += indexed_Wi
            expr = Assignment(Fhs, Fhs / (1 - nu * dt * F))

            self.require_symbolic_kernel(kname, expr)

        self._kernel_names = kernel_names

    @debug
    def setup(self, work):
        super().setup(work)
        queue = self.cl_env.default_queue

        diffusion_filters = ()
        for kname in self._kernel_names:
            kernel, up = self.symbolic_kernels[kname]
            kernel = lambda kernel=kernel, queue=queue, up=up: kernel(
                queue=queue, **up()
            )
            diffusion_filters += (kernel,)

        eg = self.dFout.exchange_ghosts(build_launcher=True)
        if eg is not None:
            eg = functools.partial(eg, queue=queue)

        self._diffusion_filters = diffusion_filters
        self._exchange_ghosts = eg

    @op_apply
    def apply(self, simulation, **kwds):
        """Solve the Diffusion equation."""
        super().apply(**kwds)
        exchange_ghosts = self._exchange_ghosts
        for Ft, Bt, filter_diffusion in zip(
            self.forward_transforms, self.backward_transforms, self._diffusion_filters
        ):
            evt = Ft(simulation=simulation)
            evt = filter_diffusion()
            evt = Bt(simulation=simulation)
        if exchange_ghosts:
            evt = exchange_ghosts()
        self.plot(simulation=simulation)
