# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Everything concerning OpenCL in hysop.
Some sources are parsed at build to handle several OpenCL features.
Other sources are generated and optimized at runtime.
see hysop.backend.device.opencl.opencl_tools.parse_file
see hysop.backend.device.codegen
"""

import os
import pyopencl
import pyopencl.tools
import pyopencl.array
import pyopencl.characterize
import pyopencl.reduction
import pyopencl.clrandom
import pyopencl.elementwise
import pyopencl.scan

try:
    # old pyopencl interface using the cffi
    from pyopencl import cffi_cl as cl_api
except ImportError:
    # new interface using pybind11
    from pyopencl import _cl as cl_api


from hysop import __DEFAULT_PLATFORM_ID__, __DEFAULT_DEVICE_ID__, __PROFILE__, get_env
from hysop.tools.io_utils import IO
from hysop.backend.device import KERNEL_DUMP_FOLDER

OPENCL_KERNEL_DUMP_FOLDER = f"{KERNEL_DUMP_FOLDER}/opencl"
"""Default opencl kernel dump folder."""

__OPENCL_PROFILE__ = get_env("OPENCL_PROFILE", __PROFILE__)
"""Boolean, true to enable OpenCL profiling events to time computations"""

# open cl underlying implementation
cl = pyopencl
"""PyOpencl module, underlying OpenCL implementation"""

clTools = pyopencl.tools
"""PyOpencl tools"""

clTypes = pyopencl.cltypes
"""PyOpencl types"""

clArray = pyopencl.array
"""PyOpenCL arrays"""

clRandom = pyopencl.clrandom
"""PyOpenCL random"""

clReduction = pyopencl.reduction
"""PyOpenCL reductions"""

clScan = pyopencl.scan
"""PyOpenCL scan"""

clElementwise = pyopencl.elementwise
"""PyOpenCL reductions"""

clCharacterize = pyopencl.characterize
"""PyOpenCL characterize"""

if "CLFFT_REQUEST_LIB_NOMEMALLOC" not in os.environ:
    os.environ["CLFFT_REQUEST_LIB_NOMEMALLOC"] = "1"
if "CLFFT_CACHE_PATH" not in os.environ:
    os.environ["CLFFT_CACHE_PATH"] = IO.default_cache_path() + "/clfft"
if not os.path.isdir(os.environ["CLFFT_CACHE_PATH"]):
    try:
        os.makedirs(os.environ["CLFFT_CACHE_PATH"])
    except:
        print(
            "Could not create clfft cache directory '{}'.".format(
                os.environ["CLFFT_CACHE_PATH"]
            )
        )
