# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import warnings
import numpy as np

try:
    from gpyfft.fft import FFT, gfft, GFFT
except ImportError as e:

    class FFT:
        def __init__(self):
            assert False, f"Du to gpyfft import error ({e}), this class is useless"

    gfft, GFFT = None, None
    print(e)
    print(
        "Some functionnalities may not work. It seems that hysop is called from non OpenCL machine."
    )

from hysop import vprint
from hysop.tools.htypes import first_not_None
from hysop.tools.warning import HysopWarning
from hysop.tools.units import bytes2str
from hysop.tools.numerics import is_complex, is_fp
from hysop.tools.numpywrappers import npw
from hysop.backend.device.opencl import cl, clArray
from hysop.backend.device.codegen.base.variables import dtype_to_ctype


class OpenClFFT(FFT):

    def __init__(
        self,
        context,
        queue,
        in_array,
        out_array=None,
        axes=None,
        fast_math=False,
        real=False,
        keep_buffer_offset=False,
    ):
        """
        Wrap gpyfft.FFT to allow more versatile callback settings
        and buffer allocations.
        """
        self.context = context
        self.queue = queue

        if axes is None:
            axes = np.argsort(in_array.strides)
        else:
            axes = np.asarray(axes)

        t_strides_in, t_distance_in, t_batchsize_in, t_shape, axes_transform = (
            self.calculate_transform_strides(axes, in_array)
        )

        if out_array is not None:
            t_inplace = False
            (
                t_strides_out,
                t_distance_out,
                t_batchsize_out,
                t_shape_out,
                axes_transform_out,
            ) = self.calculate_transform_strides(axes, out_array)
            if in_array.base_data == out_array.base_data:
                # enforce no overlap (unless inplace)
                if in_array.offset < out_array.offset:
                    assert (in_array.offset + in_array.nbytes) < out_array.offset
                elif in_array.offset > out_array.offset:
                    assert (out_array.offset + out_array.nbytes) < in_array.offset
                else:
                    t_inplace = True
            msg = "error finding transform axis (consider setting axes argument)"
            assert np.all(axes_transform == axes_transform_out), msg
        else:
            out_array = in_array
            t_inplace = True
            t_strides_out, t_distance_out = t_strides_in, t_distance_in

        if in_array.dtype in (np.float32, np.complex64):
            precision = gfft.CLFFT_SINGLE
        elif in_array.dtype in (np.float64, np.complex128):
            precision = gfft.CLFFT_DOUBLE

        if in_array.dtype in (np.float32, np.float64):
            layout_in = gfft.CLFFT_REAL
            layout_out = gfft.CLFFT_HERMITIAN_INTERLEAVED

            expected_out_shape = list(in_array.shape)
            expected_out_shape[axes_transform[0]] = (
                expected_out_shape[axes_transform[0]] // 2 + 1
            )
            msg = "output array shape {} does not match expected shape: {}"
            msg = msg.format(out_array.shape, expected_out_shape)
            assert out_array.shape == tuple(expected_out_shape), msg
        elif in_array.dtype in (np.complex64, np.complex128):
            if not real:
                layout_in = gfft.CLFFT_COMPLEX_INTERLEAVED
                layout_out = gfft.CLFFT_COMPLEX_INTERLEAVED
            else:
                layout_in = gfft.CLFFT_HERMITIAN_INTERLEAVED
                layout_out = gfft.CLFFT_REAL
                t_shape = t_shape_out

        if t_inplace and (
            (layout_in is gfft.CLFFT_REAL) or (layout_out is gfft.CLFFT_REAL)
        ):
            assert (
                in_array.strides[axes_transform[0]] == in_array.dtype.itemsize
            ) and (
                out_array.strides[axes_transform[0]] == out_array.dtype.itemsize
            ), "inline real transforms need stride 1 for first transform axis"

        self.t_shape = t_shape
        self.batchsize = t_batchsize_in
        self.t_inplace = t_inplace

        plan = GFFT.create_plan(context, t_shape)
        plan.inplace = t_inplace
        plan.strides_in = t_strides_in
        plan.strides_out = t_strides_out
        plan.distances = (t_distance_in, t_distance_out)
        plan.batch_size = self.batchsize
        plan.precision = precision
        plan.layouts = (layout_in, layout_out)

        assert not keep_buffer_offset
        (in_data, out_data) = self.set_offset_callbacks(
            plan, in_array, out_array, layout_in, layout_out, keep_buffer_offset
        )

        self.plan = plan
        self.in_array = in_array
        self.out_array = out_array
        self.in_data = in_data
        self.out_data = out_data

        self.temp_buffer = None
        self._baked = False
        self._allocated = False

    def set_offset_callbacks(
        self, plan, in_array, out_array, layout_in, layout_out, keep_buffer_offset
    ):
        try:
            if keep_buffer_offset:
                raise clArray.ArrayHasOffsetError
            in_data = in_array.data
            input_buffer_offset = "0uL"
        except clArray.ArrayHasOffsetError:
            in_data = in_array.base_data
            pre, input_buffer_offset = self.pre_offset_callback(in_array, layout_in)
            plan.set_callback("pre_callback", pre, "pre", user_data=None)

        try:
            if keep_buffer_offset:
                raise clArray.ArrayHasOffsetError
            out_data = out_array.data
            output_buffer_offset = "0uL"
        except clArray.ArrayHasOffsetError:
            out_data = out_array.base_data
            post, output_buffer_offset = self.post_offset_callback(
                out_array, layout_out
            )
            plan.set_callback("post_callback", post, "post", user_data=None)

        self.input_buffer_offset = input_buffer_offset
        self.output_buffer_offset = output_buffer_offset

        return (in_data, out_data)

    def bake(self):
        if self._baked:
            msg = "Plan was already baked."
            raise RuntimeError(msg)
        msg = "Baking {}[precision={}, shape={}, inplace={}, layout_in={}, layout_out={}]".format(
            self.__class__.__name__,
            self.precision,
            self.t_shape,
            self.t_inplace,
            self.layout_in,
            self.layout_out,
        )
        self.plan.bake(self.queue)
        self._baked = True
        return self

    def allocate(self, buf=None):
        if self._allocated:
            msg = "Plan was already allocated."
            raise RuntimeError(msg)
        size = self.plan.temp_array_size
        if size > 0:
            if buf is None:
                msg = "Allocating temporary buffer of size {} for clFFT::{}."
                msg = msg.format(bytes2str(size), id(self))
                warnings.warn(msg, HysopWarning)
                buf = cl.Buffer(self.context, cl.mem_flags.READ_WRITE, size=size)
                self.temp_buffer = buf
            elif buf.size != size:
                msg = "Buffer does not match required size: {} != {}"
                msg = msg.format(buf.size, size)
                raise ValueError(msg)
            else:
                self.temp_buffer = buf.data
        else:
            assert buf is None, buf
        self._allocated = True
        return self

    def enqueue(self, queue=None, wait_for_events=None, direction_forward=True):
        r"""
        Enqueue transform with array base_data.
        /!\ Do not forget to offset input and output by array.offset
            within custom user callbacks, only base_data is passed
            to ensure OpenCL pointers alignment of kernel arguments.
            See self.set_offset_callbacks().
        """
        self._assert_ready()
        in_data, out_data = self.in_data, self.out_data

        queue = first_not_None(queue, self.queue)
        if self.t_inplace:
            events = self.plan.enqueue_transform(
                (queue,),
                (in_data,),
                direction_forward=direction_forward,
                temp_buffer=self.temp_buffer,
                wait_for_events=wait_for_events,
            )
        else:
            events = self.plan.enqueue_transform(
                (queue,),
                (in_data,),
                (out_data),
                direction_forward=direction_forward,
                temp_buffer=self.temp_buffer,
                wait_for_events=wait_for_events,
            )
        return events

    def enqueue_arrays(self, *args, **kwds):
        msg = "Enqueue arrays is not supported."
        raise NotImplementedError(msg)

    @property
    def required_buffer_size(self):
        assert self._baked, "plan has not been baked yet."
        return self.plan.temp_array_size

    @property
    def ready(self):
        return self._baked and self._allocated

    def _assert_ready(self):
        if __debug__ and not self.ready:
            msg = "Plan is not ready:"
            msg += "\n  *baked:     {}"
            msg += "\n  *allocated: {}"
            msg += "\n"
            msg = msg.format(self._baked, self._allocated)
            raise RuntimeError(msg)

    @classmethod
    def check_dtype(cls, dtype, layout):
        if layout in (gfft.CLFFT_HERMITIAN_INTERLEAVED, gfft.CLFFT_COMPLEX_INTERLEAVED):
            if not is_complex(dtype):
                msg = "Layout is {} but got array with dtype {}."
                msg = msg.format(layout, dtype)
                raise RuntimeError(msg)
        elif layout in (gfft.CLFFT_REAL,):
            if not is_fp(dtype):
                msg = "Layout is CLFFT_REAL but got array with dtype {}."
                msg = msg.format(dtype)
                raise RuntimeError(msg)
        else:
            msg = "Unsupported data layout {}."
            msg = msg.format(layout)
            raise NotImplementedError(msg)

    def pre_offset_callback(self, in_array, layout_in):
        dtype = in_array.dtype
        fp = dtype_to_ctype(dtype)
        self.check_dtype(dtype, layout_in)
        if (in_array.offset % dtype.itemsize) != 0:
            msg = "Unaligned array offset."
            raise RuntimeError(msg)
        base_offset = in_array.offset // dtype.itemsize

        callback = """
        {fp} pre_callback(__global void* input,
                          const uint offset,
                          __global void* userdata) {{
            __global {fp}* in = (__global {fp}*) input;
            return in[{base_offset}uL+offset];
        }}
        """.format(
            fp=fp, base_offset=base_offset
        )

        input_buffer_offset = f"{base_offset}uL"

        return callback, input_buffer_offset

    def post_offset_callback(self, out_array, layout_out):
        dtype = out_array.dtype
        self.check_dtype(dtype, layout_out)
        fp = dtype_to_ctype(dtype)
        if (out_array.offset % dtype.itemsize) != 0:
            msg = "Unaligned array offset."
            raise RuntimeError(msg)
        base_offset = out_array.offset // dtype.itemsize

        callback = """
        void post_callback(__global void* output,
                           const uint offset,
                          __global void* userdata,
                           const {fp} fftoutput) {{
            __global {fp}* out = (__global {fp}*) output;
            out[{base_offset}uL+offset] = fftoutput;
        }}
        """.format(
            fp=fp, base_offset=base_offset
        )

        output_buffer_offset = f"{base_offset}uL"

        return callback, output_buffer_offset

    @classmethod
    def allocate_plans(cls, operator, plans):
        tmp_size = max(plan.required_buffer_size for plan in plans)

        msg = "Allocating an additional {} temporary buffer for clFFT in operator {}.".format(
            bytes2str(tmp_size), operator.name
        )

        if tmp_size > 0:
            vprint(msg)
            tmp_buffer = operator.backend.empty(shape=(tmp_size), dtype=npw.uint8)
            for plan in plans:
                if plan.required_buffer_size > tmp_buffer.nbytes:
                    msg = (
                        "\nFATAL ERROR: Failed to allocate temporary buffer for clFFT."
                    )
                    msg += "\n => clFFT expected {} bytes but only {} bytes have been allocated.\n"
                    msg = msg.format(plan.required_buffer_size, tmp_buffer.nbytes)
                    raise RuntimeError(msg)
                elif plan.required_buffer_size > 0:
                    buf = tmp_buffer[: plan.required_buffer_size]
                    plan.allocate(buf=buf)
                else:
                    plan.allocate(buf=None)
        else:
            for plan in plans:
                assert plan.required_buffer_size == 0
                plan.allocate()
            tmp_buffer = None
        return tmp_buffer
