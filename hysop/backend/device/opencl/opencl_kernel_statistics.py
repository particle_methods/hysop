# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.htypes import check_instance
from hysop.backend.device.kernel_statistics import KernelStatistics
from hysop.backend.device.opencl import cl


class OpenClKernelStatistics(KernelStatistics):
    """
    Build execution statistics from OpenCL kernel events.
    """

    def __init__(self, events=None, **kwds):
        check_instance(events, (tuple, list), values=cl.Event, allow_none=True)
        if (events is not None) and events:
            nruns = len(events)
            p0 = events[0].profile
            dt0 = p0.end - p0.start
            total = dt0
            maxi = dt0
            mini = dt0
            data = [dt0]
            for evt in events[1:]:
                dt = evt.profile.end - evt.profile.start
                total += dt
                if dt < mini:
                    mini = dt
                if dt > maxi:
                    maxi = dt
                data.append(dt)
        else:
            nruns = 0
            mini = None
            maxi = None
            total = None
            data = None

        # OpenCl profiling units are already in nanoseconds so we
        # are good.
        # CL_DEVICE_PROFILING_TIMER_RESOLUTION determines the accuracy
        # of the timer not the unit.
        super().__init__(
            nruns=nruns, total=total, min_=mini, max_=maxi, data=data, **kwds
        )
