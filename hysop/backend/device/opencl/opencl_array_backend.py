# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import re
import warnings
import numpy as np

from hysop import __KERNEL_DEBUG__
from hysop.tools.htypes import check_instance, to_tuple
from hysop.tools.misc import prod
from hysop.tools.numerics import (
    is_complex,
    get_dtype,
    float_to_complex_dtype,
    complex_to_float_dtype,
    find_common_dtype,
)
from hysop.constants import Backend
from hysop.constants import HYSOP_REAL, HYSOP_INTEGER, HYSOP_INDEX, HYSOP_BOOL
from hysop.backend.device.opencl import (
    cl,
    clArray,
    clTools,
    clRandom,
    clReduction,
    clElementwise,
    clScan,
)

from hysop.core.arrays import default_order
from hysop.core.arrays import MemoryOrdering, MemoryType, QueuePolicy

from hysop.core.arrays.array_backend import ArrayBackend
from hysop.core.arrays.array import Array

# from hysop.core.memory.mempool import MemoryPool

from hysop.backend.device.opencl.opencl_allocator import OpenClAllocator
from hysop.backend.device.opencl.opencl_env import OpenClEnvironment
from hysop.backend.device.opencl.opencl_kernel_launcher import (
    OpenClKernelLauncherI,
    trace_kernel,
    profile_kernel,
)

from hysop.tools.numerics import (
    is_fp,
    is_integer,
    is_signed,
    is_unsigned,
    get_dtype,
    match_dtype,
)
from hysop.tools.htypes import first_not_None

from hysop.backend.device.opencl.opencl_tools import get_or_create_opencl_env


class _ElementwiseKernel:
    """
    Evaluating involved expressions on OpenClArray instances by using overloaded
    operators can be somewhat inefficient, because a new temporary is created for
    each intermediate result. ElementwiseKernel generate kernels that evaluate
    multi-stage expressions on one or several operands in a single pass.

    clElementwise.ElementwiseKernel adapted for use with OpenClArrayBackend.elementwise(...)
    """

    def __init__(
        self,
        context,
        arguments,
        operation,
        name="elementwise",
        preamble="",
        options=[],
        **kwds,
    ):
        self.kernel = clElementwise.ElementwiseKernel(
            context=context,
            arguments=arguments,
            operation=operation,
            name=name,
            preamble=preamble,
            options=options,
        )

    def __call__(self, wait_for, args, **kwds):
        return self.kernel.__call__(
            wait_for=wait_for, queue=kwds.get("queue", None), *args
        )

    def to_kernel_launcher(self, name, wait_for, queue, args, **kwds):
        """Build an _OpenClElementWiseKernelLauncher from self."""

        class OpenClElementwiseKernelLauncher(OpenClKernelLauncherI):
            """Utility class to build opencl reduction kernel launchers."""

            __slots__ = (
                "_name",
                "kernel",
                "kernel_args",
                "kernel_kwds",
                "default_queue",
            )

            def __init__(self, name, kernel, args, default_queue, **extra_kernel_kwds):
                super().__init__(name=name)
                kernel_args = args
                kernel_kwds = {}
                self.kernel = kernel
                self.kernel_args = kernel_args
                self.kernel_kwds = kernel_kwds
                self.default_queue = default_queue
                self._apply_msg = "  " + name + f"<<<{args[0].shape}>>>()"

            def global_size_configured(self):
                return True

            def __call__(self, queue=None, wait_for=None, **kwds):
                trace_kernel(self._apply_msg)
                queue = first_not_None(queue, self.default_queue)
                self.kernel_kwds["queue"] = queue
                self.kernel_kwds["wait_for"] = wait_for
                evt = self.kernel(*self.kernel_args, **self.kernel_kwds)
                profile_kernel(None, evt, self._apply_msg)
                self._register_event(queue, evt)
                return evt

        return OpenClElementwiseKernelLauncher(
            name=name, kernel=self.kernel, args=args, default_queue=queue, **kwds
        )


class _ReductionKernel:
    """
    Generate a kernel that takes a number of scalar or vector arguments
    (at least one vector argument), performs the map_expr on each entry
    of the vector argument and then the reduce_expr on the outcome.

    neutral serves as an initial value and is specified as float or
    integer formatted as string.

    preamble offers the possibility to add preprocessor directives
    and other code (such as helper functions) to be included before the
    actual reduction kernel code.

    vectors in map_expr should be indexed by the variable i.

    reduce_expr uses the formal values 'a' and 'b' to indicate
    two operands of a binary reduction operation.

    If you do not specify a map_expr, in[i] is automatically
    assumed and treated as the only one input argument.

    dtype_out specifies the numpy.dtype in which the reduction is performed
    and in which the result is returned.

    clReduction.ReductionKernel adapted for use with OpenClArrayBackend.elementwise(...)
    """

    def __init__(
        self,
        context,
        arguments,
        reduce_expr,
        map_expr,
        neutral,
        dtype,
        name="reduce",
        preamble="",
        options=[],
        **kargs,
    ):
        # remove output argument from arguments list
        arguments = arguments.split(",")
        arguments = ",".join(arguments[1:])

        self.kernel = clReduction.ReductionKernel(
            ctx=context,
            dtype_out=dtype,
            neutral=neutral,
            reduce_expr=reduce_expr,
            map_expr=map_expr,
            arguments=arguments,
            name=name,
            preamble=preamble,
            options=options,
        )

    def __call__(self, wait_for, return_event, iargs, oargs, pargs, queue, **kargs):
        assert len(oargs) == 1
        assert len(iargs) >= 1
        return self.kernel.__call__(
            *(iargs + pargs),
            out=oargs[0],
            queue=queue,
            wait_for=wait_for,
            return_event=return_event,
            allocator=None,
            range=None,
            slice=None,
        )

    def to_kernel_launcher(self, name, wait_for, queue, args, **kwds):
        """Build an _OpenClReductionKernelLauncher from self."""

        class OpenClReductionKernelLauncher(OpenClKernelLauncherI):
            """Utility class to build opencl reduction kernel launchers."""

            __slots__ = (
                "_name",
                "kernel",
                "kernel_args",
                "kernel_kwds",
                "default_queue",
                "return_event",
            )

            def __init__(
                self,
                name,
                kernel,
                return_event,
                iargs,
                oargs,
                pargs,
                default_queue,
                **extra_kernel_kwds,
            ):
                super().__init__(name=name)
                kernel_args = iargs + pargs
                kernel_kwds = dict(
                    out=oargs[0],
                    return_event=True,
                    allocator=None,
                    range=None,
                    slice=None,
                )
                self.kernel = kernel
                self.kernel_args = kernel_args
                self.kernel_kwds = kernel_kwds
                self.default_queue = default_queue
                self._apply_msg = "  " + name + f"<<<{self.kernel_args[0].shape}>>>()"

            def global_size_configured(self):
                return True

            def __call__(self, queue=None, wait_for=None, **kwds):
                trace_kernel(self._apply_msg)
                queue = first_not_None(queue, self.default_queue)
                self.kernel_kwds["queue"] = queue
                self.kernel_kwds["wait_for"] = wait_for
                (out, evt) = self.kernel(*self.kernel_args, **self.kernel_kwds)
                profile_kernel(None, evt, self._apply_msg)
                self._register_event(queue, evt)
                self.out = out
                return evt

        return OpenClReductionKernelLauncher(
            name=name, kernel=self.kernel, default_queue=queue, **kwds
        )


class _GenericScanKernel:
    """
    Generates an OpenCL kernel that performs prefix sums ('scans') on arbitrary types,
    with many possible tweaks.

    Adapted for use with OpenClArrayBackend.elementwise(...)

    Context and devices:
        - context: context within the code for this kernel will be generated.
        - devices may be used to restrict the set of devices on which the kernel is meant to
          run (defaults to all devices in the context ctx).

    Prefix sum configuration:
        - dtype specifies the numpy.dtype with which the scan will be performed.
          May be a structured type if that type was registered through
          clTools.get_or_register_dtype().

        - arguments: An optional string of comma-separated C argument declarations.
                     If arguments is specified, then input_expr must also be specified.

        - input_expr: A C expression, encoded as a string, resulting in the values to which
                      the scan is applied. This may be used to apply a mapping to values stored
                      in arguments before being scanned.
                      The result of this expression must match dtype.

        - scan_expr: The associative, binary operation carrying out the scan,
                     represented as a C string. Its two arguments are available
                     as a and b when it is evaluated. b is guaranteed to be the
                     element being updated, and a is the increment.

        - neutral is the neutral element of scan_expr, obeying scan_expr(a, neutral) == a.

        - output_statement: a C statement that writes the output of the scan.
          It has access to the scan result as item, the preceding scan result item as
          prev_item, and the current index as i.
          prev_item in a segmented scan will be the neutral element i
          at a segment boundary, not the immediately preceding item.

          Using prev_item in output statement has a small run-time cost.
          prev_item enables the construction of an exclusive scan.

          For non-segmented scans, output_statement may also reference last_item,
          which evaluates to the scan result of the last array entry.

    Segmented scans:
        - is_segment_start_expr: A C expression, encoded as a string, resulting in a C bool
          value that determines whether a new scan segments starts at index i.
          If given, makes the scan a segmented scan. Has access to the current index i,
          the result of input_expr as a, and in addition may use arguments and input_fetch_expr
          variables just like input_expr.

          If it returns true, then previous sums will not spill over into the item with
          index i or subsequent items.

        - input_fetch_exprs: a list of tuples (NAME, ARG_NAME, OFFSET).
          An entry here has the effect of doing the equivalent of the
          following before input_expr:   ARG_NAME_TYPE NAME = ARG_NAME[i+OFFSET];

          OFFSET is allowed to be 0 or -1
          ARG_NAME_TYPE is the type of ARG_NAME.

    Kernel generation and build options:
        - name is used for kernel names to ensure recognizability in profiles and logs.
        - options is a list of compiler options to use when building.
        - preamble specifies a string of code that is inserted before the actual kernels.

    clScan.GenericScanKernel adapted for use with OpenClArrayBackend.elementwise(...)
    """

    def __init__(
        self,
        context,
        dtype,
        arguments,
        input_expr,
        scan_expr,
        neutral,
        output_statement,
        is_segment_start_expr=None,
        input_fetch_exprs=None,
        name="generic_scan",
        options=[],
        preamble="",
        devices=None,
        index_dtype=np.int32,
        **kargs,
    ):

        input_fetch_exprs = first_not_None(input_fetch_exprs, [])

        self.kernel = clScan.GenericScanKernel(
            ctx=context,
            dtype=dtype,
            index_dtype=index_dtype,
            arguments=arguments,
            neutral=neutral,
            scan_expr=scan_expr,
            input_expr=input_expr,
            output_statement=output_statement,
            is_segment_start_expr=is_segment_start_expr,
            input_fetch_exprs=input_fetch_exprs,
            name_prefix=name,
            preamble=preamble,
            options=options,
        )

    def __call__(self, args, queue, wait_for=None, size=None, **kargs):
        """
        Call the kernel with arguments args.
        size may specify the length of the scan to be carried out.
        If not given, this length is inferred from the first array argument passed.
        """
        return self.kernel.__call__(*args, queue=queue, size=size, wait_for=wait_for)


class OpenClArrayBackend(ArrayBackend):
    """
    Opencl array backend, extending pyopencl.array.Array capabilities.

    By default all methods are using the default opencl device (setup during
    build in hysop.__init__.py and hysop.backend.device.opencl.__init__.py)
    and an associated default command queue. An OpenClArrayBackend can be
    created with any context and default queue through an OpenClEnvironment.

    See hysop.backend.device.opencl.opencl_tools.get_or_create_opencl_env() for more
    informations on how to create or get an OpenClEnvironment.

    The default allocator is a hysop.backend.device.opencl.opencl_allocator.OpenClImmediateAllocator
    and the arrays are created within a hysop.core.memory.MemoryPool based on this allocator
    so that there is no surprise when the array is first used (out of memory).
    """

    def get_kind(self):
        return Backend.OPENCL

    kind = property(get_kind)

    def get_host_array_backend(self):
        return self._host_array_backend

    host_array_backend = property(get_host_array_backend)

    def short_description(self):
        return (
            ":OpenClBackend:  tag={}, cl_env={}, allocator={}, host_backend={}]".format(
                self.tag,
                self.cl_env.tag,
                self.allocator.full_tag,
                self.host_array_backend.full_tag,
            )
        )

    def __eq__(self, other):
        if not other.__class__ == self.__class__:
            return NotImplemented
        eq = self._context is other._context
        eq &= self._default_queue is other._default_queue
        eq &= self._allocator is other._allocator
        eq &= self.host_array_backend is other.host_array_backend
        return eq

    def __ne__(self, other):
        return not (self == other)

    def __hash__(self):
        return (
            id(self._context)
            ^ id(self._default_queue)
            ^ id(self.allocator)
            ^ id(self.host_array_backend)
        )

    __backends = {}

    @classmethod
    def get_or_create(cls, cl_env, queue=None, allocator=None, host_array_backend=None):
        from hysop.core.arrays.all import HostArrayBackend, default_host_array_backend

        host_array_backend = first_not_None(
            host_array_backend, default_host_array_backend
        )
        if queue is None:
            queue = cl_env.default_queue
        allocator = first_not_None(allocator, cl_env.allocator)
        key = (cl_env, queue, allocator, host_array_backend)
        if key in cls.__backends:
            return cls.__backends[key]
        else:
            obj = cls(
                cl_env=cl_env,
                queue=queue,
                allocator=allocator,
                host_array_backend=host_array_backend,
            )
            cls.__backends[key] = obj
            return obj

    def __new__(cls, cl_env=None, queue=None, allocator=None, host_array_backend=None):
        return super().__new__(cls, allocator=None)

    def __init__(
        self, cl_env=None, queue=None, allocator=None, host_array_backend=None
    ):
        """
        Initialize and OpenClArrayBackend with OpenCL environment cl_env.

        If cl_env and queue are not specified, the default environment returned
        by get_or_create_opencl_env() will be used along with its default queue.

        If cl_env is not specified but a queue is specified, this queue will be
        used instead along with its associated context.

        If both queue and cl_env are speficied, context should match.
        Allocator can be used to override cl_env.memory_pool and should also match context.

        Note that allocation queue can be different from default array computation queue.
        """
        check_instance(cl_env, OpenClEnvironment, allow_none=True)
        check_instance(queue, cl.CommandQueue, allow_none=True)
        check_instance(allocator, OpenClAllocator, allow_none=True)

        # disable multi queue support at this time
        msg = (
            "Non-default queue support has been disabled. Please provide a cl_env only."
        )
        if cl_env is None:
            raise RuntimeError(msg)
        # if (queue is not None) and (queue is not cl_env.default_queue):
        # raise RuntimeError(msg)

        if queue is None:
            if cl_env is None:
                cl_env = get_or_create_opencl_env()
            context = cl_env.context
            queue = cl_env.default_queue
            allocator = allocator or cl_env.allocator
        elif cl_env is None:
            context = queue.context
            if allocator is None:
                msg = "A custom allocator should be given as input "
                msg += "(cl_env was not specified but queue was)."
                raise ValueError(msg)
        else:
            # cl_env and queue specified
            context = cl_env.context
            allocator = allocator or cl_env.allocator

        check_instance(allocator, OpenClAllocator)
        check_instance(context, cl.Context)
        check_instance(queue, cl.CommandQueue)

        if queue.context != context:
            msg = "Queue does not match context:"
            msg += f"\n  *Given context is {context}."
            msg += f"\n  *Command queue context is {queue.context}."
            raise ValueError(msg)
        if allocator.context != context:
            msg = "Allocator does not match context."
            msg += f"\n  *Given context is {context}."
            msg += f"\n  *Allocator context is {allocator.context}."
            raise ValueError(msg)

        assert len(context.devices) == 1, "Multidevice contexts are not supported."

        from hysop.core.arrays.all import HostArrayBackend, default_host_array_backend

        host_array_backend = host_array_backend or default_host_array_backend
        check_instance(host_array_backend, HostArrayBackend)

        super().__init__(allocator=allocator)
        self._context = context
        self._default_queue = queue
        self._host_array_backend = host_array_backend
        self._cl_env = cl_env

        from hysop.backend.device.opencl.opencl_elementwise import (
            OpenClElementwiseKernelGenerator,
        )

        self._kernel_generator = OpenClElementwiseKernelGenerator(cl_env=cl_env)

    def get_default_queue(self):
        return self._default_queue

    def get_context(self):
        return self._context

    def get_device(self):
        return self._context.devices[0]

    def get_cl_env(self):
        return self._cl_env

    def any_backend_from_kind(self, *kinds):
        for kind in kinds:
            if kind == self.kind:
                return self
        for kind in kinds:
            if kind == Backend.HOST:
                return self._host_array_backend
        return self.any_backend_from_kind(*kinds)

    def __str__(self):
        return f"platform {self._cl_env.platform.name.strip()} -- device {self._cl_env.name}"

    default_queue = property(get_default_queue)
    context = property(get_context)
    device = property(get_device)
    cl_env = property(get_cl_env)

    # BACKEND SPECIFIC METHODS #
    ############################

    def can_wrap(self, handle):
        """
        Should return True if handle is an Array or a array handle corresponding
        this backend.
        """
        from hysop.core.arrays.all import OpenClArray

        return isinstance(handle, (OpenClArray, clArray.Array))

    def wrap(self, handle):
        """
        Create an hysop.backend.device.OpenclArray from an pyopencl.array.Array instance.
        """
        from hysop.core.arrays.all import OpenClArray

        if isinstance(handle, OpenClArray):
            return handle

        check_instance(handle, clArray.Array)

        return OpenClArray(backend=self, handle=handle)

    def _arg(self, arg):
        """
        Prepare one argument for a call to pyopencl backend.
        """
        if isinstance(arg, QueuePolicy):
            if arg == QueuePolicy.COPY_QUEUE:
                return clArray._copy_queue
            elif arg == QueuePolicy.SAME_AS_TRANSFER:
                return clArray._same_as_transfer
            elif arg == QueuePolicy.NO_QUEUE:
                return None
            else:
                msg = f"Unknown queue policy {arg}."
                raise ValueError(msg)
        else:
            return super()._arg(arg)

    def copyto(self, dst, src, queue=None, synchronize=True, **kargs):
        """
        src is a OpenClArray
        dst can be everything
        """
        from hysop.core.arrays.all import OpenClArray, HostArray

        self._check_argtype("copyto", "src", src, OpenClArray)
        queue = first_not_None(queue, self._default_queue)

        if dst.size != src.size:
            raise ValueError("'dst' has non-matching size.")
        if dst.dtype != src.dtype:
            raise ValueError("'dst' has non-matching dtype.")
        if dst.nbytes != src.nbytes:
            raise ValueError("'dst' has non-matching nbytes.")
        if dst.size == 0:
            return

        if isinstance(dst, OpenClArray):
            from hysop.backend.device.opencl.opencl_copy_kernel_launchers import (
                OpenClCopyBufferRectLauncher,
            )

            kl = OpenClCopyBufferRectLauncher.from_slices("buffer", src=src, dst=dst)
            evt = kl(queue=queue)
        elif isinstance(dst, Array):
            (_, evt) = src.handle.get_async(queue=queue, ary=dst.handle)
        elif isinstance(dst, np.ndarray):
            (_, evt) = src.handle.get_async(queue=queue, ary=dst)
        else:
            msg = "Unknown type to copy to ({}) for array of type {}."
            msg = msg.format(dst.__class__.__name__, src.__class__.__name__)
            raise TypeError(msg)

        if synchronize:
            evt.wait()
        else:
            return evt

    # HELPER FUNCTIONS #
    ####################

    def format_kernel_arg(self, arg, argname, const=False, allow_none=False):
        from hysop.core.arrays.all import OpenClArray

        const = "const" if const else ""
        is_scalar = False
        if arg is None:
            if allow_none:
                pass
            else:
                msg = "Got None argument and allow_none is not set."
                raise ValueError(msg)
        elif np.isscalar(arg):
            dtype = get_dtype(arg)
            ctype = clTools.dtype_to_ctype(dtype)
            argument = f"{const} {ctype} {argname}"
            arg = np.asarray(arg, dtype=dtype)
            is_scalar = True
        elif isinstance(arg, OpenClArray):
            dtype = arg.dtype
            ctype = arg.ctype()
            argument = f"__global {ctype} {const}* {argname}"
        elif isinstance(arg, Array):
            msg = "Argument '{}' is an Array but not an OpenClArray "
            msg += "(size={}, shape={}, dtype={})."
            msg = msg.format(argname, arg.size, arg.shape, arg.dtype)
            raise TypeError(msg)
        elif isinstance(arg, np.ndarray):
            if arg.size == 1:
                dtype = arg.dtype
                ctype = clTools.dtype_to_ctype(arg.dtype)
                argument = f"{const} {ctype} {argname}"
                is_scalar = True
            else:
                msg = "Argument '{}' is a non-scalar np.ndarray (size={}, shape={}, dtype={})."
                msg = msg.format(argname, arg.size, arg.shape, arg.dtype)
                raise TypeError(msg)
        else:
            msg = f"Unknown argument type {arg.__class__} (value={arg})."
            raise TypeError(msg)
        return arg, argument, is_scalar, dtype

    def format_kernel_args(
        self,
        kargs,
        argprefix,
        argflags,
        argcast=None,
        dtype=None,
        is_output=False,
        arguments=None,
        const=False,
        filter_expr=None,
    ):

        if argflags is not None:
            assert "has_fp16" in argflags
            assert "has_fp64" in argflags
        if filter_expr is None:
            filter_expr = dict()
        else:
            self._check_argtype("format_kernel_args", "filter_expr", filter_expr, dict)

        if not isinstance(argcast, tuple):
            assert not isinstance(argcast, set) and not isinstance(argcast, list)
            argcast = (argcast,) * len(kargs)
        else:
            if len(argcast) != len(kargs):
                msg = "Allocation dtypes input length mismatch:"
                msg += "\n\tkargs: {}\n\targcast={}\n"
                msg = msg.format(kargs, argcast)
                raise ValueError(msg)

        # find common floating point type
        fdtypes = []
        for i, arg in enumerate(kargs):
            if (argcast[i] == "f") or (argcast[i] == "c"):
                cast_dtype = match_dtype(arg, argcast[i])
                if is_complex(cast_dtype):
                    fdtypes.append(complex_to_float_dtype(cast_dtype))
                else:
                    fdtypes.append(cast_dtype)
        if fdtypes:
            fcast_dtype = find_common_dtype(*tuple(fdtypes))
            # TODO fix np.float16 and np.longdouble support
            if fcast_dtype == np.float16:
                fcast_dtype = np.float32
            if fcast_dtype == np.longdouble:
                fcast_dtype = np.float64
        else:
            fcast_dtype = np.float64
        ccast_dtype = float_to_complex_dtype(fcast_dtype)
        del fdtypes

        if arguments is None:
            arguments = [
                None,
            ] * len(kargs)
        else:
            self._check_argtype("format_kernel_args", "arguments", arguments, list)
            if len(kargs) != len(arguments):
                msg = "Argument count mismatch:\n\tkargs: {}\n\targuments={}\n"
                msg = msg.format(kargs, arguments)
                raise ValueError(msg)

        kargs = list(kargs)
        for i, arg in enumerate(kargs):
            argname = f"{argprefix}{i}"
            arg, argument, is_scalar, dtype = self.format_kernel_arg(
                arg, argname=argname, const=const
            )
            kargs[i] = arg
            if arguments[i] is None:
                arguments[i] = argument
            if is_output:
                affectation = rf"{argname}\[i\]\s*=\s*([\s\S]*?)\s*$"
                affectation = re.compile(affectation)
            is_cast = argcast[i] is not None
            for k, v in filter_expr.items():
                expr = filter_expr[k]
                if expr is None:
                    continue
                argi = argname + "[i]"
                if is_output:  # cast to nearest output value dtype
                    if not is_complex(dtype):
                        ctype = clTools.dtype_to_ctype(dtype)
                        convert = f"{argi}=convert_{ctype}_rte(\\1)"
                        expr = expr.split(";")
                        for i, subexpr in enumerate(expr):
                            expr[i] = re.sub(affectation, convert, subexpr)
                        expr = ";".join(expr)
                elif is_cast:  # input variable should be cast
                    if (is_complex(dtype)) or (argcast[i] == "c"):
                        cast_dtype = ccast_dtype
                    elif (is_fp(dtype)) or (argcast[i] == "f"):
                        cast_dtype = fcast_dtype
                    else:
                        cast_dtype = match_dtype(dtype, argcast[i])
                    if cast_dtype != dtype:
                        if is_complex(cast_dtype):
                            ftype = complex_to_float_dtype(cast_dtype)
                            ctype = clTools.dtype_to_ctype(ftype)
                            if is_complex(dtype):
                                argc = "c{}_new({argi}.real,{argi}.imag)".format(
                                    ctype, argi=argi
                                )
                            else:
                                if dtype != ftype:
                                    argc = f"convert_{ctype}({argi})"
                                else:
                                    argc = argi
                                argc = f"c{ctype}_fromreal({argc})"
                            expr = expr.replace(argi, argc)
                        else:
                            ctype = clTools.dtype_to_ctype(cast_dtype)
                            argc = f"convert_{ctype}({argi})"
                            expr = expr.replace(argi, argc)
                if is_scalar:
                    expr = expr.replace(argi, argname)
                filter_expr[k] = expr
            if argflags is not None:
                if dtype == np.float16:
                    argflags["has_fp16"] = True
                elif (dtype == np.float64) or (dtype == np.complex128):
                    argflags["has_fp64"] = True
        kargs, _ = self._prepare_args(*kargs)
        return kargs, arguments

    @classmethod
    def complex_fn(cls, fname, handle):
        """
        Return corresponding complex function name from pyopencl-complex.h
        matching handle complex datatype.
        """
        # see pyopencl/cl/pyopencl-complex.h
        assert is_complex(handle)
        if fname not in [
            "real",
            "imag",
            "abs",
            "abs_squared",
            "new",
            "fromreal",
            "neg",
            "conj",
            "add",
            "addr",
            "radd",
            "sub",
            "mul",
            "mulr",
            "rdivide",
            "divide",
            "divider",
            "pow",
            "powr",
            "rpow",
            "sqrt",
            "exp",
            "log",
            "sin",
            "cos",
            "tan",
            "sinh",
            "cosh",
            "tanh",
        ]:
            msg = "The function '{}' has not been implemented for complex numbers, "
            msg += "see '{}/cl/pyopencl-complex.h' for more informations on "
            msg += "available methods."
            msg = msg.format(fname, os.path.dirname(cl.__file__))
            raise NotImplementedError(msg)

        dtype = get_dtype(handle)
        if dtype not in [np.complex64, np.complex128]:
            msg = "{} complex type has not been implemented yet."
            msg = msg.format(dtype)
            raise NotImplementedError(msg)
        ftype = complex_to_float_dtype(dtype)
        ctype = clTools.dtype_to_ctype(ftype)
        prefix = f"c{ctype}_"

        return prefix + fname

    @classmethod
    def binary_complex_fn(cls, fname, x0, x1):
        assert is_complex(x0) or is_complex(x1)
        if not is_complex(x0):
            fname = "r" + fname
        elif not is_complex(x1):
            fname = fname + "r"

        # rsub and subr do not exist in complex header
        if fname == "subr":
            operands_prefix = ("", "-")
        elif fname == "rsub":
            operands_prefix = ("", cls.complex_fn("neg", x1))
        else:
            operands_prefix = ("", "")

        ftype = find_common_dtype(x0, x1)
        cmplx_fname = cls.complex_fn(fname, ftype)

        expr = "{}({}(x0[i]), {}(x1[i]))"
        expr = expr.format(cmplx_fname, *operands_prefix)

        # fix 0^0 and 0^x
        if fname.find("pow") >= 0:
            default = expr
            expr0 = "{}(1)".format(cls.complex_fn("fromreal", ftype))
            expr1 = "{}(0)".format(cls.complex_fn("fromreal", ftype))
            expr2 = "{}(NAN,NAN)".format(cls.complex_fn("new", ftype))
            if fname == "rpow":
                cond0 = "(x0[i]==0)"
                cond1 = "((x1[i].real==0) && (x1[i].imag==0))"
            elif fname == "powr":
                cond0 = "((x0[i].real==0) && (x0[i].imag==0))"
                cond1 = "(x1[i]==0)"
            elif fname == "pow":
                cond0 = "((x0[i].real==0) && (x0[i].imag==0))"
                cond1 = "(x1[i].imag==0)"
                cond2 = "(x1[i].real==0)"
            expr = "({cond0} ? ({cond1} ? ({cond2} ? {expr0} : {expr1}) : {expr2}) : {default})".format(
                cond0=cond0,
                cond1=cond1,
                cond2=cond2,
                expr0=expr0,
                expr1=expr1,
                expr2=expr2,
                default=default,
            )

        expr = f"y0[i]={expr}"
        return expr

    def elementwise(
        self,
        ikargs,
        okargs,
        extra_kargs=None,
        input_arguments=None,
        output_arguments=None,
        extra_arguments=None,
        filter_expr=None,
        dtype=None,
        infer_dtype=False,
        queue=None,
        infer_queue=False,
        synchronize=True,
        convert_inputs=None,
        allocator=None,
        alloc_shapes=None,
        alloc_dtypes=None,
        Kernel=_ElementwiseKernel,
        kernel_build_kwargs=None,
        kernel_call_kwargs=None,
        build_kernel_launcher=False,
    ):
        """
        Build and call kernel that takes:
                 n read-only scalars or OpenClArray as input arguments xi,
                 m OpenClArray as output arguments yi,
                 k read-only parameters pi
        and performs an operation specified as a snippet of C99 on these arguments,
        depending on passed Kernel class and additional arguments passed
        in kernel_build_kwargs and kernel_call_kwargs.

        This method performs:
            y0[i],...,ym[i] = f( x0[i],...,xn[i] ; p0,...,pk )

            1) outputs allocation
            2) expression and arguments preprocessing
            3) kernel creation and compilation:
                kernel = Kernel(**kernel_build_kwargs)
            4) kernel call:
                return kernel(**kernel_call_kwargs)

            The OpenCL kernel signature should match something like this:
                    kernel_name(*y0,...,*ym, const *x0,...,const *xn, const p0, ... const pk)

        Kernel arguments:
            For all input and output arguments, if they are OpenClArray, all shapes
            and order should match or an error is raised.

            If one of the output arg is set to None, it is allocated by the
            given allocator, dtype and queue with the global shape and order.

            Allocation shape can be overidden by using the argument 'alloc_shapes' which
            should be a list of the same size as okargs (output arguments) or None.
            If alloc_shapes is not None at allocated variable index, this shape is used
            instead of the common operand shape.

            If infer_dtype is set to True and dtype is None, every None output argument will
            be allocated with the first OpenClArray input dtype contained in ikargs (input
            arguments). If there is no input OpenClArray the lookup is extended to okargs
            (output arguments). This can be overhidden using alloc_dtypes witch should be
            a list of size of the output argument.

            Default arguments names (generated in opencl kernel) are:
                x0,x1,...,xn for inputs     (     ikargs -> input_arguments)
                y0,y1,...,ym for outputs    (     okargs -> output_arguments)
                p0,p1,...,pk for parameters (extra_kargs -> extra_arguments)
            input arguments and extra arguments are read-only.

            input  arguments (x0,...,xn) can be scalars or OpenClArrays.
            output arguments (y0,...,ym) can only be OpenClArrays.
            extra  arguments (p0,...,pk) can be anything compatible with pyopencl.

        Expression filtering:
            filter_expr is the dictionary of expr_name (str) -> expression (str)
            Expressions in filter_expr are filtered according the following rule:
                If input xi is a scalar, elementwise access 'x[i]' is replaced by 'x'
                in given 'expr'.
            This allows broadcasting of scalars in operators.

            Filtered expressions are added to kernel_build_kwargs entries prior
            to kernel construction. This implies that kernel_build_kwargs keys and
            filter_expr keys should not clash.

        Kernel build and kernel call:
            kernel creation:
                knl = Kernel(**kernel_build_kwargs)
            kernel call:
                knl(**kernel_call_kwargs)

            User supplied 'kernel_build_kwargs' are completed with:
                - all filtered expressions contained in dictionnary 'filter_expr'
                - kernel_build_kwargs['context']   = context corresponding to queue
                - kernel_build_kwargs['arguments'] = preprocessed arguments
                                                            (output+input+params)
                - kernel_build_kwargs['dtype']     = dtype

            User supplied 'kernel_call_kwargs' are completed with:
                - kernel_call_kwargs['queue'] = queue
                - kernel_call_kwargs['args']  = kargs (okargs+ikargs+extra_kargs), preprocessed.
                - kernel_call_kwargs['iargs'] = ikargs, preprocessed
                - kernel_call_kwargs['oargs'] = okargs, preprocessed, allocated if necessary
                - kernel_call_kwargs['pargs'] = extra_kargs, preprocessed

                Preprocessed arguments are passed through self._prepare_args(...)

            If there is a key clash in those dictionaries, the method will fail.

        Queue priorities:
             queue (function argument),
             if infer_queue is set to True:
                 x0.default_queue, ..., xn.default_queue
                 y0.default_queue, ..., ym.default_queue
             /!\ default_cl_queue is not used in this function.

        Call and events:
            wait_for specify a list a event to wait prior applying the kernel.

            If synchronize is set, this is a blocking opencl call and this functions returns
            output arguments okargs as a tuple unless there is only one output.

            If synchronize not set, this is a non blocking call and an event is returned in
            addition to the outputs as last argument.

            The returned event may not be used for profiling purposes, because it only
            covers the last effective kernel call (reductions may use two kernels
            for example).
        """
        from hysop.core.arrays.all import OpenClArray

        extra_kargs = first_not_None(extra_kargs, ())
        kernel_build_kwargs = first_not_None(kernel_build_kwargs, {})
        kernel_call_kwargs = first_not_None(kernel_call_kwargs, {})

        self._check_argtype("elementwise", "ikargs", ikargs, tuple)
        self._check_argtype("elementwise", "okargs", okargs, tuple)
        self._check_argtype("elementwise", "extra_kargs", extra_kargs, tuple)
        self._check_argtype(
            "elementwise", "kernel_build_kwargs", kernel_build_kwargs, dict
        )
        self._check_argtype(
            "elementwise", "kernel_call_kwargs", kernel_call_kwargs, dict
        )

        if alloc_dtypes is None:
            alloc_dtypes = (None,) * len(okargs)
        elif isinstance(alloc_dtypes, str):
            alloc_dtypes = (alloc_dtypes,) * len(okargs)
        else:
            self._check_argtype("elementwise", "alloc_dtype", alloc_dtypes, tuple)
            if len(alloc_dtypes) != len(okargs):
                msg = "Allocation dtypes input length mismatch:"
                msg += "\n\tokargs: {}\n\talloc_dtypes={}\n"
                msg = msg.format(okargs, alloc_dtypes)
                raise ValueError(msg)

        alloc_shapes = alloc_shapes or (None,) * len(okargs)
        self._check_argtype("elementwise", "alloc_shapes", alloc_shapes, tuple)

        order = None
        shape = None
        for arg in ikargs + okargs:
            if isinstance(arg, OpenClArray):
                if infer_queue:
                    queue = first_not_None(queue, arg.default_queue)
                if infer_dtype:
                    dtype = first_not_None(dtype, arg.dtype)
                if order is None:
                    shape = arg.shape
                    order = arg.order
                elif arg.shape != shape:
                    msg = "{} with shape {} does not match reference shape {}."
                    msg = msg.format(arg, args.shape, shape)
                elif arg.order != order:
                    msg = "{} with order {} does not match reference order {}."
                    msg = msg.format(arg, args.order, order)
        if shape is None:
            msg = "No OpenClArray argument was found in ikargs and okargs."
            raise ValueError(msg)
        if queue is None:
            msg = "Queue has not been specified."
            raise ValueError(msg)

        alloc_dtypes = tuple(
            match_dtype(dtype, alloc_dtypes[i]) for i in range(len(okargs))
        )

        okargs = list(okargs)
        for i, (arg, _shape, _dtype) in enumerate(
            zip(okargs, alloc_shapes, alloc_dtypes)
        ):
            if arg is None:
                alloc_shape = first_not_None(_shape, shape)
                alloc_dtype = first_not_None(_dtype, dtype)
                if alloc_dtype is None:
                    msg = "Output argument y{} has not been allocated and dtype was"
                    msg += " not set (dtype={}, infer_dtype={}, alloc_dtypes={})."
                    msg = msg.format(i, dtype, infer_dtype, alloc_dtypes)
                    raise ValueError(msg)
                if order is None:
                    msg = "Output argument y{} has not been allocated and order was"
                    msg += " not set."
                    msg = msg.format(i)
                    raise ValueError(msg)
                okargs[i] = self.empty(
                    shape=alloc_shape, dtype=alloc_dtype, queue=queue, order=order
                )
        okargs = tuple(okargs)

        argflags = {
            "has_fp16": (dtype == np.float16),
            "has_fp64": (dtype == np.float64),
        }

        ikargs, input_arguments = self.format_kernel_args(
            kargs=ikargs,
            argprefix="x",
            is_output=False,
            arguments=input_arguments,
            filter_expr=filter_expr,
            const=True,
            argflags=argflags,
            argcast=convert_inputs,
            dtype=dtype,
        )

        okargs, output_arguments = self.format_kernel_args(
            kargs=okargs,
            argprefix="y",
            is_output=True,
            arguments=output_arguments,
            filter_expr=filter_expr,
            const=False,
            argflags=argflags,
        )

        extra_kargs, extra_arguments = self.format_kernel_args(
            kargs=extra_kargs,
            argprefix="p",
            is_output=False,
            arguments=extra_arguments,
            const=True,
            argflags=argflags,
        )

        kargs = okargs + ikargs + extra_kargs
        arguments = output_arguments + input_arguments + extra_arguments
        arguments = ", ".join(arguments)

        # kernel build keyword arguments
        assert not set(kernel_build_kwargs.keys()).intersection(filter_expr.keys())
        kernel_build_kwargs.update(filter_expr)

        blacklist = ["context", "arguments", "dtype", "argflags"]
        assert not set(kernel_build_kwargs.keys()).intersection(blacklist)

        kernel_build_kwargs["context"] = queue.context
        kernel_build_kwargs["arguments"] = arguments
        kernel_build_kwargs["dtype"] = dtype
        kernel_build_kwargs["argflags"] = argflags

        # kernel call keyword arguments
        blacklist = ["queue", "args", "iargs", "oargs", "pargs"]
        assert not set(kernel_call_kwargs.keys()).intersection(blacklist)
        kernel_call_kwargs["queue"] = queue
        kernel_call_kwargs["args"] = kargs
        kernel_call_kwargs["iargs"] = ikargs
        kernel_call_kwargs["oargs"] = okargs
        kernel_call_kwargs["pargs"] = extra_kargs

        try:
            knl = Kernel(**kernel_build_kwargs)
            ret = knl(**kernel_call_kwargs)
        except:

            def val2str(val):
                if val.__class__ in [list, tuple, set]:
                    out = "{}[{}]".format(
                        val.__class__.__name__, ", ".join([val2str(v) for v in val])
                    )
                elif isinstance(val, Array):
                    out = "{}(shape={}, dtype={}, order={}, strides={}, offset={})"
                    out = out.format(
                        val.__class__.__name__,
                        val.shape,
                        val.dtype,
                        val.order,
                        val.strides,
                        val.offset,
                    )
                elif val.__class__ in self._registered_backends().keys():
                    out = "{}(shape={}, dtype={}, strides={})"
                    out = out.format(
                        val.__class__.__name__, val.shape, val.dtype, val.strides
                    )
                else:
                    out = val
                return out

            def dic2str(dic):
                entries = []
                for k, v in dic.items():
                    e = f"{k}: {val2str(v)}"
                    entries.append(e)
                return "\n\t" + "\n\t".join(entries)

            msg = "Something went wrong during kernel build or kernel call:"
            msg += f"\n  build kargs:{dic2str(kernel_build_kwargs)}"
            msg += f"\n  call kargs:{dic2str(kernel_call_kwargs)}"
            msg += "\n"
            print(msg)
            raise

        if build_kernel_launcher:
            return knl.to_kernel_launcher(
                name=kernel_build_kwargs["name"], **kernel_call_kwargs
            )

        if isinstance(ret, tuple):
            evt = ret[-1]
        else:
            evt = ret

        okargs = self._return(okargs)

        if synchronize:
            evt.wait()
            if len(okargs) == 1:
                return okargs[0]
            else:
                return okargs
        else:
            if len(okargs) == 1:
                return okargs[0], evt
            else:
                return okargs, evt

    def nary_op(
        self,
        ikargs,
        okargs,
        operation,
        extra_kargs=None,
        extra_arguments=None,
        input_arguments=None,
        output_arguments=None,
        dtype=None,
        infer_dtype=True,
        queue=None,
        infer_queue=True,
        allocator=None,
        alloc_shapes=None,
        alloc_dtypes=None,
        synchronize=True,
        wait_for=None,
        convert_inputs=None,
        build_kernel_launcher=False,
        name="nary_op",
        options=[],
        preamble="",
    ):
        """
        A kernel that takes n vector as input and outputs m vectors.
        The operation is specified as a snippet of C99 on these arguments.
        Default arguments names are x0,x1,...,xn and y0,y1,...,ym.

        Queue priorities: queue (function argument),
                          if infer_queue:
                              x0.default_queue, ..., xn.default_queue,
                              y0.default_queue, ..., yn.default_queue,

        For all input and output arguments, if they are OpenClArray, all shapes should match.
        If out is None, it is allocated to input shape with specified dtype.

        y0[i] = f( x0[i],...,xn[i] ; p0,...,pk )
        <=>
        y0[i] = ElementwiseKernel(const x0[i],...,const xn[i], const p0,..., const pk)

        If synchronize is set to false, evt is returned as last argument.
        If out and dtype is None it is set to xi.dtype where xi is the first OpenClArray

        options are opencl kernel build options.
        preamble is appended unmodified in source code before kernel declaration.
        """
        Kernel = _ElementwiseKernel
        kernel_build_kwargs = {"options": options, "preamble": preamble, "name": name}
        kernel_call_kwargs = {"wait_for": wait_for}
        filter_expr = {"operation": operation}
        return self.elementwise(
            ikargs=ikargs,
            okargs=okargs,
            extra_kargs=extra_kargs,
            extra_arguments=extra_arguments,
            input_arguments=input_arguments,
            output_arguments=output_arguments,
            filter_expr=filter_expr,
            dtype=dtype,
            infer_dtype=infer_dtype,
            queue=queue,
            infer_queue=infer_queue,
            synchronize=synchronize,
            allocator=allocator,
            convert_inputs=convert_inputs,
            alloc_shapes=alloc_shapes,
            alloc_dtypes=alloc_dtypes,
            Kernel=Kernel,
            kernel_build_kwargs=kernel_build_kwargs,
            kernel_call_kwargs=kernel_call_kwargs,
            build_kernel_launcher=build_kernel_launcher,
        )

    def unary_op(
        self,
        x0,
        expr,
        out=None,
        extra_kargs=None,
        extra_arguments=None,
        input_arguments=None,
        output_arguments=None,
        dtype=None,
        infer_dtype=True,
        queue=None,
        infer_queue=True,
        convert_inputs=None,
        allocator=None,
        alloc_shapes=None,
        alloc_dtypes=None,
        synchronize=True,
        wait_for=None,
        build_kernel_launcher=False,
        name="unary_op",
        options=[],
        preamble="",
    ):

        return self.nary_op(
            ikargs=(x0,),
            okargs=(out,),
            operation=expr,
            extra_kargs=extra_kargs,
            extra_arguments=extra_arguments,
            input_arguments=input_arguments,
            output_arguments=output_arguments,
            dtype=dtype,
            infer_dtype=infer_dtype,
            queue=queue,
            infer_queue=infer_queue,
            allocator=allocator,
            convert_inputs=convert_inputs,
            alloc_shapes=alloc_shapes,
            alloc_dtypes=alloc_dtypes,
            synchronize=synchronize,
            wait_for=wait_for,
            build_kernel_launcher=build_kernel_launcher,
            name=name,
            options=options,
            preamble=preamble,
        )

    def binary_op(
        self,
        x0,
        x1,
        expr,
        out=None,
        extra_kargs=None,
        extra_arguments=None,
        input_arguments=None,
        output_arguments=None,
        dtype=None,
        infer_dtype=True,
        queue=None,
        infer_queue=True,
        convert_inputs=None,
        allocator=None,
        alloc_shapes=None,
        alloc_dtypes=None,
        synchronize=True,
        wait_for=None,
        build_kernel_launcher=False,
        name="binary_op",
        options=[],
        preamble="",
    ):

        return self.nary_op(
            ikargs=(x0, x1),
            okargs=(out,),
            operation=expr,
            extra_kargs=extra_kargs,
            extra_arguments=extra_arguments,
            input_arguments=input_arguments,
            output_arguments=output_arguments,
            dtype=dtype,
            infer_dtype=infer_dtype,
            queue=queue,
            infer_queue=infer_queue,
            allocator=allocator,
            convert_inputs=convert_inputs,
            alloc_shapes=alloc_shapes,
            alloc_dtypes=alloc_dtypes,
            synchronize=synchronize,
            wait_for=wait_for,
            build_kernel_launcher=build_kernel_launcher,
            name=name,
            options=options,
            preamble=preamble,
        )

    def reduce(
        self,
        kargs,
        neutral,
        reduce_expr,
        map_expr="x0[i]",
        axis=None,
        out=None,
        extra_kargs=None,
        extra_arguments=None,
        input_arguments=None,
        output_arguments=None,
        convert_inputs=None,
        alloc_dtypes=None,
        dtype=None,
        infer_dtype=True,
        queue=None,
        infer_queue=True,
        allocator=None,
        synchronize=True,
        wait_for=None,
        build_kernel_launcher=False,
        name="reduction",
        options=[],
        preamble="",
    ):
        """
        Reduce arrays elements over the whole array.
        kargs = kernel array or scalar arguments (tuple)
        y0 = ReductionKernel( MAP_EXPR(const x0[i],...,const xn[i], const p0,..., const pk) )
        """
        from hysop.core.arrays.all import OpenClArray

        if dtype is None:
            if any([is_signed(k) for k in kargs]):
                dtype = np.int64
            elif any([is_unsigned(k) for k in kargs]):
                dtype = np.uint64
            else:
                pass

        map_expr = map_expr.strip()
        reduce_expr = reduce_expr.strip()

        input_dtype = find_common_dtype(*kargs)
        if is_complex(input_dtype):
            map_ops = {
                "a+b": "add",
                "a-b": "sub",
                "a*b": "mul",
            }
            if reduce_expr in map_ops:
                neutral = "{}({})".format(
                    self.complex_fn("fromreal", input_dtype), neutral
                )
                reduce_expr = (
                    f"{self.complex_fn(map_ops[reduce_expr], input_dtype)}(a,b)"
                )

        self._unsupported_argument("reduce", "axis", axis)
        if out is not None:
            self._check_argtype("reduce", "out", out, OpenClArray)
            dtype = out.dtype
            assert out.size == 1
            out = out.handle

        Kernel = _ReductionKernel
        kernel_build_kwargs = {
            "name": name,
            "options": options,
            "preamble": preamble,
            "reduce_expr": reduce_expr,
            "neutral": neutral,
        }
        kernel_call_kwargs = {
            "return_event": True,
            "wait_for": wait_for,
        }
        filter_expr = {"map_expr": map_expr}
        return self.elementwise(
            ikargs=kargs,
            okargs=(out,),
            extra_kargs=extra_kargs,
            extra_arguments=extra_arguments,
            input_arguments=input_arguments,
            output_arguments=output_arguments,
            filter_expr=filter_expr,
            dtype=dtype,
            infer_dtype=infer_dtype,
            queue=queue,
            infer_queue=infer_queue,
            synchronize=synchronize,
            convert_inputs=convert_inputs,
            alloc_dtypes=alloc_dtypes,
            allocator=allocator,
            alloc_shapes=((1,),),
            Kernel=Kernel,
            build_kernel_launcher=build_kernel_launcher,
            kernel_build_kwargs=kernel_build_kwargs,
            kernel_call_kwargs=kernel_call_kwargs,
        )

    def nanreduce(
        self,
        kargs,
        neutral,
        reduce_expr,
        map_expr="x0[i]",
        axis=None,
        out=None,
        extra_kargs=None,
        extra_arguments=None,
        input_arguments=None,
        output_arguments=None,
        dtype=None,
        infer_dtype=True,
        queue=None,
        infer_queue=True,
        allocator=None,
        convert_inputs=None,
        synchronize=True,
        wait_for=None,
        build_kernel_launcher=False,
        name="nan_reduction",
        options=[],
        preamble="",
    ):
        """
        Reduce arrays elements over the whole array, replacing mapped expr NaNs by the neutral.
        dtype should be a floating point type.
        kargs = kernel args as a scalar or tuple
        """
        if any([is_complex(k) for k in kargs]):
            ctype = find_common_dtype(*kargs)
            nan_map_expr = (
                "((isnan({expr}.real) || isnan({expr}.imag)) ? {neutral} : {expr})"
            )
            nan_map_expr = nan_map_expr.format(
                expr=map_expr,
                neutral="{}({})".format(self.complex_fn("fromreal", ctype), neutral),
            )
        elif any([is_fp(k) for k in kargs]):
            nan_map_expr = "(isnan({expr}) ? {neutral} : {expr})"
            nan_map_expr = nan_map_expr.format(expr=map_expr, neutral=neutral)
        else:
            nan_map_expr = map_expr

        return self.reduce(
            kargs=kargs,
            neutral=neutral,
            convert_inputs=convert_inputs,
            reduce_expr=reduce_expr,
            map_expr=nan_map_expr,
            axis=axis,
            out=out,
            extra_kargs=extra_kargs,
            extra_arguments=extra_arguments,
            input_arguments=input_arguments,
            output_arguments=output_arguments,
            dtype=dtype,
            infer_dtype=infer_dtype,
            queue=queue,
            infer_queue=infer_queue,
            allocator=allocator,
            synchronize=synchronize,
            wait_for=wait_for,
            build_kernel_launcher=build_kernel_launcher,
            name=name,
            options=options,
            preamble=preamble,
        )

    def generic_scan(
        self,
        kargs,
        neutral,
        scan_expr,
        output_statement,
        input_expr="x0[i]",
        size=None,
        is_segment_start_expr=None,
        input_fetch_exprs=None,
        axis=None,
        out=None,
        extra_kargs=None,
        extra_arguments=None,
        input_arguments=None,
        output_arguments=None,
        dtype=None,
        infer_dtype=True,
        queue=None,
        infer_queue=True,
        allocator=None,
        convert_inputs=None,
        synchronize=True,
        wait_for=None,
        build_kernel_launcher=False,
        name="generic_scan",
        options=[],
        preamble="",
    ):
        """
        Scan arrays elements over the whole array.
        kargs = kernel array or scalar arguments (tuple)
        """
        from hysop.core.arrays.all import OpenClArray

        self._unsupported_argument("generic_scan", "axis", axis)

        if dtype is None:
            if any([is_signed(k) for k in kargs]):
                dtype = np.int64
            elif any([is_unsigned(k) for k in kargs]):
                dtype = np.uint64
            # numpy does not force np.float16 / np.float32 to np.float64

        scan_expr = scan_expr.strip()
        input_dtype = find_common_dtype(*kargs)
        if is_complex(input_dtype):
            if input_dtype == np.complex128:
                define = "#define PYOPENCL_DEFINE_CDOUBLE"
            else:
                define = ""
            include = "#include <pyopencl-complex.h>\n" + preamble
            preamble = f"{define}\n{include}\n{preamble}"

            map_ops = {
                "a+b": "add",
                "a-b": "sub",
                "a*b": "mul",
            }
            if scan_expr in map_ops:
                neutral = "{}({})".format(
                    self.complex_fn("fromreal", input_dtype), neutral
                )
                scan_expr = f"{self.complex_fn(map_ops[scan_expr], input_dtype)}(a,b)"

        Kernel = _GenericScanKernel
        kernel_build_kwargs = {
            "name": name,
            "options": options,
            "preamble": preamble,
            "scan_expr": scan_expr,
            "neutral": neutral,
            "input_fetch_exprs": input_fetch_exprs,
        }

        kernel_call_kwargs = {"wait_for": wait_for, "size": size}
        filter_expr = {
            "input_expr": input_expr,
            "output_statement": output_statement,
            "is_segment_start_expr": is_segment_start_expr,
        }
        return self.elementwise(
            ikargs=kargs,
            okargs=(out,),
            extra_kargs=extra_kargs,
            extra_arguments=extra_arguments,
            input_arguments=input_arguments,
            output_arguments=output_arguments,
            filter_expr=filter_expr,
            dtype=dtype,
            infer_dtype=infer_dtype,
            queue=queue,
            infer_queue=infer_queue,
            synchronize=synchronize,
            allocator=allocator,
            alloc_shapes=None,
            convert_inputs=convert_inputs,
            Kernel=Kernel,
            build_kernel_launcher=build_kernel_launcher,
            kernel_build_kwargs=kernel_build_kwargs,
            kernel_call_kwargs=kernel_call_kwargs,
        )

    def inclusive_scan(
        self,
        kargs,
        neutral,
        scan_expr,
        input_expr="x0[i]",
        size=None,
        axis=None,
        out=None,
        extra_kargs=None,
        extra_arguments=None,
        input_arguments=None,
        dtype=None,
        infer_dtype=True,
        queue=None,
        infer_queue=True,
        allocator=None,
        convert_inputs=None,
        synchronize=True,
        wait_for=None,
        build_kernel_launcher=False,
        name="inclusive_scan",
        options=[],
        preamble="",
    ):

        output_arguments = None
        output_statement = "y0[i] = item"

        return self.generic_scan(
            kargs=kargs,
            neutral=neutral,
            scan_expr=scan_expr,
            input_expr=input_expr,
            output_statement=output_statement,
            size=size,
            axis=axis,
            out=out,
            extra_kargs=extra_kargs,
            extra_arguments=extra_arguments,
            input_arguments=input_arguments,
            output_arguments=output_arguments,
            dtype=dtype,
            infer_dtype=infer_dtype,
            queue=queue,
            infer_queue=infer_queue,
            allocator=allocator,
            convert_inputs=convert_inputs,
            synchronize=synchronize,
            wait_for=wait_for,
            build_kernel_launcher=build_kernel_launcher,
            name=name,
            options=options,
            preamble=preamble,
        )

    def exclusive_scan(
        self,
        kargs,
        neutral,
        scan_expr,
        input_expr="x0[i]",
        size=None,
        axis=None,
        out=None,
        extra_kargs=None,
        extra_arguments=None,
        input_arguments=None,
        dtype=None,
        infer_dtype=True,
        queue=None,
        infer_queue=True,
        allocator=None,
        synchronize=True,
        wait_for=None,
        build_kernel_launcher=False,
        name="exclusive_scan",
        options=[],
        preamble="",
    ):

        output_arguments = None
        output_statement = "y0[i] = prev_item"

        return self.generic_scan(
            ikargs=kargs,
            neutral=neutral,
            scan_expr=scan_expr,
            input_expr=input_expr,
            output_statement=output_statement,
            size=size,
            axis=axis,
            out=out,
            extra_kargs=extra_kargs,
            extra_arguments=extra_arguments,
            input_arguments=input_arguments,
            output_arguments=output_arguments,
            dtype=dtype,
            infer_dtype=infer_dtype,
            queue=queue,
            infer_queue=infer_queue,
            allocator=allocator,
            synchronize=synchronize,
            wait_for=wait_for,
            build_kernel_launcher=build_kernel_launcher,
            name=name,
            options=options,
            preamble=preamble,
        )

    def inclusive_nanscan(
        self,
        kargs,
        neutral,
        scan_expr,
        input_expr="x0[i]",
        size=None,
        axis=None,
        out=None,
        extra_kargs=None,
        extra_arguments=None,
        input_arguments=None,
        dtype=None,
        infer_dtype=True,
        queue=None,
        infer_queue=True,
        allocator=None,
        synchronize=True,
        wait_for=None,
        build_kernel_launcher=False,
        name="inclusive_nanscan",
        options=[],
        preamble="",
    ):

        if any([is_complex(k) for k in kargs]):
            ctype = find_common_dtype(*kargs)
            nan_input_expr = (
                "((isnan({expr}.real) || isnan({expr}.imag)) ? {neutral} : {expr})"
            )
            nan_input_expr = nan_input_expr.format(
                expr=input_expr,
                neutral="{}({})".format(self.complex_fn("fromreal", ctype), neutral),
            )
        elif any([is_fp(k) for k in kargs]):
            nan_input_expr = "(isnan({expr}) ? {neutral} : {expr})"
            nan_input_expr = nan_input_expr.format(expr=input_expr, neutral=neutral)
        else:
            nan_input_expr = input_expr

        return self.inclusive_scan(
            kargs=kargs,
            neutral=neutral,
            scan_expr=scan_expr,
            input_expr=nan_input_expr,
            size=size,
            axis=axis,
            out=out,
            extra_kargs=extra_kargs,
            extra_arguments=extra_arguments,
            input_arguments=input_arguments,
            dtype=dtype,
            infer_dtype=infer_dtype,
            queue=queue,
            infer_queue=infer_queue,
            allocator=allocator,
            synchronize=synchronize,
            wait_for=wait_for,
            build_kernel_launcher=build_kernel_launcher,
            name=name,
            options=options,
            preamble=preamble,
        )

    def exclusive_nanscan(
        self,
        kargs,
        neutral,
        scan_expr,
        input_expr="x0[i]",
        size=None,
        axis=None,
        out=None,
        extra_kargs=None,
        extra_arguments=None,
        input_arguments=None,
        dtype=None,
        infer_dtype=True,
        queue=None,
        infer_queue=True,
        allocator=None,
        synchronize=True,
        wait_for=None,
        build_kernel_launcher=False,
        name="exclusive_nanscan",
        options=[],
        preamble="",
    ):

        if any([is_complex(k) for k in kargs]):
            ctype = find_common_dtype(*kargs)
            nan_input_expr = (
                "((isnan({expr}.real) || isnan({expr}.imag)) ? {neutral} : {expr})"
            )
            nan_input_expr = nan_input_expr.format(
                expr=input_expr,
                neutral="{}({})".format(self.complex_fn("fromreal", ctype), neutral),
            )
        elif any([is_fp(k) for k in kargs]):
            nan_input_expr = "(isnan({expr}) ? {neutral} : {expr})"
            nan_input_expr = nan_input_expr.format(expr=input_expr, neutral=neutral)
        else:
            nan_input_expr = input_expr

        return self.exclusive_scan(
            kargs=kargs,
            neutral=neutral,
            scan_expr=scan_expr,
            input_expr=nan_input_expr,
            size=size,
            axis=axis,
            out=out,
            extra_kargs=extra_kargs,
            extra_arguments=extra_arguments,
            input_arguments=input_arguments,
            dtype=dtype,
            infer_dtype=infer_dtype,
            queue=queue,
            infer_queue=infer_queue,
            allocator=allocator,
            synchronize=synchronize,
            wait_for=wait_for,
            build_kernel_launcher=build_kernel_launcher,
            name=name,
            options=options,
            preamble=preamble,
        )

    def check_queue(self, queue):
        if queue is None:
            return self.default_queue
        if queue.context != self.context:
            msg = "Given queue does not match backend context."
            raise RuntimeError(msg)
        return queue

    ###########################
    # ARRAY CREATION ROUTINES #
    def array(
        self,
        shape,
        dtype=HYSOP_REAL,
        order=default_order,
        queue=None,
        min_alignment=None,
        buf=None,
        offset=0,
        strides=None,
        events=None,
    ):
        """
        Create an OpenClArray, see pyopencl.array.Array constructor.
        If a queue is specified, it is set as default queue,
        else it will be backend.default_queue.
        """
        # FIXME OpenCL half float support
        if dtype == np.float16:
            dtype = np.float32
            msg = "OpenClArrayBackend promoted float16 to float32 in array allocation of shape {}."
            msg = msg.format(shape)
            warnings.warn(RuntimeWarning(msg))
        # FIXME OpenCL long double support
        if dtype == np.longdouble:
            msg = "OpenClArrayBackend demoted {} to float64 in array allocation of shape {}."
            msg = msg.format(dtype, shape)
            dtype = np.float64
            warnings.warn(RuntimeWarning(msg))
        # FIXME OpenCL bool support
        if dtype == np.bool_:
            dtype = HYSOP_BOOL
            msg = "OpenClArrayBackend promoted np.bool_ to {} in array allocation of shape {}."
            msg = msg.format(dtype, shape)
            warnings.warn(RuntimeWarning(msg))

        shape = to_tuple(shape)

        # at this time the opencl backend works only with the default_queue
        # so we enforce it by not binding any Array to any queue.
        if (queue is not None) and (queue is not self.default_queue):
            msg = "pyopencl.Array has been created with non-default queue."
            raise RuntimeError(msg)
        queue = self.check_queue(queue)
        cq = queue  # force default queue

        if buf is None:
            assert offset == 0
            assert strides is None
            allocator = self.allocator
            if (min_alignment is None) or (
                min_alignment < self.allocator.device.mem_base_addr_align
            ):
                alignment = 1
                dtype = np.dtype(dtype)
                size = int(prod(shape) * dtype.itemsize)
                # prod( shape=(,) ) will return 1.0, so we cast to int for scalars
            else:
                (size, nbytes, alignment) = self.get_alignment_and_size(
                    shape=shape, dtype=dtype, min_alignment=min_alignment
                )
            buf = allocator.allocate_aligned(size, alignment=alignment)

        handle = self._call(
            clArray.Array,
            cq=cq,
            shape=shape,
            dtype=dtype,
            order=order,
            allocator=None,
            data=buf,
            offset=offset,
            strides=strides,
            events=events,
        )
        array = self.wrap(handle)
        return array

    def asarray(
        self,
        a,
        queue=None,
        synchronize=True,
        dtype=None,
        order=default_order,
        array_queue=QueuePolicy.SAME_AS_TRANSFER,
    ):
        """
        Convert the input to an OpenClArray.
        Queue is set as default queue.
        """
        from hysop.backend.device.opencl.opencl_array import Array, OpenClArray
        from hysop.backend.host.host_array import HostArray

        acls = a.__class__.__name__
        queue = self.check_queue(queue)

        if dtype is not None:
            pass
        if isinstance(a, Array):
            dtype = a.dtype
        elif hasattr(a, "dtype"):
            dtype = a.dtype
        else:
            msg = "Could not extract dtype from type {} and argument not set (dtype)."
            msg = msg.format(acls)
            raise ValueError(msg)

        if isinstance(a, OpenClArray):
            array = a
        elif isinstance(a, clArray.Array):
            array = self.wrap(handle=handle)
        elif a.__class__ in [list, tuple, set, np.ndarray, HostArray]:
            if a.__class__ in [list, tuple, set]:
                a = np.asarray(a)
            elif isinstance(a, HostArray):
                a = a.handle

            array = self._call(
                clArray.to_device,
                queue=queue,
                ary=a,
                async_=(not synchronize),
                array_queue=array_queue,
            )
        else:
            msg = "Unknown type to convert from {}."
            msg = msg.format(acls)
            raise TypeError(msg)

        return array.view(dtype)

    def copy(self, a, order=MemoryOrdering.SAME_ORDER, queue=None):
        """
        Return an array copy of the given object.
        """
        self._unsupported_argument("copy", "order", order, MemoryOrdering.SAME_ORDER)
        queue = queue = self.check_queue(queue)
        return self._call(a.handle.copy, queue=queue)

    # Filling
    def fill(self, a, value, queue=None):
        """
        Fill the array with given value
        """
        queue = self.check_queue(queue)
        if a.flags.forc:
            # only c-contiguous arrays are handled by pyopencl
            a.handle.fill(value=value, queue=queue)
        else:
            # else we have to use hysop opencl codegen backend
            from hysop.symbolic.relational import Assignment

            (a,) = self._kernel_generator.arrays_to_symbols(a)
            expr = Assignment(a, value)
            self._kernel_generator.elementwise_kernel(
                "fill", expr, queue=queue, call_only_once=True
            )

    # Ones and zeros
    def empty(
        self,
        shape,
        dtype=HYSOP_REAL,
        order=default_order,
        queue=None,
        min_alignment=None,
    ):
        """
        Return a new array of given shape and type, without initializing entries.
        If queue is specified, the queue becomes the default queue, else
        backend.default_queue is used instead.
        """
        return self.array(
            shape=shape,
            dtype=dtype,
            order=order,
            queue=queue,
            min_alignment=min_alignment,
        )

    def full(
        self,
        shape,
        fill_value,
        dtype=HYSOP_REAL,
        order=default_order,
        queue=None,
        min_alignment=None,
    ):
        """
        Return a new array of given shape and type, filled with fill_value.
        Queue is set as default queue.
        """
        a = self.empty(
            shape=shape,
            dtype=dtype,
            order=order,
            queue=queue,
            min_alignment=min_alignment,
        )
        self.fill(a=a, value=fill_value, queue=queue)
        return a

    def zeros(
        self,
        shape,
        dtype=HYSOP_REAL,
        order=default_order,
        queue=None,
        min_alignment=None,
    ):
        """
        Return a new array of given shape and type, filled with zeros.
        Queue is set as default queue.
        """
        return self.full(
            shape=shape,
            dtype=dtype,
            order=order,
            queue=queue,
            fill_value=0,
            min_alignment=min_alignment,
        )

    def ones(
        self,
        shape,
        dtype=HYSOP_REAL,
        order=default_order,
        queue=None,
        min_alignment=None,
    ):
        """
        Return a new array of given shape and type, filled with ones.
        Queue is set as default queue.
        """
        return self.full(
            shape=shape,
            dtype=dtype,
            order=order,
            queue=queue,
            fill_value=1,
            min_alignment=min_alignment,
        )

    def empty_like(
        self,
        a,
        shape=None,
        dtype=None,
        order=None,
        subok=True,
        queue=None,
        min_alignment=None,
    ):
        """
        Return a new array with the same shape and type as a given array.
        Queue is set as default queue.
        """
        dtype = first_not_None(dtype, a.dtype)
        shape = first_not_None(shape, a.shape)
        order = first_not_None(order, getattr(a, "order", default_order))
        return self.array(
            shape=shape,
            queue=queue,
            dtype=dtype,
            order=order,
            min_alignment=min_alignment,
        )

    def full_like(
        self,
        a,
        fill_value,
        dtype=None,
        order=None,
        subok=True,
        queue=None,
        min_alignment=None,
        shape=None,
    ):
        """
        Return a new array with the same shape and type as a given array.
        Queue is set as default queue.
        """
        a = self.empty_like(
            a=a,
            dtype=dtype,
            order=order,
            subok=subok,
            queue=queue,
            min_alignment=min_alignment,
            shape=shape,
        )
        self.fill(a, value=fill_value, queue=queue)
        return a

    def zeros_like(
        self,
        a,
        dtype=None,
        order=None,
        subok=True,
        queue=None,
        min_alignment=None,
        shape=None,
    ):
        """
        Return an array of zeros with the same shape and type as a given array.
        Queue is set as default queue.
        """
        return self.full_like(
            a=a,
            fill_value=0,
            dtype=dtype,
            order=order,
            subok=subok,
            queue=queue,
            min_alignment=min_alignment,
            shape=shape,
        )

    def ones_like(
        self,
        a,
        dtype=None,
        order=None,
        subok=True,
        queue=None,
        min_alignment=None,
        shape=None,
    ):
        """
        Return an array of ones with the same shape and type as a given array.
        Queue is set as default queue.
        """
        return self.full_like(
            a=a,
            fill_value=1,
            dtype=dtype,
            order=order,
            subok=subok,
            queue=queue,
            min_alignment=min_alignment,
            shape=shape,
        )

    def arange(self, *args, **kargs):
        """
        Return evenly spaced values within a given interval.
        """
        assert "allocator" not in kargs
        if "queue" not in kargs:
            queue = self.default_queue
        else:
            queue = kargs.pop("queue")
        if "dtype" not in kargs:
            kargs["dtype"] = HYSOP_INTEGER
        return self.wrap(
            clArray.arange(queue, *args, allocator=self.allocator, **kargs)
        )

    ###############################
    # ARRAY MANIPULATION ROUTINES #
    # See https://docs.scipy.org/doc/numpy/reference/routines.array-manipulation.html

    # Changing array shape

    def reshape(self, a, newshape, order=default_order, **kargs):
        """
        Gives a new shape to an array without changing its data.
        """
        return a._call("reshape", newshape=newshape, order=order, **kargs)

    def ravel(self, a, order=MemoryOrdering.SAME_ORDER):
        """
        Return a contiguous flattened array.
        """
        self._unsupported_argument("ravel", "order", order, MemoryOrdering.SAME_ORDER)
        return a._call("ravel")

    # Changing number of dimensions
    def squeeze(self, a, axis=None):
        """
        Remove single-dimensional entries from the shape of an array.
        """
        self._unsupported_argument("squeeze", "axis", axis, None)
        return a._call("squeeze")

    # Transpose-like operations
    # /!\ those functions can alter the logical transposition state /!\
    def transpose(self, a, axes=None):
        """
        Permute the dimensions of an array (only array strides are permutated).
        """
        return a._call("transpose", axes=axes)

    # BINARY OPERATIONS #
    # See https://docs.scipy.org/doc/numpy/reference/routines.bitwise.html
    #####################

    # Elementwise bit operations

    def bitwise_and(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Compute the bit-wise AND of two arrays element-wise.
        """
        expr = "y0[i] = (x0[i] & x1[i])"
        return self.binary_op(
            x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
        )

    def bitwise_or(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Compute the bit-wise OR of two arrays element-wise.
        """
        expr = "y0[i] = (x0[i] | x1[i])"
        return self.binary_op(
            x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
        )

    def bitwise_xor(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Compute the bit-wise XOR of two arrays element-wise.
        """
        expr = "y0[i] = (x0[i] ^ x1[i])"
        return self.binary_op(
            x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
        )

    def invert(self, x, out=None, queue=None, dtype=None):
        """
        Compute bit-wise inversion, or bit-wise NOT, element-wise.
        """
        expr = "y0[i] = (~x0[i])"
        return self.unary_op(x0=x, expr=expr, out=out, queue=queue, dtype=dtype)

    def left_shift(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Shift the bits of an integer to the left.
        """
        expr = "y0[i] = (x0[i] << x1[i])"
        return self.binary_op(
            x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
        )

    def right_shift(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Shift the bits of an integer to the right.
        """
        expr = "y0[i] = (x0[i] >> x1[i])"
        return self.binary_op(
            x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
        )

    # LOGIC FUNCTIONS #
    ###################
    # See https://docs.scipy.org/doc/numpy/reference/routines.logic.html

    # Truth value testing

    def any(self, a, axis=None, out=None, queue=None, dtype=HYSOP_BOOL):
        """
        Test whether any array elements along a given axis evaluate to True.
        """
        return self.reduce(
            kargs=(a,),
            axis=axis,
            dtype=dtype,
            out=out,
            queue=queue,
            neutral="false",
            reduce_expr="a||b",
        )

    def all(self, a, axis=None, out=None, queue=None, dtype=HYSOP_BOOL):
        """
        Test whether all array elements along a given axis evaluate to True.
        """
        return self.reduce(
            kargs=(a,),
            axis=axis,
            dtype=dtype,
            out=out,
            queue=queue,
            neutral="true",
            reduce_expr="a&&b",
        )

    # Array contents
    def isfinite(self, x, out=None, queue=None, dtype=HYSOP_BOOL):
        """
        Test element-wise for finiteness (not infinity or not Not a Number).
        """
        if is_fp(x):
            expr = "y0[i] = (!isinf(x0[i])) && (!isnan(x0[i]))"
        elif is_complex(x):
            expr = "y0[i] = ((!isinf(x0[i].real)) && (!isnan(x0[i].real))"
            expr += "&& (!isinf(x0[i].imag)) && (!isnan(x0[i].imag)))"
        else:
            expr = "y0[i] = 1"
        return self.unary_op(x0=x, expr=expr, out=out, queue=queue, dtype=dtype)

    def isinf(self, x, out=None, queue=None, dtype=HYSOP_BOOL):
        """
        Test element-wise for positive or negative infinity.
        """
        if is_fp(x):
            expr = "y0[i] = isinf(x0[i])"
        elif is_complex(x):
            expr = "y0[i] = (isinf(x0[i].real) || isinf(x0[i].imag))"
        else:
            expr = "y0[i] = 0"
        return self.unary_op(x0=x, expr=expr, out=out, queue=queue, dtype=dtype)

    def isnan(self, x, out=None, queue=None, dtype=HYSOP_BOOL):
        """
        Test element-wise for NaN and return result as a boolean array.
        """
        if is_fp(x):
            expr = "y0[i] = isnan(x0[i])"
        elif is_complex(x):
            expr = "y0[i] = (isnan(x0[i].real) || isnan(x0[i].imag))"
        else:
            expr = "y0[i] = 0"
        return self.unary_op(x0=x, expr=expr, out=out, queue=queue, dtype=dtype)

    def isneginf(self, x, out=None, queue=None, dtype=HYSOP_BOOL):
        """
        Test element-wise for negative infinity, return result as bool array.
        """
        assert not is_complex(x)
        if is_fp(x):
            expr = "y0[i] = signbit(x0[i]) && isinf(x0[i])"
        elif is_complex(x):
            expr = "y0[i] = ((signbit(x0[i].real) && isinf(x0[i].real))"
            expr += "|| (signbit(x0[i].imag) && isinf(x0[i].imag)))"
        else:
            expr = "y0[i] = 0"
        return self.unary_op(x0=x, expr=expr, out=out, queue=queue, dtype=dtype)

    def isposinf(self, x, out=None, queue=None, dtype=HYSOP_BOOL):
        """
        Test element-wise for positive infinity, return result as bool array.
        """
        assert not is_complex(x)
        if is_fp(x):
            expr = "y0[i] = (!signbit(x0[i])) && isinf(x0[i])"
        elif is_complex(x):
            expr = "y0[i] = ((!signbit(x0[i].real) && isinf(x0[i].real))"
            expr += "|| (!signbit(x0[i].imag) && isinf(x0[i].imag)))"
        else:
            expr = "y0[i] = 0"
        return self.unary_op(x0=x, expr=expr, out=out, queue=queue, dtype=dtype)

    # Logical operations
    def logical_and(self, x1, x2, out=None, queue=None, dtype=HYSOP_BOOL):
        """
        Compute the truth value of x1 AND x2 element-wise.
        """
        expr = "y0[i] = (x0[i] && x1[i])"
        return self.binary_op(
            x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
        )

    def logical_or(self, x1, x2, out=None, queue=None, dtype=HYSOP_BOOL):
        """
        Compute the truth value of x1 OR x2 element-wise.
        """
        expr = "y0[i] = (x0[i] || x1[i])"
        return self.binary_op(
            x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
        )

    def logical_not(self, x, out=None, queue=None, dtype=HYSOP_BOOL):
        """
        Compute the truth value of NOT x element-wise.
        """
        expr = "y0[i] = (!x0[i])"
        return self.unary_op(x0=x, expr=expr, out=out, queue=queue, dtype=dtype)

    def logical_xor(self, x1, x2, out=None, queue=None, dtype=HYSOP_BOOL):
        """
        Compute the truth value of x1 XOR x2, element-wise.
        """
        expr = "y0[i] = (x0[i] ? (!x1[i]) : x1[i])"
        return self.binary_op(
            x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
        )

    # Comparisson
    def allequal(self, a, b, equal_nan=False, queue=None, dtype=HYSOP_BOOL):
        """
        Returns True if two arrays are element-wise equal within a tolerance.
        """
        x0, x1 = a, b
        map_expr = "( x0[i]==x1[i] )"
        if is_fp(x0) and is_fp(x1) and equal_nan:
            map_expr += " || ( isnan(x0[i]) && isnan(x1[i]) )"
        return self.reduce(
            kargs=(x0, x1),
            neutral="true",
            reduce_expr="a&&b",
            queue=queue,
            dtype=dtype,
            map_expr=map_expr,
            arguments=arguments,
        )

    def allclose(
        self,
        a,
        b,
        rtol=1e-05,
        atol=1e-08,
        equal_nan=False,
        queue=None,
        dtype=HYSOP_BOOL,
    ):
        """
        Returns True if two arrays are element-wise equal within a tolerance.
        """
        if is_complex(a) or is_complex(b):
            map_expr = "({fabs}({sub}(x0[i],x1[i])) <= (atol + rtol*{fabs}(x1[i])))"
            common_dtype = find_common_dtype(a, b)
            map_expr = map_expr.format(
                fabs=self.complex_fn("abs", common_dtype),
                sub=self.complex_fn("sub", common_dtype),
            )
            convert_inputs = "c"
        else:
            map_expr = "(fabs(x0[i]-x1[i]) <= (atol + rtol*fabs(x1[i])))"
            convert_inputs = "f"
        return self.reduce(
            kargs=(a, b),
            neutral="true",
            reduce_expr="a&&b",
            queue=queue,
            dtype=dtype,
            map_expr=map_expr,
            extra_kargs=(np.float64(rtol), np.float64(atol)),
            extra_arguments=["const double rtol", "const double atol"],
            convert_inputs=convert_inputs,
            alloc_dtypes="f",
        )

    def isclose(
        self,
        a,
        b,
        rtol=1e-05,
        atol=1e-08,
        equal_nan=False,
        queue=None,
        dtype=HYSOP_BOOL,
    ):
        """
        Returns a boolean array where two arrays are element-wise equal within a tolerance.
        """
        if is_complex(a) or is_complex(b):
            map_expr = "({fabs}({sub}(x0[i],x1[i])) <= (atol + rtol*{fabs}(x1[i])))"
            common_dtype = find_common_dtype(a, b)
            map_expr = map_expr.format(
                fabs=self.complex_fn("abs", common_dtype),
                sub=self.complex_fn("sub", common_dtype),
            )
            convert_inputs = "c"
        else:
            map_expr = "(fabs(x0[i]-x1[i]) <= (atol + rtol*fabs(x1[i])))"
            convert_inputs = "f"
        return self.binary_op(
            x0=a,
            x1=b,
            expr=expr,
            out=out,
            extra_kargs=(np.float64(rtol), np.float64(atol)),
            extra_arguments=["double rtol", "double atol"],
            queue=queue,
            dtype=dtype,
            convert_inputs=convert_inputs,
        )

    def array_equal(self, a1, a2, queue=None, dtype=HYSOP_BOOL):
        """
        True if two arrays have the same shape and elements, False otherwise.
        """
        return (a1.shape == a2.shape) and self.all_equal(
            x1=a1, x2=a2, queue=queue, dtype=HYSOP_BOOL
        )

    @staticmethod
    def _make_comparisson_expr(x1, x2, comp):
        iseq = comp == "=="
        expr = "y0[i] = (({}) || (({}) && ({})))"
        if is_complex(x1) and is_complex(x2):
            expr = expr.format(
                "false" if iseq else "x0[i].real {comp}  x1[i].real",
                "x0[i].real == x1[i].real",
                "x0[i].imag {comp}  x1[i].imag",
            )
        elif is_complex(x1):
            expr = expr.format(
                "false" if iseq else "x0[i].real {comp}  x1[i]",
                "x0[i].real == x1[i]",
                "x0[i].imag {comp}  0",
            )
        elif is_complex(x2):
            expr = expr.format(
                "false" if iseq else "x0[i] {comp}  x1[i].real",
                "x0[i] == x1[i].real",
                "0     {comp}  x1[i].imag",
            )
        else:
            expr = expr.format(
                "false" if iseq else "x0[i] {comp}  x1[i]",
                "x0[i] == x1[i]",
                "0     {comp}  0",
            )
        return expr.format(comp=comp)

    def greater(self, x1, x2, out=None, queue=None, dtype=HYSOP_BOOL):
        """
        Return the truth value of (x1 > x2) element-wise.
        """
        expr = self._make_comparisson_expr(x1, x2, ">")
        return self.binary_op(
            x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
        )

    def greater_equal(self, x1, x2, out=None, queue=None, dtype=HYSOP_BOOL):
        """
        Return the truth value of (x1 >= x2) element-wise.
        """
        expr = self._make_comparisson_expr(x1, x2, ">=")
        return self.binary_op(
            x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
        )

    def less(self, x1, x2, out=None, queue=None, dtype=HYSOP_BOOL):
        """
        Return the truth value of (x1 < x2) element-wise.
        """
        expr = self._make_comparisson_expr(x1, x2, "<")
        return self.binary_op(
            x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
        )

    def less_equal(self, x1, x2, out=None, queue=None, dtype=HYSOP_BOOL):
        """
        Return the truth value of (x1 =< x2) element-wise.
        """
        expr = self._make_comparisson_expr(x1, x2, "<=")
        return self.binary_op(
            x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
        )

    def equal(self, x1, x2, out=None, queue=None, dtype=HYSOP_BOOL):
        """
        Return (x1 == x2) element-wise.
        """
        expr = self._make_comparisson_expr(x1, x2, "==")
        return self.binary_op(
            x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
        )

    def not_equal(self, x1, x2, out=None, queue=None, dtype=HYSOP_BOOL):
        """
        Return (x1 != x2) element-wise.
        """
        expr = self._make_comparisson_expr(x1, x2, "!=")
        return self.binary_op(
            x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
        )

    # MATHEMATICAL FUNCTIONS #
    ##########################
    # See https://docs.scipy.org/doc/numpy/reference/routines.math.html

    def _flt_or_cplx_unary_op(self, x, fname, **kargs):
        assert "expr" not in kargs
        assert "x0" not in kargs
        assert "convert_inputs" not in kargs
        assert "alloc_dtypes" not in kargs
        expr = "y0[i] = {}(x0[i])"
        if is_complex(x):
            expr = expr.format(self.complex_fn(fname, x))
            convert_inputs = "c"
            alloc_dtypes = "c"
        else:
            expr = expr.format(fname)
            convert_inputs = "f"
            alloc_dtypes = "f"
        return self.unary_op(
            expr=expr,
            x0=x,
            convert_inputs=convert_inputs,
            alloc_dtypes=alloc_dtypes,
            **kargs,
        )

    def _flt_or_map_cplx_unary_op(self, x, fname, **kargs):
        assert "expr" not in kargs
        assert "x0" not in kargs
        if is_complex(x):
            expr = "y0[i] = {}({fn}(x0[i].real), {fn}(x0[i].imag))"
            expr = expr.format(self.complex_fn("new", x), fn=fname)
            _convert_inputs = None
            _alloc_dtypes = (x.dtype,)
        else:
            expr = "y0[i] = {}(x0[i])"
            expr = expr.format(fname)
            _convert_inputs = "f"
            _alloc_dtypes = "f"
        alloc_dtypes = kargs.pop("alloc_dtypes", None) or _alloc_dtypes
        convert_inputs = kargs.pop("convert_inputs", None) or _convert_inputs
        return self.unary_op(
            expr=expr,
            x0=x,
            convert_inputs=convert_inputs,
            alloc_dtypes=alloc_dtypes,
            **kargs,
        )

    def _cplx_binary_op(self, x0, x1, fname, **kargs):
        assert is_complex(x0) or is_complex(x1)
        assert "expr" not in kargs
        assert "convert_inputs" not in kargs
        assert "alloc_dtypes" not in kargs

        convert_inputs = "f"
        alloc_dtypes = "c"
        expr = self.binary_complex_fn(fname, x0, x1)

        return self.binary_op(
            expr=expr,
            x0=x0,
            x1=x1,
            convert_inputs=convert_inputs,
            alloc_dtypes=alloc_dtypes,
            **kargs,
        )

    # Trigonometric functions

    def sin(self, x, out=None, queue=None, dtype=None):
        """
        Trigonometric sine, element-wise.
        """
        return self._flt_or_cplx_unary_op(x, "sin", out=out, queue=queue, dtype=dtype)

    def cos(self, x, out=None, queue=None, dtype=None):
        """
        Cosine element-wise.
        """
        return self._flt_or_cplx_unary_op(x, "cos", out=out, queue=queue, dtype=dtype)

    def tan(self, x, out=None, queue=None, dtype=None):
        """
        Compute tangent element-wise.
        """
        return self._flt_or_cplx_unary_op(x, "tan", out=out, queue=queue, dtype=dtype)

    def arcsin(self, x, out=None, queue=None, dtype=None):
        """
        Inverse sine, element-wise.
        """
        return self._flt_or_cplx_unary_op(x, "asin", out=out, queue=queue, dtype=dtype)

    def arccos(self, x, out=None, queue=None, dtype=None):
        """
        Trigonometric inverse cosine, element-wise.
        """
        return self._flt_or_cplx_unary_op(x, "acos", out=out, queue=queue, dtype=dtype)

    def arctan(self, x, out=None, queue=None, dtype=None):
        """
        Trigonometric inverse tangent, element-wise.
        """
        return self._flt_or_cplx_unary_op(x, "atan", out=out, queue=queue, dtype=dtype)

    def arctan2(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Element-wise arc tangent of x1/x2 choosing the quadrant correctly.
        """
        expr = "y0[i] = atan2(x0[i],x1[i])"
        assert not is_complex(x1) and not is_complex(x2)
        return self.binary_op(
            x0=x1,
            x1=x2,
            expr=expr,
            out=out,
            queue=queue,
            dtype=dtype,
            convert_inputs="f",
            alloc_dtypes="f",
        )

    def hypot(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Given the legs of a right triangle, return its hypotenuse.
        """
        assert not is_complex(x1) and not is_complex(x2)
        expr = "y0[i] = hypot(x0[i],x1[i])"
        return self.binary_op(
            x0=x1,
            x1=x2,
            expr=expr,
            out=out,
            queue=queue,
            dtype=dtype,
            convert_inputs="f",
            alloc_dtypes="f",
        )

    # def unwrap(self, p, discont=3.141592653589793, axis=-1):
    # """
    # Unwrap by changing deltas between values to 2*pi complement.
    # """
    # self.unary_op(x0=x, expr='y0[i] = unwrap(x0[i])', out=out,
    # queue=queue, dtype=dtype,
    # convert_inputs='f', alloc_dtypes='f')

    def deg2rad(self, x, out=None, queue=None, dtype=None):
        """
        Convert angles from degrees to radians.
        """
        assert not is_complex(x)
        return self.unary_op(
            x0=x,
            expr="y0[i] = x0[i] * M_PI/180.0",
            out=out,
            queue=queue,
            dtype=dtype,
            convert_inputs="f",
            alloc_dtypes="f",
        )

    def rad2deg(self, x, out=None, queue=None, dtype=None):
        """
        Convert angles from radians to degrees.
        """
        assert not is_complex(x)
        return self.unary_op(
            x0=x,
            expr="y0[i] = x0[i] * 180.0/M_PI",
            out=out,
            queue=queue,
            dtype=dtype,
            convert_inputs="f",
            alloc_dtypes="f",
        )

    # Hyperbolic functions
    def sinh(self, x, out=None, queue=None, dtype=None):
        """
        Hyperbolic sine, element-wise.
        """
        return self._flt_or_cplx_unary_op(x, "sinh", out=out, queue=queue, dtype=dtype)

    def cosh(self, x, out=None, queue=None, dtype=None):
        """
        Hyperbolic cosine, element-wise.
        """
        return self._flt_or_cplx_unary_op(x, "cosh", out=out, queue=queue, dtype=dtype)

    def tanh(self, x, out=None, queue=None, dtype=None):
        """
        Compute hyperbolic tangent element-wise.
        """
        return self._flt_or_cplx_unary_op(x, "tanh", out=out, queue=queue, dtype=dtype)

    def arcsinh(self, x, out=None, queue=None, dtype=None):
        """
        Inverse hyperbolic sine element-wise.
        """
        return self._flt_or_cplx_unary_op(x, "asinh", out=out, queue=queue, dtype=dtype)

    def arccosh(self, x, out=None, queue=None, dtype=None):
        """
        Inverse hyperbolic cosine, element-wise.
        """
        return self._flt_or_cplx_unary_op(x, "acosh", out=out, queue=queue, dtype=dtype)

    def arctanh(self, x, out=None, queue=None, dtype=None):
        """
        Inverse hyperbolic tangent element-wise.
        """
        return self._flt_or_cplx_unary_op(x, "atanh", out=out, queue=queue, dtype=dtype)

    # Rounding
    def around(self, a, decimals=0, out=None, queue=None, dtype=None):
        """
        Round an array to the given number of decimals.
        """
        self._unsupported_argument("round", "decimals", decimals, 0)
        if is_complex(dtype):
            convert_inputs = (np.complex128,)
        else:
            convert_inputs = None
        alloc_dtypes = (a.dtype,)
        return self._flt_or_map_cplx_unary_op(
            a,
            "round",
            out=out,
            queue=queue,
            dtype=dtype,
            alloc_dtypes=alloc_dtypes,
            convert_inputs=convert_inputs,
        )

    def fix(self, x, y=None, out=None, queue=None, dtype=None):
        """
        Round to nearest integer towards zero.
        """
        assert not is_complex(x)
        return self.unary_op(
            x0=x,
            expr="y0[i] = trunc(x0[i])",
            out=y,
            queue=queue,
            dtype=dtype,
            convert_inputs="f",
            alloc_dtypes="f",
        )

    def rint(self, x, out=None, queue=None, dtype=None):
        """
        Round elements of the array to the nearest integer.
        """
        return self._flt_or_map_cplx_unary_op(
            x, "rint", out=out, queue=queue, dtype=dtype
        )

    def floor(self, x, out=None, queue=None, dtype=None):
        """
        Return the floor of the input, element-wise.
        """
        assert not is_complex(x)
        if x.is_fp():
            expr = "y0[i] = floor(x0[i])"
        else:
            expr = "y0[i] = x0[i]"
        return self.unary_op(
            x0=x,
            expr=expr,
            out=out,
            queue=queue,
            dtype=dtype,
            convert_inputs="f",
            alloc_dtypes="f",
        )

    def ceil(self, x, out=None, queue=None, dtype=None):
        """
        Return the ceiling of the input, element-wise.
        """
        assert not is_complex(x)
        return self.unary_op(
            x0=x,
            expr="y0[i] = ceil(x0[i])",
            out=out,
            queue=queue,
            dtype=dtype,
            convert_inputs="f",
            alloc_dtypes="f",
        )

    def trunc(self, x, out=None, queue=None, dtype=None):
        """
        Return the truncated value of the input, element-wise.
        """
        assert not is_complex(x)
        return self.unary_op(
            x0=x,
            expr="y0[i] = trunc(x0[i])",
            out=out,
            queue=queue,
            dtype=dtype,
            convert_inputs="f",
            alloc_dtypes="f",
        )

    # Sums, product, differences

    def sum(self, a, axis=None, dtype=None, out=None, queue=None, **kwds):
        """
        Sum of array elements over a given axis.
        """
        return self.reduce(
            kargs=(a,),
            axis=axis,
            dtype=dtype,
            out=out,
            queue=queue,
            neutral="0",
            reduce_expr="a+b",
            **kwds,
        )

    def prod(self, a, axis=None, dtype=None, out=None, queue=None):
        """
        Return the product of array elements over a given axis.
        """
        return self.reduce(
            kargs=(a,),
            axis=axis,
            dtype=dtype,
            out=out,
            queue=queue,
            neutral="1",
            reduce_expr="a*b",
        )

    def nansum(self, a, axis=None, dtype=None, out=None, queue=None, **kwds):
        """
        Return the sum of array elements over a given axis treating Not a Numbers (NaNs)
        as zeros.
        """
        return self.nanreduce(
            kargs=(a,),
            axis=axis,
            dtype=dtype,
            out=out,
            queue=queue,
            neutral="0",
            reduce_expr="a+b",
            **kwds,
        )

    def nanprod(self, a, axis=None, dtype=None, out=None, queue=None):
        """
        Return the product of array elements over a given axis treating
        Not a Numbers (NaNs) as ones.
        """
        return self.nanreduce(
            kargs=(a,),
            axis=axis,
            dtype=dtype,
            out=out,
            queue=queue,
            neutral="1",
            reduce_expr="a*b",
        )

    def cumprod(self, a, axis=None, dtype=None, out=None, queue=None):
        r"""
        Return the cumulative product of elements along a given axis.
        /!\ precision loss because of operation ordering
        """
        return self.inclusive_scan(
            kargs=(a,),
            axis=axis,
            dtype=dtype,
            out=out,
            neutral="1",
            scan_expr="a*b",
            queue=queue,
        )

    def cumsum(self, a, axis=None, dtype=None, out=None, queue=None):
        """
        Return the cumulative sum of the elements along a given axis.
        """
        return self.inclusive_scan(
            kargs=(a,),
            axis=axis,
            dtype=dtype,
            out=out,
            neutral="0",
            scan_expr="a+b",
            queue=queue,
        )

    def nancumprod(self, a, axis=None, dtype=None, out=None, queue=None):
        r"""
        Return the cumulative product of array elements over a given axis treating
        Not a Numbers (NaNs) as one.
        /!\ precision loss because of operation ordering
        """
        return self.inclusive_nanscan(
            kargs=(a,),
            axis=axis,
            dtype=dtype,
            out=out,
            neutral="1",
            scan_expr="a*b",
            queue=queue,
        )

    def nancumsum(self, a, axis=None, dtype=None, out=None, queue=None):
        """
        Return the cumulative sum of array elements over a given axis treating
        Not a Numbers (NaNs) as zero.
        """
        return self.inclusive_nanscan(
            kargs=(a,),
            axis=axis,
            dtype=dtype,
            out=out,
            neutral="0",
            scan_expr="a+b",
            queue=queue,
        )

    def diff(self, a, n=1, axis=-1):
        """
        Calculate the n-th discrete difference along given axis.
        """
        self._not_implemented_yet("diff")

    def ediff1d(self, ary, to_end=None, to_begin=None):
        """
        The differences between consecutive elements of an array.
        """
        self._not_implemented_yet("ediff1d")

    def gradient(self, f, *varargs, **kwargs):
        """
        Return the gradient of an N-dimensional array.
        """
        self._not_implemented_yet("gradient")

    def cross(self, a, b, axisa=-1, axisb=-1, axisc=-1, axis=None):
        """
        Return the cross product of two (arrays of) vectors.
        """
        self._not_implemented_yet("cross")

    def trapz(self, y, x=None, dx=1.0, axis=-1):
        """
        Integrate along the given axis using the composite trapezoidal rule.
        """
        self._not_implemented_yet("trapz")

    # Exponents and logarithms
    def exp(self, x, out=None, queue=None, dtype=None):
        """
        Calculate the exponential of all elements in the input array.
        """
        return self._flt_or_cplx_unary_op(x, "exp", out=out, queue=queue, dtype=dtype)

    def exp2(self, x, out=None, queue=None, dtype=None):
        """
        Calculate 2**p for all p in the input array.
        """
        expr = "y0[i] = exp2(x0[i])"
        return self._flt_or_cplx_unary_op(x, "exp2", out=out, queue=queue, dtype=dtype)

    def expm1(self, x, out=None, queue=None, dtype=None):
        """
        Calculate exp(x) - 1 for all elements in the array.
        """
        expr = "y0[i] = expm1(x0[i])"
        return self._flt_or_cplx_unary_op(x, "expm1", out=out, queue=queue, dtype=dtype)

    def log(self, x, out=None, queue=None, dtype=None):
        """
        Natural logarithm, element-wise.
        """
        return self._flt_or_cplx_unary_op(x, "log", out=out, queue=queue, dtype=dtype)

    def log2(self, x, out=None, queue=None, dtype=None):
        """
        Base-2 logarithm of x.
        """
        return self._flt_or_cplx_unary_op(x, "log2", out=out, queue=queue, dtype=dtype)

    def log10(self, x, out=None, queue=None, dtype=None):
        """
        Return the base 10 logarithm of the input array, element-wise.
        """
        return self._flt_or_cplx_unary_op(x, "log10", out=out, queue=queue, dtype=dtype)

    def log1p(self, x, out=None, queue=None, dtype=None):
        """
        Return the natural logarithm of one plus the input array, element-wise.
        """
        expr = "y0[i] = log1p(x0[i])"
        return self._flt_or_cplx_unary_op(x, "log1p", out=out, queue=queue, dtype=dtype)

    def logaddexp(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Logarithm of the sum of exponentiations of the inputs.
        """
        assert not is_complex(x1) and not is_complex(x2)
        expr = "y0[i] = log(exp(x0[i]) + exp(x1[i]))"
        return self.binary_op(
            x0=x1,
            x1=x2,
            expr=expr,
            out=out,
            queue=queue,
            dtype=dtype,
            convert_inputs="f",
            alloc_dtypes="f",
        )

    def logaddexp2(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Logarithm of the sum of exponentiations of the inputs in base-2.
        """
        assert not is_complex(x1) and not is_complex(x2)
        expr = "y0[i] = log2(pow(2,x0[i]) + pow(2,x1[i]))"
        return self.binary_op(
            x0=x1,
            x1=x2,
            expr=expr,
            out=out,
            queue=queue,
            dtype=dtype,
            convert_inputs="f",
            alloc_dtypes="f",
        )

    # Other special functions
    def i0(self, x, out=None, queue=None, dtype=None):
        """
        Modified Bessel function of the first kind, order 0.
        """
        self._not_implemented_yet("i0", convert_inputs="f", alloc_dtypes="f")

    def sinc(self, x, out=None, queue=None, dtype=None):
        """
        Return the sinc function.
        """
        assert not is_complex(x)
        expr = "y0[i] = sin(M_PI*x0[i]) / (M_PI*x0[i])"
        return self.unary_op(
            x0=x,
            expr=expr,
            out=out,
            queue=queue,
            dtype=dtype,
            convert_inputs="f",
            alloc_dtypes="f",
        )

    # Floating point routines
    def signbit(self, x, out=None, queue=None, dtype=HYSOP_BOOL):
        """
        Returns element-wise True where signbit is set (less than zero).
        """
        assert not is_complex(x)
        expr = "y0[i] = signbit(x0[i])"
        return self.unary_op(
            x0=x, expr=expr, out=out, queue=queue, dtype=dtype, convert_inputs="f"
        )

    def copysign(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Change the sign of x1 to that of x2, element-wise.
        """
        assert not is_complex(x1) and not is_complex(x2)
        expr = "y0[i] = copysign(x0[i], x1[i])"
        return self.binary_op(
            x0=x1,
            x1=x2,
            expr=expr,
            out=out,
            queue=queue,
            dtype=dtype,
            convert_inputs="f",
            alloc_dtypes="f",
        )

    def frexp(self, x, out1=None, out2=None, queue=None):
        """
        Decompose the elements of x into mantissa and twos exponent.
        """
        assert not is_complex(x)
        expr = "int buf; y0[i] = frexp(x0[i], &buf); y1[i]=buf;"
        return self.nary_op(
            ikargs=(x,),
            okargs=(out1, out2),
            operation=expr,
            queue=queue,
            convert_inputs="f",
            alloc_dtypes=("f", np.int32),
        )

    def ldexp(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Returns x1 * 2**x2, element-wise.
        """
        expr = "y0[i] = ldexp(x0[i])"
        return self.unary_op(
            x0=x,
            expr=expr,
            out=out,
            queue=queue,
            dtype=dtype,
            convert_inputs="f",
            alloc_dtypes="f",
        )

    # Arithmetic operations

    def add(self, x1, x2, out=None, queue=None, dtype=None, name="add", **kwds):
        """
        Add arguments element-wise.
        """
        if is_complex(x1) or is_complex(x2):
            return self._cplx_binary_op(
                x1, x2, "add", out=out, queue=queue, dtype=dtype, name=name, **kwds
            )
        else:
            expr = "y0[i] = (x0[i] + x1[i])"
            return self.binary_op(
                x0=x1,
                x1=x2,
                expr=expr,
                out=out,
                queue=queue,
                dtype=dtype,
                name=name,
                **kwds,
            )

    def reciprocal(self, x, out=None, queue=None, dtype=None):
        """
        Return the reciprocal of the argument, element-wise.
        """
        dt = get_dtype(x)
        if is_complex(x):
            expr = "y0[i] = {}(1,x0[i])".format(self.complex_fn("rdivide", x))
        elif is_integer(x):
            info = np.iinfo(dt)
            if (dt == np.int32) or (dt == np.int64):  # to match numpy output
                suffix = "u" if is_unsigned(x) else ""
                suffix += "L" if (dt == np.int64) else ""
                imin = f"{info.min}{suffix}"
                if dt == np.int64:
                    imin = f"{info.min+1}{suffix}"  # ...
                expr = "y0[i] = (x0[i]==0 ? {imin} : (1{suffix}/x0[i]))"
                expr = expr.format(imin=imin, suffix=suffix)
            else:
                expr = "y0[i] = (x0[i]==0 ? 0 : (1/x0[i]))"
        else:
            expr = "y0[i] = (1.0/x0[i])"
        return self.unary_op(x0=x, expr=expr, out=out, queue=queue, dtype=dtype)

    def negative(self, x, out=None, queue=None, dtype=None):
        """
        Numerical negative, element-wise.
        """
        if is_complex(x):
            expr = "y0[i] = {}(x0[i])".format(self.complex_fn("neg", x))
        else:
            expr = "y0[i] = (-x0[i])"
        return self.unary_op(x0=x, expr=expr, out=out, queue=queue, dtype=dtype)

    def multiply(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Multiply arguments element-wise.
        """
        if is_complex(x1) or is_complex(x2):
            return self._cplx_binary_op(
                x1,
                x2,
                "mul",
                out=out,
                queue=queue,
                dtype=dtype,
            )
        else:
            expr = "y0[i] = (x0[i] * x1[i])"
            return self.binary_op(
                x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
            )

    def divide(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Divide arguments element-wise.
        """
        if is_complex(x1) or is_complex(x2):
            return self._cplx_binary_op(
                x1, x2, "divide", out=out, queue=queue, dtype=dtype
            )
        elif is_integer(x2):
            expr = "y0[i] = (x1[i]==0 ? 0 : floor(x0[i] / x1[i]))"
            convert_inputs = np.float64
        else:
            expr = "y0[i] = (x0[i] / x1[i])"
            convert_inputs = None
        return self.binary_op(
            x0=x1,
            x1=x2,
            expr=expr,
            out=out,
            queue=queue,
            dtype=dtype,
            convert_inputs=convert_inputs,
        )

    def power(self, x1, x2, out=None, queue=None, dtype=None):
        """
        First array elements raised to powers from second array, element-wise.
        """
        if is_complex(x1) or is_complex(x2):
            return self._cplx_binary_op(
                x1,
                x2,
                "pow",
                out=out,
                queue=queue,
                dtype=dtype,
            )
        else:
            expr = "pow(x0[i], x1[i])"
            expr = "y0[i] = " + expr
            return self.binary_op(
                x0=x1,
                x1=x2,
                expr=expr,
                out=out,
                queue=queue,
                dtype=dtype,
                convert_inputs="f",
            )

    def subtract(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Subtract arguments, element-wise.
        """
        if is_complex(x1) or is_complex(x2):
            return self._cplx_binary_op(
                x1,
                x2,
                "sub",
                out=out,
                queue=queue,
                dtype=dtype,
            )
        else:
            expr = "y0[i] = (x0[i] - x1[i])"
            return self.binary_op(
                x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
            )

    def true_divide(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Returns a true division of the inputs, element-wise.
        """
        if is_complex(x1) or is_complex(x2):
            return self._cplx_binary_op(
                x1, x2, "divide", out=out, queue=queue, dtype=dtype
            )
        else:
            expr = "y0[i] = (x0[i] / x1[i])"
            convert_inputs = None
            if is_integer(x1) and is_integer(x2):
                dtype = dtype or HYSOP_REAL
                convert_inputs = dtype
            return self.binary_op(
                x0=x1,
                x1=x2,
                expr=expr,
                out=out,
                queue=queue,
                dtype=dtype,
                convert_inputs=convert_inputs,
            )

    def floor_divide(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Return the largest integer smaller or equal to the division of the inputs.
        Returns y = floor(x1/x2)

        Special floating point values are handled this way:

        x1   x2   output
        nan  ***   nan
        ***  nan   nan
        inf  ***   nan
        ---  inf   0 if (x1 and x2 have same sign) else -1
        ---  ---   floor(x1/x2)

        Note: inf means +inf or -inf.
        """
        if is_complex(x1) or is_complex(x2):
            self._not_implemented_yet("floor_divide.")
        elif is_integer(x1):
            expr = "y0[i] = (x1[i]==0 ? 0 : floor(x0[i]/x1[i]))"
            convert_inputs = (np.float64, None)
        else:
            # NG        expr = 'y0[i] = ((isnan(x0[i])||isnan(x1[i])||isinf(x0[i]))?NAN:(isinf(x1[i])?((signbit(x0[i])^signbit(x1[i]))?-1:0):floor(x0[i]/x1[i])))'
            # NG: Error using y0_tmp????
            #            expr = clTools.dtype_to_ctype(np.float64)+ \
            #                  ' y0_tmp = ((isnan(x0[i]) ||isinf(x0[i]))?NAN:666); \
            #                    y0_tmp = ((isnan(y0_tmp)||isnan(x1[i]))?NAN:666); \
            #                    y0[i]  = ( isnan(y0_tmp)?NAN:(isinf(x1[i])?((signbit(x0[i])^signbit(x1[i]))?-1:0):floor(x0[i]/x1[i])))'
            expr = "y0[i] = ((isnan(x0[i])||isinf(x0[i]))?NAN:666); \
                    y0[i] = ((isnan(y0[i])||isnan(x1[i]))?NAN:666); \
                    y0[i]  = ( isnan(y0[i])?NAN:(isinf(x1[i])?((signbit(x0[i])^signbit(x1[i]))?-1:0):floor(x0[i]/x1[i])))"
            convert_inputs = "f"
        return self.binary_op(
            x0=x1,
            x1=x2,
            expr=expr,
            out=out,
            queue=queue,
            dtype=dtype,
            convert_inputs=convert_inputs,
        )

    def fmod(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Return the element-wise remainder of division (REM).
        Remainder has the same sign as the dividend x1.
        This should not be confused with the Python modulus operator x1 % x2.
        Returns x - y*trunc(x/y)
        """
        assert not is_complex(x1) and not is_complex(x2)
        if is_fp(x1) or is_fp(x2):
            # expr = 'y0[i] = x0[i] - x1[i]*trunc(x0[i]/x1[i])'
            expr = "y0[i] = fmod(x0[i],x1[i])"
        else:
            expr = "y0[i] = (x1[i] == 0 ? 0 : (x0[i] % x1[i]))"
        return self.binary_op(
            x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
        )

    def mod(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Return element-wise remainder of division (MOD).
        Remainder has the same sign as the divisor x2.
        Match Python modulus operator x1 % x2.
        Returns x - y*floor(x/y)

        Special floating point values are handled this way:

        x1   x2   output
        nan  ***   nan
        ***  nan   nan
        inf  inf   nan
        inf  ---   x2 if (x1 and x2 have same sign) else x1
        ---  ---   x1 - x2 * floor(x1/x2)

        Note: inf means +inf or -inf.
        """
        assert not is_complex(x1) and not is_complex(x2)
        expr = "x0[i] - x1[i]*floor(x0[i]/x1[i])"
        convert_inputs = None
        if is_integer(x1):
            expr = f"(x1[i]==0 ? 0 : {expr})"
            convert_inputs = (np.float64, None)
        if is_fp(x1) or is_fp(x2):
            # NG bug bypass
            # NG: error when using y0_tmp ????
            #            expr = clTools.dtype_to_ctype(np.float64)+ \
            #                   f' y0_tmp = (isnan(x0[i]) ?NAN:666); \
            #                     y0_tmp = (isnan(y0_tmp)?NAN:(isnan(x1[i])?NAN:666)); \
            #                     y0[i]  = (isnan(y0_tmp)?NAN:(isinf(x1[i])?(isinf(x0[i])?NAN:(signbit(x0[i])^signbit(x1[i])?x1[i]:x0[i])):{expr}))'
            expr = f"y0[i] = (isnan(x0[i])?NAN:666); \
                     y0[i] = (isnan(y0[i])?NAN:(isnan(x1[i])?NAN:666)); \
                     y0[i]  = (isnan(y0[i])?NAN:(isinf(x1[i])?(isinf(x0[i])?NAN:(signbit(x0[i])^signbit(x1[i])?x1[i]:x0[i])):{expr}))"
            convert_inputs = "f"
        return self.binary_op(
            x0=x1,
            x1=x2,
            expr=expr,
            out=out,
            queue=queue,
            dtype=dtype,
            convert_inputs=convert_inputs,
        )

    def modf(self, x, out1=None, out2=None, queue=None, dtype=None):
        """
        Return the fractional and integral parts of an array, element-wise.
        The fractional and integral parts are negative if the given number is negative.
        => they have the same sign as the input argument.
        out1 = fractional part
        out2 = integral part
        """
        assert not is_complex(x)
        if x.is_fp():
            expr = "y0[i] = modf(x0[i], &y1[i])"
        else:
            expr = "y0[i] = 0; y1[i] = x0[i];"
        return self.nary_op(
            ikargs=(x,),
            okargs=(out1, out2),
            operation=expr,
            queue=queue,
            dtype=dtype,
            alloc_dtypes="f",
        )

    # Handling complex numbers
    def angle(self, z, deg=False, queue=None, dtype=None):
        """
        Return the angle of the complex argument.
        """
        if deg:
            fact = 180 / np.pi
        else:
            fact = 1.0
        if is_complex(z):
            zimag = z.imag
            zreal = z.real
        else:
            zimag = 0
            zreal = z
        return self.arctan2(zimag, zreal, queue=queue) * fact

    def real(self, val, queue=None, out=None, dtype=None):
        """
        Return the real part of the elements of the array.
        """
        if is_complex(val):
            expr = "y0[i] = x0[i].real"
            dtype = dtype or complex_to_float_dtype(val.dtype)
            return self.unary_op(x0=val, expr=expr, out=out, queue=queue, dtype=dtype)
        else:
            return val

    def imag(self, val):
        """
        Return the imaginary part of the elements of the array.
        """
        return self.wrap(val.handle.imag)

    def conj(self, x, out=None):
        """
        Return the complex conjugate, element-wise.
        """
        return self.wrap(x.handle.conj())

    # Miscellanous
    def convolve(self, a, v, mode="full", queue=None, dtype=None):
        """
        Returns the discrete, linear convolution of two one-dimensional sequences.
        """
        self._not_implemented_yet("convolve")

    def clip(self, a, a_min, a_max, out=None, queue=None, dtype=None):
        """
        Clip (limit) the values in an array.
        """
        if is_fp(a):
            expr = "y0[i] = (isnan(x0[i]) ? NAN : clamp(x0[i], p0, p1))"
        elif is_complex(a):
            expr = "y0[i] = ({cond0} ? {expr0} : ({cond1} ? {expr1} : {default}))"
            cond0 = "({xr}<{p0r} || ({xr}=={p0r} && {xi}<{p0i}))"
            cond1 = "({xr}>{p1r} || ({xr}=={p1r} && {xi}>{p1i}))"
            expr0 = "p0"
            expr1 = "p1"
            default = "x0[i]"
            expr = expr.format(
                cond0=cond0, cond1=cond1, expr0=expr0, expr1=expr1, default=default
            )
            expr = expr.format(
                xr="x0[i].real",
                p0r="p0.real",
                p1r="p1.real",
                xi="x0[i].imag",
                p0i="p0.imag",
                p1i="p1.imag",
            )
        else:
            expr = "y0[i] = clamp(x0[i], p0, p1)"
            atype = a.dtype
        atype = a.dtype
        a_min = np.asarray(a_min, dtype=atype)
        a_max = np.asarray(a_max, dtype=atype)
        assert a_min <= a_max
        return self.unary_op(
            x0=a,
            expr=expr,
            out=out,
            extra_kargs=(a_min, a_max),
            queue=queue,
            dtype=dtype,
        )

    def clip_components(self, a, a_min, a_max, out=None, queue=None, dtype=None):
        """
        Clip (limit) the real and imaginary part of a complex number independantly.
        ie:
            a.real = clip(a.real, a_min.real, a_max.real)
            a.imag = clip(a.imag, a_min.imag, a_max.imag)
        """
        assert is_complex(a) and is_complex(a_min) and is_complex(a_max)
        assert a_min.real <= a_max.real
        assert a_min.imag <= a_max.imag
        a_min = np.asarray(a_min, dtype=a.dtype)
        a_max = np.asarray(a_max, dtype=a.dtype)
        expr0 = "y0[i].real = (isnan(x0[i].real) ? NAN : clamp(x0[i].real, p0.real, p1.real));"
        expr1 = "y0[i].imag = (isnan(x0[i].imag) ? NAN : clamp(x0[i].imag, p0.imag, p1.imag));"
        expr = expr0 + expr1
        return self.unary_op(
            x0=a,
            expr=expr,
            out=out,
            extra_kargs=(a_min, a_max),
            queue=queue,
            dtype=dtype,
        )

    def sqrt(self, x, out=None, queue=None, dtype=None):
        """
        Return the positive square-root of an array, element-wise.
        """
        return self._flt_or_cplx_unary_op(x, "sqrt", out=out, queue=queue, dtype=dtype)

    def cbrt(self, x, out=None, queue=None, dtype=None):
        """
        Return the cube-root of an array, element-wise.
        """
        assert not is_complex(x)
        expr = "y0[i] = cbrt(x0[i])"
        return self.unary_op(
            x0=x,
            expr=expr,
            out=out,
            queue=queue,
            dtype=dtype,
            convert_inputs="f",
            alloc_dtypes="f",
        )

    def square(self, x, out=None, queue=None, dtype=None):
        """
        Return the element-wise square of the input.
        """
        expr = "y0[i] = x0[i]*x0[i]"
        if is_complex(x):
            return self._cplx_binary_op(x, x, "mul", queue=queue, dtype=dtype, out=out)
        else:
            return self.unary_op(x0=x, expr=expr, out=out, queue=queue, dtype=dtype)

    def nan_to_num(self, x, out=None, queue=None, dtype=None):
        """
        Replace nan with zero and inf with finite numbers.
        """

        if is_fp(x) or is_complex(x):
            dtype = dtype or x.dtype
            if is_complex(dtype):
                ftype = complex_to_float_dtype(dtype)
            else:
                ftype = dtype
            info = np.finfo(ftype)
            max_val = np.asarray(info.max, dtype=ftype)
            min_val = np.asarray(info.min, dtype=ftype)

            if is_fp(x):
                # NG              expr = 'y0[i] = ( isnan(x0[i]) ? 0 : ( isinf(x0[i]) ? (signbit(x0[i]) ? p0 : p1) : x0[i] ) )'
                expr = (
                    clTools.dtype_to_ctype(dtype)
                    + " y0_tmp = (isinf(x0[i])  ? (signbit(x0[i]) ? p0 : p1) : x0[i]); \
                        y0[i]  = (isnan(y0_tmp) ? 0 : y0_tmp)"
                )
            elif is_complex(x):
                real = "(isnan(x0[i].real) ? 0 : (isinf(x0[i].real) ? (signbit(x0[i].real) ? p0 : p1) : x0[i].real))"
                imag = "(isnan(x0[i].imag) ? 0 : (isinf(x0[i].imag) ? (signbit(x0[i].imag) ? p0 : p1) : x0[i].imag))"
                expr = "y0[i] = {new}({real},{imag})".format(
                    real=real, imag=imag, new=self.complex_fn("new", dtype)
                )

            return self.unary_op(
                x0=x,
                expr=expr,
                out=out,
                extra_kargs=(min_val, max_val),
                queue=queue,
                dtype=dtype,
            )
        elif out:
            out.copyfrom(x)
        else:
            return x.astype(dtype=dtype or x.dtype, queue=queue)

    def real_if_close(self, a, tol=100, queue=None):
        """
        If complex input returns a real array if complex parts are close to zero.
        """
        if is_complex(a):
            if tol > 1:
                from numpy.core import getlimits

                f = getlimits.finfo(a.dtype.type)
                tol = f.eps * tol

            if self.reduce(
                kargs=(a,),
                neutral="true",
                reduce_expr="a&&b",
                map_expr="(fabs(x0[i].imag) < tol)",
                queue=queue,
                dtype=HYSOP_BOOL,
                extra_kargs=(np.float64(tol),),
                extra_arguments=["const double tol"],
            ).get():
                return a.real
            else:
                return a
        else:
            return a

    def maximum(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Element-wise maximum of array elements, priority to NaNs.
        """
        if is_fp(x1) and is_fp(x2):
            expr = "(isnan(x0[i]) ? NAN : (isnan(x1[i]) ? NAN : max(x0[i], x1[i])))"
        elif is_fp(x1):
            expr = "(isnan(x0[i]) ? NAN : max(x0[i], x1[i]))"
        elif is_fp(x2):
            expr = "(isnan(x1[i]) ? NAN : max(x0[i], x1[i]))"
        elif is_complex(x1) and is_complex(x2):
            default = "(((x0[i].real>x1[i].real) || ((x0[i].real==x1[i].real) && (x0[i].imag>x1[i].imag))) ? x0[i] : x1[i])"
            expr = "({} ? {} : ({} ? {} : {}))".format(
                "(isnan(x0[i].real) || isnan(x0[i].imag))",
                "x0[i]",
                "(isnan(x1[i].real) || isnan(x1[i].imag))",
                "x1[i]",
                default,
            )
        elif is_complex(x1):
            fromreal = self.complex_fn("fromreal", x1)
            default = f"(((x0[i].real>x1[i]) || ((x0[i].real==x1[i]) && (x0[i].imag>0))) ? x0[i] : {fromreal}(x1[i])"
            expr = "({} ? {} : ({} ? {} : {}))".format(
                "(isnan(x0[i].real) || isnan(x0[i].imag))",
                "x0[i]",
                "isnan(x1[i])",
                f"{fromreal}(NAN)",
                default,
            )
        elif is_complex(x2):
            fromreal = self.complex_fn("fromreal", x2)
            default = f"(((x0[i]>x1[i].real) || ((x0[i]==x1[i].real) && (0>x1[i].imag))) ? {fromreal}(x0[i]) : x1[i])"
            expr = "({} ? {} : ({} ? {} : {}))".format(
                "isnan(x0[i])",
                f"{fromreal}(x0[i])",
                "(isnan(x1[i].real) || isnan(x1[i].imag))",
                "x1[i]",
                default,
            )
        else:
            expr = "max(x0[i], x1[i])"
        expr = "y0[i] = " + expr
        return self.binary_op(
            x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
        )

    def minimum(self, x1, x2, out=None, queue=None, dtype=None):
        """
        Element-wise minimum of array elements, priority to NaNs.
        """
        if is_fp(x1) and is_fp(x2):
            expr = "(isnan(x0[i]) ? NAN : (isnan(x1[i]) ? NAN : min(x0[i], x1[i])))"
        elif is_fp(x1):
            expr = "(isnan(x0[i]) ? NAN : min(x0[i], x1[i]))"
        elif is_fp(x2):
            expr = "(isnan(x1[i]) ? NAN : min(x0[i], x1[i]))"
        elif is_complex(x1) and is_complex(x2):
            default = "(((x0[i].real<x1[i].real) || ((x0[i].real==x1[i].real) && (x0[i].imag<x1[i].imag))) ? x0[i] : x1[i])"
            expr = "({} ? {} : ({} ? {} : {}))".format(
                "(isnan(x0[i].real) || isnan(x0[i].imag))",
                "x0[i]",
                "(isnan(x1[i].real) || isnan(x1[i].imag))",
                "x1[i]",
                default,
            )
        elif is_complex(x1):
            fromreal = self.complex_fn("fromreal", x1)
            default = f"(((x0[i].real<x1[i]) || ((x0[i].real==x1[i]) && (x0[i].imag<0))) ? x0[i] : {fromreal}(x1[i])"
            expr = "({} ? {} : ({} ? {} : {}))".format(
                "(isnan(x0[i].real) || isnan(x0[i].imag))",
                "x0[i]",
                "isnan(x1[i])",
                f"{fromreal}(x1[i])",
                default,
            )
        elif is_complex(x2):
            fromreal = self.complex_fn("fromreal", x2)
            default = f"(((x0[i]<x1[i].real) || ((x0[i]==x1[i].real) && (0<x1[i].imag))) ? {fromreal}(x0[i]) : x1[i])"
            expr = "({} ? {} : ({} ? {} : {}))".format(
                "isnan(x0[i])",
                f"{fromreal}(x0[i])",
                "(isnan(x1[i].real) || isnan(x1[i].imag))",
                "x1[i]",
                default,
            )
        else:
            expr = "min(x0[i], x1[i])"
        expr = "y0[i] = " + expr
        return self.binary_op(
            x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
        )

    def fmax(self, x1, x2, out=None, queue=None, dtype=None, name="_fmax"):
        """
        Element-wise maximum of array elements, ignoring NaNs.
        """
        if is_fp(x1) and is_fp(x2):
            expr = "(isnan(x0[i]) ? x1[i] : (isnan(x1[i]) ? x0[i] : max(x0[i], x1[i])))"
        elif is_fp(x1):
            expr = "(isnan(x0[i]) ? x1[i] : max(x0[i], x1[i]))"
        elif is_fp(x2):
            expr = "(isnan(x1[i]) ? x0[i] : max(x0[i], x1[i]))"
        elif is_complex(x1) and is_complex(x2):
            default = "(((x0[i].real>x1[i].real) || ((x0[i].real==x1[i].real) && (x0[i].imag>x1[i].imag))) ? x0[i] : x1[i])"
            expr = "({} ? {} : ({} ? {} : {}))".format(
                "(isnan(x0[i].real) || isnan(x0[i].imag))",
                "x1[i]",
                "(isnan(x1[i].real) || isnan(x1[i].imag))",
                "x0[i]",
                default,
            )
        elif is_complex(x1):
            fromreal = self.complex_fn("fromreal", x1)
            default = f"(((x0[i].real>x1[i]) || ((x0[i].real==x1[i]) && (x0[i].imag>0))) ? x0[i] : {fromreal}(x1[i])"
            expr = "({} ? {} : ({} ? {} : {}))".format(
                "(isnan(x0[i].real) || isnan(x0[i].imag))",
                f"{fromreal}(x1[i])",
                "isnan(x1[i])",
                "x0[i]",
                default,
            )
        elif is_complex(x2):
            fromreal = self.complex_fn("fromreal", x2)
            default = f"(((x0[i]>x1[i].real) || ((x0[i]==x1[i].real) && (0>x1[i].imag))) ? {fromreal}(x0[i]) : x1[i])"
            expr = "({} ? {} : ({} ? {} : {}))".format(
                "isnan(x0[i])",
                "x1[i]",
                "(isnan(x1[i].real) || isnan(x1[i].imag))",
                f"{fromreal}(x0[i])",
                default,
            )
        else:
            expr = "max(x0[i], x1[i])"
        expr = "y0[i] = " + expr
        return self.binary_op(
            x0=x1, x1=x2, expr=expr, out=out, queue=queue, dtype=dtype
        )

    def fmin(self, x1, x2, out=None, queue=None, dtype=None, name="_fmin", **kwds):
        """
        Element-wise minimum of array elements, ignoring NaNs.
        """
        if is_fp(x1) and is_fp(x2):
            expr = "(isnan(x0[i]) ? x1[i] : (isnan(x1[i]) ? x0[i] : min(x0[i], x1[i])))"
        elif is_fp(x1):
            expr = "(isnan(x0[i]) ? x1[i] : min(x0[i], x1[i]))"
        elif is_fp(x2):
            expr = "(isnan(x1[i]) ? x0[i] : min(x0[i], x1[i]))"
        elif is_complex(x1) and is_complex(x2):
            default = "(((x0[i].real<x1[i].real) || ((x0[i].real==x1[i].real) && (x0[i].imag<x1[i].imag))) ? x0[i] : x1[i])"
            expr = "({} ? {} : ({} ? {} : {}))".format(
                "(isnan(x0[i].real) || isnan(x0[i].imag))",
                "x1[i]",
                "(isnan(x1[i].real) || isnan(x1[i].imag))",
                "x0[i]",
                default,
            )
        elif is_complex(x1):
            fromreal = self.complex_fn("fromreal", x1)
            default = f"(((x0[i].real<x1[i]) || ((x0[i].real==x1[i]) && (x0[i].imag<0))) ? x0[i] : {fromreal}(x1[i])"
            expr = "({} ? {} : ({} ? {} : {}))".format(
                "(isnan(x0[i].real) || isnan(x0[i].imag))",
                f"{fromreal}(x1[i])",
                "isnan(x1[i])",
                "x0[i]",
                default,
            )
        elif is_complex(x2):
            fromreal = self.complex_fn("fromreal", x2)
            default = f"(((x0[i]<x1[i].real) || ((x0[i]==x1[i].real) && (0<x1[i].imag))) ? {fromreal}(x0[i]) : x1[i])"
            expr = "({} ? {} : ({} ? {} : {}))".format(
                "isnan(x0[i])",
                "x1[i]",
                "(isnan(x1[i].real) || isnan(x1[i].imag))",
                f"{fromreal}(x0[i])",
                default,
            )
        else:
            expr = "min(x0[i], x1[i])"
        expr = "y0[i] = " + expr
        return self.binary_op(
            x0=x1,
            x1=x2,
            expr=expr,
            out=out,
            queue=queue,
            dtype=dtype,
            name=name,
            **kwds,
        )

    def fabs(self, x, out=None, queue=None, dtype=None):
        """
        Calculate the absolute value element-wise, outputs real values unless out or dtype
        is set.
        """
        assert not is_complex(x)
        if is_fp(x):
            expr = "y0[i] = fabs(x0[i])"
        else:
            expr = "y0[i] = abs(x0[i])"
        alloc_dtypes = "f"
        return self.unary_op(
            x0=x,
            expr=expr,
            out=out,
            queue=queue,
            dtype=dtype,
            alloc_dtypes=alloc_dtypes,
        )

    def absolute(self, x, out=None, queue=None, dtype=None):
        """
        Calculate the absolute value element-wise, ouputs values as input type
        unless out or dtype is set.
        """
        if is_complex(x):
            expr = "y0[i] = {}(x0[i])".format(self.complex_fn("abs", x))
            alloc_dtypes = (complex_to_float_dtype(x),)
        elif is_fp(x):
            expr = "y0[i] = fabs(x0[i])"
            alloc_dtypes = None
        else:
            expr = "y0[i] = abs(x0[i])"
            alloc_dtypes = None
        return self.unary_op(
            x0=x,
            expr=expr,
            out=out,
            queue=queue,
            dtype=dtype,
            alloc_dtypes=alloc_dtypes,
        )

    def sign(self, x, out=None, queue=None, dtype=None):
        """
        Returns an element-wise indication of the sign of a number.
        NaNs values are kept to NaNs to comply with numpy.
        """
        if is_fp(x):
            expr = "y0[i] = (isnan(x0[i]) ? NAN : sign(x0[i]))"
        elif is_complex(x):
            expr = "y0[i] = {}(isnan(x0[i].real) ? NAN : sign(x0[i].real))"
            expr = expr.format(self.complex_fn("fromreal", x))
        else:
            expr = "y0[i] = (sign(x0[i]))"
        return self.unary_op(
            x0=x, expr=expr, out=out, queue=queue, dtype=dtype, convert_inputs="f"
        )

    # RANDOM SAMPLING #
    ###################
    # See https://docs.scipy.org/doc/numpy/reference/routines.random.html

    # Simple random data

    def rand(
        self,
        shape=None,
        queue=None,
        order=default_order,
        dtype=HYSOP_REAL,
        out=None,
        a=0.0,
        b=1.0,
        generator=None,
    ):
        """
        Return samples from the uniform distribution [a,b].
        Default generator is clRandom.PhiloxGenerator.
        """
        self._check_argtype("rand", "shape", shape, tuple)
        queue = self.check_queue(queue)
        if generator is None:
            generator = clRandom.PhiloxGenerator(context=self.context)
        if out is None:
            out = self.empty(shape=shape, dtype=dtype, order=order, queue=queue)
        generator.fill_uniform(ary=out.handle, a=a, b=b, queue=queue)
        return out

    def randn(
        self,
        shape=None,
        queue=None,
        order=default_order,
        dtype=HYSOP_REAL,
        out=None,
        mu=0.0,
        sigma=1.0,
        generator=None,
        *args,
    ):
        """
        Return samples from the standard normal distribution [mu,sigma].
        Default generator is clRandom.PhiloxGenerator.
        """
        queue = self.check_queue(queue)
        self._check_argtype("randn", "shape", shape, tuple)
        if generator is None:
            generator = clRandom.PhiloxGenerator(context=self.context)
        if out is None:
            out = self.empty(shape=shape, dtype=dtype, order=order, queue=queue)
        generator.fill_normal(ary=out.handle, mu=mu, sigma=sigma, queue=queue)
        return out

    # STATISTICS #
    ##############
    # See https://docs.scipy.org/doc/numpy/reference/routines.sort.html

    # Order statistics

    def amin(self, a, axis=None, out=None, queue=None, name="amin", **kwds):
        """
        Return the minimum of an array.
        """
        return self.reduce(
            kargs=(a,),
            neutral="x0[0]",
            reduce_expr="min(a,b)",
            axis=axis,
            out=out,
            queue=queue,
            name=name,
            **kwds,
        )

    def amax(self, a, axis=None, out=None, slice=None, queue=None, name="amax", **kwds):
        """
        Return the maximum of an array.
        """
        return self.reduce(
            kargs=(a,),
            neutral="x0[0]",
            reduce_expr="max(a,b)",
            axis=axis,
            out=out,
            queue=queue,
            name=name,
            **kwds,
        )

    def nanmin(self, a, axis=None, out=None, queue=None, name="nanmin", **kwds):
        """
        Return the minimum of an array.
        """
        reduce_expr = "(isnan(a) ? b : (isnan(b) ? a : min(a,b)))"
        return self.reduce(
            kargs=(a,),
            neutral="x0[0]",
            reduce_expr=reduce_expr,
            axis=axis,
            out=out,
            queue=queue,
            name=name,
            **kwds,
        )

    def nanmax(
        self, a, axis=None, out=None, slice=None, queue=None, name="nanmax", **kwds
    ):
        """
        Return the maximum of an array.
        """
        reduce_expr = "(isnan(a) ? b : (isnan(b) ? a : max(a,b)))"
        return self.reduce(
            kargs=(a,),
            neutral="x0[0]",
            reduce_expr=reduce_expr,
            axis=axis,
            out=out,
            queue=queue,
            name=name,
            **kwds,
        )

    def average(self, a, axis=None, weights=None, returned=False, queue=None):
        """
        Compute the weighted average along the specified axis.
        """
        self._unsupported_argument("average", "returned", returned, False)
        if weights is None:
            return self.mean(a=a, axis=axis, queue=queue)
        else:
            arguments = "__global {ctype} const* x0, __global {ctype} const* w0"
            arguments = arguments.format(ctype=a.ctype())
            return self.reduce(
                kargs=(a,),
                neutral="0",
                reduce_expr="a+b",
                axis=axis,
                out=out,
                queue=queue,
                dtype=dtype,
                map_expr="w0[i]*x0[i]",
                arguments=arguments,
            ) / float(a.size)

    def mean(self, a, axis=None, dtype=None, out=None, queue=None):
        """
        Compute the arithmetic mean along the specified axis.
        """
        return a.sum(axis=axis, dtype=dtype, out=out, queue=queue) / float(a.size)

    def std(self, a, axis=None, dtype=None, out=None, ddof=0, queue=None):
        """
        Compute the standard deviation along the specified axis.
        """
        self._not_implemented_yet("std")

    def var(self, a, axis=None, dtype=None, out=None, ddof=0):
        """
        Compute the variance along the specified axis.
        """
        self._not_implemented_yet("var")

    def nanmedian(self, a, axis=None, out=None, overwrite_input=False):
        """
        Compute the median along the specified axis, while ignoring NaNs.
        """
        self._not_implemented_yet("nanmedian")

    def nanmean(self, a, axis=None, dtype=None, out=None):
        """
        Compute the arithmetic mean along the specified axis, ignoring NaNs.
        """
        self._not_implemented_yet("nanmean")

    def nanstd(self, a, axis=None, dtype=None, out=None, ddof=0):
        """
        Compute the standard deviation along the specified axis, while ignoring NaNs.
        """
        self._not_implemented_yet("nanstd")

    def nanvar(self, a, axis=None, dtype=None, out=None, ddof=0):
        """
        Compute the variance along the specified axis, while ignoring NaNs.
        """
        self._not_implemented_yet("nanvar")

    @staticmethod
    def build_codegen_arguments(
        args,
        name,
        typegen,
        ctype,
        mesh_dim,
        known_vars,
        symbolic_mode,
        itype="ulong",
        const=False,
        ptr=True,
        volatile=False,
        **kargs,
    ):

        from hysop.backend.device.codegen.base.utils import ArgDict
        from hysop.backend.device.codegen.base.variables import (
            CodegenVariable,
            CodegenVectorClBuiltin,
        )

        check_instance(args, ArgDict)

        # strides are in number of elements, offset in bytes
        base = f"{name}_base"
        strides = f"{name}_strides"
        offset = f"{name}_offset"
        assert base not in args
        assert offset not in args
        assert strides not in args

        assert "nl" not in kargs
        assert "add_impl_const" not in kargs
        assert "init" not in kargs
        assert mesh_dim in [1, 2, 3, 4, 8, 16]

        args[base] = CodegenVariable(
            name=base,
            typegen=typegen,
            ctype=ctype,
            ptr=ptr,
            const=const,
            volatile=volatile,
            add_impl_const=True,
            nl=False,
            **kargs,
        )

        args[strides] = CodegenVectorClBuiltin(
            name=strides,
            typegen=typegen,
            btype="uint",
            dim=mesh_dim,
            add_impl_const=True,
            nl=False,
        )

        args[offset] = CodegenVariable(
            name=offset, typegen=typegen, ctype=itype, add_impl_const=True, nl=False
        )

        if (offset in known_vars) and (strides in known_vars):
            args[base].nl = True
        elif offset in known_vars:
            args[strides].nl = True
        else:
            args[offset].nl = True

        char_alias = args[base].full_ctype(ctype="char", cast=True, align=True)
        ctype_alias = args[base].full_ctype(cast=True, align=True)

        if (not symbolic_mode) and (offset in known_vars):
            offset_str = typegen.dump(np.uint64(known_vars[offset]))
        else:
            offset_str = offset
        # init = '({})(({})({})+{})'.format(ctype_alias, char_alias, base, offset_str)
        init = f"{base} $+ {offset_str}"

        array = CodegenVariable(
            name=name,
            typegen=typegen,
            ctype=ctype,
            ptr=ptr,
            const=const,
            volatile=volatile,
            add_impl_const=True,
            nl=False,
            init=init,
            **kargs,
        )
        strides = args[strides]

        return array, strides


ArrayBackend._register_backend(clArray.Array, OpenClArrayBackend)
