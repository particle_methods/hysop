# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.core.memory.buffer import PooledBuffer
from hysop.backend.device.device_buffer import DeviceBuffer
from hysop.backend.device.opencl import cl


class OpenClBuffer(DeviceBuffer, cl.Buffer):
    """
    OpenCL buffer object.
    Simple wrapper of a pyopencl.Buffer.
    """

    def __init__(self, context, mem_flags, size=0, hostbuf=None):
        assert (hostbuf is not None) or (size > 0)
        super().__init__(context=context, flags=mem_flags, size=size, hostbuf=hostbuf)

    def get_int_ptr(self):
        return self.int_ptr

    def ref_count(self):
        return self.reference_count

    def aligned_view(self, alignment, size=None):
        assert alignment > 0
        assert not (alignment & (alignment - 1)), "alignment is not a power of 2."
        ptr = self.get_int_ptr()
        offset = -ptr % alignment
        if size is None:
            size = self.size - offset
        else:
            assert self.size >= (offset + size)
        if offset == 0:
            # Do NOT create a sub buffer if we do not have to, subbuffers are precious
            return self
        else:
            if self._DEBUG:
                print(f"Taking aligned subbuffer with alignment {alignment}.")
            return self[offset : offset + size]

    def __getitem__(self, key):
        if self._DEBUG:
            print(f"Getting opencl subbuffer view {key}.")
        return super().__getitem__(key)

    def release(self):
        super().release()


class OpenClPooledBuffer(PooledBuffer, cl.MemoryObjectHolder):
    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)

    def get_ptr(self):
        return self._bufview.ptr

    ptr = property(get_ptr)
