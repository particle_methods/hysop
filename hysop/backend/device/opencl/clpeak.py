# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


try:
    import cPickle as pickle
except:
    import pickle

import tempfile, re, os, warnings, gzip, portalocker, subprocess
from xml.dom import minidom
from hysop import vprint
from hysop.tools.decorators import requires_cmd
from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.units import bdw2str, flops2str, iops2str
from hysop.tools.warning import HysopWarning
from hysop.tools.io_utils import IO
from hysop.tools.cache import load_attributes_from_cache, update_cache, machine_id
from hysop.backend.hardware.hwinfo import HardwareStatistics


class ClPeakInfo:
    __FNULL = open(os.devnull, "w")
    __CMD_TIME_OUT = 60  # 60s timeout for clpeak calls
    __clpeak_bandwidth_units = {
        "bps": 1e00,
        "kbps": 1e03,
        "mbps": 1e06,
        "gbps": 1e09,
        "tbps": 1e12,
        "pbps": 1e15,
        "ebps": 1e18,
        "zbps": 1e21,
        "ybps": 1e24,
    }
    __clpeak_compute_units = {
        "flops": 1e00,
        "kflops": 1e03,
        "mflops": 1e06,
        "gflops": 1e09,
        "tflops": 1e12,
        "pflops": 1e15,
        "eflops": 1e18,
        "zflops": 1e21,
        "yflops": 1e24,
    }

    __int_results = ("int", "int2", "int4", "int8", "int16")
    __float_results = ("float", "float2", "float4", "float8", "float16")
    __double_results = ("double", "double2", "double4", "double8", "double16")

    __cached_datakeys = (
        "_preferred_exec_params",
        "_transfer_bandwidth_values",
        "_global_bdw_values",
        "_sp_compute_values",
        "_dp_compute_values",
        "_int_compute_values",
    )

    @classmethod
    def _cache_file(cls):
        return IO.cache_path() + "/hardware/clpeak.pklz"

    def _load_from_cache(self, key):
        filepath = self._cache_file()
        success = load_attributes_from_cache(
            filepath, key, self, self.__cached_datakeys
        )
        return success

    def _update_cache_data(self, key):
        filepath = self._cache_file()
        cached_data = dict(
            zip(
                self.__cached_datakeys,
                (getattr(self, cdk) for cdk in self.__cached_datakeys),
            )
        )
        update_cache(filepath, key, cached_data)

    def __init__(
        self,
        platform_name,
        device_name,
        platform_id,
        device_id,
        is_cpu,
        override_cache=False,
    ):

        self.platform_name = platform_name
        self.device_name = device_name
        self.platform_id = platform_id
        self.device_id = device_id
        self.is_cpu = is_cpu

        key = (machine_id, platform_name, device_name, platform_id, device_id, is_cpu)

        if override_cache:
            success = False
        else:
            success = self._load_from_cache(key)

        if not success:
            self._exec_node_id = None
            self._exec_cpu_id = None
            self._preferred_exec_params = None

            self._transfer_bandwidth_values = None
            self._gather_transfer_bandwidth_info(is_cpu)

            self._global_bdw_values = None
            self._gather_global_bandwidth_info()

            self._sp_compute_values = None
            self._dp_compute_values = None
            self._int_compute_values = None
            self._gather_compute_info()

            self._update_cache_data(key)

            del self._exec_node_id
            del self._exec_cpu_id

    @property
    def has_memory_bandwidth(self):
        return self._global_bdw_values is not None

    @property
    def has_transfer_bandwdith(self):
        return self._transfer_bandwidth_values is not None

    @property
    def has_single_precision_compute(self):
        return self._sp_compute_values is not None

    @property
    def has_double_precision_compute(self):
        return self._dp_compute_values is not None

    @property
    def has_integer_compute(self):
        return self._int_compute_values is not None

    @property
    def preferred_exec_params(self):
        return self._preferred_exec_params

    @property
    def transfer_bandwidth_values(self):
        return self._transfer_bandwidth_values

    @property
    def optimal_transfer_bandwidth_values(self):
        if self._transfer_bandwidth_values is None:
            return None
        else:
            return self._transfer_bandwidth_values[self._preferred_exec_params]

    @property
    def global_bdw_values(self):
        return self._global_bdw_values

    @property
    def mean_global_bdw(self):
        if self._global_bdw_values is None:
            return None
        else:
            return sum(self._global_bdw_values.values()) / len(self._global_bdw_values)

    @property
    def sp_compute_values(self):
        return self._sp_compute_values

    @property
    def dp_compute_values(self):
        return self._dp_compute_values

    @property
    def int_compute_values(self):
        return self._int_compute_values

    @property
    def max_sp_compute(self):
        if self._sp_compute_values is None:
            return None
        else:
            return max(self._sp_compute_values.values())

    @property
    def max_dp_compute(self):
        if self._dp_compute_values is None:
            return None
        else:
            return max(self._dp_compute_values.values())

    @property
    def max_int_compute(self):
        if self._int_compute_values is None:
            return None
        else:
            return max(self._int_compute_values.values())

    def __str__(self):
        ss = []
        if self.has_memory_bandwidth:
            ss += [f"Global memory bandwidth:         {bdw2str(self.mean_global_bdw)}"]
        if self.has_transfer_bandwdith:
            otbv = self.optimal_transfer_bandwidth_values
            read = otbv["enqueuereadbuffer"]
            write = otbv["enqueuewritebuffer"]
            mapped_read = otbv["memcpy_from_mapped_ptr"]
            mapped_write = otbv["memcpy_to_mapped_ptr"]
            ss += [f"Read  memory bandwidth:          {bdw2str(read)}"]
            ss += [f"Write memory bandwidth:          {bdw2str(write)}"]
            ss += [f"Read  memory bandwidth (mapped): {bdw2str(mapped_read)}"]
            ss += [f"Write memory bandwidth (mapped): {bdw2str(mapped_write)}"]
        if self.has_single_precision_compute:
            ss += [
                "Single precision compute:        {} (SP)".format(
                    flops2str(self.max_sp_compute)
                )
            ]
        if self.has_double_precision_compute:
            ss += [
                "Double precision compute:        {} (DP)".format(
                    flops2str(self.max_dp_compute)
                )
            ]
        if self.has_integer_compute:
            ss += [f"Integer compute:                 {iops2str(self.max_int_compute)}"]
        return "\n".join(ss)

    def _gather_transfer_bandwidth_info(self, is_cpu):

        def handle_results(dev, cmd):
            try:
                assert dev is not None
                tag = "transfer_bandwidth"
                result_tags = (
                    "enqueuewritebuffer",
                    "enqueuereadbuffer",
                    "enqueuemapbuffer",
                    "memcpy_from_mapped_ptr",
                    "enqueueunmap",
                    "memcpy_to_mapped_ptr",
                )
                bdw = dev.getElementsByTagName(tag)
                assert len(bdw) == 1
                bdw = bdw[0]
                unit = str(bdw.attributes["unit"].nodeValue).lower()
                assert unit in self.__clpeak_bandwidth_units, unit
                coeff = self.__clpeak_bandwidth_units[unit]
                transfer_bdw_values = {}
                for tag in result_tags:
                    attr = bdw.getElementsByTagName(tag)
                    assert len(attr) == 1
                    attr = attr[0]
                    transfer_bdw_values[tag] = coeff * float(
                        attr.firstChild.nodeValue.strip()
                    )
                return transfer_bdw_values
            except AssertionError as e:
                msg = "Failed to parse output of command:\n {}\n{}"
                msg = msg.format(cmd, e)
                warnings.warn(msg, HysopWarning)
                raise

        transfer_bandwidth = {}
        if is_cpu:
            self._set_exec_params(None, None)
            try:
                res = self._exec_clpeak(
                    test="--transfer-bandwidth", handle_results=handle_results
                )
                transfer_bandwidth[(None, None)] = res
            except (
                AssertionError,
                subprocess.CalledProcessError,
                subprocess.TimeoutExpired,
            ):
                pass
        else:
            node_id = 0
            core_id = 0
            exception_counter = 0
            while exception_counter < 2:
                self._set_exec_params(membind=node_id, cpubind=core_id)
                try:
                    res = self._exec_clpeak(
                        test="--transfer-bandwidth",
                        handle_results=handle_results,
                        catch=False,
                    )
                    transfer_bandwidth[(node_id, core_id)] = res
                    exception_counter = 0
                    core_id += 1
                    # only test 2 first cores
                    if core_id >= 2:
                        node_id += 1
                        core_id = 0
                except (subprocess.CalledProcessError, subprocess.TimeoutExpired):
                    exception_counter += 1
                    node_id += 1
                    core_id = 0
                except AssertionError:
                    break

        if len(transfer_bandwidth) > 0:

            def criteria(key):
                values = transfer_bandwidth[key]
                return -(values["enqueuewritebuffer"] + values["enqueuereadbuffer"])

            params = tuple(sorted(transfer_bandwidth.keys(), key=criteria))
            self._set_exec_params(*params[0])
            self._preferred_exec_params = params[0]
            self._transfer_bandwidth_values = transfer_bandwidth
        else:
            self._set_exec_params(None, None)
            self._transfer_bandwidth_values = None
            self._preferred_exec_params = (None, None)

    def _gather_global_bandwidth_info(self):
        def handle_results(dev, cmd):
            try:
                assert dev is not None
                bdw = dev.getElementsByTagName("global_memory_bandwidth")
                assert len(bdw) == 1
                bdw = bdw[0]
                unit = str(bdw.attributes["unit"].nodeValue).lower()
                assert unit in self.__clpeak_bandwidth_units, unit
                coeff = self.__clpeak_bandwidth_units[unit]
                global_bdw_values = {}
                for element in self.__float_results:
                    attr = bdw.getElementsByTagName(element)
                    assert len(attr) == 1
                    attr = attr[0]
                    global_bdw_values[element] = coeff * float(
                        attr.firstChild.nodeValue.strip()
                    )
                self._global_bdw_values = global_bdw_values
            except AssertionError as e:
                msg = "Failed to parse output of command:\n {}\n{}"
                msg = msg.format(cmd, e)
                warnings.warn(msg, HysopWarning)
                self._global_bdw_values = None

        self._exec_clpeak(test="--global-bandwidth", handle_results=handle_results)

    def _gather_compute_info(self):
        def handle_results(ctype):
            def _handle_results(dev, cmd):
                try:
                    assert dev is not None
                    compute_values = {}
                    if ctype == "float":
                        tag = "single_precision_compute"
                        result_tags = self.__float_results
                    elif ctype == "double":
                        tag = "double_precision_compute"
                        result_tags = self.__double_results
                    elif ctype == "int":
                        tag = "integer_compute"
                        result_tags = self.__int_results
                    else:
                        msg = f"Unkown ctype '{ctype}'."
                        raise NotImplementedError(msg)
                    compute = dev.getElementsByTagName(tag)
                    assert len(compute) == 1
                    compute = compute[0]
                    unit = str(compute.attributes["unit"].nodeValue).lower()
                    assert unit in self.__clpeak_compute_units, unit
                    coeff = self.__clpeak_compute_units[unit]
                    for tag in result_tags:
                        attr = compute.getElementsByTagName(tag)
                        assert len(attr) == 1
                        attr = attr[0]
                        compute_values[tag] = coeff * float(
                            attr.firstChild.nodeValue.strip()
                        )
                    if ctype == "float":
                        self._sp_compute_values = compute_values
                    elif ctype == "double":
                        self._dp_compute_values = compute_values
                    elif ctype == "int":
                        self._int_compute_values = compute_values
                    else:
                        msg = f"Unkown ctype '{ctype}'."
                        raise NotImplementedError(msg)
                except AssertionError as e:
                    msg = "Failed to parse output of command:\n {}\n{}"
                    msg = msg.format(cmd, e)
                    warnings.warn(msg, HysopWarning)
                    if ctype == "float":
                        self._sp_compute_values = None
                    elif ctype == "double":
                        self._dp_compute_values = None
                    elif ctype == "int":
                        self._int_compute_values = None
                    else:
                        msg = f"Unkown ctype '{ctype}'."
                        raise NotImplementedError(msg)

            return _handle_results

        self._exec_clpeak(
            test="--compute-sp", handle_results=handle_results(ctype="float")
        )
        self._exec_clpeak(
            test="--compute-dp", handle_results=handle_results(ctype="double")
        )
        self._exec_clpeak(
            test="--compute-integer", handle_results=handle_results(ctype="int")
        )

    def _set_exec_params(self, membind, cpubind):
        self._exec_node_id = membind
        self._exec_cpu_id = cpubind

    @requires_cmd("clpeak")
    @requires_cmd("hwloc-bind")
    def _exec_clpeak(self, test, handle_results, catch=True):
        with tempfile.NamedTemporaryFile(delete=True) as tmp:
            _platform = "--platform"
            _device = "--device"
            _xml = "--enable-xml-dump"
            _xml_file = "--xml-file"

            opts = []
            if (self._exec_node_id is not None) or (self._exec_cpu_id is not None):
                opts += ["hwloc-bind"]
                if self._exec_node_id is not None:
                    opts += ["--membind", f"node:{self._exec_node_id}"]
                if self._exec_node_id is not None:
                    opts += ["--cpubind", f"core:{self._exec_cpu_id}"]
                opts += ["--"]
            opts += [
                "clpeak",
                _platform,
                str(self.platform_id),
                _device,
                str(self.device_id),
                test,
                _xml,
                _xml_file,
                tmp.name,
            ]
            cmd = " ".join(opts)
            print(f"[CMD] {cmd}")
            if catch:
                try:
                    vprint(cmd)
                    subprocess.check_call(
                        opts, stdout=self.__FNULL, timeout=self.__CMD_TIME_OUT
                    )
                except subprocess.CalledProcessError as e:
                    msg = "Command\n {}\n failed with exit status {}."
                    msg = msg.format(cmd, e.returncode)
                    warnings.warn(msg, HysopWarning)
                    return None
                except subprocess.TimeoutExpired:
                    msg = "Command\n {}\n timed out after {}s."
                    msg = msg.format(cmd, self.__CMD_TIME_OUT)
                    warnings.warn(msg, HysopWarning)
                    return None
            else:
                subprocess.check_call(
                    opts,
                    stdout=self.__FNULL,
                    stderr=self.__FNULL,
                    timeout=self.__CMD_TIME_OUT,
                )
            tmp.flush()
            data = minidom.parse(tmp.name)
            plat = data.getElementsByTagName("platform")
            assert len(plat) == 1
            plat = plat[0]
            plat_name = str(plat.attributes["name"].nodeValue)
            if plat_name != str(self.platform_name):
                msg = "clpeak has a different platform ordering, "
                msg += "expected platform '{}' but got platform '{}'."
                msg = msg.format(self.platform_name, plat_name)
                raise RuntimeError(msg)
            dev = plat.getElementsByTagName("device")
            assert len(dev) == 1
            dev = dev[0]
            dev_name = str(dev.attributes["name"].nodeValue)
            if dev_name != self.device_name:
                msg = "clpeak has a different device ordering, "
                msg += "expected device '{}' but got device '{}'."
                msg = msg.format(self.device_name, dev_name)
                raise RuntimeError(msg)

            return handle_results(dev=dev, cmd=cmd)

    def stats(self):
        return ClPeakStatistics(self)


class ClPeakStatistics(HardwareStatistics):

    def __init__(self, info):
        self._platform_name = None
        self._device_name = None
        self._counter = 0

        self._transfer_bandwidth_values = []
        self._global_bdw_values = []
        self._sp_compute_values = []
        self._dp_compute_values = []
        self._int_compute_values = []

        if info is not None:
            check_instance(info, ClPeakInfo)
            self._platform_name = info.platform_name
            self._device_name = info.device_name
            self._counter += 1

            self._global_bdw_values += [info.mean_global_bdw]
            self._sp_compute_values += [info.max_sp_compute]
            self._dp_compute_values += [info.max_dp_compute]
            self._int_compute_values += [info.max_int_compute]

    def __iadd__(self, other):
        if other is None:
            return self
        if isinstance(other, ClPeakInfo):
            other = other.stats()
        if not isinstance(other, ClPeakStatistics):
            msg = "Unknown type {}, expected ClPeakInfo or ClPeakStatistics."
            msg = msg.format(type(other))
            raise TypeError(msg)
        if self._counter == 0:
            assert self._platform_name is None
            assert self._device_name is None
            self._platform_name = other._platform_name
            self._device_name = other._device_name
        else:
            assert self._platform_name == other._platform_name
            assert self._device_name == other._device_name
        self._counter += other._counter

        self._global_bdw_values += other._global_bdw_values
        self._sp_compute_values += other._sp_compute_values
        self._dp_compute_values += other._dp_compute_values
        self._int_compute_values += other._int_compute_values
        return self

    def to_string(self, indent=0, increment=2):
        ind = " " * indent
        inc = " " * increment
        ss = []
        ss += [f"{self._mean(self._global_bdw_values,  op=bdw2str):^14}"]
        ss += [f"{self._mean(self._sp_compute_values,  op=flops2str):^14}"]
        ss += [f"{self._mean(self._dp_compute_values,  op=flops2str):^14}"]
        ss += [f"{self._mean(self._int_compute_values, op=iops2str):^14}"]
        return " ".join(s.format(ind=ind, inc=inc) for s in ss)

    @property
    def has_memory_bandwidth(self):
        return len(self._global_bdw_values) > 0

    @property
    def has_transfer_bandwdith(self):
        return len(self._transfer_bandwidth_values) > 0

    @property
    def has_single_precision_compute(self):
        return len(self._sp_compute_values) > 0

    @property
    def has_double_precision_compute(self):
        return len(self._dp_compute_values) > 0

    @property
    def has_integer_compute(self):
        return len(self._int_compute_values) > 0

    @property
    def has_statistics(self):
        return (
            self.has_memory_bandwidth
            or self.has_transfer_bandwdith
            or self.has_single_precision_compute
            or self.has_double_precision_compute
            or self.has_integer_compute
        )
