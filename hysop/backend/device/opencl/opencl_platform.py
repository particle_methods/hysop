# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.htypes import check_instance
from hysop.backend.device.device_platform import Platform
from hysop.backend.device.opencl import cl
from hysop.backend.device.opencl.opencl_device import OpenClDevice
from hysop.backend.hardware.hwinfo import HardwareStatistics


class OpenClPlatform(Platform):

    def __init__(self, hardware_topo, platform_id, platform_handle=None, **kwds):
        if platform_handle is None:
            platforms = cl.get_platforms()
            if len(platforms) < platform_id:
                msg = "Requested OpenCL platform id {} but pyopencl only found {} platforms."
                msg = msg.format(platform_id, len(platforms))
                raise RuntimeError(msg)
            platform_handle = platforms[platform_id]

        self._name = platform_handle.name.strip()
        self._extensions = platform_handle.extensions
        self._profile = platform_handle.profile
        self._version = platform_handle.version
        self._real_name = platform_handle.name
        # we do not keep a reference to platform_handle as we
        # need to pickle this object

        super().__init__(
            hardware_topo=hardware_topo,
            platform_id=platform_id,
            platform_handle=platform_handle,
            **kwds,
        )

    @classmethod
    def handle_cls(cls):
        return cl.Platform

    def _discover_devices(self, hardware_topo, platform_handle):
        for i, device_handle in enumerate(platform_handle.get_devices()):
            dev = OpenClDevice(
                platform=self,
                platform_handle=platform_handle,
                device_id=i,
                device_handle=device_handle,
                hardware_topo=hardware_topo,
            )
            self._logical_devices[i] = dev

    def to_string(self, indent=0, increment=2):
        new_indent = indent + increment
        ind = " " * indent
        inc = " " * increment
        new_indent = indent + 2 * increment
        devices = "\n".join(
            x.to_string(indent=new_indent, increment=increment, short=True)
            for x in self.logical_devices.values()
        )
        sep = "\n{ind}{inc}{inc}{inc}|".format(ind=ind, inc=inc)
        extensions = sep + sep.join(
            e.strip()
            for e in sorted(self._extensions.split(" "))
            if e not in ("", " ", "\t", "\n")
        )
        ss = """{ind}>Platform {}: {}
{ind}{inc}*Version: {}
{ind}{inc}*Profile: {}
{}
{ind}{inc}*Extensions: {}
""".format(
            self._platform_id,
            self._name,
            self._version,
            self._profile,
            devices,
            extensions,
            ind=ind + inc,
            inc=inc,
        )
        return ss

    def __str__(self):
        return self.to_string()

    def stats(self):
        return OpenClPlatformStatistics(self)


class OpenClPlatformStatistics(HardwareStatistics):
    def __init__(self, platform=None):
        self._name = None
        self._counter = 0
        self._device_statistics = {}
        if platform is not None:
            check_instance(platform, OpenClPlatform)
            self._name = platform._name
            self._counter += 1
            for device_id, device in platform._logical_devices.items():
                self._device_statistics[device._name] = device.stats()

    @property
    def device_statistics(self):
        return self._device_statistics

    def __iadd__(self, other):
        if other is None:
            return self
        if isinstance(other, OpenClPlatform):
            other = other.stats()
        if not isinstance(other, OpenClPlatformStatistics):
            msg = (
                "Unknown type {}, expected OpenClPlatform or OpenClPlatformStatistics."
            )
            msg = msg.format(type(other))
            raise TypeError(msg)
        if self._counter == 0:
            assert self._name is None
            self._name = other._name
        assert self._name == other._name
        self._counter += other._counter
        for dname, dstats in other._device_statistics.items():
            if dname in self._device_statistics:
                self._device_statistics[dname] += dstats
            else:
                self._device_statistics[dname] = dstats
        return self

    def to_string(self, indent=0, increment=2):
        ind = " " * indent
        inc = " " * increment
        if self._device_statistics:
            ss = []
            for dname in sorted(
                self._device_statistics,
                key=lambda k: -self._device_statistics[k]._counter,
            ):
                dstats = self._device_statistics[dname]
                ss += [dstats.to_string(indent + increment, increment)]
        else:
            ss += ["{ind}{inc}No device found."]
        return "\n".join(s.format(ind=ind, inc=inc) for s in ss)
