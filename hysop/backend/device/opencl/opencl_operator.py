# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Abstract class providing a common interface to all
discrete operators working on the OpenCl backend.

* :class:`~hysop.backend.device.opencl.opencl_operator.OpenClOperator` is an abstract class
    used to provide a common interface to all discrete operators working on the
    opencl backend.
"""
from abc import ABCMeta

from hysop.constants import Precision, Backend, MemoryOrdering
from hysop.tools.decorators import debug
from hysop.tools.htypes import check_instance, first_not_None, InstanceOf
from hysop.tools.parameters import MPIParams
from hysop.tools.numpywrappers import npw
from hysop.core.graph.computational_operator import ComputationalGraphOperator
from hysop.core.graph.graph import op_apply
from hysop.backend.device.opencl.opencl_kernel_config import OpenClKernelConfig
from hysop.backend.device.opencl.opencl_tools import get_or_create_opencl_env
from hysop.backend.device.opencl.opencl_env import OpenClEnvironment
from hysop.topology.topology import Topology, TopologyView
from hysop.topology.topology_descriptor import TopologyDescriptor
from hysop.fields.discrete_field import DiscreteScalarFieldView


class OpenClOperator(ComputationalGraphOperator, metaclass=ABCMeta):
    """
    Abstract class for discrete operators working on OpenCL backends.
    """

    __default_method = {OpenClKernelConfig: OpenClKernelConfig()}

    __available_methods = {OpenClKernelConfig: InstanceOf(OpenClKernelConfig)}

    @classmethod
    def default_method(cls):
        dm = super().default_method()
        dm.update(cls.__default_method)
        return dm

    @classmethod
    def available_methods(cls):
        am = super().available_methods()
        am.update(cls.__available_methods)
        return am

    @debug
    def __new__(
        cls, cl_env=None, mpi_params=None, requested_symbolic_kernels=None, **kwds
    ):
        return super().__new__(cls, mpi_params=mpi_params, **kwds)

    @debug
    def __init__(
        self, cl_env=None, mpi_params=None, requested_symbolic_kernels=None, **kwds
    ):
        """
        Create the common attributes of all OpenCL operators.
        See handle_method() and setup().

        All input and output variable topologies should be of kind
        Backend.OPENCL and share the same OpenClEnvironment.

        Attributes
        ----------
        cl_env: OpenClEnvironment
            OpenCL environment shared accross all topologies.
        Notes
        -----
        About method keys:
            OpenClKernelConfig: user build options, defines, precision
                                and autotuner configuration
        """
        check_instance(cl_env, OpenClEnvironment, allow_none=True)
        check_instance(mpi_params, MPIParams, allow_none=True)

        msg = "mpi_params was {} and cl_env was {}."

        for topo in set(kwds.get("input_fields", {}).values()).union(
            kwds.get("output_fields", {}).values()
        ):
            if isinstance(topo, Topology):
                if cl_env is None:
                    cl_env = topo.backend.cl_env
                else:
                    assert cl_env is topo.backend.cl_env, "Topology cl_env mismatch."

        if mpi_params is None:
            if cl_env is None:
                _vars = kwds.get("input_fields", None) or kwds.get(
                    "output_fields", None
                )
                if not _vars:
                    msg = "Cannot deduce domain without input or output fields."
                    raise RuntimeError(msg)
                domain = next(iter(_vars)).domain
                mpi_params = MPIParams(
                    comm=domain.task_comm, task_id=domain.current_task()
                )
                cl_env = get_or_create_opencl_env(mpi_params)
                msg = msg.format("None", "None")
            else:
                mpi_params = cl_env.mpi_params
                msg = msg.format("None", "given")
        else:
            if cl_env is None:
                cl_env = get_or_create_opencl_env(mpi_params)
                msg = msg.format("given", "None")
            else:
                cl_env = cl_env
                msg = msg.format("given", "given")

        super().__init__(mpi_params=mpi_params, **kwds)
        self.cl_env = cl_env

        if cl_env.mpi_params != self.mpi_params:
            msg0 = "MPI Communicators do not match between OpenClEnvironment and MPIParams."
            msg0 += f"\n  => {msg}"
            raise RuntimeError(msg0)

    @classmethod
    def supported_backends(cls):
        """
        Return the backends that this operator's topologies can support.
        """
        return {Backend.OPENCL}

    @debug
    def handle_method(self, method):
        """
        Extract device configuration and precision from OpenClKernelConfig.
        """
        from hysop.backend.device.opencl.opencl_tools import convert_precision

        super().handle_method(method)

        assert OpenClKernelConfig in method

        kernel_config = method.pop(OpenClKernelConfig)
        autotuner_config = kernel_config.autotuner_config

        precision = kernel_config.precision
        float_dump_mode = kernel_config.float_dump_mode
        use_short_circuit_ops = kernel_config.use_short_circuit_ops
        unroll_loops = kernel_config.unroll_loops

        if precision == Precision.SAME:
            precisions = tuple(f.dtype for f in self.fields)
            if npw.float64 in precisions:
                precision = Precision.DOUBLE
            elif npw.float32 in precisions:
                precision = Precision.FLOAT
            elif npw.float16 in precisions:
                precision = Precision.HALF
            else:
                precision = Precision.DEFAULT

        if precision in [Precision.LONG_DOUBLE, Precision.QUAD]:
            msg = "Precision {} is not supported yet for OpenCl environment."
            msg = msg.format(precision)
            raise NotImplementedError(msg)

        self.typegen = self.cl_env.build_typegen(
            precision=precision,
            float_dump_mode=float_dump_mode,
            use_short_circuit_ops=use_short_circuit_ops,
            unroll_loops=unroll_loops,
        )
        self.autotuner_config = autotuner_config
        self.kernel_config = kernel_config
        self.precision = convert_precision(precision)

        self._initialize_cl_build_options(kernel_config.user_build_options)
        self._initialize_cl_size_constants(kernel_config.user_size_constants)
        self._initialize_kernel_generator()

    def check(self):
        super().check()
        self._check_cl_env()

    @debug
    def create_topology_descriptors(self):
        # by default we create OPENCL (gpu) TopologyDescriptors
        for field, topo_descriptor in self.input_fields.items():
            topo_descriptor = TopologyDescriptor.build_descriptor(
                backend=Backend.OPENCL,
                operator=self,
                field=field,
                handle=topo_descriptor,
                cl_env=self.cl_env,
            )
            self.input_fields[field] = topo_descriptor

        for field, topo_descriptor in self.output_fields.items():
            topo_descriptor = TopologyDescriptor.build_descriptor(
                backend=Backend.OPENCL,
                operator=self,
                field=field,
                handle=topo_descriptor,
                cl_env=self.cl_env,
            )
            self.output_fields[field] = topo_descriptor

    @debug
    def get_field_requirements(self):
        """
        Called just after handle_method(), ie self.method has been set.
        topology requirements are:
            1) min and max ghosts for each input and output variables
            2) allowed splitting directions for cartesian topologies
            3) required local and global transposition state, if any.
            and more
        they are stored in self.input_field_requirements and
        self.output_field_requirements.

        keys are continuous fields and values are of type
        hysop.fields.field_requirement.discretefieldrequirements

        default is backend.opencl, no min or max ghosts and no specific
        transposition state for each input and output variables.
        """
        requirements = super().get_field_requirements()

        for is_input, reqs in requirements.iter_requirements():
            if reqs is None:
                continue
            (field, td, req) = reqs
            req.memory_order = MemoryOrdering.C_CONTIGUOUS

        return requirements

    @debug
    def discretize(self):
        if self.discretized:
            return
        super().discretize()

    @debug
    def setup(self, work):
        super().setup(work)
        self.check_memory_order()

    def check_memory_order(self):
        for df in self.input_discrete_fields.values():
            self.check_dfield_memory_order(df)
        for df in self.output_discrete_fields.values():
            self.check_dfield_memory_order(df)

    def check_dfield_memory_order(self, df):
        check_instance(df, DiscreteScalarFieldView)
        data = df.sdata
        if not data.flags.c_contiguous:
            msg = "At least one OpenClOperator data view has no C ordering "
            msg += "for discrete scalar field {} in operator {}."
            msg += "\nTo explicitely allow some fortran ordered arrays in a OpenClOperator, "
            msg += "please redefine the check_dfield_memory_order method."
            msg = msg.format(df.name, self.name)
            raise RuntimeError(msg)

    def build_options(self):
        """
        Build and return opencl build option string from
        self._cl_build_options and self._cl_defines.
        """
        build_options = self._cl_build_options
        defines = set()
        for define, value in self._cl_defines.items():
            if value is not None:
                define = f"{define.strip()}={value.strip()}"
            else:
                define = define.strip()
            define = f"-D{define}"
            defines.update(define)
        return tuple(build_options) + tuple(defines)

    @debug
    def _check_cl_env(self):
        """
        Check if all topologies are on OpenCL backend and check that all opencl environments
        match.
        """
        topologies = set(self.input_fields.values()).union(self.output_fields.values())
        if not topologies:
            return
        topo = next(iter(topologies))
        assert isinstance(topo, TopologyView)
        assert topo.backend.kind == Backend.OPENCL
        ref_env = self.cl_env

        for topo in topologies:
            assert isinstance(topo, TopologyView)
            assert topo.backend.kind == Backend.OPENCL
            assert topo.backend.cl_env == ref_env

    @debug
    def _initialize_cl_build_options(self, user_options):
        """
        Initialize OpenCl build options.
        """
        check_instance(user_options, list)
        build_options = set()
        build_options.update(self.cl_env.default_build_opts)
        build_options.update(self.typegen.ftype_build_options())
        build_options.update(user_options)
        self._cl_build_options = build_options

    @debug
    def _initialize_cl_size_constants(self, user_size_constants):
        """
        Initialize compile time constants (preprocessor defines) for kernels.
        """
        check_instance(user_size_constants, list)
        cl_defines = {}
        for usc in user_size_constants:
            if isinstance(usc, tuple):
                assert len(usc) == 2
                cl_defines[usc[0]] = usc[1]
            else:
                cl_defines[usc[0]] = None
        self._cl_defines = cl_defines

    def _initialize_kernel_generator(self):
        """
        Initialize a OpenClElementwiseKernelGenerator.
        """
        from hysop.backend.device.opencl.opencl_elementwise import (
            OpenClElementwiseKernelGenerator,
        )

        self.elementwise_kernel_generator = OpenClElementwiseKernelGenerator(
            cl_env=self.cl_env, kernel_config=self.kernel_config
        )

    @classmethod
    def supports_multiple_topologies(cls):
        return True
