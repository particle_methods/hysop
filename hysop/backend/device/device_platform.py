# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABCMeta, abstractmethod


class Platform(metaclass=ABCMeta):

    def __init__(self, hardware_topo, platform_handle, platform_id, **kwds):
        super().__init__(**kwds)
        self._platform_id = platform_id
        self._logical_devices = {}
        self._discover_devices(hardware_topo, platform_handle)
        # we do not keep a reference to platform_handle as we
        # need to pickle this object

    @property
    def platform_id(self):
        return self._platform_id

    @property
    def logical_devices(self):
        return self._logical_devices

    @property
    def physical_devices(self):
        return [dev.physical_device() for dev in self.logical_devices()]

    @abstractmethod
    def _discover_devices(self, hardware_topo, platform_handle):
        pass
