# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.htypes import check_instance
from hysop.tools.units import time2str


class KernelStatistics:
    """
    Execution statistics extracted from kernel events.
    """

    def __init__(self, min_, max_, total, nruns, data=None, **kwds):
        """
        Initialize KernelStatistics from nruns.
        Statistics should be given in nanoseconds.
        """
        super().__init__(**kwds)
        check_instance(min_, int, allow_none=True)
        check_instance(max_, int, allow_none=True)
        check_instance(total, int, allow_none=True)
        check_instance(nruns, int)
        check_instance(data, (list, tuple), allow_none=True)
        self._min = min_
        self._max = max_
        self._total = total
        self._nruns = nruns
        self._data = None if (data is None) else tuple(data)

    def _get_min(self):
        return self._min

    def _get_max(self):
        return self._max

    def _get_mean(self):
        assert self._nruns > 0
        return self._total / float(self._nruns)

    def _get_total(self):
        return self._total

    def _get_nruns(self):
        return self._nruns

    def _get_data(self):
        return self._data

    min = property(_get_min)
    max = property(_get_max)
    mean = property(_get_mean)
    total = property(_get_total)
    nruns = property(_get_nruns)
    data = property(_get_data)

    @staticmethod
    def cmp(lhs, rhs):
        """Compare two KernelStatistics."""
        if lhs.mean == rhs.mean:
            if lhs.max == rhs.max:
                if lhs.min == rhs.min:
                    return 0
                elif lhs.min > rhs.min:
                    return 1
                else:
                    return -1
            elif lhs.max > rhs.max:
                return 1
            else:
                return -1
        elif lhs.mean > rhs.mean:
            return 1
        else:
            return -1

    def __lt__(self, other):
        return self.cmp(self, other) < 0

    def __gt__(self, other):
        return self.cmp(self, other) > 0

    def __eq__(self, other):
        return self.cmp(self, other) == 0

    def __le__(self, other):
        return self.cmp(self, other) <= 0

    def __ge__(self, other):
        return self.cmp(self, other) >= 0

    def __ne__(self, other):
        return self.cmp(self, other) != 0

    def __iadd__(self, other):
        if other.nruns == 0:
            return
        if self.nruns == 0:
            self._nruns = other.nruns
            self._min = other.min
            self._max = other.max
            self._total = other.total
            self._data = tuple(other.data)
        else:
            self._min = min(self.min, other.min)
            self._max = max(self.max, other.max)
            self._nruns += other.nruns
            self._total += other.total
            self._data += other.data
        return self

    def __str__(self):
        mini = float(self.min) * 1e-9  # ns
        maxi = float(self.max) * 1e-9  # ns
        mean = float(self.mean) * 1e-9  # ns
        return "min={}, max={}, mean={} (nruns={})".format(
            time2str(mini), time2str(maxi), time2str(mean), self.nruns
        )
