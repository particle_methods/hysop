# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
import sympy as sm

from hysop.tools.htypes import check_instance
from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.codegen.base.function_codegen import (
    OpenClFunctionCodeGenerator,
)
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
)
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.codegen.base.utils import WriteOnceDict, ArgDict
from hysop.backend.device.codegen.base.statistics import WorkStatistics


class PolynomialFunction(OpenClFunctionCodeGenerator):

    def __init__(
        self, typegen, ftype, dim, coeffs, name, var="x", use_fma=True, known_args=None
    ):

        vtype = typegen.vtype(ftype, dim)

        args = self.build_prototype(typegen, ftype, dim, var)

        reqs = self.build_requirements(typegen, ftype, dim)

        super().__init__(
            basename=name,
            output=vtype,
            typegen=typegen,
            inline=True,
            args=args,
            known_args=known_args,
        )

        self.update_requirements(reqs)

        self.dim = dim
        self.ftype = ftype
        self.vtype = vtype
        self.coeffs = np.asarray(coeffs)
        self.use_fma = use_fma
        self.var = var

        self.gencode()

    def build_prototype(self, typegen, ftype, dim, var):
        args = ArgDict()
        args[var] = CodegenVectorClBuiltin(
            var, ftype, dim, typegen, add_impl_const=True
        )
        return args

    def build_requirements(self, typegen, ftype, dim):
        reqs = WriteOnceDict()
        return reqs

    def gencode(self):
        s = self
        dim = s.dim
        tg = self.typegen

        ftype = s.ftype
        vtype = s.vtype

        coeffs = s.coeffs
        use_fma = s.use_fma

        assert coeffs.size >= 1

        x = s.args[s.var]

        C = [tg.dump(float(Ci)) if Ci != 0 else None for Ci in coeffs]

        with s._function_():
            mul = "{}*{}"
            fma = "fma({},{},{})" if use_fma else "({}*{}+{})"

            i = 0
            while C[i] is None:
                i += 1
            P = C[i]
            for Ci in C[i + 1 :]:
                if Ci is None:
                    P = mul.format(x, P)
                else:
                    P = fma.format(x, P, Ci)
            P = f"return {P};"
            s.append(P)

    def per_work_statistics(self):
        from hysop.backend.device.codegen.base.statistics import WorkStatistics

        dim = self.dim
        ftype = self.ftype
        ncoeffs = np.sum(self.coeffs != 0)

        stats = WorkStatistics()
        stats.ops_per_type[ftype] = 2 * (ncoeffs - 1) * dim

        return stats


if __name__ == "__main__":

    from hysop.backend.device.codegen.base.test import _test_typegen

    tg = _test_typegen("float")
    pf = PolynomialFunction(
        tg,
        "float",
        4,
        [0, 1, 2, 3, 4, 0, 0, 0, 5, 6, 7, 8, 9, 0],
        "test_poly",
        "x",
        True,
    )
    pf.edit()

    print(pf.per_work_statistics())

    pf.test_compile()
