# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.backend.device.codegen.base.codegen import CodeGenerator
from hysop.backend.device.codegen.base.function_codegen import FunctionCodeGenerator
from hysop.backend.device.codegen.base.variables import CodegenVariable
from hysop.backend.device.opencl.opencl_types import TypeGen
from hysop.backend.device.codegen.base.utils import ArgDict


class EmptyFunction(FunctionCodeGenerator):

    def __init__(self, typegen):
        args = ArgDict()
        args["size"] = CodegenVariable(
            "size", "unsigned int", add_impl_const=True, typegen=typegen
        )
        args["data"] = CodegenVariable(
            typegen.fbtype, "data", ptr=True, const=True, typegen=typegen
        )
        fname = "empty_function"
        name = f"{fname}_{typegen.fbtype[0]}"
        super().__init__(
            basename=fname, ext=".c", output="void", args=args, typegen=typegen
        )
        self.gencode()

    def gencode(self):
        s = self
        with s._function_():
            s._noop()


if __name__ == "__main__":
    cg = CodeGenerator("main", TypeGen("float"))

    h = EmptyFunction(TypeGen("half"))
    f = EmptyFunction(TypeGen("float"))
    d = EmptyFunction(TypeGen("double"))
    cg.require(h.name, h)
    cg.require(f.name, f)
    cg.require(d.name, d)

    cg.edit()
