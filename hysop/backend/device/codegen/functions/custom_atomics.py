# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.codegen.base.function_codegen import (
    OpenClFunctionCodeGenerator,
)
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
)
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.codegen.base.utils import WriteOnceDict, ArgDict


class CustomAtomicFunction(OpenClFunctionCodeGenerator):

    def __init__(self, typegen, ftype, op, storage="__global", opname=None):

        reqs, union = self.build_union(ftype, typegen)

        args = ArgDict()
        args["p"] = CodegenVariable(
            "p",
            ftype,
            add_impl_const=False,
            typegen=typegen,
            ptr=True,
            volatile=True,
            storage=storage,
        )
        args["val"] = CodegenVariable(
            "val", ftype, add_impl_const=True, typegen=typegen
        )

        fname = "custom_atomic_{}".format(
            f"op{hex(hash(op))[2:6]}" if (opname is None) else opname
        )
        super().__init__(basename=fname, output=ftype, args=args, typegen=typegen)

        self.op = op
        self.ftype = ftype
        self.storage = storage

        if typegen.opencl_version_greater(1, 0):
            if ftype == "float":
                atomic_cmpxchg = "atomic_cmpxchg({ptr},{intcmp},{val});"
            elif ftype == "double":
                self.declare_cl_extension("cl_khr_int64_base_atomics")
                atomic_cmpxchg = "atom_cmpxchg({ptr},{intcmp},{val});"
            else:
                raise NotImplementedError(f"Unknown ftype {ftype}.")
        else:
            if ftype == "float":
                if storage == "__global":
                    self.declare_cl_extension("cl_khr_global_int32_base_atomics")
                elif storage == "__local":
                    self.declare_cl_extension("cl_khr_local_int32_base_atomics")
                else:
                    raise NotImplementedError(f"Unknown storage {storage}.")
            elif ftype == "double":
                self.declare_cl_extension("cl_khr_int64_base_atomics")
            else:
                raise NotImplementedError(f"Unknown ftype {ftype}.")
            atomic_cmpxchg = "atom_cmpxchg({ptr},{intcmp},{val});"

        self.update_requirements(reqs)
        self.gencode(atomic_cmpxchg)

    def build_union(self, ftype, tg):
        from hysop.backend.device.codegen.unions.float_int import FloatIntegerUnion

        reqs = WriteOnceDict()
        union = FloatIntegerUnion(ftype, tg)
        reqs["union"] = union
        return reqs, union

    def gencode(self, atomic_cmpxchg):
        s = self
        tg = s.typegen
        op = s.op

        p = s.args["p"]
        val = s.args["val"]

        union = self.reqs["union"]
        old_val = union.build_codegen_variable("old_val")
        new_val = union.build_codegen_variable("new_val")
        ret_val = union.build_codegen_variable("ret_val")
        alias = p.pointer_alias(name="ip", ctype=old_val.intval.ctype)
        with s._function_():
            old_val.declare(s)
            new_val.declare(s)
            ret_val.declare(s)
            s.jumpline()
            alias.declare(s)
            with s._do_while_(f"{ret_val.intval} != {old_val.intval}"):
                load = f"{old_val.floatval} = *{p};"
                op = f"{new_val.floatval} = {op.format(old_val.floatval, val)};"
                cmpxchg = "{} = {}".format(
                    ret_val.intval,
                    atomic_cmpxchg.format(
                        ptr=alias, intcmp=old_val.intval, val=new_val.intval
                    ),
                )
                s.append(load)
                s.append(op)
                s.append(cmpxchg)
            s.jumpline()
            s.append(f"return {ret_val.floatval};")


if __name__ == "__main__":
    from hysop.backend.device.codegen.base.test import _test_typegen

    tg = _test_typegen("double")
    cg = OpenClCodeGenerator("main", tg)

    h = CustomAtomicFunction(tg, "float", "{} + {}", "__global")
    f = CustomAtomicFunction(tg, "double", "{} + {}", "__local")
    cg.require(h.name, h)
    cg.require(f.name, f)

    cg.edit()

    cg.test_compile()
