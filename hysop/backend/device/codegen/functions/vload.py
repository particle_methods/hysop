# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.htypes import check_instance, first_not_None
from hysop.backend.device.codegen.base.codegen import CodeGenerator
from hysop.backend.device.codegen.base.function_codegen import (
    OpenClFunctionCodeGenerator,
)
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
)
from hysop.backend.device.opencl.opencl_types import TypeGen
from hysop.backend.device.codegen.base.utils import ArgDict


class Vload(OpenClFunctionCodeGenerator):

    def __init__(
        self,
        typegen,
        ptype,
        vectorization,
        default_val="0",
        itype="int",
        restrict=True,
        storage=None,
        known_args=None,
        use_short_circuit=None,
    ):
        check_instance(ptype, str)
        check_instance(itype, str)
        check_instance(restrict, bool)
        assert vectorization in typegen.vsizes

        fname = f"vload_{ptype}{vectorization}"
        ctype = typegen.vtype(ptype, vectorization)
        check_instance(use_short_circuit, bool, allow_none=True)

        use_short_circuit = first_not_None(
            use_short_circuit, typegen.use_short_circuit_ops
        )

        args = ArgDict()
        args["data"] = CodegenVariable(
            "data",
            ptype,
            ptr=True,
            const=True,
            add_impl_const=True,
            storage=storage,
            ptr_restrict=restrict,
            typegen=typegen,
        )
        args["offset"] = CodegenVariable(
            "offset", itype, add_impl_const=True, typegen=typegen
        )
        args["size"] = CodegenVariable(
            "size", itype, add_impl_const=True, typegen=typegen
        )

        super().__init__(
            basename=fname,
            output=ctype,
            args=args,
            typegen=typegen,
            known_args=known_args,
            inline=True,
        )

        self.vectorization = vectorization
        self.ctype = ctype
        self.default_val = default_val
        self.use_short_circuit = use_short_circuit

        self.gencode()

    def gencode(self):
        s = self
        typegen = s.typegen
        offset = s.args["offset"]
        data = s.args["data"]
        size = s.args["size"]
        vectorization = s.vectorization
        default_val = s.default_val
        use_short_circuit = s.use_short_circuit

        ret = CodegenVectorClBuiltin("res", data.ctype, vectorization, typegen)

        with s._function_():
            vcond = "({i}>=0) && ({i}<({}-{}))".format(
                size, vectorization - 1, i=offset
            )
            scond = "(({i}>=0) $&& ({i}<{}))"
            ret.declare(s)
            with s._if_(vcond):
                load = s.vload(vectorization, data, offset, offset_is_ftype=True)
                ret.affect(s, init=load)
            with s._else_():
                with s._align_() as al:
                    for j in range(vectorization):
                        offsetj = f"{offset}+{j}"
                        scondj = scond.format(size, i=offsetj)
                        loadj = data[offsetj]
                        defaultj = default_val
                        if use_short_circuit:
                            code = f"{ret[j]} $= ({scondj} $? {loadj} $: {defaultj});"
                        else:
                            code = "if {} ${{ {var} $= {}; $}} else {{ {var} = {}; }};".format(
                                scondj, loadj, defaultj, var=ret[j]
                            )
                        al.append(code)
            s._return(ret)


if __name__ == "__main__":
    from hysop.backend.device.codegen.base.test import _test_typegen

    tg = _test_typegen()

    f = Vload(tg, "double", 16)
    f.edit()
    f.test_compile()
