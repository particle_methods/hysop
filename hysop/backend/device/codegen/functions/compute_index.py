# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.codegen.base.function_codegen import (
    OpenClFunctionCodeGenerator,
)
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
)
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.codegen.base.utils import ArgDict
from hysop.backend.device.codegen.base.statistics import WorkStatistics


class ComputeIndexFunction(OpenClFunctionCodeGenerator):

    def __init__(self, typegen, dim, wrap=None, itype="int"):
        assert dim > 0
        args = ArgDict()
        args["idx"] = CodegenVectorClBuiltin(
            "idx", "int", dim, typegen, add_impl_const=bool(wrap == False)
        )
        args["size"] = CodegenVectorClBuiltin(
            "size", itype, dim, typegen, add_impl_const=True
        )
        args["wrap"] = CodegenVariable(
            "wrap", "bool", typegen, add_impl_const=True, value=wrap
        )

        basename = f"compute_index_{dim}d"
        if wrap is not None:
            basename += "_" + ("" if wrap else "no") + "wrap"
        super().__init__(
            basename=basename,
            ext=".c",
            output=itype,
            args=args,
            typegen=typegen,
            inline=True,
        )
        self.dim = dim
        self.wrap = wrap
        self.itype = itype

        self.gencode()

    def gencode(self):
        s = self
        dim = s.dim
        idx = s.args["idx"]
        size = s.args["size"]
        wrap = s.args["wrap"]
        with s._function_():
            with s._if_(wrap()):
                self.append(
                    "{idx} = ({idx}+{size}) % {size};".format(idx=idx(), size=size)
                )
            ss = f"{idx[dim-1]}"
            for i in range(dim - 2, -1, -1):
                ss = f"({ss}*{size[i]}+{idx[i]})"
            ret = f"return {ss};"
            self.append(ret)

    def per_work_statistics(self):
        dim = self.dim
        wrap = self.wrap
        itype = self.itype

        ops = {}
        ops[itype] = int(wrap) * 2 * dim + 2 * (dim - 1) * dim

        stats = WorkStatistics()
        stats.ops_per_type = ops

        return stats


if __name__ == "__main__":
    from hysop.backend.device.codegen.base.test import test_typegen

    tg = test_typegen("float")
    cg = OpenClCodeGenerator("main", tg)

    ci0 = ComputeIndexFunction(tg, 3, wrap=None)
    ci1 = ComputeIndexFunction(tg, 3, wrap=True)
    ci2 = ComputeIndexFunction(tg, 3, wrap=False)

    cg.require(ci0.name, ci0)
    cg.require(ci1.name, ci1)
    cg.require(ci2.name, ci2)

    cg.edit()
