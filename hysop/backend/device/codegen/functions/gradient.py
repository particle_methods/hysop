# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np

from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.codegen.base.function_codegen import (
    OpenClFunctionCodeGenerator,
)
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
)
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.codegen.base.utils import ArgDict

from hysop.numerics.stencil.stencil import Stencil

from hysop.backend.device.codegen.functions.compute_index import ComputeIndexFunction
from hysop.backend.device.codegen.functions.cache_load import CacheLoadFunction
from hysop.backend.device.codegen.functions.apply_stencil import ApplyStencilFunction


class GradientFunction(OpenClFunctionCodeGenerator):

    def __init__(self, typegen, dim, order, itype="int", cached=True, known_args=None):

        assert dim > 0
        fbtype = typegen.fbtype
        output = typegen.vtype(fbtype, dim)
        tg = typegen

        args = ArgDict()
        args["field"] = CodegenVariable(
            "field",
            fbtype,
            typegen=tg,
            ptr=True,
            const=True,
            storage="__global",
            nl=True,
        )
        if cached:
            args["buffer"] = CodegenVariable(
                "buffer",
                fbtype,
                typegen=tg,
                ptr=True,
                const=False,
                storage="__local",
                nl=True,
            )
        args["global_id"] = CodegenVectorClBuiltin(
            "gid", "int", 3, typegen, add_impl_const=True
        )
        args["local_id"] = CodegenVectorClBuiltin(
            "lid", "int", 3, typegen, add_impl_const=True
        )
        args["field_size"] = CodegenVectorClBuiltin(
            "field_size", itype, 3, tg, add_impl_const=True
        )
        args["local_size"] = CodegenVectorClBuiltin(
            "local_size", itype, 3, tg, add_impl_const=True, symbolic_mode=True
        )
        args["inv_dx"] = CodegenVectorClBuiltin(
            "inv_dx", fbtype, 3, tg, add_impl_const=True, symbolic_mode=True
        )

        basename = f"gradient_{dim}d"
        if cached:
            basename += "_cached"
        super().__init__(
            basename=basename,
            ext=".cl",
            output=output,
            args=args,
            typegen=typegen,
            inline=True,
            known_args=known_args,
        )

        self.dim = dim
        self.cached = cached

        self.gen_stencil(order)

        self.gencode()

    def gen_stencil(self, order):

        tg = self.typegen
        dim = self.dim
        order = [order] * dim if np.isscalar(order) else order

        S = []
        L = []
        R = []
        if np.isscalar(order):
            order = (order,) * dim
        for i in range(dim):
            o = order[i]
            assert o % 2 == 0
            h = o // 2
            Si = Stencil1D().generate(Lx=h, Rx=+h, k=1, order=order[i])
            S.append(Si)
            L.append(h)
            R.append(h)

        self.order = order

        lghosts = CodegenVectorClBuiltin("lghosts", "int", dim, tg, const=True)
        rghosts = CodegenVectorClBuiltin("rghosts", "int", dim, tg, const=True)
        self.known_vars.update(lghosts=L, rghosts=R)
        self.update_vars(lghosts=lghosts, rghosts=rghosts)

        compute_index = ComputeIndexFunction(tg, dim, wrap=False, itype="int")
        self.require("compute_index", compute_index)

        if self.cached:
            cache_load = CacheLoadFunction(
                dtype=tg.fbtype, dim=dim, typegen=tg, boundary="periodic"
            )
            self.require("cache_load", cache_load)

        self.stencil_name = []
        for i in range(dim):
            Si = S[i]
            stencil = Stencil(Si.stencil, origin=Si.Lx, order=Si.order)

            apply_stencil = ApplyStencilFunction(
                typegen=tg,
                stencil=stencil,
                ftype=tg.fbtype,
                itype="int",
                data_storage="__local",
                known_args=None,
            )
            self.require(apply_stencil.name, apply_stencil)
            self.stencil_name.append(apply_stencil.name)

    def gencode(self):
        s = self
        tg = s.typegen
        fbtype = s.fbtype
        dim = s.dim
        cached = s.cached

        field = s.vars["field"]
        if cached:
            cache_buffer = s.vars["buffer"]
        global_id = s.vars["global_id"]
        local_id = s.vars["local_id"]
        field_size = s.vars["field_size"]
        local_size = s.vars["local_size"]
        lghosts = s.vars["lghosts"]
        rghosts = s.vars["rghosts"]
        inv_dx = s.vars["inv_dx"]

        compute_index = s.reqs["compute_index"]
        if cached:
            cache_load = s.reqs["cache_load"]

        offset = CodegenVariable("offset", "int", tg, const=True)
        stride = CodegenVectorClBuiltin("stride", "int", 1, tg)
        S = CodegenVectorClBuiltin("S", "int", 3, tg, const=True)

        if dim > 1:
            grad = CodegenVectorClBuiltin("grad", fbtype, dim, tg)
        else:
            grad = CodegenVariable("grad", fbtype, tg)

        with s._function_():
            s.append(lghosts.declare())
            s.append(rghosts.declare())
            s.append(S.declare(init=f"{lghosts()}+{local_size()}+{rghosts()}"))
            s.jumpline()
            if cached:
                call = cache_load(
                    src=field,
                    dst=cache_buffer,
                    global_id=global_id,
                    local_id=local_id,
                    local_size=local_size,
                    src_size=field_size,
                    lghosts=lghosts,
                    rghosts=rghosts,
                )
                s.comment("Load field in local memory cache")
                s.append(call + ";")
            s.jumpline()
            s.comment("Compute gradient")
            s.append(grad.declare())
            with s._block_():
                s.append(
                    offset.declare(
                        init=compute_index(idx=f"{local_id()}+{lghosts()}", size=S())
                    )
                )
                s.append(stride.declare(init="1"))
                for i in range(dim):
                    F = self.reqs[self.stencil_name[i]]
                    call = F(data=cache_buffer, offset=offset, stride=stride)
                    code = f"{grad[i]} = {call}*{inv_dx[i]};"
                    s.append(code)
                    if i < dim - 1:
                        s.append(f"stride *= {S[i]};")
            s.append(f"return {grad()};")


if __name__ == "__main__":
    from hysop.backend.device.codegen.base.test import test_typegen

    tg = test_typegen("float", float_dump_mode="dec")
    gf = GradientFunction(tg, dim=3, order=6, known_args=dict(local_size=(8, 8, 8)))
    gf.edit()
