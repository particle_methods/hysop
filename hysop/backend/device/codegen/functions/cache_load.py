# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import contextlib
from contextlib import contextmanager

from hysop.tools.htypes import check_instance
from hysop.tools.contexts import nested
from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.codegen.base.function_codegen import (
    OpenClFunctionCodeGenerator,
)
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
)
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.codegen.base.utils import ArgDict
from hysop.backend.device.codegen.base.statistics import WorkStatistics

from hysop.backend.device.codegen.functions.compute_index import ComputeIndexFunction
from hysop.constants import BoundaryCondition


class CacheLoadFunction(OpenClFunctionCodeGenerator):

    arguments = {
        "src": "source data",
        "dst": "destination buffer",
        "global_id:": "global thread_id",
        "local_id": "local thread_id",
        "local_size": "size of a workgroup",
        "data_size": "size of source data",
    }

    def __init__(
        self,
        typegen,
        ftype,
        work_dim,
        boundary,
        components=1,
        src_vectorize=True,
        dst_vectorize=True,
        itype="int",
        with_gid_ghost_offset=True,
        with_gid_bound_check=True,
        known_args=None,
        force_symbolic=False,
    ):

        assert work_dim > 0

        check_instance(boundary, BoundaryCondition)
        if boundary not in [BoundaryCondition.NONE, BoundaryCondition.PERIODIC]:
            raise NotImplemented(
                f"Boundary '{str(boundary).lower()}' not implemented yet!"
            )

        tg = typegen
        fs = force_symbolic
        vtype = tg.vtype(ftype, components)
        name = f"cache_load_{work_dim}d"
        if boundary != BoundaryCondition.NONE:
            name += f"_{str(boundary).lower()}"
        output = "void"

        args = ArgDict()
        if src_vectorize:
            args["src"] = CodegenVariable(
                "src",
                vtype,
                typegen=tg,
                ptr=True,
                const=True,
                storage="__global",
                nl=True,
                ptr_restrict=True,
            )
        else:
            for i in range(components):
                src = f"src{i}"
                args[src] = CodegenVariable(
                    src,
                    ftype,
                    typegen=tg,
                    ptr=True,
                    const=True,
                    storage="__global",
                    nl=True,
                    ptr_restrict=True,
                )
        if dst_vectorize:
            args["dst"] = CodegenVariable(
                "dst",
                vtype,
                typegen=tg,
                ptr=True,
                const=False,
                storage="__local",
                nl=True,
                ptr_restrict=True,
            )
        else:
            for i in range(components):
                dst = f"dst{i}"
                args[dst] = CodegenVariable(
                    dst,
                    ftype,
                    typegen=tg,
                    ptr=True,
                    const=False,
                    storage="__local",
                    nl=True,
                    ptr_restrict=True,
                )

        args["global_id"] = CodegenVectorClBuiltin(
            "gid", itype, work_dim, typegen, add_impl_const=False
        )
        args["local_id"] = CodegenVectorClBuiltin(
            "lid", itype, work_dim, typegen, add_impl_const=False, nl=True
        )

        args["local_size"] = CodegenVectorClBuiltin(
            "L", itype, work_dim, tg, add_impl_const=True, symbolic_mode=fs
        )
        args["src_size"] = CodegenVectorClBuiltin(
            "N", itype, work_dim, tg, add_impl_const=True, symbolic_mode=fs, nl=True
        )

        args["multiplier"] = CodegenVectorClBuiltin(
            "multiplier", itype, work_dim, tg, add_impl_const=True, symbolic_mode=fs
        )
        args["lghosts"] = CodegenVectorClBuiltin(
            "lghosts", itype, work_dim, tg, add_impl_const=True, symbolic_mode=fs
        )
        args["rghosts"] = CodegenVectorClBuiltin(
            "rghosts", itype, work_dim, tg, add_impl_const=True, symbolic_mode=fs
        )

        super().__init__(
            basename=name,
            output=output,
            args=args,
            known_args=known_args,
            typegen=tg,
            inline=True,
        )

        self.itype = itype
        self.ftype = ftype
        self.vtype = vtype
        self.work_dim = work_dim
        self.components = components
        self.src_vectorize = src_vectorize
        self.dst_vectorize = dst_vectorize

        self.with_gid_ghost_offset = with_gid_ghost_offset
        self.with_gid_bound_check = with_gid_bound_check

        self.boundary = boundary
        self.declare_code()

    def declare_code(self):
        s = self
        tg = s.typegen
        work_dim = s.work_dim
        itype = s.itype
        ftype = s.ftype
        vtype = s.vtype
        components = s.components
        src_vectorize = s.src_vectorize
        dst_vectorize = s.dst_vectorize
        boundary = s.boundary

        with_gid_ghost_offset = self.with_gid_ghost_offset
        with_gid_bound_check = self.with_gid_bound_check

        global_id = s.args["global_id"]
        local_id = s.args["local_id"]

        local_size = s.args["local_size"]
        src_size = s.args["src_size"]

        lghosts = s.args["lghosts"]
        rghosts = s.args["rghosts"]
        multiplier = s.args["multiplier"]

        cache_size = CodegenVectorClBuiltin("S", itype, work_dim, tg, const=True)
        local_pos = CodegenVectorClBuiltin("lidx", itype, work_dim, tg)
        global_pos = CodegenVectorClBuiltin("gidx", itype, work_dim, tg)

        LID = CodegenVariable("LID", itype, tg)
        GID = CodegenVariable("GID", itype, tg)

        tmp = CodegenVectorClBuiltin("tmp", ftype, components, tg)

        compute_index = ComputeIndexFunction(tg, work_dim, wrap=False)
        s.require("compute_index", compute_index)

        with s._function_():
            s.jumpline()
            s.append(
                cache_size.declare(
                    init="{}+{}*{}+{}".format(
                        lghosts(), multiplier(), local_size(), rghosts()
                    )
                )
            )
            s.jumpline()
            s.append(global_pos.declare())
            s.append(local_pos.declare())
            s.jumpline()

            if boundary == BoundaryCondition.PERIODIC:
                if with_gid_ghost_offset:
                    s.append(f"{global_id()} += {src_size()}-{lghosts()};")
                else:
                    s.append(f"{global_id()} += {src_size()};")
                s.append(f"{global_id()} %= {src_size()};")

            @contextmanager
            def _cache_iterate_(i):
                try:
                    if local_size.known() and lghosts.known() and rghosts.known():
                        s.pragma("unroll")
                    else:
                        s.pragma("unroll 8")
                    with s._for_(
                        "{} k{i}=0; k{i}<({}+{L}-1)/{L}; k{i}++".format(
                            itype, cache_size[i], i=i, L=local_size[i]
                        )
                    ) as ctx:
                        s.append(
                            "{} = {}+k{i}*{};".format(
                                local_pos[i], local_id[i], local_size[i], i=i
                            )
                        )
                        s.append(
                            "{} = {}+k{i}*{};".format(
                                global_pos[i], global_id[i], local_size[i], i=i
                            )
                        )
                        cond = f"({local_pos[i]}>={cache_size[i]})"
                        if with_gid_bound_check:
                            if boundary != BoundaryCondition.PERIODIC:
                                cond += (
                                    f" || ({global_pos[i]}>={src_size[i]}+{rghosts[i]})"
                                )
                        s.append(f"if ({cond}) continue;")

                        if boundary == BoundaryCondition.PERIODIC:
                            s.append(f"{global_pos[i]} %= {src_size[i]};")
                        yield ctx
                except:
                    raise

            s.barrier(_local=True)
            with s._block_():
                nested_loops = [_cache_iterate_(i) for i in range(work_dim - 1, -1, -1)]
                with nested(*nested_loops):
                    with s._block_():
                        s.append(
                            LID.declare(
                                const=True,
                                init=compute_index(
                                    idx=local_pos, size=cache_size[:work_dim]
                                ),
                            )
                        )
                        s.append(
                            GID.declare(
                                const=True,
                                init=compute_index(
                                    idx=global_pos, size=src_size[:work_dim]
                                ),
                            )
                        )

                        if src_vectorize:
                            load = s.vars["src"][GID()]
                        else:
                            load = ",".join(
                                [s.vars[f"src{i}"][GID()] for i in range(components)]
                            )
                            load = f"({vtype})({load})"
                        tmp.declare(s, init=load, const=True)
                        if dst_vectorize:
                            store = "{} = {};".format(s.vars["dst"][LID()], tmp())
                            s.append(store)
                        else:
                            for i in range(components):
                                store = "{} = {};".format(
                                    s.vars[f"dst{i}"][LID()], tmp[i]
                                )
                                s.append(store)

            s.barrier(_local=True)

    def per_work_statistics(self):
        typegen = self.typegen
        itype = self.itype
        ftype = self.ftype
        work_dim = self.work_dim
        components = self.components

        size = typegen.FLT_BYTES[ftype]
        reads = components * size
        writes = reads

        ops = {}
        ops[itype] = 2 * work_dim
        ops[itype] += 10 * work_dim

        stats = WorkStatistics()
        stats.ops_per_type = ops
        stats.global_mem_byte_reads += reads
        stats.local_mem_byte_writes += writes

        stats += 2 * self.reqs["compute_index"].per_work_statistics()

        return stats


if __name__ == "__main__":
    from hysop.backend.device.codegen.base.test import _test_typegen

    tg = _test_typegen("float")
    cg = OpenClCodeGenerator(name="base", typegen=tg)

    cache_load0 = CacheLoadFunction(
        typegen=tg, ftype=tg.fbtype, work_dim=1, boundary=BoundaryCondition.PERIODIC
    )

    cache_load1 = CacheLoadFunction(
        typegen=tg,
        ftype=tg.fbtype,
        work_dim=3,
        boundary=BoundaryCondition.PERIODIC,
        components=4,
        src_vectorize=False,
        dst_vectorize=True,
    )

    print(cache_load0.per_work_statistics())
    print()
    print(cache_load1.per_work_statistics())

    cg.require("cl0", cache_load0)
    # cg.require('cl1',cache_load1)

    cg.edit()
    cg.test_compile()
