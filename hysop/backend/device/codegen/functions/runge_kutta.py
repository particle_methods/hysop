# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import hashlib, copy
import numpy as np

from hysop.tools.htypes import check_instance
from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.codegen.base.function_codegen import (
    OpenClFunctionCodeGenerator,
)
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
    CodegenArray,
)
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.codegen.base.utils import ArgDict
from hysop.backend.device.codegen.base.statistics import WorkStatistics

from hysop.numerics.odesolvers.runge_kutta import ExplicitRungeKutta


class RungeKuttaFunction(OpenClFunctionCodeGenerator):

    _default_used_vars = {"t": "t", "dt": "dt", "y": "y", "step": "step"}

    def __init__(
        self, typegen, ftype, method, rhs, used_vars=_default_used_vars, known_args=None
    ):

        check_instance(method, ExplicitRungeKutta)
        check_instance(rhs, OpenClFunctionCodeGenerator)
        method.dump = typegen.dump

        # find out rhs function arguments
        rhs_args_name = set(rhs.args.keys())
        rhs_args_name = rhs_args_name.difference(rhs.known_args.keys())

        t = used_vars["t"]
        dt = used_vars["dt"]
        y = used_vars["y"]
        step = used_vars["step"]

        has_time = t in rhs_args_name
        has_step = step in rhs_args_name
        has_dt = dt in rhs_args_name
        has_var = y in rhs_args_name

        # runge kutta function args
        # it is ok for the rhs not to depend on 't' or 'dt' but not on the scheme variable 'y'
        args = ArgDict()
        rhs_args = {}
        if has_time:
            args[t] = rhs.args[t]
            rhs_args[t] = args[t]
        if has_dt:
            args[dt] = rhs.args[dt]
            rhs_args[dt] = args[dt]
        else:
            args[dt] = CodegenVariable(dt, ftype, typegen, add_impl_const=True)
        if has_var:
            args[y] = rhs.args[y]
            rhs_args[y] = args[y]
        else:
            msg = f"{rhs.fname} function does not depend on variable {y}."
            raise RuntimeError(msg)
        for arg in sorted(rhs_args_name):
            if (arg not in args) and (arg not in [t, dt, y, step]):
                args[arg] = rhs.args[arg]
                rhs_args[arg] = args[arg]

        rhs_name = hashlib.md5(rhs.fname.encode("utf-8")).hexdigest()[0:8]
        ctype = args[y].ctype
        basename = f"apply_{method.name()}_{ctype}_{rhs_name}"

        ctype = args[y].ctype

        super().__init__(
            basename=basename,
            output=ctype,
            typegen=typegen,
            inline=True,
            args=args,
            known_args=known_args,
        )

        self.update_requirements({"rhs": rhs})

        self.ftype = ftype
        self.method = method

        self.has_time = has_time
        self.has_step = has_step
        self.rhs_args = rhs_args
        self.used_vars = used_vars
        self.rhs = rhs

        self.gencode()

    def gencode(self):
        s = self
        tg = s.typegen
        ftype = s.ftype
        method = s.method
        used_vars = s.used_vars

        has_time = s.has_time
        has_step = s.has_step
        rhs_args = s.rhs_args
        rhs = s.reqs["rhs"]

        dt = s.vars[used_vars["dt"]]
        y = s.vars[used_vars["y"]]
        if has_time:
            t = s.vars[used_vars["t"]]
        if has_step:
            step = rhs.args[used_vars["step"]]
            rhs_args[used_vars["step"]] = step

        Ti = CodegenVariable("ti", ftype, tg)
        Ki = copy.copy(y)
        Yi = copy.copy(y)

        K = CodegenArray("K", Yi.ctype, tg, shape=(method.order,))

        dy = copy.copy(y)
        dy.name = "k"

        with s._function_():

            s.jumpline()
            s.comment("Estimated slopes")
            K.declare(s)
            if has_step:
                step.declare(s, init=0, const=False)
            __sum = ""
            for i in range(method.stages):
                s.jumpline()
                alpha = method.alpha[i]
                beta = method.beta[i]
                gamma = method.gamma[i - 1, :]
                s.comment(f"Computing Runge-Kutta step {i}")
                if has_time:
                    _t = t
                    if alpha != 0:
                        _t += f" + {tg.dump(float(alpha))}*{dt}"
                _sum = ""
                if i > 0:
                    for j, g in enumerate(gamma):
                        if g == 0:
                            continue
                        else:
                            _sum += f"+{tg.dump(float(g))}*{K[j]}"

                with s._block_():
                    Yi.name = f"{y}{i}"
                    rhs_args[used_vars["y"]] = Yi
                    if has_time:
                        Ti.name = f"{t}{i}"
                        s.append(Ti.declare(init=_t))
                        rhs_args[used_vars["t"]] = Ti
                    if i > 0:
                        Ki.name = "{}{}".format("k", i)
                        s.append(Ki.declare(init=_sum))
                        s.append(Yi.declare(init=f"{y} + {Ki}*{dt}"))
                    else:
                        s.append(Yi.declare(init=y))

                    code = f"{K[i]} = {rhs(**rhs_args)};"
                    s.append(code)
                    if beta != 0:
                        __sum += f"+{tg.dump(float(beta))}*{K[i]}"

                    if has_step:
                        s.append(f"{step} += 1;")

            s.jumpline()
            s.append(dy.declare(const=True, init=__sum))
            s.append(f"return {y} + {dy}*{dt};")

    def per_work_statistics(self):
        ftype = self.ftype
        method = self.method
        stages = method.stages
        rhs = self.rhs
        has_time = int(self.has_time)

        ops = {}
        ops[ftype] = 2 * stages
        ops[ftype] += 2 * has_time * np.sum(method.alpha != 0)
        ops[ftype] += np.sum(2 * np.sum(method.gamma != 0, axis=1) - 1)
        ops[ftype] += 2 * np.sum(method.beta != 0) - 1

        stats = WorkStatistics()
        stats.ops_per_type = ops

        return stats + stages * rhs.per_work_statistics()


if __name__ == "__main__":

    from hysop.backend.device.codegen.base.test import test_typegen

    method = ExplicitRungeKutta("RK4_38")

    tg = test_typegen("float")
    rkf = RungeKuttaFunction(tg, ftype=tg.fbtype, method=method, rhs="function")
    rkf.edit()
    print
