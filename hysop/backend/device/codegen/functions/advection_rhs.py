# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.htypes import check_instance
from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.codegen.base.function_codegen import (
    OpenClFunctionCodeGenerator,
)
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
)
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.codegen.base.utils import WriteOnceDict, ArgDict
from hysop.backend.device.codegen.base.statistics import WorkStatistics
from hysop.constants import BoundaryCondition

from hysop.backend.device.codegen.functions.apply_stencil import ApplyStencilFunction

from hysop.numerics.stencil.stencil import Stencil


# compute alpha*grad(u).w + (1-alpha)*grad(u)^T.w
# where u may be is_cached and w is private
# with finite difference centred stencil of given order
class DirectionalAdvectionRhsFunction(OpenClFunctionCodeGenerator):

    def __init__(
        self,
        typegen,
        ftype,
        work_dim,
        nparticles,
        is_cached,
        boundary,
        relative_velocity,
        ptr_restrict=True,
        itype="int",
        known_args=None,
        field_infos=None,
    ):

        assert work_dim >= 1 and work_dim <= 3
        check_instance(boundary, BoundaryCondition)
        assert nparticles in [1, 2, 4, 8, 16]
        assert isinstance(relative_velocity, (float, str))

        is_periodic = boundary == BoundaryCondition.PERIODIC

        if is_cached:
            storage = "__local"
        else:
            storage = "__global"

        vtype = typegen.vtype(ftype, nparticles)

        (args, basename) = self.build_prototype(
            typegen,
            work_dim,
            itype,
            ftype,
            vtype,
            nparticles,
            ptr_restrict,
            storage,
            is_cached,
            is_periodic,
            field_infos,
        )

        reqs = self.build_requirements(
            typegen,
            work_dim,
            itype,
            ftype,
            vtype,
            nparticles,
            ptr_restrict,
            storage,
            is_cached,
            is_periodic,
        )

        super().__init__(
            basename=basename,
            output=vtype,
            typegen=typegen,
            inline=True,
            args=args,
            known_args=known_args,
        )

        self.update_requirements(reqs)

        self.work_dim = work_dim

        self.itype = itype
        self.ftype = ftype
        self.vtype = vtype

        self.storage = storage
        self.boundary = boundary
        self.nparticles = nparticles
        self.is_cached = is_cached
        self.is_periodic = is_periodic
        self.relative_velocity = relative_velocity

        self.gencode()

    def build_prototype(
        self,
        typegen,
        work_dim,
        itype,
        ftype,
        vtype,
        nparticles,
        ptr_restrict,
        storage,
        is_cached,
        is_periodic,
        field_infos,
    ):

        args = ArgDict()

        args["line_velocity"] = CodegenVariable(
            "line_velocity",
            ftype,
            typegen,
            const=True,
            add_impl_const=True,
            storage=storage,
            ptr=True,
            ptr_restrict=ptr_restrict,
            nl=True,
        )
        args["X"] = CodegenVectorClBuiltin(
            "X", ftype, nparticles, typegen, add_impl_const=True, nl=True
        )

        if is_periodic and (not is_cached):
            args["line_width"] = CodegenVariable(
                "line_width", itype, typegen, add_impl_const=True
            )
        args["line_offset"] = CodegenVariable(
            "line_offset", itype, typegen, add_impl_const=True, nl=True
        )

        # args['rk_step'] = CodegenVariable('rk_step', itype, typegen)
        args["inv_dx"] = CodegenVariable(
            "inv_dx", ftype, typegen, add_impl_const=True, nl=True
        )
        # args['active'] = CodegenVariable('active','bool',typegen, add_impl_const=True)

        if field_infos is not None:
            args["field_infos"] = field_infos.pointer(
                name="field_infos", ptr_level=1, const=True, ptr_const=True, nl=True
            )

        cached = "cached_" if is_cached else ""
        basename = f"advection_rhs_{cached}{ftype[0]}{nparticles}p"

        return (args, basename)

    def build_requirements(
        self,
        typegen,
        work_dim,
        itype,
        ftype,
        vtype,
        nparticles,
        ptr_restrict,
        storage,
        is_cached,
        is_periodic,
    ):
        reqs = WriteOnceDict()
        return reqs

    def gencode(self):
        s = self
        tg = self.typegen
        work_dim = s.work_dim

        nparticles = s.nparticles

        is_cached = s.is_cached
        is_periodic = s.is_periodic

        itype = s.itype
        ftype = s.ftype
        vtype = s.vtype

        position = s.args["X"]
        velocity = s.args["line_velocity"]

        line_offset = s.args["line_offset"]
        if not is_cached and is_periodic:
            line_width = s.args["line_width"]

        inv_dx = s.args["inv_dx"]
        # rk_step      = s.args['rk_step']

        dX_dt = CodegenVectorClBuiltin("dX_dt", ftype, nparticles, tg)

        pos = CodegenVectorClBuiltin("pos", ftype, nparticles, tg)
        lidx = CodegenVectorClBuiltin("lidx", itype, nparticles, tg)
        idx = CodegenVectorClBuiltin("idx", itype, nparticles, tg)
        alpha = CodegenVectorClBuiltin("alpha", ftype, nparticles, tg)

        Vl = CodegenVectorClBuiltin("Vl", ftype, nparticles, tg)
        Vr = CodegenVectorClBuiltin("Vr", ftype, nparticles, tg)

        relative_velocity = self.relative_velocity
        if isinstance(relative_velocity, float):
            Vrel = CodegenVectorClBuiltin(
                "Vrel",
                ftype,
                nparticles,
                tg,
                const=True,
                value=(relative_velocity,) * nparticles,
            )
        elif isinstance(relative_velocity, str):
            init = f"({vtype})({relative_velocity})"
            Vrel = CodegenVectorClBuiltin(
                "Vrel", ftype, nparticles, tg, const=True, init=init
            )
        else:
            msg = "Unknown type for relative velocity."
            raise NotImplementedError(msg)

        part_ftype = Vl.ctype
        part_itype = lidx.ctype

        with s._function_():
            s.jumpline()

            dX_dt.declare(s, init=0)
            with s._align_() as al:
                pos.declare(al, init=f"{position()}*{inv_dx()}", align=True)
                lidx.declare(al, init=f"convert_{part_itype}_rtn({pos()})", align=True)
                alpha.declare(
                    al, init=f"{pos()} - convert_{part_ftype}({lidx()})", align=True
                )
            Vrel.declare(s)
            idx.declare(s)
            s.jumpline()

            # /!\
            # DEPENDING ON USED PRECISION, ASSUMING alpha = 0.0 is not TRUE
            # for the first step, so we interpolate even on the first step

            # with s._if_('{} == 0'.format(rk_step())):
            # s.append('{} = {}-{};'.format(idx(), lidx(), line_offset()))
            # reads = [velocity[idx[i] ] for i in range(nparticles)]
            # init = '({})({})'.format(part_ftype,', '.join(reads))
            # s.append('printf("X=%f => alpha=%f, idx=%i, V=%f\\n", X, alpha, idx, {});'.format(velocity[idx[0]]))

            # s.append('{} = {};'.format(dX_dt(), init))
            # with s._else_():

            if is_periodic and not is_cached:
                s.append(
                    "{lidx} = ({lidx}+{size})%{size};".format(
                        lidx=lidx(), size=line_width()
                    )
                )
            s.append(f"{idx()} = {lidx()}-{line_offset()};")
            reads = [velocity[idx[i]] for i in range(nparticles)]
            init = "({})({})".format(part_ftype, ", ".join(reads))
            Vl.declare(s, init=init)
            if is_periodic and not is_cached:
                s.append(
                    "{lidx} = ({lidx}+1)%{size};".format(lidx=lidx(), size=line_width())
                )
                s.append(f"{idx()} = {lidx()}-{line_offset()};")
            else:
                s.append(f"{idx()} += 1;")
            Vr.declare(s, init=init)
            s.append(f"{dX_dt()} = mix({Vl()},{Vr()},{alpha()}) - {Vrel};")

            s.append(f"return {dX_dt()};")


if __name__ == "__main__":

    from hysop.backend.device.codegen.base.test import _test_typegen

    tg = _test_typegen("float")
    asf = DirectionalAdvectionRhsFunction(
        tg, "float", 3, 4, False, BoundaryCondition.PERIODIC, 0.66
    )
    asf.edit()
