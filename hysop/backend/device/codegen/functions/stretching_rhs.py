# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sympy as sm

from hysop.tools.htypes import check_instance
from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.codegen.base.function_codegen import (
    OpenClFunctionCodeGenerator,
)
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
)
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.codegen.base.utils import WriteOnceDict, ArgDict
from hysop.backend.device.codegen.base.statistics import WorkStatistics
from hysop.methods import StretchingFormulation
from hysop.constants import BoundaryCondition

from hysop.backend.device.codegen.functions.apply_stencil import ApplyStencilFunction

from hysop.numerics.stencil.stencil_generator import StencilGenerator


# compute alpha*grad(u).w + (1-alpha)*grad(u)^T.w
# where u may be cached and w is private
# with finite difference centred stencil of given order
class DirectionalStretchingRhsFunction(OpenClFunctionCodeGenerator):

    _default_used_variables = {
        "U": "U",
        "W": "W",
        "components": "xyz",
        "directions": "XYZ",
    }

    def __init__(
        self,
        typegen,
        dim,
        ftype,
        order,
        direction,
        formulation,
        cached,
        boundary,
        ptr_restrict=True,
        vectorize_u=False,
        itype="int",
        used_variables=_default_used_variables,
        known_args=None,
    ):

        assert dim == 3
        assert direction < dim
        assert order > 1 and order % 2 == 0
        check_instance(formulation, StretchingFormulation)
        check_instance(boundary, tuple, values=BoundaryCondition)

        is_conservative = formulation == StretchingFormulation.CONSERVATIVE
        is_periodic = (boundary[0] == BoundaryCondition.PERIODIC) and (
            boundary[1] == BoundaryCondition.PERIODIC
        )

        if cached:
            storage = "__local"
        else:
            storage = "__global"

        vtype = typegen.vtype(ftype, dim)

        (args, basename) = self.build_prototype(
            typegen,
            dim,
            itype,
            ftype,
            vtype,
            order,
            direction,
            cached,
            ptr_restrict,
            storage,
            vectorize_u,
            used_variables,
            formulation,
            is_conservative,
            is_periodic,
        )

        reqs = self.build_requirements(
            typegen,
            dim,
            itype,
            ftype,
            vtype,
            order,
            direction,
            boundary,
            cached,
            ptr_restrict,
            storage,
            vectorize_u,
            used_variables,
            is_conservative,
            is_periodic,
            args,
        )

        super().__init__(
            basename=basename,
            output=vtype,
            typegen=typegen,
            inline=True,
            args=args,
            known_args=known_args,
        )

        self.update_requirements(reqs)

        self.dim = dim
        self.itype = itype
        self.ftype = ftype
        self.vtype = vtype

        self.order = order
        self.direction = direction
        self.used_variables = used_variables
        self.vectorize_u = vectorize_u
        self.storage = storage
        self.is_conservative = is_conservative
        self.is_periodic = is_periodic
        self.formulation = formulation
        self.cached = cached
        self.boundary = boundary

        self.gencode()

    def build_prototype(
        self,
        typegen,
        dim,
        itype,
        ftype,
        vtype,
        order,
        direction,
        cached,
        ptr_restrict,
        storage,
        vectorize_u,
        used_variables,
        formulation,
        is_conservative,
        is_periodic,
    ):

        U = used_variables["U"]
        W = used_variables["W"]
        xyz = used_variables["components"]
        XYZ = used_variables["directions"]

        args = ArgDict()
        if vectorize_u:
            args[U] = CodegenVariable(
                U,
                vtype,
                typegen,
                const=True,
                add_impl_const=True,
                storage=storage,
                ptr=True,
                ptr_restrict=ptr_restrict,
                nl=True,
            )
        else:
            for i in range(dim):
                Uxyz = f"{U}{xyz[i]}"
                args[Uxyz] = CodegenVariable(
                    Uxyz,
                    ftype,
                    typegen,
                    const=True,
                    add_impl_const=True,
                    storage=storage,
                    ptr=True,
                    ptr_restrict=ptr_restrict,
                    nl=True,
                )
        if is_conservative:
            Wd = f"{W}{xyz[direction]}"
            args[Wd] = CodegenVariable(
                Wd,
                ftype,
                typegen,
                const=False,
                add_impl_const=True,
                storage=storage,
                ptr=True,
                ptr_restrict=ptr_restrict,
                nl=True,
            )

        args[W] = CodegenVectorClBuiltin(W, ftype, dim, typegen, add_impl_const=True)
        args["inv_dx"] = CodegenVariable(
            "inv_dx", ftype, typegen, add_impl_const=True, nl=True
        )

        if is_conservative:
            args["rk_step"] = CodegenVariable(
                "rk_step", itype, typegen, add_impl_const=True
            )
        if is_periodic and (not cached):
            args["base"] = CodegenVariable(
                "base", itype, typegen, add_impl_const=True, nl=True
            )
            args["offset"] = CodegenVariable(
                "offset", itype, typegen, add_impl_const=True, nl=True
            )
            args["width"] = CodegenVariable(
                "width", itype, typegen, add_impl_const=True, nl=True
            )
        else:
            args["offset"] = CodegenVariable(
                "offset", itype, typegen, add_impl_const=True, nl=True
            )
        args["lidx"] = CodegenVariable("lidx", itype, typegen, add_impl_const=True)
        args["Lx"] = CodegenVariable("Lx", itype, typegen, add_impl_const=True, nl=True)
        args["active"] = CodegenVariable("active", "bool", typegen)

        basename = "stretching_rhs_{}_{}{}{}_fdc{}".format(
            str(formulation).lower(), ftype[0], dim, ("v" if vectorize_u else ""), order
        )
        basename += "_" + XYZ[direction]

        return (args, basename)

    def build_stencil(self, order):
        assert order % 2 == 0
        h = order // 2
        sg = StencilGenerator()
        sg.configure(dim=1, derivative=1, order=order)
        stencil = sg.generate_exact_stencil(origin=h)
        return stencil

    def build_requirements(
        self,
        typegen,
        dim,
        itype,
        ftype,
        vtype,
        order,
        direction,
        boundary,
        cached,
        ptr_restrict,
        storage,
        vectorize_u,
        used_variables,
        is_conservative,
        is_periodic,
        args,
    ):

        reqs = WriteOnceDict()

        U = used_variables["U"]
        W = used_variables["W"]
        xyz = used_variables["components"]

        vector_inputs = [U]
        if is_conservative:
            scalar_inputs = [f"{W}{xyz[direction]}"]
            op = "{vinput0}[{id}] * {sinput0}[{id}]"
        else:
            scalar_inputs = []
            op = "{vinput0}[{id}]"

        if is_periodic and not cached:
            base = args["base"]
            offset = args["offset"]
            width = args["width"]
            extra_inputs = [base, offset, width]
            custom_id = "{}+({}+{}+{})%{}".format(
                base(), width(), offset(), "{offset}", width()
            )
            known_args = {}
        else:
            extra_inputs = []
            custom_id = None
            known_args = {"stride": 1}

        inv_dx_s = sm.Symbol("inv_dx")
        inv_dx_var = CodegenVariable(
            "inv_dx", ftype, typegen, add_impl_const=True, nl=True
        )

        stencil = self.build_stencil(order)
        stencil.replace_symbols({stencil.dx: 1.0 / inv_dx_s})
        symbol2vars = {inv_dx_s: inv_dx_var}

        apply_stencil = ApplyStencilFunction(
            typegen=typegen,
            stencil=stencil,
            symbol2vars=symbol2vars,
            ftype=ftype,
            itype=itype,
            data_storage=storage,
            vectorize=vectorize_u,
            components=dim,
            extra_inputs=extra_inputs,
            scalar_inputs=scalar_inputs,
            vector_inputs=vector_inputs,
            vector_suffixes=xyz,
            op=op,
            custom_id=custom_id,
            known_args=known_args,
        )
        reqs["apply_stencil"] = apply_stencil

        return reqs

    def gencode(self):
        s = self
        dim = s.dim
        tg = self.typegen

        cached = s.cached
        direction = s.direction
        formulation = s.formulation
        vectorize_u = s.vectorize_u
        itype = s.itype
        ftype = s.ftype
        order = s.order

        is_conservative = s.is_conservative
        is_periodic = s.is_periodic

        apply_stencil = s.reqs["apply_stencil"]

        used_variables = s.used_variables
        U = used_variables["U"]
        W = used_variables["W"]
        xyz = used_variables["components"]

        fargs = {}
        if is_periodic and not cached:
            fargs["base"] = s.args["base"]
            fargs["offset"] = s.args["offset"]
            fargs["width"] = s.args["width"]
        else:
            fargs["offset"] = s.args["offset"]
        fargs["inv_dx"] = s.args["inv_dx"]
        if vectorize_u:
            fargs[U] = s.args[U]
        else:
            for i in range(dim):
                Ui = U + xyz[i]
                fargs[Ui] = s.args[Ui]
        if is_conservative:
            Wd = W + xyz[direction]
            fargs[Wd] = s.args[Wd]
            Wd = s.args[Wd]
        call = apply_stencil(**fargs)

        W = s.args[W]
        lidx = s.args["lidx"]
        Lx = s.args["Lx"]
        if is_conservative:
            rk_step = s.args["rk_step"]

        offset = s.args["offset"]
        active = s.args["active"]

        dw_dt = CodegenVectorClBuiltin("dW_dt", ftype, dim, tg)
        ghosts = CodegenVariable(
            "ghosts", itype, tg, const=True, value=order // 2, symbolic_mode=True
        )

        with s._function_():
            s.jumpline()

            ghosts.declare(s)

            if is_conservative:
                s.jumpline()
                s.comment("Synchronize required vorticiy component across workitems")
                s.barrier(_local=cached, _global=not cached)
                with s._if_(active()):
                    code = f"{Wd[offset()]} = {W[direction]};"
                    s.append(code)
                s.barrier(_local=cached, _global=not cached)
                s.jumpline(2)

            dw_dt.declare(s, init=0)

            if is_conservative:
                cond = "({active}) && ({lid}>={step}*{ghosts}) && ({lid}<{L}-{step}*{ghosts})".format(
                    active=active(),
                    lid=lidx(),
                    L=Lx(),
                    ghosts=ghosts(),
                    step=f"({rk_step()}+1)",
                )
            else:
                cond = "({active}) && ({lid}>={ghosts}) && ({lid}<{L}-{ghosts})".format(
                    active=active(), lid=lidx(), L=Lx(), ghosts=ghosts()
                )

            with s._if_(cond):

                if is_conservative:
                    s.comment(
                        "compute d(U*W{0})/d{0} using finite difference stencil".format(
                            xyz[direction]
                        )
                    )
                    du_dx = CodegenVectorClBuiltin(
                        "dUW{0}_d{0}".format(xyz[direction]), ftype, dim, tg, const=True
                    )
                    du_dx.declare(s, init=call)

                    s.jumpline()

                    s.comment("directional contribution due to div(U:W)")
                    code = f"{dw_dt()} += {du_dx()};"
                    s.append(code)
                else:
                    s.comment("compute dU/dx using finite difference stencil")
                    du_dx = CodegenVectorClBuiltin(
                        "dU_d" + xyz[direction], ftype, dim, tg, const=True
                    )
                    du_dx.declare(s, init=call)

                    s.jumpline()
                    if formulation == StretchingFormulation.GRAD_UW:
                        s.comment("directional contribution due to grad(U).W")
                        code = f"{dw_dt()} += {du_dx()}*{W[direction]};"
                        s.append(code)

                    elif formulation == StretchingFormulation.GRAD_UW_T:
                        s.comment("directional contribution due to grad(U)^T.W")
                        code = f"{dw_dt[direction]} += dot({du_dx()},{W()});"
                        s.append(code)

                    elif formulation == StretchingFormulation.MIXED_GRAD_UW:
                        s.comment("directional contribution due to grad(U).W")
                        code = f"{dw_dt()}   += 0.5*{du_dx()}*{W[direction]};"
                        s.append(code)
                        s.comment("directional contribution due to grad(U)^T.W")
                        code = f"{dw_dt[direction]} += 0.5*dot({du_dx()},{W()});"
                        s.append(code)
                    else:
                        raise ValueError()

            s.append(f"return {dw_dt()};")

    def per_work_statistics(self):
        dim = self.dim
        ftype = self.ftype
        storage = self.storage

        stats = self.reqs["apply_stencil"].per_work_statistics()

        if "alpha" in self.known_args:
            alpha = self.known_args["alpha"]
        else:
            alpha = 0.5
        stats.ops_per_type[ftype] += int(alpha != 0) * dim * 3
        stats.ops_per_type[ftype] += int(alpha != 1) * dim * 2 + 2

        return stats


if __name__ == "__main__":

    from hysop.backend.device.codegen.base.test import test_typegen

    formulation = StretchingFormulation.GRAD_UW

    tg = test_typegen("float")
    asf = DirectionalStretchingRhsFunction(
        tg, 3, tg.fbtype, 4, 1, vectorize_u=False, formulation=formulation, cached=False
    )
    asf.edit()
