# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import contextlib
import numpy as np
import sympy as sm

from hysop.tools.htypes import check_instance, first_not_None
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen

from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.codegen.base.function_codegen import (
    OpenClFunctionCodeGenerator,
)
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
)
from hysop.backend.device.codegen.base.utils import WriteOnceDict, ArgDict
from hysop.backend.device.codegen.base.statistics import WorkStatistics

from hysop.backend.device.codegen.functions.polynomial import PolynomialFunction
from hysop.backend.device.codegen.functions.custom_atomics import CustomAtomicFunction

from hysop.constants import BoundaryCondition
from hysop.numerics.remesh.remesh import RemeshKernel
from hysop.numerics.remesh.kernel_generator import Kernel

from hysop.numerics.stencil.stencil_generator import StencilGenerator


class DirectionalRemeshFunction(OpenClFunctionCodeGenerator):

    def __init__(
        self,
        typegen,
        work_dim,
        itype,
        ftype,
        nparticles,
        nscalars,
        sboundary,
        remesh_kernel,
        use_atomics,
        remesh_criteria_eps,
        use_short_circuit=None,
        known_args=None,
        debug_mode=False,
    ):

        check_instance(sboundary, tuple, values=BoundaryCondition)
        check_instance(remesh_kernel, (RemeshKernel, Kernel))
        assert (remesh_kernel.n % 2 == 0) or (remesh_kernel.n == 1)
        assert remesh_kernel.n > 0

        use_short_circuit = first_not_None(
            use_short_circuit, typegen.use_short_circuit_ops
        )

        is_periodic = (sboundary[0] == BoundaryCondition.PERIODIC) and (
            sboundary[1] == BoundaryCondition.PERIODIC
        )

        ivtype = typegen.vtype(itype, nparticles)
        fvtype = typegen.vtype(ftype, nparticles)

        reqs = self.build_requirements(
            typegen,
            work_dim,
            itype,
            ftype,
            ivtype,
            fvtype,
            nparticles,
            nscalars,
            sboundary,
            remesh_kernel,
            use_atomics,
            remesh_criteria_eps,
        )

        (args, basename) = self.build_prototype(
            reqs,
            typegen,
            work_dim,
            itype,
            ftype,
            ivtype,
            fvtype,
            nparticles,
            nscalars,
            sboundary,
            remesh_kernel,
            use_atomics,
            remesh_criteria_eps,
            is_periodic,
        )

        super().__init__(
            basename=basename,
            output=None,
            typegen=typegen,
            inline=True,
            args=args,
            known_args=known_args,
        )

        self.update_requirements(reqs)

        self.work_dim = work_dim
        self.itype = itype
        self.ftype = ftype
        self.ivtype = ivtype
        self.fvtype = fvtype

        self.nparticles = nparticles
        self.nscalars = nscalars
        self.sboundary = sboundary
        self.kernel = remesh_kernel
        self.use_atomics = use_atomics
        self.remesh_criteria_eps = remesh_criteria_eps
        self.is_periodic = is_periodic
        self.debug_mode = debug_mode

        self.use_short_circuit = use_short_circuit

        self.gencode()

    @staticmethod
    def _printv(ncomponents):
        assert ncomponents in [1, 2, 4, 8, 16]
        if ncomponents == 1:
            return ""
        else:
            return f"v{ncomponents}"

    def build_prototype(
        self,
        reqs,
        typegen,
        work_dim,
        itype,
        ftype,
        ivtype,
        fvtype,
        nparticles,
        nscalars,
        sboundary,
        remesh_kernel,
        use_atomics,
        remesh_criteria_eps,
        is_periodic,
    ):

        args = ArgDict()

        atomic = "atomic_" if use_atomics else ""
        criteria = (
            f"{remesh_criteria_eps}eps__"
            if (remesh_criteria_eps is not None)
            else "full"
        )

        basename = "__{}remesh_{}d__lambda_{}_{}__{}__{}p__{}s__{}".format(
            atomic,
            work_dim,
            remesh_kernel.n,
            remesh_kernel.r,
            ftype,
            nparticles,
            nscalars,
            criteria,
        )

        args["p"] = CodegenVectorClBuiltin(
            name="p",
            btype=ftype,
            dim=nparticles,
            typegen=typegen,
            add_impl_const=True,
            nl=True,
        )

        scalars = []
        for i in range(nscalars):
            Si = CodegenVectorClBuiltin(
                name=f"s{i}",
                btype=ftype,
                dim=nparticles,
                typegen=typegen,
                add_impl_const=True,
                nl=(i == nscalars - 1),
            )
            scalars.append(Si)
            args[Si.name] = Si

        args["dx"] = CodegenVariable(
            name="dx", ctype=ftype, typegen=typegen, add_impl_const=True
        )
        args["inv_dx"] = CodegenVariable(
            name="inv_dx", ctype=ftype, typegen=typegen, add_impl_const=True, nl=True
        )

        if is_periodic:
            args["grid_size"] = CodegenVariable(
                name="grid_size", ctype=itype, typegen=typegen, add_impl_const=True
            )
        args["line_offset"] = CodegenVariable(
            name="line_offset", ctype=itype, typegen=typegen, add_impl_const=True
        )
        args["cache_width"] = CodegenVariable(
            "cache_width", ctype=itype, typegen=typegen
        )
        args["cache_ghosts"] = CodegenVariable(
            name="cache_ghosts", ctype=itype, typegen=typegen, add_impl_const=True
        )
        args["active"] = CodegenVariable(
            name="active", ctype="bool", typegen=typegen, add_impl_const=True, nl=True
        )

        cached_scalars = []
        for i in range(nscalars):
            Si = CodegenVariable(
                name=f"S{i}",
                ctype=ftype,
                typegen=typegen,
                ptr_restrict=True,
                ptr=True,
                storage=self._local,
                ptr_const=False,
                add_impl_const=True,
                nl=True,
            )
            cached_scalars.append(Si)
            args[Si.name] = Si

        self.scalars = scalars
        self.cached_scalars = cached_scalars

        return (args, basename)

    def build_requirements(
        self,
        typegen,
        work_dim,
        itype,
        ftype,
        ivtype,
        fvtype,
        nparticles,
        nscalars,
        sboundary,
        remesh_kernel,
        use_atomics,
        remesh_criteria_eps,
    ):

        reqs = WriteOnceDict()

        P = []
        kernel = remesh_kernel
        if kernel.poly_splitted:
            self.poly_splitted = True
            name = "lambda_{}_{}__{}".format(kernel.n, kernel.r, "{}_{}")
            for i, _ in enumerate(kernel.Pt_l):
                ilname = name.format(i, "left")
                irname = name.format(i, "right")
                Pil = PolynomialFunction(
                    typegen,
                    ftype,
                    nparticles,
                    kernel.Cl[:, i],
                    ilname,
                    use_fma=True,
                    var="y",
                )
                Pir = PolynomialFunction(
                    typegen,
                    ftype,
                    nparticles,
                    kernel.Cr[:, i],
                    irname,
                    use_fma=True,
                    var="y",
                )
                P.append((Pil, Pir))
                reqs[ilname] = Pil
                reqs[irname] = Pir
        else:
            self.poly_splitted = False
            name = "lambda_{}_{}__{}".format(kernel.n, kernel.r, "{}")
            for i, _ in enumerate(kernel.Pt):
                iname = name.format(i)
                Pi = PolynomialFunction(
                    typegen,
                    ftype,
                    nparticles,
                    kernel.C[:, i],
                    iname,
                    use_fma=True,
                    var="y",
                )
                P.append((Pi,))
                reqs[iname] = Pi
        self.polynomials = P

        if use_atomics:
            reqs["atomic_add"] = CustomAtomicFunction(
                typegen, ftype, "{} + {}", self._local, "add"
            )

        return reqs

    def gencode(self):
        s = self
        dim = s.work_dim
        tg = self.typegen

        itype = s.itype
        ftype = s.ftype
        ivtype = s.ivtype
        fvtype = s.fvtype

        work_dim = s.work_dim
        itype = s.itype
        ftype = s.ftype
        ivtype = s.ivtype
        fvtype = s.fvtype

        nparticles = s.nparticles
        nscalars = s.nscalars
        sboundary = s.sboundary
        use_atomics = s.use_atomics
        use_short_circuit = s.use_short_circuit
        remesh_criteria_eps = s.remesh_criteria_eps
        debug_mode = s.debug_mode

        is_periodic = s.is_periodic

        scalars = s.scalars
        cached_scalars = s.cached_scalars

        pos = s.vars["p"]

        dx = s.vars["dx"]
        inv_dx = s.vars["inv_dx"]

        active = s.vars["active"]
        line_offset = s.vars["line_offset"]
        cache_ghosts = s.vars["cache_ghosts"]
        cache_width = s.vars["cache_width"]

        poly_splitted = s.poly_splitted

        lb = "[" if (nparticles > 1) else ""
        rb = "]" if (nparticles > 1) else ""
        vnf = "{}{}{}".format(lb, ", ".join("%2.2f" for _ in range(nparticles)), rb)
        vni = "{}{}{}".format(lb, ", ".join("%i" for _ in range(nparticles)), rb)

        def expand_printf_vector(x):
            return (
                str(x)
                if (nparticles == 1)
                else ",".join(
                    (
                        "({}).s{}".format(x, "0123456789abcdef"[i])
                        if isinstance(x, str)
                        else x[i]
                    )
                    for i in range(nparticles)
                )
            )

        epv = expand_printf_vector

        @contextlib.contextmanager
        def if_thread_active():
            with s._if_(f"{active}"):
                yield

        if is_periodic:
            grid_size = s.vars["grid_size"]

        dtype = tg.np_dtype(ftype)

        P = CodegenVariable(
            name="P",
            ctype=itype,
            typegen=tg,
            const=True,
            value=1 + (self.kernel.n // 2),
        )
        eps = CodegenVariable(
            name="eps", ctype=ftype, typegen=tg, const=True, value=np.finfo(dtype).eps
        )

        rp = CodegenVectorClBuiltin(name="rp", btype=ftype, dim=nparticles, typegen=tg)
        y = CodegenVectorClBuiltin(name="y", btype=ftype, dim=nparticles, typegen=tg)
        ind = CodegenVectorClBuiltin(
            name="ind", btype=itype, dim=nparticles, typegen=tg
        )
        find = CodegenVectorClBuiltin(
            name="find", btype=ftype, dim=nparticles, typegen=tg
        )
        vone = CodegenVectorClBuiltin(
            name="one", btype=ftype, dim=nparticles, typegen=tg, value=(1,) * nparticles
        )

        if debug_mode:
            tst0 = CodegenVectorClBuiltin(
                name="tst0", btype=ftype, dim=nparticles, typegen=tg
            )
            tst1 = CodegenVectorClBuiltin(
                name="tst1", btype=ftype, dim=nparticles, typegen=tg
            )

        if poly_splitted:
            wl = CodegenVectorClBuiltin(
                name="Wl", btype=ftype, dim=nparticles, typegen=tg
            )
            wr = CodegenVectorClBuiltin(
                name="Wr", btype=ftype, dim=nparticles, typegen=tg
            )
            w = CodegenVectorClBuiltin(
                name="W", btype=ftype, dim=nparticles, typegen=tg
            )
            weights = (wl, wr, w)
        else:
            w = CodegenVectorClBuiltin(
                name="W", btype=ftype, dim=nparticles, typegen=tg
            )
            weights = (w,)

        with s._function_():
            P.declare(s)
            if remesh_criteria_eps is not None:
                eps.declare(s)
            s.jumpline()

            if debug_mode:
                s.decl_aligned_vars(tst0, tst1)
            s.decl_aligned_vars(rp, find, ind, y)
            vone.declare(s, const=True)
            s.decl_vars(*weights)
            s.jumpline()

            with if_thread_active():
                s.append(f"{rp} = {pos}*{inv_dx};")
                s.append(f"{y} = fract({rp}, &{find});")
                s.append(f"{ind} = convert_{ivtype}_rtn({find});")
                if debug_mode:
                    s.append(f"{tst0} = floor({rp});")
                    s.append("{} = {rp} - floor({rp});".format(tst1, rp=rp))
                    with self._ordered_wi_execution_(barrier=False):
                        code = 'printf("%lu p={vnf}, rp={vnf}, floor(rp)={vnf}, rp-floor(rp)={vnf}, ind={vni}, y={vnf}, s0={vnf}.\\n", {}, {}, {}, {}, {}, {}, {}, {});'.format(
                            "get_local_id(0)",
                            epv(pos),
                            epv(rp),
                            epv(tst0),
                            epv(tst1),
                            epv(ind),
                            epv(y),
                            epv(scalars[0]),
                            vnf=vnf,
                            vni=vni,
                        )
                        s.append(code)
                y.affect(s, init=f"{vone}-{y}")
                ind.affect(s, init=f"{ind} - {P} - {line_offset}")
            if debug_mode:
                s.barrier(_local=True)
                if debug_mode:
                    with s._first_wi_execution_():
                        s.append(f'printf("SCALAR CACHE ({-1}): ");')
                        with s._for_("int ii=0; ii<cache_width; ii++"):
                            s.append('printf("%2.2f, ", S0[ii]);')
                        s.append('printf("\\n");')

            for i, Pi in enumerate(self.polynomials):
                s.jumpline()
                with if_thread_active():
                    s.append(f"{ind} += 1;")
                    with s._align_() as al:
                        for wj, Pij, yj in zip(weights, Pi, (y, f"1-{y}")):
                            wj.affect(al, align=True, init=Pij(y=yj))
                        if poly_splitted:
                            if nparticles > 1:
                                w.affect(
                                    al,
                                    align=True,
                                    init="select({}, {}, ({}<{}))".format(
                                        wr, wl, y, tg.dump(0.5)
                                    ),
                                )
                            else:
                                w.affect(
                                    al,
                                    align=True,
                                    init="convert_{fvtype}({}<{})*{} + convert_{fvtype}({}>={})*{}".format(
                                        y,
                                        tg.dump(0.5),
                                        wl,
                                        y,
                                        tg.dump(0.5),
                                        wr,
                                        fvtype=fvtype,
                                    ),
                                )

                    for iscal, (cached_scalar, scalar) in enumerate(
                        zip(cached_scalars, scalars)
                    ):
                        if nparticles > 1:
                            comment = (
                                "Remeshing scalar {}, {} particles at a time.".format(
                                    iscal, nparticles
                                )
                            )
                            s.jumpline()
                            s.comment(comment)
                        criterias = []
                        with s._block_():
                            for ipart in range(nparticles):
                                cache_idx = f"{cache_ghosts}+{ind[ipart]}"
                                val = f"{w[ipart]}*{scalar[ipart]}"
                                if remesh_criteria_eps is not None:
                                    criteria = (
                                        f"fabs({val}) > {remesh_criteria_eps}*eps"
                                    )
                                    criterias.append(criteria)

                                if use_atomics:
                                    atomic_add = s.reqs["atomic_add"]
                                    atom_add = atomic_add(
                                        p=f"{cached_scalar}+{cache_idx}", val=val
                                    )
                                    if remesh_criteria_eps is not None:
                                        if use_short_circuit:
                                            code = f"({criteria}) && ({atom_add},true);"
                                        else:
                                            code = f"if({criteria}) {{ {atom_add}; }}"
                                    else:
                                        code = f"{atom_add};"
                                else:
                                    inplace_add = f"{cached_scalar[cache_idx]} += {val}"
                                    if remesh_criteria_eps is not None:
                                        if use_short_circuit:
                                            code = "({}) && (({}),true);".format(
                                                criteria, inplace_add
                                            )
                                        else:
                                            code = (
                                                f"if ({criteria}) {{ {inplace_add}; }}"
                                            )
                                    else:
                                        code = f"{inplace_add};"
                                s.append(code)
                    if debug_mode:
                        val = f"{w}*{scalars[0]}"
                        cache_idx = f"{cache_ghosts}+{ind[0]}"

                        if poly_splitted:
                            printf = 'printf("BATCH {}: %lu remeshed {vnf} at idx={vni} with Wl={vnf}, Wr={vnf}, W={vnf}, cond={vni}.\\n",{},{},{},{},{},{},{});'.format(
                                i,
                                "get_local_id(0)",
                                epv(val),
                                epv(ind),
                                epv(wl),
                                epv(wr),
                                epv(w),
                                epv("(y<0.5f)"),
                                vnf=vnf,
                                vni=vni,
                            )
                        else:
                            printf = 'printf("BATCH {}: %lu remeshed {vnf} at idx {vni} with W({vnf})={vnf}, new value is S0[{vni}]={vnf}.\\n",{},{},{},{},{},{},{});'.format(
                                i,
                                "get_local_id(0)",
                                epv(val),
                                epv(ind),
                                epv(y),
                                epv(w),
                                cache_idx,
                                epv(cached_scalars[0][cache_idx]),
                                vni=vni,
                                vnf=vnf,
                            )
                        with s._first_wg_execution_():
                            if criterias:
                                with s._if_(
                                    " || ".join(map(lambda x: f"({x})", criterias))
                                ):
                                    s.append(printf)
                            else:
                                s.append(printf)
                if not use_atomics:
                    s.barrier(_local=True)
                if debug_mode:
                    with s._first_wi_execution_():
                        s.append(f'printf("SCALAR CACHE ({i}): ");')
                        with s._for_("int ii=0; ii<cache_width; ii++"):
                            s.append('printf("%2.2f, ", S0[ii]);')
                        s.append('printf("\\n");')
            if use_atomics:
                s.barrier(_local=True)


if __name__ == "__main__":
    from hysop.backend.device.codegen.base.test import _test_typegen
    from hysop.numerics.remesh.remesh import RemeshKernel

    tg = _test_typegen("float")

    kernel = RemeshKernel(2, 2, split_polys=False)

    work_dim = 3
    nparticles = 2
    nscalars = 1

    use_atomics = False
    remesh_criteria_eps = None

    drf = DirectionalRemeshFunction(
        tg,
        work_dim,
        "int",
        tg.fbtype,
        nparticles,
        nscalars,
        (BoundaryCondition.PERIODIC, BoundaryCondition.PERIODIC),
        kernel,
        use_atomics,
        remesh_criteria_eps,
        debug_mode=False,
    )
    drf.edit()
