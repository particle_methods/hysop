# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sympy as sm

from hysop.tools.htypes import check_instance, first_not_None
from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.codegen.base.function_codegen import (
    OpenClFunctionCodeGenerator,
)
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
)
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.codegen.base.utils import ArgDict
from hysop.backend.device.codegen.base.statistics import WorkStatistics

from hysop.numerics.stencil.stencil import Stencil


class ApplyStencilFunction(OpenClFunctionCodeGenerator):

    def __init__(
        self,
        typegen,
        stencil,
        ftype,
        symbol2vars=None,
        components=1,
        vectorize=True,
        extra_inputs=[],
        scalar_inputs=[],
        vector_inputs=["data"],
        op="{vinput0}[id]",
        custom_id=None,
        vector_suffixes=None,
        data_storage="__local",
        ptr_restrict=True,
        multipliers={},
        itype="int",
        known_args=None,
    ):

        check_instance(stencil, Stencil)
        check_instance(
            symbol2vars, dict, keys=sm.Symbol, values=CodegenVariable, allow_none=True
        )
        symbol2vars = first_not_None(symbol2vars, {})
        assert set(symbol2vars.keys()) == stencil.variables()

        extra_inputs = set(extra_inputs).union(symbol2vars.values())
        scalar_inputs = set(scalar_inputs)
        vector_inputs = set(vector_inputs)

        dim = stencil.dim

        vtype = typegen.vtype(ftype, components)

        if vector_suffixes is None:
            vector_suffixes = tuple(range(components))

        has_custom_id = custom_id is not None

        args = ArgDict()
        for iname in vector_inputs:
            if vectorize:
                name = iname
                args[name] = CodegenVariable(
                    name,
                    vtype,
                    typegen,
                    const=True,
                    add_impl_const=True,
                    storage=data_storage,
                    ptr=True,
                    ptr_restrict=ptr_restrict,
                )
            else:
                for i in range(components):
                    name = f"{iname}{vector_suffixes[i]}"
                    args[name] = CodegenVariable(
                        name,
                        ftype,
                        typegen,
                        const=True,
                        add_impl_const=True,
                        storage=data_storage,
                        ptr=True,
                        ptr_restrict=ptr_restrict,
                    )
        for iname in scalar_inputs:
            name = iname
            args[name] = CodegenVariable(
                name,
                ftype,
                typegen,
                const=True,
                add_impl_const=True,
                storage=data_storage,
                ptr=True,
                ptr_restrict=ptr_restrict,
            )
        for arg in extra_inputs:
            args[arg.name] = arg

        if not has_custom_id:
            args["offset"] = CodegenVariable(
                "offset", itype, typegen, add_impl_const=True
            )
            args["stride"] = CodegenVectorClBuiltin(
                "stride", itype, dim, typegen, add_impl_const=True, nl=True
            )
        for varname, vartype in multipliers.items():
            if vartype == "ftype":
                vartype = ftype
            elif vartype == "vtype":
                vartype = vtype
            args[varname] = CodegenVariable(
                varname, vartype, typegen, add_impl_const=True
            )

        _type = f"{ftype[0]}{components}"
        if vectorize:
            _type += "v"
        basename = f"apply_stencil_{_type}_{stencil.hash()}"
        super().__init__(
            basename=basename,
            output=vtype,
            typegen=typegen,
            inline=True,
            args=args,
            known_args=known_args,
        )

        self.dim = dim
        self.itype = itype
        self.ftype = ftype
        self.vtype = vtype
        self.vectorized = vectorize
        self.components = components
        self.data_storage = data_storage
        self.multipliers = multipliers
        self.stencil = stencil
        self.vector_inputs = vector_inputs
        self.vector_suffixes = vector_suffixes
        self.scalar_inputs = scalar_inputs
        self.has_custom_id = has_custom_id
        self.custom_id = custom_id
        self.op = op
        self.symbol2vars = symbol2vars

        self.gencode()

    def is_cached(self):
        if self.data_storage == "__local":
            return True
        elif self.data_storage == "__global":
            return False
        else:
            raise NotImplemented()

    def per_work_statistics(self):
        tg = self.typegen
        itype = self.itype
        ftype = self.ftype
        components = self.components
        known_args = self.known_args
        cached = self.is_cached()

        ncoeffs = self.stencil.non_zero_coefficients()
        nmul = len(self.multipliers) + int(self.stencil.has_factor())

        stats = WorkStatistics()

        # Reads
        size = tg.FLT_BYTES[ftype]
        byte_reads = components * ncoeffs * size
        if cached:
            stats.local_mem_byte_reads += byte_reads
        else:
            stats.global_mem_byte_reads += byte_reads

        # Ops
        ops = {}
        ops[ftype] = (2 * ncoeffs + nmul) * components
        ops[itype] = 2 * ncoeffs
        stats.ops_per_type = ops

        return stats

    def gencode(self):
        s = self
        dim = s.dim
        tg = self.typegen

        components = s.components
        vectorized = s.vectorized

        has_custom_id = s.has_custom_id

        vector_suffixes = s.vector_suffixes

        ftype = s.ftype
        vtype = s.vtype
        stencil = s.stencil

        res = CodegenVectorClBuiltin("res", ftype, components, tg)

        with s._function_():
            s.append(res.declare(init=0))
            with s._block_():
                with s._align_() as al:
                    for i in range(components):
                        if i > 0:
                            al.jumpline()
                        operands = {}
                        if s.vectorized:
                            vector_varnames = s.vector_inputs
                            _res = res()
                        else:
                            vector_varnames = [
                                f"{varname}{vector_suffixes[i]}"
                                for varname in s.vector_inputs
                            ]
                            _res = res[i]
                        for j, vn in enumerate(vector_varnames):
                            operands[f"vinput{j}"] = s.vars[vn]()
                        for j, vn in enumerate(s.scalar_inputs):
                            operands[f"sinput{j}"] = s.vars[vn]()
                        for off, coeff in stencil.items(include_factor=False):
                            if coeff == "0":
                                continue
                            if not has_custom_id:
                                offset = s.vars["offset"]
                                stride = s.vars["stride"]
                                strided = ""
                                for i in range(dim):
                                    if stride.known() and stride.value[0] == 1:
                                        strided += f"{tg.dump(off[i])}"
                                    else:
                                        strided += f"{tg.dump(off[i])}*{stride[i]}"
                                operands["id"] = f"{offset()}+{strided}"
                            else:
                                operands["id"] = s.custom_id.format(
                                    offset=tg.dump(off[0])
                                )
                            code = "{} += {} $* {};".format(
                                _res,
                                tg.dump_expr(coeff, symbol2vars=s.symbol2vars),
                                s.op.format(**operands),
                            )
                            al.append(code)
                        if vectorized:
                            break

            mul = ""
            for mult in s.multipliers:
                mul += f"{s.vars[mult]()}*"
            if stencil.has_factor():
                mul += f"{tg.dump_expr(stencil.factor, symbol2vars=s.symbol2vars)}*"

            ret = f"return {mul}{res()};"
            s.append(ret)


if __name__ == "__main__":

    from hysop.backend.device.codegen.base.test import _test_typegen

    stencil = Stencil([2.0, 1.0, 0.0, -1.0, -2.0], origin=2, order=2)

    tg = _test_typegen("double", float_dump_mode="hex")
    asf = ApplyStencilFunction(
        tg,
        stencil,
        ftype=tg.fbtype,
        components=3,
        vectorize=False,
        data_storage="__local",
        scalar_inputs=["S"],
        vector_inputs=["A", "B"],
        vector_suffixes=["x", "y", "z"],
        op="{sinput0}[{id}] * ({vinput0}[{id}] + {vinput1}[{id}])",
        multipliers={"a": "int", "b": "float", "c": "ftype", "d": "vtype"},
    )
    print(asf.per_work_statistics())
    asf.edit()
