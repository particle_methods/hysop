# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np

from hysop.tools.htypes import check_instance
from hysop.backend.device.codegen.base.union_codegen import UnionCodeGenerator
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen


class FloatIntegerUnion(UnionCodeGenerator):
    def __init__(self, ftype, typegen, typedef=None):

        name, dtype, comments = self.build_dtype(typegen, ftype)

        super().__init__(
            name=name, dtype=dtype, typegen=typegen, typedef=typedef, comments=comments
        )

    @staticmethod
    def build_dtype(typegen, ftype):
        tg = typegen

        name = "float_int"

        dtype = []
        if ftype == "half":
            name += "16"
            dtype.append(("intval", np.int16))
            dtype.append(("uintval", np.uint16))
            dtype.append(("floatval", np.float16))
        elif ftype == "float":
            name += "32"
            dtype.append(("intval", np.int32))
            dtype.append(("uintval", np.uint32))
            dtype.append(("floatval", np.float32))
        elif ftype == "double":
            name += "64"
            dtype.append(("intval", np.int64))
            dtype.append(("uintval", np.uint64))
            dtype.append(("floatval", np.float64))
        else:
            msg = "Unknown ftype '{}', only half, float and double are supported."
            msg = msg.format(ftype)
            raise ValueError(msg)

        comments = [
            "Access value as a signed integer",
            "Access value as a unsigned integer",
            "Access value as a floating point",
        ]

        return name, dtype, comments


if __name__ == "__main__":
    from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
    from hysop.backend.device.codegen.base.test import _test_typegen

    tg = _test_typegen()

    u1 = FloatIntegerUnion("double", tg)
    u2 = FloatIntegerUnion("float", tg, "custom32")

    cg = OpenClCodeGenerator("test_generator", tg)
    cg.declare_cl_extension("cl_khr_fp64")
    cg.require("u1", u1)
    cg.require("u2", u2)

    cg.edit()

    cg.test_compile()
