# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import contextlib
import math
import operator
import hashlib
from contextlib import contextmanager
import numpy as np

from hysop import __VERBOSE__, __KERNEL_DEBUG__

from hysop.tools.misc import Utils, upper_pow2_or_3
from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.numpywrappers import npw
from hysop.tools.contexts import nested
from hysop.constants import DirectionLabels, BoundaryCondition, Backend, Precision

from hysop.core.arrays.all import OpenClArray
from hysop.numerics.remesh.remesh import RemeshKernel
from hysop.numerics.remesh.kernel_generator import Kernel
from hysop.fields.continuous_field import Field
from hysop.fields.discrete_field import DiscreteScalarFieldView

from hysop.backend.device.opencl import cl, clTools, clCharacterize
from hysop.backend.device.opencl.opencl_env import OpenClEnvironment
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.opencl.opencl_array_backend import OpenClArrayBackend

from hysop.backend.device.codegen import CodeGeneratorWarning
from hysop.backend.device.codegen.base.utils import WriteOnceDict, ArgDict
from hysop.backend.device.codegen.base.statistics import WorkStatistics
from hysop.backend.device.codegen.base.variables import CodegenStruct
from hysop.backend.device.codegen.structs.mesh_info import (
    MeshBaseStruct,
    MeshInfoStruct,
)
from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.codegen.base.kernel_codegen import KernelCodeGenerator
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
    CodegenArray,
)

from hysop.backend.device.codegen.functions.directional_remesh import (
    DirectionalRemeshFunction,
)


class DirectionalRemeshKernelGenerator(KernelCodeGenerator):

    @staticmethod
    def codegen_name(
        work_dim,
        remesh_kernel,
        ftype,
        nparticles,
        nscalars,
        remesh_criteria_eps,
        use_atomics,
        is_inplace,
    ):
        inplace = "inplace_" if is_inplace else ""
        atomic = "atomic_" if use_atomics else ""
        criteria = (
            f"{remesh_criteria_eps}eps__"
            if (remesh_criteria_eps is not None)
            else "full"
        )
        return "directional_{}{}remesh_{}d__lambda_{}_{}__{}__{}p__{}s__{}".format(
            inplace,
            atomic,
            work_dim,
            remesh_kernel.n,
            remesh_kernel.r,
            ftype,
            nparticles,
            nscalars,
            criteria,
        )

    @classmethod
    def scalars_out_cache_ghosts(cls, scalar_cfl, remesh_kernel):
        assert scalar_cfl > 0.0, "cfl <= 0.0"
        assert remesh_kernel.n >= 1, "Bad remeshing kernel."
        if remesh_kernel.n > 1:
            assert remesh_kernel.n % 2 == 0, "Odd remeshing kernel moments."
        min_ghosts = int(1 + npw.floor(scalar_cfl) + remesh_kernel.n // 2)
        return min_ghosts

    @classmethod
    def get_max_global_size(cls, work_size, work_load, nparticles, **kargs):
        """
        Return global_work_size from effective work_size without
        taking into account local_work_size alignment
        """
        work_size = np.asarray(work_size).copy()
        work_load = np.asarray(work_load).copy()

        assert work_load[0] == 1
        work_load[0] = nparticles

        global_size = work_size.copy()
        global_size = (global_size + work_load - 1) // work_load
        return global_size

    def get_global_size(self, work_size, local_work_size, work_load=None):
        """
        Return global_work_size from effective work_size and given local_work_size
        global_work_size will be a multiple of local_work_size
        """
        work_dim = self.work_dim
        work_load = [1] * work_dim if (work_load is None) else work_load

        work_size = np.asarray(work_size)
        work_load = np.asarray(work_load)
        local_work_size = np.asarray(local_work_size)

        nparticles = self.nparticles

        for i in range(1, work_dim):
            assert local_work_size[i] == 1, "local_work_size error!"

        if "local_size" in self.known_vars:
            assert (
                self.known_vars["local_size"] == local_work_size[:work_dim]
            ).all(), "local_work_size mismatch!"

        max_global_size = self.get_max_global_size(work_size, work_load, nparticles)
        global_size = (
            (max_global_size + local_work_size - 1) // local_work_size
        ) * local_work_size
        global_size[0] = local_work_size[0]

        return global_size

    def required_workgroup_cache_size(self, local_work_size):
        """
        Return a tuple of required (static,dynamic,total) cache bytes per workgroup
        """
        work_dim = self.work_dim
        ftype = self.ftype
        flt_bytes = self.typegen.FLT_BYTES[ftype]

        local_work_size = np.asarray(local_work_size)

        sc, dc = 0, 0
        count = self.nscalars * (
            self.nparticles * local_work_size[0] + 2 * self.min_ghosts
        )
        if self.local_size_known:
            assert (self.known_vars["local_size"] == local_work_size[:work_dim]).all()
            sc += count
        else:
            dc += count

        sc *= flt_bytes
        dc *= flt_bytes
        tc = sc + dc

        return (sc, dc, tc)

    def __init__(
        self,
        typegen,
        work_dim,
        ftype,
        nparticles,
        nscalars,
        sboundary,
        is_inplace,
        scalar_cfl,
        remesh_kernel,
        use_short_circuit=None,
        unroll_loops=None,
        group_scalars=None,
        remesh_criteria_eps=None,
        use_atomics=False,
        symbolic_mode=False,
        debug_mode=False,
        tuning_mode=False,
        known_vars=None,
    ):

        assert work_dim > 0 and work_dim <= 3
        assert nscalars > 0
        assert nparticles in [1, 2, 4, 8, 16]
        check_instance(sboundary, tuple, values=BoundaryCondition)
        check_instance(remesh_kernel, (RemeshKernel, Kernel))

        use_short_circuit = first_not_None(
            use_short_circuit, typegen.use_short_circuit_ops
        )
        unroll_loops = first_not_None(unroll_loops, typegen.unroll_loops)

        # unrolling loops take to much time during autotuning
        if tuning_mode:
            unroll_loops = False

        group_scalars = first_not_None(group_scalars, tuple(1 for _ in range(nscalars)))
        check_instance(group_scalars, tuple, values=int)
        assert sum(group_scalars) == nscalars
        nfields = len(group_scalars)

        assert sboundary[0] in [BoundaryCondition.PERIODIC, BoundaryCondition.NONE]
        assert sboundary[1] in [BoundaryCondition.PERIODIC, BoundaryCondition.NONE]
        is_periodic = (
            sboundary[0] == BoundaryCondition.PERIODIC
            and sboundary[1] == BoundaryCondition.PERIODIC
        )

        if is_periodic:
            msg = "Local periodic boundary have been deprecated, use BoundaryCondition.NONE instead."
            raise RuntimeError(msg)

        known_vars = first_not_None(known_vars, {})
        local_size_known = "local_size" in known_vars

        itype = "int"
        vftype = typegen.vtype(ftype, nparticles)
        vitype = typegen.vtype(itype, nparticles)

        name = DirectionalRemeshKernelGenerator.codegen_name(
            work_dim,
            remesh_kernel,
            ftype,
            nparticles,
            nscalars,
            remesh_criteria_eps,
            use_atomics,
            is_inplace,
        )

        kernel_reqs = self.build_requirements(
            typegen,
            work_dim,
            itype,
            ftype,
            sboundary,
            nparticles,
            nscalars,
            nfields,
            group_scalars,
            symbolic_mode,
            remesh_criteria_eps,
            use_atomics,
            remesh_kernel,
            use_short_circuit,
            known_vars,
            debug_mode,
        )

        kernel_args = self.gen_kernel_arguments(
            typegen,
            work_dim,
            itype,
            ftype,
            nparticles,
            nscalars,
            nfields,
            group_scalars,
            local_size_known,
            is_inplace,
            debug_mode,
            kernel_reqs,
            known_vars,
            symbolic_mode,
        )

        super().__init__(
            name=name,
            typegen=typegen,
            work_dim=work_dim,
            kernel_args=kernel_args,
            known_vars=known_vars,
            vec_type_hint=ftype,
            symbolic_mode=symbolic_mode,
        )

        self.update_requirements(kernel_reqs)

        self.min_ghosts = self.scalars_out_cache_ghosts(scalar_cfl, remesh_kernel)
        self.itype = itype
        self.ftype = ftype
        self.vitype = vitype
        self.vftype = vftype
        self.work_dim = work_dim
        self.sboundary = sboundary
        self.nparticles = nparticles
        self.nscalars = nscalars
        self.nfields = nfields
        self.group_scalars = group_scalars
        self.local_size_known = local_size_known
        self.is_inplace = is_inplace
        self.use_atomics = use_atomics
        self.use_short_circuit = use_short_circuit
        self.unroll_loops = unroll_loops
        self.remesh_kernel = remesh_kernel
        self.debug_mode = debug_mode
        self.tuning_mode = tuning_mode

        self.gencode()
        # self.edit()

    def build_requirements(
        self,
        typegen,
        work_dim,
        itype,
        ftype,
        sboundary,
        nparticles,
        nscalars,
        nfields,
        group_scalars,
        symbolic_mode,
        remesh_criteria_eps,
        use_atomics,
        remesh_kernel,
        use_short_circuit,
        known_vars,
        debug_mode,
    ):
        reqs = WriteOnceDict()

        vsize = upper_pow2_or_3(work_dim)

        mesh_base_struct = MeshBaseStruct(typegen=typegen, vsize=vsize)
        reqs["MeshBaseStruct"] = mesh_base_struct

        mesh_info_struct = MeshInfoStruct(typegen=typegen, vsize=vsize)
        reqs["MeshInfoStruct"] = mesh_info_struct

        # without atomics we can only remesh on particle at a time
        nparticles_remeshed = nparticles if use_atomics else 1
        reqs["remesh"] = DirectionalRemeshFunction(
            typegen=typegen,
            work_dim=work_dim,
            itype=itype,
            ftype=ftype,
            nparticles=nparticles_remeshed,
            nscalars=nscalars,
            sboundary=sboundary,
            remesh_kernel=remesh_kernel,
            use_atomics=use_atomics,
            remesh_criteria_eps=remesh_criteria_eps,
            debug_mode=debug_mode,
            use_short_circuit=use_short_circuit,
        )

        return reqs

    def gen_kernel_arguments(
        self,
        typegen,
        work_dim,
        itype,
        ftype,
        nparticles,
        nscalars,
        nfields,
        group_scalars,
        local_size_known,
        is_inplace,
        debug_mode,
        kernel_reqs,
        known_vars,
        symbolic_mode,
    ):

        kargs = ArgDict()
        mesh_dim = upper_pow2_or_3(work_dim)
        self.position, self.position_strides = (
            OpenClArrayBackend.build_codegen_arguments(
                kargs,
                name="position",
                known_vars=known_vars,
                symbolic_mode=symbolic_mode,
                storage=self._global,
                ctype=ftype,
                typegen=typegen,
                mesh_dim=mesh_dim,
                ptr_restrict=True,
                const=True,
            )
        )

        scalars_data_in = []
        scalars_strides_in = []
        for i in range(nfields):
            args_in = []
            strides_in = []
            for j in range(group_scalars[i]):
                if is_inplace:
                    arg, strides = OpenClArrayBackend.build_codegen_arguments(
                        kargs,
                        name=f"S{i}_{j}_inout",
                        known_vars=known_vars,
                        symbolic_mode=symbolic_mode,
                        storage=self._global,
                        ctype=ftype,
                        typegen=typegen,
                        mesh_dim=mesh_dim,
                        const=False,
                        ptr_restrict=True,
                    )
                else:
                    arg, strides = OpenClArrayBackend.build_codegen_arguments(
                        kargs,
                        name=f"S{i}_{j}_in",
                        known_vars=known_vars,
                        symbolic_mode=symbolic_mode,
                        storage=self._global,
                        ctype=ftype,
                        typegen=typegen,
                        mesh_dim=mesh_dim,
                        const=True,
                        ptr_restrict=True,
                    )
                args_in.append(arg)
                strides_in.append(strides)
            scalars_data_in.append(tuple(args_in))
            scalars_strides_in.append(tuple(strides_in))
        scalars_data_in = tuple(scalars_data_in)
        scalars_strides_in = tuple(scalars_strides_in)

        if is_inplace:
            scalars_data_out = scalars_data_in
            scalars_strides_out = scalars_strides_in
        else:
            scalars_data_out = []
            scalars_strides_out = []
            for i in range(nfields):
                args_out = []
                strides_out = []
                for j in range(group_scalars[i]):
                    arg, strides = OpenClArrayBackend.build_codegen_arguments(
                        kargs,
                        name=f"S{i}_{j}_out",
                        known_vars=known_vars,
                        symbolic_mode=symbolic_mode,
                        storage=self._global,
                        ctype=ftype,
                        typegen=typegen,
                        mesh_dim=mesh_dim,
                        const=False,
                        ptr_restrict=True,
                    )
                    args_out.append(arg)
                    strides_out.append(strides)
                scalars_data_out.append(tuple(args_out))
                scalars_strides_out.append(tuple(strides_out))
            scalars_data_out = tuple(scalars_data_out)
            scalars_strides_out = tuple(scalars_strides_out)

        self.scalars_data_in = scalars_data_in
        self.scalars_data_out = scalars_data_out
        self.scalars_strides_in = scalars_strides_in
        self.scalars_strides_out = scalars_strides_out

        # if debug_mode:
        # kargs['dbg0'] = CodegenVariable(storage=self._global,name='dbg0',ctype=itype,
        # typegen=typegen, ptr_restrict=True,ptr=True,const=False,add_impl_const=True)
        # kargs['dbg1'] = CodegenVariable(storage=self._global,name='dbg1',ctype=itype,
        # typegen=typegen, ptr_restrict=True,ptr=True,const=False,add_impl_const=True)

        kargs["position_mesh_info"] = kernel_reqs[
            "MeshInfoStruct"
        ].build_codegen_variable(const=True, name="position_mesh_info")

        if is_inplace:
            for i in range(nfields):
                kargs[f"S{i}_inout_mesh_info"] = kernel_reqs[
                    "MeshInfoStruct"
                ].build_codegen_variable(
                    const=True, name=f"S{i}_inout_mesh_info", nl=True
                )
        else:
            for i in range(nfields):
                kargs[f"S{i}_in_mesh_info"] = kernel_reqs[
                    "MeshInfoStruct"
                ].build_codegen_variable(const=True, name=f"S{i}_in_mesh_info", nl=True)
            for i in range(nfields):
                kargs[f"S{i}_out_mesh_info"] = kernel_reqs[
                    "MeshInfoStruct"
                ].build_codegen_variable(
                    const=True, name=f"S{i}_out_mesh_info", nl=True
                )

        if not local_size_known:
            kargs["buffer"] = CodegenVariable(
                storage=self._local,
                ctype=ftype,
                add_impl_const=True,
                name="buffer",
                ptr=True,
                ptr_restrict=True,
                typegen=typegen,
                nl=False,
            )

        return kargs

    def gencode(self):
        s = self
        tg = s.typegen

        dim = s.work_dim
        itype = s.itype
        ftype = s.ftype
        vitype = s.vitype
        vftype = s.vftype
        sboundary = s.sboundary
        nparticles = s.nparticles
        nscalars = s.nscalars
        nfields = s.nfields
        min_ghosts = s.min_ghosts
        is_inplace = s.is_inplace
        use_atomics = s.use_atomics
        work_dim = s.work_dim
        debug_mode = s.debug_mode
        tuning_mode = s.tuning_mode

        use_short_circuit = s.use_short_circuit
        unroll_loops = s.unroll_loops

        symbolic_mode = s.symbolic_mode
        group_scalars = s.group_scalars

        local_size_known = s.local_size_known

        global_id = s.vars["global_id"]
        local_id = s.vars["local_id"]
        group_id = s.vars["group_id"]

        global_index = s.vars["global_index"]
        local_index = s.vars["local_index"]

        global_size = s.vars["global_size"]
        local_size = s.vars["local_size"]

        # if debug_mode:
        # dbg0 = s.vars['dbg0']
        # dbg1 = s.vars['dbg1']

        lb = "[" if (nparticles > 1) else ""
        rb = "]" if (nparticles > 1) else ""
        vnf = "{}{}{}".format(lb, ", ".join("%2.2f" for _ in range(nparticles)), rb)
        vni = "{}{}{}".format(lb, ", ".join("%i" for _ in range(nparticles)), rb)

        def expand_printf_vector(x):
            return (
                str(x)
                if (nparticles == 1)
                else ",".join(
                    (
                        "({}).s{}".format(x, "0123456789abcdef"[i])
                        if isinstance(x, str)
                        else x[i]
                    )
                    for i in range(nparticles)
                )
            )

        epv = expand_printf_vector

        position_mesh_info = s.vars["position_mesh_info"]

        if is_inplace:
            scalars_mesh_info_in = [
                s.vars[f"S{i}_inout_mesh_info"] for i in range(nfields)
            ]
            scalars_mesh_info_out = scalars_mesh_info_in
        else:
            scalars_mesh_info_in = [
                s.vars[f"S{i}_in_mesh_info"] for i in range(nfields)
            ]
            scalars_mesh_info_out = [
                s.vars[f"S{i}_out_mesh_info"] for i in range(nfields)
            ]

        position_base = s.vars["position_base"]
        position_offset = s.vars["position_offset"]
        position_strides = s.vars["position_strides"]

        position = s.position
        scalars_data_in = s.scalars_data_in
        scalars_data_out = s.scalars_data_out
        scalars_strides_in = s.scalars_strides_in
        scalars_strides_out = s.scalars_strides_out

        compute_grid_size = position_mesh_info["local_mesh"]["compute_resolution"].view(
            "compute_grid_size", slice(None, work_dim), const=True
        )

        position_grid_size = position_mesh_info["local_mesh"]["resolution"].view(
            "pos_grid_size", slice(0, work_dim), const=True
        )
        position_grid_ghosts = position_mesh_info["ghosts"].view(
            "pos_grid_ghosts", slice(0, work_dim), const=True
        )
        position_global_id = CodegenVectorClBuiltin(
            "pos_gid", itype, work_dim, typegen=tg
        )

        dx = position_mesh_info["dx"].view("dx", slice(0, 1), const=True)
        inv_dx = position_mesh_info["inv_dx"].view("inv_dx", slice(0, 1), const=True)
        xmin = position_mesh_info["local_mesh"]["xmin"].view(
            "xmin", slice(0, 1), const=True
        )

        if is_inplace:
            scalars_in_grid_size = tuple(
                smi["local_mesh"]["resolution"].view(
                    f"S{i}_inout_grid_size", slice(0, work_dim), const=True
                )
                for (i, smi) in enumerate(scalars_mesh_info_out)
            )
            scalars_in_grid_ghosts = tuple(
                smi["ghosts"].view(
                    f"S{i}_inout_grid_ghosts", slice(0, work_dim), const=True
                )
                for (i, smi) in enumerate(scalars_mesh_info_out)
            )

            scalars_in_global_id = tuple(
                CodegenVectorClBuiltin(f"S{i}_inout_gid", itype, work_dim, typegen=tg)
                for i in range(nfields)
            )

            scalars_data_out_grid_size = scalars_in_grid_size
            scalars_data_out_grid_ghosts = scalars_in_grid_ghosts
            scalars_data_out_global_id = scalars_in_global_id

            grid_ghosts = (position_grid_ghosts,) + scalars_in_grid_ghosts
            grid_sizes = (position_grid_size,) + scalars_in_grid_size
            global_ids = (position_global_id,) + scalars_in_global_id
        else:
            scalars_in_grid_size = tuple(
                smi["local_mesh"]["resolution"].view(
                    f"S{i}_in_grid_size", slice(0, work_dim), const=True
                )
                for (i, smi) in enumerate(scalars_mesh_info_in)
            )
            scalars_data_out_grid_size = tuple(
                smi["local_mesh"]["resolution"].view(
                    f"S{i}_out_grid_size", slice(0, work_dim), const=True
                )
                for (i, smi) in enumerate(scalars_mesh_info_out)
            )

            scalars_in_grid_ghosts = tuple(
                smi["ghosts"].view(
                    f"S{i}_in_grid_ghosts", slice(0, work_dim), const=True
                )
                for (i, smi) in enumerate(scalars_mesh_info_in)
            )
            scalars_data_out_grid_ghosts = tuple(
                smi["ghosts"].view(
                    f"S{i}_out_grid_ghosts", slice(0, work_dim), const=True
                )
                for (i, smi) in enumerate(scalars_mesh_info_out)
            )

            scalars_in_global_id = tuple(
                CodegenVectorClBuiltin(f"S{i}_in_gid", itype, work_dim, typegen=tg)
                for i in range(nfields)
            )
            scalars_data_out_global_id = tuple(
                CodegenVectorClBuiltin(f"S{i}_out_gid", itype, work_dim, typegen=tg)
                for i in range(nfields)
            )

            grid_ghosts = (
                (position_grid_ghosts,)
                + scalars_in_grid_ghosts
                + scalars_data_out_grid_ghosts
            )
            grid_sizes = (
                (position_grid_size,)
                + scalars_in_grid_size
                + scalars_data_out_grid_size
            )
            global_ids = (
                (position_global_id,)
                + scalars_in_global_id
                + scalars_data_out_global_id
            )

        s.update_vars(
            position=position,
            inv_dx=inv_dx,
            position_grid_ghosts=position_grid_ghosts,
            compute_grid_size=compute_grid_size,
        )
        s.update_vars(**{sij.name: sij for si in scalars_data_in for sij in si})
        if not is_inplace:
            s.update_vars(**{sij.name: sij for si in scalars_data_out for sij in si})

        npart = CodegenVariable(
            name="nparticles", ctype=itype, typegen=tg, value=nparticles
        )

        cache_ghosts = CodegenVariable(
            "cache_ghosts",
            itype,
            typegen=tg,
            value=min_ghosts,
            symbolic_mode=symbolic_mode,
        )
        cache_width = CodegenVariable(
            "cache_width",
            itype,
            typegen=tg,
            init=f"{npart}*{local_size[0]} + 2*{cache_ghosts}",
        )

        local_work = CodegenVariable(
            "lwork", "int", tg, const=True, init=f"{nparticles}*{local_size[0]}"
        )

        local_offset = CodegenVariable("local_offset", itype, tg)
        line_offset = CodegenVariable("line_offset", itype, tg)
        particle_offset = CodegenVariable("particle_offset", itype, tg)

        def _line_init(base_ptr, global_id, grid_strides):
            _id = ""
            if work_dim > 1:
                _id += f"$ + ({global_id[work_dim-1]} $* {grid_strides[work_dim-1]})"
                for i in range(work_dim - 2, 0, -1):
                    _id += f" $+ ({global_id[i]} $* {grid_strides[i]})"
            _id += f" $+ (({global_id[0]} $- {particle_offset}))"
            return f"{base_ptr}{_id}"

        line_position = position.newvar(
            "line_position",
            init=_line_init(position, position_global_id, position_strides),
        )

        line_scalars_in = []
        line_scalars_data_out = []
        for Si, Si_global_id, Si_grid_strides in zip(
            scalars_data_in, scalars_in_global_id, scalars_strides_in
        ):
            Li = []
            for Sij, Sij_strides in zip(Si, Si_grid_strides):
                lij = Sij.newvar(
                    f"line_{Sij.name}", init=_line_init(Sij, Si_global_id, Sij_strides)
                )
                Li.append(lij)
            line_scalars_in.append(tuple(Li))
        for Si, Si_global_id, Si_grid_strides in zip(
            scalars_data_out, scalars_data_out_global_id, scalars_strides_out
        ):
            Li = []
            for Sij, Sij_strides in zip(Si, Si_grid_strides):
                lij = Sij.newvar(
                    f"line_{Sij.name}", init=_line_init(Sij, Si_global_id, Sij_strides)
                )
                Li.append(lij)
            line_scalars_data_out.append(tuple(Li))
        line_scalars_in = tuple(line_scalars_in)
        line_scalars_data_out = tuple(line_scalars_data_out)

        line_vars = ((line_position,),) + line_scalars_in
        if not is_inplace:
            line_vars += line_scalars_data_out

        cached_scalars = []
        boundary_scalars = []
        if local_size_known:
            L = s.known_vars["local_size"]
            for i in range(nfields):
                Si = []
                BSi = []
                for j in range(group_scalars[i]):
                    Sij = CodegenArray(
                        name=f"S{i}_{j}",
                        dim=1,
                        ctype=ftype,
                        typegen=tg,
                        shape=(nparticles * L[0] + 2 * min_ghosts,),
                        storage=self._local,
                    )
                    Si.append(Sij)

                cached_scalars.append(tuple(Si))
        else:
            buf = self.vars["buffer"]
            k = 0
            for i in range(nfields):
                Si = []
                BSi = []
                for j in range(group_scalars[i]):
                    Sij = CodegenVariable(
                        name=f"S{i}_{j}",
                        ctype=ftype,
                        typegen=tg,
                        ptr_restrict=True,
                        ptr=True,
                        storage=self._local,
                        ptr_const=True,
                        init=f"{buf} + {k}*{cache_width}",
                    )
                    Si.append(Sij)

                    k += 1
                cached_scalars.append(tuple(Si))
        cache_scalars = tuple(cached_scalars)
        boundary_scalars = tuple(boundary_scalars)

        pos = CodegenVectorClBuiltin("p", ftype, nparticles, tg)
        tuning_pos = CodegenVectorClBuiltin("tp", ftype, nparticles, tg)
        scalars = []
        for i in range(nfields):
            si = []
            for j in range(group_scalars[i]):
                sij = CodegenVectorClBuiltin(f"s{i}_{j}", ftype, nparticles, tg)
                si.append(sij)
            scalars.append(tuple(si))
        scalars = tuple(scalars)

        vzero = CodegenVectorClBuiltin(
            "vzero",
            ftype,
            nparticles,
            tg,
            value=np.zeros(shape=(nparticles,), dtype=np.float64),
        )

        kmax = CodegenVariable(
            "kmax",
            itype,
            tg,
            const=True,
            init="(({}+{lwork}-1)/{lwork})".format(
                compute_grid_size[0], lwork=local_work
            ),
        )

        loopvars = "kji"
        first = CodegenVariable(
            "first", "bool", tg, const=True, init=f"({loopvars[0]}==0)"
        )
        last = CodegenVariable(
            "last", "bool", tg, const=True, init=f"({loopvars[0]}=={kmax}-1)"
        )
        active = CodegenVariable("active", "bool", tg, const=True)
        last_active = CodegenVariable("last_active", "bool", tg, const=True)

        last_particle = CodegenVariable(
            "last_particle",
            itype,
            tg,
            const=True,
            init=f"{compute_grid_size[0]} - {nparticles}*({kmax}-1)*{local_size[0]}",
        )

        @contextlib.contextmanager
        def _work_iterate_(i):
            try:
                if i == 0:
                    fval = "0"
                    gsize = "1"
                    N = kmax
                    ghosts = f"({position_grid_ghosts[i]}-{cache_ghosts})"
                else:
                    fval = global_id.fval(i)
                    gsize = global_size[i]
                    N = f"{compute_grid_size[i]}"
                    ghosts = position_grid_ghosts[i]

                with s._for_(
                    "int {i}={fval}; {i}<{N}; {i}+={gsize}".format(
                        i=loopvars[i], fval=fval, gsize=gsize, N=N
                    ),
                    unroll=(i == 0) and unroll_loops,
                ) as ctx:

                    if i == 0:
                        with s._align_() as al:
                            line_offset.declare(
                                al,
                                align=True,
                                const=True,
                                init="{}*{}".format(loopvars[0], local_work),
                            )
                            local_offset.declare(
                                al,
                                align=True,
                                const=True,
                                init="{}*{}".format(nparticles, local_id[0]),
                            )
                            particle_offset.declare(
                                al,
                                align=True,
                                const=True,
                                init="{} + {}".format(line_offset, local_offset),
                            )
                        s.jumpline()
                        with s._align_() as al:
                            first.declare(al, align=True)
                            last.declare(al, align=True)
                            active.declare(
                                al,
                                init="({} < {})".format(
                                    particle_offset, compute_grid_size[0]
                                ),
                                align=True,
                            )
                            last_active.declare(
                                al,
                                init="{} && ({}+{}-1 >= {})".format(
                                    active,
                                    particle_offset,
                                    nparticles,
                                    compute_grid_size[0],
                                ),
                                align=True,
                            )
                    elif i == 1:
                        kmax.declare(s)
                        last_particle.declare(s)
                    s.jumpline()

                    if i > 0:
                        with s._align_() as al:
                            for field_gid, field_ghosts in zip(global_ids, grid_ghosts):
                                al.append(
                                    "{} $= {} $+ {};".format(
                                        field_gid[i], loopvars[i], field_ghosts[i]
                                    )
                                )
                            al.jumpline()
                    yield ctx
            except:
                raise

        # when compute_grid_size is not a multiple of nparticles,
        # we need to go back to scalars load/store for the last particles.
        def if_last_thread_active(cond=None):
            cond = last_active if (cond is None) else cond
            with s._if_(cond):
                with s._align_() as al:
                    for j in range(nparticles):
                        cond = f"({particle_offset}+{j}$ < {compute_grid_size[0]})"
                        yield al, cond, j

        @contextlib.contextmanager
        def if_thread_active(not_first=False):
            if not_first:
                cond = f"(!{first}) && {active}"
            else:
                cond = active
            with s._elif_(cond):
                yield

        def if_first_or_last_thread_active():
            # cond= '{} || {}'.format(last_active, first)
            cond = f"{last_active}"
            return if_last_thread_active(cond)

        with s._kernel_():
            s.jumpline()

            vars_ = ((position,),) + scalars_data_in
            if not is_inplace:
                vars_ += scalars_data_out
            s.decl_aligned_vars(*tuple(vij for vi in vars_ for vij in vi))

            s.decl_aligned_vars(local_id, global_size, local_size, const=True)

            s.decl_aligned_vars(xmin, inv_dx, dx, const=True)

            s.decl_aligned_vars(
                npart, cache_ghosts, cache_width, local_work, const=True
            )

            s.decl_aligned_vars(vzero, const=True)

            s.decl_vars(compute_grid_size)
            s.jumpline()

            s.decl_aligned_vars(*grid_sizes)
            s.decl_aligned_vars(*grid_ghosts)

            for vargroup in cached_scalars:
                s.decl_vars(*vargroup)
            s.jumpline()

            s.decl_vars(*global_ids)

            with s._align_() as al:
                _pos = (pos,)
                if tuning_mode:
                    _pos += (tuning_pos,)
                s.decl_vars(
                    *(_pos + tuple(sij for si in scalars for sij in si)),
                    align=True,
                    codegen=al,
                )
            s.jumpline()

            nested_loops = [_work_iterate_(i) for i in range(dim - 1, -1, -1)]
            if work_dim == 1:
                kmax.declare(s)
                last_particle.declare(s)
            with nested(*nested_loops):

                s.comment("Compute global offsets and line pointers")
                with s._align_() as al:
                    position_global_id.affect(
                        al,
                        i=0,
                        align=True,
                        init=f"{particle_offset} + {position_grid_ghosts[0]}",
                    )
                    if not is_inplace:
                        for _gid, _ghosts in zip(
                            scalars_in_global_id, scalars_in_grid_ghosts
                        ):
                            _gid.affect(
                                al,
                                i=0,
                                align=True,
                                init=f"{particle_offset} + {_ghosts[0]}",
                            )
                    for _gid, _ghosts in zip(
                        scalars_data_out_global_id, scalars_data_out_grid_ghosts
                    ):
                        _gid.affect(
                            al,
                            i=0,
                            align=True,
                            init="{} + {} - {}".format(
                                particle_offset, _ghosts[0], cache_ghosts
                            ),
                        )
                s.jumpline()
                s.decl_aligned_vars(*(lvij for lvi in line_vars for lvij in lvi))

                s.comment("Get back left cache from right cache")
                with s._for_(
                    "int l={}; l<2*{}; l+={}".format(
                        local_id[0], cache_ghosts, local_size[0]
                    ),
                    unroll=unroll_loops,
                ):
                    with s._if_(f"{first}"):
                        with s._align_() as al:
                            for csi in cached_scalars:
                                for csij in csi:
                                    csij.affect(
                                        al, align=True, i="l", init=tg.dump(+0.0)
                                    )
                    with s._else_():
                        end_id = "{}+{}".format(local_work, "l")
                        if debug_mode:
                            s.append(
                                'printf("%i loaded back %2.2f from position %i.\\n", {}, {}, {});'.format(
                                    local_id[0], cached_scalars[0][0][end_id], end_id
                                )
                            )
                        with s._align_() as al:
                            for csi in cached_scalars:
                                for csij in csi:
                                    csij.affect(
                                        al, align=True, i="l", init=csij[end_id]
                                    )
                s.barrier(_local=True)
                s.jumpline()

                s.comment("Fill right cache with zeros, excluding left ghosts")
                for csi in cached_scalars:
                    for csij in csi:
                        offset = f"{local_offset}+2*{cache_ghosts}"
                        s.append(s.vstore(nparticles, csij, offset, vzero))
                s.jumpline()

                s.comment(
                    f"Load position and scalars at current index, {nparticles} particles at a time."
                )
                for al, active_cond, k in if_last_thread_active():
                    load = self.vload(
                        1, line_position, f"{particle_offset}+{k}", align=True
                    )
                    if use_short_circuit:
                        code = "{} $= ({} $? {} $: {});".format(
                            pos[k], active_cond, load, tg.dump(0.0)
                        )
                    else:
                        code = "if ({}) {{ {posk} $= {}; $}} $else {{ {posk} $= {}; $}}".format(
                            active_cond, load, tg.dump(0.0), posk=pos[k]
                        )
                    al.append(code)
                    if is_inplace:
                        _id = f"{particle_offset}+{cache_ghosts}+{k}"
                    else:
                        _id = f"{particle_offset}+{k}"
                    for si, line_si in zip(scalars, line_scalars_in):
                        for sij, line_sij in zip(si, line_si):
                            load = self.vload(1, line_sij, _id, align=True)
                            if use_short_circuit:
                                code = "{} $= ({} $? {} $: {});".format(
                                    sij[k], active_cond, load, tg.dump(0.0)
                                )
                            else:
                                code = "if ({}) {{ {sijk} $= {}; $}} $else {{ {sijk} $= {}; $}}".format(
                                    active_cond, load, tg.dump(0.0), sijk=sij[k]
                                )
                            al.append(code)
                with if_thread_active():
                    with s._align_() as al:
                        pos.affect(
                            al,
                            align=True,
                            init=self.vload(
                                nparticles, line_position, particle_offset, align=True
                            ),
                        )

                        if is_inplace:
                            _id = f"{particle_offset}+{cache_ghosts}"
                        else:
                            _id = particle_offset
                        for si, line_si in zip(scalars, line_scalars_in):
                            for sij, line_sij in zip(si, line_si):
                                sij.affect(
                                    al,
                                    align=True,
                                    init=self.vload(
                                        nparticles, line_sij, _id, align=True
                                    ),
                                )
                with s._else_():
                    for k in range(nparticles):
                        code = f"{pos[k]} = NAN;"
                        s.append(code)
                        for si, line_si in zip(scalars, line_scalars_in):
                            for sij, line_sij in zip(si, line_si):
                                code = f"{sij[k]} = NAN;"
                                s.append(code)

                s.barrier(_local=True)
                s.jumpline()

                if tuning_mode:
                    code = "{} = max(min({pos}, ({pid}+{one})*{dx}), ({pid}-{one})*{dx});".format(
                        tuning_pos,
                        pos=pos,
                        dx=dx,
                        pid=particle_offset,
                        one=tg.dump(1.0),
                    )
                    s.append(code)

                if debug_mode:
                    with s._first_wi_execution_():
                        s.append('printf("\\n");')
                        s.append('printf("\\nGLOBAL SCALAR VALUES\\n");')
                        with s._for_(f"int ii=0; ii<{compute_grid_size[0]}; ii++"):
                            s.append(
                                f'printf("%2.2f, ", {line_scalars_in[0][0]}[cache_ghosts+ii]);'
                            )
                        s.append('printf("\\n\\n");')

                    with s._ordered_wi_execution_(barrier=True):
                        s.append(
                            'printf("lid.x=%i, k=%i/%i, line_offset=%i, local_offset=%i, poffset=%i, first=%i, last=%i, active=%i, last_active=%i, p={vnf}, s0={vnf}\\n", {},k,kmax,line_offset,local_offset,particle_offset,first,last,active,last_active,{},{});'.format(
                                local_id[0], epv(pos), epv(scalars[0][0]), vnf=vnf
                            )
                        )
                    s.barrier(_local=True)

                    with s._first_wi_execution_():
                        s.append('printf("\\nLEFT SCALAR CACHE (BEFORE REMESH)\\n");')
                        with s._for_("int ii=0; ii<cache_width; ii++"):
                            s.append('printf("%2.2f, ", S0_0[ii]);')
                        s.append('printf("\\n");')

                s.comment("Remesh scalars in cache.")
                with s._block_():
                    remesh = s.reqs["remesh"]
                    remesh_kargs = {
                        "dx": dx,
                        "inv_dx": inv_dx,
                        "cache_width": cache_width,
                        "cache_ghosts": cache_ghosts,
                        "active": active,
                        "line_offset": line_offset,
                    }

                    k = 0
                    for ci in cached_scalars:
                        for cij in ci:
                            remesh_kargs[f"S{k}"] = cij
                            k += 1

                    if use_atomics:
                        # with atomic summation in cache, we can remesh everything at a time
                        if tuning_mode:
                            remesh_kargs["p"] = tuning_pos
                        else:
                            remesh_kargs["p"] = pos
                        k = 0
                        for si in scalars:
                            for sij in si:
                                remesh_kargs[f"s{k}"] = sij
                                k += 1
                        call = remesh(**remesh_kargs)
                        s.append(f"{call};")
                    else:
                        # without atomics we can only remesh on particle at a time
                        for p in range(nparticles):
                            if debug_mode:
                                with s._first_wi_execution_():
                                    s.append(
                                        f'printf("\\nREMESHING PARTICLES {nparticles}*k+{p}:\\n");'
                                    )
                            if tuning_mode:
                                remesh_kargs["p"] = tuning_pos[p]
                            else:
                                remesh_kargs["p"] = pos[p]
                            k = 0
                            for si in scalars:
                                for sij in si:
                                    remesh_kargs[f"s{k}"] = sij[p]
                                    k += 1
                            call = remesh(**remesh_kargs)
                            s.append(f"{call};")
                s.barrier(_local=True, _global=True)
                s.jumpline()

                if debug_mode:
                    with s._first_wi_execution_():
                        s.append('printf("\\nSCALAR CACHE (AFTER REMESH)\\n");')
                        with s._for_("int ii=0; ii<cache_width-2*cache_ghosts; ii++"):
                            s.append('printf("%2.2f, ", S0_0[ii]);')
                        s.append('printf("\\n");')
                        s.append('printf("\\nRIGHT SCALAR CACHE (AFTER REMESH)\\n");')
                        with s._for_(
                            "int ii=cache_width-2*cache_ghosts; ii<cache_width; ii++"
                        ):
                            s.append('printf("%2.2f, ", S0_0[ii]);')
                        s.append('printf("\\n\\n");')

                s.comment("Store back remeshed scalars to global memory")
                for al, active_cond, k in if_first_or_last_thread_active():
                    for si, li in zip(cached_scalars, line_scalars_data_out):
                        for sij, lij in zip(si, li):
                            load = self.vload(1, sij, f"{local_offset}+{k}")
                            _id = f"{particle_offset}+{k}"
                            store = self.vstore(
                                1,
                                lij,
                                _id,
                                load,
                                align=True,
                                jmp=True,
                                suppress_semicolon=True,
                            )
                            if use_short_circuit:
                                code = f"{active_cond} $&& (({store}),true)"
                                if debug_mode:
                                    code += ' && (printf("last_active %i wrote %2.2f from cache position %i to global id %i.\\n", {},{},{},{}));'.format(
                                        local_id[0], load, f"{local_offset}+{k}", _id
                                    )
                                else:
                                    code += ";"
                            else:
                                code = f"if ({active_cond}) ${{ {store}; "
                                if debug_mode:
                                    code += 'printf("last_active %i wrote %2.2f from cache position %i to global id %i.\\n", {},{},{},{});'.format(
                                        local_id[0], load, f"{local_offset}+{k}", _id
                                    )
                                code += "}"
                            al.append(code)
                with if_thread_active(not_first=False):
                    with s._align_() as al:
                        if debug_mode:
                            s.append(
                                'printf("%i wrote {vnf} from cache position %i to global id %i.\\n", {},{},{},{},{},{},{});'.format(
                                    local_id[0],
                                    epv(
                                        self.vload(
                                            nparticles,
                                            cached_scalars[0][0],
                                            local_offset,
                                        )
                                    ),
                                    local_offset,
                                    particle_offset,
                                    compute_grid_size[0],
                                    cache_ghosts,
                                    compute_grid_size[0],
                                    vnf=vnf,
                                )
                            )
                        for ci, li in zip(cached_scalars, line_scalars_data_out):
                            for cij, lij in zip(ci, li):
                                load = self.vload(nparticles, cij, f"{local_offset}")
                                _id = particle_offset
                                store = self.vstore(
                                    nparticles, lij, _id, load, align=True, jmp=True
                                )
                                al.append(store)

                s.jumpline()

                with s._if_(f"{last}"):
                    with s._for_(
                        "int l={}; l<2*{}; l+={}".format(
                            local_id[0], cache_ghosts, local_size[0]
                        ),
                        unroll=unroll_loops,
                    ):
                        with s._align_() as al:
                            _gid = "{}+{}+{}".format(line_offset, last_particle, "l")
                            for ci, li in zip(cached_scalars, line_scalars_data_out):
                                for cij, lij in zip(ci, li):
                                    init = cij["{}+{}".format(last_particle, "l")]
                                    lij.affect(
                                        i=_gid, init=init, align=True, codegen=al
                                    )
                            if debug_mode:
                                with s._ordered_wi_execution_(barrier=False):
                                    s.append(
                                        'printf("%i initiated last write from cache position %i+%i=%i to global id %i+%i+%i=%i with value %2.2f.\\n", {}, {},{},{}, {},{},{},{}, {});'.format(
                                            local_id[0],
                                            last_particle,
                                            "l",
                                            "{}+l".format(last_particle),
                                            line_offset,
                                            last_particle,
                                            "l",
                                            "{}+{}+l".format(
                                                line_offset, last_particle
                                            ),
                                            f"S0_0[{last_particle}+l]",
                                        )
                                    )
                s.barrier(_local=True, _global=True)


if __name__ == "__main__":
    from hysop.backend.device.opencl import cl
    from hysop.backend.device.codegen.base.test import _test_mesh_info, _test_typegen
    from hysop.numerics.remesh.remesh import RemeshKernel

    kernel = RemeshKernel(4, 2, split_polys=False)

    work_dim = 3
    ghosts = (0, 0, 0)
    sresolution = (1024, 512, 256)
    local_size = (128, 1, 1)
    global_size = (16050, 55, 440)

    tg = _test_typegen("float", "hex")
    (_, smesh_info) = _test_mesh_info(
        "scalars_mesh_info", tg, work_dim, ghosts, sresolution
    )
    (_, pmesh_info) = _test_mesh_info(
        "position_mesh_info", tg, work_dim, ghosts, sresolution
    )

    scalar_cfl = 1.5

    dak = DirectionalRemeshKernelGenerator(
        typegen=tg,
        ftype=tg.fbtype,
        work_dim=work_dim,
        nparticles=4,
        nscalars=2,
        remesh_kernel=kernel,
        scalar_cfl=scalar_cfl,
        use_atomics=False,
        is_inplace=True,
        symbolic_mode=False,
        debug_mode=False,
        tuning_mode=False,
        sboundary=(BoundaryCondition.NONE, BoundaryCondition.NONE),
        known_vars=dict(
            S0_inout_mesh_info=smesh_info,
            S1_inout_mesh_info=smesh_info,
            S0_in_mesh_info=smesh_info,
            S1_in_mesh_info=smesh_info,
            S0_out_mesh_info=smesh_info,
            S1_out_mesh_info=smesh_info,
            position_mesh_info=pmesh_info,
            local_size=local_size[:work_dim],
            global_size=global_size[:work_dim],
        ),
    )

    print(f"scalars_out_min_gosts = {dak.scalars_out_cache_ghosts(scalar_cfl, kernel)}")
    print(f"required cache: {dak.required_workgroup_cache_size(local_size)}")

    dak.edit()
    dak.test_compile()
