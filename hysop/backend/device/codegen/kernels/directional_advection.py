# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import contextlib
import math
import operator
import hashlib
from contextlib import contextmanager
import numpy as np

from hysop.constants import DirectionLabels

from hysop import __VERBOSE__, __KERNEL_DEBUG__
from hysop.backend.device.opencl import cl

from hysop.tools.misc import Utils, upper_pow2_or_3
from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.contexts import nested

from hysop.constants import BoundaryCondition, Backend
from hysop.core.arrays.all import OpenClArray

from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.codegen.base.kernel_codegen import KernelCodeGenerator
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
    CodegenArray,
)
from hysop.backend.device.opencl import cl, clTools
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.opencl.opencl_array_backend import OpenClArrayBackend
from hysop.backend.device.codegen.base.utils import WriteOnceDict, ArgDict
from hysop.backend.device.codegen.base.statistics import WorkStatistics

from hysop.backend.device.codegen.base.variables import CodegenStruct
from hysop.backend.device.codegen.structs.mesh_info import (
    MeshBaseStruct,
    MeshInfoStruct,
)
from hysop.backend.device.codegen.structs.indices import GlobalFieldInfos

from hysop.backend.device.codegen.functions.runge_kutta import RungeKuttaFunction
from hysop.backend.device.codegen.functions.advection_rhs import (
    DirectionalAdvectionRhsFunction,
)

from hysop.numerics.odesolvers.runge_kutta import ExplicitRungeKutta

from hysop.backend.device.opencl import cl, clCharacterize
from hysop.backend.device.opencl.opencl_env import OpenClEnvironment
from hysop.backend.device.kernel_autotuner import KernelAutotuner, KernelGenerationError
from hysop.backend.device.kernel_autotuner_config import AutotunerFlags

from hysop.fields.discrete_field import DiscreteScalarFieldView


class DirectionalAdvectionKernelGenerator(KernelCodeGenerator):

    @staticmethod
    def codegen_name(
        ftype,
        is_cached,
        rk_scheme,
        nparticles,
        min_ghosts,
        relative_velocity,
        bilevel,
        **kargs,
    ):
        cache = ""
        if is_cached:
            cache = "cached"
            if bilevel is not None:
                cache += str(bilevel[-1])
            cache += "_"
        return "directional_{}advection_{}_{}{}p_{}g__{}".format(
            cache,
            rk_scheme.name(),
            ftype[0],
            nparticles,
            min_ghosts,
            abs(hash(relative_velocity)),
        )

    def __init__(
        self,
        typegen,
        work_dim,
        ftype,
        is_cached,
        rk_scheme,
        vboundary,
        nparticles,
        relative_velocity,
        offset_by_xmin=False,
        min_ghosts=0,
        use_short_circuit=None,
        unroll_loops=None,
        symbolic_mode=False,
        debug_mode=False,
        tuning_mode=False,
        known_vars=None,
        is_bilevel=None,
    ):

        assert work_dim > 0 and work_dim <= 3
        assert nparticles in [1, 2, 4, 8, 16]
        assert isinstance(relative_velocity, (str, float))
        check_instance(vboundary[0], BoundaryCondition)
        check_instance(vboundary[1], BoundaryCondition)
        check_instance(rk_scheme, ExplicitRungeKutta)

        if (is_bilevel is not None) and (nparticles > 1):
            msg = "Bilevel support with multiple particles at a time has not been implemented yet."
            raise NotImplementedError(msg)

        known_vars = first_not_None(known_vars, {})

        use_short_circuit = first_not_None(
            use_short_circuit, typegen.use_short_circuit_ops
        )

        unroll_loops = first_not_None(unroll_loops, typegen.unroll_loops)

        # unrolling loops take to much time during autotuning
        if tuning_mode:
            unroll_loops = False

        is_periodic = (
            vboundary[0] == BoundaryCondition.PERIODIC
            and vboundary[1] == BoundaryCondition.PERIODIC
        )
        assert (is_periodic and not is_cached) or min_ghosts > 0

        cache_size_known = "local_size" in known_vars or is_bilevel is not None

        _global = OpenClCodeGenerator.default_keywords["global"]
        _local = OpenClCodeGenerator.default_keywords["local"]

        if is_cached:
            storage = _local
        else:
            storage = _global

        self._global, self._local = _global, _local

        itype = "int"

        name = DirectionalAdvectionKernelGenerator.codegen_name(
            ftype,
            is_cached,
            rk_scheme,
            nparticles,
            min_ghosts,
            relative_velocity,
            is_bilevel,
        )

        kernel_reqs = self.build_requirements(
            typegen,
            work_dim,
            itype,
            ftype,
            is_cached,
            rk_scheme,
            vboundary,
            relative_velocity,
            nparticles,
            symbolic_mode,
            storage,
            is_periodic,
            known_vars,
        )

        kernel_args = self.gen_kernel_arguments(
            typegen,
            work_dim,
            itype,
            ftype,
            kernel_reqs,
            is_cached,
            cache_size_known,
            debug_mode,
            known_vars,
            symbolic_mode,
        )

        super().__init__(
            name=name,
            typegen=typegen,
            work_dim=work_dim,
            kernel_args=kernel_args,
            known_vars=known_vars,
            vec_type_hint=ftype,
            symbolic_mode=symbolic_mode,
        )

        self.update_requirements(kernel_reqs)

        self.itype = itype
        self.ftype = ftype
        self.work_dim = work_dim
        self.vboundary = vboundary
        self.nparticles = nparticles
        self.rk_scheme = rk_scheme
        self.storage = storage
        self.cache_size_known = cache_size_known
        self.is_periodic = is_periodic
        self.is_cached = is_cached
        self.is_bilevel = is_bilevel
        self.min_ghosts = min_ghosts
        self.tuning_mode = tuning_mode
        self.offset_by_xmin = offset_by_xmin
        self.relative_velocity = relative_velocity
        self.use_short_circuit = use_short_circuit
        self.unroll_loops = unroll_loops

        self.gencode()

    @classmethod
    def advec_ghosts(cls, velocity_cfl):
        """Return the minimal numbers of ghosts required on the lasr axe of the velocity grid."""
        return int(1 + math.floor(velocity_cfl))

    @classmethod
    def min_ghosts(cls, work_dim, velocity_cfl):
        """Return the minimal numbers of ghosts required on the velocity grid."""
        assert velocity_cfl > 0.0, "cfl <= 0.0"
        ghosts = [0] * work_dim
        ghosts[-1] = cls.advec_ghosts(velocity_cfl)
        return np.asarray(ghosts, dtype=np.int32)

    @classmethod
    def min_wg_size(cls, work_dim, velocity_cfl):
        """Return the minimum workgroup size."""
        ghosts = cls.min_ghosts(work_dim, velocity_cfl)
        return np.asarray(2 * ghosts + 1, dtype=np.int32).copy()

    def required_workgroup_cache_size(self, local_work_size):
        """Return a tuple of required (static,dynamic,total) cache bytes per workgroup."""
        work_dim = self.work_dim
        ftype = self.ftype
        is_cached = self.is_cached
        flt_bytes = self.typegen.FLT_BYTES[ftype]

        local_work_size = np.asarray(local_work_size)

        sc, dc = 0, 0
        if is_cached:
            count = self.nparticles * local_work_size[0] + 2 * self.min_ghosts
            if "local_size" in self.known_vars:
                assert (
                    self.known_vars["local_size"] == local_work_size[:work_dim]
                ).all()
                sc += count
            else:
                dc += count

        sc *= flt_bytes
        dc *= flt_bytes
        tc = sc + dc

        return (sc, dc, tc)

    def required_workgroup_velocity_cache_size(self):
        ftype = self.ftype
        flt_bytes = self.typegen.FLT_BYTES[ftype]
        c = self.is_bilevel[-1] + 2 * self.min_ghosts
        c *= flt_bytes
        return c

    def build_requirements(
        self,
        typegen,
        work_dim,
        itype,
        ftype,
        is_cached,
        rk_scheme,
        vboundary,
        relative_velocity,
        nparticles,
        force_symbolic,
        storage,
        is_periodic,
        known_vars,
    ):
        tg = typegen
        reqs = WriteOnceDict()

        vsize = upper_pow2_or_3(work_dim)

        mesh_base_struct = MeshBaseStruct(typegen=typegen, vsize=vsize)
        reqs["MeshBaseStruct"] = mesh_base_struct

        mesh_info_struct = MeshInfoStruct(typegen=typegen, vsize=vsize)
        reqs["MeshInfoStruct"] = mesh_info_struct

        field_names = ("V", "P")
        global_field_infos = GlobalFieldInfos(
            typegen=typegen, field_names=field_names, workdim=work_dim, vsize=nparticles
        )
        reqs["GlobalFieldInfos"] = global_field_infos

        self.field_names = field_names
        self.field_infos = global_field_infos.build_codegen_variable(name="field_infos")

        advection_rhs = DirectionalAdvectionRhsFunction(
            typegen=typegen,
            work_dim=work_dim,
            ftype=ftype,
            is_cached=is_cached,
            boundary=vboundary[0],
            nparticles=nparticles,
            relative_velocity=relative_velocity,
            ptr_restrict=True,
            itype=itype,
            field_infos=self.field_infos,
        )

        used_vars = RungeKuttaFunction._default_used_vars.copy()
        used_vars["y"] = "X"
        used_vars["step"] = "rk_step"
        runge_kutta = RungeKuttaFunction(
            typegen=tg,
            ftype=ftype,
            method=rk_scheme,
            rhs=advection_rhs,
            used_vars=used_vars,
            known_args=None,
        )
        reqs["runge_kutta"] = runge_kutta

        return reqs

    def gen_kernel_arguments(
        self,
        typegen,
        work_dim,
        itype,
        ftype,
        requirements,
        is_cached,
        cache_size_known,
        debug_mode,
        known_vars,
        symbolic_mode,
    ):

        kargs = ArgDict()
        kargs["dt"] = CodegenVariable(
            ctype=ftype, name="dt", typegen=typegen, add_impl_const=True, nl=True
        )

        mesh_dim = upper_pow2_or_3(work_dim)
        self.velocity, self.velocity_strides = (
            OpenClArrayBackend.build_codegen_arguments(
                kargs,
                name="V",
                known_vars=known_vars,
                symbolic_mode=symbolic_mode,
                storage=self._global,
                ctype=ftype,
                typegen=typegen,
                mesh_dim=mesh_dim,
                ptr_restrict=True,
                const=True,
            )
        )
        self.position, self.position_strides = (
            OpenClArrayBackend.build_codegen_arguments(
                kargs,
                name="P",
                known_vars=known_vars,
                symbolic_mode=symbolic_mode,
                storage=self._global,
                ctype=ftype,
                typegen=typegen,
                mesh_dim=mesh_dim,
                ptr_restrict=True,
                const=False,
            )
        )

        if debug_mode:
            kargs["dbg0"] = CodegenVariable(
                storage=self._global,
                name="dbg0",
                ctype=itype,
                typegen=typegen,
                ptr_restrict=True,
                ptr=True,
                const=False,
                add_impl_const=True,
            )
            kargs["dbg1"] = CodegenVariable(
                storage=self._global,
                name="dbg1",
                ctype=itype,
                typegen=typegen,
                ptr_restrict=True,
                ptr=True,
                const=False,
                add_impl_const=True,
            )

        kargs["V_mesh_info"] = requirements["MeshInfoStruct"].build_codegen_variable(
            const=True, name="V_mesh_info"
        )
        kargs["P_mesh_info"] = requirements["MeshInfoStruct"].build_codegen_variable(
            const=True, name="P_mesh_info"
        )

        if is_cached and not cache_size_known:
            _local = OpenClCodeGenerator.default_keywords["local"]
            kargs["Vc"] = CodegenVariable(
                storage=_local,
                ctype=ftype,
                add_impl_const=True,
                name="Vc",
                ptr=True,
                ptr_restrict=True,
                typegen=typegen,
                nl=False,
            )

        return kargs

    def gencode(self):
        s = self
        tg = s.typegen

        work_dim = s.work_dim
        itype = s.itype
        ftype = s.ftype
        vboundary = s.vboundary
        storage = s.storage
        nparticles = s.nparticles
        min_ghosts = s.min_ghosts
        field_infos = s.field_infos
        tuning_mode = s.tuning_mode

        symbolic_mode = s.symbolic_mode
        use_short_circuit = s.use_short_circuit

        is_periodic = s.is_periodic
        is_cached = s.is_cached
        cache_size_known = s.cache_size_known

        vtype = tg.vtype(ftype, work_dim)
        pvtype = tg.vtype(ftype, nparticles)

        global_id = s.vars["global_id"]
        local_id = s.vars["local_id"]
        group_id = s.vars["group_id"]

        global_index = s.vars["global_index"]
        local_index = s.vars["local_index"]

        global_size = s.vars["global_size"]
        local_size = s.vars["local_size"]

        dt = s.vars["dt"]
        position_mesh_info = s.vars["P_mesh_info"]
        velocity_mesh_info = s.vars["V_mesh_info"]

        velocity, velocity_strides = s.velocity, s.velocity_strides
        position, position_strides = s.position, s.position_strides

        has_bilevel = self.is_bilevel is not None

        if has_bilevel:
            assert is_cached and cache_size_known, (
                "In bilevel, velocity must be cached "
                + str(is_cached)
                + " "
                + str(cache_size_known)
            )

        compute_grid_size = position_mesh_info["local_mesh"]["compute_resolution"].view(
            "p_compute_grid_size", slice(0, work_dim), const=True
        )
        if has_bilevel:
            v_grid_size = velocity_mesh_info["local_mesh"]["resolution"].view(
                "v_grid_size", slice(0, work_dim), const=True
            )

        P_grid_ghosts = position_mesh_info["ghosts"].view(
            "P_grid_ghosts", slice(0, work_dim), const=True
        )
        V_grid_ghosts = velocity_mesh_info["ghosts"].view(
            "V_grid_ghosts", slice(0, work_dim), const=True
        )

        dx = position_mesh_info["dx"].view("p_dx", slice(0, work_dim), const=True)
        inv_dx = position_mesh_info["inv_dx"].view(
            "p_inv_dx", slice(0, work_dim), const=True
        )
        v_dx = velocity_mesh_info["dx"].view("v_dx", slice(0, work_dim), const=True)
        v_inv_dx = velocity_mesh_info["inv_dx"].view(
            "v_inv_dx", slice(0, work_dim), const=True
        )

        xmin = CodegenVariable(
            name="xmin",
            ctype=ftype,
            typegen=tg,
            const=True,
            init="{} + {}*{}".format(
                position_mesh_info["local_mesh"]["xmin"][0], P_grid_ghosts[0], dx[0]
            ),
        )

        position_gid = CodegenVectorClBuiltin("P_gid", itype, work_dim, typegen=tg)
        velocity_gid = CodegenVectorClBuiltin("V_gid", itype, work_dim, typegen=tg)

        if has_bilevel:
            velocity_gid_pos = CodegenVectorClBuiltin(
                "V_pos", ftype, work_dim, typegen=tg
            )
            velocity_h = CodegenVectorClBuiltin("Vh", ftype, work_dim, typegen=tg)
            velocity_ix = CodegenVariable(
                name="V_gid_x", ctype=itype, typegen=tg, const=True
            )
            line_offset_for_v = CodegenVariable(
                name="line_offset_for_v", ctype=itype, typegen=tg, const=False
            )

        runge_kutta = self.reqs["runge_kutta"]

        advec_ghosts = CodegenVariable(
            "V_advec_ghosts", itype, typegen=tg, value=min_ghosts
        )

        line_offset = CodegenVariable(
            name="line_offset", ctype=itype, typegen=tg, const=True
        )
        line_work = CodegenVariable(
            name="line_work", ctype=itype, typegen=tg, const=True
        )
        line_velocity = CodegenVariable(
            name="Vl",
            ctype=ftype,
            ptr=True,
            storage="__global",
            ptr_restrict=True,
            ptr_const=True,
            const=True,
            typegen=tg,
        )
        line_position = CodegenVariable(
            name="Pl",
            ctype=ftype,
            ptr=True,
            storage="__global",
            ptr_restrict=True,
            ptr_const=True,
            const=False,
            typegen=tg,
        )

        X = CodegenVectorClBuiltin("X", ftype, nparticles, typegen=tg)
        pid = CodegenVectorClBuiltin("pid", itype, nparticles, typegen=tg, const=True)
        poffset = CodegenVectorClBuiltin("poffset", itype, nparticles, typegen=tg)

        npart = CodegenVariable(name="npart", ctype=itype, typegen=tg, init=nparticles)
        if is_cached:
            V_cache_width = CodegenVariable(
                "V_cache_width", itype, typegen=tg, const=True
            )
            if cache_size_known:
                if has_bilevel:
                    # if bilevel, velocity cache is the entire local line
                    # WARNING : is_bilevel is HySoP velocity resolution (using transposition state)
                    #           whereas 'local_size' is a OpenCL size # if bilevel, velocity cache is the entire local line
                    if "local_size" in s.known_vars.keys():
                        L = np.minimum(s.is_bilevel[-1], s.known_vars["local_size"][0])
                    else:
                        L = s.is_bilevel[-1]
                    velocity_cache_full_length = L == s.is_bilevel[-1]
                    assert (
                        velocity_cache_full_length
                    ), "With bilevel, only a full length cache for velocity is supported yet"
                else:
                    L = s.known_vars["local_size"][0]
                Vc_shape = (nparticles * L + 2 * min_ghosts,)
                Vc = CodegenArray(
                    name="Vc",
                    dim=1,
                    ctype=ftype,
                    typegen=tg,
                    shape=Vc_shape,
                    storage="__local",
                )
            else:
                Vc = s.vars["Vc"]

        part_ftype = X.ctype
        part_itype = pid.ctype

        is_last = CodegenVariable("is_last", "bool", tg, const=True)
        is_active = CodegenVariable("is_active", "bool", tg, const=True)

        kmax = CodegenVariable(
            "kmax",
            itype,
            tg,
            const=True,
            init="({Sx}+{npart}*{Lx}-1)/({npart}*{Lx})".format(
                Sx=compute_grid_size[0], npart=npart(), Lx=local_size[0]
            ),
        )

        @contextmanager
        def _work_iterate_(i):
            try:
                if i == 0:
                    fval = 0
                    gsize = 1
                    N = kmax
                else:
                    fval = global_id[i]
                    gsize = global_size[i]
                    N = f"{compute_grid_size[i]}"
                position_ghosts = P_grid_ghosts[i]
                velocity_ghosts = V_grid_ghosts[i]

                with s._for_(
                    "int {i}={fval}; {i}<{N}; {i}+={gsize}".format(
                        i="kji"[i], fval=fval, gsize=gsize, N=N
                    ),
                    unroll=(i == 0) and self.unroll_loops,
                ) as ctx:
                    if i == 0:
                        with s._align_() as al:
                            is_last.declare(
                                al,
                                align=True,
                                init="({} == ({}-1))".format("kji"[0], kmax),
                            )
                            is_active.declare(
                                al,
                                align=True,
                                init="({k}*{ls}+{lid} <= ({gs}+{npart}-1)/{npart})".format(
                                    npart=npart,
                                    lid=local_id[0],
                                    k="kji"[0],
                                    gs=compute_grid_size[0],
                                    ls=local_size[0],
                                ),
                            )
                        s.jumpline()

                        with s._align_() as al:
                            line_offset.declare(
                                al,
                                align=True,
                                init="{}*{}*{}".format("kji"[0], local_size[0], npart),
                            )
                            line_work.declare(
                                al,
                                align=True,
                                init="({} ? {}-{} : {}*{})".format(
                                    is_last,
                                    compute_grid_size[0],
                                    line_offset,
                                    local_size[0],
                                    npart,
                                ),
                            )
                            if is_cached and not has_bilevel:
                                al.jumpline()
                                V_cache_width.declare(
                                    al,
                                    align=True,
                                    init="{} + 2*{}".format(line_work, advec_ghosts),
                                )
                        s.jumpline()

                        for k in s.field_names:
                            v = field_infos[k]
                            v0 = f"{v}.idx.IX"
                            v1 = f"{v}.pos.X"
                            mi = s.vars[f"{k}_mesh_info"]
                            s.append(
                                "{} = {} + {} + {};".format(
                                    v0, mi["start"][0], line_offset, poffset
                                )
                            )
                            s.append(
                                "{} = {} + convert_{}({})*{};".format(
                                    v1,
                                    mi["global_mesh"]["xmin"][0],
                                    pvtype,
                                    v0,
                                    mi["dx"][0],
                                )
                            )
                        s.jumpline()

                        position_gid.affect(
                            i=0,
                            codegen=s,
                            init="{} + {}".format(line_offset, position_ghosts),
                        )
                        if not has_bilevel:
                            velocity_gid.affect(
                                i=0,
                                codegen=s,
                                init="{} + {} - {}".format(
                                    line_offset, velocity_ghosts, advec_ghosts
                                ),
                            )

                        line_velocity_offset = " + ".join(
                            f"{velocity_gid[j]}*{velocity_strides[j]}"
                            for j in range(work_dim - 1, -1, -1)
                        )
                        line_position_offset = " + ".join(
                            f"{position_gid[j]}*{position_strides[j]}"
                            for j in range(work_dim - 1, -1, -1)
                        )

                        with s._align_() as al:
                            line_position.declare(
                                al,
                                init=f"{position} + {line_position_offset}",
                                align=True,
                            )
                            if s.is_bilevel is None:
                                line_velocity.declare(
                                    al,
                                    init=f"{velocity} + {line_velocity_offset}",
                                    align=True,
                                )
                    else:
                        for k in s.field_names:
                            v = field_infos[k]
                            d = DirectionLabels[i]
                            v0 = f"{v}.idx.I{d}"
                            v1 = f"{v}.pos.{d}"
                            mi = s.vars[f"{k}_mesh_info"]
                            s.append(
                                "{} = {} + {};".format(v0, mi["start"][i], "kji"[i])
                            )
                            s.append(
                                "{} = {} + {}*{};".format(
                                    v1, mi["global_mesh"]["xmin"][i], v0, mi["dx"][i]
                                )
                            )
                        s.jumpline()

                        s.append(
                            "{} = {} + {};".format(
                                position_gid[i], "kji"[i], position_ghosts
                            )
                        )
                        if has_bilevel:
                            s.append(
                                "{} = ({} * {}) * {};".format(
                                    velocity_gid_pos[i], "kji"[i], dx[i], v_inv_dx[i]
                                )
                            )
                            s.append(
                                f"{velocity_gid[i]} = convert_int_rtn({velocity_gid_pos[i]});"
                            )
                            s.append(
                                "{} = {} - convert_{}({});".format(
                                    velocity_h[i],
                                    velocity_gid_pos[i],
                                    part_ftype,
                                    velocity_gid[i],
                                )
                            )
                            if i == 1:
                                if work_dim == 3:
                                    line_velocity_offset = (
                                        "({}+{})*{}+({}+{})*{}+{}-{}".format(
                                            velocity_gid[2],
                                            V_grid_ghosts[2],
                                            velocity_strides[2],
                                            velocity_gid[1],
                                            V_grid_ghosts[1],
                                            velocity_strides[1],
                                            V_grid_ghosts[0],
                                            advec_ghosts,
                                        )
                                    )
                                elif work_dim == 2:
                                    line_velocity_offset = "({}+{})*{}+{}-{}".format(
                                        velocity_gid[1],
                                        V_grid_ghosts[1],
                                        velocity_strides[1],
                                        V_grid_ghosts[0],
                                        advec_ghosts,
                                    )
                                s.jumpline()
                                with s._align_() as al:
                                    line_velocity.declare(
                                        al,
                                        init=f"{velocity} + {line_velocity_offset}",
                                        align=True,
                                    )
                                    line_offset_for_v.declare(al, init="0")
                                s.jumpline()
                                if velocity_cache_full_length:
                                    s.comment(
                                        "Load velocity in cache with linear interpolation"
                                    )
                                    with s._for_(
                                        "int {i}={fval}; {i}<{N}; {i}+={gsize}".format(
                                            i=velocity_ix,
                                            fval=local_id[0],
                                            gsize=local_size[0],
                                            N=Vc_shape[0],
                                        ),
                                        unroll=False,
                                    ):
                                        is_first = True
                                        if work_dim == 3:
                                            for ii, jj in [
                                                (ii, jj)
                                                for ii in range(2)
                                                for jj in range(2)
                                            ]:
                                                s.append(
                                                    "{}[{}] {} ({}{})*({}{})*{};".format(
                                                        Vc,
                                                        velocity_ix,
                                                        " =" if is_first else "+=",
                                                        "1.0-" if ii == 0 else "    ",
                                                        velocity_h[2],
                                                        "1.0-" if jj == 0 else "    ",
                                                        velocity_h[1],
                                                        s.vload(
                                                            n=nparticles,
                                                            ptr=line_velocity,
                                                            offset="({})*{}+({})*{}+{}*{}".format(
                                                                ii,
                                                                velocity_strides[2],
                                                                jj,
                                                                velocity_strides[1],
                                                                velocity_ix,
                                                                velocity_strides[0],
                                                            ),
                                                        ),
                                                    )
                                                )
                                                is_first = False
                                        elif work_dim == 2:
                                            for ii in range(2):
                                                s.append(
                                                    "{}[{}] {} ({}{})*{};".format(
                                                        Vc,
                                                        velocity_ix,
                                                        " =" if is_first else "+=",
                                                        "1.0-" if ii == 0 else "    ",
                                                        velocity_h[1],
                                                        s.vload(
                                                            n=nparticles,
                                                            ptr=line_velocity,
                                                            offset="({})*{}+{}*{}".format(
                                                                ii,
                                                                velocity_strides[1],
                                                                velocity_ix,
                                                                velocity_strides[0],
                                                            ),
                                                        ),
                                                    )
                                                )
                                                is_first = False
                                        else:
                                            raise RuntimeError(
                                                "Bilevel advection 1D need further developments"
                                            )
                                    s.barrier(_local=True)
                                    s.jumpline()
                        else:
                            s.append(
                                "{} = {} + {};".format(
                                    velocity_gid[i], "kji"[i], velocity_ghosts
                                )
                            )
                    yield ctx
            except:
                raise

        nested_loops = [_work_iterate_(i) for i in range(work_dim - 1, -1, -1)]

        with s._kernel_():
            field_infos.declare(s)
            s.jumpline()
            with s._align_() as al:
                global_id.declare(al, align=True, const=True)
                local_id.declare(al, align=True, const=True)
                global_size.declare(al, align=True, const=True)
                local_size.declare(al, align=True, const=True)
            s.jumpline()

            with s._align_() as al:
                compute_grid_size.declare(al, align=True)
                if has_bilevel:
                    v_grid_size.declare(al, align=True)
                P_grid_ghosts.declare(al, align=True)
                V_grid_ghosts.declare(al, align=True)
                al.jumpline()
                dx.declare(al, align=True)
                inv_dx.declare(al, align=True)
                if has_bilevel:
                    v_dx.declare(al, align=True)
                    v_inv_dx.declare(al, align=True)
                    s.jumpline()
                xmin.declare(al, align=True)
            s.jumpline()

            with s._align_() as al:
                npart.declare(al, const=True, align=True)
                poffset.declare(
                    al, init=tuple(range(nparticles)), const=True, align=True
                )
            s.jumpline()

            if is_cached:
                with s._align_() as al:
                    advec_ghosts.declare(al, const=True, align=True)
                s.jumpline()

            with s._align_() as al:
                velocity.declare(al, align=True)
                position.declare(al, align=True)
            s.jumpline()

            if is_cached and cache_size_known:
                Vc.declare(s)
                s.jumpline()

            kmax.declare(s)
            s.decl_aligned_vars(position_gid, velocity_gid)
            if has_bilevel:
                s.decl_aligned_vars(velocity_gid_pos, velocity_h)

            with nested(*nested_loops):
                s.jumpline()

                if is_cached and not has_bilevel:
                    if tuning_mode:
                        loop = "int {i}={Lx}; {i}<{N}; {i}+={gsize}".format(
                            i="idx",
                            N=V_cache_width,
                            Lx=local_id[0],
                            gsize=local_size[0],
                        )
                        with s._for_(loop):
                            code = "{dst}[{i}] = 0.5;".format(i="idx", dst=Vc)
                            s.append(code)
                        s.barrier(_local=True)
                    else:
                        code = "event_t event = async_work_group_copy({dst}, {src}, {ne}, {event});".format(
                            dst=Vc, src=line_velocity, ne=V_cache_width, event=0
                        )
                        s.append(code)
                        code = "wait_group_events(1, &event);"
                        s.append(code)
                    s.jumpline()
                elif has_bilevel and not velocity_cache_full_length:
                    # We must load velocity cache line on the fly
                    s.comment("Load velocity in cache with linear interpolation")
                    mesh_ratio = 1
                    s_mesh_size = position_mesh_info["global_mesh"][
                        "compute_resolution"
                    ].value[0]
                    v_mesh_size = velocity_mesh_info["global_mesh"][
                        "compute_resolution"
                    ].value[0]
                    if s_mesh_size % v_mesh_size == 0:
                        mesh_ratio = s_mesh_size // v_mesh_size
                    with s._if_(f"k%{mesh_ratio}==0"):
                        s.append(
                            f"{line_offset_for_v} = convert_int_rtn(convert_{ftype}({line_offset}+1)*{dx[0]}*{v_inv_dx[0]});"
                        )
                        with s._for_(
                            "int {i}={fval}; {i}<{N} && ({i}+{o})<{Nv}; {i}+={gsize}".format(
                                i=velocity_ix,
                                fval=local_id[0],
                                gsize=local_size[0],
                                N=Vc_shape[0],
                                o=line_offset_for_v,
                                Nv=v_grid_size[0],
                            ),
                            unroll=False,
                        ):
                            if work_dim == 3:
                                is_first = True
                                for ii, jj in [
                                    (ii, jj) for ii in range(2) for jj in range(2)
                                ]:
                                    s.append(
                                        "{}[{}] {} ({}{})*({}{})*{};".format(
                                            Vc,
                                            velocity_ix,
                                            " =" if is_first else "+=",
                                            "1.0-" if ii == 0 else "    ",
                                            velocity_h[2],
                                            "1.0-" if jj == 0 else "    ",
                                            velocity_h[1],
                                            s.vload(
                                                n=nparticles,
                                                ptr=line_velocity,
                                                offset="({})*{}+({})*{}+({}+{})*{}".format(
                                                    ii,
                                                    velocity_strides[2],
                                                    jj,
                                                    velocity_strides[1],
                                                    line_offset_for_v,
                                                    velocity_ix,
                                                    velocity_strides[0],
                                                ),
                                            ),
                                        )
                                    )
                                    is_first = False
                            elif work_dim == 2:
                                for ii in range(2):
                                    s.append(
                                        "{}[{}] {} ({}{})*{};".format(
                                            Vc,
                                            velocity_ix,
                                            " =" if is_first else "+=",
                                            "1.0-" if jj == 0 else "    ",
                                            velocity_h[1],
                                            s.vload(
                                                n=nparticles,
                                                ptr=line_velocity,
                                                offset="({})*{}+({}+{})*{}".format(
                                                    jj,
                                                    velocity_strides[1],
                                                    line_offset_for_v,
                                                    velocity_ix,
                                                    velocity_strides[0],
                                                ),
                                            ),
                                        )
                                    )
                                    is_first = False
                        s.barrier(_local=True)
                    s.jumpline()

                with s._if_(is_active()):
                    pid.declare(
                        s,
                        init="{} + {}*{} + {}".format(
                            line_offset, local_id[0], npart, poffset
                        ),
                    )
                    X.declare(
                        s,
                        init="convert_{}(min({},{}-1))*{}".format(
                            part_ftype, pid, compute_grid_size[0], dx[0]
                        ),
                    )
                    s.jumpline()

                    # Modify for bilevel interpolation : in this case, the line
                    # offset is set to 0 because cache length equals local resolution
                    if has_bilevel:
                        rk_args = {
                            "X": X,
                            "dt": dt,
                            "inv_dx": v_inv_dx[0],
                            "line_offset": (
                                "0" if velocity_cache_full_length else line_offset_for_v
                            ),
                            "field_infos": f"&{field_infos}",
                        }
                    else:
                        rk_args = {
                            "X": X,
                            "dt": dt,
                            "inv_dx": inv_dx[0],
                            "line_offset": line_offset,
                            "field_infos": f"&{field_infos}",
                        }
                    if is_cached:
                        rk_args["line_velocity"] = Vc
                    else:
                        rk_args["line_velocity"] = line_velocity
                    rk_args["line_velocity"] = "{}+{}".format(
                        rk_args["line_velocity"], min_ghosts
                    )
                    if is_periodic:
                        rk_args["line_width"] = compute_grid_size[0]
                    call = runge_kutta(**rk_args)
                    code = f"{X()}  = {call};"
                    s.append(code)

                    if s.offset_by_xmin:
                        s.append(f"{X()} += {xmin};")
                    s.jumpline()

                    s.vstore_if(
                        cond=is_last,
                        scalar_cond=lambda i: f"{pid[i]} < {compute_grid_size[0]}",
                        n=nparticles,
                        ptr=line_position,
                        offset=local_id[0],
                        data=X,
                        offset_is_ftype=False,
                        use_short_circuit=use_short_circuit,
                    )


if __name__ == "__main__":
    from hysop.backend.device.opencl import cl
    from hysop.backend.device.codegen.base.test import _test_mesh_info, _test_typegen

    work_dim = 3
    ghosts = (0, 0, 0)
    vresolution = (128, 64, 32)
    presolution = (128, 64, 32)
    local_size = (1024, 1, 1)
    global_size = (16050, 55, 440)

    tg = _test_typegen("float")
    (_, vmesh_info) = _test_mesh_info(
        "velocity_mesh_info", tg, work_dim, ghosts, vresolution
    )
    (_, pmesh_info) = _test_mesh_info(
        "position_mesh_info", tg, work_dim, ghosts, presolution
    )

    dak = DirectionalAdvectionKernelGenerator(
        typegen=tg,
        ftype=tg.fbtype,
        work_dim=work_dim,
        rk_scheme=ExplicitRungeKutta("Euler"),
        vboundary=(BoundaryCondition.PERIODIC, BoundaryCondition.PERIODIC),
        is_cached=True,
        min_ghosts=10,
        symbolic_mode=True,
        relative_velocity=0.66,
        nparticles=4,  # MONOLEVEL TEST
        tuning_mode=True,
        # nparticles=1, is_bilevel=(256,64,32), # BILEVEL TEST
        known_vars=dict(
            V_mesh_info=vmesh_info,
            P_mesh_info=pmesh_info,
            local_size=local_size[:work_dim],
            global_size=global_size[:work_dim],
        ),
    )
    dak.edit()
    dak.test_compile()
