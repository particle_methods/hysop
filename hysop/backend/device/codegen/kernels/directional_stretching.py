# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import contextlib
import hashlib
import operator

import numpy as np
from hysop import __KERNEL_DEBUG__, __VERBOSE__
from hysop.backend.device.codegen.base.kernel_codegen import KernelCodeGenerator
from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.codegen.base.statistics import WorkStatistics
from hysop.backend.device.codegen.base.utils import ArgDict, WriteOnceDict
from hysop.backend.device.codegen.base.variables import (
    CodegenArray,
    CodegenStruct,
    CodegenVariable,
    CodegenVectorClBuiltin,
)
from hysop.backend.device.codegen.functions.cache_load import CacheLoadFunction
from hysop.backend.device.codegen.functions.compute_index import ComputeIndexFunction
from hysop.backend.device.codegen.functions.runge_kutta import RungeKuttaFunction
from hysop.backend.device.codegen.functions.stretching_rhs import (
    DirectionalStretchingRhsFunction,
)
from hysop.backend.device.codegen.structs.mesh_info import (
    MeshBaseStruct,
    MeshInfoStruct,
)
from hysop.backend.device.kernel_autotuner import KernelAutotuner, KernelGenerationError
from hysop.backend.device.opencl import cl, clCharacterize, clTools
from hysop.backend.device.opencl.opencl_env import OpenClEnvironment
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.constants import (
    AutotunerFlags,
    Backend,
    BoundaryCondition,
    DirectionLabels,
    SpaceDiscretization,
)
from hysop.fields.discrete_field import DiscreteScalarFieldView
from hysop.methods import StretchingFormulation
from hysop.numerics.odesolvers.runge_kutta import ExplicitRungeKutta
from hysop.tools.contexts import nested
from hysop.tools.misc import Utils, upper_pow2_or_3
from hysop.tools.htypes import check_instance


class DirectionalStretchingKernel(KernelCodeGenerator):

    @staticmethod
    def codegen_name(ftype, is_cached, is_inplace, direction, formulation):
        inplace = "inplace_" if is_inplace else ""
        cache = "cached_" if is_cached else ""
        sformulation = str(formulation).lower()
        return "directional_{}{}stretching_{}_{}{}".format(
            cache, inplace, sformulation, ftype[0], DirectionLabels[direction]
        )

    def __init__(
        self,
        typegen,
        dim,
        ftype,
        order,
        direction,
        is_cached,
        is_inplace,
        boundary,
        formulation,
        time_integrator,
        symbolic_mode=False,
        known_vars=None,
    ):

        check_instance(formulation, StretchingFormulation)
        check_instance(boundary[0], BoundaryCondition)
        check_instance(boundary[1], BoundaryCondition)
        check_instance(time_integrator, ExplicitRungeKutta)
        check_instance(is_inplace, bool)

        assert dim == 3
        assert direction < dim
        assert order > 1 and order % 2 == 0
        assert boundary[0] in [BoundaryCondition.NONE, BoundaryCondition.PERIODIC]
        assert boundary[1] in [BoundaryCondition.NONE, BoundaryCondition.PERIODIC]

        if known_vars is None:
            known_vars = {}

        local_size_known = "local_size" in known_vars
        is_conservative = formulation == StretchingFormulation.CONSERVATIVE
        is_periodic = (boundary[0] == BoundaryCondition.PERIODIC) and (
            boundary[1] == BoundaryCondition.PERIODIC
        )

        if is_cached:
            storage = OpenClCodeGenerator.default_keywords["local"]
        else:
            storage = OpenClCodeGenerator.default_keywords["global"]

        if is_inplace and is_conservative and not is_cached:
            raise ValueError("Inplace conservetive stretching requires caching.")

        name = DirectionalStretchingKernel.codegen_name(
            ftype, is_cached, is_inplace, direction, formulation
        )

        kernel_reqs = self.build_requirements(
            typegen,
            dim,
            ftype,
            order,
            is_cached,
            time_integrator,
            direction,
            boundary,
            symbolic_mode,
            formulation,
            storage,
            is_periodic,
            is_inplace,
        )

        kernel_args = self.gen_kernel_arguments(
            typegen, dim, ftype, kernel_reqs, is_cached, is_inplace, local_size_known
        )

        super().__init__(
            name=name,
            typegen=typegen,
            work_dim=dim,
            kernel_args=kernel_args,
            known_vars=known_vars,
            vec_type_hint=ftype,
            symbolic_mode=symbolic_mode,
        )

        self.update_requirements(kernel_reqs)

        self.order = order
        self.ftype = ftype
        self.direction = direction
        self.dim = dim
        self.boundary = boundary
        self.time_integrator = time_integrator
        self.formulation = formulation
        self.storage = storage
        self.local_size_known = local_size_known

        self.is_conservative = is_conservative
        self.is_periodic = is_periodic
        self.is_cached = is_cached
        self.is_inplace = is_inplace

        self.gencode()

    # return minimal number of ghosts required on the grid
    # for input velocity and vorticity.
    @staticmethod
    def min_ghosts(boundary, formulation, order, time_integrator, direction):
        (lboundary, rboundary) = boundary
        u_ghosts = [0] * 3
        if (lboundary == BoundaryCondition.PERIODIC) and (
            rboundary == BoundaryCondition.PERIODIC
        ):
            pass
        elif lboundary in [BoundaryCondition.NONE, BoundaryCondition.PERIODIC]:
            assert order % 2 == 0
            stencil_ghost = order // 2
            if formulation == StretchingFormulation.CONSERVATIVE:
                u_ghosts[direction] = time_integrator.stages * stencil_ghost
            else:
                u_ghosts[direction] = stencil_ghost
        else:
            raise ValueError(f"Unknown lboundary {lboundary}.")
        if formulation == StretchingFormulation.CONSERVATIVE:
            # div(u x w)
            w_ghosts = u_ghosts
        else:
            # (a*grad(u) + (1-a)*grad(u)^T) w
            w_ghosts = [0] * 3
        return np.asarray(u_ghosts), np.asarray(w_ghosts)

    @staticmethod
    def min_wg_size(formulation, order, time_integrator):
        ghosts = [1] * 3
        stencil_ghost = order // 2
        if formulation == StretchingFormulation.CONSERVATIVE:
            ghosts[0] = time_integrator.stages * stencil_ghost
        else:
            ghosts[0] = stencil_ghost
        ghosts[0] *= 4
        return np.asarray(ghosts)

    # return global_work_size from effective work_size without
    # taking into account local_work_size alignment
    @staticmethod
    def get_max_global_size(work_size, work_load, **kargs):
        work_size = np.asarray(work_size)
        work_load = np.asarray(work_load)
        assert work_load[0] == 1

        global_size = work_size.copy()
        global_size = (global_size + work_load - 1) // work_load
        return global_size

    # return global_work_size from effective work_size and given local_work_size
    # global_work_size will be a multiple of local_work_size
    def get_global_size(self, work_size, local_work_size, work_load=[1, 1, 1]):
        work_size = np.asarray(work_size)
        work_load = np.asarray(work_load)
        local_work_size = np.asarray(local_work_size)
        v_min_ghosts, w_min_ghosts = self._min_ghosts()

        assert (local_work_size[1] == 1) and (local_work_size[2] == 1)
        assert (local_work_size > 2 * w_min_ghosts).all()

        if "local_size" in self.known_vars:
            assert (self.known_vars["local_size"] == local_work_size).all()
        if "velocity_mesh_info" in self.known_vars and (not self.is_periodic):
            velocity_mesh_info = self.known_vars["velocity_mesh_info"].value
            vghosts = velocity_mesh_info["ghosts"][:3]
            assert (vghosts >= v_min_ghosts).all()
        if "vorticity_mesh_info" in self.known_vars and (not self.is_periodic):
            vorticity_mesh_info = self.known_vars["vorticity_mesh_info"].value
            wghosts = vorticity_mesh_info["ghosts"][:3]
            assert (wghosts >= w_min_ghosts).all()

        max_global_size = self.get_max_global_size(work_size, work_load)
        max_global_size[0] = local_work_size[0]
        global_size = (
            (max_global_size + local_work_size - 1) // local_work_size
        ) * local_work_size

        return global_size

    # return a tuple of required (static,dynamic,total) cache bytes per workgroup
    def required_workgroup_cache_size(self, local_work_size):
        dim = self.work_dim
        ftype = self.ftype
        is_cached = self.is_cached
        direction = self.direction
        cache_ghosts = self._cache_ghosts()
        is_periodic = self.is_periodic
        is_conservative = self.is_conservative
        flt_bytes = self.typegen.FLT_BYTES[ftype]

        local_work_size = np.asarray(local_work_size)

        sc, dc = 0, 0
        if is_cached:
            count = dim * local_work_size[0]
            if is_conservative:
                count += local_work_size[0]

            if "local_size" in self.known_vars:
                assert (self.known_vars["local_size"] == local_work_size).all()
                sc += count
            else:
                dc += count

        sc += 2 * dim * (2 * cache_ghosts)
        if self.is_periodic:
            sc += 2 * dim * (2 * cache_ghosts)

        sc *= flt_bytes
        dc *= flt_bytes
        tc = sc + dc

        return (sc, dc, tc)

    def _cache_ghosts(self):
        stencil_ghost = self.order // 2
        if self.is_conservative:
            return self.time_integrator.stages * stencil_ghost
        else:
            return stencil_ghost

    def _min_ghosts(self):
        return self.min_ghosts(
            self.boundary,
            self.formulation,
            self.order,
            self.time_integrator,
            self.direction,
        )

    def build_requirements(
        self,
        typegen,
        work_dim,
        ftype,
        order,
        is_cached,
        time_integrator,
        direction,
        boundary,
        force_symbolic,
        formulation,
        storage,
        is_periodic,
        is_inplace,
    ):
        tg = typegen
        reqs = WriteOnceDict()

        compute_id = ComputeIndexFunction(
            typegen=typegen, dim=work_dim, itype="int", wrap=is_periodic
        )
        reqs["compute_id"] = compute_id

        vsize = upper_pow2_or_3(work_dim)
        mesh_base_struct = MeshBaseStruct(typegen=typegen, vsize=vsize)
        reqs["MeshBaseStruct"] = mesh_base_struct

        mesh_info_struct = MeshInfoStruct(typegen=typegen, vsize=vsize)
        reqs["MeshInfoStruct"] = mesh_info_struct

        stretching_rhs = DirectionalStretchingRhsFunction(
            typegen=typegen,
            dim=work_dim,
            ftype=ftype,
            cached=is_cached,
            order=order,
            direction=direction,
            boundary=boundary,
            formulation=formulation,
            ptr_restrict=True,
            vectorize_u=False,
            itype="int",
        )

        used_vars = RungeKuttaFunction._default_used_vars.copy()
        used_vars["y"] = "W"
        used_vars["step"] = "rk_step"
        runge_kutta = RungeKuttaFunction(
            typegen=tg,
            ftype=ftype,
            method=time_integrator,
            rhs=stretching_rhs,
            used_vars=used_vars,
            known_args=None,
        )
        reqs["runge_kutta"] = runge_kutta

        return reqs

    def gen_kernel_arguments(
        self,
        typegen,
        work_dim,
        ftype,
        requirements,
        is_cached,
        is_inplace,
        local_size_known,
    ):

        xyz = "xyz"
        svelocity = "U"
        svorticity = "W"

        kargs = ArgDict()
        kargs["dt"] = CodegenVariable(
            ctype=ftype, name="dt", typegen=typegen, add_impl_const=True, nl=True
        )

        _global = OpenClCodeGenerator.default_keywords["global"]
        _local = OpenClCodeGenerator.default_keywords["local"]
        for i in range(work_dim):
            name = svelocity + xyz[i]
            kargs[name] = CodegenVariable(
                storage=_global,
                name=name,
                typegen=typegen,
                ctype=ftype,
                ptr=True,
                ptr_restrict=True,
                const=True,
                add_impl_const=True,
            )

        for i in range(work_dim):
            name = svorticity + xyz[i] + "_in"
            kargs[name] = CodegenVariable(
                storage=_global,
                name=name,
                typegen=typegen,
                ctype=ftype,
                ptr=True,
                ptr_restrict=(not is_inplace),
                const=True,
                add_impl_const=True,
            )

        for i in range(work_dim):
            name = svorticity + xyz[i] + "_out"
            kargs[name] = CodegenVariable(
                storage=_global,
                name=name,
                typegen=typegen,
                ctype=ftype,
                ptr=True,
                ptr_restrict=(not is_inplace),
                const=False,
                add_impl_const=True,
            )

        kargs["velocity_mesh_info"] = requirements[
            "MeshInfoStruct"
        ].build_codegen_variable(const=True, name="velocity_mesh_info")
        kargs["vorticity_mesh_info"] = requirements[
            "MeshInfoStruct"
        ].build_codegen_variable(const=True, name="vorticity_mesh_info")

        if is_cached and not local_size_known:
            kargs["buffer"] = CodegenVariable(
                storage=_local,
                ctype=ftype,
                add_impl_const=True,
                name="buffer",
                ptr=True,
                ptr_restrict=True,
                typegen=typegen,
                nl=False,
            )

        self.svorticity = svorticity
        self.svelocity = svelocity
        self.xyz = xyz

        return kargs

    def gencode(self):
        s = self
        tg = s.typegen

        direction = s.direction
        work_dim = s.work_dim
        dim = s.dim
        ftype = s.ftype
        boundary = s.boundary
        is_cached = s.is_cached
        storage = s.storage

        symbolic_mode = s.symbolic_mode

        formulation = s.formulation
        is_conservative = s.is_conservative
        is_periodic = s.is_periodic
        local_size_known = s.local_size_known

        xyz = s.xyz
        vtype = tg.vtype(ftype, work_dim)

        global_id = s.vars["global_id"]
        local_id = s.vars["local_id"]
        group_id = s.vars["group_id"]

        global_index = s.vars["global_index"]
        local_index = s.vars["local_index"]

        global_size = s.vars["global_size"]
        local_size = s.vars["local_size"]

        dt = s.vars["dt"]
        velocity_mesh_info = s.vars["velocity_mesh_info"]
        vorticity_mesh_info = s.vars["vorticity_mesh_info"]

        grid_size = vorticity_mesh_info["local_mesh"]["resolution"].view(
            "grid_size", slice(None, dim)
        )
        compute_grid_size = vorticity_mesh_info["local_mesh"][
            "compute_resolution"
        ].view("compute_grid_size", slice(None, dim))
        compute_grid_ghosts = vorticity_mesh_info["ghosts"].view(
            "compute_grid_ghosts", slice(0, dim), const=True
        )
        inv_dx = vorticity_mesh_info["inv_dx"].view("inv_dx", slice(0, 1), const=True)
        s.update_vars(
            grid_size=grid_size,
            inv_dx=inv_dx,
            compute_grid_ghosts=compute_grid_ghosts,
            compute_grid_size=compute_grid_size,
        )

        compute_index = self.reqs["compute_id"]
        runge_kutta = self.reqs["runge_kutta"]

        W = CodegenVectorClBuiltin("W", ftype, dim, tg)
        U = CodegenVectorClBuiltin("U", ftype, dim, tg)

        first = CodegenVariable("first", "bool", tg, init="true")
        active = CodegenVariable("active", "bool", tg, const=True)

        cache_ghosts = CodegenVariable(
            "cache_ghosts", "int", tg, const=True, value=self._cache_ghosts()
        )
        local_work = CodegenVariable("lwork", "int", tg, const=True)

        cached_vars = ArgDict()
        if is_cached:
            for i in range(work_dim):
                Vi = self.svelocity + self.xyz[i]
                if local_size_known:
                    Vic = CodegenArray(
                        name=Vi + "c",
                        dim=1,
                        ctype=ftype,
                        typegen=tg,
                        shape=(local_size.value[0],),
                        storage=storage,
                    )
                else:
                    buf = s.vars["buffer"]
                    init = f"{buf()} + {i}*{local_size[0]}"
                    Vic = CodegenVariable(
                        storage=storage,
                        name=Vi + "c",
                        ctype=ftype,
                        typegen=tg,
                        const=False,
                        ptr_restrict=True,
                        ptr=True,
                        init=init,
                    )
                cached_vars[Vi] = Vic

            if is_conservative:
                Wi = self.svorticity + self.xyz[direction]
                if local_size_known:
                    Wic = CodegenArray(
                        storage=storage,
                        name=Wi + "c",
                        dim=1,
                        ctype=ftype,
                        typegen=tg,
                        shape=(local_size.value[0],),
                    )
                else:
                    buf = s.vars["buffer"]
                    init = f"{buf()} + {work_dim}*{local_size[0]}"
                    Wic = CodegenVariable(
                        storage=storage,
                        name=Wi + "c",
                        ctype=ftype,
                        typegen=tg,
                        const=False,
                        ptr_restrict=True,
                        ptr=True,
                        init=init,
                    )
                cached_vars[Wi] = Wic

        _U = self.svelocity
        size = cache_ghosts.value
        Ur = CodegenArray(
            storage="__local",
            name=_U + "r",
            dim=1,
            ctype=vtype,
            typegen=tg,
            shape=(2 * size,),
        )
        _W = self.svorticity
        size = cache_ghosts.value
        Wr = CodegenArray(
            storage="__local",
            name=_W + "r",
            dim=1,
            ctype=vtype,
            typegen=tg,
            shape=(2 * size,),
        )
        if is_periodic:
            Ul = CodegenArray(
                storage="__local",
                name=_U + "l",
                dim=1,
                ctype=vtype,
                typegen=tg,
                shape=(2 * size,),
            )
            Wl = CodegenArray(
                storage="__local",
                name=_W + "l",
                dim=1,
                ctype=vtype,
                typegen=tg,
                shape=(2 * size,),
            )

        @contextlib.contextmanager
        def _work_iterate_(i):
            try:
                if i == 0:
                    fval = "0"
                    gsize = local_work()
                    N = "(({}+2*{}+{lwork}-1)/{lwork})*{lwork}".format(
                        compute_grid_size[i], cache_ghosts(), lwork=local_work()
                    )
                    ghosts = f"({compute_grid_ghosts[i]}-{cache_ghosts()})"
                else:
                    fval = global_id.fval(i)
                    gsize = global_size[i]
                    N = f"{compute_grid_size[i]}"
                    ghosts = compute_grid_ghosts[i]

                s.append("#pragma unroll 4")
                with s._for_(
                    "int {i}={fval}; {i}<{N}; {i}+={gsize}".format(
                        i="kji"[i], fval=fval, gsize=gsize, N=N
                    )
                ) as ctx:

                    if i == 0:
                        s.append(
                            "{} = {}+{};".format(global_id[i], "kji"[i], local_id[0])
                        )
                    else:
                        s.append("{} = {}+{};".format(global_id[i], "kji"[i], ghosts))

                    if i == 0:
                        active.declare(
                            s,
                            init="({} < {}+2*{})".format(
                                global_id[0], compute_grid_size[0], cache_ghosts()
                            ),
                        )
                        s.append(f"{global_id[i]} += {ghosts};")
                    elif i == 1:
                        first.declare(s)

                    yield ctx
            except:
                raise

        nested_loops = [_work_iterate_(i) for i in range(dim - 1, -1, -1)]

        @contextlib.contextmanager
        def if_thread_active():
            with s._if_(f"{active()}"):
                yield

        with s._kernel_():
            s.jumpline()
            with s._align_() as al:
                local_id.declare(al, align=True, const=True)
                global_size.declare(al, align=True, const=True)
                local_size.declare(al, align=True, const=True)
            s.jumpline()

            with s._align_() as al:
                compute_grid_size.declare(al, const=True, align=True)
                compute_grid_ghosts.declare(al, align=True)
                grid_size.declare(al, align=True, const=True)
                inv_dx.declare(al, align=True)
            s.jumpline()

            with s._align_() as al:
                cache_ghosts.declare(al, align=True)
                local_work.declare(
                    al, align=True, init=f"{local_size[0]} - 2*{cache_ghosts()}"
                )
            s.jumpline()

            if is_cached:
                with s._align_() as al:
                    for varname, var in cached_vars.items():
                        var.declare(al, align=True)
                s.jumpline()

            Ur.declare(s)
            Wr.declare(s)
            s.jumpline()

            if is_periodic:
                Ul.declare(s)
                Wl.declare(s)
                s.jumpline()

            s.jumpline()

            global_id.declare(s, init=False)
            s.jumpline()

            with nested(*nested_loops):
                s.jumpline()
                init = compute_index(idx=global_id, size=grid_size)
                global_index.declare(s, init=init, const=True)

                winit, uinit = "", ""
                for i in range(work_dim):
                    Wi_in = self.svorticity + self.xyz[i] + "_in"
                    Wi_out = self.svorticity + self.xyz[i] + "_out"
                    Ui = self.svelocity + self.xyz[i]
                    uinit += self.args[Ui][global_index()] + ","
                    winit += self.args[Wi_in][global_index()] + ","
                uinit = f"({ftype}{work_dim})({uinit[:-1]})"
                winit = f"({ftype}{work_dim})({winit[:-1]})"

                s.jumpline()
                s.append(f"{U.ctype} {U()},{W()};")
                with if_thread_active():
                    with s._if_(f"{first()}"):
                        s.append(f"{U()} = {uinit};")
                        s.append(f"{W()} = {winit};")
                        s.append(f"{first()} = false;")
                        if is_periodic:
                            with s._if_(
                                "{lid} < 2*{ghosts}".format(
                                    lid=local_id[0], ghosts=cache_ghosts()
                                )
                            ):
                                s.append(f"{Ul[local_id[0]]} = {U()};")
                                s.append(f"{Wl[local_id[0]]} = {W()};")
                    with s._else_():
                        if is_periodic:
                            with s._if_(
                                "{} >= {}-{}".format(
                                    global_id[0], compute_grid_size[0], cache_ghosts()
                                )
                            ):
                                _id = "{}-{}+{}".format(
                                    global_id[0], compute_grid_size[0], cache_ghosts()
                                )
                                s.append(f"{U()} = {Ul[_id]};")
                                s.append(f"{W()} = {Wl[_id]};")
                            with s._elif_(f"{local_id[0]} < 2*{cache_ghosts()}"):
                                s.append(f"{U()} = {Ur[local_id[0]]};")
                                s.append(f"{W()} = {Wr[local_id[0]]};")
                        else:
                            with s._if_(f"{local_id[0]} < 2*{cache_ghosts()}"):
                                s.append(f"{U()} = {Ur[local_id[0]]};")
                                s.append(f"{W()} = {Wr[local_id[0]]};")
                        with s._else_():
                            s.append(f"{U()} = {uinit};")
                            s.append(f"{W()} = {winit};")

                s.barrier(_local=True)
                s.jumpline()

                with if_thread_active():
                    if self.is_cached:
                        for i in range(work_dim):
                            Ui = self.svelocity + self.xyz[i]
                            Uic = cached_vars[Ui]
                            code = f"{Uic[local_id[0]]} = {U[i]};"
                            s.append(code)
                        s.jumpline()

                    with s._if_(
                        "{} >= {}-2*{}".format(
                            local_id[0], local_size[0], cache_ghosts()
                        )
                    ):
                        _id = f"{local_id[0]}-{local_size[0]}+2*{cache_ghosts()}"
                        s.append(f"{Ur[_id]} = {U()};")
                        s.append(f"{Wr[_id]} = {W()};")

                s.barrier(_local=True)
                s.jumpline()

                rk_args = {
                    "dt": dt,
                    "inv_dx": inv_dx,
                    "W": W,
                    "active": active,
                    "Lx": local_size[0],
                    "lidx": local_id[0],
                }

                if is_periodic and (not is_cached):
                    base = CodegenVariable("base", "int", typegen=tg, const=True)
                    base.declare(
                        s,
                        init="({}/{}) * {}".format(
                            global_index(), grid_size[0], grid_size[0]
                        ),
                    )
                    offset = CodegenVariable("offset", "int", typegen=tg, const=True)
                    offset.declare(s, init=f"{global_index()}-{base()}")
                    rk_args["base"] = base
                    rk_args["offset"] = offset
                    rk_args["width"] = grid_size[0]
                else:
                    rk_args["offset"] = local_id[0] if is_cached else global_index

                for i in range(work_dim):
                    Ui_name = self.svelocity + xyz[i]
                    if is_cached:
                        Ui = cached_vars[Ui_name]
                    else:
                        Ui = s.vars[Ui_name]
                    rk_args[Ui_name] = Ui

                if is_conservative:
                    Wd_name = self.svorticity + xyz[direction]
                    if is_cached:
                        Wd = cached_vars[Wd_name]
                    else:
                        Wd = s.vars[Wd_name]
                    rk_args[Wd_name] = Wd

                call = runge_kutta(**rk_args)
                code = f"{W()} = {call};"
                s.append(code)
                s.jumpline()

                with if_thread_active():
                    if is_periodic:
                        cond = "({lid}>={ghosts}) && ({lid}<{L}-{ghosts}) && ({gidx}<{size})"
                        cond = cond.format(
                            lid=local_id[0],
                            ghosts=cache_ghosts(),
                            L=local_size[0],
                            gidx=global_id[0],
                            size=compute_grid_size[0],
                        )
                    else:
                        cond = "({lid}>={ghosts}) && ({lid}<{L}-{ghosts})".format(
                            lid=local_id[0], ghosts=cache_ghosts(), L=local_size[0]
                        )
                    with s._if_(cond):
                        for i in range(work_dim):
                            Wi_out = self.svorticity + self.xyz[i] + "_out"
                            Wi_out = s.vars[Wi_out]
                            code = f"{Wi_out[global_index()]} = {W[i]};"
                            s.append(code)

    def per_work_statistics(self):
        tg = self.typegen
        dim = self.dim
        ftype = self.ftype
        cached = self.is_cached

        compute_id = self.reqs["compute_id"]
        runge_kutta = self.reqs["runge_kutta"]

        stats = compute_id.per_work_statistics()
        stats += runge_kutta.per_work_statistics()

        size = tg.FLT_BYTES[ftype]
        stats.global_mem_byte_reads += dim * size
        stats.global_mem_byte_writes += dim * size

        return stats

    if False:

        @staticmethod
        def autotune(
            cl_env,
            typegen,
            build_options,
            autotuner_config,
            direction,
            time_integrator,
            formulation,
            discretization,
            velocity,
            vorticity_in,
            vorticity_out,
            velocity_mesh_info,
            vorticity_mesh_info,
        ):

            dir = direction

            if not isinstance(cl_env, OpenClEnvironment):
                raise ValueError("cl_env is not an OpenClEnvironment.")
            if not isinstance(typegen, OpenClTypeGen):
                raise ValueError("typegen is not an OpenClTypeGen.")

            precision = typegen.dtype
            ftype = typegen.fbtype

            device = cl_env.device
            context = cl_env.context
            platform = cl_env.platform
            queue = cl_env.default_queue

            if context is None:
                raise ValueError("context cannot be None.")
            if device is None:
                raise ValueError("device cannot be None.")
            if platform is None:
                raise ValueError("platform cannot be None.")
            if queue is None:
                raise ValueError("queue cannot be None.")
            if typegen.platform != platform or typegen.device != device:
                raise ValueError("platform or device mismatch.")

            if (
                not isinstance(velocity, DiscreteScalarFieldView)
                or velocity.backend.kind != Backend.OPENCL
            ):
                raise ValueError(
                    "velocity is not a DiscreteScalarFieldView of kind OpenCL."
                )
            if (
                not isinstance(vorticity_in, DiscreteScalarFieldView)
                or velocity.backend.kind != Backend.OPENCL
            ):
                raise ValueError(
                    "vorticity_in is not a DiscreteScalarFieldView of kind OpenCL."
                )
            if (
                not isinstance(vorticity_out, DiscreteScalarFieldView)
                or velocity.backend.kind != Backend.OPENCL
            ):
                raise ValueError(
                    "vorticity_out is not a DiscreteScalarFieldView of kind OpenCL"
                )

            dim = velocity.nb_components
            if dim != 3:
                raise ValueError("Stretching only appears in 3D...")
            if direction >= dim:
                raise ValueError("direction >= dim.")
            if (
                (velocity.nb_components != dim)
                or (vorticity_in.nb_components != dim)
                or (vorticity_out.nb_components != dim)
            ):
                raise ValueError(f"Vector components mismatch with dim {dim}.")

            if not isinstance(time_integrator, ExplicitRungeKutta):
                msg = "Given time integrator is not an instance of ExplicitRungeKutta, "
                msg += f"got a {time_integrator.__class__}."
                raise TypeError(msg)

            if not isinstance(formulation, StretchingFormulation):
                msg = "Unknown stretching formulation of type '{}', valid ones are {}."
                msg = msg.format(formulation.__class__, formulation.svalues())
                raise TypeError(msg)

            if not isinstance(discretization, SpaceDiscretization):
                msg = "Discretization parameter is not an instance of SpaceDiscretization, "
                msg += f"but a {discretization.__class__}."
                raise TypeError(msg)
            elif discretization == SpaceDiscretization.FDC2:
                order = 2
            elif discretization == SpaceDiscretization.FDC4:
                order = 4
            elif discretization == SpaceDiscretization.FDC6:
                order = 6
            elif discretization == SpaceDiscretization.FDC8:
                order = 8
            else:
                msg = f"Unknown discretization {discretization}."
                raise ValueError(msg)
            if order % 2 != 0:
                raise ValueError("order must be even.")

            if (not isinstance(velocity_mesh_info, CodegenStruct)) or (
                not velocity_mesh_info.known()
            ):
                msg = (
                    "velocity_mesh_info is not a known MeshInfoStruct codegen variable."
                )
                raise ValueError(msg)
            if (not isinstance(vorticity_mesh_info, CodegenStruct)) or (
                not vorticity_mesh_info.known()
            ):
                msg = "vorticity_mesh_info is not a known MeshInfoStruct codegen variable."
                raise ValueError(msg)

            v_resolution = velocity_mesh_info["local_mesh"]["compute_resolution"].value[
                :dim
            ]
            v_lboundary = velocity_mesh_info["local_mesh"]["lboundary"].value[:dim]
            v_rboundary = velocity_mesh_info["local_mesh"]["rboundary"].value[:dim]
            v_ghosts = velocity_mesh_info["ghosts"].value[:dim]
            v_dx = velocity_mesh_info["dx"].value[:dim]

            w_resolution = vorticity_mesh_info["local_mesh"][
                "compute_resolution"
            ].value[:dim]
            w_lboundary = vorticity_mesh_info["local_mesh"]["lboundary"].value[:dim]
            w_rboundary = vorticity_mesh_info["local_mesh"]["rboundary"].value[:dim]
            w_ghosts = vorticity_mesh_info["ghosts"].value[:dim]
            w_dx = vorticity_mesh_info["dx"].value[:dim]

            is_multi_scale = (v_resolution != w_resolution).any()
            is_inplace = (
                (vorticity_in.data[0].data == vorticity_out.data[0].data)
                or (vorticity_in.data[1].data == vorticity_out.data[1].data)
                or (vorticity_in.data[2].data == vorticity_out.data[2].data)
            )

            w_boundary = (w_lboundary[dir], w_rboundary[dir])
            v_boundary = (v_lboundary[dir], v_rboundary[dir])
            if v_boundary != w_boundary:
                msg = "Boundaries mismatch:\n *velocity: {}\n *vorticity: {}\n"
                msg = msg.formulation(v_boundary, w_boundary)
                raise ValueError(boundary)
            boundary = v_boundary

            (min_v_ghosts, min_w_ghosts) = DirectionalStretchingKernel.min_ghosts(
                boundary, formulation, order, time_integrator, direction
            )

            assert (min_v_ghosts >= 0).all()
            assert (min_w_ghosts >= 0).all()

            if (v_ghosts < min_v_ghosts).any():
                msg = "Given boundary condition implies minimum ghosts numbers to be at least {} "
                msg += "in current direction for velocity but only {} ghosts "
                msg += "are present in the grid."
                msg = msg.format(min_v_ghosts, v_ghosts)
                raise RuntimeError(msg)

            if (w_ghosts < min_w_ghosts).any():
                msg = "Given boundary condition implies minimum ghosts numbers to be at least {} "
                msg += "in current direction for position but only {} ghosts "
                msg += "are present in the grid."
                msg = msg.format(min_w_ghosts, w_ghosts)
                raise RuntimeError(msg)

            if is_multi_scale:
                msg = "Compute_resolution mismatch between velocity and vorticity, "
                msg += "got {} and {} and multiscale has not been implemented yet."
                msg = msg.format(v_resolution, w_resolution)
                raise RuntimeError(msg)

            min_wg_size = DirectionalStretchingKernel.min_wg_size(
                formulation, order, time_integrator
            )

            # work size is the resolution without ghosts
            compute_resolution = w_resolution
            work_size = np.ones(3, dtype=np.int32)
            work_size[:dim] = compute_resolution

            # autotuner parameters

            dump_src = __KERNEL_DEBUG__
            symbolic_mode = False  # __KERNEL_DEBUG__

            min_local_size = np.maximum(
                min_wg_size, [clCharacterize.get_simd_group_size(device, 1), 1, 1]
            )

            caching_options = [True]
            if formulation != StretchingFormulation.CONSERVATIVE:
                caching_options.append(False)

            autotuner_flag = autotuner_config.autotuner_flag
            if autotuner_flag == AutotunerFlags.ESTIMATE:
                max_workitem_workload = (1, 1, 1)
            elif autotuner_flag == AutotunerFlags.MEASURE:
                max_workitem_workload = (1, 1, 8)
            elif autotuner_flag == AutotunerFlags.PATIENT:
                max_workitem_workload = (1, 8, 8)
            elif autotuner_flag == AutotunerFlags.EXHAUSTIVE:
                max_workitem_workload = (1, 16, 16)

            # kernel generator
            def kernel_generator(
                work_size,
                work_load,
                local_work_size,
                kernel_args,
                extra_parameters,
                force_verbose=False,
                force_debug=False,
                return_codegen=False,
                **kargs,
            ):

                # Compile time known variables
                # dt is not known because it depends on splitting direction
                # and simulation current time_step
                known_vars = dict(
                    velocity_mesh_info=velocity_mesh_info,
                    vorticity_mesh_info=vorticity_mesh_info,
                    local_size=local_work_size[:dim],
                )

                # CodeGenerator
                cached = True
                codegen = DirectionalStretchingKernel(
                    typegen=typegen,
                    order=order,
                    dim=dim,
                    direction=direction,
                    boundary=boundary,
                    formulation=formulation,
                    time_integrator=time_integrator,
                    is_inplace=is_inplace,
                    symbolic_mode=symbolic_mode,
                    ftype=ftype,
                    known_vars=known_vars,
                    **extra_parameters,
                )

                global_size = codegen.get_global_size(
                    work_size=work_size,
                    work_load=work_load,
                    local_work_size=local_work_size,
                )

                usable_cache_bytes_per_wg = clCharacterize.usable_local_mem_size(device)
                if (
                    codegen.required_workgroup_cache_size(local_work_size[:dim])[2]
                    > usable_cache_bytes_per_wg
                ):
                    raise KernelGenerationError("Insufficient device cache.")

                # generate source code and build kernel
                src = codegen.__str__()
                src_hash = hashlib.sha512(src).hexdigest()
                prg = cl_env.build_raw_src(
                    src,
                    build_options,
                    kernel_name=codegen.name,
                    force_verbose=force_verbose,
                    force_debug=force_debug,
                )
                kernel = prg.all_kernels()[0]

                if return_codegen:
                    return (codegen, kernel, kernel_args, src_hash, global_size)
                else:
                    return (kernel, kernel_args, src_hash, global_size)

            # Kernel Autotuner
            name = DirectionalStretchingKernel.codegen_name(
                ftype, False, is_inplace, direction, formulation
            )

            autotuner = KernelAutotuner(
                name=name,
                work_dim=dim,
                local_work_dim=1,
                build_opts=build_options,
                autotuner_config=autotuner_config,
            )
            autotuner.add_filter("1d_shape_min", autotuner.min_workitems_per_direction)
            autotuner.register_extra_parameter("is_cached", caching_options)
            autotuner.enable_variable_workitem_workload(
                max_workitem_workload=max_workitem_workload
            )

            dt = 1.0
            kernel_args = [precision(dt)]
            kernel_args += (
                velocity.buffers + vorticity_in.buffers + vorticity_out.buffers
            )

            kernel_args_mapping = {
                "dt": (0, precision),
                "velocity": (slice(1 + 0 * dim, 1 + 1 * dim, 1), cl.MemoryObjectHolder),
                "vorticity_in": (
                    slice(1 + 1 * dim, 1 + 2 * dim, 1),
                    cl.MemoryObjectHolder,
                ),
                "vorticity_out": (
                    slice(1 + 2 * dim, 1 + 3 * dim, 1),
                    cl.MemoryObjectHolder,
                ),
            }

            (gwi, lwi, stats, work_load, extra_params) = autotuner.bench(
                typegen=typegen,
                work_size=work_size,
                kernel_args=kernel_args,
                kernel_generator=kernel_generator,
                dump_src=dump_src,
                min_local_size=min_local_size,
                get_max_global_size=DirectionalStretchingKernel.get_max_global_size,
            )

            (codegen, kernel, kernel_args, src_hash, global_size) = kernel_generator(
                work_size=work_size,
                work_load=work_load,
                local_work_size=lwi,
                kernel_args=kernel_args,
                extra_parameters=extra_params,
                force_verbose=False,
                force_debug=False,
                return_codegen=True,
            )

            kernel_launcher = (
                None  # OpenClKernelLauncher(kernel, queue, list(gwi), list(lwi))
            )

            total_work = work_size[0] * work_size[1] * work_size[2]
            per_work_statistics = codegen.per_work_statistics()

            cache_info = codegen.required_workgroup_cache_size(lwi)

            return (
                kernel_launcher,
                kernel_args,
                kernel_args_mapping,
                total_work,
                per_work_statistics,
                cache_info,
            )


if __name__ == "__main__":
    from hysop.backend.device.codegen.base.test import _test_mesh_info, _test_typegen
    from hysop.backend.device.opencl import cl

    dim = 3
    ghosts = (0, 0, 0)
    v_resolution = (256, 128, 64)
    w_resolution = (1024, 512, 256)
    local_size = (1024, 1, 1)

    tg = _test_typegen("float")
    (_, w_mesh_info) = _test_mesh_info(
        "vorticity_mesh_info", tg, dim, ghosts, w_resolution
    )
    (_, v_mesh_info) = _test_mesh_info(
        "velocity_mesh_info", tg, dim, ghosts, v_resolution
    )

    dsk = DirectionalStretchingKernel(
        typegen=tg,
        ftype=tg.fbtype,
        order=4,
        dim=dim,
        direction=0,
        formulation=StretchingFormulation.CONSERVATIVE,
        time_integrator=ExplicitRungeKutta("RK4"),
        is_cached=True,
        is_inplace=True,
        symbolic_mode=True,
        boundary=(BoundaryCondition.NONE, BoundaryCondition.NONE),
        known_vars=dict(
            velocity_mesh_info=v_mesh_info,
            vorticity_mesh_info=w_mesh_info,
            local_size=local_size[:dim],
        ),
    )
    dsk.edit()
    dsk.test_compile()
