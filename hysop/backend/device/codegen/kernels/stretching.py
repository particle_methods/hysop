# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import operator
import numpy as np

from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.codegen.base.kernel_codegen import KernelCodeGenerator
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
)
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.codegen.base.utils import WriteOnceDict, ArgDict

from hysop.backend.device.codegen.structs.mesh_info import (
    MeshBaseStruct,
    MeshInfoStruct,
)

from hysop.backend.device.codegen.functions.compute_index import ComputeIndexFunction
from hysop.backend.device.codegen.functions.gradient import GradientFunction


class CachedStretchingKernel(KernelCodeGenerator):

    @staticmethod
    def codegen_name(ftype, work_dim):
        return f"cached_stretching_{ftype}_{work_dim}d"

    def __init__(
        self,
        typegen,
        dim,
        device,
        context,
        order=2,
        ftype=None,
        known_vars=None,
        symbolic_mode=True,
    ):

        cached = True
        ftype = ftype if ftype is not None else typegen.fbtype

        work_dim = 3
        kernel_reqs = self.build_requirements(
            typegen=typegen,
            device=device,
            context=context,
            work_dim=work_dim,
            order=order,
            cached=cached,
        )
        kernel_args = self.gen_kernel_arguments(typegen, work_dim, ftype, kernel_reqs)

        name = CachedStretchingKernel.codegen_name(ftype, dim)
        super().__init__(
            name=name,
            typegen=typegen,
            work_dim=work_dim,
            kernel_args=kernel_args,
            known_vars=known_vars,
            device=device,
            context=context,
            vec_type_hint=ftype,
        )

        self.update_requirements(kernel_reqs)

        self.order = order
        self.ftype = ftype
        self.dim = dim

        self.gencode()

    def cache_alloc_bytes(self, local_size):
        order = (
            np.asarray([self.order] * self.work_dim)
            if np.isscalar(self.order)
            else self.order
        )
        return (
            reduce(operator.mul, local_size + order, 1)
            * self.typegen.FLT_BYTES[self.ftype]
        )

    def build_requirements(self, typegen, device, context, work_dim, order, cached):
        reqs = WriteOnceDict()

        compute_id = ComputeIndexFunction(
            typegen=typegen, dim=work_dim, itype="int", wrap=False
        )
        reqs["compute_id"] = compute_id

        mesh_base_struct = MeshBaseStruct(typegen=typegen, typedef="MeshBaseStruct_s")
        reqs["MeshBaseStruct"] = mesh_base_struct

        mesh_info_struct = MeshInfoStruct(typegen=typegen, typedef="MeshInfoStruct_s")
        reqs["MeshInfoStruct"] = mesh_info_struct

        gradient = GradientFunction(
            typegen=typegen, dim=work_dim, order=order, itype="int", cached=cached
        )
        reqs["gradient"] = gradient

        return reqs

        self.update_requirements(reqs)

    def gen_kernel_arguments(self, typegen, work_dim, ftype, requirements):
        fbtype = typegen.fbtype
        _global = OpenClCodeGenerator.default_keywords["global"]
        _local = OpenClCodeGenerator.default_keywords["local"]

        xyz = ["x", "y", "z"]
        svelocity = "V"
        svorticity = "W"

        kargs = ArgDict()
        kargs["dt"] = CodegenVariable(
            ctype=fbtype, name="dt", typegen=typegen, add_impl_const=True, nl=True
        )

        for i in range(work_dim):
            name = svorticity + xyz[i]
            kargs[name] = CodegenVariable(
                storage=_global, name=name, typegen=typegen, ctype=ftype, ptr=True
            )
        for i in range(work_dim):
            name = svelocity + xyz[i]
            kargs[name] = CodegenVariable(
                storage=_global,
                name=name,
                ctype=ftype,
                typegen=typegen,
                ptr=True,
                const=True,
            )

        kargs["mesh_info"] = requirements["MeshInfoStruct"].build_codegen_variable(
            storage="__constant", ptr=True, name="mesh_info"
        )
        kargs["buffer"] = CodegenVariable(
            storage="__local",
            ctype=ftype,
            name="buffer",
            ptr=True,
            typegen=typegen,
            nl=False,
        )

        self.svorticity = svorticity
        self.svelocity = svelocity
        self.xyz = xyz

        return kargs

    def gencode(self):
        s = self
        tg = s.typegen
        fbtype = tg.fbtype
        work_dim = self.work_dim
        dim = self.dim

        global_id = s.vars["global_id"]
        local_id = s.vars["local_id"]
        global_index = s.vars["global_index"]
        local_index = s.vars["local_index"]

        local_size = s.vars["local_size"]
        mesh_info = s.vars["mesh_info"]
        buffer = s.vars["buffer"]
        dt = s.vars["dt"]

        inv_dx = CodegenVectorClBuiltin("inv_dx", fbtype, 3, tg, const=True)
        grid_size = CodegenVectorClBuiltin("N", "int", 3, tg, const=True)
        W = CodegenVectorClBuiltin("W", fbtype, 3, tg, const=True)

        s.update_vars(grid_size=grid_size, inv_dx=inv_dx, W=W)

        compute_index = self.reqs["compute_id"]
        gradient = self.reqs["gradient"]

        with s._kernel_():
            with s._align_() as al:
                al.jumpline()
                global_id.declare(al, align=True, const=True)
                local_id.declare(al, align=True, const=True)
                al.jumpline()
            with s._align_() as al:
                al.append(local_size.declare(align=True, const=True))
                al.append(
                    grid_size.declare(
                        align=True, init=mesh_info["local_mesh"]["resolution"][:3]
                    )
                )
            s.check_workitem_bounds(grid_size, compact=False)
            s.jumpline()
            s.append(inv_dx.declare(init=mesh_info["inv_dx"][:3]))
            with s._block_():
                with s._align_() as al:
                    al.append(
                        global_index.declare(
                            const=True,
                            init=compute_index(idx=global_id, size=grid_size),
                            align=True,
                        )
                    )
                    winit = ""
                    for i in range(work_dim):
                        Wi = self.svorticity + self.xyz[i]
                        winit += self.vars[Wi][global_index()] + ","
                    winit = f"({fbtype}{work_dim})({winit[:-1]})"
                    al.append(W.declare(init=winit, align=True))
                for i in range(work_dim):
                    Wi = self.svorticity + self.xyz[i]
                    Wi = self.vars[Wi]
                    Vi = self.svelocity + self.xyz[i]
                    Vi = self.vars[Vi]
                    grad = gradient(
                        field=Vi,
                        buffer=buffer,
                        global_id=global_id,
                        local_id=local_id,
                        field_size=grid_size,
                        local_size=local_size,
                        inv_dx=inv_dx,
                    )
                    code = f"{Wi[global_index()]} = {W[i]} + {dt()}*dot({W()}, {grad});"
                    s.append(code)
            s.jumpline()


if __name__ == "__main__":

    import pyopencl as cl
    from hysop.backend.device.codegen.base.test import _test_typegen

    devices = []
    contexts = {}
    for plat in cl.get_platforms():
        devices += plat.get_devices()
    for dev in devices:
        ctx = cl.Context([dev])
        contexts[dev] = ctx

    tg = _test_typegen("float", "dec")
    for dev, ctx in contexts.items():
        ek = CachedStretchingKernel(
            typegen=tg,
            context=ctx,
            device=dev,
            order=16,
            dim=1,
            ftype=tg.fbtype,
            known_vars=dict(local_size=(1024, 1, 1)),
        )
        ek.edit()
        ek.test_compile()
        break
