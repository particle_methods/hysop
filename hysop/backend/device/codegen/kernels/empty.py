# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.backend.device.codegen.base.variables import CodegenVectorClBuiltin
from hysop.backend.device.codegen.base.kernel_codegen import KernelCodeGenerator
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.codegen.base.utils import ArgDict


class EmptyKernel(KernelCodeGenerator):

    def __init__(self, typegen, known_vars=None, symbolic_mode=True):

        kargs = ArgDict()
        kargs["grid_size"] = CodegenVectorClBuiltin(
            "N", "int", 3, typegen, symbolic_mode=symbolic_mode
        )

        super().__init__(
            name="empty_kernel",
            typegen=typegen,
            work_dim=3,
            kernel_args=kargs,
            known_vars=known_vars,
        )

        self.gencode()

    def gencode(self):
        s = self
        with s._kernel_():
            # s.vars['global_size'].declare(s)
            # s.vars['local_size'].declare(s)
            s.vars["global_id"].declare(s)
            s.check_workitem_bounds("grid_size")


if __name__ == "__main__":

    from hysop.backend.device.codegen.base.test import _test_typegen

    typegen = _test_typegen("float")

    ek = EmptyKernel(typegen)
    print(ek)
    print()

    known_vars = dict(
        global_size=(1024, 1024, 1024),
        local_size=(8, 8, 8),
        grid_size=(1023, 1023, 1023),
    )
    ek = EmptyKernel(typegen, known_vars, symbolic_mode=True)
    print(ek)
    print()

    known_vars = dict(
        global_size=(1024, 1024, 1024),
        local_size=(8, 8, 8),
        grid_size=(1023, 1023, 1023),
    )
    ek = EmptyKernel(typegen, known_vars, symbolic_mode=False)
    print(ek)
    print()

    ek.edit()
