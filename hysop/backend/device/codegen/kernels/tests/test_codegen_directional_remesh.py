# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import copy
import math

from hysop import __ENABLE_LONG_TESTS__
from hysop.backend.device.codegen.base.test import _test_mesh_info, _test_typegen
from hysop.backend.device.codegen.kernels.directional_remesh import (
    DirectionalRemeshKernelGenerator,
)
from hysop.backend.device.opencl import cl
from hysop.constants import BoundaryCondition, Precision
from hysop.numerics.odesolvers.runge_kutta import ExplicitRungeKutta
from hysop.numerics.remesh.remesh import RemeshKernel
from hysop.testsenv import (
    __HAS_OPENCL_BACKEND__,
    FakeCartesianField,
    iter_clenv,
    opencl_failed,
)
from hysop.tools.contexts import printoptions
from hysop.tools.misc import prod, upper_pow2_or_3
from hysop.tools.numpywrappers import npw
from hysop.tools.htypes import check_instance, first_not_None, to_list
from hysop.tools.units import bytes2str


class TestDirectionalRemesh:

    DEBUG = False

    @classmethod
    def setup_class(
        cls,
        enable_extra_tests=__ENABLE_LONG_TESTS__,
        enable_error_plots=False,
        enable_debug_logs=False,
        grid_size=None,
    ):

        # grid configuration
        default_grid_size = [9, 7, 3]
        grid_size = first_not_None(grid_size, default_grid_size)
        compute_grid_size = npw.asintarray(grid_size)  # X Y Z
        compute_grid_shape = compute_grid_size[::-1]
        assert compute_grid_size.size == 3

        # velocity and scalar bounds
        umax = +1.0
        umin = -1.0
        uinf = 1.0

        S0_min = -1.0
        S0_max = +1.0
        S0_inf = 1.0

        S1_min = -10.0
        S1_max = +10.0
        S1_inf = 10.0

        # time and space discretizations
        inv_dx = compute_grid_size[0] - 1
        dx = 1.0 / (compute_grid_size[0] - 1)

        # field configs
        extra_ghosts = {
            "V": [1, 2, 3],
            "P": [3, 2, 1],
            "S0_in": [2, 3, 1],
            "S1_in": [1, 3, 2],
            "S0_out": [2, 1, 3],
            "S1_out": [3, 1, 2],
        }
        init = {
            "V": ((umin, umax), npw.inf, npw.nan),
            "P": (npw.nan, npw.nan, npw.nan),
            "S0_in": ((S0_min, S0_max), npw.nan, npw.nan),
            "S1_in": ((S1_min, S1_max), npw.nan, npw.nan),
            "S0_out": (npw.inf, npw.inf, npw.nan),
            "S1_out": (npw.inf, npw.inf, npw.nan),
        }
        mf = cl.mem_flags
        intent = {
            "V": mf.READ_ONLY,
            "P": mf.READ_WRITE,
            "S0_in": mf.READ_ONLY,
            "S1_in": mf.READ_ONLY,
            "S0_out": mf.READ_WRITE,
            "S1_out": mf.READ_WRITE,
        }

        print("\n::Setup TestDirectionalRemesh ::")
        print(f"    enable_error_plots: {enable_error_plots}")
        print(f"    enable_extra_tests: {enable_extra_tests}")
        print(f"    enable_debug_logs:  {enable_debug_logs}")
        print("  Compute Grid:")
        print(f"    compute_grid_size:  {compute_grid_size}")
        print(f"    compute_grid_shape: {compute_grid_shape}")
        print(f"    dx:                 {dx}")
        print(f"    umin/umax:          {umin} / {umax}")
        print(f"    S0_min/S0_max:      {S0_min} / {S0_max}")
        print(f"    S1_min/S1_max:      {S1_min} / {S1_max}")

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_error_plots = enable_error_plots
        cls.enable_debug_logs = enable_debug_logs
        cls.compute_grid_size = compute_grid_size
        cls.compute_grid_shape = compute_grid_shape
        cls.umin, cls.umax, cls.uinf = umin, umax, uinf
        cls.S0_min, cls.S0_max, cls.S0_inf = S0_min, S0_max, S0_inf
        cls.S1_min, cls.S1_max, cls.S1_inf = S1_min, S1_max, S1_inf
        cls.dx, cls.inv_dx = dx, inv_dx
        cls.extra_ghosts = extra_ghosts
        cls.init = init
        cls.intent = intent

    def _initialize(
        self, cl_env, precision, cfl, kernel_config, null_velocity, dump_mode="dec"
    ):

        print(
            """
Allocating fields:
    platform:   {}
    device:     {}
    precision:  {}
    cfl:        {}
    kernel_cfg: {}
""".format(
                cl_env.platform.name.strip(),
                cl_env.device.name.strip(),
                precision,
                cfl,
                kernel_config,
            )
        )

        ctx = cl_env.context
        device = cl_env.device
        platform = cl_env.platform
        queue = cl_env.default_queue

        if precision is Precision.FLOAT:
            dtype = npw.float32
            typegen = _test_typegen("float", dump_mode)
        elif precision is Precision.DOUBLE:
            dtype = npw.float64
            typegen = _test_typegen("double", dump_mode)
        else:
            msg = f"Unsupported precision {precision}."

        eps = dtype(npw.finfo(dtype).eps)

        # min and max ghosts
        dt = dtype((cfl * self.dx) / self.uinf)
        kernel_moments = kernel_config[0]
        MAX_ADVEC = 1 + int(npw.floor(dt * self.uinf * self.inv_dx))
        MAX_REMESH = kernel_moments // 2
        assert kernel_moments % 2 == 0
        assert MAX_ADVEC > 0
        assert MAX_REMESH > 0

        min_velocity_ghosts = MAX_ADVEC
        min_scalar_ghosts_out = MAX_ADVEC + MAX_REMESH

        # fields
        min_ghosts = {
            "V": min_velocity_ghosts,
            "P": 0,
            "S0_in": 0,
            "S1_in": 0,
            "S0_out": min_scalar_ghosts_out,
            "S1_out": min_scalar_ghosts_out,
        }

        all_fields = {}
        keys = ["no_ghosts", "with_ghosts"]
        for i, k in enumerate(keys):
            fields = all_fields.setdefault(k, {})
            for vname in self.init.keys():
                mg = [min_ghosts[vname], 0, 0]
                eg = self.extra_ghosts[vname] if (i > 0) else None
                (val, gval, egval) = self.init[vname]
                if (vname == "V") and null_velocity:
                    val = 0.0
                field = FakeCartesianField(
                    name=vname,
                    dim=3,
                    dtype=dtype,
                    nb_components=1,
                    compute_grid_size=self.compute_grid_size.tolist(),
                    min_ghosts=mg,
                    extra_ghosts=eg,
                    init=val,
                    ghosts_init=gval,
                    extra_ghosts_init=egval,
                )
                fields[vname] = field

        ug = self.extra_ghosts["V"][0]
        umin, umax = self.umin, self.umax
        if not null_velocity:
            for fields in all_fields.values():
                view, _ = fields["V"].view(dim=3)
                fields["V"].data[0][view][..., :+MAX_ADVEC] = umin
                fields["V"].data[0][view][..., -MAX_ADVEC:] = umax

        # compute opencl mesh infos
        all_mesh_infos = {}
        dx = dtype(self.dx)
        for i, k in enumerate(keys):
            mesh_infos = all_mesh_infos.setdefault(k, {})
            for dim in range(1, 4):
                ref_values, _ = _test_mesh_info(
                    "ref_mesh_info",
                    typegen,
                    dim,
                    [
                        0,
                    ]
                    * dim,
                    self.compute_grid_size[:dim],
                )
                if dim == 1:
                    assert ref_values["dx"][0] == dx
                else:
                    assert ref_values["dx"][0][0] == dx

                dim_mesh_infos = mesh_infos.setdefault(dim, {})
                for vname, field in all_fields[k].items():
                    (values, mesh_info) = _test_mesh_info(
                        f"{vname}_mesh_info",
                        typegen,
                        dim,
                        field.ghosts[:dim],
                        field.grid_size[:dim],
                    )
                    if dim == 1:
                        assert values["dx"][0] == dx
                    else:
                        assert values["dx"][0][0] == dx
                    dim_mesh_infos[vname] = mesh_info

        intent = self.intent
        mf = cl.mem_flags

        def clbuf(vname, data):
            return tuple(
                cl.Buffer(
                    ctx, flags=intent[vname] | mf.COPY_HOST_PTR, hostbuf=data.data[i]
                )
                for i in range(data.nb_components)
            )

        host_buffers_init = dict(
            no_ghosts={k: v.data for (k, v) in all_fields["no_ghosts"].items()},
            with_ghosts={k: v.data for (k, v) in all_fields["with_ghosts"].items()},
        )
        device_buffers = dict(
            no_ghosts={k: clbuf(k, v) for (k, v) in all_fields["no_ghosts"].items()},
            with_ghosts={
                k: clbuf(k, v) for (k, v) in all_fields["with_ghosts"].items()
            },
        )
        host_buffers_reference = copy.deepcopy(host_buffers_init)
        host_buffers_gpu = copy.deepcopy(host_buffers_init)

        print("\n" + "\n\n".join(str(x) for x in all_fields["with_ghosts"].values()))

        Lx = min(
            typegen.device.max_work_item_sizes[0], typegen.device.max_work_group_size
        )
        Lx = min(Lx, self.compute_grid_size[0] // 2)

        local_work_size = npw.asarray([Lx, 1, 1])
        work_load = npw.asarray([1, 1, 3])

        self.typegen = typegen
        self.queue = queue
        self.ctx = ctx
        self.device = device
        self.kernel_moments = kernel_moments

        self.dtype = dtype
        self.precision = precision
        self.eps = eps
        self.dt = dt

        self.MAX_ADVEC = MAX_ADVEC
        self.MAX_REMESH = MAX_REMESH

        self.all_fields = all_fields
        self.all_mesh_infos = all_mesh_infos

        self.host_buffers_init = host_buffers_init
        self.host_buffers_reference = host_buffers_reference
        self.host_buffers_gpu = host_buffers_gpu
        self.device_buffers = device_buffers

        self.local_work_size = local_work_size
        self.work_load = work_load
        self.min_scalar_ghosts_out = min_scalar_ghosts_out

    def _do_advec_on_cpu(self, rk_scheme, boundary):

        print(f"\n_do_advec_on_cpu(rk_scheme={rk_scheme.name()}, boundary={boundary})")
        dt = self.dt
        dx = self.dtype(self.dx)
        inv_dx = self.dtype(self.inv_dx)

        compute_grid_size = self.compute_grid_size
        compute_grid_shape = self.compute_grid_shape
        enable_debug_logs = self.enable_debug_logs

        for key in self.all_fields.keys():
            P = self.all_fields[key]["P"]
            V = self.all_fields[key]["V"]
            P_field_view, P_view_cg = P.view(dim=3)
            V_field_view, V_view_cg = V.view(dim=3)

            host_init_buffers = self.host_buffers_init[key]
            host_buffers_reference = self.host_buffers_reference[key]

            velocity = host_init_buffers["V"][0][V_field_view]
            position = host_init_buffers["P"][0][P_field_view]

            pos = npw.zeros_like(position)
            pos[...] = npw.arange(compute_grid_size[0])[None, None, :]
            pos *= dx
            if enable_debug_logs:
                print("TIME STEP (DT):")
                print(dt)
                print("INITIAL REFERENCE COMPUTE POSITION LINE:")
                print(pos[0, 0])
                print("COMPUTE REFERENCE GRID VELOCITY:")
                print(velocity[V_view_cg][0, 0])

            def interp_velocity(X):
                Gx = compute_grid_size[0]
                X = X.copy() * inv_dx
                lidx = npw.floor(X).astype(npw.int32)
                alpha = X - lidx
                lidx += self.V_min_ghosts[0]
                ridx = lidx + 1

                Vl = npw.empty_like(X)
                Vr = npw.empty_like(X)
                for i in range(*V_view_cg[0].indices(velocity.shape[0])):
                    for j in range(*V_view_cg[1].indices(velocity.shape[1])):
                        Vl[i, j, :] = velocity[i, j, lidx[i, j, :]]
                        Vr[i, j, :] = velocity[i, j, ridx[i, j, :]]
                return Vl + alpha * (Vr - Vl)

            if rk_scheme.name() == "Euler":
                pos += velocity[V_view_cg] * dt
            elif rk_scheme.name() == "RK2":
                X0 = pos.copy()
                K0 = velocity[V_view_cg]
                X1 = X0 + 0.5 * K0 * dt
                K1 = interp_velocity(X1)
                K = K1
                pos = X0 + K * dt
            elif rk_scheme.name() == "RK4":
                X0 = pos.copy()
                K0 = velocity[V_view_cg]
                X1 = X0 + 0.5 * K0 * dt
                K1 = interp_velocity(X1)
                X2 = X0 + 0.5 * K1 * dt
                K2 = interp_velocity(X2)
                X3 = X0 + 1.0 * K2 * dt
                K3 = interp_velocity(X3)
                K = 1.0 / 6 * K0 + 2.0 / 6 * K1 + 2.0 / 6 * K2 + 1.0 / 6 * K3
                pos = X0 + K * dt
            else:
                msg = f"Unknown Runge-Kutta scheme {rk_scheme}."
                raise ValueError(msg)

            if enable_debug_logs:
                print("FINAL REFERENCE COMPUTE POSITION LINE")
                print(pos[0, 0])

            host_buffers_reference["P"][0][P_field_view] = pos

    def _do_remesh_on_cpu(self, rk_scheme, boundary, kernel, remesh_criteria_eps):
        print()
        print(
            "_do_remesh_on_cpu(rk_scheme={}, boundary={}, criteria_eps={},\n\t\t   kernel={})".format(
                rk_scheme, boundary, remesh_criteria_eps, kernel
            )
        )

        P = 1 + kernel.n // 2
        assert boundary == BoundaryCondition.NONE
        assert kernel.n == self.kernel_moments
        assert (remesh_criteria_eps is None) or (remesh_criteria_eps > 0)
        assert P == self.MAX_REMESH + 1

        eps = self.eps
        dt = self.dt
        dx = self.dx
        inv_dx = self.inv_dx

        compute_grid_size = self.compute_grid_size
        compute_grid_shape = self.compute_grid_shape

        theorical_min_ind_advec = -self.MAX_ADVEC
        theorical_max_ind_advec = self.compute_grid_size[0] + self.MAX_ADVEC

        theorical_min_ind = theorical_min_ind_advec - self.MAX_REMESH
        theorical_max_ind = theorical_max_ind_advec + self.MAX_REMESH - 1
        MIN_GHOSTS = self.MAX_ADVEC + self.MAX_REMESH
        self.MIN_GHOSTS = MIN_GHOSTS

        for target, fields in self.all_fields.items():
            print(f">Target {target}")
            host_init_buffers = self.host_buffers_init[target]
            host_buffers_reference = self.host_buffers_reference[target]
            device_buffers = self.device_buffers[target]

            assert fields["S0_out"].min_ghosts[0] == MIN_GHOSTS
            assert fields["S1_out"].min_ghosts[0] == MIN_GHOSTS
            assert fields["S0_out"].ghosts[0] >= MIN_GHOSTS
            assert fields["S1_out"].ghosts[0] >= MIN_GHOSTS

            P_view, P_view_cg = fields["P"].view(dim=3)
            S0_in_view, S0_in_view_cg = fields["S0_in"].view(dim=3)
            S1_in_view, S1_in_view_cg = fields["S1_in"].view(dim=3)
            S0_out_view, S0_out_view_cg = fields["S0_out"].view(dim=3)
            S1_out_view, S1_out_view_cg = fields["S1_out"].view(dim=3)

            pos = host_buffers_reference["P"][0][P_view]
            S0_in = host_init_buffers["S0_in"][0][S0_in_view]
            S1_in = host_init_buffers["S1_in"][0][S1_in_view]
            S0_out = host_buffers_reference["S0_out"][0][S0_out_view]
            S1_out = host_buffers_reference["S1_out"][0][S1_out_view]

            S0_ghosts_in = fields["S0_in"].min_ghosts
            S0_ghosts_out = fields["S0_out"].min_ghosts
            S1_ghosts_in = fields["S1_in"].min_ghosts
            S1_ghosts_out = fields["S1_out"].min_ghosts

            # floor is round toward negative infinity
            ind = npw.floor(pos * inv_dx).astype(npw.int32)
            y = (pos - ind * dx) * inv_dx
            y = 1 - y

            min_ind, max_ind = npw.min(ind), npw.max(ind)
            ymin, ymax = npw.min(y), npw.max(y)

            if self.DEBUG:
                print(f"imin={min_ind}, imax={max_ind}")
                print(f"ymin={ymin}, ymax={ymax}")
            # assert (y>0.0).all() and (y<1.0).all()

            if min_ind < theorical_min_ind:
                msg0 = "min_ind={} <= floor(umin*dt*inv_dx)={}".format(
                    min_ind, theorical_min_ind_advec
                )
                raise RuntimeError(msg0)

            if max_ind > theorical_max_ind:
                msg1 = "max_ind={} <= floor(umax*dt*inv_dx)={}".format(
                    max_ind, theorical_max_ind_advec
                )
                raise RuntimeError(msg1)

            Gx = compute_grid_size[0]
            ind -= P
            assert (MIN_GHOSTS + ind + 1 >= 0).all()

            S0_out[...] = 0.0
            S1_out[...] = 0.0
            S0_not_remeshed = 0.0
            S1_not_remeshed = 0.0
            for q in range(-P, +P):
                yi = y + q
                wi = kernel.gamma(yi)
                ind += 1
                if self.DEBUG:
                    print("ind", ind[0, 0, :])
                    print("yi", yi[0, 0, :])
                    print()

                _min_ind, _max_ind = npw.min(ind), npw.max(ind)
                assert (
                    _min_ind >= theorical_min_ind
                ).all(), "min_ind = {} < {}".format(_min_ind, theorical_min_ind)
                assert (
                    _max_ind <= theorical_max_ind
                ).all(), "max_ind = {} > {}".format(_max_ind, theorical_max_ind)

                for i, j, k in npw.ndindex(*self.compute_grid_shape):
                    in_index = (
                        S0_ghosts_in[2] + i,
                        S0_ghosts_in[1] + j,
                        S0_ghosts_in[0] + k,
                    )
                    out_index = (
                        S0_ghosts_out[2] + i,
                        S0_ghosts_out[1] + j,
                        S0_ghosts_out[0] + ind[i, j, k],
                    )
                    sin = S0_in[in_index]
                    W = wi[i, j, k]
                    val = W * sin
                    if (remesh_criteria_eps is None) or (
                        abs(sin) > remesh_criteria_eps * eps
                    ):
                        S0_out[out_index] += val
                    else:
                        S0_not_remeshed += val

                    if self.DEBUG and (i == 0) and (j == 0):
                        msg = "wrote {:0.6f} to index {} with y={}, s0={} and W={}"
                        msg = msg.format(val, out_index[2], y[i, j, k], sin, W)
                        print(msg)

                    in_index = (
                        S1_ghosts_in[2] + i,
                        S1_ghosts_in[1] + j,
                        S1_ghosts_in[0] + k,
                    )
                    out_index = (
                        S1_ghosts_out[2] + i,
                        S1_ghosts_out[1] + j,
                        S1_ghosts_out[0] + ind[i, j, k],
                    )
                    sin = S1_in[in_index]
                    val = W * sin
                    if (remesh_criteria_eps is None) or (
                        abs(sin) > remesh_criteria_eps * eps
                    ):
                        S1_out[out_index] += val
                    else:
                        S1_not_remeshed += val

            # check if scalar was conserved
            I0 = npw.sum(S0_in[S0_in_view_cg])
            I1 = npw.sum(S0_out)
            J0 = npw.sum(S1_in[S1_in_view_cg])
            J1 = npw.sum(S1_out)

            err0 = I1 - I0 + S0_not_remeshed
            err1 = J1 - J0 + S1_not_remeshed

            if not npw.allclose(I0, I1 + S0_not_remeshed, atol=2e-6):
                msg = "  #S0 failed: I0:{} !=  I1:{}, error={}, not remeshed {:3.02}%".format(
                    I0,
                    I1 + S0_not_remeshed,
                    I1 + S0_not_remeshed - I0,
                    abs(100 * S1_not_remeshed / J0),
                )
                raise ValueError(msg)
            else:
                print(
                    "  #S0 remesh error: {} ({} eps, not remeshed {:3.02}%)".format(
                        err0, int(err0 / eps), abs(100 * S0_not_remeshed / I0)
                    )
                )

            if not npw.isclose(J0, J1 + S1_not_remeshed, atol=2e-5):
                msg = "  #S1 failed: J0:{} !=  J1:{}, error={}, not remesh {:3.02}%".format(
                    J0,
                    J1 + S1_not_remeshed,
                    J1 + S1_not_remeshed - J0,
                    abs(100 * S1_not_remeshed / J0),
                )
                raise ValueError(msg)
            else:
                print(
                    "  #S1 remesh error: {} ({} eps, not remeshed {:3.02}%)".format(
                        err1, int(err1 / eps), abs(100 * S1_not_remeshed / J0)
                    )
                )

            host_buffers_reference["S0_out"][0][S0_out_view] = S0_out
            host_buffers_reference["S1_out"][0][S1_out_view] = S1_out

    def _do_remesh_on_gpu_and_check(
        self,
        cfl,
        cl_env,
        boundary,
        kernel,
        work_dim,
        is_inplace,
        use_atomics,
        use_short_circuit,
        symbolic_mode,
        remesh_criteria_eps,
        nparticles,
        nscalars,
        null_velocity,
    ):

        msg = """
_do_remesh_on_gpu_and_check()
 -- testing directional {}d remesh with {}
 --     boundaries={}, nparticles={}, nscalars={}
 --     inplace={}, atomic={}, criteria={}'
 --     use_short_circuit={}, symbolic_mode={}'
 --     {}
""".format(
            work_dim,
            kernel,
            str(boundary).lower(),
            nparticles,
            nscalars,
            is_inplace,
            use_atomics,
            remesh_criteria_eps,
            use_short_circuit,
            symbolic_mode,
            "NULL VELOCITY" if null_velocity else "RANDOM_VELOCITY",
        )
        print(msg)

        work_size = self.compute_grid_size[:work_dim]
        work_load = self.work_load[:work_dim]
        local_work_size = self.local_work_size.copy()
        queue = self.queue

        local_work_size = local_work_size[:work_dim]
        max_global_size = DirectionalRemeshKernelGenerator.get_max_global_size(
            work_size, work_load, nparticles
        )
        local_work_size = npw.minimum(local_work_size, max_global_size)
        local_work_size[0] = max(local_work_size[0], 2 * self.min_scalar_ghosts_out)

        print(
            f"  work_size={work_size}, work_load={work_load}, nparticles={nparticles}"
        )
        print(f"  max_global_size={max_global_size}")
        print()

        eps = self.eps
        itemsize = self.dt.itemsize

        sboundary = (
            boundary,
            boundary,
        )
        typegen = self.typegen
        ftype = typegen.fbtype
        scalar_cfl = cfl

        if boundary != BoundaryCondition.NONE:
            raise RuntimeError(f"Unknown boundaty {boundary}.")

        for target, fields in self.all_fields.items():
            print(f">Target {target}")
            mesh_infos = self.all_mesh_infos[target][work_dim]
            device_buffers = self.device_buffers[target]
            host_buffers_gpu = self.host_buffers_gpu[target]
            host_init_buffers = self.host_buffers_init[target]
            host_buffers_reference = self.host_buffers_reference[target]

            field_views = {}
            field_cg_views = {}
            field_mesh_infos = {}
            field_offsets = {}
            field_strides = {}
            for fname, field in fields.items():
                grid_size = field.grid_size
                ghosts = field.ghosts
                views = field.view(dim=work_dim)

                strides = field.data[0].strides[-work_dim:]
                assert (npw.mod(strides, itemsize) == 0).all()
                strides = tuple(x // itemsize for x in strides[::-1])

                if work_dim == 3:
                    offset = 0
                elif work_dim == 2:
                    offset = ghosts[2] * grid_size[1] * grid_size[0]
                elif work_dim == 1:
                    offset = (ghosts[2] * grid_size[1] + ghosts[1]) * grid_size[0]
                else:
                    msg = f"Invalid work dimesion {work_dim}."
                    raise ValueError(msg)
                offset = npw.uint64(offset)

                vnames = {
                    "P": "position",
                    "S0_in": "S0_0_in",
                    "S1_in": "S1_0_in",
                    "S0_out": ("S0_0_inout" if is_inplace else "S0_0_out"),
                    "S1_out": ("S1_0_inout" if is_inplace else "S1_0_out"),
                }
                vname = vnames.get(fname, fname)

                field_views[fname] = views[0]
                field_cg_views[fname] = views[1]
                if fname == "P":
                    field_mesh_infos[vname + "_mesh_info"] = mesh_infos[fname]
                else:
                    mname = fname.replace("out", "inout") if is_inplace else fname
                    field_mesh_infos[mname + "_mesh_info"] = mesh_infos[fname]
                field_offsets[vname + "_offset"] = offset
                field_strides[vname + "_strides"] = strides

            known_vars = dict(local_size=local_work_size)
            known_vars.update(field_mesh_infos)
            known_vars.update(field_offsets)
            known_vars.update(field_strides)

            drk = DirectionalRemeshKernelGenerator(
                typegen=typegen,
                work_dim=work_dim,
                ftype=ftype,
                nparticles=nparticles,
                nscalars=nscalars,
                sboundary=sboundary,
                is_inplace=is_inplace,
                remesh_kernel=kernel,
                scalar_cfl=scalar_cfl,
                use_atomics=use_atomics,
                remesh_criteria_eps=remesh_criteria_eps,
                symbolic_mode=symbolic_mode,
                use_short_circuit=use_short_circuit,
                unroll_loops=False,
                debug_mode=False,
                tuning_mode=False,
                known_vars=known_vars,
            )

            global_work_size = drk.get_global_size(
                work_size, local_work_size, work_load=work_load
            )
            print("  |- Generating and compiling Kernel...")
            print(
                f"  |- global_work_size={global_work_size}, local_work_size={local_work_size}"
            )
            (static_shared_bytes, dynamic_shared_bytes, total_sharedbytes) = (
                drk.required_workgroup_cache_size(local_work_size)
            )
            assert dynamic_shared_bytes == 0

            in_variables = ["P"]
            out_variables = []
            if is_inplace:
                in_variables += [f"S{i}_out" for i in range(nscalars)]
            else:
                in_variables += [f"S{i}_in" for i in range(nscalars)]
            out_variables += [f"S{i}_out" for i in range(nscalars)]

            kernel_args = []
            for varname in in_variables:
                kernel_args.append(device_buffers[varname][0])
            if not is_inplace:
                for varname in out_variables:
                    kernel_args.append(device_buffers[varname][0])

            source = drk.__str__()
            prg = cl.Program(self.ctx, source)
            prg.build(devices=[self.device])
            cl_kernel = prg.all_kernels()[0]

            try:
                cl_kernel.set_args(*kernel_args)
            except:
                msg = f"call to set_args failed, args were: {kernel_args}"
                raise RuntimeError(msg)

            print("  |- CPU => GPU:  ", sep=" ")
            for buf in in_variables + out_variables:
                print(f"{buf}, ", sep=" ")
                if buf == "P":
                    src = host_buffers_reference[buf][0]
                elif is_inplace and buf in ["S0_out", "S1_out"]:
                    ibuf = buf.replace("out", "in")
                    src = npw.full_like(fill_value=npw.nan, a=host_init_buffers[buf][0])
                    src[field_views[buf]][field_cg_views[buf]] = host_init_buffers[
                        ibuf
                    ][0][field_views[ibuf]][field_cg_views[ibuf]]
                else:
                    src = host_init_buffers[buf][0]
                dst = device_buffers[buf][0]
                cl.enqueue_copy(queue, dst, src)
            print(f"\n  |- Kernel execution <<<{global_work_size},{local_work_size}>>>")
            evt = cl.enqueue_nd_range_kernel(
                queue, cl_kernel, global_work_size.tolist(), local_work_size.tolist()
            )
            evt.wait()

            print("  |- GPU => CPU:  ", sep=" ")
            for buf in in_variables + out_variables:
                print(f"{buf}, ", sep=" ")
                src = device_buffers[buf][0]
                dst = host_buffers_gpu[buf][0]
                cl.enqueue_copy(queue, dst, src)
            print()
            print("  |- Synchronizing queue")
            queue.finish()

            if self.DEBUG:
                S0_in_grid_size, S0_out_grid_size = (
                    fields["S0_in"].grid_size,
                    fields["S0_out"].grid_size,
                )
                S1_in_grid_size, S1_out_grid_size = (
                    fields["S1_in"].grid_size,
                    fields["S1_out"].grid_size,
                )
                S0_in_grid_ghosts, S0_out_grid_ghosts = (
                    fields["S0_in"].ghosts,
                    fields["S0_out"].ghosts,
                )
                S1_in_grid_ghosts, S1_out_grid_ghosts = (
                    fields["S1_in"].ghosts,
                    fields["S1_out"].ghosts,
                )
                if is_inplace:
                    S0_out_strides, S1_out_strides = (
                        field_strides["S0_0_inout_strides"],
                        field_strides["S1_0_inout_strides"],
                    )
                    S0_out_offset, S1_out_offset = (
                        field_offsets["S0_0_inout_offset"],
                        field_offsets["S1_0_inout_offset"],
                    )
                else:
                    S0_in_offset, S0_out_offset = (
                        field_offsets["S0_0_in_offset"],
                        field_offsets["S0_0_out_offset"],
                    )
                    S1_in_offset, S1_out_offset = (
                        field_offsets["S1_0_in_offset"],
                        field_offsets["S1_0_out_offset"],
                    )
                    S0_in_strides, S0_out_strides = (
                        field_strides["S0_0_in_strides"],
                        field_strides["S0_0_out_strides"],
                    )
                    S1_in_strides, S1_out_strides = (
                        field_strides["S1_0_in_strides"],
                        field_strides["S1_0_out_strides"],
                    )
                S0_in_view, S0_out_view = field_views["S0_in"], field_views["S0_out"]
                S1_in_view, S1_out_view = field_views["S1_in"], field_views["S1_out"]
                P_view, V_view = field_views["P"], field_views["V"]
                P_strides, P_offset = (
                    field_strides["position_strides"],
                    field_offsets["position_offset"],
                )
                P_grid_size, P_grid_ghosts = fields["P"].grid_size, fields["P"].ghosts
                print()
                print(f"WORKDIM = {work_dim}")
                print(f"ADVEC   = {self.MAX_ADVEC}")
                print(f"REMESH  = {self.MAX_REMESH}")
                print(f"GHOSTS  = {self.MIN_GHOSTS}")
                print()
                print(f"COMPUTE_GRID_SIZE = {self.compute_grid_size}")
                print(f"PRECISION         = {self.precision}")
                print(f"ITEMSIZE          = {self.dt.itemsize}")
                print()
                print(
                    "::P::\n  *grid_size={}\n  *ghosts={},\n  *strides={},\n  *offset={}\n  *view={}".format(
                        P_grid_size, P_grid_ghosts, P_strides, P_offset, P_view
                    )
                )
                print()
                if not is_inplace:
                    print(
                        "::S0_in::\n  *grid_size={}\n  *ghosts={},\n  *strides={},\n  *offset={}\n  *view={}".format(
                            S0_in_grid_size,
                            S0_in_grid_ghosts,
                            S0_in_strides,
                            S0_in_offset,
                            S0_in_view,
                        )
                    )
                    print()
                    if nscalars > 1:
                        print(
                            "::S1_in::\n  *grid_size={}\n  *ghosts={},\n  *strides={},\n  *offset={}\n  *view={}".format(
                                S1_in_grid_size,
                                S1_in_grid_ghosts,
                                S1_in_strides,
                                S1_in_offset,
                                S1_in_view,
                            )
                        )
                        print()
                print(
                    "::S0_out::\n  *grid_size={}\n  *ghosts={},\n  *strides={},\n  *offset={}\n  *view={}".format(
                        S0_out_grid_size,
                        S0_out_grid_ghosts,
                        S0_out_strides,
                        S0_out_offset,
                        S0_out_view,
                    )
                )
                print()
                if nscalars > 1:
                    print(
                        "::S1_out::\n  *grid_size={}\n  *ghosts={},\n  *strides={},\n  *offset={}\n  *view={}".format(
                            S1_out_grid_size,
                            S1_out_grid_ghosts,
                            S1_out_strides,
                            S1_out_offset,
                            S1_out_view,
                        )
                    )
                    print()

                if work_dim < 3:
                    print(
                        "::velocity CPU::\n{}".format(
                            host_buffers_reference["V"][0][V_view]
                        )
                    )
                    print(
                        "::velocity GPU::\n{}".format(host_buffers_gpu["V"][0][V_view])
                    )
                    print()

                    print(
                        "::pos CPU::\n{}".format(host_buffers_reference["P"][0][P_view])
                    )
                    print("::pos GPU::\n{}".format(host_buffers_gpu["P"][0][P_view]))
                    print()

                    print(
                        "::S0_in CPU::\n{}".format(
                            host_init_buffers["S0_in"][0][S0_in_view]
                        )
                    )
                    if not is_inplace:
                        print(
                            "::S0_in GPU::\n{}".format(
                                host_buffers_gpu["S0_in"][0][S0_in_view]
                            )
                        )
                        print()
                    else:
                        print()

                    if nscalars > 1:
                        print(
                            "::S1_in CPU::\n{}".format(
                                host_init_buffers["S1_in"][0][S1_in_view]
                            )
                        )
                        if not is_inplace:
                            print(
                                "::S1_in GPU::\n{}".format(
                                    host_buffers_gpu["S1_in"][0][S1_in_view]
                                )
                            )
                            print()
                        else:
                            print()

                    print(
                        "::S0_out CPU::\n{}".format(
                            host_buffers_reference["S0_out"][0][S0_out_view]
                        )
                    )
                    print(
                        "::S0_out GPU::\n{}".format(
                            host_buffers_gpu["S0_out"][0][S0_out_view]
                        )
                    )
                    print()

                    if nscalars > 1:
                        print(
                            "::S1_out CPU::\n{}".format(
                                host_buffers_reference["S1_out"][0][S1_out_view]
                            )
                        )
                        print(
                            "::S1_out GPU::\n{}".format(
                                host_buffers_gpu["S1_out"][0][S1_out_view]
                            )
                        )
                        print()

            variables = out_variables
            if self.DEBUG:
                variables += in_variables
                if not is_inplace:
                    variables.remove("S0_in")
                    if nscalars > 1:
                        variables.remove("S1_in")

            def max_tol(vname):
                _max_tol = 100 * eps
                if vname.find("S0") == 0:
                    return self.S0_inf * _max_tol
                elif vname.find("S1") == 0:
                    return self.S1_inf * _max_tol
                else:
                    return _max_tol

            buffers = [
                (
                    varname,
                    host_buffers_reference[varname][0],
                    host_buffers_gpu[varname][0],
                    field_views[varname],
                    max_tol(varname),
                )
                for varname in variables
            ]

            self._cmp_buffers(buffers, drk, work_dim)

    def _cmp_buffers(self, buffers, drk, work_dim):
        good = True
        err_buffers = []
        eps = self.eps

        print(f"  |- Checking outputs (eps={eps}):")
        for name, host, dev, view, max_tol in buffers:
            if not npw.isfinite(host[view]).all():
                msg = f"FATAL ERROR: Host input for field {name} is not finite."
                raise ValueError(msg)
            if npw.isfinite(dev[view]).all():
                (l1, l2, linf) = self._distances(host, dev, view)
                print(f"  |  *{name} -> l1={l1}  l2={l2}  linf={linf}")
                if linf > max_tol:
                    err_buffers.append(name)
                    good = False
            else:
                print(f"  |  *{name} -> output is not finite")
                good = False
        if not good:
            msg = f"\n[FAIL] Buffer comparisson failed for buffers {err_buffers}.\n"
            print(msg)
            if self.enable_error_plots:
                from matplotlib import pyplot as plt

                for name, host, dev, view, max_tol in buffers:
                    if name in err_buffers:
                        host = host[view]
                        dev = dev[view]

                        d = (dev - host) * (dev - host)
                        d -= npw.mean(d)

                        if work_dim == 3:
                            fig, axes = plt.subplots(2, 2)
                            axes[0][0].imshow(
                                npw.sum(d, axis=0), interpolation="nearest"
                            )
                            axes[0][1].imshow(
                                npw.sum(d, axis=1), interpolation="nearest"
                            )
                            axes[1][0].imshow(
                                npw.sum(d, axis=2), interpolation="nearest"
                            )
                            axes[1][1].imshow(
                                npw.sum(d, axis=(0, 1))[npw.newaxis, :],
                                interpolation="nearest",
                            )
                            plt.title(name)
                            fig.show()
                            raw_input("Press enter to close all windows.")
                            break
                        elif work_dim == 2:
                            fig, axe = plt.subplots()
                            axe.imshow(d, interpolation="nearest")
                            plt.title(name)
                            fig.show()
                            raw_input("Press enter to close all windows.")
                            break
                        else:
                            pass
            else:
                for name, host, dev, view, max_tol in buffers:
                    if name in err_buffers:
                        host = host[view]
                        dev = dev[view]
                        dist = npw.abs(dev - host)
                        dist[~npw.isfinite(dev)] = npw.inf

                        print(f"{name} HOST")
                        print(host)
                        print(f"{name} DEVICE")
                        print(dev)
                        print(f"{name} ABS(DEVICE - HOST)")
                        npw.fancy_print(
                            dist, replace_values={(lambda a: a < max_tol): "."}
                        )

                        I0, I1 = npw.sum(host), npw.sum(dev)
                        print(f"I0={I0}, I1={I1}")
                        if abs(I1 - I0) < 10 * self.eps:
                            print(
                                f"[FATAL ERROR] scalar {name} was conserved but output mismatched."
                            )
                        else:
                            print(
                                f"[FATAL ERROR] scalar {name} was not conserved and output mismatched"
                            )
                        print()
                        print()
            if self.DEBUG:
                drk.edit()

            print("  |>TEST KO")
            raise RuntimeError(msg)
        print("  |>TEST OK")

    def _distances(self, lhs, rhs, view):
        d = rhs[view] - lhs[view]
        da = npw.abs(d)

        l1 = npw.sum(da) / d.size
        l2 = npw.sqrt(npw.sum(d * d) / d.size)
        linf = npw.max(da)
        return (l1, l2, linf)

    def _check_kernels(self, rk_scheme, cfl):
        assert cfl > 0
        check_instance(rk_scheme, ExplicitRungeKutta)

        boundaries = (BoundaryCondition.NONE,)

        split_polys = (False, True)
        use_atomics = (False, True)
        is_inplaces = (False, True)

        if self.enable_extra_tests:
            symbolic_modes = (False, True)
            use_short_circuits = (False, True)
            nscalars = (1, 2)
            precisions = (Precision.FLOAT, Precision.DOUBLE)
            kernels = [(2, 1), (2, 2), (4, 2), (4, 4), (6, 4), (6, 6), (8, 4)]
            work_dims = (1, 2, 3)
            nparticles = (1, 2, 4, 8, 16)
            remesh_criterias = (None, 1000000)
        else:
            symbolic_modes = (False,)
            use_short_circuits = (False,)
            nscalars = (1,)
            precisions = (Precision.FLOAT,)
            kernels = [(2, 2)]
            work_dims = (2,)
            nparticles = (4,)
            remesh_criterias = (None,)

        cl_envs = tuple(cl_env for cl_env in iter_clenv())
        print()
        print(f"ENABLE_LONG_TESTS = {__ENABLE_LONG_TESTS__}")
        print()
        if not __ENABLE_LONG_TESTS__:
            print("Long tests disabled, only the first OpenCl platform will be tested.")
        print()
        print(f"Found {len(cl_envs)} OpenCl environments:")
        for cl_env in cl_envs:
            print(cl_env)

        ntests = 0
        for cl_env in cl_envs:
            print(
                "SWITCHING CL_ENV TO: platform {}, device {}".format(
                    cl_env.platform.name.strip(), cl_env.device.name.strip()
                )
            )
            for precision in precisions:
                for kernel_config in kernels:
                    for null_velocity in [True, False]:
                        self._initialize(
                            cl_env=cl_env,
                            precision=precision,
                            cfl=cfl,
                            kernel_config=kernel_config,
                            null_velocity=null_velocity,
                        )
                        for boundary in boundaries:
                            self._do_advec_on_cpu(
                                boundary=boundary, rk_scheme=rk_scheme
                            )
                            for split_poly in split_polys:
                                kernel = RemeshKernel(
                                    *kernel_config,
                                    split_polys=split_poly,
                                    verbose=False,
                                )
                                for remesh_criteria_eps in remesh_criterias:
                                    self._do_remesh_on_cpu(
                                        boundary=boundary,
                                        rk_scheme=rk_scheme,
                                        kernel=kernel,
                                        remesh_criteria_eps=remesh_criteria_eps,
                                    )
                                    for work_dim in work_dims:
                                        for is_inplace in is_inplaces:
                                            for use_atomic in use_atomics:
                                                for nparticle in nparticles:
                                                    if (not use_atomic) and (
                                                        nparticle
                                                        <= int(2 * npw.floor(cfl) + 1)
                                                    ):
                                                        continue
                                                    for nscalar in nscalars:
                                                        for (
                                                            symbolic_mode
                                                        ) in symbolic_modes:
                                                            for (
                                                                use_short_circuit
                                                            ) in use_short_circuits:
                                                                self._do_remesh_on_gpu_and_check(
                                                                    cfl=cfl,
                                                                    cl_env=cl_env,
                                                                    boundary=boundary,
                                                                    kernel=kernel,
                                                                    work_dim=work_dim,
                                                                    is_inplace=is_inplace,
                                                                    use_atomics=use_atomic,
                                                                    use_short_circuit=use_short_circuit,
                                                                    symbolic_mode=symbolic_mode,
                                                                    remesh_criteria_eps=remesh_criteria_eps,
                                                                    nparticles=nparticle,
                                                                    nscalars=nscalar,
                                                                    null_velocity=null_velocity,
                                                                )
                                                                ntests += 2  # with and without extra ghosts
        print()
        print(f"DirectionalRemesh: All {ntests} tests passed.")

    @opencl_failed
    def test_remesh_from_Euler_advection_low_cfl(self, cfl=0.5788):
        rk_scheme = ExplicitRungeKutta("Euler")
        self._check_kernels(rk_scheme=rk_scheme, cfl=cfl)

    @opencl_failed
    def test_remesh_from_Euler_advection_high_cfl(self, cfl=1.78):
        rk_scheme = ExplicitRungeKutta("Euler")
        self._check_kernels(rk_scheme=rk_scheme, cfl=cfl)


if __name__ == "__main__":
    if not __HAS_OPENCL_BACKEND__:
        msg = "OpenCL is not present (pyopencl has not been found)."
        raise RuntimeError(msg)

    TestDirectionalRemesh.setup_class(
        enable_extra_tests=False, enable_error_plots=False, enable_debug_logs=False
    )
    test = TestDirectionalRemesh()

    with printoptions(linewidth=200, formatter={"float": lambda x: f"{x:0.2f}"}):
        test.test_remesh_from_Euler_advection_low_cfl()
        test.test_remesh_from_Euler_advection_high_cfl()
