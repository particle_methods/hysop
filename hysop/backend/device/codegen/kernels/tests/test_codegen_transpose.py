# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import copy
import itertools as it
import math
import os
import sys
import tempfile

import numpy as np
from hysop import __ENABLE_LONG_TESTS__
from hysop.backend.device.codegen.base.test import _test_typegen
from hysop.backend.device.codegen.base.variables import dtype_to_ctype
from hysop.backend.device.codegen.kernels.transpose import TransposeKernelGenerator
from hysop.backend.device.opencl import cl, cl_api, clCharacterize, clTools
from hysop.tools.contexts import printoptions
from hysop.tools.misc import prod, upper_pow2_or_3
from hysop.tools.numerics import is_integer
from hysop.tools.numpywrappers import npw
from hysop.tools.htypes import check_instance, to_list


class TestTranspose:

    @classmethod
    def setup_class(
        cls,
        enable_extra_tests=__ENABLE_LONG_TESTS__,
        enable_error_plots=False,
        enable_interactive_debug=False,
        enable_debug_mode=False,
    ):

        typegen = _test_typegen("float", "dec")
        queue = cl.CommandQueue(typegen.context)
        ctx = typegen.context

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_error_plots = enable_error_plots
        cls.enable_interactive_debug = enable_interactive_debug

        if enable_debug_mode:
            cls.size_min = 4
            cls.size_max = 5
        else:
            cls.size_min = 1
            cls.size_max = 32
            # sizeof(double) * (16^4) = 2^(4*4+3) = 2^(20)/2 = 512Mo buffers in worst case

        cls.queue = queue
        cls.ctx = ctx
        cls.typegen = typegen
        cls.device = typegen.device

        cls.enable_debug_mode = enable_debug_mode

    def _alloc_cpu_gpu(self, dtype, dim, is_inplace):

        ctx = self.ctx
        queue = self.queue

        grid_size = np.rint(
            self.size_min + np.random.rand(dim) * (self.size_max - self.size_min)
        )
        grid_size = grid_size.astype(np.int32)
        if is_inplace:
            grid_size[1:] = grid_size[0]

        igrid_ghosts = 1 + np.rint(dim * np.random.rand(dim)).astype(np.int32)
        ogrid_ghosts = 1 + np.rint(dim * np.random.rand(dim)).astype(np.int32)

        igrid_size = grid_size + 2 * igrid_ghosts
        ogrid_size = grid_size + 2 * ogrid_ghosts

        grid_shape = grid_size[::-1]
        igrid_shape = igrid_size[::-1]
        ogrid_shape = ogrid_size[::-1]

        grid_bytes = grid_size.size * np.dtype(dtype).itemsize
        igrid_bytes = igrid_size.size * np.dtype(dtype).itemsize
        ogrid_bytes = ogrid_size.size * np.dtype(dtype).itemsize

        grid_view = [slice(0, grid_size[i] + 0) for i in range(dim - 1, -1, -1)]
        igrid_view = [
            slice(igrid_ghosts[i], grid_size[i] + igrid_ghosts[i])
            for i in range(dim - 1, -1, -1)
        ]
        ogrid_view = [
            slice(ogrid_ghosts[i], grid_size[i] + ogrid_ghosts[i])
            for i in range(dim - 1, -1, -1)
        ]

        print()
        print(f"::Alloc:: dtype={dtype}  dim={dim}")
        print(f"  *INPUT:  base={grid_size}  ghosts={igrid_ghosts}  size={igrid_size}")
        print(f"  *OUTPUT: base={grid_size}  ghosts={ogrid_ghosts}  size={ogrid_size}")

        mf = cl.mem_flags

        def random(*shape):
            array = np.random.rand(*shape)
            if is_integer(dtype):
                info = np.iinfo(dtype)
                min_ = max(-99, info.min)
                max_ = min(+99, info.max)
                return np.rint(min_ + array * (max_ - min_)).astype(dtype)
            else:
                return array.astype(dtype)

        host_buffers_init = {
            "no_ghosts": {
                "Tin": random(*grid_shape),
                "Tout": -1 * np.ones(shape=grid_shape, dtype=dtype),
            },
            "with_ghosts": {
                "Tin": random(*igrid_shape),
                "Tout": -1 * np.ones(shape=ogrid_shape, dtype=dtype),
            },
        }

        device_buffers = {
            "no_ghosts": {
                "Tin": cl.Buffer(
                    ctx,
                    flags=mf.READ_ONLY | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["no_ghosts"]["Tin"],
                ),
                "Tout": cl.Buffer(
                    ctx,
                    flags=mf.WRITE_ONLY | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["no_ghosts"]["Tout"],
                ),
            },
            "with_ghosts": {
                "Tin": cl.Buffer(
                    ctx,
                    flags=mf.READ_ONLY | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["with_ghosts"]["Tin"],
                ),
                "Tout": cl.Buffer(
                    ctx,
                    flags=mf.WRITE_ONLY | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["with_ghosts"]["Tout"],
                ),
            },
        }

        for i in range(TransposeKernelGenerator.n_dbg_arrays):
            name = f"dbg{i}"
            for target in ["no_ghosts", "with_ghosts"]:
                arr = (-i) * np.ones(shape=grid_shape, dtype=np.int32)
                host_buffers_init[target][name] = arr
                device_buffers[target][name] = cl.Buffer(
                    ctx, flags=mf.READ_WRITE | mf.COPY_HOST_PTR, hostbuf=arr
                )

        host_buffers_reference = copy.deepcopy(host_buffers_init)
        host_buffers_gpu = copy.deepcopy(host_buffers_init)

        self.dtype = dtype
        self.ctype = dtype_to_ctype(dtype)
        self.dim = dim

        self.host_buffers_init = host_buffers_init
        self.host_buffers_reference = host_buffers_reference
        self.host_buffers_gpu = host_buffers_gpu
        self.device_buffers = device_buffers

        self.grid_size = grid_size
        self.igrid_size = igrid_size
        self.ogrid_size = ogrid_size

        self.grid_shape = grid_shape
        self.igrid_shape = igrid_shape
        self.ogrid_shape = ogrid_shape

        self.grid_bytes = grid_bytes
        self.igrid_bytes = igrid_bytes
        self.ogrid_bytes = ogrid_bytes

        self.grid_view = grid_view
        self.igrid_view = igrid_view
        self.ogrid_view = ogrid_view

        self.igrid_ghosts = igrid_ghosts
        self.ogrid_ghosts = ogrid_ghosts

    @classmethod
    def teardown_class(cls):
        pass

    def setup_method(self, method):
        pass

    def teardown_method(self, method):
        pass

    def _do_transpose_cpu(self, is_inplace, axes):
        print("  *Transposing on CPU.")
        out_target = "Tout"

        for target in ["no_ghosts"]:  # , 'with_ghosts']:
            host_init_buffers = self.host_buffers_init[target]
            host_buffers_reference = self.host_buffers_reference[target]

            if target == "no_ghosts":
                in_view = self.grid_view
                out_view = self.grid_view
            else:
                in_view = self.igrid_view
                out_view = self.igrid_view if is_inplace else self.ogrid_view
            Tin = host_init_buffers["Tin"]
            Tout = host_buffers_reference[out_target]

            out_view = [out_view[i] for i in axes]
            Tout = Tout.reshape(tuple(Tout.shape[i] for i in axes))
            Tout[tuple(out_view)] = np.transpose(Tin[tuple(in_view)].copy(), axes=axes)

    def _do_compute_gpu_and_check(
        self, dim, axes, is_inplace, vectorization, tile_size, tile_padding
    ):

        msg = "\nTesting {}d {} transposition with {} axes, ({})({}) "
        msg += "vectorization and tiles of size {}."
        msg = msg.format(
            dim,
            "inplace" if is_inplace else "out of place",
            axes,
            self.ctype,
            vectorization,
            tile_size,
        )
        print(msg)

        device = self.device
        enable_debug_mode = self.enable_debug_mode
        typegen = self.typegen
        ndim = upper_pow2_or_3(dim)
        offset_dtype = npw.uint64

        def compute_offset(base, slc, work_dim):
            view = base[tuple(slc)]
            start = []
            for slci, sh in zip(slc, base.shape):
                if isinstance(slci, slice):
                    start.append(slci.indices(sh)[0])
                else:
                    start.append(slci)
            start_offset = npw.sum(
                npw.asarray(start[:-work_dim]) * base.strides[:-work_dim]
            )
            return start_offset

        def compute_strides(base, slc, work_dim):
            view = base[tuple(slc)]
            strides = view.strides
            return strides

        def make_offset(offset, dtype):
            """Build an offset in number of elements instead of bytes."""
            msg = "Unaligned offset {} for dtype {} (itemsize={}).".format(
                offset, dtype, dtype.itemsize
            )
            assert (offset % dtype.itemsize) == 0
            return offset_dtype(offset // dtype.itemsize)

        def make_strides(bstrides, dtype):
            msg = "Invalid strides {} for dtype {} (itemsize={}).".format(
                bstrides, dtype.__class__.__name__, dtype.itemsize
            )
            assert (npw.mod(bstrides, dtype.itemsize) == 0).all(), msg
            data = typegen.make_uintn(
                vals=tuple(x // dtype.itemsize for x in bstrides[::-1]), n=ndim, dval=0
            )
            return to_list(data)[:ndim]

        for target in ["no_ghosts"]:
            host_init_buffers = self.host_buffers_init[target]
            host_buffers_reference = self.host_buffers_reference[target]
            host_buffers_gpu = self.host_buffers_gpu[target]
            device_buffers = self.device_buffers[target]

            if target == "no_ghosts":
                in_view = self.grid_view
                out_view = self.grid_view
            else:
                in_view = self.igrid_view
                out_view = self.igrid_view if is_inplace else self.ogrid_view
            out_view = [out_view[i] for i in axes]

            shape_dim = upper_pow2_or_3(dim)
            shape = tuple(gs for gs in self.grid_size)
            shape += (shape_dim - dim) * (0,)

            known_vars = {"shape": shape}

            if is_inplace:
                inout_strides = compute_strides(host_init_buffers["Tin"], in_view, dim)
                inout_offset = compute_offset(host_init_buffers["Tin"], in_view, dim)
                known_vars["inout_strides"] = make_strides(
                    inout_strides, host_init_buffers["Tin"].dtype
                )
                known_vars["inout_offset"] = make_offset(
                    inout_offset, host_init_buffers["Tin"].dtype
                )
            else:
                in_strides = compute_strides(host_init_buffers["Tin"], in_view, dim)
                in_offset = compute_offset(host_init_buffers["Tin"], in_view, dim)
                known_vars["in_strides"] = make_strides(
                    in_strides, host_init_buffers["Tin"].dtype
                )
                known_vars["in_offset"] = make_offset(
                    in_offset, host_init_buffers["Tin"].dtype
                )
                out_strides = compute_strides(host_init_buffers["Tout"], in_view, dim)
                out_offset = compute_offset(host_init_buffers["Tout"], in_view, dim)
                known_vars["out_strides"] = make_strides(
                    out_strides, host_init_buffers["Tout"].dtype
                )
                known_vars["out_offset"] = make_offset(
                    out_offset, host_init_buffers["Tout"].dtype
                )

            dak = TransposeKernelGenerator(
                tile_size=tile_size,
                tile_padding=tile_padding,
                axes=axes,
                ctype=self.ctype,
                typegen=self.typegen,
                vectorization=vectorization,
                is_inplace=is_inplace,
                symbolic_mode=False,
                debug_mode=enable_debug_mode,
                known_vars=known_vars,
            )
            # dak.edit()

            usable_cache_bytes_per_wg = clCharacterize.usable_local_mem_size(device)
            (static_shared_bytes, dynamic_shared_bytes, total_bytes) = (
                dak.required_workgroup_cache_size()
            )
            assert total_bytes <= usable_cache_bytes_per_wg, "Tiles are too big."

            work_dim = dak.work_dim

            # local_work_size = np.minimum([tile_size]*work_dim,
            #         device.max_work_item_sizes[:work_dim])
            # for i in dak.workload_indexes:
            #     local_work_size[i] = 1
            # max_work = device.max_work_group_size
            # while (prod(local_work_size) > max_work):
            #     for i in range(work_dim-1, -1, -1):
            #         if local_work_size[i] > 1:
            #             break
            #     local_work_size[i] //= 2

            # work_load = [1]*work_dim
            # work_size = TransposeKernelGenerator.max_local_worksize(
            #     self.grid_size, len(axes), tile_size,
            #         vectorization, axes)#, local_work_size)

            # max_global_size  = TransposeKernelGenerator.max_global_size(work_size, work_load)
            # local_work_size  = np.minimum(max_global_size, local_work_size)
            local_work_size = TransposeKernelGenerator.max_local_worksize(
                axes=axes,
                shape=shape,
                work_dim=work_dim,
                tile_size=tile_size,
                vectorization=vectorization,
            )
            work_load = (1,) * work_dim
            global_work_size = TransposeKernelGenerator.compute_global_size(
                shape=shape,
                tile_size=tile_size,
                vectorization=vectorization,
                axes=axes,
                local_work_size=local_work_size,
                work_load=work_load,
            )

            tile_grid_size = np.asarray(
                [
                    self.grid_size[i]
                    for (i, j) in enumerate(axes)
                    if (i != j) or (i == 0)
                ]
            )

            print(
                "AXES={}   TILE=({})".format(
                    axes,
                    ",".join(
                        [
                            str(j) if (i != j) else ("X" if i > 0 else "*")
                            for (i, j) in enumerate(axes)
                        ]
                    ),
                )
            )
            print(
                "  *shape={}  in_tile_shape=[{}]  out_tile_shape=[{}]".format(
                    self.grid_size,
                    ",".join(
                        [
                            str(self.grid_size[i])
                            for (i, j) in enumerate(axes)
                            if (i != j) or (i == 0)
                        ]
                    ),
                    ",".join(
                        [
                            str(self.grid_size[j])
                            for (i, j) in enumerate(axes)
                            if (i != j) or (i == 0)
                        ]
                    ),
                )
            )

            print(
                "  *tile={} vec={} L={} WL={} G={} num_groups={} num_blocks={}".format(
                    tile_size,
                    vectorization,
                    local_work_size,
                    work_load,
                    global_work_size,
                    (global_work_size + local_work_size - 1) // local_work_size,
                    (tile_grid_size + tile_size - 1) // tile_size,
                )
            )

            if is_inplace:
                variables = ["Tin"]
            else:
                variables = ["Tin", "Tout"]

            debug = []
            if enable_debug_mode:
                debug += [f"dbg{i}" for i in range(dak.n_dbg_arrays)]
            kernel_args = []
            for varname in variables + debug:
                kernel_args.append(device_buffers[varname])
            assert dynamic_shared_bytes == 0

            print("  *Generating and compiling Kernel...")
            source = dak.__str__()

            # loop for interactive debugging
            good = False
            i = 0
            e = None
            while not good:
                if self.enable_interactive_debug:
                    print(f"== {i}th iteration ==")
                    print()
                try:
                    prg = cl.Program(self.ctx, source)
                    prg.build(devices=[self.device])
                    kernel = prg.all_kernels()[0]
                    print(kernel_args, variables)
                    kernel.set_args(*kernel_args)

                    queue = self.queue

                    print("   ::CPU => GPU")
                    for buf in variables + debug:
                        src = host_init_buffers[buf]
                        dst = device_buffers[buf]
                        cl.enqueue_copy(queue, dst, src)

                    print(
                        "   ::Kernel execution <<<{},{}>>>".format(
                            global_work_size, local_work_size
                        )
                    )
                    evt = cl.enqueue_nd_range_kernel(
                        queue,
                        kernel,
                        global_work_size.tolist(),
                        local_work_size.tolist(),
                    )
                    evt.wait()

                    print("   ::GPU => CPU")
                    for buf in variables + debug:
                        src = device_buffers[buf]
                        if is_inplace and buf == "Tin":
                            dst = host_buffers_gpu["Tout"]
                        else:
                            dst = host_buffers_gpu[buf]
                        cl.enqueue_copy(queue, dst, src)

                    print("   ::Synchronize queue")
                    queue.flush()
                    queue.finish()

                    Tin = host_buffers_gpu["Tin"]
                    Tout = host_buffers_gpu["Tout"]
                    Tin_cpu = host_buffers_reference["Tin"]
                    Tout_cpu = host_buffers_reference["Tout"]
                    nshape = tuple(Tin.shape[i] for i in axes)
                    Tout = Tout.reshape(nshape)
                    Tout_cpu = Tout_cpu.reshape(nshape)

                    if enable_debug_mode:
                        for i in range(dak.n_dbg_arrays):
                            print(f"::DBG{i}::")
                            print(host_buffers_gpu[f"dbg{i}"])
                            print()
                        print("::IN::")
                        print(Tin_cpu[tuple(in_view)])
                        print()
                        print("::OUT CPU::")
                        print(Tout_cpu[tuple(out_view)])
                        print()
                        print("::OUT GPU::")
                        print(Tout[tuple(out_view)])

                    if is_inplace:
                        buffers = (("Tout", Tout_cpu, Tout, out_view),)
                    else:
                        buffers = (
                            ("Tin", Tin_cpu, Tin, in_view),
                            ("Tout", Tout_cpu, Tout, out_view),
                        )
                    good, err_buffers = self._cmp_buffers(buffers, dak, dim)
                except cl_api.RuntimeError as error:
                    e = error
                    print("ERROR: ", e)
                    good = False

                if not self.enable_interactive_debug:
                    break
                if not good:
                    filepath = f"{tempfile.gettempdir()}/debug_dump_{i}.cl"
                    dak.edit(filepath=filepath, modified=True)
                    source = dak.modified_code
                    print(f"Source dumped to {filepath}.")
                    if source[:4] == "exit":
                        break
                os.system("clear")
                i += 1
            if e is not None:
                raise e
            if self.enable_debug_mode and not good:
                dak.edit()
            self._check_errors(buffers, dak, dim, good, err_buffers)

    def _cmp_buffers(self, buffers, dak, dim):
        good = True
        err_buffers = []

        print("  *Comparing outputs")
        for name, host, dev, view in buffers:
            if host.shape != dev.shape:
                msg = "Incompatible buffer shapes between cpu {} and gpu {} outputs."
                msg = msg.format(host.shape, dev.shape)
                raise RuntimeError(msg)
            (l1, l2, linf) = self._distances(host, dev, view)
            print(f"   ::{name:6} -> l1={l1}  l2={l2}  linf={linf}")
            if linf > 1e-12:
                err_buffers.append(name)
                good = False
        return good, err_buffers

    def _check_errors(self, buffers, dak, dim, good, err_buffers):
        if not good:
            msg = f"\n[FAIL] Buffer comparisson failed for buffers {err_buffers}.\n"
            print(msg)
            if self.enable_error_plots:
                from matplotlib import pyplot as plt

                for name, host, dev, view in buffers:
                    if name in err_buffers:

                        host = host[tuple(view)]
                        dev = dev[tuple(view)]

                        E = (dev - host).astype(np.float64)
                        d = E * E
                        d -= np.mean(d)

                        if dim == 4:
                            fig, axes = plt.subplots(4, 3)
                            axes[0][0].imshow(
                                np.sum(d, axis=(0, 1)), interpolation="nearest"
                            )
                            axes[0][1].imshow(
                                np.sum(d, axis=(0, 2)), interpolation="nearest"
                            )
                            axes[0][2].imshow(
                                np.sum(d, axis=(0, 3)), interpolation="nearest"
                            )
                            axes[1][0].imshow(
                                np.sum(d, axis=(1, 0)), interpolation="nearest"
                            )
                            axes[1][1].imshow(
                                np.sum(d, axis=(1, 2)), interpolation="nearest"
                            )
                            axes[1][2].imshow(
                                np.sum(d, axis=(1, 3)), interpolation="nearest"
                            )
                            axes[2][0].imshow(
                                np.sum(d, axis=(2, 0)), interpolation="nearest"
                            )
                            axes[2][1].imshow(
                                np.sum(d, axis=(2, 1)), interpolation="nearest"
                            )
                            axes[2][2].imshow(
                                np.sum(d, axis=(2, 3)), interpolation="nearest"
                            )
                            axes[3][0].imshow(
                                np.sum(d, axis=(3, 0)), interpolation="nearest"
                            )
                            axes[3][1].imshow(
                                np.sum(d, axis=(3, 1)), interpolation="nearest"
                            )
                            axes[3][2].imshow(
                                np.sum(d, axis=(3, 2)), interpolation="nearest"
                            )
                            plt.title(name)
                            fig.show()
                            # raw_input('Press enter to close all windows.')
                            break
                        elif dim == 3:
                            fig, axes = plt.subplots(2, 2)
                            axes[0][0].imshow(
                                np.sum(d, axis=0), interpolation="nearest"
                            )
                            axes[0][1].imshow(
                                np.sum(d, axis=1), interpolation="nearest"
                            )
                            axes[1][0].imshow(
                                np.sum(d, axis=2), interpolation="nearest"
                            )
                            axes[1][1].imshow(
                                np.sum(d, axis=(0, 1))[np.newaxis, :],
                                interpolation="nearest",
                            )
                            plt.title(name)
                            fig.show()
                            # raw_input('Press enter to close all windows.')
                            break
                        elif dim == 2:
                            fig, axe = plt.subplots()
                            axe.imshow(d, interpolation="nearest")
                            plt.title(name)
                            fig.show()
                            # raw_input('Press enter to close all windows.')
                            break
                        else:
                            pass
            raise RuntimeError(msg)

    def _distances(self, lhs, rhs, view):
        rhs = rhs[tuple(view)].astype(np.float64)
        lhs = lhs[tuple(view)].astype(np.float64)

        d = rhs - lhs
        da = np.abs(d)

        l1 = np.sum(da) / d.size
        l2 = np.sqrt(np.sum(d * d)) / d.size
        linf = np.max(da)
        return (l1, l2, linf)

    # ### TESTS ###
    # ## OUT OF PLACE TRANSPOSE
    # # 2d tests
    def test_tobefixed_transpose_YX_out_of_place(self):
        self._test_transpose(dim=2, axes=(1, 0), is_inplace=False)

    # 3d tests
    def test_tobefixed_transpose_XZY_out_of_place(self):
        self._test_transpose(dim=3, axes=(1, 0, 2), is_inplace=False)

    def test_tobefixed_transpose_YXZ_out_of_place(self):
        self._test_transpose(dim=3, axes=(0, 2, 1), is_inplace=False)

    def test_tobefixed_transpose_YZX_out_of_place(self):
        self._test_transpose(dim=3, axes=(2, 0, 1), is_inplace=False)

    def test_tobefixed_transpose_ZXY_out_of_place(self):
        self._test_transpose(dim=3, axes=(1, 2, 0), is_inplace=False)

    def test_tobefixed_transpose_ZYX_out_of_place(self):
        self._test_transpose(dim=3, axes=(2, 1, 0), is_inplace=False)

    # 4d tests
    def test_long_transpose_AXZY_out_of_place(self):
        self._test_transpose(dim=4, axes=(1, 0, 2, 3), is_inplace=False)

    def test_long_transpose_AYXZ_out_of_place(self):
        self._test_transpose(dim=4, axes=(0, 2, 1, 3), is_inplace=False)

    def test_long_transpose_AYZX_out_of_place(self):
        self._test_transpose(dim=4, axes=(2, 0, 1, 3), is_inplace=False)

    def test_long_transpose_AZXY_out_of_place(self):
        self._test_transpose(dim=4, axes=(1, 2, 0, 3), is_inplace=False)

    def test_long_transpose_AZYX_out_of_place(self):
        self._test_transpose(dim=4, axes=(2, 1, 0, 3), is_inplace=False)

    def test_long_transpose_XAYZ_out_of_place(self):
        self._test_transpose(dim=4, axes=(0, 1, 3, 2), is_inplace=False)

    def test_long_transpose_XAZY_out_of_place(self):
        self._test_transpose(dim=4, axes=(1, 0, 3, 2), is_inplace=False)

    def test_long_transpose_YAXZ_out_of_place(self):
        self._test_transpose(dim=4, axes=(0, 2, 3, 1), is_inplace=False)

    def test_long_transpose_YAZX_out_of_place(self):
        self._test_transpose(dim=4, axes=(2, 0, 3, 1), is_inplace=False)

    def test_long_transpose_ZAXY_out_of_place(self):
        self._test_transpose(dim=4, axes=(1, 2, 3, 0), is_inplace=False)

    def test_long_transpose_ZAYX_out_of_place(self):
        self._test_transpose(dim=4, axes=(2, 1, 3, 0), is_inplace=False)

    def test_long_transpose_XYAZ_out_of_place(self):
        self._test_transpose(dim=4, axes=(0, 3, 1, 2), is_inplace=False)

    def test_long_transpose_XZAY_out_of_place(self):
        self._test_transpose(dim=4, axes=(1, 3, 0, 2), is_inplace=False)

    def test_long_transpose_YXAZ_out_of_place(self):
        self._test_transpose(dim=4, axes=(0, 3, 2, 1), is_inplace=False)

    def test_long_transpose_YZAX_out_of_place(self):
        self._test_transpose(dim=4, axes=(2, 3, 0, 1), is_inplace=False)

    def test_long_transpose_ZXAY_out_of_place(self):
        self._test_transpose(dim=4, axes=(1, 3, 2, 0), is_inplace=False)

    def test_long_transpose_ZYAX_out_of_place(self):
        self._test_transpose(dim=4, axes=(2, 3, 1, 0), is_inplace=False)

    def test_long_transpose_XYZA_out_of_place(self):
        self._test_transpose(dim=4, axes=(3, 0, 1, 2), is_inplace=False)

    def test_long_transpose_XZYA_out_of_place(self):
        self._test_transpose(dim=4, axes=(3, 1, 0, 2), is_inplace=False)

    def test_long_transpose_YXZA_out_of_place(self):
        self._test_transpose(dim=4, axes=(3, 0, 2, 1), is_inplace=False)

    def test_long_transpose_YZXA_out_of_place(self):
        self._test_transpose(dim=4, axes=(3, 2, 0, 1), is_inplace=False)

    def test_long_transpose_ZXYA_out_of_place(self):
        self._test_transpose(dim=4, axes=(3, 1, 2, 0), is_inplace=False)

    def test_long_transpose_ZYXA_out_of_place(self):
        self._test_transpose(dim=4, axes=(3, 2, 1, 0), is_inplace=False)

    def test_long_transpose_5d_out_of_place(self):
        for axes in it.permutations(range(5)):
            if tuple(axes) == tuple(range(5)):
                continue
            self._test_transpose(dim=5, axes=axes, is_inplace=False)

    # ## INPLACE TRANSPOSE
    # # 2d tests
    def test_tobefixed_transpose_YX_inplace(self):
        self._test_transpose(dim=2, axes=(1, 0), is_inplace=True)

    # # 3d tests
    def test_tobefixed_transpose_XZY_inplace(self):
        self._test_transpose(dim=3, axes=(1, 0, 2), is_inplace=True)

    def test_transpose_YXZ_inplace(self):
        self._test_transpose(dim=3, axes=(0, 2, 1), is_inplace=True)

    def test_tobefixed_transpose_YZX_inplace(self):
        self._test_transpose(dim=3, axes=(2, 0, 1), is_inplace=True)

    def test_tobefixed_transpose_ZXY_inplace(self):
        self._test_transpose(dim=3, axes=(1, 2, 0), is_inplace=True)

    def test_tobefixed_transpose_ZYX_inplace(self):
        self._test_transpose(dim=3, axes=(2, 1, 0), is_inplace=True)

    # 4d tests
    def test_long_transpose_AXZY_inplace(self):
        self._test_transpose(dim=4, axes=(1, 0, 2, 3), is_inplace=True)

    def test_long_transpose_AYXZ_inplace(self):
        self._test_transpose(dim=4, axes=(0, 2, 1, 3), is_inplace=True)

    def test_long_transpose_AYZX_inplace(self):
        self._test_transpose(dim=4, axes=(2, 0, 1, 3), is_inplace=True)

    def test_long_transpose_AZXY_inplace(self):
        self._test_transpose(dim=4, axes=(1, 2, 0, 3), is_inplace=True)

    def test_long_transpose_AZYX_inplace(self):
        self._test_transpose(dim=4, axes=(2, 1, 0, 3), is_inplace=True)

    def test_long_transpose_XAYZ_inplace(self):
        self._test_transpose(dim=4, axes=(0, 1, 3, 2), is_inplace=True)

    def test_long_transpose_XAZY_inplace(self):
        self._test_transpose(dim=4, axes=(1, 0, 3, 2), is_inplace=True)

    def test_long_transpose_YAXZ_inplace(self):
        self._test_transpose(dim=4, axes=(0, 2, 3, 1), is_inplace=True)

    def test_long_transpose_YAZX_inplace(self):
        self._test_transpose(dim=4, axes=(2, 0, 3, 1), is_inplace=True)

    def test_long_transpose_ZAXY_inplace(self):
        self._test_transpose(dim=4, axes=(1, 2, 3, 0), is_inplace=True)

    def test_long_transpose_ZAYX_inplace(self):
        self._test_transpose(dim=4, axes=(2, 1, 3, 0), is_inplace=True)

    def test_long_transpose_XYAZ_inplace(self):
        self._test_transpose(dim=4, axes=(0, 3, 1, 2), is_inplace=True)

    def test_long_transpose_XZAY_inplace(self):
        self._test_transpose(dim=4, axes=(1, 3, 0, 2), is_inplace=True)

    def test_long_transpose_YXAZ_inplace(self):
        self._test_transpose(dim=4, axes=(0, 3, 2, 1), is_inplace=True)

    def test_long_transpose_YZAX_inplace(self):
        self._test_transpose(dim=4, axes=(2, 3, 0, 1), is_inplace=True)

    def test_long_transpose_ZXAY_inplace(self):
        self._test_transpose(dim=4, axes=(1, 3, 2, 0), is_inplace=True)

    def test_long_transpose_ZYAX_inplace(self):
        self._test_transpose(dim=4, axes=(2, 3, 1, 0), is_inplace=True)

    def test_long_transpose_XYZA_inplace(self):
        self._test_transpose(dim=4, axes=(3, 0, 1, 2), is_inplace=True)

    def test_long_transpose_XZYA_inplace(self):
        self._test_transpose(dim=4, axes=(3, 1, 0, 2), is_inplace=True)

    def test_long_transpose_YXZA_inplace(self):
        self._test_transpose(dim=4, axes=(3, 0, 2, 1), is_inplace=True)

    def test_long_transpose_YZXA_inplace(self):
        self._test_transpose(dim=4, axes=(3, 2, 0, 1), is_inplace=True)

    def test_long_transpose_ZXYA_inplace(self):
        self._test_transpose(dim=4, axes=(3, 1, 2, 0), is_inplace=True)

    def test_long_transpose_ZYXA_inplace(self):
        self._test_transpose(dim=4, axes=(3, 2, 1, 0), is_inplace=True)

    # 5d tests
    def test_long_transpose_5d_inplace(self):
        for axes in it.permutations(range(5)):
            if tuple(axes) == tuple(range(5)):
                continue
            self._test_transpose(dim=5, axes=axes, is_inplace=True)

    def _test_transpose(self, dim, axes, is_inplace):
        check_instance(axes, tuple, values=int)
        assert dim > 1
        if self.enable_extra_tests:
            vectorizations = [1]  # ,2,4,8,16]
            dtypes = [np.float32, np.int32]
            tile_sizes = [4, 11]
            tile_paddings = [0, 1]
        else:
            vectorizations = [2]
            dtypes = [np.float32]
            tile_sizes = [4]
            tile_paddings = [0]

        for dtype in dtypes:
            self._alloc_cpu_gpu(dtype=dtype, dim=dim, is_inplace=is_inplace)
            self._do_transpose_cpu(axes=axes, is_inplace=is_inplace)
            for tile_size in tile_sizes:
                for tile_padding in tile_paddings:
                    for vectorization in vectorizations:
                        self._do_compute_gpu_and_check(
                            dim=dim,
                            axes=axes,
                            tile_size=tile_size,
                            tile_padding=tile_padding,
                            vectorization=vectorization,
                            is_inplace=is_inplace,
                        )


if __name__ == "__main__":
    TestTranspose.setup_class(
        enable_extra_tests=False,
        enable_error_plots=True,
        enable_debug_mode=False,
        enable_interactive_debug=False,
    )

    test = TestTranspose()

    enable_out_of_place = True
    enable_inplace = True

    with printoptions(linewidth=200, formatter={"float": lambda x: f"{x:0.2f}"}):

        if enable_out_of_place:
            ## 2d transpose
            test.test_tobefixed_transpose_YX_out_of_place()

            ## 3d transpose
            test.test_tobefixed_transpose_XZY_out_of_place()
            test.test_tobefixed_transpose_YXZ_out_of_place()
            test.test_tobefixed_transpose_YZX_out_of_place()
            test.test_tobefixed_transpose_ZXY_out_of_place()
            test.test_tobefixed_transpose_ZYX_out_of_place()

            if __ENABLE_LONG_TESTS__:
                ## 4d transpose
                test.test_long_transpose_AXZY_out_of_place()
                test.test_long_transpose_AYXZ_out_of_place()
                test.test_long_transpose_AYZX_out_of_place()
                test.test_long_transpose_AZXY_out_of_place()
                test.test_long_transpose_AZYX_out_of_place()
                test.test_long_transpose_XAYZ_out_of_place()
                test.test_long_transpose_XAZY_out_of_place()
                test.test_long_transpose_YAXZ_out_of_place()
                test.test_long_transpose_YAZX_out_of_place()
                test.test_long_transpose_ZAXY_out_of_place()
                test.test_long_transpose_ZAYX_out_of_place()
                test.test_long_transpose_XYAZ_out_of_place()
                test.test_long_transpose_XZAY_out_of_place()
                test.test_long_transpose_YXAZ_out_of_place()
                test.test_long_transpose_YZAX_out_of_place()
                test.test_long_transpose_ZXAY_out_of_place()
                test.test_long_transpose_ZYAX_out_of_place()
                test.test_long_transpose_XYZA_out_of_place()
                test.test_long_transpose_XZYA_out_of_place()
                test.test_long_transpose_YXZA_out_of_place()
                test.test_long_transpose_YZXA_out_of_place()
                test.test_long_transpose_ZXYA_out_of_place()
                test.test_long_transpose_ZYXA_out_of_place()

                ## 5d transpose
                test.test_long_transpose_5d_out_of_place()

        if enable_inplace:
            # only C(dim,k) permutations (ie transpositions) are currently
            # supported per dimension for inplace kernels.
            # In addition the permutated slice has to be a square matrix.
            # See https://en.wikipedia.org/wiki/In-place_matrix_transposition

            ## 2d transpose
            test.test_tobefixed_transpose_YX_inplace()

            ## 3d transpose
            test.test_tobefixed_transpose_XZY_inplace()
            test.test_transpose_YXZ_inplace()
            test.test_tobefixed_transpose_YZX_inplace()
            test.test_tobefixed_transpose_ZXY_inplace()
            test.test_tobefixed_transpose_ZYX_inplace()

            if __ENABLE_LONG_TESTS__:
                ## 4d transpose
                test.test_long_transpose_AXZY_inplace()
                test.test_long_transpose_AYXZ_inplace()
                test.test_long_transpose_AYZX_inplace()
                test.test_long_transpose_AZXY_inplace()
                test.test_long_transpose_AZYX_inplace()
                test.test_long_transpose_XAYZ_inplace()
                test.test_long_transpose_XAZY_inplace()
                test.test_long_transpose_YAXZ_inplace()
                test.test_long_transpose_YAZX_inplace()
                test.test_long_transpose_ZAXY_inplace()
                test.test_long_transpose_ZAYX_inplace()
                test.test_long_transpose_XYAZ_inplace()
                test.test_long_transpose_XZAY_inplace()
                test.test_long_transpose_YXAZ_inplace()
                test.test_long_transpose_YZAX_inplace()
                test.test_long_transpose_ZXAY_inplace()
                test.test_long_transpose_ZYAX_inplace()
                test.test_long_transpose_XYZA_inplace()
                test.test_long_transpose_XZYA_inplace()
                test.test_long_transpose_YXZA_inplace()
                test.test_long_transpose_YZXA_inplace()
                test.test_long_transpose_ZXYA_inplace()
                test.test_long_transpose_ZYXA_inplace()

                ## 5d transpose
                test.test_long_transpose_5d_inplace()

    TestTranspose.teardown_class()
