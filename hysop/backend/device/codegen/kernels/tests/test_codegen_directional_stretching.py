# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import copy

from hysop import __ENABLE_LONG_TESTS__
from hysop.backend.device.codegen.base.test import _test_mesh_info, _test_typegen
from hysop.backend.device.codegen.kernels.directional_stretching import (
    DirectionalStretchingKernel,
)
from hysop.backend.device.opencl import cl
from hysop.constants import BoundaryCondition, np
from hysop.methods import StretchingFormulation
from hysop.numerics.odesolvers.runge_kutta import ExplicitRungeKutta
from hysop.tools.htypes import check_instance


class TestDirectionalStretching:

    @classmethod
    def setup_class(
        cls, do_extra_tests=__ENABLE_LONG_TESTS__, enable_error_plots=False
    ):
        typegen = _test_typegen("double", "dec")
        dtype = np.float64

        queue = cl.CommandQueue(typegen.context)
        ctx = typegen.context

        grid_size = np.asarray([50, 10, 10])
        compute_grid_ghosts = np.asarray([3 * 4, 0, 0])
        compute_grid_size = grid_size + 2 * compute_grid_ghosts

        (A, grid_mesh_info) = _test_mesh_info(
            "grid_mesh_info", typegen, 3, 0, grid_size
        )
        (B, compute_grid_mesh_info) = _test_mesh_info(
            "compute_grid_mesh_info", typegen, 3, compute_grid_ghosts, compute_grid_size
        )

        grid_shape = grid_size[::-1]
        compute_grid_shape = compute_grid_size[::-1]

        assert A["inv_dx"][0] == B["inv_dx"][0]
        inv_dx = A["inv_dx"][0]

        grid_bytes = grid_size.size * typegen.FLT_BYTES[typegen.fbtype]
        compute_grid_bytes = compute_grid_size.size * typegen.FLT_BYTES[typegen.fbtype]

        mf = cl.mem_flags

        host_buffers_init = {
            "no_ghosts": {
                "ux": np.random.rand(*grid_shape).astype(dtype),
                "uy": np.random.rand(*grid_shape).astype(dtype),
                "uz": np.random.rand(*grid_shape).astype(dtype),
                "wx_in": np.random.rand(*grid_shape).astype(dtype),
                "wy_in": np.random.rand(*grid_shape).astype(dtype),
                "wz_in": np.random.rand(*grid_shape).astype(dtype),
                "wx_out": np.random.rand(*grid_shape).astype(dtype),
                "wy_out": np.random.rand(*grid_shape).astype(dtype),
                "wz_out": np.random.rand(*grid_shape).astype(dtype),
                "dbg0": -np.ones(shape=grid_shape, dtype=np.int32),
                "dbg1": -np.ones(shape=grid_shape, dtype=np.int32),
            },
            "with_ghosts": {
                "ux": np.random.rand(*compute_grid_shape).astype(dtype),
                "uy": np.random.rand(*compute_grid_shape).astype(dtype),
                "uz": np.random.rand(*compute_grid_shape).astype(dtype),
                "wx_in": np.random.rand(*compute_grid_shape).astype(dtype),
                "wy_in": np.random.rand(*compute_grid_shape).astype(dtype),
                "wz_in": np.random.rand(*compute_grid_shape).astype(dtype),
                "wx_out": np.random.rand(*compute_grid_shape).astype(dtype),
                "wy_out": np.random.rand(*compute_grid_shape).astype(dtype),
                "wz_out": np.random.rand(*compute_grid_shape).astype(dtype),
                "dbg0": -np.ones(shape=compute_grid_shape, dtype=np.int32),
                "dbg1": -np.ones(shape=compute_grid_shape, dtype=np.int32),
            },
        }

        device_buffers = {
            "no_ghosts": {
                "ux": cl.Buffer(
                    ctx,
                    flags=mf.READ_ONLY | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["no_ghosts"]["ux"],
                ),
                "uy": cl.Buffer(
                    ctx,
                    flags=mf.READ_ONLY | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["no_ghosts"]["uy"],
                ),
                "uz": cl.Buffer(
                    ctx,
                    flags=mf.READ_ONLY | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["no_ghosts"]["uz"],
                ),
                "wx_in": cl.Buffer(
                    ctx,
                    flags=mf.READ_ONLY | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["no_ghosts"]["wx_in"],
                ),
                "wy_in": cl.Buffer(
                    ctx,
                    flags=mf.READ_ONLY | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["no_ghosts"]["wy_in"],
                ),
                "wz_in": cl.Buffer(
                    ctx,
                    flags=mf.READ_ONLY | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["no_ghosts"]["wz_in"],
                ),
                "wx_out": cl.Buffer(
                    ctx,
                    flags=mf.READ_WRITE | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["no_ghosts"]["wx_in"],
                ),
                "wy_out": cl.Buffer(
                    ctx,
                    flags=mf.READ_WRITE | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["no_ghosts"]["wy_in"],
                ),
                "wz_out": cl.Buffer(
                    ctx,
                    flags=mf.READ_WRITE | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["no_ghosts"]["wz_in"],
                ),
                "dbg0": cl.Buffer(
                    ctx,
                    flags=mf.READ_WRITE | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["no_ghosts"]["dbg0"],
                ),
                "dbg1": cl.Buffer(
                    ctx,
                    flags=mf.READ_WRITE | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["no_ghosts"]["dbg1"],
                ),
            },
            "with_ghosts": {
                "ux": cl.Buffer(
                    ctx,
                    flags=mf.READ_ONLY | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["with_ghosts"]["ux"],
                ),
                "uy": cl.Buffer(
                    ctx,
                    flags=mf.READ_ONLY | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["with_ghosts"]["uy"],
                ),
                "uz": cl.Buffer(
                    ctx,
                    flags=mf.READ_ONLY | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["with_ghosts"]["uz"],
                ),
                "wx_in": cl.Buffer(
                    ctx,
                    flags=mf.READ_ONLY | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["with_ghosts"]["wx_in"],
                ),
                "wy_in": cl.Buffer(
                    ctx,
                    flags=mf.READ_ONLY | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["with_ghosts"]["wy_in"],
                ),
                "wz_in": cl.Buffer(
                    ctx,
                    flags=mf.READ_ONLY | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["with_ghosts"]["wz_in"],
                ),
                "wx_out": cl.Buffer(
                    ctx,
                    flags=mf.READ_WRITE | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["with_ghosts"]["wx_in"],
                ),
                "wy_out": cl.Buffer(
                    ctx,
                    flags=mf.READ_WRITE | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["with_ghosts"]["wy_in"],
                ),
                "wz_out": cl.Buffer(
                    ctx,
                    flags=mf.READ_WRITE | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["with_ghosts"]["wz_in"],
                ),
                "dbg0": cl.Buffer(
                    ctx,
                    flags=mf.READ_WRITE | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["with_ghosts"]["dbg0"],
                ),
                "dbg1": cl.Buffer(
                    ctx,
                    flags=mf.READ_WRITE | mf.COPY_HOST_PTR,
                    hostbuf=host_buffers_init["with_ghosts"]["dbg1"],
                ),
            },
        }

        host_buffers_reference = copy.deepcopy(host_buffers_init)
        host_buffers_gpu = copy.deepcopy(host_buffers_init)

        cls.typegen = typegen
        cls.queue = queue

        cls.grid_size = grid_size
        cls.grid_shape = grid_shape
        cls.grid_bytes = grid_bytes

        cls.grid_mesh_info = grid_mesh_info
        cls.compute_grid_mesh_info = compute_grid_mesh_info

        cls.compute_grid_ghosts = compute_grid_ghosts
        cls.compute_grid_size = compute_grid_size
        cls.compute_grid_shape = compute_grid_shape
        cls.compute_grid_bytes = compute_grid_bytes

        cls.host_buffers_init = host_buffers_init
        cls.host_buffers_reference = host_buffers_reference
        cls.host_buffers_gpu = host_buffers_gpu
        cls.device_buffers = device_buffers

        Lx = min(
            typegen.device.max_work_item_sizes[0], typegen.device.max_work_group_size
        )
        Lx = min(Lx, grid_size[0])

        cls.local_work_size = np.asarray([Lx, 1, 1])
        cls.work_load = np.asarray([1, 2, 3])
        cls.inv_dx = inv_dx
        cls.dt = dtype(0.5)

        cls.do_extra_tests = do_extra_tests
        cls.enable_error_plots = enable_error_plots

    @classmethod
    def teardown_class(cls):
        pass

    def setup_method(self, method):
        pass

    def teardown_method(self, method):
        pass

    def _do_compute_cpu(self, formulation, rk_scheme, order, direction, boundary):

        dt = self.dt
        ghosts = self.compute_grid_ghosts
        grid_size = self.grid_size

        if boundary == BoundaryCondition.PERIODIC:
            target = "no_ghosts"
        elif boundary == BoundaryCondition.NONE:
            target = "with_ghosts"
        else:
            raise ValueError()

        if order == 2:
            stencil = [-1.0 / 2, 0, +1.0 / 2]
        elif order == 4:
            stencil = [+1.0 / 12, -2.0 / 3, 0, +2.0 / 3, -1.0 / 12]
        elif order == 6:
            stencil = [
                -1.0 / 60,
                +3.0 / 20,
                -3.0 / 4,
                0,
                +3.0 / 4,
                -3.0 / 20,
                +1.0 / 60,
            ]
        else:
            raise ValueError()

        def deriv(field):
            res = np.zeros_like(field)
            for i in range(-order // 2, order // 2 + 1, 1):
                res += stencil[i + order // 2] * np.roll(field, -i, axis=2)
            return res * self.inv_dx[0]

        host_init_buffers = self.host_buffers_init[target]
        host_buffers_reference = self.host_buffers_reference[target]

        vorticity = ["wx_in", "wy_in", "wz_in"]
        vorticity_out = ["wx_out", "wy_out", "wz_out"]
        velocity = ["ux", "uy", "uz"]
        U = [host_init_buffers[name].copy() for name in velocity]
        W = [host_init_buffers[name].copy() for name in vorticity + vorticity_out]
        dUdx = [deriv(ui) for ui in U]

        if rk_scheme.name() == "Euler":
            Wc = [w.copy() for w in W]
            if formulation == StretchingFormulation.GRAD_UW:
                for i in range(3):
                    W[i] += dt * dUdx[i] * Wc[direction]
            elif formulation == StretchingFormulation.GRAD_UW_T:
                for i in range(3):
                    W[direction] += dt * dUdx[i] * Wc[i]
            elif formulation == StretchingFormulation.MIXED_GRAD_UW:
                for i in range(3):
                    W[i] += 0.5 * dt * dUdx[i] * Wc[direction]
                    W[direction] += 0.5 * dt * dUdx[i] * Wc[i]
            elif formulation == StretchingFormulation.CONSERVATIVE:
                W0 = Wc
                K0 = [deriv(ui * W0[direction]) for ui in U]
                W1 = [W0[i] + dt * K0[i] for i in range(3)]
                W = W1
            else:
                msg = f"Unknown stretching formulation scheme {formulation}."
                raise ValueError(msg)
        elif rk_scheme.name() == "RK2":
            Wc = [w.copy() for w in W]
            if formulation == StretchingFormulation.GRAD_UW:
                W0 = Wc
                K0 = [dUdx[i] * W0[direction] for i in range(3)]
                W1 = [W0[i] + 0.5 * dt * K0[i] for i in range(3)]
                K1 = [dUdx[i] * W1[direction] for i in range(3)]
                W2 = [W0[i] + 1.0 * dt * K1[i] for i in range(3)]
                W = W2
            elif formulation == StretchingFormulation.GRAD_UW_T:
                W0 = Wc
                K0 = sum(dUdx[i] * W0[i] for i in range(3))
                W1 = W0[direction] + 0.5 * dt * K0
                K1 = (
                    sum(dUdx[i] * W0[i] for i in range(3) if i != direction)
                    + W1 * dUdx[direction]
                )
                W2 = W0
                W2[direction] += dt * K1
                W = W2
            elif formulation == StretchingFormulation.MIXED_GRAD_UW:
                W0 = Wc
                K0 = [0.5 * dUdx[i] * W0[direction] for i in range(3)]
                K0[direction] += sum(0.5 * dUdx[i] * W0[i] for i in range(3))
                W1 = [W0[i] + 0.5 * dt * K0[i] for i in range(3)]
                K1 = [0.5 * dUdx[i] * W1[direction] for i in range(3)]
                K1[direction] += sum(0.5 * dUdx[i] * W1[i] for i in range(3))
                W2 = [W0[i] + 1.0 * dt * K1[i] for i in range(3)]
                W = W2
            elif formulation == StretchingFormulation.CONSERVATIVE:
                W0 = Wc
                K0 = [deriv(ui * W0[direction]) for ui in U]
                W1 = [W0[i] + 0.5 * dt * K0[i] for i in range(3)]
                K1 = [deriv(ui * W1[direction]) for ui in U]
                W2 = [W0[i] + 1.0 * dt * K1[i] for i in range(3)]
                W = W2
            else:
                msg = f"Unknown stretching formulation scheme {formulation}."
                raise ValueError(msg)
        elif rk_scheme.name() == "RK4":
            Wc = [w.copy() for w in W]
            if formulation == StretchingFormulation.GRAD_UW:
                W0 = Wc
                K0 = [dUdx[i] * W0[direction] for i in range(3)]
                W1 = [W0[i] + 0.5 * dt * K0[i] for i in range(3)]
                K1 = [dUdx[i] * W1[direction] for i in range(3)]
                W2 = [W0[i] + 0.5 * dt * K1[i] for i in range(3)]
                K2 = [dUdx[i] * W2[direction] for i in range(3)]
                W3 = [W0[i] + 1.0 * dt * K2[i] for i in range(3)]
                K3 = [dUdx[i] * W3[direction] for i in range(3)]
                K = [
                    1.0 / 6 * K0[i]
                    + 1.0 / 3 * K1[i]
                    + 1.0 / 3 * K2[i]
                    + 1.0 / 6 * K3[i]
                    for i in range(3)
                ]
                W4 = [W0[i] + dt * K[i] for i in range(3)]
                W = W4
            elif formulation == StretchingFormulation.GRAD_UW_T:
                W0 = Wc
                K0 = sum(dUdx[i] * W0[i] for i in range(3))
                W1 = W0[direction] + 0.5 * dt * K0
                K1 = (
                    sum(dUdx[i] * W0[i] for i in range(3) if i != direction)
                    + W1 * dUdx[direction]
                )
                W2 = W0[direction] + 0.5 * dt * K1
                K2 = (
                    sum(dUdx[i] * W0[i] for i in range(3) if i != direction)
                    + W2 * dUdx[direction]
                )
                W3 = W0[direction] + 1.0 * dt * K2
                K3 = (
                    sum(dUdx[i] * W0[i] for i in range(3) if i != direction)
                    + W3 * dUdx[direction]
                )
                K = 1.0 / 6 * K0 + 1.0 / 3 * K1 + 1.0 / 3 * K2 + 1.0 / 6 * K3
                W4 = W0
                W4[direction] += dt * K
                W = W4
            elif formulation == StretchingFormulation.MIXED_GRAD_UW:
                W0 = Wc
                K0 = [0.5 * dUdx[i] * W0[direction] for i in range(3)]
                K0[direction] += sum(0.5 * dUdx[i] * W0[i] for i in range(3))
                W1 = [W0[i] + 0.5 * dt * K0[i] for i in range(3)]
                K1 = [0.5 * dUdx[i] * W1[direction] for i in range(3)]
                K1[direction] += sum(0.5 * dUdx[i] * W1[i] for i in range(3))
                W2 = [W0[i] + 0.5 * dt * K1[i] for i in range(3)]
                K2 = [0.5 * dUdx[i] * W2[direction] for i in range(3)]
                K2[direction] += sum(0.5 * dUdx[i] * W2[i] for i in range(3))
                W3 = [W0[i] + 1.0 * dt * K2[i] for i in range(3)]
                K3 = [0.5 * dUdx[i] * W3[direction] for i in range(3)]
                K3[direction] += sum(0.5 * dUdx[i] * W3[i] for i in range(3))
                K = [
                    1.0 / 6 * K0[i]
                    + 1.0 / 3 * K1[i]
                    + 1.0 / 3 * K2[i]
                    + 1.0 / 6 * K3[i]
                    for i in range(3)
                ]
                W4 = [W0[i] + dt * K[i] for i in range(3)]
                W = W4
            elif formulation == StretchingFormulation.CONSERVATIVE:
                W0 = Wc
                K0 = [deriv(ui * W0[direction]) for ui in U]
                W1 = [W0[i] + 0.5 * dt * K0[i] for i in range(3)]
                K1 = [deriv(ui * W1[direction]) for ui in U]
                W2 = [W0[i] + 0.5 * dt * K1[i] for i in range(3)]
                K2 = [deriv(ui * W2[direction]) for ui in U]
                W3 = [W0[i] + 1.0 * dt * K2[i] for i in range(3)]
                K3 = [deriv(ui * W3[direction]) for ui in U]
                K = [
                    1.0 / 6 * K0[i]
                    + 1.0 / 3 * K1[i]
                    + 1.0 / 3 * K2[i]
                    + 1.0 / 6 * K3[i]
                    for i in range(3)
                ]
                W4 = [W0[i] + dt * K[i] for i in range(3)]
                W = W4
            else:
                msg = f"Unknown stretching formulation scheme {formulation}."
                raise ValueError(msg)
        else:
            msg = f"Unknown Runge-Kutta scheme {rk_scheme}."
            raise ValueError(msg)

        for i, name in enumerate(vorticity_out):
            host_buffers_reference[name] = W[i]

    def _do_compute_gpu_and_check(
        self, formulation, rk_scheme, order, direction, boundary, cached
    ):

        msg = "\nTesting {}{} with order {} and scheme {} in direction {} with {} boundaries.".format(
            "cached " if cached else "",
            str(formulation).lower(),
            order,
            rk_scheme.name(),
            direction,
            str(boundary).lower(),
        )
        print(msg)

        dt = self.dt
        work_size = self.grid_size
        work_load = self.work_load
        local_work_size = self.local_work_size
        queue = self.queue

        grid_size = self.grid_size
        grid_shape = self.grid_shape
        ghosts = self.compute_grid_ghosts

        kernel_args = [dt]
        if boundary == BoundaryCondition.PERIODIC:
            target = "no_ghosts"
            mesh_info = self.grid_mesh_info
            view = [
                slice(0, grid_size[2]),
                slice(0, grid_size[1]),
                slice(0, grid_size[0]),
            ]
        elif boundary == BoundaryCondition.NONE:
            target = "with_ghosts"
            mesh_info = self.compute_grid_mesh_info
            view = [
                slice(ghosts[2], grid_size[2] + ghosts[2]),
                slice(ghosts[1], grid_size[1] + ghosts[1]),
                slice(ghosts[0], grid_size[0] + ghosts[0]),
            ]
        else:
            raise ValueError()

        known_vars = {"vorticity_mesh_info": mesh_info, "velocity_mesh_info": mesh_info}

        host_init_buffers = self.host_buffers_init[target]
        host_buffers_reference = self.host_buffers_reference[target]
        host_buffers_gpu = self.host_buffers_gpu[target]
        device_buffers = self.device_buffers[target]

        dsk = DirectionalStretchingKernel(
            typegen=self.typegen,
            dim=3,
            ftype=self.typegen.fbtype,
            order=order,
            direction=direction,
            is_cached=cached,
            is_inplace=False,
            boundary=(boundary, boundary),
            formulation=formulation,
            time_integrator=rk_scheme,
            symbolic_mode=False,
            known_vars=known_vars,
        )

        global_work_size = dsk.get_global_size(work_size, local_work_size, work_load)
        (static_shared_bytes, dynamic_shared_bytes, total_bytes) = (
            dsk.required_workgroup_cache_size(local_work_size)
        )

        vorticity_in = ["wx_in", "wy_in", "wz_in"]
        vorticity_out = ["wx_out", "wy_out", "wz_out"]
        vorticity = ["wx_in", "wy_in", "wz_in", "wx_out", "wy_out", "wz_out"]
        velocity = ["ux", "uy", "uz"]
        debug = ["dbg0", "dbg1"]
        for varname in velocity + vorticity:
            kernel_args.append(device_buffers[varname])
        if dynamic_shared_bytes != 0:
            shared_buffer = cl.LocalMemory(dynamic_shared_bytes)
            kernel_args.append(shared_buffer)

        print("\tGenerating and compiling Kernel...")
        # dsk.edit()
        source = dsk.__str__()
        prg = cl.Program(self.typegen.context, source)
        prg.build(devices=[self.typegen.device])
        kernel = prg.all_kernels()[0]
        kernel.set_args(*kernel_args)

        print("\tCPU => GPU")
        for buf in velocity + vorticity_in + vorticity_out:
            src = host_init_buffers[buf]
            dst = device_buffers[buf]
            cl.enqueue_copy(queue, dst, src)

        print(f"\tKernel execution <<<{global_work_size},{local_work_size}>>>")
        evt = cl.enqueue_nd_range_kernel(
            queue, kernel, global_work_size.tolist(), local_work_size.tolist()
        )
        evt.wait()

        print("\tGPU => CPU")
        for buf in vorticity_out:
            src = device_buffers[buf]
            dst = host_buffers_gpu[buf]
            cl.enqueue_copy(queue, dst, src)

        print("\tSynchronize queue")
        queue.flush()
        queue.finish()

        buffers = [
            (varname, host_buffers_reference[varname], host_buffers_gpu[varname])
            for varname in vorticity_out
        ]
        self._cmp_buffers(buffers, view, dsk)

    def _cmp_buffers(self, buffers, view, dsk):
        good = True
        err_buffers = []

        for name, host, dev in buffers:
            (l1, l2, linf) = self._distances(host, dev, view)
            print(f"\t{name} -> l1={l1}  l2={l2}  linf={linf}")
            if l2 > 1e-8:
                err_buffers.append(name)
                good = False
        if not good:
            msg = f"\n[FAIL] Buffer comparisson failed for buffers {err_buffers}.\n"
            print(msg)
            # dsk.edit()
            if self.enable_error_plots:
                from matplotlib import pyplot as plt

                for name, host, dev in buffers:
                    if name in err_buffers:
                        fig, axes = plt.subplots(2, 2)

                        host = host[view]
                        dev = dev[view]

                        d = (dev - host) * (dev - host)
                        d -= np.mean(d)

                        plt.title(name)

                        axes[0][0].imshow(np.sum(d, axis=0), interpolation="nearest")
                        axes[0][1].imshow(np.sum(d, axis=1), interpolation="nearest")
                        axes[1][0].imshow(np.sum(d, axis=2), interpolation="nearest")
                        axes[1][1].imshow(
                            np.sum(d, axis=(0, 1))[np.newaxis, :],
                            interpolation="nearest",
                        )

                        fig.show()
                        break
                raw_input("Press enter to close all windows.")
            raise RuntimeError(msg)

    def _distances(self, lhs, rhs, view):
        d = rhs[tuple(view)] - lhs[tuple(view)]
        da = np.abs(d)

        l1 = np.sum(da) / d.size
        l2 = np.sqrt(np.sum(d * d)) / d.size
        linf = np.max(da)
        return (l1, l2, linf)

    def _check_kernels(self, formulation, rk_scheme):
        check_instance(formulation, StretchingFormulation)
        check_instance(rk_scheme, ExplicitRungeKutta)

        cached = [False, True]
        boundaries = [
            BoundaryCondition.PERIODIC,
        ]  # BoundaryCondition.NONE,

        if self.do_extra_tests:
            directions = [0, 1, 2]
            orders = [2, 4, 6]
        else:
            directions = [1]
            orders = [4]

        for cache in cached:
            if (formulation == StretchingFormulation.CONSERVATIVE) and not cache:
                continue
            for boundary in boundaries:
                for direction in directions:
                    for order in orders:
                        self._do_compute_cpu(
                            order=order,
                            direction=direction,
                            boundary=boundary,
                            formulation=formulation,
                            rk_scheme=rk_scheme,
                        )

                        self._do_compute_gpu_and_check(
                            order=order,
                            direction=direction,
                            boundary=boundary,
                            formulation=formulation,
                            rk_scheme=rk_scheme,
                            cached=cache,
                        )

    def test_stretching_gradUW_Euler(self):
        formulation = StretchingFormulation.GRAD_UW
        rk_scheme = ExplicitRungeKutta("Euler")
        self._check_kernels(formulation=formulation, rk_scheme=rk_scheme)

    def test_stretching_gradUW_T_Euler(self):
        formulation = StretchingFormulation.GRAD_UW_T
        rk_scheme = ExplicitRungeKutta("Euler")
        self._check_kernels(formulation=formulation, rk_scheme=rk_scheme)

    def test_stretching_mixed_gradUW_Euler(self):
        formulation = StretchingFormulation.MIXED_GRAD_UW
        rk_scheme = ExplicitRungeKutta("Euler")
        self._check_kernels(formulation=formulation, rk_scheme=rk_scheme)

    def test_stretching_conservative_Euler(self):
        formulation = StretchingFormulation.CONSERVATIVE
        rk_scheme = ExplicitRungeKutta("Euler")
        self._check_kernels(formulation=formulation, rk_scheme=rk_scheme)

    def test_stretching_gradUW_RK2(self):
        formulation = StretchingFormulation.GRAD_UW
        rk_scheme = ExplicitRungeKutta("RK2")
        self._check_kernels(formulation=formulation, rk_scheme=rk_scheme)

    def test_stretching_gradUW_T_RK2(self):
        formulation = StretchingFormulation.GRAD_UW_T
        rk_scheme = ExplicitRungeKutta("RK2")
        self._check_kernels(formulation=formulation, rk_scheme=rk_scheme)

    def test_stretching_mixed_gradUW_RK2(self):
        formulation = StretchingFormulation.MIXED_GRAD_UW
        rk_scheme = ExplicitRungeKutta("RK2")
        self._check_kernels(formulation=formulation, rk_scheme=rk_scheme)

    def test_stretching_conservative_RK2(self):
        formulation = StretchingFormulation.CONSERVATIVE
        rk_scheme = ExplicitRungeKutta("RK2")
        self._check_kernels(formulation=formulation, rk_scheme=rk_scheme)

    def test_stretching_gradUW_RK4(self):
        formulation = StretchingFormulation.GRAD_UW
        rk_scheme = ExplicitRungeKutta("RK4")
        self._check_kernels(formulation=formulation, rk_scheme=rk_scheme)

    def test_stretching_gradUW_T_RK4(self):
        formulation = StretchingFormulation.GRAD_UW_T
        rk_scheme = ExplicitRungeKutta("RK4")
        self._check_kernels(formulation=formulation, rk_scheme=rk_scheme)

    def test_stretching_mixed_gradUW_RK4(self):
        formulation = StretchingFormulation.MIXED_GRAD_UW
        rk_scheme = ExplicitRungeKutta("RK4")
        self._check_kernels(formulation=formulation, rk_scheme=rk_scheme)

    def test_stretching_conservative_RK4(self):
        formulation = StretchingFormulation.CONSERVATIVE
        rk_scheme = ExplicitRungeKutta("RK4")
        self._check_kernels(formulation=formulation, rk_scheme=rk_scheme)


if __name__ == "__main__":
    TestDirectionalStretching.setup_class(
        do_extra_tests=False, enable_error_plots=False
    )
    test = TestDirectionalStretching()

    test.test_stretching_gradUW_Euler()
    test.test_stretching_gradUW_T_Euler()
    test.test_stretching_mixed_gradUW_Euler()
    test.test_stretching_conservative_Euler()

    test.test_stretching_gradUW_RK2()
    test.test_stretching_gradUW_T_RK2()
    test.test_stretching_mixed_gradUW_RK2()
    test.test_stretching_conservative_RK2()

    test.test_stretching_gradUW_RK4()
    test.test_stretching_gradUW_T_RK4()
    test.test_stretching_mixed_gradUW_RK4()
    test.test_stretching_conservative_RK4()

    TestDirectionalStretching.teardown_class()
