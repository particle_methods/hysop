# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import copy
import math

from hysop import __ENABLE_LONG_TESTS__
from hysop.backend.device.codegen.base.test import _test_mesh_info, _test_typegen
from hysop.backend.device.codegen.kernels.directional_advection import (
    DirectionalAdvectionKernelGenerator,
)
from hysop.backend.device.opencl import cl
from hysop.constants import BoundaryCondition
from hysop.numerics.odesolvers.runge_kutta import ExplicitRungeKutta
from hysop.tools.contexts import printoptions
from hysop.tools.misc import prod, upper_pow2_or_3
from hysop.tools.numpywrappers import npw
from hysop.tools.htypes import check_instance, to_list
from hysop.tools.units import bytes2str


class TestDirectionalAdvection:

    @classmethod
    def setup_class(
        cls,
        enable_extra_tests=__ENABLE_LONG_TESTS__,
        enable_error_plots=False,
        enable_debug_logs=False,
    ):

        typegen = _test_typegen("float", "dec")
        dtype = npw.float32

        queue = cl.CommandQueue(typegen.context)
        ctx = typegen.context

        max_dim = 3

        compute_grid_size = npw.asintarray([13, 7, 23])  # X Y Z
        compute_grid_shape = compute_grid_size[::-1]

        dt = dtype(0.1)
        dx = dtype(1.0 / (compute_grid_size[0] - 1))
        inv_dx = dtype(1.0 / dx)

        umax = dtype(+1.0)
        umin = dtype(-1.0)
        uinf = dtype(1.0)

        V_min_ghosts = 1 + int(math.floor(dt * uinf / float(dx)))
        V_min_ghosts = npw.asintarray([V_min_ghosts, 0, 0])
        V_extra_ghosts = npw.asintarray([1, 2, 3])
        V_ghosts = V_min_ghosts + V_extra_ghosts
        V_grid_size = compute_grid_size + 2 * V_ghosts
        V_grid_shape = V_grid_size[::-1]
        V_grid_bytes = prod(V_grid_size) * dt.itemsize

        P_min_ghosts = npw.asintarray([0, 0, 0])
        P_extra_ghosts = npw.asintarray([3, 2, 1])
        P_ghosts = P_min_ghosts + P_extra_ghosts
        P_grid_size = compute_grid_size + 2 * P_ghosts
        P_grid_shape = P_grid_size[::-1]
        P_grid_bytes = prod(P_grid_size) * dt.itemsize

        mesh_infos = {}
        for dim in range(1, max_dim + 1):
            (A, V_mesh_info) = _test_mesh_info(
                "V_mesh_info", typegen, dim, V_ghosts[:dim], V_grid_size[:dim]
            )
            (B, P_mesh_info) = _test_mesh_info(
                "P_mesh_info", typegen, dim, P_ghosts[:dim], P_grid_size[:dim]
            )
            if dim == 1:
                assert A["dx"][0] == dx
                assert B["dx"][0] == dx
            else:
                assert A["dx"][0][0] == dx
                assert B["dx"][0][0] == dx
            mesh_infos[dim] = {"P_mesh_info": P_mesh_info, "V_mesh_info": V_mesh_info}

        print(
            """\
        Compute Grid:
            base size:  {}
        Velocity:
            min_ghosts: {}
            sup_ghosts: {}
            ghosts:     {}
            size:       {}
            bytes:      {}
        Position:
            min_ghosts: {}
            sup_ghosts: {}
            ghosts:     {}
            size:       {}
            bytes:      {}
        """.format(
                compute_grid_size,
                V_min_ghosts,
                V_extra_ghosts,
                V_ghosts,
                V_grid_size,
                bytes2str(V_grid_bytes),
                P_min_ghosts,
                P_extra_ghosts,
                P_ghosts,
                P_grid_size,
                bytes2str(P_grid_bytes),
            )
        )

        mf = cl.mem_flags

        host_buffers_init = {
            "V": umin + (umax - umin) * npw.random.rand(*V_grid_shape).astype(dtype),
            "P": -1 * npw.ones(shape=P_grid_shape, dtype=dtype),
            "dbg0": -1 * npw.ones(shape=compute_grid_shape, dtype=npw.int32),
            "dbg1": -1 * npw.ones(shape=compute_grid_shape, dtype=npw.int32),
        }

        device_buffers = {
            "V": cl.Buffer(
                ctx,
                flags=mf.READ_ONLY | mf.COPY_HOST_PTR,
                hostbuf=host_buffers_init["V"],
            ),
            "P": cl.Buffer(
                ctx,
                flags=mf.WRITE_ONLY | mf.COPY_HOST_PTR,
                hostbuf=host_buffers_init["P"],
            ),
            "dbg0": cl.Buffer(
                ctx,
                flags=mf.READ_WRITE | mf.COPY_HOST_PTR,
                hostbuf=host_buffers_init["dbg0"],
            ),
            "dbg1": cl.Buffer(
                ctx,
                flags=mf.READ_WRITE | mf.COPY_HOST_PTR,
                hostbuf=host_buffers_init["dbg1"],
            ),
        }

        host_buffers_reference = copy.deepcopy(host_buffers_init)
        host_buffers_gpu = copy.deepcopy(host_buffers_init)

        Lx = min(
            typegen.device.max_work_item_sizes[0], typegen.device.max_work_group_size
        )
        Lx = min(Lx, compute_grid_size[0])

        local_work_size = npw.asarray([Lx, 1, 1])
        work_load = npw.asarray([1, 1, 1])

        cls.typegen = typegen
        cls.queue = queue
        cls.max_dim = max_dim

        cls.compute_grid_size = compute_grid_size
        cls.compute_grid_shape = compute_grid_shape

        cls.V_min_ghosts = V_min_ghosts
        cls.V_extra_ghosts = V_extra_ghosts
        cls.V_ghosts = V_ghosts
        cls.V_grid_size = V_grid_size
        cls.V_grid_shape = V_grid_shape

        cls.P_min_ghosts = P_min_ghosts
        cls.P_extra_ghosts = P_extra_ghosts
        cls.P_ghosts = P_ghosts
        cls.P_grid_size = P_grid_size
        cls.P_grid_shape = P_grid_shape

        cls.mesh_infos = mesh_infos

        cls.host_buffers_init = host_buffers_init
        cls.host_buffers_reference = host_buffers_reference
        cls.host_buffers_gpu = host_buffers_gpu
        cls.device_buffers = device_buffers

        cls.local_work_size = local_work_size
        cls.work_load = work_load

        cls.dx, cls.inv_dx, cls.dt = dx, inv_dx, dt
        cls.umin, cls.umax, cls.uinf = umin, umax, uinf

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_error_plots = enable_error_plots
        cls.enable_debug_logs = enable_debug_logs

    def P_view(self, dim):
        compute_grid_size = self.compute_grid_size
        P_min_ghosts, P_extra_ghosts, P_ghosts = (
            self.P_min_ghosts,
            self.P_extra_ghosts,
            self.P_ghosts,
        )
        field_view = []  # view of the computational grid with ghosts in the full grid
        cg_view = (
            []
        )  # view of the computational grid without ghosts in the field_view subgrid
        for i in range(self.max_dim):
            j = self.max_dim - i - 1
            if j < dim:
                field_sl = slice(
                    P_extra_ghosts[j] - P_min_ghosts[j],
                    compute_grid_size[j] + P_min_ghosts[j] + P_extra_ghosts[j],
                )
                cg_sl = slice(P_min_ghosts[j], compute_grid_size[j] + P_min_ghosts[j])
                field_view.append(field_sl)
                cg_view.append(cg_sl)
            else:
                field_view.append(P_ghosts[j])
        return tuple(field_view), tuple(cg_view)

    def V_view(self, dim):
        compute_grid_size = self.compute_grid_size
        V_min_ghosts, V_extra_ghosts, V_ghosts = (
            self.V_min_ghosts,
            self.V_extra_ghosts,
            self.V_ghosts,
        )
        field_view = []  # view of the computational grid with ghosts in the full grid
        cg_view = []  # view of the computational grid without ghosts in the field_view
        # subgrid
        for i in range(self.max_dim):
            j = self.max_dim - i - 1
            if j < dim:
                field_sl = slice(
                    V_ghosts[j] - V_min_ghosts[j],
                    compute_grid_size[j] + V_min_ghosts[j] + V_ghosts[j],
                )
                cg_sl = slice(V_min_ghosts[j], compute_grid_size[j] + V_min_ghosts[j])
                field_view.append(field_sl)
                cg_view.append(cg_sl)
            else:
                field_view.append(V_ghosts[j])
        return tuple(field_view), tuple(cg_view)

    @classmethod
    def teardown_class(cls):
        pass

    def setup_method(self, method):
        pass

    def teardown_method(self, method):
        pass

    def _do_compute_cpu(self, rk_scheme, boundary):

        dt = self.dt
        dx = self.dx
        inv_dx = self.inv_dx
        P_ghosts = self.P_ghosts
        V_ghosts = self.V_ghosts
        compute_grid_size = self.compute_grid_size
        compute_grid_shape = self.compute_grid_shape
        enable_debug_logs = self.enable_debug_logs

        P_field_view, P_view_cg = self.P_view(dim=3)
        V_field_view, V_view_cg = self.V_view(dim=3)

        host_init_buffers = self.host_buffers_init
        host_buffers_reference = self.host_buffers_reference

        velocity = host_init_buffers["V"][V_field_view]
        position = host_init_buffers["P"][P_field_view]

        pos = npw.zeros_like(position)
        pos[...] = npw.arange(compute_grid_size[0])[None, None, :]
        pos *= dx
        if enable_debug_logs:
            print("TIME STEP (DT):")
            print(dt)
            print("INITIAL REFERENCE COMPUTE POSITION LINE:")
            print(pos[0, 0])
            print("COMPUTE REFERENCE GRID VELOCITY:")
            print(velocity[V_view_cg][0, 0])

        is_periodic = False

        def interp_velocity(X):
            Gx = compute_grid_size[0]
            X = X.copy() * inv_dx
            lidx = npw.floor(X).astype(npw.int32)
            alpha = X - lidx
            if is_periodic:
                lidx = (lidx + Gx) % Gx
                ridx = (lidx + 1) % Gx
            else:
                lidx += self.V_min_ghosts[0]
                ridx = lidx + 1

            Vl = npw.empty_like(X)
            Vr = npw.empty_like(X)
            for i in range(*V_view_cg[0].indices(velocity.shape[0])):
                for j in range(*V_view_cg[1].indices(velocity.shape[1])):
                    Vl[i, j, :] = velocity[i, j, lidx[i, j, :]]
                    Vr[i, j, :] = velocity[i, j, ridx[i, j, :]]
            return Vl + alpha * (Vr - Vl)

        if rk_scheme.name() == "Euler":
            pos += velocity[V_view_cg] * dt
        elif rk_scheme.name() == "RK2":
            X0 = pos.copy()
            K0 = velocity[V_view_cg]
            X1 = X0 + 0.5 * K0 * dt
            K1 = interp_velocity(X1)
            K = K1
            pos = X0 + K * dt
        elif rk_scheme.name() == "RK4":
            X0 = pos.copy()
            K0 = velocity[V_view_cg]
            X1 = X0 + 0.5 * K0 * dt
            K1 = interp_velocity(X1)
            X2 = X0 + 0.5 * K1 * dt
            K2 = interp_velocity(X2)
            X3 = X0 + 1.0 * K2 * dt
            K3 = interp_velocity(X3)
            K = 1.0 / 6 * K0 + 2.0 / 6 * K1 + 2.0 / 6 * K2 + 1.0 / 6 * K3
            pos = X0 + K * dt
        else:
            msg = f"Unknown Runge-Kutta scheme {rk_scheme}."
            raise ValueError(msg)

        if enable_debug_logs:
            print("FINAL REFERENCE COMPUTE POSITION LINE")
            print(pos[0, 0])

        host_buffers_reference["P"][P_field_view] = pos

    def _do_compute_gpu_and_check(
        self, rk_scheme, boundary, cached, nparticles, work_dim
    ):

        msg = "\nTesting {}directional {}d advection with {} scheme and {} boundaries, "
        msg += "{} particles at a time."
        msg = msg.format(
            "cached " if cached else "",
            work_dim,
            rk_scheme.name(),
            str(boundary).lower(),
            nparticles,
        )
        print(msg)
        grid_size = self.compute_grid_size[:work_dim]
        grid_shape = self.compute_grid_shape[:work_dim]

        work_size = self.compute_grid_size[:work_dim]
        work_load = self.work_load[:work_dim]
        local_work_size = self.local_work_size[:work_dim]
        lwsl = work_load * local_work_size
        global_work_size = ((work_size + lwsl - 1) // lwsl) * lwsl

        typegen = self.typegen
        ndim = upper_pow2_or_3(work_dim)
        strides_dtype = typegen.uintn(ndim)
        offset_dtype = npw.uint64

        def compute_offset(base, slc, work_dim):
            view = base[slc]
            start = []
            for slci, sh in zip(slc, base.shape):
                if isinstance(slci, slice):
                    start.append(slci.indices(sh)[0])
                else:
                    start.append(slci)
            start_offset = npw.sum(
                npw.asarray(start[:-work_dim]) * base.strides[:-work_dim]
            )
            return start_offset

        def compute_strides(base, slc, work_dim):
            view = base[slc]
            strides = view.strides
            return strides

        def make_offset(offset, dtype):
            """Build an offset in number of elements instead of bytes."""
            msg = "Unaligned offset {} for dtype {} (itemsize={}).".format(
                offset, dtype, dtype.itemsize
            )
            assert (offset % dtype.itemsize) == 0
            return offset_dtype(offset // dtype.itemsize)

        def make_strides(bstrides, dtype):
            msg = "Invalid strides {} for dtype {} (itemsize={}).".format(
                bstrides, dtype.__class__.__name__, dtype.itemsize
            )
            assert (npw.mod(bstrides, dtype.itemsize) == 0).all(), msg
            data = typegen.make_uintn(
                vals=tuple(x // dtype.itemsize for x in bstrides[::-1]), n=ndim, dval=0
            )
            return to_list(data)[:ndim]

        host_init_buffers = self.host_buffers_init
        host_buffers_reference = self.host_buffers_reference
        host_buffers_gpu = self.host_buffers_gpu
        device_buffers = self.device_buffers

        P_field_view, P_view_cg = self.P_view(dim=work_dim)
        V_field_view, V_view_cg = self.V_view(dim=work_dim)

        P_host, V_host = host_init_buffers["P"], host_init_buffers["V"]
        P_device, V_device = device_buffers["P"], device_buffers["V"]
        P_strides = compute_strides(P_host, P_field_view, work_dim)
        V_strides = compute_strides(V_host, V_field_view, work_dim)
        P_offset = compute_offset(P_host, P_field_view, work_dim)
        V_offset = compute_offset(V_host, V_field_view, work_dim)

        known_vars = {"global_size": global_work_size, "local_size": local_work_size}
        known_vars.update(self.mesh_infos[work_dim])
        known_vars["V_strides"] = make_strides(V_strides, V_host.dtype)
        known_vars["V_offset"] = make_offset(V_offset, V_host.dtype)
        known_vars["P_strides"] = make_strides(P_strides, P_host.dtype)
        known_vars["P_offset"] = make_offset(P_offset, P_host.dtype)

        min_ghosts = self.V_min_ghosts[0]

        dak = DirectionalAdvectionKernelGenerator(
            typegen=self.typegen,
            ftype=self.typegen.fbtype,
            work_dim=work_dim,
            rk_scheme=rk_scheme,
            is_cached=cached,
            vboundary=(boundary, boundary),
            nparticles=nparticles,
            min_ghosts=min_ghosts,
            relative_velocity=0.0,
            symbolic_mode=False,
            debug_mode=False,
            known_vars=known_vars,
        )
        # dak.edit()

        (static_shared_bytes, dynamic_shared_bytes, total_bytes) = (
            dak.required_workgroup_cache_size(local_work_size)
        )

        queue = self.queue
        kernel_args = [self.dt]
        kernel_args.append(device_buffers["V"])
        kernel_args.append(device_buffers["P"])
        if dynamic_shared_bytes != 0:
            shared_buffer = cl.LocalMemory(dynamic_shared_bytes)
            kernel_args.append(shared_buffer)

        print("\tGenerating and compiling Kernel...")
        source = dak.__str__()
        prg = cl.Program(self.typegen.context, source)
        prg.build(devices=[self.typegen.device])
        kernel = prg.all_kernels()[0]
        kernel.set_args(*kernel_args)

        print("\tCPU => GPU")
        variables = (
            "P",
            "V",
        )
        views = (
            P_field_view,
            V_field_view,
        )
        for buf in variables:
            src = host_init_buffers[buf]
            dst = device_buffers[buf]
            cl.enqueue_copy(queue, dst, src)

        print(f"\tKernel execution <<<{global_work_size},{local_work_size}>>>")
        evt = cl.enqueue_nd_range_kernel(
            queue, kernel, global_work_size.tolist(), local_work_size.tolist()
        )
        evt.wait()

        print("\tGPU => CPU")
        for buf in variables:
            src = device_buffers[buf]
            dst = host_buffers_gpu[buf]
            cl.enqueue_copy(queue, dst, src)

        print("\tSynchronize queue")
        queue.flush()
        queue.finish()

        buffers = [
            (varname, host_buffers_reference[varname], host_buffers_gpu[varname], view)
            for varname, view in zip(variables, views)
        ]
        self._cmp_buffers(buffers, dak, work_dim)

    def _cmp_buffers(self, buffers, dak, work_dim):
        good = True
        err_buffers = []

        for name, host, dev, view in buffers:
            (l1, l2, linf) = self._distances(host, dev, view)
            print(f"\t{name} -> l1={l1}  l2={l2}  linf={linf}")
            if linf > 100 * npw.finfo(host.dtype).eps:
                err_buffers.append(name)
                good = False
        if not good:
            msg = f"\n[FAIL] Buffer comparisson failed for buffers {err_buffers}.\n"
            print(msg)
            dak.edit()
            if self.enable_error_plots:
                from matplotlib import pyplot as plt

                for name, host, dev, view in buffers:
                    if name in err_buffers:
                        host = host[view]
                        dev = dev[view]

                        d = (dev - host) * (dev - host)
                        d -= npw.mean(d)

                        if work_dim == 3:
                            fig, axes = plt.subplots(2, 2)
                            axes[0][0].imshow(
                                npw.sum(d, axis=0), interpolation="nearest"
                            )
                            axes[0][1].imshow(
                                npw.sum(d, axis=1), interpolation="nearest"
                            )
                            axes[1][0].imshow(
                                npw.sum(d, axis=2), interpolation="nearest"
                            )
                            axes[1][1].imshow(
                                npw.sum(d, axis=(0, 1))[npw.newaxis, :],
                                interpolation="nearest",
                            )
                            plt.title(name)
                            fig.show()
                            raw_input("Press enter to close all windows.")
                            break
                        elif work_dim == 2:
                            fig, axe = plt.subplots()
                            axe.imshow(d, interpolation="nearest")
                            plt.title(name)
                            fig.show()
                            raw_input("Press enter to close all windows.")
                            break
                        else:
                            pass
            else:
                for name, host, dev, view in buffers:
                    if name in err_buffers:
                        host = host[view]
                        dev = dev[view]

                        d = npw.sqrt((dev - host) * (dev - host))
                        print(f"== {name} HOST ==")
                        print(host)
                        print(f"== {name} DEVICE ==")
                        print(dev)

            raise RuntimeError(msg)

    def _distances(self, lhs, rhs, view):
        d = rhs[view] - lhs[view]
        da = npw.abs(d)

        l1 = npw.sum(da) / d.size
        l2 = npw.sqrt(npw.sum(d * d)) / d.size
        linf = npw.max(da)
        return (l1, l2, linf)

    def _check_kernels(self, rk_scheme):
        check_instance(rk_scheme, ExplicitRungeKutta)

        boundaries = (BoundaryCondition.NONE,)
        cached = (False, True)

        if self.enable_extra_tests:
            nparticles = (
                1,
                2,
                4,
                8,
                16,
            )
            work_dims = (
                1,
                2,
                3,
            )
        else:
            nparticles = (1, 2)
            work_dims = (1, 2, 3)

        with printoptions(
            threshold=10000,
            linewidth=240,
            nanstr="nan",
            infstr="inf",
            formatter={"float": lambda x: f"{x:>6.2f}"},
        ):
            for boundary in boundaries:
                self._do_compute_cpu(boundary=boundary, rk_scheme=rk_scheme)
                for work_dim in work_dims:
                    for cache in cached:
                        for nparticle in nparticles:
                            self._do_compute_gpu_and_check(
                                boundary=boundary,
                                rk_scheme=rk_scheme,
                                work_dim=work_dim,
                                nparticles=nparticle,
                                cached=cache,
                            )

    def test_advection_Euler(self):
        rk_scheme = ExplicitRungeKutta("Euler")
        self._check_kernels(rk_scheme=rk_scheme)

    def test_advection_RK2(self):
        rk_scheme = ExplicitRungeKutta("RK2")
        self._check_kernels(rk_scheme=rk_scheme)

    def test_advection_RK4(self):
        rk_scheme = ExplicitRungeKutta("RK4")
        self._check_kernels(rk_scheme=rk_scheme)


if __name__ == "__main__":
    TestDirectionalAdvection.setup_class(
        enable_extra_tests=False, enable_error_plots=False, enable_debug_logs=False
    )
    test = TestDirectionalAdvection()

    test.test_advection_Euler()
    test.test_advection_RK2()
    test.test_advection_RK4()

    TestDirectionalAdvection.teardown_class()
