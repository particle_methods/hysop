# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import contextlib, random
from contextlib import contextmanager

import operator, hashlib

from hysop import __VERBOSE__, __KERNEL_DEBUG__

from hysop.tools.misc import Utils
from hysop.constants import np

from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.codegen.base.kernel_codegen import KernelCodeGenerator
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
)
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.codegen.base.utils import WriteOnceDict, ArgDict

from hysop.backend.device.opencl import cl, clCharacterize
from hysop.backend.device.opencl.opencl_env import OpenClEnvironment
from hysop.backend.device.opencl.opencl_kernel import OpenClKernelLauncher
from hysop.backend.device.kernel_autotuner import KernelAutotuner
from hysop.backend.device.kernel_autotuner_config import KernelAutotunerConfig


class BandwidthKernel(KernelCodeGenerator):

    @staticmethod
    def codegen_name(vtype, nreads, nwrites):
        vtype = vtype.replace(" ", "_")
        name = f"bandwidth_{vtype}_{nreads}_{nwrites}"
        return name

    def __init__(self, typegen, vtype, nreads, nwrites, known_vars=None):

        assert nreads > 0 and nwrites > 0

        kernel_args = self.gen_kernel_arguments(typegen, vtype)

        name = BandwidthKernel.codegen_name(vtype, nreads, nwrites)

        super().__init__(
            name=name,
            typegen=typegen,
            work_dim=1,
            kernel_args=kernel_args,
            known_vars=known_vars,
        )

        self.vtype = vtype
        self.nreads = nreads
        self.nwrites = nwrites
        self.gencode()

    def gen_kernel_arguments(self, typegen, vtype):
        kargs = ArgDict()
        kargs["dst"] = CodegenVariable(
            ctype=vtype,
            name="dst",
            ptr=True,
            typegen=typegen,
            nl=True,
            ptr_restrict=True,
            ptr_volatile=True,
            storage="__global",
        )
        kargs["src"] = CodegenVariable(
            ctype=vtype,
            name="src",
            ptr=True,
            typegen=typegen,
            nl=True,
            ptr_restrict=True,
            ptr_volatile=True,
            storage="__global",
            const=True,
        )
        return kargs

    def gencode(self):
        s = self
        vtype = s.vtype

        global_id = s.vars["global_id"]
        global_size = s.vars["global_size"]

        dst = s.vars["dst"]
        src = s.vars["src"]

        buf = CodegenVariable(ctype=vtype, name="buffer", typegen=self.typegen, init=0)

        with s._kernel_():
            global_size.declare(s, const=True)
            global_id.declare(s, const=True)
            s.jumpline()

            buf.declare(s)
            for i in range(self.nreads):
                offset = f"{global_id()}+{i}uL*{global_size()}"
                code = "{} {}= {};".format(buf(), "+*-/"[i % 4], src[offset])
                s.append(code)

            for i in range(self.nwrites):
                offset = f"{global_id()}+{i}uL*{global_size()}"
                code = f"{dst[offset]} = {buf()}+{i};"
                s.append(code)


if __name__ == "__main__":
    from hysop.backend.device.codegen.base.test import _test_typegen

    tg = _test_typegen("float")
    vtype = "float4"

    ck = BandwidthKernel(
        tg,
        vtype,
        nreads=10,
        nwrites=1,
        known_vars={"global_size": 1024, "local_size": 512},
    )
    ck.edit()
