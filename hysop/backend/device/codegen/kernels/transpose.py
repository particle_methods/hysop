# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import operator
from contextlib import contextmanager

import numpy as np
from hysop.backend.device.codegen.base.kernel_codegen import KernelCodeGenerator
from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.codegen.base.utils import ArgDict, WriteOnceDict
from hysop.backend.device.codegen.base.variables import (
    CodegenArray,
    CodegenVariable,
    CodegenVectorClBuiltin,
    ctype_to_dtype,
)
from hysop.backend.device.codegen.functions.compute_index import ComputeIndexFunction
from hysop.backend.device.opencl import clCharacterize
from hysop.backend.device.opencl.opencl_array_backend import OpenClArrayBackend
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.tools.contexts import nested
from hysop.tools.decorators import static_vars
from hysop.tools.misc import prod, upper_pow2, upper_pow2_or_3
from hysop.tools.numpywrappers import npw
from hysop.tools.htypes import check_instance
from hysop.tools.units import bytes2str


class TransposeKernelGenerator(KernelCodeGenerator):
    n_dbg_arrays = 2

    @staticmethod
    def codegen_name(
        is_inplace,
        axes,
        ctype,
        tile_size,
        tile_padding,
        vectorization,
        use_diagonal_coordinates,
    ):
        pdim = len(axes)
        axes = [str(j) if i != j else "X" for i, j in enumerate(axes)]
        return "transpose{}_{}_{}_{}d__N{}__T{}__P{}__{}".format(
            "_dc" if use_diagonal_coordinates else "_nc",
            "inplace" if is_inplace else "out_of_place",
            ctype.replace(" ", "_"),
            pdim,
            vectorization,
            tile_size,
            tile_padding,
            "_".join(axes),
        )

    @classmethod
    def characterize_permutation(cls, shape, axes, max_device_workdim):
        pdim = len(axes)
        assert pdim >= 2
        assert set(axes) == set(range(pdim))
        contiguous_permutation = axes[-1] != (pdim - 1)
        if contiguous_permutation:
            tile_indexes = (pdim - 1, axes[-1])
        else:
            tile_indexes = (pdim - 1,)
        tile_indexes = list(pdim - 1 - idx for idx in tile_indexes)
        wdim = min(len(axes), max_device_workdim)

        extra_work_indexes = []

        work_shape = np.empty(shape=(wdim,), dtype=np.int32)
        j = 0
        for i, Si in enumerate(shape):
            if i == 0:
                work_shape[0] = Si
            elif i in tile_indexes:
                work_shape[j] = Si
            elif i < (
                wdim - int(contiguous_permutation and tile_indexes[1] > wdim - 1)
            ):
                work_shape[j] = Si
            else:
                continue
            j += 1
        return (contiguous_permutation, wdim, work_shape, tile_indexes)

    @classmethod
    def max_local_worksize(cls, shape, work_dim, tile_size, vectorization, axes):
        pdim = len(axes)
        contiguous_permutation = axes[-1] != (pdim - 1)
        if contiguous_permutation:
            tile_indexes = (pdim - 1, axes[-1])
        else:
            tile_indexes = (pdim - 1,)
        tile_indexes = tuple(pdim - 1 - idx for idx in tile_indexes)

        assert work_dim <= pdim, "workdim to big."
        assert work_dim >= (1 + int(contiguous_permutation)), "workdim to small."

        wdim = work_dim
        max_local_worksize = np.empty(shape=(wdim,), dtype=np.int32)
        j = 0
        for i, Si in enumerate(shape):
            if i == 0:
                max_local_worksize[j] = (tile_size + vectorization - 1) // vectorization
            elif i in tile_indexes:
                max_local_worksize[j] = tile_size
            elif i < (
                wdim - int(contiguous_permutation and tile_indexes[1] > wdim - 1)
            ):
                max_local_worksize[j] = 1
            else:
                continue
            j += 1
        assert j == wdim, f"{j} != {wdim}"
        return max_local_worksize

    @classmethod
    def compute_global_size(
        cls, shape, tile_size, vectorization, axes, local_work_size, work_load
    ):

        pdim = len(axes)
        contiguous_permutation = axes[-1] != (pdim - 1)
        if contiguous_permutation:
            tile_indexes = (pdim - 1, axes[-1])
        else:
            tile_indexes = (pdim - 1,)
        tile_indexes = tuple(pdim - 1 - idx for idx in tile_indexes)

        wdim = len(local_work_size)
        assert wdim <= pdim, "workdim to big."
        assert wdim >= (1 + int(contiguous_permutation)), "workdim to small."

        ngroups = np.empty(shape=(wdim,), dtype=np.int32)
        vts = tile_size * vectorization
        ts = tile_size
        j = 0
        for i, Si in enumerate(shape):
            if i == 0:
                wl = work_load[j]
                ngroups[j] = (Si + vts * wl - 1) // (vts * wl)
            elif i in tile_indexes:
                wl = work_load[j]
                ngroups[j] = (Si + ts * wl - 1) // (ts * wl)
            elif i < (
                wdim - int(contiguous_permutation and tile_indexes[1] > wdim - 1)
            ):
                wl = work_load[j]
                ngroups[j] = (Si + wl - 1) // wl
            else:
                continue
            j += 1
        assert j == wdim, f"{j} != {wdim}"
        global_size = ngroups * local_work_size
        return global_size

    def required_workgroup_cache_size(self):
        """
        Return a tuple of required (static,dynamic,total) cache bytes per workgroup
        """
        dtype = self.dtype
        nbytes = np.dtype(dtype).itemsize

        tile_shape = self.tile_shape
        tile_bytes = prod(tile_shape) * nbytes

        if self.contiguous_permutation:
            count = tile_bytes
            if self.is_inplace:
                count *= 2
        else:
            count = 0

        sc = count
        dc = 0
        tc = sc + dc

        return (sc, dc, tc)

    def __init__(
        self,
        typegen,
        ctype,
        vectorization,
        axes,
        tile_size,
        tile_padding,
        symbolic_mode,
        use_diagonal_coordinates=True,
        is_inplace=False,
        known_vars=None,
        debug_mode=False,
        tuning_mode=False,
        **kargs,
    ):

        axes = np.asarray(axes)
        pdim = axes.size
        Pdim = upper_pow2_or_3(pdim)
        assert pdim <= 16, "Maximal permutation dimension is 16."
        assert Pdim in [1, 2, 3, 4, 8, 16]
        assert vectorization in [1, 2, 4, 8, 16]
        assert tile_padding >= 0

        # check permutation axes
        msg = "Invalid permutation {} for dimension {}."
        msg = msg.format(axes, pdim)
        assert axes.size == pdim, msg
        assert (axes < pdim).all(), msg
        _axes = set(axes.tolist())
        if len(_axes) != pdim:
            raise ValueError(msg)

        _permutation = axes != set(range(pdim))
        _naxes = sum(_permutation)
        if _naxes == 0:
            msg = "There is nothing to transpose with given axes {}."
            msg = msg.format(axes)
            raise ValueError(msg)
        assert _naxes >= 2, msg

        # We need the first axe for contiguous reads and writes even if it's not part
        # of the permutation scheme. If the contiguous axe is permutated, we need
        # the second axe that will become the contiguous one as well.
        # Those contiguous axes are stored in tile_indexes.
        contiguous_permutation = axes[-1] != (pdim - 1)
        if contiguous_permutation:
            tile_indexes = (pdim - 1, axes[-1])
        else:
            tile_indexes = (pdim - 1,)
        tile_axes = tuple(pdim - 1 - axes[idx] for idx in tile_indexes)
        tile_indexes = tuple(pdim - 1 - idx for idx in tile_indexes)

        permutation_axes = tuple(pdim - 1 - i for i, idx in enumerate(axes) if i != idx)

        tdim = len(tile_indexes)
        Tdim = upper_pow2_or_3(tdim)
        tile_shape = [
            tile_size,
        ] * tdim
        tile_shape[-1] += tile_padding
        tile_shape = tuple(tile_shape)

        is_tile_index = tuple((i in tile_indexes) for i in range(pdim))
        tile_index_to_id = {j: i for (i, j) in enumerate(tile_indexes)}

        device = typegen.device
        if device.max_work_item_dimensions < tdim:
            msg = "OpenCL device {} does not support {} working dimensions required "
            msg += "to transpose whith axes {}."
            msg = msg.format(device.name, tdim, axes)
        work_dim = min(pdim, device.max_work_item_dimensions)

        workload_indexes = tuple(
            i
            for i in range(pdim)
            if (not is_tile_index[i])
            and (
                i
                < (
                    work_dim
                    - int(contiguous_permutation and (tile_indexes[1] > work_dim - 1))
                )
            )
        )
        is_workload_index = tuple((i in workload_indexes) for i in range(pdim))
        wl_index_to_id = {j: i for (i, j) in enumerate(workload_indexes)}
        wldim = len(workload_indexes)
        WLdim = upper_pow2_or_3(wldim)

        name = TransposeKernelGenerator.codegen_name(
            is_inplace,
            axes,
            ctype,
            tile_size,
            tile_padding,
            vectorization,
            use_diagonal_coordinates,
        )

        kernel_args = self.gen_kernel_arguments(
            typegen, ctype, Pdim, debug_mode, is_inplace, known_vars, symbolic_mode
        )

        super().__init__(
            name=name,
            typegen=typegen,
            work_dim=work_dim,
            known_vars=known_vars,
            kernel_args=kernel_args,
            symbolic_mode=symbolic_mode,
            **kargs,
        )

        dtype = ctype_to_dtype(ctype)

        if debug_mode:
            print("Transpose codegen configuration:")
            print(f" *dimension:         {pdim}")
            print(f" *axes:              {axes}")
            print(f" *tile_dimension:    {tdim}")
            print(f" *tile_shape:        {tile_shape}")
            print(f" *tile_indexes:      {tile_indexes}")
            print(f" *tile_axes:         {tile_axes}")
            print(f" *tile_index_to_id:  {tile_index_to_id}")
            print(f" *workload_indexes:  {workload_indexes}")
            print(f" *wl_index_to_id:    {wl_index_to_id}")
            print(f" *is_tile_index:     {is_tile_index}")
            print(f" *is_workload_index: {is_workload_index}")
            print(
                " *work_dim:          {} (tile[{}] + device_workload[{}])".format(
                    work_dim, tdim, work_dim - tdim
                )
            )
            print(f" *ctype:             {ctype}")
            print(f" *dtype:             {dtype}")

        self.ctype = ctype
        self.dtype = dtype
        self.axes = axes
        self.pdim = pdim
        self.Pdim = Pdim
        self.tdim = tdim
        self.Tdim = Tdim
        self.wldim = wldim
        self.WLdim = WLdim
        self.tile_size = tile_size
        self.tile_shape = tile_shape
        self.tile_padding = tile_padding
        self.tile_indexes = tile_indexes
        self.tile_axes = tile_axes
        self.tile_index_to_id = tile_index_to_id
        self.workload_indexes = workload_indexes
        self.wl_index_to_id = wl_index_to_id
        self.is_tile_index = is_tile_index
        self.is_workload_index = is_workload_index
        self.is_inplace = is_inplace
        self.permutation_axes = permutation_axes
        self.vectorization = vectorization
        self.contiguous_permutation = contiguous_permutation
        self.use_diagonal_coordinates = use_diagonal_coordinates

        self.gencode()

    def cache_alloc_bytes(self, local_size):
        pass

    def required_cache_size(self):
        return self.tile_size

    def build_requirements(self):
        reqs = WriteOnceDict()
        return reqs

    def gen_kernel_arguments(
        self, typegen, ctype, Pdim, debug_mode, is_inplace, known_vars, symbolic_mode
    ):
        _global = OpenClCodeGenerator.default_keywords["global"]
        tg = typegen
        mesh_dim = Pdim

        kargs = ArgDict()
        if is_inplace:
            data, strides = OpenClArrayBackend.build_codegen_arguments(
                kargs,
                name="inout",
                known_vars=known_vars,
                symbolic_mode=symbolic_mode,
                storage=self._global,
                ctype=ctype,
                typegen=typegen,
                mesh_dim=mesh_dim,
                const=False,
                ptr_restrict=True,
            )
            self.inout_strides = strides
            self.inout_data = data
        else:
            in_data, in_strides = OpenClArrayBackend.build_codegen_arguments(
                kargs,
                name="in",
                known_vars=known_vars,
                symbolic_mode=symbolic_mode,
                storage=self._global,
                ctype=ctype,
                typegen=typegen,
                mesh_dim=mesh_dim,
                const=True,
                ptr_restrict=True,
            )
            out_data, out_strides = OpenClArrayBackend.build_codegen_arguments(
                kargs,
                name="out",
                known_vars=known_vars,
                symbolic_mode=symbolic_mode,
                storage=self._global,
                ctype=ctype,
                typegen=typegen,
                mesh_dim=mesh_dim,
                const=False,
                ptr_restrict=True,
            )
            self.in_data = in_data
            self.out_data = out_data
            self.in_strides = in_strides
            self.out_strides = out_strides

        if debug_mode:
            n_dbg_arrays = self.n_dbg_arrays
            for i in range(n_dbg_arrays):
                kargs[f"dbg{i}"] = CodegenVariable(
                    ctype="int",
                    name=f"dbg{i}",
                    typegen=tg,
                    storage=_global,
                    ptr=True,
                    ptr_const=True,
                    ptr_restrict=True,
                    nl=True,
                )
        self.debug_mode = debug_mode

        kargs["shape"] = CodegenVectorClBuiltin(
            btype="int",
            dim=Pdim,
            name="shape",
            typegen=tg,
            add_impl_const=True,
            symbolic_mode=True,
        )

        return kargs

    def gencode(self):
        kernel_reqs = self.build_requirements()
        self.update_requirements(kernel_reqs)

        _local = OpenClCodeGenerator.default_keywords["local"]

        s = self
        tg = s.typegen
        work_dim = s.work_dim
        symbolic_mode = s.symbolic_mode

        ctype = s.ctype
        dtype = s.dtype

        pdim = s.pdim
        Pdim = s.Pdim
        tdim = s.tdim
        Tdim = s.Tdim
        wldim = s.wldim
        WLdim = s.WLdim

        axes = s.axes
        permutation_axes = s.permutation_axes

        tile_size = s.tile_size
        tile_shape = s.tile_shape
        tile_padding = s.tile_padding
        tile_indexes = s.tile_indexes
        tile_axes = s.tile_axes
        tile_index_to_id = s.tile_index_to_id
        workload_indexes = s.workload_indexes
        wl_index_to_id = s.wl_index_to_id

        is_tile_index = s.is_tile_index
        is_workload_index = s.is_workload_index
        is_inplace = s.is_inplace
        vectorization = s.vectorization

        contiguous_permutation = s.contiguous_permutation
        use_diagonal_coordinates = s.use_diagonal_coordinates

        debug_mode = s.debug_mode
        n_dbg_arrays = s.n_dbg_arrays

        local_id = s.vars["local_id"]
        local_size = s.vars["local_size"]
        group_id = s.vars["group_id"]
        group_size = s.vars["num_groups"]
        global_size = s.vars["global_size"]
        global_id = s.vars["global_id"]

        S = s.vars["shape"]
        if is_inplace:
            _in = self.inout_data
            _out = self.inout_data
            _in_strides = self.inout_strides
            _out_strides = self.inout_strides
        else:
            _in = self.in_data
            _out = self.out_data
            _in_strides = self.in_strides
            _out_strides = self.out_strides

        if debug_mode:
            dbg = [s.vars[f"dbg{i}"] for i in range(n_dbg_arrays)]

        if is_inplace and S.known():
            msg = "Permutated shape axis should form an hypercube for inplace transpositions."
            Sp = S.value[np.asarray(permutation_axes)]
            Sp0 = S.value[permutation_axes[0]]
            assert (Sp == Sp0).all(), msg

        tile_size = CodegenVariable(
            typegen=tg,
            name="tile_size",
            ctype="int",
            const=True,
            value=self.tile_size,
            symbolic_mode=symbolic_mode,
        )
        tile_padding = CodegenVariable(
            typegen=tg,
            name="tile_padding",
            ctype="int",
            const=True,
            value=self.tile_padding,
            symbolic_mode=symbolic_mode,
        )
        tile_sshape = [tile_size.value] * tdim
        tile_sshape[-1] = f"{tile_size.value}+{tile_padding.value}"

        tile_indexes_extended = tile_indexes + (0,) * (Tdim - tdim)

        ntiles = f"(({S}+{tile_size}-1)/{tile_size})"
        ntiles = CodegenVectorClBuiltin(
            "ntiles", "int", Pdim, tg, const=True, init=ntiles
        )

        nwork = f"(({S[:work_dim]}+{local_size}-1)/{local_size})"
        nwork = CodegenVectorClBuiltin(
            "nwork", "int", work_dim, tg, const=True, init=nwork
        )

        idx = CodegenVectorClBuiltin("idx", "int", Pdim, tg)
        bidx = CodegenVectorClBuiltin("bidx", "int", Tdim, tg)
        tidx = CodegenVectorClBuiltin("tidx", "int", Tdim, tg)
        lidx = CodegenVectorClBuiltin("lidx", "int", Tdim, tg)
        kidx = CodegenVectorClBuiltin("kidx", "int", WLdim, tg)

        tmp = CodegenVectorClBuiltin("tmp", ctype, vectorization, tg)

        tile_offset_in, tile_offset_out = "", ""
        local_offset_in, local_offset_out = "", ""
        ki = kj = tdim - 1
        for k in range(pdim):
            i = pdim - 1 - k
            j = pdim - 1 - axes[k]
            if i == pdim - 1:
                tile_offset_in = f"{_in_strides[i]}*{idx[i]}"
                tile_offset_out = f"{_out_strides[i]}*{idx[j]}"
            else:
                tile_offset_in += f" $+ {_in_strides[i]}*{idx[i]}"
                tile_offset_out += f" $+ {_out_strides[i]}*{idx[j]}"

            if i in tile_indexes:
                if ki == tdim - 1:
                    local_offset_in = f"{_in_strides[i]}*{lidx[ki]}"
                else:
                    local_offset_in += f" $+ {_in_strides[i]}*{lidx[ki]}"
                ki -= 1

            if j in tile_indexes:
                if kj == tdim - 1:
                    local_offset_out = f"{_out_strides[i]}*{lidx[kj]}"
                else:
                    local_offset_out += f" $+ {_out_strides[i]}*{lidx[kj]}"
                kj -= 1

        assert ki == -1
        assert kj == -1

        tile_id = ""
        block_id = ""
        loc_id = ""
        for i in range(tdim - 1, -1, -1):
            if i == tdim - 1:
                tile_id = f"{tidx[i]}"
                block_id = f"{bidx[i]}"
            else:
                tile_id = f"({tile_id}*{ntiles[i]}+{tidx[i]})"
                block_id = f"({block_id}*{ntiles[i]}+{bidx[i]})"

        for i in range(work_dim - 1, -1, -1):
            if i == work_dim - 1:
                loc_id = f"{local_id[i]}"
            else:
                loc_id = f"({loc_id}*{local_size[i]}+{local_id[i]})"

        tile_offset_in = CodegenVariable(
            "tile_offset_in", "ulong", tg, init=tile_offset_in, const=True
        )
        tile_offset_out = CodegenVariable(
            "tile_offset_out", "ulong", tg, init=tile_offset_out, const=True
        )
        local_offset_in = CodegenVariable(
            "local_offset_in", "ulong", tg, init=local_offset_in, const=True
        )
        local_offset_out = CodegenVariable(
            "local_offset_out", "ulong", tg, init=local_offset_out, const=True
        )

        TID = CodegenVariable("TID", "int", tg, const=True, init=tile_id)
        BID = CodegenVariable("BID", "int", tg, const=True, init=block_id)
        LID = CodegenVariable("LID", "int", tg, const=True, init=loc_id)

        active = CodegenVariable("active", "bool", tg)

        active_cond = " && ".join(
            [
                f"({idx[i]}<{S[i]})"
                for i in range(pdim)
                if (
                    i
                    < (
                        work_dim
                        - int(
                            contiguous_permutation and (tile_indexes[1] > work_dim - 1)
                        )
                    )
                )
                and (i not in tile_indexes)
            ]
        )

        @contextmanager
        def _block_iterate_(i):
            try:
                if is_tile_index[i]:
                    tid = tile_index_to_id[i]
                    imin = min(i, work_dim - 1)
                    loop = "{i}={ig}; {i}<{N}; {i}+={ng}".format(
                        i=bidx[tid], ig=group_id[imin], N=ntiles[i], ng=group_size[imin]
                    )
                    unroll = True
                elif is_workload_index[i]:
                    wid = wl_index_to_id[i]
                    loop = "{i}={ig}; {i}<{N}; {i}+={ng}".format(
                        i=kidx[wid], ig=group_id[i], ng=group_size[i], N=nwork[i]
                    )
                    unroll = True
                else:
                    loop = "{i}=0; {i}<{N}; {i}+=1".format(i=idx[i], N=S[i])
                    unroll = False
                with s._for_(loop, unroll=unroll) as ctx:
                    yield ctx
            except:
                raise

        @contextmanager
        def _tile_iterate(i, tile_idx):
            try:
                loop = "{var}={lid}; ({var}<{N}) && ({glob}+{var} < {S}); {var}+={L}".format(
                    i=i,
                    var=lidx[i],
                    glob=idx[tile_idx],
                    lid=local_id[i],
                    L=local_size[i],
                    N=tile_size,
                    S=S[tile_idx],
                )
                unroll = True
                with s._for_(loop, unroll=unroll) as ctx:
                    yield ctx
            except:
                raise

        block_loops = [_block_iterate_(i) for i in range(pdim)][::-1]
        if is_inplace and contiguous_permutation:
            tile0 = CodegenArray(
                typegen=tg,
                name="tile0",
                ctype=ctype,
                storage="__local",
                dim=tdim,
                shape=tile_sshape,
            )
            tile1 = CodegenArray(
                typegen=tg,
                name="tile1",
                ctype=ctype,
                storage="__local",
                dim=tdim,
                shape=tile_sshape,
            )
            tiles = (tile0, tile1)

            tile_loops_in0 = [_tile_iterate(i, j) for i, j in enumerate(tile_indexes)][
                ::-1
            ]
            tile_loops_out0 = [
                _tile_iterate(i, j) for i, j in enumerate(tile_indexes[::-1])
            ][::-1]
            tile_loops_in1 = [
                _tile_iterate(i, j) for i, j in enumerate(tile_indexes[::-1])
            ][::-1]
            tile_loops_out1 = [_tile_iterate(i, j) for i, j in enumerate(tile_indexes)][
                ::-1
            ]

            tile_in0 = tile0() + "".join([f"[{lidx[i]}]" for i in range(tdim)][::-1])
            tile_out0 = tile0() + "".join(
                [f"[{lidx[axes.tolist().index(axes[i])]}]" for i in range(tdim)]
            )
            tile_in1 = tile1() + "".join(
                [f"[{lidx[axes.tolist().index(axes[i])]}]" for i in range(tdim)]
            )
            tile_out1 = tile1() + "".join([f"[{lidx[i]}]" for i in range(tdim)][::-1])
        else:
            tile = CodegenArray(
                typegen=tg,
                name="tile",
                ctype=ctype,
                storage="__local",
                dim=tdim,
                shape=tile_sshape,
            )
            tiles = (tile,)

            tile_loops_in0 = [_tile_iterate(i, j) for i, j in enumerate(tile_indexes)][
                ::-1
            ]
            tile_loops_out0 = [
                _tile_iterate(i, j) for i, j in enumerate(tile_indexes[::-1])
            ][::-1]

            tile_in0 = tile() + "".join([f"[{lidx[i]}]" for i in range(tdim)][::-1])
            tile_out0 = tile() + "".join(
                [f"[{lidx[axes.tolist().index(axes[i])]}]" for i in range(tdim)]
            )

        # include complex definitions if required
        with s._codeblock_("pragma_extensions"):
            if ctype == "cdouble_t":
                s.define("PYOPENCL_DEFINE_CDOUBLE")
            if ctype in ("cfloat_t", "cdouble_t"):
                s.include('"pyopencl-complex.h"')

        with s._kernel_():
            with s._align_() as al:
                tile_size.declare(al, align=True)
                tile_padding.declare(al, align=True)
            s.jumpline()
            s.decl_aligned_vars(
                global_id,
                local_id,
                group_id,
                global_size,
                local_size,
                group_size,
                const=True,
            )
            ptrs = (_in,)
            if not is_inplace:
                ptrs += (_out,)
            s.decl_aligned_vars(*ptrs)
            s.jumpline()
            nwork.declare(s)
            ntiles.declare(s)
            s.jumpline()
            if contiguous_permutation:
                s.decl_vars(*tiles)

            s.jumpline()

            comment = """Iteration over all active and non active axes.
Inactive axes are the ones that are not permutated.
Active axes contain tiles that should be permutated.
Each workgroup handle a certain amount of tiles on those active axes,
depending on the number of scheduled workgroups and the work dimension.
Each tiles is loaded in shared memory (inner loops) by a given workgroup,
using the n-dimensional workgroup size as subtile blocks.
The same workgroup writes than back the permutated tile into the output array.
The two first block coordinates of a tile can be mapped to diagonal coordinates
to prevent memory camping that may occur during global input read or output write
(tiles are not wide enough in the contiguous direction)."""
            # s.comment(comment)
            s.decl_vars(idx)
            s.decl_vars(bidx, tidx)
            if wldim:
                s.decl_vars(kidx)
            if not contiguous_permutation and is_inplace:
                s.decl_vars(tmp)
            s.decl_vars(active)
            with nested(*block_loops):
                s.comment("Map block index to tile index.")
                if use_diagonal_coordinates and contiguous_permutation:
                    BID.declare(s)
                    gz0 = ntiles[tile_indexes[1]]
                    gz1 = ntiles[0]
                    tidx.affect(s, i=1, init=f"{BID} % {gz0}")
                    tidx.affect(s, i=0, init=f"(({BID}/{gz0}) + {tidx[1]}) % {gz1}")
                else:
                    tidx.affect(s, init=bidx)
                if debug_mode:
                    if not BID.declared:
                        BID.declare(s)
                    TID.declare(s)
                s.jumpline()

                if wldim:
                    s.comment("Adjust global offset index using the workload index")
                    code = "{} = {}*{}+{};".format(
                        idx[workload_indexes],
                        kidx[:wldim],
                        local_size[workload_indexes],
                        local_id[workload_indexes],
                    )
                    s.append(code)
                    s.jumpline()

                s.comment("Adjust global offset index using the tile index")
                code = f"{idx[tile_indexes]} = {tile_size}*{tidx[:tdim]};"
                s.append(code)
                s.jumpline()

                s.comment("Determine if this index is active")
                if is_inplace:
                    assert len(permutation_axes) == 2
                    acond = "({}<={})".format(
                        idx[permutation_axes[-2]], idx[permutation_axes[-1]]
                    )
                    if active_cond:
                        active_cond += f" && {acond}"
                    else:
                        active_cond = acond
                active.affect(s, init=active_cond or "true")
                s.jumpline()

                s.comment(
                    f"idx[0:{pdim}] now identifies a unique input and output tile, workgroup can iterate over it."
                )
                s.decl_aligned_vars(tile_offset_in, tile_offset_out)

                s.decl_aligned_vars(lidx)
                if contiguous_permutation:
                    with s._if_(active):
                        with nested(*tile_loops_in0):
                            local_offset_in.declare(s)
                            s.append(
                                "{} = {};".format(
                                    tile_in0,
                                    _in[
                                        "{}+{}".format(tile_offset_in, local_offset_in)
                                    ],
                                )
                            )
                        if is_inplace:
                            with nested(*tile_loops_in1):
                                local_offset_out.declare(s)
                                s.append(
                                    "{} = {};".format(
                                        tile_in1,
                                        _out[
                                            "{}+{}".format(
                                                tile_offset_out, local_offset_out
                                            )
                                        ],
                                    )
                                )

                    s.barrier(_local=True)
                    s.jumpline()

                    with s._if_(active):
                        with nested(*tile_loops_out0):
                            local_offset_out.declare(s)
                            s.append(
                                "{} = {};".format(
                                    _out[
                                        "{}+{}".format(
                                            tile_offset_out, local_offset_out
                                        )
                                    ],
                                    tile_out0,
                                )
                            )
                        if is_inplace:
                            with nested(*tile_loops_out1):
                                local_offset_in.declare(s)
                                s.append(
                                    "{} = {};".format(
                                        _in[
                                            "{}+{}".format(
                                                tile_offset_in, local_offset_in
                                            )
                                        ],
                                        tile_out1,
                                    )
                                )
                    s.barrier(_local=True)
                else:
                    with s._if_(active):
                        with tile_loops_in0[0]:
                            if is_inplace:

                                offset_in = f"{tile_offset_in}$+{lidx[0]}"
                                offset_out = f"{tile_offset_out}$+{lidx[0]}"

                                tmp_load = self.vload(vectorization, _in, offset_out)
                                tmp.affect(s, init=tmp_load)

                                swap = self.vload(
                                    vectorization, _in, offset_in, align=True
                                )
                                swap = self.vstore(
                                    vectorization,
                                    _in,
                                    offset_out,
                                    "$" + swap,
                                    jmp=True,
                                    align=True,
                                )
                                s.align(swap)

                                tmp_store = self.vstore(
                                    vectorization, _out, offset_in, tmp
                                )
                                s.append(tmp_store)
                            else:
                                code = "{} = {};".format(
                                    _out[f"{tile_offset_out}+{lidx[0]}"],
                                    _in[f"{tile_offset_in}+{lidx[0]}"],
                                )
                                s.append(code)


if __name__ == "__main__":
    from hysop.backend.device.codegen.base.test import _test_typegen

    tg = _test_typegen("float")
    ek = TransposeKernelGenerator(
        typegen=tg,
        ctype="short",
        vectorization=4,
        axes=(2, 1, 0, 4, 3),
        tile_size=8,
        tile_padding=1,
        is_inplace=False,
        symbolic_mode=True,
        known_vars={"shape": (256, 128, 64, 32, 16, 0, 0, 0)},
    )
    ek.edit()
    ek.test_compile()
