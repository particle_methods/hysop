# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import contextlib
from contextlib import contextmanager

import operator, hashlib

from hysop import __VERBOSE__, __KERNEL_DEBUG__

from hysop.tools.misc import Utils
from hysop.constants import np

from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.codegen.base.kernel_codegen import KernelCodeGenerator
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
)
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.codegen.base.utils import WriteOnceDict, ArgDict

from hysop.backend.device.opencl import cl, clCharacterize
from hysop.backend.device.opencl.opencl_env import OpenClEnvironment
from hysop.backend.device.opencl.opencl_kernel import OpenClKernelLauncher
from hysop.backend.device.kernel_autotuner import KernelAutotuner, AutotunerConfig


class CopyKernel(KernelCodeGenerator):

    @staticmethod
    def codegen_name(src_mem, dst_mem, vtype, restrict, vectorized):
        name = f"copy_{src_mem}_{dst_mem}_{vtype}"
        if restrict:
            name += "_noalias"
        if vectorized > 1:
            name += f"_v{vectorized}"
        return name

    def __init__(
        self,
        typegen,
        vtype,
        vectorized=1,
        src_mem="global",
        dst_mem="global",
        restrict=True,
        known_vars=None,
        force_symbolic=False,
    ):

        if vectorized == 1:
            pass
        elif vectorized not in typegen.vsizes:
            raise ValueError("Invalid vector size.")
        elif typegen.basetype(vtype) != vtype:
            raise ValueError("Cannot vectorize vector types.")

        kernel_args = self.gen_kernel_arguments(
            typegen, vtype, src_mem, dst_mem, restrict
        )

        name = CopyKernel.codegen_name(src_mem, dst_mem, vtype, restrict, vectorized)

        super().__init__(
            name=name,
            typegen=typegen,
            work_dim=1,
            kernel_args=kernel_args,
            known_vars=known_vars,
            vec_type_hint=vtype,
            symbolic_mode=force_symbolic,
        )

        self.vtype = vtype
        self.src_mem = src_mem
        self.dst_mem = dst_mem
        self.restrict = restrict
        self.vectorized = vectorized

        self.gencode()

    def gen_kernel_arguments(self, typegen, vtype, src_mem, dst_mem, restrict):

        ftype = typegen.basetype(vtype)
        components = typegen.components(vtype)
        is_base_type = components == 1

        _src_mem = OpenClCodeGenerator.default_keywords[src_mem]
        _dst_mem = OpenClCodeGenerator.default_keywords[dst_mem]

        kargs = ArgDict()

        kargs["dst"] = CodegenVariable(
            ctype=vtype,
            name="dst",
            ptr=True,
            typegen=typegen,
            nl=True,
            restrict=restrict,
            storage=_src_mem,
        )
        kargs["src"] = CodegenVariable(
            ctype=vtype,
            name="src",
            ptr=True,
            typegen=typegen,
            nl=True,
            restrict=restrict,
            storage=_dst_mem,
            const=True,
        )
        kargs["count"] = CodegenVariable(
            ctype="unsigned int",
            name="count",
            typegen=typegen,
            add_impl_const=True,
            nl=True,
        )

        return kargs

    def gencode(self):
        s = self

        global_id = s.vars["global_id"]
        global_size = s.vars["global_size"]

        dst = s.vars["dst"]
        src = s.vars["src"]
        count = s.vars["count"]

        with s._kernel_():
            global_size.declare(s, const=True)

            if global_size.known():
                s.pragma("unroll")
            with s._for_(
                "int {gid}={fval}; {gid}<{N}; {gid}+={gsize}".format(
                    gid=global_id(),
                    fval=global_id.fval(0),
                    gsize=global_size(),
                    N=count(),
                )
            ):
                N = s.vectorized
                if N > 1:
                    vload = f"vload{N}({global_id()}, {src()})"
                    vstore = f"vstore{N}({vload}, {global_id()}, {dst()});"
                    s.append(vstore)
                else:
                    s.append(f"{dst[global_id()]} = {src[global_id()]};")

    @staticmethod
    def autotune(
        cl_env, typegen, src, dst, vtype, count, restrict, build_opts, autotuner_config
    ):

        if not isinstance(cl_env, OpenClEnvironment):
            raise ValueError("cl_env is not an OpenClEnvironment.")
        if not isinstance(typegen, OpenClTypeGen):
            raise ValueError("typegen is not an OpenClTypeGen.")

        device = cl_env.device
        context = cl_env.ctx
        platform = cl_env.platform
        queue = cl_env.queue

        if vtype not in typegen.builtin_types:
            raise ValueError(f"{vtype} is not an opencl bultin type.")
        if count < 0:
            raise ValueError("count < 0")

        if context is None:
            raise ValueError("context cannot be None.")
        if device is None:
            raise ValueError("device cannot be None.")
        if platform is None:
            raise ValueError("platform cannot be None.")
        if queue is None:
            raise ValueError("queue cannot be None.")
        if typegen.platform != platform or typegen.device != device:
            raise ValueError("platform or device mismatch.")

        # autotuner parameters
        work_size = count
        min_local_size = max(1, clCharacterize.get_simd_group_size(device, 1))
        max_workitem_workload = 256

        symbolic_mode = False  # __KERNEL_DEBUG__
        dump_src = __KERNEL_DEBUG__

        ## kernel generator
        def kernel_generator(
            work_size,
            global_size,
            local_size,
            kernel_args,
            vectorized,
            force_verbose=False,
            force_debug=False,
            **kargs,
        ):

            ## Compile time known variables
            known_vars = dict(
                global_size=global_size[0], local_size=local_size[0], count=work_size[0]
            )

            ## CodeGenerator
            codegen = CopyKernel(
                typegen=typegen,
                vtype=vtype,
                vectorized=vectorized,
                src_mem="global",
                dst_mem="global",
                restrict=restrict,
                force_symbolic=symbolic_mode,
                known_vars=known_vars,
            )

            ## generate source code and build kernel
            src = codegen.__str__()
            src_hash = hashlib.sha512(src).hexdigest()
            prg = cl_env.build_raw_src(
                src,
                build_opts,
                kernel_name=codegen.name,
                force_verbose=force_verbose,
                force_debug=force_debug,
            )
            kernel = prg.all_kernels()[0]

            return (kernel, kernel_args, 0, src_hash)

        ## Kernel Autotuner
        def _autotune(vectorized):

            if work_size % vectorized != 0:
                raise ValueError("Invalid vector size.")

            codegen_name = CopyKernel.codegen_name(
                "global", "global", vtype, restrict, vectorized
            )
            autotuner = KernelAutotuner(
                name=codegen_name,
                work_dim=1,
                build_opts=build_opts,
                autotuner_config=autotuner_config,
            )
            autotuner.add_filter("1d_shape_min", autotuner.min_workitems_per_direction)
            autotuner.enable_variable_workitem_workload(
                max_workitem_workload=max_workitem_workload
            )

            kernel_args = [dst, src]
            kernel_args_mapping = {"dst": slice(1, 2, 1), "src": slice(2, 3, 1)}

            (gwi, lwi, stats, wl) = autotuner.bench(
                typegen=typegen,
                work_size=work_size // vectorized,
                kernel_args=kernel_args,
                kernel_generator=kernel_generator,
                dump_src=dump_src,
                min_local_size=min_local_size,
                vectorized=vectorized,
            )

            (kernel, kernel_args, cached_bytes, src_hash) = kernel_generator(
                work_size=[work_size // vectorized],
                global_size=gwi,
                local_size=lwi,
                kernel_args=kernel_args,
                vectorized=vectorized,
                force_verbose=None,
                force_debug=None,
            )

            kernel_launcher = OpenClKernelLauncher(kernel, queue, list(gwi), list(lwi))
            return (
                stats,
                kernel_launcher,
                kernel_args,
                kernel_args_mapping,
                cached_bytes,
            )

        candidates = [i for i in typegen.vsizes if work_size % i == 0]

        best = None
        for vectorized in candidates:
            res = _autotune(vectorized)
            if (best is None) or res[0] < best[0]:
                best = res
        return best[1:]


if __name__ == "__main__":
    from hysop.backend.device.codegen.base.test import test_typegen

    tg = test_typegen("float", "dec")
    vtype = "float"

    ck = CopyKernel(
        tg,
        vtype,
        vectorized=16,
        force_symbolic=True,
        known_vars=dict(count=1024 * 1024, local_size=1024, global_size=1024 * 512),
    )
    ck.edit()
