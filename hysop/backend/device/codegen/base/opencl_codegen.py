# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import pyopencl as cl
from contextlib import contextmanager

from hysop.tools.htypes import check_instance, first_not_None
from hysop.backend.device.codegen.base.codegen import CodeGenerator
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.codegen.base.utils import VarDict
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
    CodegenVectorClBuiltinFunc,
)


class OpenClCodeGenerator(CodeGenerator):
    default_keywords = {
        "global": "__global",
        "local": "__local",
        "constant": "__constant",
        "private": "__private",
        "read_only": "__read_only",
        "write_only": "__write_only",
        "read_write": "__read_write",
        "inline": "inline",
        "kernel": "__kernel",
    }
    default_escape_seqs = {"\t": " " * 4, "\n": "\n", " ": " "}

    _global, _local = "__global", "__local"

    def __init__(
        self, name, typegen, ext=".cl", known_vars=None, declare_cl_exts=True, **kargs
    ):

        check_instance(typegen, OpenClTypeGen)
        super().__init__(
            name=name,
            typegen=typegen,
            ext=ext,
            known_vars=known_vars,
            keywords=self.default_keywords,
            escape_seqs=self.default_escape_seqs,
            **kargs,
        )

        self.device = typegen.device
        self.context = typegen.context
        self.platform = typegen.platform

        if declare_cl_exts:
            for cl_ext in typegen.cl_requirements():
                if cl_ext is not None:
                    self.declare_cl_extension(cl_ext)

    def test_compile(self, contexts=None):
        print(
            f"Test build on device {self.device.name}: ",
        )
        src = self.__str__()
        prg = cl.Program(self.context, src)
        prg.build()
        print("OK")

    @staticmethod
    def _mem_flags(_local, _global):
        if _local and _global:
            return "CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE"
        elif _local:
            return "CLK_LOCAL_MEM_FENCE"
        elif _global:
            return "CLK_GLOBAL_MEM_FENCE"
        else:
            return None

    def barrier(self, _local=False, _global=False):
        if not _local and not _global:
            raise ValueError("Bad barrier configuration!")
        code = f"barrier({self._mem_flags(_local,_global)});"
        self.append(code)

    def mem_fence(self, read=False, write=False, _local=False, _global=False):
        if (not read and not write) or (not _local and not _global):
            raise ValueError("Bad memfence configuration!")

        # opencl 1.0 has only the barrier function
        self.append("#if __OPENCL_VERSION__ < 110")
        self.indent()
        self.barrier(_local=_local, _global=_global)
        self.dedent()
        self.append("#else")
        self.indent()
        if read and write:
            mem_fun = "mem_fence"
        elif read:
            mem_fun = "read_mem_fence"
        else:
            mem_fun = "write_mem_fence"
        mem_flags = self._mem_flags(_local, _global)
        code = f"{mem_fun}({mem_flags});"
        self.append(code)
        self.dedent()
        self.append("#endif")

    def declare_cl_extension(self, extname):
        from hysop.backend.device.codegen.base.cl_extensions import ClExtCodeGen

        self.require(extname, ClExtCodeGen(extname))
        return self

    @contextmanager
    def _kernel_(
        self, name, args=None, args_impl=None, arg_spaces=True, attributes=None
    ):

        def filter_args(_args):
            if not args:
                newargs = [""]
            else:
                newargs = []
                for i, arg in enumerate(_args):
                    newl = arg[-1] == self.newl()
                    arg = arg[:-1] if newl else arg
                    newargs.append(arg)
                    if newl and i != len(_args) - 1:
                        newargs.append(self.newl())
            return newargs

        args = filter_args(args)
        if not args_impl:
            args_impl = args
        else:
            args_impl = filter_args(args_impl)

        if arg_spaces:
            comma = ", "
        else:
            comma = ","

        kernel_kwd = self.keywords["kernel"]

        prefix = f"{kernel_kwd} void {name}("
        indent_proto = len(prefix) * " "
        if attributes:
            attr = [
                "{} __attribute__(({}))".format(len(kernel_kwd) * " ", at)
                for at in attributes.values()
            ]
            attr[0] = attr[0][len(kernel_kwd) + 1 :]
            attr = "\n".join(attr)
            proto_prefix = "{kernel} {attr}\nvoid {name}(".format(
                kernel=kernel_kwd, attr=attr, name=name
            )
            indent_proto = len(f"void {name}(") * " "
        else:
            proto_prefix = prefix
        suffix = "{args})"
        indent = len(self.current_indent() + prefix) * " "

        pargs = [
            arg + comma if arg != self.newl() else arg + indent_proto
            for arg in args[:-1]
        ] + [args[-1]]
        prototype = proto_prefix + suffix.format(args="".join(pargs)) + ";"

        dargs = [
            arg + comma if arg != self.newl() else arg + indent
            for arg in args_impl[:-1]
        ] + [args_impl[-1]]
        definition = prefix + suffix.format(args="".join(dargs)) + " "

        self.declare_prototype(prototype, "kernel")
        with self._codeblock_("kernel_declarations"):
            with self._block_(header_prefix=definition) as b:
                yield b

    def vstore(
        self,
        n,
        ptr,
        offset,
        data,
        offset_is_ftype=True,
        align=False,
        jmp=False,
        suppress_semicolon=False,
    ):
        assert n * ptr.dim in [1, 2, 4, 8, 16]
        if ptr.dim > 1:
            cast_ptr = (
                lambda x: f"$({ptr.full_ctype(cast=True, ctype=ptr.basetype)})({x})"
            )
        else:
            cast_ptr = lambda x: f"${x}"
        if offset_is_ftype:
            if n > 1:
                shift = 0
                _n = n
                while _n > 1:
                    _n >>= 1
                    shift += 1
                _offset = f"$({offset}) $>> {shift}"
                address = cast_ptr(f"{ptr} $+ (({offset}) $& 0x{((1<<shift)-1):02x})")
                code = f"vstore{n*ptr.dim}({data},\n ${_offset}, ${address});"
            else:
                code = f"{ptr[offset]} $= {data};"
        else:
            if ptr.dim > 1:
                raise NotImplementedError()
            if n > 1:
                code = f"vstore{n}({data},\n ${offset}, ${ptr});"
            else:
                code = "{} $= {};".format(ptr[f"{n}*{offset}"], data)
        if not align:
            code = code.replace("$", "")
        if not jmp:
            code = code.replace("\n", "")
        if suppress_semicolon:
            code = code.replace(";", "")
        return code

    def vstore_if(
        self,
        cond,
        scalar_cond,
        n,
        ptr,
        offset,
        data,
        offset_is_ftype=True,
        align=False,
        jmp=False,
        suppress_semicolon=False,
        use_short_circuit=False,
        else_cond=None,
    ):
        with self._if_(cond):
            for i in range(n):
                assert callable(scalar_cond), type(scalar_cond)
                icond = scalar_cond(i)
                idata = data[i]
                if offset_is_ftype:
                    ioffset = f"{offset}+{i}"
                else:
                    ioffset = f"{n}*{offset}+{i}"
                istore = f"{ptr[ioffset]} = {idata}"
                if use_short_circuit:
                    code = f"({icond}) && (({istore}),true);"
                else:
                    code = f"if({icond}) {{ {istore}; }}"
                self.append(code)
        _else = self._else_() if (else_cond is None) else self._elif_(else_cond)
        with _else:
            code = self.vstore(
                n=n,
                ptr=ptr,
                offset=offset,
                data=data,
                offset_is_ftype=offset_is_ftype,
                align=align,
                jmp=jmp,
                suppress_semicolon=suppress_semicolon,
            )
            self.append(code)

    def vload(self, n, ptr, offset, offset_is_ftype=True, align=False, jmp=False):
        assert n * ptr.dim in [1, 2, 4, 8, 16]
        if ptr.dim > 1:
            cast_ptr = (
                lambda x: f"$({ptr.full_ctype(cast=True, ctype=ptr.basetype)})({x})"
            )
        else:
            cast_ptr = lambda x: f"${x}"
        if offset_is_ftype:
            if n > 1:
                shift = 0
                _n = n
                while _n > 1:
                    _n >>= 1
                    shift += 1
                _offset = f"$({offset}) $>> {shift}"
                address = cast_ptr(f"{ptr} $+ (({offset}) $& 0x{((1<<shift)-1):02x})")
                code = f"vload{n*ptr.dim}({_offset},\n ${address})"
            else:
                code = ptr[offset]
        else:
            if ptr.dim > 1:
                raise NotImplementedError()
            if n > 1:
                code = f"vload{n*ptr.dim}({offset},\n ${_ptr})"
            else:
                code = ptr[f"{n}*{offset}"]

        if not align:
            code = code.replace("$", "")
        if not jmp:
            code = code.replace("\n", "")
        return code

    def vload_if(
        self,
        cond,
        scalar_cond,
        n,
        ptr,
        offset,
        dst,
        default_value,
        offset_is_ftype=True,
        align=False,
        jmp=False,
        use_short_circuit=False,
        else_cond=None,
    ):
        with self._if_(cond):
            for i in range(n):
                assert callable(scalar_cond), type(scalar_cond)
                icond = scalar_cond(i)
                odata = dst[i]
                if offset_is_ftype:
                    ioffset = f"{offset}+{i}"
                else:
                    ioffset = f"{n}*{offset}+{i}"
                if use_short_circuit:
                    code = f"{odata} = ({icond} ? {ptr[ioffset]} : {default_value});"
                else:
                    iload = f"{odata} = {ptr[ioffset]}"
                    dload = f"{odata} = {default_value}"
                    code = f"if({icond}){{ {iload}; }} else {{ {dload}; }}"
                self.append(code)
        _else = self._else_() if (else_cond is None) else self._elif_(else_cond)
        with _else:
            code = self.vload(
                n=n,
                ptr=ptr,
                offset=offset,
                offset_is_ftype=offset_is_ftype,
                align=align,
                jmp=jmp,
            )
            code = f"{dst} = {code};"
            self.append(code)

    def multi_vload_if(
        self,
        cond,
        scalar_cond,
        n,
        offset,
        srcs,
        dsts,
        default_values,
        offset_is_ftype=True,
        jmp=False,
        use_short_circuit=False,
        else_cond=None,
        extra_offsets=None,
    ):
        assert len(srcs) == len(dsts) == len(default_values) >= 1
        extra_offsets = first_not_None(extra_offsets, (0,) * len(srcs))
        with self._if_(cond):
            with self._align_() as al:
                for i in range(n):
                    assert callable(scalar_cond), type(scalar_cond)
                    icond = scalar_cond(i)
                    for ptr, dst, dval, eo in zip(
                        srcs, dsts, default_values, extra_offsets
                    ):
                        if n * ptr.dim != dst.dim:
                            msg = "Pointer datatype is not consistent with destination variable."
                            raise RuntimeError(msg)
                        ii = f"{i}"
                        if eo:
                            ii += f"+{eo}"
                        odata = dst[i * ptr.dim : (i + 1) * ptr.dim]
                        if offset_is_ftype:
                            ioffset = f"{offset}+{ii}"
                        else:
                            ioffset = f"{n}*{offset}+{ii}"
                        if use_short_circuit:
                            code = f"{odata} $= ({icond} $? {ptr[ioffset]} $: {dval});"
                        else:
                            iload = f"{odata} $= {ptr[ioffset]}"
                            dload = f"{odata} $= {dval}"
                            code = f"if({icond}){{ {iload}; }} $else {{ {dload}; }}"
                        al.append(code)
        _else = self._else_() if (else_cond is None) else self._elif_(else_cond)
        with _else:
            with self._align_() as al:
                for ptr, dst, eo in zip(srcs, dsts, extra_offsets):
                    eoffset = f"{offset}"
                    if eo:
                        eoffset += f"+{eo}"
                    code = self.vload(
                        n=n,
                        ptr=ptr,
                        offset=eoffset,
                        offset_is_ftype=offset_is_ftype,
                        align=True,
                        jmp=jmp,
                    )
                    code = f"{dst} $= {code};"
                    al.append(code)

    def multi_vstore_if(
        self,
        cond,
        scalar_cond,
        n,
        offset,
        srcs,
        dsts,
        offset_is_ftype=True,
        jmp=False,
        use_short_circuit=False,
        else_cond=None,
        extra_offsets=None,
    ):
        assert len(srcs) == len(dsts) >= 1
        extra_offsets = first_not_None(extra_offsets, (0,) * len(srcs))
        with self._if_(cond):
            with self._align_() as al:
                for i in range(n):
                    assert callable(scalar_cond), type(scalar_cond)
                    icond = scalar_cond(i)
                    for src, ptr, eo in zip(srcs, dsts, extra_offsets):
                        idata = src[i * ptr.dim : (i + 1) * ptr.dim]
                        ii = f"{i}"
                        if eo:
                            ii += f"+{eo}"
                        if offset_is_ftype:
                            ioffset = f"{offset}+{ii}"
                        else:
                            ioffset = f"{n}*{offset}+{ii}"
                        if use_short_circuit:
                            code = f"({icond}) $&& ({ptr[ioffset]} $= {idata}, true);"
                        else:
                            code = f"if({icond})${{ {ptr[ioffset]} $= {idata}; }}"
                        al.append(code)
        _else = self._else_() if (else_cond is None) else self._elif_(else_cond)
        with _else:
            with self._align_() as al:
                for src, ptr, eo in zip(srcs, dsts, extra_offsets):
                    eoffset = f"{offset}"
                    if eo:
                        eoffset += f"+{eo}"
                    code = self.vstore(
                        n=n,
                        ptr=ptr,
                        offset=eoffset,
                        data=src,
                        offset_is_ftype=offset_is_ftype,
                        align=True,
                        jmp=jmp,
                    )
                    al.append(code)

    def async_work_group_copy(self, dst, src, num_elements, event, align=False):
        code = f"{event} = async_work_group_copy({dst}, ${src}, ${num_elements}, ${event});"
        if not align:
            code = code.replace("$", "")
        return code

    def wait_group_events(self, num_events, event_list):
        return f"wait_group_events({num_events}, {event_list});"

    # looping facilities
    @contextmanager
    def _ordered_wi_execution_(self, barrier=True):
        cond0 = "int wi{i}=0; wi{i}<get_local_size({i}); wi{i}++"
        cond1 = " && ".join("(wi{i}==get_local_id({i}))".format(i=i) for i in range(3))
        cond2 = " && ".join(f"(get_group_id({i})==0)" for i in range(3))
        with self._if_(cond2):
            with self._for_(cond0.format(i=2)):
                with self._for_(cond0.format(i=1)):
                    with self._for_(cond0.format(i=0)):
                        if barrier:
                            self.barrier(_local=True)
                        with self._if_(cond1):
                            yield

    @contextmanager
    def _first_wg_execution_(self):
        cond = " && ".join(f"(0==get_group_id({i}))" for i in range(3))
        with self._if_(cond):
            yield

    @contextmanager
    def _first_wi_execution_(self):
        cond1 = " && ".join(f"(0==get_local_id({i}))" for i in range(3))
        cond2 = " && ".join(f"(0==get_group_id({i}))" for i in range(3))
        cond = f"{cond1} && {cond2}"
        with self._if_(cond):
            yield

    @staticmethod
    def _printv(ncomponents):
        assert ncomponents in [1, 2, 4, 8, 16]
        if ncomponents == 1:
            return ""
        else:
            return f"v{ncomponents}"
