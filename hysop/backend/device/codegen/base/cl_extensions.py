# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


_cl_khr_fp64_code = """
#if __OPENCL_VERSION__ < 120
    #if defined(cl_khr_fp64)
      #pragma OPENCL EXTENSION cl_khr_fp64 : enable
    #elif defined(cl_amd_fp64)
      #pragma OPENCL EXTENSION cl_amd_fp64 : enable
    #else
      #error Your OpenCL device is missing double precision extension!
    #endif
#endif
"""

_cl_extension_custom_declarations = {"cl_khr_fp64": _cl_khr_fp64_code}

from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.codegen.base.test import _test_typegen


class ClExtCodeGen(OpenClCodeGenerator):
    def __init__(self, ext_name):
        super().__init__(
            name=ext_name,
            typegen=OpenClTypeGen.devicelessTypegen(),
            declare_cl_exts=False,
        )
        self._generate_cl_extension(ext_name)

    def _generate_cl_extension(self, ext_name):
        with self._codeblock_("pragma_extensions"):
            if ext_name not in _cl_extension_custom_declarations.keys():
                with self._align_() as al:
                    base = """
#if defined({ext})
    #pragma OPENCL EXTENSION {ext}$ : enable
#else
    #error your opencl device is missing the {ext} extension!
#endif
"""
                    base = """
#pragma OPENCL EXTENSION {ext}$ : enable
"""
                    al.append(base.format(ext=ext_name))
            else:
                self.append(_cl_extension_custom_declarations[ext_name])
