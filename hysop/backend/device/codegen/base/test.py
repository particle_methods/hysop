# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.constants import np, BoundaryCondition
from hysop.tools.misc import upper_pow2_or_3
from hysop.backend.device.opencl import cl
from hysop.backend.device.codegen.structs.mesh_info import (
    MeshBaseStruct,
    MeshInfoStruct,
)
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.core.mpi import default_mpi_params


## Quickly get a default opencl typegen **ONLY** for testing or debugging purpose
##  => do not use in production code
def _test_typegen(fbtype="float", float_dump_mode="dec"):
    from hysop.backend.device.opencl.opencl_tools import get_or_create_opencl_env

    env = get_or_create_opencl_env(default_mpi_params())
    return OpenClTypeGen(
        context=env.context,
        device=env.device,
        platform=env.platform,
        fbtype=fbtype,
        float_dump_mode=float_dump_mode,
        unroll_loops=True,
        use_short_circuit_ops=False,
    )


def _test_mesh_info(name, typegen, dim, ghosts, resolution, **kargs):
    vsize = upper_pow2_or_3(dim)
    ghosts = [ghosts] * dim if np.isscalar(ghosts) else ghosts
    ghosts = np.asarray(ghosts)[:dim]

    resolution = [resolution] * dim if np.isscalar(resolution) else resolution
    resolution = np.asarray(resolution)[:dim]

    lboundary = [BoundaryCondition.PERIODIC] * dim
    rboundary = [BoundaryCondition.PERIODIC] * dim
    boundaries = (lboundary, rboundary)

    compute_resolution = resolution - 2 * ghosts

    xmin = np.zeros((dim,))
    xmax = np.ones((dim,))
    size = xmax - xmin
    dx = size / (compute_resolution - 1)

    start = ghosts
    stop = resolution - ghosts

    mbs = MeshBaseStruct(typegen, vsize)
    mis = MeshInfoStruct(typegen, vsize, mbs_typedef=mbs.typedef)

    # create a local numpy and a codegen MeshInfoStruct variable
    global_mesh = mbs.create(
        name="global",
        resolution=resolution,
        compute_resolution=compute_resolution,
        xmin=xmin,
        xmax=xmax,
        boundaries=boundaries,
        size=size,
        **kargs,
    )

    xmin -= ghosts * dx
    xmax += ghosts * dx

    local_mesh = mbs.create(
        name="local",
        resolution=resolution,
        compute_resolution=compute_resolution,
        xmin=xmin,
        xmax=xmax,
        boundaries=boundaries,
        size=size,
        **kargs,
    )

    (np_mis, cg_mis) = mis.create(
        name=name,
        dim=dim,
        start=start,
        stop=stop,
        ghosts=ghosts,
        dx=dx,
        local_mesh=local_mesh,
        global_mesh=global_mesh,
        **kargs,
    )

    return (np_mis, cg_mis)


def make_slice_views(compute_grid_size, lghosts=None, rghosts=None, step=None):
    compute_grid_size = np.asarray(compute_grid_size)
    dim = compute_grid_size.size

    if lghosts is None:
        lghosts = (0,) * dim
    elif np.isscalar(lghosts):
        lghosts = (lghosts,) * dim
    lghosts = np.asarray(lghosts)

    if rghosts is None:
        rghosts = (0,) * dim
    elif np.isscalar(rghosts):
        rghosts = (rghosts,) * dim
    rghosts = np.asarray(rghosts)

    if step is None:
        step = (1,) * dim
    elif np.isscalar(step):
        step = (step,) * dim
    step = np.asarray(step)

    view = [slice(lg, gs + lg) for lg, gs in zip(lghosts, compute_grid_size)]

    grid_size = compute_grid_size + lghosts + rghosts
    grid_shape = grid_size[::-1]

    return view[::-1], grid_size, grid_shape, lghosts, rghosts, step
