# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
import pyopencl as cl
from hysop.backend.device.codegen.base.codegen import CodeGenerator
from hysop.backend.device.opencl.opencl_types import TypeGen
from hysop.tools.henum import EnumFactory
from hysop.backend.device.codegen.base.variables import register_ctype_dtype


class EnumCodeGenerator(CodeGenerator):

    def __init__(
        self,
        enum,
        comments=None,
        ext=".c",
        initial_indent_level=0,
        escape_seqs=None,
        keywords=None,
    ):

        super().__init__(
            name=enum.name,
            typegen=TypeGen("float"),
            ext=ext,
            initial_indent_level=initial_indent_level,
            escape_seqs=escape_seqs,
            keywords=keywords,
        )

        if not isinstance(enum, EnumFactory.MetaEnum):
            raise ValueError(
                "Input enum should be generated with hysop.tools.henum.EnumFactory."
            )

        self.enum = enum

        self.dtype = self.enum.dtype
        self.fields = self.enum.fields
        self.rfields = self.enum.rfields
        self.__getitem__ = self.enum.__getitem__
        for k, v in self.fields().items():
            setattr(self, k, v)

        self.ctype = "enum " + self.enum.name

        register_ctype_dtype(self.ctype, self.dtype)
        self.gencode(comments)

    def gencode(self, comments):
        with self._codeblock_("enum_definitions"):
            if comments is not None:
                self.jumpline()
                self.comment(comments)
            if self.dtype != np.int32:
                prefix = f"enum {self.name} : {self.ctype(self.dtype)} "
            else:
                prefix = f"enum {self.name} "
            with self._block_(header_prefix=prefix, footer_postfix=";"):
                with self._align_() as al:
                    for v in sorted(self.fields().values()):
                        decl = f"{self.rfields()[v]}$ = ${v},"
                        al.append(decl)

    @staticmethod
    def ctype(dtype):
        if dtype == np.int32:
            return "int"
        elif dtype == np.int64:
            return "long long int"
        elif dtype == np.uint32:
            return "unsigned int"
        elif dtype == np.uint64:
            return "unsigned long long int"
        else:
            msg = "Not Implemented Yet!"
            raise NotImplemented(msg)


if __name__ == "__main__":

    from hysop.tools.enum import EnumFactory

    days = ["MONDAY", "TUESDAY"]
    enum = EnumFactory.create("Days", days)
    scg = EnumCodeGenerator(enum, comments="Days enumeration")
    scg.edit()
