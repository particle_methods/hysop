# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import itertools as it, sys, os, string, tempfile, operator
from contextlib import contextmanager
from subprocess import call

from hysop.tools.htypes import check_instance
from hysop.backend.device.opencl import cl
from hysop.backend.device.codegen.base.utils import WriteOnceDict, VarDict
from hysop.backend.device.codegen.base.variables import CodegenVariable
from hysop.core.mpi import main_rank


class CodeGenerator:

    default_escape_seqs = {"\t": " " * 4, "\n": "\n", " ": " "}
    default_keywords = {
        "inline": "inline",
    }
    default_block_priorities = {
        "pragma_extensions": -10000,
        "enum_definitions": -1000,
        "union_prototypes": -900,
        "struct_prototypes": -800,
        "function_prototypes": -700,
        "kernel_prototypes": -600,
        "union_declarations": -500,
        "struct_declarations": -400,
        "global_scope_constants": -300,
        "function_declarations": -200,
        "kernel_declarations": -100,
    }

    def __init__(
        self,
        name,
        typegen,
        ext=".tmp",
        known_vars=None,
        initial_indent_level=0,
        escape_seqs=None,
        keywords=None,
        **kargs,
    ):

        super().__init__(**kargs)
        self.name = name
        self.typegen = typegen
        self.fbtype = typegen.fbtype
        self.initial_indent_level = initial_indent_level
        self.ext = ext

        self.reset()
        if known_vars is not None:
            self.known_vars = known_vars
        self._configure(escape_seqs, keywords)

    def reset(self):
        self.code = ""
        self.indent_level = self.initial_indent_level
        self.prototypes = {}

        self.vars = VarDict()
        self.known_vars = WriteOnceDict()
        self.reqs = WriteOnceDict()

        self.init_blocks()

    def _configure(self, escape_seqs, keywords):
        if escape_seqs is None:
            self.escape_seqs = self.default_escape_seqs
        else:
            self.escape_seqs = escape_seqs

        if keywords is None:
            self.keywords = self.default_keywords
        else:
            self.keywords = keywords

    def init_blocks(self):
        self.blocks = {}
        self.block_priorities_override = {}
        self.last_block_priority = -1
        self.register_default_codeblock()
        return self

    def register_default_codeblock(self, name="default", priority=1000, comment=None):
        self.default_block_name = name
        self.default_block = [priority, "", comment]
        return self

    def inject_vars(self, new_vars):
        for varname in new_vars.keys():
            if varname in self.vars.keys():
                raise RuntimeError(f"Variable {varname} was already registered!")
            self.vars[varname] = new_vars[varname]
        return self

    def update_vars(self, new_vars=None, **kargs):
        if new_vars is None:
            new_vars = VarDict()
        new_vars.update(kargs)
        for varname in new_vars.keys():
            if varname in self.vars.keys():
                raise RuntimeError(f"Variable {varname} was already registered!")
            var = new_vars[varname]
            if varname in self.known_vars.keys():
                var.set_value(self.known_vars[varname])
            self.vars[varname] = var
        return self

    def update_requirements(self, reqs):
        for reqname, req in reqs.items():
            self.reqs[reqname] = req
        return self

    def require(self, name, codegen):
        try:
            self.reqs[name] = codegen
        except:
            pass
        return self

    def to_file(self, folder, filename):
        dst_file = folder + "/" + filename
        if not os.path.exists(folder) and (main_rank == 0):
            os.makedirs(folder)
        with open(dst_file, "w+") as f:
            f.write(self.__str__())
        return self

    def edit(self, editor=None, filepath=None, modified=False):
        if editor is None:
            default_editor = "vi"
            editor = os.environ.get("EDITOR", default_editor)
        source = (
            self.modified_code
            if modified and hasattr(self, "modified_code")
            else self.__str__()
        )
        file_ctx = (
            tempfile.NamedTemporaryFile(suffix=self.ext)
            if (filepath is None)
            else open(filepath, "w+")
        )
        with file_ctx as f:
            f.write(source.encode())
            f.flush()
            call([editor, f.name])
            f.seek(0)
            self.modified_code = f.read()

    def tab(self):
        return self.escape_seqs["\t"]

    def newl(self):
        return self.escape_seqs["\n"]

    def space(self):
        return self.escape_seqs[" "]

    def empty(self):
        return ""

    def current_indent(self):
        return self.tab() * self.indent_level

    def append(self, code, newline=True):
        if isinstance(code, str):
            code = [code]
        for line in code:
            self.code = self.code + self.current_indent() + line
            if newline:
                self.code += self.newl()
            else:
                self.code += self.space()
        return self

    def prepend(self, code):
        if isinstance(code, str):
            code = [code]
            for line in code[::-1]:
                self.code = (
                    self.tab() * self.initial_indent_level
                    + line
                    + self.newl()
                    + self.code
                )
        return self

    def jumpline(self, count=1):
        lines = [self.empty()] * count
        self.append(lines)
        return self

    def indent(self, count=1):
        self.indent_level += count
        return self

    def dedent(self, count=1):
        self.indent_level -= count
        return self

    def supress_newline(self):
        k = self.code.rfind(self.newl())
        if k > 0:
            self.code = self.code[:k]
        return self

    def _noop(self):
        code = ";"
        self.append(code)

    def _continue(self):
        code = "continue;"
        self.append(code)

    def _break(self):
        code = "break;"
        self.append(code)

    def pragma(self, what):
        code = f"#pragma {what}"
        self.append(code)

    def define(self, what, prepend=True):
        code = f"#define {what}"
        self.append(code)

    def include(self, *args):
        code = []
        for k in args:
            code.append(f"#include {k}")
        return self.append(code)

    def comment(
        self,
        comments,
        simple=False,
        force_spacing=True,
        align=True,
        upperband=False,
        lowerband=False,
        prepend=False,
    ):

        comments = comments.split("\n")
        code = []
        spaces = [" ", "\t"]
        maxlen = 0
        for c in comments:
            if c == "":
                c = " "
            if force_spacing:
                c_prefix = " " if (c[0] not in spaces) else ""
                c_postfix = " " if (c[-1] not in spaces) else ""
                c = c_prefix + c + c_postfix
            if simple:
                comment = "//" + c
            else:
                comment = "/*" + c
                if not align:
                    comment += "*/"
                elif len(comment) > maxlen:
                    maxlen = len(comment)
            code.append(comment)

        if not simple and align:
            for i, c in enumerate(code):
                code[i] = c + " " * (maxlen - len(c)) + "*/"
            band = "/*" + "*" * (maxlen - 2) + "*/"
            if lowerband:
                code.append(band)
            if upperband:
                code.insert(0, band)
        if prepend:
            self.prepend(code)
        else:
            self.append(code)

    def decl_vars(self, *variables, **kargs):
        codegen = kargs.pop("codegen", None)
        align = kargs.pop("align", False)
        assert len({var.base_ctype() for var in variables}) == 1
        base = variables[0].base_ctype()
        svars = []
        for var in variables:
            check_instance(var, CodegenVariable)
            svars.append(var.declare(multidecl=True, **kargs))
        decl = "{} ${};".format(base, ", ".join(svars))
        if not align:
            decl = decl.replace("$", "")
        if codegen is None:
            self.append(decl)
        else:
            codegen.append(decl)

    def decl_aligned_vars(self, *variables, **kargs):
        if len(variables) == 0:
            return
        jmp = kargs.pop("jmp", True)
        try:
            with self._align_() as al:
                for var in variables:
                    check_instance(var, CodegenVariable)
                    var.declare(codegen=al, align=True, **kargs)
                if jmp:
                    al.jumpline()
        except:
            raise

    class VarBlock:
        def __init__(self):
            self._types = []
            self._varnames = []
            self._inits = []
            self._comments = []
            self._cv_qualifiers = []

        def decl_var(
            self, _type, _varnames, _inits=None, comment=None, cv_qualifier=None
        ):
            self._varnames.append(_varnames)
            self._inits.append(_inits)
            self._types.append(_type)
            self._comments.append(comment)
            self._cv_qualifiers.append(cv_qualifier)

        def code(self):
            maxlen = lambda L: (
                max(len(s) if s is not None else 0 for s in L) if L else 0
            )
            allnone = lambda L: all([s is None for s in L]) if L else True
            entries = ["cv_qualifiers", "types", "varnames", "inits", "comments"]
            kargs = ""
            line = ""
            for e in entries:
                Le = eval(f"self._{e}")
                ee = e + ":" + str(maxlen(Le) + 1)
                if not allnone(Le):
                    if e == "inits":
                        line += "= {" + ee + "} "
                    elif e == "comments":
                        line += " /* {" + ee + "} */"
                    else:
                        line += "{" + ee + "} "
                    kargs += f"{e}=self._{e}[i],"
                if e == "inits":
                    for i, v in enumerate(self._varnames):
                        self._varnames[i] = v + ";"

            kargs = kargs[:-1].replace("[i]", "[{i}]")
            lines = []
            for i in range(len(self._varnames)):
                l = eval("line.format(" + kargs.format(i=i) + ")")
                lines.append(l)
            return lines

    @contextmanager
    def _var_block_(self):
        vb = self.VarBlock()
        yield vb
        self.append(vb.code())

    class AlignBlock:
        def __init__(self, sep="$"):
            self.sep = sep
            self._lines = []
            self._parts_count = None

        def jumpline(self, count=1):
            lines = "" + "\n" * (count - 1)
            self.append(lines)
            return self

        def append(self, lines):
            lines = lines.split("\n")
            if not isinstance(lines, list):
                lines = [lines]
            for line in lines:
                self._append(line)

        def _append(self, line):
            line = line.split(self.sep)
            if len(line) > 1:
                N = len(line)
                if self._parts_count is None:
                    self._parts_count = N
                elif N != self._parts_count:
                    raise ValueError(
                        f"Alignment count mismatch N={N} vs already initialized count={self._parts_count}."
                    )
            self._lines.append(line)

        def code(self):
            if self._parts_count is None:
                # msg='Call at least one append() before closing an _align_!'
                # msg+='\n got: {}'.format(self._lines)
                # raise RuntimeError(msg)
                return []

            maxlen = lambda i: max(
                len(line[i]) for line in self._lines if len(line) > 1
            )
            line_str = ""
            for i in range(self._parts_count):
                ml = maxlen(i)
                if ml == 0:
                    line_str += "{}"
                else:
                    line_str += "{:" + str(ml) + "}"
            code = []
            for line in self._lines:
                if len(line) > 1:
                    code.append(line_str.format(*line))
                else:
                    code.append(line[0])
            return code

    @contextmanager
    def _align_(self, sep="$"):
        ab = self.AlignBlock(sep=sep)
        yield ab
        self.append(ab.code())

    def align(self, *args):
        with self._align_() as al:
            for arg in args:
                al.append(arg)

    def declare_codeblocks(
        self, names, initial_code=None, priorities=None, comments=None
    ):
        if not isinstance(names, list):
            names = [names]
            priorities = [priorities]
            initial_code = [initial_code]
            comments = [comments]
        for i, blockname in enumerate(names):
            if priorities is None:
                self.last_block_priority += 1
                priority = self.last_block_priority
            else:
                priority = priorities[i]
                self.last_block_priority = max(priority, self.last_block_priority)
            if comments is None:
                comment = None
            else:
                comment = comments[i]
            if initial_code is None:
                code = ""
            else:
                code = initial_code[i]
            self.blocks[blockname] = [priority, code, comment]
        return self

    @contextmanager
    def _codeblock_(self, blockname, priority=None):
        if blockname not in self.blocks:
            comment = blockname
            if blockname in self.default_block_priorities.keys():
                priority = self.default_block_priorities[blockname]
            if priority is None:
                # self.last_block_priority += 1
                # priority = self.last_block_priority
                raise ValueError("Unknown block {}".format())
            self.blocks[blockname] = [priority, "", comment]
        elif priority is not None and self.blocks[blockname][0] != priority:
            raise ValueError("Priority mismatch!")

        code = self.code
        indent = self.indent_level
        self.indent_level = self.initial_indent_level
        self.code = self.blocks[blockname][1]
        yield
        self.blocks[blockname][1] = self.code
        self.code = code
        self.indent_level = indent

    def block_exists(self, name):
        return name in self.blocks

    def check_block_exists(self, name):
        if not self.block_exists(name):
            raise ValueError(f"Block {name} does not exist!")

    def declare_prototype(self, prototype_declaration, category="default"):
        blockname = f"{category}_prototypes"
        default_block_priorities = self.default_block_priorities
        priority = (
            default_block_priorities[blockname]
            if blockname in default_block_priorities
            else 0
        )
        with self._codeblock_(blockname, priority=priority):
            self.append(prototype_declaration)
        return self

    def override_block_priorities(self, **kargs):
        self.block_priorities_override.update(kargs)

    ## Context managers
    # base context manager to create blocks
    @contextmanager
    def _block_(
        self, header_prefix="", header_postfix="", footer_postfix="", compact=False
    ):
        count = 1 - int(compact)
        newline = not compact

        header = header_prefix + "{" + header_postfix
        self.append(header.split("\n"), newline).indent(count)
        yield self
        if compact:
            self.supress_newline()
            self.code += " "
        footer = "}" + footer_postfix
        self.dedent(count).append(footer)

    # conditional facilities
    @contextmanager
    def _if_(self, cond, compact=False, force_spaces=False):
        if cond == "true":
            yield
        elif cond == "false":
            code = self.code
            indent = self.indent_level
            self.code = ""
            yield
        else:
            sep = self.space() if force_spaces else self.empty()
            header_prefix = f"if ({sep}{cond}{sep}) "
            with self._block_(header_prefix=header_prefix, compact=compact) as b:
                yield b
        if cond == "false":
            self.code = code
            self.indent_level = indent

    @contextmanager
    def _elif_(self, cond, compact=False, force_spaces=False):
        sep = self.space() if force_spaces else self.empty()
        header_prefix = f"else if ({sep}{cond}{sep}) "
        with self._block_(header_prefix=header_prefix, compact=compact) as b:
            yield b

    @contextmanager
    def _else_(self, compact=False):
        header_prefix = "else "
        with self._block_(header_prefix=header_prefix, compact=compact) as b:
            yield b

    # class / struct facilities
    @contextmanager
    def _struct_(self, name, variables=None, typedef=None):
        if name == "" and typedef is None:
            raise ValueError("Cannot define a non typedefed anonymous struct!")

        header_prefix = "struct " + name
        if typedef:
            declaration = "typedef " + header_prefix + " " + typedef + ";"
        else:
            declaration = header_prefix + ";"
        self.declare_prototype(declaration, "struct")

        with self._codeblock_("struct_declarations"):
            with self._block_(
                header_prefix=header_prefix + " ", footer_postfix=";", compact=False
            ) as b:
                if variables is not None:
                    for v in variables:
                        self.append(v + ";")
                yield b

    # union facilities
    @contextmanager
    def _union_(self, name, variables=None, typedef=None):
        if name == "" and typedef is None:
            raise ValueError("Cannot define a non typedefed anonymous struct!")

        header_prefix = "union " + name
        if typedef:
            declaration = "typedef " + header_prefix + " " + typedef + ";"
        else:
            declaration = header_prefix + ";"
        self.declare_prototype(declaration, "union")

        with self._codeblock_("union_declarations"):
            with self._block_(
                header_prefix=header_prefix + " ", footer_postfix=";", compact=False
            ) as b:
                if variables is not None:
                    for v in variables:
                        self.append(v + ";")
                yield b

    # looping facilities
    @contextmanager
    def _for_(self, custom, compact=False, unroll=False):
        header_prefix = "for (" + custom + ") "
        if (unroll is not None) and (unroll is not False):
            if unroll is True:
                header_prefix = f"#pragma unroll\n{header_prefix}"
            elif isinstance(unroll, int):
                header_prefix = f"#pragma unroll {unroll}\n{header_prefix}"
        with self._block_(header_prefix=header_prefix, compact=compact) as b:
            yield b

    @contextmanager
    def _while_(self, custom, compact=False):
        header_prefix = "while (" + custom + ") "
        with self._block_(header_prefix=header_prefix, compact=compact) as b:
            yield b

    @contextmanager
    def _do_while_(self, custom, compact=False):
        header_prefix = "do "
        footer_postfix = " while (" + custom + ");"
        with self._block_(
            header_prefix=header_prefix, footer_postfix=footer_postfix, compact=compact
        ) as b:
            yield b

    # function facilities
    @contextmanager
    def _function_(
        self,
        name,
        output,
        args=[],
        args_impl=None,
        arg_spaces=True,
        inline=False,
        compact=False,
    ):
        def filter_args(_args):
            if not args:
                newargs = [""]
            else:
                newargs = []
                for i, arg in enumerate(_args):
                    newl = arg[-1] == self.newl()
                    arg = arg[:-1] if newl else arg
                    newargs.append(arg)
                    if newl and i != len(_args) - 1:
                        newargs.append(self.newl())
            return newargs

        args = filter_args(args)
        if not args_impl:
            args_impl = args
        else:
            args_impl = filter_args(args_impl)

        if arg_spaces:
            comma = ", "
        else:
            comma = ","

        sprefix = "{inline}{output} {name}"
        sfun = "{prefix}({args})"
        inline = self.keywords["inline"] + " " if inline else ""

        prefix = sprefix.format(inline=inline, output=output, name=name)
        indent = len(self.current_indent() + prefix) * " " + " "
        if args:
            pargs = [
                arg + comma if arg != self.newl() else arg + indent for arg in args[:-1]
            ] + [args[-1]]
        else:
            pargs = []
        prototype = sfun.format(prefix=prefix, args="".join(pargs))
        prototype += ";"

        prefix = sprefix.format(inline="", output=output, name=name)
        indent = len(self.current_indent() + prefix) * " " + " "
        if args_impl:
            dargs = [
                arg + comma if arg != self.newl() else arg + indent
                for arg in args_impl[:-1]
            ] + [args_impl[-1]]
        else:
            dargs = []
        definition = sfun.format(prefix=prefix, args="".join(dargs))

        self.declare_prototype(prototype, "function")

        with self._codeblock_("function_declarations"):
            with self._block_(header_prefix=definition + " ", compact=compact) as b:
                yield b
            if not compact:
                self.jumpline()

    def generate(self, blocks, genset):
        for req in self.reqs.values():
            req.generate(blocks, genset)

        if self.name in genset:
            return

        dname = self.default_block_name
        (prio, _, self_com) = self.default_block
        poverride = (
            self.block_priorities_override[dname]
            if dname in self.block_priorities_override
            else None
        )
        if dname in blocks.keys():
            (prio, code, com) = blocks[dname]
            blocks[dname] = [
                poverride if poverride else prio,
                code + self.code,
                self_com,
            ]
        else:
            blocks[dname] = [poverride if poverride else prio, self.code, self_com]

        for blk_name, blk_val in self.blocks.items():
            priority, self_code, self_com = blk_val
            poverride = (
                self.block_priorities_override[blk_name]
                if blk_name in self.block_priorities_override
                else None
            )
            if blk_name in blocks.keys():
                (priority, code, com) = blocks[blk_name]
                blocks[blk_name] = [
                    poverride if poverride else priority,
                    code + self_code,
                    self_com if self_com != "" else com,
                ]
            else:
                blocks[blk_name] = [
                    poverride if poverride else priority,
                    self_code,
                    self_com,
                ]

        genset.add(self.name)

    def __str__(self):
        blocks = {}
        genset = set()
        self.generate(blocks, genset)
        if blocks:
            entire_code = ""
            blocks = tuple(sorted(blocks.items(), key=operator.itemgetter(1)))
            for bn, (priority, code, comment) in blocks:
                if not code:
                    continue
                if comment:
                    entire_code += f"/* {comment} */\n"
                entire_code += code + self.newl()

        else:
            entire_code = self.code
        return entire_code


if __name__ == "__main__":

    from func.empty import EmptyFunction

    cg = CodeGenerator(name="test generator", ext=".c")
    cg.declare_codeblocks(
        names=["top", "footer"], priorities=[10, 40]
    ).register_default_codeblock("body", 30).override_block_priorities(
        struct_prototypes=20, function_prototypes=21, kernel_prototypes=22
    )

    with cg._codeblock_("top"):
        cg.jumpline()
        cg.comment(
            "This is a test comment!\nIt is aligned even with different sizes!",
            upperband=True,
            lowerband=True,
        )
        cg.jumpline()

        cg.include("<stdio.h>", "<stdbool.h>")
        cg.jumpline()

    cg.comment("A simple struct:", simple=True)
    with cg._struct_(name="TestStruct"):
        cg.append("int i,j,k;")
        cg.append("double d;")
    cg.jumpline()

    cg.comment("Test of block compact mode:", simple=True)
    with cg._function_(
        "add", "double", ["double lhs", "const double rhs"], inline=True, compact=True
    ):
        cg.append("return lhs+rhs;")
    cg.jumpline()

    cg.comment("Without compact mode:")
    with cg._function_("get_number", "int"):
        cg.append("int k = 42;")
        with cg._for_("int i=0; i<10; i++"):
            cg.append("k++;")
        cg.append("return k;")
    cg.jumpline()

    cg.comment("Nested blocks:", simple=True)
    with cg._block_():
        with cg._block_():
            with cg._block_():
                cg.append("i++;")
    cg.jumpline()

    cg.comment("Conditionals!", simple=True)
    cg.append("const unsigned int N = 100u;")
    cg.append("unsigned int i = 0u;")
    with cg._while_("true"):
        with cg._if_("i==N"):
            cg._break()
        with cg._elif_("i%42==12"):
            cg.append("i+=2")
            cg._continue()
        with cg._else_():
            cg.append("i++;")

    # test requirements feature (include another codegenerator and merge blocks at code generation)
    ef = EmptyFunction()
    ef.jumpline()
    ef.register_default_codeblock(
        cg.default_block_name
    )  # share the same default codeblock for the two generators
    cg.require(ef)
    cg.require(
        ef
    )  # multiple dependency on the same generator are ignored (name based lookup)

    # edit the current code directly by hand in a temporary file !
    # cg.edit()

    # save the file
    cg.to_file(".", "test.c")

    # output the generated code to stdout
    # print(cg)
