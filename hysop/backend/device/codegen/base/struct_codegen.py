# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import pyopencl as cl
import pyopencl.tools

import numpy as np
import re

from hysop.backend.device.opencl.opencl_types import np_dtype
from hysop.backend.device.codegen.base.opencl_codegen import OpenClCodeGenerator
from hysop.backend.device.codegen.base.variables import (
    VarDict,
    CodegenVariable,
    CodegenVector,
    CodegenStruct,
    CodegenVectorClBuiltin,
)
from hysop.backend.device.codegen.base.variables import register_ctype_dtype


class StructCodeGenerator(OpenClCodeGenerator):
    def __init__(
        self,
        name,
        dtype,
        typegen,
        typedef=None,
        comments=None,
        ctype_overrides=None,
        custom_types={},
    ):

        super().__init__(name=name, typegen=typegen)

        self.typedef = typedef
        self.dtype = np.dtype(dtype)
        self.ctype = self.typedef if self.typedef else f"struct {self.name}"

        cl.tools.get_or_register_dtype(self.ctype, self.dtype)
        register_ctype_dtype(self.ctype, self.dtype)

        for _ctype, _dtype in custom_types.items():
            cl.tools.get_or_register_dtype(_ctype, dtype=_dtype)

        self.gencode(comments, ctype_overrides)

    def fields(self):
        return self.dtype.fields

    def c_decl(self):
        assert self.context is not None
        (dtype, cdecl) = cl.tools.match_dtype_to_c_struct(
            self.device, self.ctype.replace("struct", ""), self.dtype, self.context
        )
        return cdecl

    def gencode(self, comments, ctype_overrides):
        struct_vars = re.compile(
            r"\s+((?:struct\s+)?\w+)\s+((?:\s*\**(?:\w+)(?:\[\d+\])*[,;])+)"
        )
        lines = self.c_decl().split("\n")

        with self._struct_(name=self.name, typedef=self.typedef):
            with self._var_block_() as vb:
                i = 0
                for l in lines:
                    match = struct_vars.match(l)
                    if match:
                        ctype = match.group(1)
                        variables = match.group(2).replace(";", "").split(",")
                        if (ctype_overrides is not None) and (
                            i in ctype_overrides.keys()
                        ):
                            ctype = ctype_overrides[i]
                        if comments is not None:
                            vb.decl_var(ctype, ",".join(variables), comment=comments[i])
                        else:
                            vb.decl_var(ctype, ",".join(variables))
                        i += 1

    def build_codegen_variable(self, name, **kargs):
        return CodegenStruct(name=name, struct=self, **kargs)


if __name__ == "__main__":

    from hysop.backend.device.codegen.base.test import _test_typegen

    tg = _test_typegen()

    dtype = []
    dtype.append(("f0", np.float32))
    dtype.append(("d0", np.float64))
    dtype.append(("v0", np_dtype("float4")))
    dtype.append(("v1", np_dtype("int16")))

    scg = StructCodeGenerator("TestStruct", dtype, typedef=None, typegen=tg)
    scg.edit()
