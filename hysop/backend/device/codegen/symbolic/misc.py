# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.backend.device.codegen.symbolic.expr import TypedI
from hysop.symbolic.misc import BroadCast, Expand, Select


class OpenClBroadCast(TypedI, BroadCast):
    """v=v.xy => v.xyxy"""

    def __new__(cls, ctype, expr, factor):
        obj = super().__new__(cls, expr, factor)
        obj.ctype = cls.vtype(expr.basetype, factor * expr.components)
        return obj

    def _ccode(self, printer):
        expr = self.expr
        val = printer._print(expr)
        if self.factor > 1:
            if self.expr.components > 1:
                indices = tuple(
                    i for j in range(self.factor) for i in range(expr.components)
                )
                mode = "hex" if (expr.components > 4) else "pos"
                bc = "({}).{}{}".format(
                    val,
                    printer.typegen.vtype_access(indices[0], expr.components, mode),
                    "".join(
                        printer.typegen.vtype_component_adressing(i, mode)
                        for i in indices[1:]
                    ),
                )
            else:
                bc = f"(({self.ctype})({val}))"
        else:
            bc = val
        return bc


class OpenClExpand(TypedI, Expand):
    """v=v.xy => v.xxyy"""

    def __new__(cls, ctype, expr, factor):
        obj = super().__new__(cls, expr, factor)
        obj.ctype = cls.vtype(expr.basetype, factor * expr.components)
        return obj

    def _ccode(self, printer):
        expr = self.expr
        val = printer._print(expr)
        if self.factor > 1:
            if self.expr.components > 1:
                indices = tuple(
                    i for i in range(expr.components) for j in range(self.factor)
                )
                mode = "hex" if (expr.components > 4) else "pos"
                bc = "({}).{}{}".format(
                    val,
                    printer.typegen.vtype_access(indices[0], expr.components, mode),
                    "".join(
                        printer.typegen.vtype_component_adressing(i, mode)
                        for i in indices[1:]
                    ),
                )
            else:
                bc = f"(({self.ctype})({val}))"
        else:
            bc = val
        return bc


class OpenClSelect(TypedI, Select):
    def __new__(cls, ctype, a, b, c):
        obj = super().__new__(cls, a, b, c)
        assert a.ctype == b.ctype, f"{a.ctype} != {b.ctype}"
        obj.ctype = a.ctype
        return obj

    def _ccode(self, printer):
        return "select({}, {}, {})".format(
            printer._print(self.a), printer._print(self.b), printer._print(self.c)
        )
