# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sympy as sm

from hysop.tools.numpywrappers import npw
from hysop.tools.htypes import check_instance, first_not_None

from hysop.backend.device.codegen.base.function_codegen import (
    OpenClFunctionCodeGenerator,
)
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.codegen.base.utils import ArgDict, WriteOnceDict
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
)
from hysop.backend.device.codegen.symbolic.functions.custom_symbolic_function import (
    CustomSymbolicFunction,
)
from hysop.backend.device.codegen.symbolic.expr import (
    OpenClPrinter,
    Return,
    NumericalConstant,
)

from hysop.symbolic.misc import ApplyStencil

from hysop.backend.device.codegen.symbolic.expr import FunctionCall, OpenClVariable
from hysop.backend.device.codegen.symbolic.relational import OpenClMul, OpenClAdd


class CustomApplyStencilFunction(CustomSymbolicFunction):
    def __init__(self, csc, name, expr, target_ctype=None, **kwds):
        check_instance(expr, ApplyStencil)
        super().__init__(
            csc=csc, name=name, expr=expr, target_ctype=target_ctype, **kwds
        )

    def parse_expr(self, csc, name, expr, args, reqs):
        if isinstance(expr, ApplyStencil):
            stencil = expr.stencil
            fn, fn_kwds = self.new_function(csc, name, expr.expr, args, reqs)
            assert "offset" in fn_kwds

            def fn_call(i, fn_kwds):
                _fn_kwds = fn_kwds.copy()
                _fn_kwds["offset"] = "{}+{}".format(
                    fn_kwds["offset"], csc.typegen.dump(int(i))
                )
                return FunctionCall(fn.ctype, fn, _fn_kwds)

            factor = self.parse_expr(csc, name, stencil.factor, args, reqs)
            pexprs = ()
            for i, coeff in stencil.items(include_factor=False):
                pexprs += (
                    OpenClMul(
                        fn.ctype,
                        NumericalConstant.build(coeff, csc.typegen),
                        fn_call(i[0], fn_kwds),
                    ),
                )
            pexpr = OpenClAdd(fn.ctype, *pexprs)
            pexpr = OpenClMul(fn.ctype, factor, pexpr)
        else:
            pexpr = super().parse_expr(csc, name, expr, args, reqs)
        return pexpr
