# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sympy as sm

from hysop.tools.numpywrappers import npw
from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.numerics import is_fp, is_signed, is_unsigned, get_dtype, is_complex
from hysop.backend.device.codegen.base.function_codegen import (
    OpenClFunctionCodeGenerator,
)
from hysop.backend.device.opencl.opencl_types import OpenClTypeGen
from hysop.backend.device.codegen.base.utils import ArgDict, WriteOnceDict
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
)
from hysop.backend.device.codegen.functions.vload import Vload

from hysop.symbolic.base import UnsplittedExpr
from hysop.symbolic.parameter import SymbolicScalarParameter, SymbolicTensorParameter
from hysop.symbolic.field import SymbolicDiscreteField
from hysop.symbolic.misc import ApplyStencil, Cast
from hysop.symbolic.array import (
    OpenClSymbolicArray,
    OpenClSymbolicBuffer,
    OpenClSymbolicNdBuffer,
)
from hysop.symbolic.tmp import TmpScalar
from hysop.symbolic.constant import SymbolicConstant
from hysop.symbolic.spectral import WaveNumberIndex

from hysop.backend.device.codegen.symbolic.expr import (
    VLoad,
    VStore,
    VStoreIf,
    VLoadIf,
    OpenClVariable,
    OpenClPrinter,
    TypedI,
    OpenClAssignment,
    OpenClCast,
    OpenClIndexedVariable,
    NumericalConstant,
    FloatingPointConstant,
    IntegerConstant,
    FunctionCall,
    Return,
    ComplexFloatingPointConstant,
)

from hysop.backend.device.codegen.symbolic.map import map_expression, OpenClCastUtils
from hysop.backend.device.codegen.base.variables import dtype_to_ctype, ctype_to_dtype


class CustomSymbolicFunction(OpenClFunctionCodeGenerator):

    @classmethod
    def varname(cls, name):
        _name = name.lower()
        if _name == name:
            _name = f"_{_name}"
        return _name

    @classmethod
    def field_name(cls, field, index):
        return cls.varname(f"{field.var_name}_{index}")

    @classmethod
    def array_name(cls, array):
        return cls.varname(array.varname)

    @classmethod
    def default_out_of_bounds_value(cls, dtype):
        if is_complex(dtype):
            if dtype is npw.complex64:
                return "(float2)(NAN, NAN)"
            elif dtype is npw.complex128:
                return "(double2)(NAN, NAN)"
            else:
                msg = f"Unsupported complex dtype {dtype}."
                raise NotImplementedError(msg)
        elif is_fp(dtype):
            # nan will propagate NaNs instead of infty which may disapear.
            dval = "NAN"
        elif is_unsigned(dtype):
            # uint max
            dval = "0x" + "".join(("ff",) * npw.dtype(dtype).itemsize)
        elif is_signed(dtype):
            # int max
            dval = "0x7f" + "".join(("ff",) * (npw.dtype(dtype).itemsize - 1))
        else:
            msg = f"Dtype {dtype} is not signed, unsigned or floating point..."
            raise TypeError(msg)
        return dval

    def __init__(
        self, csc, name, expr, target_ctype=None, inline=True, known_args=None
    ):
        from hysop.backend.device.codegen.kernels.custom_symbolic import (
            SymbolicCodegenContext,
        )

        check_instance(csc, SymbolicCodegenContext)
        check_instance(name, str)
        check_instance(target_ctype, str, allow_none=True)

        args = ArgDict()
        reqs = WriteOnceDict()

        self.stencils = {}
        self.__fn_counter = 0
        self.known_args = known_args

        pexpr = self.parse_expr(csc, name, expr, args, reqs)

        if (target_ctype is not None) and (pexpr.ctype != target_ctype):
            pexpr = OpenClCastUtils.promote_to(pexpr, target_ctype)

        self.is_fcall = isinstance(pexpr, FunctionCall)
        self.pexpr = pexpr

        super().__init__(
            basename=name,
            output=pexpr.ctype,
            args=args,
            typegen=csc.typegen,
            inline=inline,
            known_args=known_args,
        )

        self.update_requirements(reqs)

        self.gencode(csc, pexpr)
        self.ctype = pexpr.ctype

    @classmethod
    def fmt_args(cls, args):
        new_args = ArgDict()
        argsmap = {}
        for i, (argname, arg) in enumerate(args.items()):
            new_argname = f"a{i}"
            new_arg = arg.newvar(name=new_argname)
            new_args[new_argname] = new_arg
            argsmap[argname] = new_argname
        return new_args, argsmap

    def fmt_kwds(self, kwds):
        new_kwds = {}
        for vname, val in kwds.items():
            new_kwds[self.argsmap[vname]] = val
        return new_kwds

    def get_stencil_fn(self, csc, expr):
        from hysop.backend.device.codegen.symbolic.functions.apply_stencil import (
            CustomApplyStencilFunction,
        )

        stencil = expr.stencil
        dummy_stencil_fn = CustomApplyStencilFunction(
            csc=csc, name="dummy", expr=expr, known_args=self.known_args
        )
        _hash = hash(str(dummy_stencil_fn))
        if _hash in self.stencils:
            stencil_fn = self.stencils[_hash]
        else:
            stencil_name = f"stencil_{len(self.stencils)}"
            stencil_fn = CustomApplyStencilFunction(
                csc, stencil_name, expr, known_args=self.known_args
            )
            self.stencils[_hash] = stencil_fn
        return stencil_fn

    def new_function(self, csc, basename, expr, args, reqs, **kwds):
        fn_counter = self.__fn_counter
        fname = f"{basename}_{fn_counter}"
        fn = CustomSymbolicFunction(csc, fname, expr, known_args=self.known_args)
        fn_kwds = fn.args.copy()
        top_fn = fn
        if kwds:
            s_fn_kwds = {k: f"({v})" for (k, v) in fn_kwds.items()}
            fn_kwds = {}
            for argname, argval in fcall.fn_kwds.items():
                argval = str(argval)
                for an, av in sorted(s_fn_kwds.items(), key=lambda x: len(x[0])):
                    argval.replace(an, av)
                fn_kwds[argname] = argval
        while isinstance(fn, CustomSymbolicFunction) and fn.is_fcall:
            fcall = fn.pexpr
            ctype = fn.ctype
            fn = fcall.fn
            s_fn_kwds = {k: f"({v})" for (k, v) in fn_kwds.items()}
            fn_kwds = {}
            for argname, argval in fcall.fn_kwds.items():
                argval = str(argval)
                for an, av in sorted(s_fn_kwds.items(), key=lambda x: len(x[0])):
                    argval.replace(an, av)
                fn_kwds[argname] = argval
            fn.ctype = ctype
        if fn.name in reqs:
            fn = reqs[fn]
        else:
            self.check_and_set(reqs, fn.name, fn)
            self.__fn_counter += 1
        for argname, arg in top_fn.args.items():
            self.check_and_set(args, argname, arg)
        return fn, fn_kwds

    @classmethod
    def check_and_set(cls, dic, key, value):
        if key in dic:
            ref = dic[key]
            if isinstance(ref, CodegenVariable) and isinstance(value, CodegenVariable):
                if (ref.ctype == value.ctype) and (ref.name == value.name):
                    return
            if ref != value:
                msg = "Overwrite for key {}: expected '{}' ({}) but got '{}' ({})."
                msg = msg.format(key, ref, type(ref), value, type(value))
                raise ValueError(msg)
        else:
            dic[key] = value

    def parse_expr(self, csc, name, expr, args, reqs):
        pexpr = None
        if isinstance(expr, (int, sm.Integer)):
            pexpr = IntegerConstant("int", expr)
        elif isinstance(expr, (float, sm.Rational, sm.Float)):
            pexpr = FloatingPointConstant(csc.typegen.fbtype, expr)
        elif isinstance(expr, complex):
            pexpr = ComplexFloatingPointConstant(csc.typegen.fbtype + "2", expr)
        elif isinstance(expr, npw.number):
            ctype = dtype_to_ctype(expr.dtype)
            pexpr = NumericalConstant(ctype, expr)
        elif isinstance(expr, SymbolicConstant):
            expr.assert_bound()
            pexpr = self.parse_expr(csc, name, expr.value, args, reqs)
        elif isinstance(expr, Cast):
            target_ctype = dtype_to_ctype(expr.dtype)
            pexpr = self.parse_expr(csc, name, expr.expr, args, reqs)
            pexpr = OpenClCastUtils.promote_to(pexpr, target_ctype)
        elif isinstance(expr, SymbolicScalarParameter):
            ctype = expr.parameter.ctype
            param = expr.parameter
            index = expr.idx

            pname = param.name
            var = csc.param_args[pname]

            self.check_and_set(args, pname, var)

            if index is None:
                pexpr = OpenClVariable(ctype, var)
            else:
                shape = param.shape
                dtype = param.dtype
                value = first_not_None(param.value, npw.empty(shape, dtype=dtype))
                strides = npw.asarray(value.strides) // dtype.itemsize
                index = npw.dot(index, strides)
                pexpr = OpenClIndexedVariable(ctype, var, index)
        elif isinstance(expr, SymbolicDiscreteField):
            index, field = expr.index, expr.field
            fname = self.field_name(field, index)
            var = csc.field_args[fname]
            self.check_and_set(args, fname, var)
            if var.is_ptr:
                dtype = var.dtype
                dval = self.default_out_of_bounds_value(dtype)
                assert csc.local_size_known
                size = csc.array_size(field, index)
                ghosts = csc.array_ghost(field, index)
                vload = Vload(
                    csc.typegen,
                    var.ctype,
                    csc.vectorization,
                    default_val=dval,
                    itype=csc.itype,
                    restrict=True,
                    storage=var.storage,
                    known_args=dict(size=size),
                )
                if vload.name in reqs:
                    vload = reqs[vload.name]
                else:
                    reqs[vload.name] = vload
                for argname, arg in vload.args.items():
                    if argname == "data":
                        argname = var.name
                        arg = var
                    self.check_and_set(args, argname, arg)
                fkwds = vload.args.copy()
                fkwds["data"] = var
                fkwds["offset"] = f"{csc.offset}+{ghosts}"
                pexpr = FunctionCall(vload.ctype, vload, fkwds)
            else:
                pexpr = OpenClVariable(var.ctype, var)
        elif isinstance(expr, OpenClSymbolicArray):
            array = expr
            aname = self.array_name(array)
            var = csc.array_args[aname]
            self.check_and_set(args, aname, var)
            if var.is_ptr:
                raise NotImplementedError()
            else:
                pexpr = OpenClVariable(var.ctype, var)
        elif isinstance(expr, (OpenClSymbolicBuffer, OpenClSymbolicNdBuffer)):
            buf = csc.buffer_args[expr]
            self.check_and_set(args, buf.name, buf)
            pexpr = OpenClVariable(buf.ctype, buf)
        elif isinstance(expr, TmpScalar):
            scalar = csc.scalar_args[expr.varname]
            self.check_and_set(args, scalar.name, scalar)
            pexpr = OpenClVariable(scalar.ctype, scalar)
        elif isinstance(expr, sm.Indexed):
            array, index = expr.args
            if isinstance(array, sm.IndexedBase):
                assert len(array.args) == 1, array.args
                array = array.args[0]
            parray = self.parse_expr(csc, name, array, args, reqs)
            pindex = self.parse_expr(csc, name, index, args, reqs)
            assert parray.var.is_ptr, parray.var.full_ctype()
            assert csc.typegen.basetype(pindex.ctype) in (
                "char",
                "uchar",
                "short",
                "ushort",
                "int",
                "uint",
                "long",
                "ulong",
            )
            pexpr = OpenClIndexedVariable(parray.ctype, parray, pindex)
        elif isinstance(expr, ApplyStencil):
            stencil_fn = self.get_stencil_fn(csc, expr)
            stencil_kwds = stencil_fn.args.copy()
            ctype = stencil_fn.ctype
            pexpr = FunctionCall(ctype, stencil_fn, stencil_kwds)
            self.check_and_set(reqs, stencil_fn.name, stencil_fn)
            for argname, argval in stencil_fn.args.items():
                self.check_and_set(args, argname, argval)
        elif isinstance(expr, WaveNumberIndex):
            expr = expr.real_index
            pexpr = self.parse_expr(csc, name, expr, args, reqs)
        elif isinstance(expr, sm.Symbol):
            sname = expr.name
            if sname == "dx":
                args[csc.dx.name] = csc.dx
                pexpr = OpenClVariable(csc.dx.ctype, csc.dx)
            elif expr in csc.space_symbols:
                xi = csc.space_symbols[expr]
                self.check_and_set(args, xi.name, xi)
                pexpr = OpenClVariable(xi.ctype, xi)
            elif expr in csc.local_indices_symbols:
                i = csc.local_indices_symbols[expr]
                self.check_and_set(args, i.name, i)
                pexpr = OpenClVariable(i.ctype, i)
            else:
                msg = f"Unknown symbol {expr} of type {type(expr)}"
                raise NotImplementedError(msg)
        elif isinstance(expr, (sm.UnevaluatedExpr, UnsplittedExpr)):
            pexpr = self.parse_expr(csc, name, expr.args[0], args, reqs)
        elif isinstance(expr, (sm.Expr, sm.Rel)):
            expanded_args = ()
            for arg in expr.args:
                arg = self.parse_expr(csc, name, arg, args, reqs)
                expanded_args += (arg,)
            pexpr = map_expression(csc, expr, expanded_args, reqs)
        else:
            msg = "Unknown expression type {}.\n  __mro__ = {}, expr={}\n"
            msg = msg.format(type(expr), type(expr).__mro__, expr)
            raise NotImplementedError(msg)

        if not isinstance(pexpr, (TypedI, CustomSymbolicFunction)):
            msg = f"Failed to parse the following expression:\n{expr}\n"
            msg += f"Expanded expression is:\n{pexpr}\n"
            raise RuntimeError(msg)
        return pexpr

    def gencode(self, csc, pexpr):
        with self._function_():
            printer = OpenClPrinter(csc.typegen, self)
            printer.doprint(Return(pexpr))
