# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.backend.device.codegen.symbolic.misc import (
    OpenClSelect,
    OpenClBroadCast,
    OpenClExpand,
)
from hysop.backend.device.codegen.symbolic.expr import BuiltinFunction
from hysop.backend.device.codegen.symbolic.relational import (
    OpenClLogicalAND,
    OpenClLogicalOR,
    OpenClLogicalXOR,
    OpenClLogicalEQ,
    OpenClLogicalNE,
    OpenClLogicalLT,
    OpenClLogicalGT,
    OpenClLogicalLE,
    OpenClLogicalGE,
    OpenClAdd,
    OpenClMul,
)
import sympy as sm

from hysop.tools.htypes import check_instance
from hysop.backend.device.codegen.symbolic.expr import TypedI
from hysop.backend.device.codegen.symbolic.cast import OpenClCastUtils
from hysop.backend.device.codegen.functions.complex import (
    OpenClComplexMul,
    OpenClComplexModulus,
    OpenClComplexModulus2,
)
from hysop.symbolic.complex import (
    ComplexExpr,
    ComplexBinaryExpr,
    ComplexMul,
    ComplexModulus,
    ComplexModulus2,
)
from hysop.symbolic.misc import Select, BroadCast, Expand
from hysop.symbolic.relational import (
    ArithmeticRelation,
    LogicalRelation,
    LogicalAND,
    LogicalOR,
    LogicalXOR,
    LogicalEQ,
    LogicalNE,
    LogicalLT,
    LogicalGT,
    LogicalLE,
    LogicalGE,
    Min,
    Max,
    Round,
)
from hysop.backend.device.codegen.symbolic.relational import basetype


def map_expression(csc, expr, args, reqs):
    check_instance(expr, (sm.Expr, sm.Rel))
    promoted_args = ()
    try:
        ctype, promoted_args = _map_ctypes(expr, args)
        func = _map_func(csc, expr, promoted_args, ctype, reqs)
        new_expr = func(ctype, *promoted_args)
        if not isinstance(new_expr, TypedI):
            msg = f"New expression is not a TypedI, got a {type(new_expr)}."
            raise TypeError(msg)
    except:
        msg = "\nFATAL ERROR: OpenCL symbolic expression expansion failed\n"
        msg += "Failed to rebuild expression by arguments:\n"
        msg += " expression was: {}\n"
        msg += " expression type.__mro__ = {}\n"
        msg += " arguments were:\n   {}\n"
        msg += " promoted arguments were:\n   {}\n"
        msg = msg.format(
            expr,
            expr.func.__mro__,
            "\n   ".join(str(pa) for pa in args),
            "\n   ".join(str(pa) for pa in promoted_args),
        )
        print(msg)
        raise
    return new_expr


def _map_ctypes(expr, args):
    if isinstance(
        expr,
        (
            sm.functions.elementary.trigonometric.TrigonometricFunction,
            sm.functions.elementary.hyperbolic.HyperbolicFunction,
            Round,
        ),
    ):
        (args, ctype) = OpenClCastUtils.promote_expressions_to_required_signature(
            args, "ftype", ret=0
        )
    elif isinstance(expr, sm.Pow):
        if args[1].is_integer:
            (args, ctype) = OpenClCastUtils.promote_expressions_to_required_signature(
                args, ("ftype", "itype"), ret="ftype"
            )
        else:
            (args, ctype) = OpenClCastUtils.promote_expressions_to_required_signature(
                args, ("ftype", "ftype"), ret="ftype"
            )
    elif isinstance(expr, sm.Rel):
        (args, ctype) = OpenClCastUtils.promote_expressions_to_required_signature(
            args, (None, None), ret="btype"
        )
    elif isinstance(expr, Select):
        (args, ctype) = OpenClCastUtils.promote_expressions_to_required_signature(
            args, (None, None, "btype"), ret=0, expand=(False, False, True)
        )
    elif isinstance(expr, LogicalRelation):
        (args, ctype) = OpenClCastUtils.promote_expressions_to_required_signature(
            args, (None,) * len(args), ret="btype", expand=(True,) * len(args)
        )
    elif isinstance(
        expr, (ArithmeticRelation, sm.Add, sm.Mul, sm.Max, sm.Min, Min, Max)
    ):
        (args, ctype) = OpenClCastUtils.promote_expressions_to_required_signature(
            args, (None,) * len(args), ret=0, expand=(True,) * len(args)
        )
    elif isinstance(expr, (ComplexModulus, ComplexModulus2)):
        (args, ctype) = OpenClCastUtils.promote_expressions_to_required_signature(
            args, "ftype", ret="complex2real"
        )

    elif isinstance(expr, ComplexBinaryExpr):
        (args, vtype, btype) = OpenClCastUtils.promote_expressions_to_required_rank(
            args, broadcast_args=True
        )
        ctype = vtype
    elif args:
        (args, vtype, btype) = OpenClCastUtils.promote_expressions_to_required_rank(
            args
        )
        ctype = vtype
    else:
        msg = (
            "\nFATAL ERROR: Could not determine ctype for the following expression: {}."
        )
        msg = msg.format(expr)
        msg += "\n Expression __mro__ is :\n  *{}".format(
            "\n  *".join(str(x) for x in type(expr).__mro__)
        )
        msg += "\n"
        print(msg)
        msg = f"Cannot determine ctype for expression {expr}."
        raise RuntimeError(msg)

    return (ctype, args)


def _map_func(csc, expr, promoted_args, ctype, reqs):
    from hysop.symbolic.complex import ComplexMul, ComplexModulus, ComplexModulus2

    if isinstance(expr, ComplexExpr):
        return _map_complex_func(csc, expr, promoted_args, ctype, reqs)
    elif expr.func is sm.Abs:
        if basetype(ctype) in ("half", "float", "double"):
            return BuiltinFunction("fabs")
        elif basetype(ctype) in ("char", "short", "int", "long"):
            return BuiltinFunction("abs")
        else:
            msg = f"abs({ctype})"
            raise NotImplementedError(msg)
    elif expr.func is sm.Pow:
        if promoted_args[1].is_integer:
            return BuiltinFunction("pown")
        else:
            return BuiltinFunction("pow")
    elif expr.func in (sm.Max, Max):
        if basetype(ctype) in ("half", "float", "double"):
            return BuiltinFunction("fmax")
        elif basetype(ctype) in ("char", "short", "int", "long"):
            return BuiltinFunction("max")
        else:
            msg = f"max({ctype})"
            raise NotImplementedError(msg)
    elif expr.func in (sm.Min, Min):
        if basetype(ctype) in ("half", "float", "double"):
            return BuiltinFunction("fmin")
        elif basetype(ctype) in ("char", "short", "int", "long"):
            return BuiltinFunction("min")
        else:
            msg = f"min({ctype})"
            raise NotImplementedError(msg)
    elif expr.func in _func_mappings:
        return _func_mappings[expr.func]
    return expr.func


def _map_complex_func(csc, expr, promoted_args, ctype, reqs):
    assert isinstance(expr, ComplexExpr)
    functions = {
        ComplexMul: OpenClComplexMul,
        ComplexModulus: OpenClComplexModulus,
        ComplexModulus2: OpenClComplexModulus2,
    }
    fn_kwds = dict(typegen=csc.typegen, ftype=ctype, vectorization=csc.vectorization)
    if expr.func in functions:
        fn = functions[expr.func](**fn_kwds)
    else:
        msg = (
            "Complex function for expressions of type {} has not been implemented yet."
        )
        msg = msg.format(expr.func)
        raise NotImplementedError(msg)
    if fn.name not in reqs:
        reqs[fn.name] = fn
    return BuiltinFunction(fn.name)


_func_mappings = {
    sm.Add: OpenClAdd,
    sm.Mul: OpenClMul,
    LogicalAND: OpenClLogicalAND,
    LogicalOR: OpenClLogicalOR,
    LogicalXOR: OpenClLogicalXOR,
    LogicalEQ: OpenClLogicalEQ,
    LogicalNE: OpenClLogicalNE,
    LogicalLT: OpenClLogicalLT,
    LogicalGT: OpenClLogicalGT,
    LogicalLE: OpenClLogicalLE,
    LogicalGE: OpenClLogicalGE,
    Select: OpenClSelect,
    BroadCast: OpenClBroadCast,
    Expand: OpenClExpand,
    Round: BuiltinFunction("round"),
    sm.cos: BuiltinFunction("cos"),
    sm.sin: BuiltinFunction("sin"),
    sm.tan: BuiltinFunction("tan"),
    sm.cosh: BuiltinFunction("cosh"),
    sm.sinh: BuiltinFunction("sinh"),
    sm.tanh: BuiltinFunction("tanh"),
    sm.exp: BuiltinFunction("exp"),
    sm.log: BuiltinFunction("log"),
    sm.sign: BuiltinFunction("sign"),
    sm.StrictGreaterThan: OpenClLogicalGT,
    sm.StrictLessThan: OpenClLogicalLT,
    sm.GreaterThan: OpenClLogicalGE,
    sm.LessThan: OpenClLogicalLE,
    sm.Equality: OpenClLogicalEQ,
    sm.Unequality: OpenClLogicalNE,
}
