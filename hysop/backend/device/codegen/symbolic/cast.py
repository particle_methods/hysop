# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.htypes import check_instance, to_tuple, first_not_None
from hysop.tools.numpywrappers import npw
from hysop.tools.numerics import (
    match_dtype,
    match_float_type,
    find_common_dtype,
    demote_dtype,
)
from hysop.backend.device.codegen.base.variables import dtype_to_ctype, ctype_to_dtype
from hysop.backend.device.opencl.opencl_types import vtype, basetype, components

from hysop.backend.device.codegen.symbolic.expr import (
    OpenClConvert,
    OpenClCast,
    OpenClBool,
    TypedI,
)
from hysop.backend.device.codegen.symbolic.misc import OpenClBroadCast, OpenClExpand


class OpenClCastUtils:

    # SCALAR RANKS
    # scalar ranks defined as Usual Arithmetic Conversions
    # http://www.informit.com/articles/article.aspx?p=1732873&seqNum=6
    # and section 6.3.1.8 of the C99 specification.
    __type_ranks = [
        npw.float64,
        npw.float32,
        npw.float16,
        npw.uint64,
        npw.int64,
        npw.uint32,
        npw.int32,
        npw.uint16,
        npw.int16,
        npw.uint8,
        npw.int8,
        npw.bool_,
    ]
    type_ranks = dict(zip(__type_ranks, range(len(__type_ranks) - 1, -1, -1)))

    # OPENCL IMPLICIT CONVERSION RULES
    # scalar op scalar => scalar promotion to higher rank type, nothing to do
    # scalar op vector => vector promotion if rank[stype] <= rank[btype] where btype is the vector base type, else compile time error
    # vector op vector => valid only if vtype0 == vtype1, else compile time error

    @classmethod
    def promote_expressions_to_required_rank(cls, exprs, broadcast_args=False):
        assert len(exprs) > 0

        vector_types, vector_ranks, vector_components = (), (), ()
        max_scalar_rank, max_scalar_type = None, None
        for expr in exprs:
            et = expr.ctype
            ecomponents = expr.components
            ebase = expr.btype
            dtype = ctype_to_dtype(ebase)
            rank = cls.type_ranks[dtype]
            if ecomponents > 1:
                vector_types += (ebase,)
                vector_ranks += (rank,)
                vector_components += (ecomponents,)
            else:
                if (max_scalar_rank is None) or (rank > max_scalar_rank):
                    max_scalar_rank = rank
                    max_scalar_type = ebase

        # if there is no vectors in the expression, there is nothing to do
        if not vector_types:
            assert max_scalar_type is not None
            return exprs, max_scalar_type, max_scalar_type

        vcomponents = npw.asarray(vector_components)
        vcomponents = vcomponents[vcomponents > 1]
        if (not broadcast_args) and (vcomponents.size > 0):
            if (vcomponents != vcomponents[0]).all():
                msg = "Vector size mismtach in expressions:"
                for e in exprs:
                    msg += f"\n  *{e.ctype}: {str(e)}"
                msg += "\n"
                raise RuntimeError(msg)

        max_vector_rank = max(vector_ranks)
        max_vector_components = max(vector_components)
        vector_btype = vector_types[vector_ranks.index(max_vector_rank)]
        vector_vtype = f"{vector_btype}{max_vector_components}"

        promoted_exprs = ()
        for expr in exprs:
            # cast all vectors to max rank and broadcast if required
            et = expr.ctype
            ebase = expr.btype
            ecomponents = expr.components
            dtype = ctype_to_dtype(ebase)
            rank = cls.type_ranks[dtype]
            if ecomponents > 1:
                assert max_vector_components % ecomponents == 0
                broadcast_factor = max_vector_components // ecomponents
                if rank != max_vector_rank:
                    expr = OpenClConvert(et, expr)
                if broadcast_factor > 1:
                    expr = OpenClBroadCast(None, expr, broadcast_factor)
            else:
                if rank > max_vector_rank:
                    et = vector_btype
                    expr = OpenClCast(et, expr)
            promoted_exprs += (expr,)

        return promoted_exprs, vector_vtype, vector_btype

    @classmethod
    def promote_expressions_to_float(cls, exprs):
        dtypes = tuple(ctype_to_dtype(e.btype) for e in exprs)
        common_dtype = find_common_dtype(*dtypes)
        float_dtype = match_float_type(common_dtype)
        if float_dtype == npw.float16:
            float_dtype = npw.float32
        elif float_dtype == npw.longdouble:
            float_dtype = npw.float64
        fbtype = dtype_to_ctype(float_dtype)

        promoted = tuple(cls.promote_basetype_to(e, fbtype) for e in exprs)
        return promoted, fbtype

    @classmethod
    def promote_to(cls, expr, target_type):
        if expr.ctype == target_type:
            return expr

        ebase, ecomponents = expr.btype, expr.components
        tbase, tcomponents = basetype(target_type), components(target_type)

        if ecomponents != tcomponents:
            if ecomponents > 1:
                msg = "Components mismatch {} vs {}."
                msg = msg.format(ecomponents, tcomponents)
                raise RuntimeError(msg)
            return OpenClCast(target_type, expr)
        return OpenClConvert(target_type, expr)

    @classmethod
    def promote_basetype_to(cls, expr, btype):
        ebase, ecomponents = expr.btype, expr.components
        if ebase == btype:
            return expr
        target_type = vtype(btype, ecomponents)
        return OpenClConvert(target_type, expr)

    @classmethod
    def promote_expressions_to_required_signature(
        cls, exprs, signature, ret, expand=None
    ):
        exprs = to_tuple(exprs)
        signature = to_tuple(signature)
        expand = to_tuple(first_not_None(expand, (False,) * len(exprs)))
        assert len(exprs) == len(signature) == len(expand) > 0
        check_instance(expand, tuple, values=bool, allow_none=False)
        check_instance(exprs, tuple, values=TypedI, allow_none=False)
        check_instance(signature, tuple, values=(type(None), str), allow_none=False)
        check_instance(ret, (str, int), allow_none=True)

        dtypes = tuple(e.dtype for e in exprs)
        components = tuple(e.components for e in exprs)

        common_dtype = find_common_dtype(*dtypes)
        n = max(components)

        _exprs = ()
        for e, s, exp in zip(exprs, signature, expand):
            ecomponents = e.components
            is_bool = s == "btype"
            if is_bool:
                s = "itype"
            if s is None:
                dtype = common_dtype
            elif isinstance(s, str):
                dtype = demote_dtype(common_dtype, s[0])
            else:
                raise NotImplementedError()

            btype = dtype_to_ctype(dtype)
            ctype = e.vtype(btype, ecomponents)
            vtype = e.vtype(btype, n)

            if ecomponents > 1:
                assert n % ecomponents == 0
                broadcast_factor = n // ecomponents
                if ctype != e.ctype:
                    e = OpenClConvert(ctype, e)
                if broadcast_factor > 1:
                    if exp:
                        e = OpenClExpand(None, e, broadcast_factor)
                    else:
                        e = OpenClBroadCast(None, e, broadcast_factor)
            else:
                if is_bool:  # set all bits to 1
                    e = OpenClBool(e)
                if vtype != e.ctype:
                    e = OpenClCast(vtype, e)
            _exprs += (e,)

        exprs = _exprs

        if ret is None:
            ctype = "void"
        elif isinstance(ret, int):
            assert ret < len(exprs)
            ctype = exprs[ret].ctype
        elif isinstance(ret, str):
            if ret == "complex2real":
                assert n >= 2 and n % 2 == 0
                ret_dtype = demote_dtype(common_dtype, "f")
                ctype = e.vtype(dtype_to_ctype(ret_dtype), n // 2)
            elif ret == "real2complex":
                ret_dtype = demote_dtype(common_dtype, "f")
                ctype = e.vtype(dtype_to_ctype(ret_dtype), n * 2)
            else:
                if ret == "btype":
                    ret = "itype"
                ret_dtype = demote_dtype(common_dtype, ret[0])
                ctype = e.vtype(dtype_to_ctype(ret_dtype), n)
        else:
            msg = f"Unknown return dtype {ret}."
            raise NotImplementedError(msg)

        return exprs, ctype
