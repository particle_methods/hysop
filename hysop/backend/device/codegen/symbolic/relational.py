# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sympy as sm
from hysop.backend.device.codegen.symbolic.expr import TypedI, cl_components
from hysop.symbolic.relational import (
    LogicalAND,
    LogicalOR,
    LogicalXOR,
    LogicalEQ,
    LogicalNE,
    LogicalLT,
    LogicalGT,
    LogicalLE,
    LogicalGE,
    Mul,
    Pow,
    Add,
)


def basetype(fulltype):
    import string

    return fulltype.translate(str.maketrans("", "", string.digits))


class RelationalTypedExpr(TypedI):
    def __new__(cls, ctype, *args, **kwds):
        obj = super().__new__(cls, *args, **kwds)
        obj.ctype = ctype
        return obj


class LogicalTypedExpr(RelationalTypedExpr):
    def __new__(cls, ctype, *args, **kwds):
        assert basetype(ctype) in ("char", "short", "int", "long"), ctype
        return super().__new__(cls, ctype, *args, **kwds)


class ArithmeticTypedExpr(RelationalTypedExpr):
    pass


class OpenClLogicalAND(LogicalTypedExpr, LogicalAND):
    pass


class OpenClLogicalOR(LogicalTypedExpr, LogicalOR):
    pass


class OpenClLogicalXOR(LogicalTypedExpr, LogicalXOR):
    pass


class OpenClLogicalEQ(LogicalTypedExpr, LogicalEQ):
    pass


class OpenClLogicalNE(LogicalTypedExpr, LogicalNE):
    pass


class OpenClLogicalLT(LogicalTypedExpr, LogicalLT):
    pass


class OpenClLogicalGT(LogicalTypedExpr, LogicalGT):
    pass


class OpenClLogicalLE(LogicalTypedExpr, LogicalLE):
    pass


class OpenClLogicalGE(LogicalTypedExpr, LogicalGE):
    pass


class OpenClAdd(ArithmeticTypedExpr, Add):
    pass


class OpenClMul(ArithmeticTypedExpr, Mul):
    pass
