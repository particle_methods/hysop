# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.backend.device.codegen.base.utils import WriteOnceDict, ArgDict
from hysop.backend.device.codegen.kernels.custom_symbolic import (
    CustomSymbolicKernelGenerator,
)
from hysop.backend.device.codegen.symbolic.expr import OpenClPrinter
from hysop.symbolic.field import SymbolicDiscreteField
from hysop.symbolic.misc import TimeIntegrate
from hysop.backend.device.codegen.symbolic.functions.custom_symbolic_function import (
    CustomSymbolicFunction,
)
from hysop.backend.device.codegen.symbolic.expr import (
    OpenClAssignment,
    OpenClVariable,
    FunctionCall,
    UpdateVars,
    IfElse,
)
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
    CodegenArray,
    ctype_to_dtype,
)
from hysop.backend.device.codegen.functions.vload import Vload
from hysop.backend.device.codegen.functions.vstore import Vstore
from hysop.operator.base.custom_symbolic_operator import ValidExpressions


class CustomSymbolicTimeIntegrateKernelGenerator(CustomSymbolicKernelGenerator):

    @classmethod
    def custom_name(cls):
        return "time_integrate"

    def build_expr_requirements(self, csc, kernel_reqs, kernel_args, known_vars):
        reqs = super().build_expr_requirements(csc, kernel_reqs, kernel_args)
        expr_info = csc.expr_info
        vectorization = csc.vectorization
        tg = csc.typegen
        itype = csc.itype

        time_integrator = expr_info.time_integrator

        K_args = ArgDict()
        Uo = ArgDict()
        Uk = ArgDict()

        vloads = {}
        vstores = {}
        _ghosts = {}
        fcalls = ()

        shared = set()
        private = set()
        vnames = tuple()
        for i, expr in enumerate(expr_info.dexprs):
            if isinstance(expr, TimeIntegrate):
                time_integrator, lhs, rhs = expr.args
                if isinstance(lhs, SymbolicDiscreteField):
                    vname = CustomSymbolicFunction.field_name(lhs.field, lhs.index)
                    ghosts = csc.array_ghost(lhs.field, lhs.index)
                    var = csc.args[vname]
                    if var.storage == "__local":
                        ctype = var.ctype
                        vtype = tg.vtype(ctype, vectorization)
                        shared.add(vname)
                        is_local = True
                    else:
                        private.add(vname)
                        vtype = var.ctype
                        ctype = var.btype
                        is_local = False

                    K_args[vname] = CodegenArray(
                        f"K_{vname}", vtype, tg, shape=(time_integrator.stages,)
                    )
                    Uo[vname] = CodegenVectorClBuiltin(
                        vname + "__0",
                        ctype,
                        vectorization,
                        typegen=tg,
                        const=True,
                        nl=True,
                    )
                    Uk[vname] = CodegenVectorClBuiltin(
                        vname + "__k",
                        ctype,
                        vectorization,
                        typegen=tg,
                        const=True,
                        nl=True,
                    )
                    _ghosts[vname] = ghosts

                    dval = CustomSymbolicFunction.default_out_of_bounds_value(
                        ctype_to_dtype(ctype)
                    )
                    if is_local:
                        size = int(csc.array_size(lhs.field, lhs.index))
                        assert size > 2 * ghosts, (size, 2 * ghosts)
                        vload = Vload(
                            tg,
                            ctype,
                            vectorization,
                            default_val=dval,
                            itype=itype,
                            restrict=True,
                            storage=var.storage,
                            known_args=dict(size=size),
                        )
                        if vload.name not in reqs:
                            reqs[vload.name] = vload
                            vloads[vname] = vload
                        else:
                            vloads[vname] = reqs[vload.name]
                        vstore = Vstore(
                            tg,
                            ctype,
                            vectorization,
                            itype=itype,
                            restrict=True,
                            storage=var.storage,
                            known_args=dict(size=size),
                        )
                        if vstore.name not in reqs:
                            reqs[vstore.name] = vstore
                            vstores[vname] = vstore
                        else:
                            vstores[vname] = reqs[vstore.name]
                else:
                    msg = "Unknown lhs type {} for assignment, valid ones are SymbolicDiscreteField."
                    msg = msg.format(type(lhs))
                    raise NotImplementedError(msg)

                fname = f"f{i}"
                rhs_fn = CustomSymbolicFunction(
                    csc=csc,
                    name=fname,
                    expr=rhs,
                    target_ctype=vtype,
                    inline=(not csc.tuning_mode),
                    known_args=known_vars,
                )
                fn_kwds = rhs_fn.args.copy()
                rhs = FunctionCall(rhs_fn.ctype, rhs_fn, fn_kwds)
                vnames += (vname,)
                fcalls += (rhs,)
                reqs[fname] = rhs_fn
                assert vtype == rhs.ctype, f"{var.ctype} != {rhs.ctype}"
            else:
                msg = "Unknown expression type {}, valid ones are {}."
                msg = msg.format(type(expr), ValidExpressions)
                raise NotImplementedError(msg)

        self.time_integrator = time_integrator
        self.vnames = vnames
        self.fcalls = fcalls
        self.K_args = K_args
        self.Uo = Uo
        self.Uk = Uk
        self.vloads = vloads
        self.vstores = vstores
        self.private = private
        self.shared = shared
        self._ghosts = _ghosts
        return reqs

    def generate_expr_code(self):
        s = self
        tg = s.typegen
        csc = s.csc
        info = csc.expr_info
        time_integrator = self.time_integrator

        args = csc.args

        K = s.K_args
        Uo = s.Uo
        Uk = s.Uk
        fcalls = s.fcalls
        vnames = s.vnames

        vloads = s.vloads
        vstores = s.vstores
        shared = s.shared
        private = s.private
        ghosts = s._ghosts

        printer = OpenClPrinter(tg, s)
        assert self.has_local_stores or self.has_private_stores

        s.comment("Load initial values")
        with s._align_() as al:
            for vname in vnames:
                uo = Uo[vname]
                arg = args[vname]
                G = ghosts[vname]
                if vname in shared:
                    load = vloads[vname](data=arg, offset=f"{csc.local_offset}+{G}")
                    uo.declare(al, align=True, init=load)
                else:
                    uo.declare(al, align=True, init=arg)

        s.jumpline()
        s.comment("Storage for computed slopes")
        s.decl_aligned_vars(*tuple(K.values()))

        for i in range(1, time_integrator.stages + 1):
            alpha = time_integrator.alpha[i - 1]
            is_last = i == time_integrator.stages
            if is_last:
                gamma = time_integrator.beta
            else:
                gamma = time_integrator.gamma[i - 1, :]
            s.comment(f"Computing Runge-Kutta stage {i}")
            with s._block_():
                with s._align_() as al:
                    for vname, fcall in zip(vnames, fcalls):
                        k = K[vname]
                        call = printer.doprint(fcall, terminate=False)
                        code = f"{k[i-1]} $= {call};"
                        al.append(code)
                if shared:
                    s.barrier(_local=True)
            s.jumpline()
            s.comment("Update values with new estimated slope")
            with s._block_():
                with s._align_() as al:
                    for vname in vnames:
                        k = K[vname]
                        uk = Uk[vname].nv_replace("k", str(i))
                        uo = Uo[vname]
                        init = " $+ ".join(
                            f"{tg.dump(float(gamma[j]))}$*{k[j]}"
                            for j in range(i)
                            if (gamma[j] != 0)
                        )
                        init = "{} $+ {}*{}*({})".format(
                            uo,
                            tg.dump(float(self.csc.expr_info.dt_coeff)),
                            self.args[self.csc.expr_info.dt.name],
                            init,
                        )
                        uk.declare(al, init=init, align=True)
                s.jumpline()
                for vname in shared:
                    arg = args[vname]
                    value = Uk[vname].nv_replace("k", str(i))
                    G = ghosts[vname]
                    code = (
                        vstores[vname](
                            value=value, data=arg, offset=f"{csc.local_offset}+{G}"
                        )
                        + ";"
                    )
                    s.append(code)
                if private:
                    with s._align_() as al:
                        for vname in private:
                            arg = args[vname]
                            value = Uk[vname].nv_replace("k", str(i))
                            arg.affect(al, align=True, init=value)
                if shared:
                    s.barrier(_local=True)
            if not is_last:
                s.jumpline()
