# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.backend.device.codegen.base.utils import WriteOnceDict, ArgDict
from hysop.backend.device.codegen.kernels.custom_symbolic import (
    CustomSymbolicKernelGenerator,
)
from hysop.backend.device.codegen.symbolic.expr import OpenClPrinter
from hysop.backend.device.codegen.symbolic.functions.custom_symbolic_function import (
    CustomSymbolicFunction,
)
from hysop.backend.device.codegen.symbolic.expr import (
    OpenClAssignment,
    OpenClVariable,
    OpenClIndexedVariable,
    FunctionCall,
    UpdateVars,
    IfElse,
)
from hysop.backend.device.codegen.base.variables import (
    CodegenVariable,
    CodegenVectorClBuiltin,
    CodegenArray,
)
from hysop.symbolic.field import SymbolicDiscreteField
from hysop.symbolic.misc import TimeIntegrate, CodeSection
from hysop.symbolic.relational import Assignment
from hysop.symbolic.array import (
    OpenClSymbolicArray,
    OpenClSymbolicBuffer,
    IndexedBuffer,
)
from hysop.symbolic.tmp import TmpScalar


class CustomSymbolicAffectKernelGenerator(CustomSymbolicKernelGenerator):

    @classmethod
    def custom_name(cls):
        return "affect"

    def generate_expr_code(self):
        self.decl_aligned_vars(*tuple(self.out_args.values()), _const=False)
        printer = OpenClPrinter(self.typegen, self)
        for pexpr in self.expressions:
            printer.doprint(pexpr)

    def build_expr_requirements(self, csc, kernel_reqs, kernel_args, known_vars):
        reqs = super().build_expr_requirements(csc, kernel_reqs, kernel_args)

        expressions = ()
        current_expr = None

        out_args = ArgDict()
        fcalls = []
        is_out_of_date = set()

        pexprs, findex = self._build_expr_requirements(
            csc,
            known_vars,
            csc.expr_info.dexprs,
            out_args,
            fcalls,
            is_out_of_date,
            reqs,
            0,
        )
        guard = IfElse(csc.is_active, pexprs)
        expressions += (guard,)

        lhs = tuple(out_args[vn] for vn in is_out_of_date)
        rhs = tuple(csc.args[vn] for vn in is_out_of_date)
        ghosts = tuple(csc.array_ghosts[vn] for vn in is_out_of_date)
        assert len(lhs) == len(rhs) == len(ghosts)
        if len(lhs) > 0:
            expressions += (UpdateVars(lhs, rhs, ghosts),)

        self.expressions = expressions
        self.fcalls = tuple(fcalls)
        self.out_args = out_args
        return reqs

    def _build_expr_requirements(
        self, csc, known_vars, exprs, out_args, fcalls, is_out_of_date, reqs, findex
    ):
        vectorization = csc.vectorization
        typegen = csc.typegen
        pexprs = ()
        for i, expr in enumerate(exprs):
            if isinstance(expr, CodeSection):
                section_pexprs, findex = self._build_expr_requirements(
                    csc,
                    known_vars,
                    expr.args,
                    out_args,
                    fcalls,
                    is_out_of_date,
                    reqs,
                    findex,
                )
                pexpr = CodeSection(*section_pexprs)
            elif isinstance(expr, Assignment):
                lhs, rhs = expr.args
                is_tmp = False
                if isinstance(lhs, SymbolicDiscreteField):
                    vname = CustomSymbolicFunction.field_name(lhs.field, lhs.index)
                    var = CodegenVectorClBuiltin(
                        vname + "_out",
                        lhs.field.ctype,
                        vectorization,
                        typegen=typegen,
                        const=True,
                        nl=True,
                    )
                    lhs = OpenClVariable(var.ctype, var)
                    out_args[vname] = var
                elif isinstance(lhs, OpenClSymbolicArray):
                    vname = CustomSymbolicFunction.array_name(lhs)
                    var = CodegenVectorClBuiltin(
                        vname + "_out",
                        lhs.ctype,
                        vectorization,
                        typegen=typegen,
                        const=True,
                        nl=True,
                    )
                    lhs = OpenClVariable(var.ctype, var)
                    out_args[vname] = var
                elif isinstance(lhs, IndexedBuffer):
                    var = lhs.indexed_object
                    index = lhs.index
                    fname = f"f{findex}"
                    findex += 1
                    index_fn = CustomSymbolicFunction(
                        csc=csc,
                        name=fname,
                        expr=index,
                        inline=(not csc.tuning_mode),
                        known_args=known_vars,
                    )
                    fn_kwds = index_fn.args.copy()
                    index = FunctionCall(index_fn.ctype, index_fn, fn_kwds)
                    fcalls.append(index)
                    reqs[fname] = index_fn
                    lhs = OpenClIndexedVariable(var.ctype, var, index)
                    is_tmp = True
                elif isinstance(lhs, TmpScalar):
                    is_tmp = True
                    vname = lhs.varname
                    if vname not in out_args:
                        var = CodegenVectorClBuiltin(
                            vname,
                            lhs.ctype,
                            vectorization,
                            typegen=typegen,
                            const=True,
                            nl=True,
                        )
                        out_args[vname] = var
                    else:
                        var = out_args[vname]
                    lhs = OpenClVariable(var.ctype, var)
                else:
                    msg = "Unknown lhs type {} for assignment, valid ones are SymbolicDiscreteField."
                    msg = msg.format(type(lhs))
                    raise NotImplementedError(msg)

                fname = f"f{findex}"
                findex += 1
                rhs_fn = CustomSymbolicFunction(
                    csc=csc,
                    name=fname,
                    expr=rhs,
                    target_ctype=lhs.ctype,
                    inline=(not csc.tuning_mode),
                    known_args=known_vars,
                )
                fn_kwds = rhs_fn.args.copy()
                rhs = FunctionCall(rhs_fn.ctype, rhs_fn, fn_kwds)
                fcalls.append(rhs)
                reqs[fname] = rhs_fn

                assert lhs.ctype == rhs.ctype, f"{lhs.ctype} != {rhs.ctype}"
                pexpr = OpenClAssignment(lhs.ctype, lhs, expr.rel_op, rhs)

                if not is_tmp:
                    is_out_of_date.add(vname)
            else:
                pexpr = expr
            pexprs += (pexpr,)
        return pexprs, findex
