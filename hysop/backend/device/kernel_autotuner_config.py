# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABCMeta, abstractmethod
from hysop.tools.htypes import check_instance, first_not_None
from hysop.constants import (
    AutotunerFlags,
    __DEBUG__,
    __VERBOSE__,
    __KERNEL_DEBUG__,
    DEFAULT_AUTOTUNER_FLAG,
    DEFAULT_AUTOTUNER_PRUNE_THRESHOLD,
)


class KernelAutotunerConfig(metaclass=ABCMeta):

    _default_initial_runs = {
        AutotunerFlags.ESTIMATE: 1,
        AutotunerFlags.MEASURE: 2,
        AutotunerFlags.PATIENT: 4,
        AutotunerFlags.EXHAUSTIVE: 8,
    }

    def __init__(
        self,
        dump_folder=None,
        autotuner_flag=None,
        prune_threshold=None,
        max_candidates=None,
        verbose=None,
        debug=None,
        dump_kernels=None,
        dump_hash_logs=None,
        generate_isolation_file=None,
        override_cache=None,
        nruns=None,
        plot_statistics=None,
        filter_statistics=None,
        postprocess_kernels=None,
        postprocess_nruns=None,
    ):

        dump_folder = first_not_None(dump_folder, self.default_dump_folder())
        autotuner_flag = first_not_None(autotuner_flag, DEFAULT_AUTOTUNER_FLAG)
        prune_threshold = first_not_None(
            prune_threshold, DEFAULT_AUTOTUNER_PRUNE_THRESHOLD
        )
        max_candidates = first_not_None(max_candidates, 1 if __KERNEL_DEBUG__ else 4)
        verbose = first_not_None(verbose, 2 * __VERBOSE__)
        debug = first_not_None(debug, __KERNEL_DEBUG__)
        dump_kernels = first_not_None(dump_kernels, __KERNEL_DEBUG__)
        dump_hash_logs = first_not_None(dump_hash_logs, __KERNEL_DEBUG__)
        generate_isolation_file = first_not_None(
            generate_isolation_file, __KERNEL_DEBUG__
        )
        override_cache = first_not_None(override_cache, False)
        plot_statistics = first_not_None(plot_statistics, False)
        filter_statistics = first_not_None(filter_statistics, lambda kernel_name: True)
        postprocess_kernels = first_not_None(postprocess_kernels, False)
        postprocess_nruns = first_not_None(postprocess_nruns, 16)

        if nruns is None:
            nruns = self._default_initial_runs[autotuner_flag]
        elif nruns < 1:
            raise ValueError("nruns<1.")

        check_instance(dump_folder, str)
        check_instance(autotuner_flag, AutotunerFlags)
        check_instance(override_cache, bool)
        check_instance(verbose, int)
        check_instance(debug, bool)
        check_instance(nruns, int)
        check_instance(max_candidates, int, allow_none=True)
        check_instance(postprocess_nruns, int)
        assert callable(filter_statistics)

        self.autotuner_flag = autotuner_flag
        self.prune_threshold = prune_threshold
        self.verbose = verbose
        self.debug = debug
        self.override_cache = override_cache
        self.nruns = nruns
        self.dump_folder = dump_folder
        self.dump_kernels = dump_kernels
        self.dump_hash_logs = dump_hash_logs
        self.max_candidates = max_candidates
        self.generate_isolation_file = generate_isolation_file
        self.plot_statistics = plot_statistics
        self.filter_statistics = filter_statistics
        self.postprocess_kernels = postprocess_kernels
        self.postprocess_nruns = postprocess_nruns

    @abstractmethod
    def default_dump_folder(self):
        pass
