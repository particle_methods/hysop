# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABCMeta, abstractmethod
from hysop import __KERNEL_DEBUG__
from hysop.constants import Precision
from hysop.backend.device.kernel_autotuner_config import KernelAutotunerConfig
from hysop.tools.htypes import check_instance, first_not_None


class KernelConfig(metaclass=ABCMeta):

    def __init__(
        self,
        autotuner_config=None,
        user_build_options=None,
        user_size_constants=None,
        precision=None,
        float_dump_mode=None,
        use_short_circuit_ops=None,
        unroll_loops=None,
    ):

        autotuner_config = first_not_None(
            autotuner_config, self.default_autotuner_config()
        )
        user_build_options = first_not_None(user_build_options, [])
        user_size_constants = first_not_None(user_size_constants, [])
        precision = first_not_None(precision, Precision.SAME)
        use_short_circuit_ops = first_not_None(use_short_circuit_ops, False)
        unroll_loops = first_not_None(unroll_loops, False)

        if float_dump_mode is None:
            if __KERNEL_DEBUG__:
                float_dump_mode = "dec"
            else:
                float_dump_mode = "hex"
        else:
            assert float_dump_mode in ["dec", "hex"]

        check_instance(user_build_options, list, values=str)
        check_instance(user_size_constants, list)
        check_instance(precision, Precision)
        check_instance(autotuner_config, KernelAutotunerConfig)
        check_instance(float_dump_mode, str, allow_none=True)
        check_instance(use_short_circuit_ops, bool)
        check_instance(unroll_loops, bool)

        self.user_build_options = user_build_options
        self.user_size_constants = user_size_constants
        self.precision = precision
        self.float_dump_mode = float_dump_mode
        self.autotuner_config = autotuner_config
        self.use_short_circuit_ops = use_short_circuit_ops
        self.unroll_loops = unroll_loops

    @abstractmethod
    def default_autotuner_config(self):
        pass
