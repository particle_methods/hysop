# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys
import numpy as np
from hysop.tools.htypes import check_instance


class AutotunedKernelStatistics(dict):
    class AutotunedParameterStatistics(dict):
        class AutotunedRunStatistics:
            def __init__(
                self,
                work_size,
                work_load,
                local_work_size,
                global_work_size,
                statistics,
                pruned,
                local_best,
                error,
            ):
                self.work_size = work_size
                self.work_load = work_load
                self.local_work_size = local_work_size
                self.global_work_size = global_work_size
                self.statistics = statistics
                self.pruned = pruned
                self.local_best = local_best
                self.error = error

            def good(self):
                return self.error is None

        def __init__(
            self,
            extra_parameters,
            max_kernel_work_group_size=None,
            preferred_work_group_size_multiple=None,
        ):
            self.extra_parameters = extra_parameters
            self.max_kernel_work_group_size = max_kernel_work_group_size
            self.preferred_work_group_size_multiple = preferred_work_group_size_multiple

        def push_run_statistics(self, run_key, **kwds):
            self[run_key] = self.AutotunedRunStatistics(**kwds)

        def good(self):
            return len(self) > 0

    def __init__(self, tkernel, extra_kwds):
        self.tkernel = tkernel
        self.extra_kwds = extra_kwds
        self.max_candidates = None
        self.nruns = None
        self.exec_time = None
        self.best_candidate = None
        self.kernel_name = None
        self.kept_count = None
        self.pruned_count = None
        self.failed_count = None
        self.total_count = None
        self.file_basename = None
        self.steps = {}

    def push_parameters(self, extra_param_hash, **kwds):
        return self.setdefault(
            extra_param_hash, self.AutotunedParameterStatistics(**kwds)
        )

    def push_step(self, step_id, candidates):
        self.steps[step_id] = candidates

    def plot(self):
        self.collect_exec_times()
        self.plot_histogram()

    def plot_histogram(self):
        from matplotlib import pyplot as plt

        run_times = self.run_times.copy()
        for unit in ("ns", "us", "ms", "s"):
            if run_times.min() < 1e2:
                break
            run_times *= 1e-3
        vmin, vmax, vmean = (
            run_times.min(),
            run_times.max(),
            np.median(run_times),
        )  # .mean()
        run_times /= np.mean(run_times)
        vnmin, vnmax, vnmean = (
            run_times.min(),
            run_times.max(),
            np.median(run_times),
        )  # .mean()
        imin = int(np.floor(np.log10(vnmin)))
        imax = int(np.ceil(np.log10(vnmax)))
        xmin = 10.0**imin
        xmax = 10.0**imax
        logbins = np.geomspace(xmin, xmax, (imax - imin + 1) * 10)
        fig, axe = plt.subplots()
        fig.suptitle(self.kernel_name, weight="bold")
        rect = plt.Rectangle((vnmin * 2, 0.0), (xmax - 2 * vnmin), 1.0e4, alpha=0.1)
        axe.add_patch(rect)
        _, _, rects = axe.hist(run_times, bins=logbins)
        h = max(r.get_height() for r in rects)
        axe.set_title(
            "{} configurations (kept={}, pruned={}, failed={}) over {} runs".format(
                self.total_count,
                self.kept_count,
                self.pruned_count,
                self.failed_count,
                self.nruns,
            )
        )
        axe.set_xlim(xmin, xmax)
        axe.set_ylim(0, int(1.05 * h))
        axe.set_xlabel("Relative performance (% of mean execution time)")
        axe.set_ylabel("Frequency")
        axe.set_xscale("log")
        axe.axvline(x=vnmin, label=rf"best: ${vmin:.1f} {unit}$", color="lime")
        axe.axvline(
            x=vnmean,
            label=rf"median: ${vmean:.1f} {unit}$ (x{vnmean/vnmin:.1f})",
            color="darkorange",
        )
        axe.axvline(
            x=vnmax,
            label=rf"worst: ${vmax:.1f} {unit}$ (x{vnmax/vnmin:.1f})",
            color="r",
        )
        axe.legend(framealpha=1.0, title="Execution times")
        fname = f"{self.tkernel.autotuner_config.dump_folder}/{self.file_basename}_histo.svg"
        fig.savefig(fname, bbox_inches="tight", format="svg")

    def collect_exec_times(self):
        run_times = ()
        for extra_param_hash, parameter_statistics in self.items():
            if not parameter_statistics.good():
                continue
            for run_key, run_statistics in parameter_statistics.items():
                if not run_statistics.good():
                    continue
                run_time = run_statistics.statistics.mean
                run_times += (run_time,)
                # run_times += run_statistics.statistics.data[:self.nruns]
        run_times = np.asarray(run_times, dtype=np.float64)
        self.run_times = run_times
