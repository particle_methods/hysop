# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np

from hysop.core.arrays import MemoryType, MemoryOrdering
from hysop.core.arrays import default_order
from hysop.core.arrays.array import Array
from hysop.backend.host.host_array_backend import HostArrayBackend


class HostArray(Array):
    """
    Host memory array wrapper.
    An HostArray is a numpy.ndarray work-alike that stores its data and performs
    its computations on CPU with numpy.
    """

    def __init__(self, handle, backend, **kwds):
        """
        Build an HostArray instance.

        Parameters
        ----------
        handle: numpy.ndarray
            implementation of this array
        backend: HostArrayBackend
            backend used to build this handle
        kwds:
            arguments for base classes.
        Notes
        -----
        This should never be called directly by the user.
        Arrays should be constructed using array backend facilities, like zeros or empty.
        The parameters given here refer to a low-level method for instantiating an array.
        """

        if not isinstance(handle, np.ndarray):
            msg = "Handle should be a np.ndarray but got a {}."
            msg = msg.format(handle.__class__.__name__)
            raise ValueError(msg)
        if not isinstance(backend, HostArrayBackend):
            msg = "Backend should be a HostArrayBackend but got a {}."
            msg = msg.format(handle.__class__.__name__)
            raise ValueError(msg)

        # if handle.dtype in  [np.bool]:
        # msg='{} unsupported yet, use HYSOP_BOOL={} instead.'.format(handle.dtype,
        # HYSOP_BOOL)
        # raise TypeError(msg)

        super().__init__(handle=handle, backend=backend, **kwds)

        # array interface
        self.__array_interface__ = handle.__array_interface__

    def as_symbolic_array(self, name, **kwds):
        """
        Return a symbolic array variable that contain a reference to this array.
        """
        from hysop.symbolic.array import HostSymbolicArray

        return HostSymbolicArray(memory_object=self, name=name, **kwds)

    def as_symbolic_buffer(self, name, **kwds):
        """
        Return a symbolic buffer variable that contain a reference to this array.
        """
        from hysop.symbolic.array import HostSymbolicBuffer

        return HostSymbolicBuffer(memory_object=self, name=name, **kwds)

    def get_ndim(self):
        return self.handle.ndim

    def get_shape(self):
        return self.handle.shape

    def set_shape(self, shape):
        self.handle.shape = shape

    def get_size(self):
        return self.handle.size

    def get_strides(self):
        return self.handle.strides

    def get_offset(self):
        return self.handle.offset

    def get_data(self):
        return self.handle.data

    def get_base(self):
        return self.handle.base

    def get_dtype(self):
        return self.handle.dtype

    def get_flags(self):
        return self.handle.flags

    def get_imag(self):
        return self.handle.imag

    def get_real(self):
        return self.handle.real

    def get_ctypes(self):
        return self.handle.ctypes

    def get_T(self):
        return self.wrap(self.handle.T)

    def get_size(self):
        return self.handle.size

    def get_itemsize(self):
        return self.handle.itemsize

    def get_nbytes(self):
        return self.handle.nbytes

    def get_int_ptr(self):
        return int(self.handle.ctypes.data)

    # array properties
    ndim = property(get_ndim)
    shape = property(get_shape, set_shape)
    offset = property(get_offset)
    strides = property(get_strides)
    data = property(get_data)
    base = property(get_base)
    dtype = property(get_dtype)
    flags = property(get_flags)
    T = property(get_T)
    imag = property(get_imag)
    real = property(get_real)
    ctypes = property(get_ctypes)
    size = property(get_size)
    itemsize = property(get_itemsize)
    nbytes = property(get_nbytes)
    int_ptr = property(get_int_ptr)

    def get(self, handle=False):
        """
        Returns equivalent array on host device, ie. self.
        """
        if handle or (self.ndim == 0):
            return self._handle
        else:
            return self

    def lock(self):
        """
        Set tab as a non-writeable array
        """
        self.handle.flags.writeable = False
        return self

    def unlock(self):
        """
        set tab as a writeable array
        """
        self.handle.flags.writeable = True
        return self

    def tofile(self, fid, sep="", format="%s"):
        """
        Write array to a file as text or binary (default).
        This is a convenience function for quick storage of array data.
        Information on endianness and precision is lost.
        """
        self.handle.tofile(fid=fid, sep=sep, format=format)
        return self

    def tolist(self):
        """
        Return the array as a possibly nested list.
        """
        return self.handle.tolist()

    def tostring(self, order=MemoryOrdering.SAME_ORDER):
        """
        Construct Python bytes containing the raw data bytes in the array.
        """
        return self.handle.tostring()

    def put(self, indices, values, mode="raise"):
        """
        Set a.flatn = valuesn for all n in indices.
        """
        self.handle.put(indices=indices, values=values, mode=mode)
        return self

    def take(self, indices, axis=None, out=None, mode="raise"):
        """
        Return an array formed from the elements of a at the given indices.
        """
        handle = self.handle.take(indices=indices, axis=axis, out=out, mode=mode)
        return self.wrap(handle)

    def astype(
        self,
        dtype,
        order=MemoryOrdering.SAME_ORDER,
        casting="unsafe",
        subok=True,
        copy=True,
    ):
        """
        Copy of the array, cast to a specified type.
        """
        handle = self.handle.astype(
            dtype=dtype, order=order, casting=casting, subok=subok, copy=copy
        )
        return self.wrap(handle)

    def view(self, dtype=None):
        """
        New view of array with the same data.
        """
        handle = self.handle.view(dtype=dtype)
        return self.wrap(handle)

    def nanmin(self, axis=None, out=None):
        return self.backend.nanmin(a=self, axis=None, out=None)

    def nanmax(self, axis=None, out=None):
        return self.backend.nanmax(a=self, axis=None, out=None)
