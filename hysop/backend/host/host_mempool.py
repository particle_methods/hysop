# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.htypes import check_instance
from hysop.backend.host.host_allocator import HostAllocator
from hysop.backend.host.host_buffer import HostPooledBuffer
from hysop.core.memory.mempool import MemoryPool


class HostMemoryPool(MemoryPool, HostAllocator):

    def __new__(cls, allocator, **kwds):
        return super().__new__(cls, allocator=allocator, **kwds)

    def __init__(self, allocator, **kwds):
        check_instance(allocator, HostAllocator)
        super().__init__(allocator=allocator, **kwds)

    def _wrap_buffer(self, buf, alloc_sz, size, alignment):
        return HostPooledBuffer(
            pool=self, buf=buf, alloc_sz=alloc_sz, size=size, alignment=alignment
        )
