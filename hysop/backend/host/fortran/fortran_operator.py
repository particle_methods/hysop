# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Abstract class providing a common interface to all
discrete operators working with fortran.

* :class:`~hysop.backend.host.fortran.FortranOperator` is an abstract class
    used to provide a common interface to all discrete operators working with
    fortran.
"""
from abc import ABCMeta
from hysop.tools.decorators import debug
from hysop.tools.htypes import check_instance
from hysop.constants import MemoryOrdering
from hysop.backend.host.host_operator import HostOperator
from hysop.fields.discrete_field import DiscreteScalarFieldView


class FortranOperator(HostOperator, metaclass=ABCMeta):
    """
    Abstract class for discrete operators working with fortran.
    """

    @debug
    def get_field_requirements(self):
        requirements = super().get_field_requirements()
        for is_input, reqs in requirements.iter_requirements():
            if reqs is None:
                continue
            (field, td, req) = reqs
            req.memory_order = MemoryOrdering.F_CONTIGUOUS
        return requirements

    @debug
    def discretize(self):
        if self.discretized:
            return
        super().discretize()

    @debug
    def setup(self, work):
        super().setup(work)
        self.check_memory_order()

    def check_memory_order(self):
        for df in self.input_discrete_fields.values():
            self.check_dfield_memory_order(df)
        for df in self.output_discrete_fields.values():
            self.check_dfield_memory_order(df)

    def check_dfield_memory_order(self, df):
        check_instance(df, DiscreteScalarFieldView)
        data = df.sdata
        if not data.flags.f_contiguous:
            msg = "At least one FortranOperator data view has no fortran ordering "
            msg += "for discrete scalar field {} in operator {}."
            msg += "\nTo explicitely allow some C ordered arrays in a FortranOperator, "
            msg += "please redefine the check_dfield_memory_order method."
            msg = msg.format(df.name, self.name)
            raise RuntimeError(msg)
