# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.htypes import check_instance
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import Field
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.constants import FieldProjection
from hysop.backend.host.fortran.operator.fortran_fftw import (
    fftw2py,
    FortranFFTWOperator,
)
from hysop.core.graph.graph import op_apply
from hysop.operator.base.poisson_curl import PoissonCurlOperatorBase
import numpy as np


class FortranPoissonCurl(PoissonCurlOperatorBase, FortranFFTWOperator):

    def initialize(self, **kwds):
        super().initialize(**kwds)
        dim = self.dim
        if dim == 2:
            self._solve_poisson = self._solve_poisson_2d
            self._solve_diffuse = self._solve_diffuse_2d
        elif dim == 3:
            self._solve_poisson = self._solve_poisson_3d
            self._solve_diffuse = self._solve_diffuse_3d
        else:
            raise NotImplementedError(f"dim = {dim}")

    @debug
    def discretize(self):
        super().discretize()
        dU = self.dU
        dW = self.dW

        ghosts_u = dU.ghosts.astype(np.int64)
        ghosts_w = dW.ghosts.astype(np.int64)

        poisson_buffers = dW.buffers + dU.buffers
        assert all(b.flags.f_contiguous for b in poisson_buffers)

        self.poisson_args = poisson_buffers + (ghosts_w, ghosts_u)
        self.diffuse_args = dW.buffers + (ghosts_w,)
        self.projection_args = dW.buffers + (ghosts_w,)

    @op_apply
    def apply(self, simulation=None, **kargs):
        super().apply(simulation=simulation, **kargs)
        if self.do_project(simulation):
            self._project()
            self.dW.exchange_ghosts()
        if self.should_diffuse:
            self._solve_diffuse()
            self.dW.exchange_ghosts()
        self._solve_poisson()
        self.dU.exchange_ghosts()

    def _project(self):
        """
        Apply projection on vorticity such that the
        resolved velocity will have div(U)=0.
        Vorticity is modified inplace.
        """
        fftw2py.projection_om_3d(*self.projection_args)

    def _solve_poisson_2d(self):
        """
        Solve 2D poisson problem, no projection, no correction.
        """
        fftw2py.solve_poisson_2d(*self.poisson_args)

    def _solve_poisson_3d(self):
        """
        Solve 3D poisson problem, no projection, no correction.
        """
        fftw2py.solve_poisson_3d(*self.poisson_args)

    def _solve_diffuse_2d(self):
        """
        Solve 2D diffusion problem, 1-component buffer expected
        """
        fftw2py.solve_diffusion_2d(self.nu() * self.dt(), *self.diffuse_args)

    def _solve_diffuse_3d(self):
        """
        Solve 3D poisson problem with diffusion, no projection, no correction.
        """
        fftw2py.solve_diffusion_3d(self.nu() * self.dt(), *self.diffuse_args)
