# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


try:
    from hysop import f2hysop
except ImportError:
    msgE = "Could not load f2hysop submodule. "
    msgE += "Try to recompile HySoP."
    print(msgE)
    raise

try:
    from hysop.f2hysop import scales2py as scales
except ImportError:
    msgE = "Scales package not available for your hysop install. "
    msgE += "Try to recompile HySoP with WITH_SCALES=ON."
    print(msgE)
    raise

from hysop import __VERBOSE__, __DEBUG__
from hysop.constants import HYSOP_REAL
from hysop.tools.decorators import debug
from hysop.core.graph.computational_operator import ComputationalGraphOperator
from hysop.tools.htypes import check_instance, InstanceOf
from hysop.fields.continuous_field import Field
from hysop.fields.cartesian_discrete_field import CartesianDiscreteTensorField
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.methods import (
    Interpolation,
    TimeIntegrator,
    Remesh,
    MultiScaleInterpolation,
    StrangOrder,
)
from hysop.numerics.remesh.remesh import RemeshKernel
from hysop.tools.numpywrappers import npw
from hysop.core.graph.graph import op_apply
from hysop.numerics.odesolvers.runge_kutta import ExplicitRungeKutta, RK2
from hysop.backend.host.fortran.fortran_operator import FortranOperator


class ScalesAdvection(FortranOperator):
    """Particle advection using Fortran Scales library."""

    __rmsh_to_scales__ = {  # Mprime kernels
        Remesh.Mp4: "p_M4",
        Remesh.Mp6: "p_M6",
        Remesh.Mp8: "p_M8",
        # Lambda kernels
        Remesh.L2_1: "p_M4",
        Remesh.L4_2: "p_M6",
        Remesh.L4_4: "p_44",
        Remesh.L6_4: "p_64",
        Remesh.L6_6: "p_66",
        Remesh.L8_4: "p_84",
        # Corrected kernels
        Remesh.O2: "p_O2",
        Remesh.O4: "p_O4",
        # Corrected and limited kernels
        Remesh.L2: "p_L2",
    }

    __interpol_to_scales = {
        Interpolation.LINEAR: "lin",
        Interpolation.L4_4: "L4_4",
        Interpolation.M4: "M4",
        Interpolation.Mp4: "Mp4",
    }

    __dim_splitting_to_scales = {
        StrangOrder.STRANG_FIRST_ORDER: "classic",
        StrangOrder.STRANG_SECOND_ORDER: "strang",
    }

    __default_method = {
        TimeIntegrator: RK2,
        Interpolation: Interpolation.LINEAR,
        MultiScaleInterpolation: Interpolation.LINEAR,
        StrangOrder: StrangOrder.STRANG_SECOND_ORDER,
        Remesh: Remesh.L2_1,
    }

    __available_methods = {
        TimeIntegrator: RK2,
        Interpolation: Interpolation.LINEAR,
        MultiScaleInterpolation: __interpol_to_scales.keys(),
        StrangOrder: __dim_splitting_to_scales.keys(),
        Remesh: __rmsh_to_scales__.keys(),
    }

    @classmethod
    def default_method(cls):
        dm = super().default_method()
        dm.update(cls.__default_method)
        return dm

    @classmethod
    def available_methods(cls):
        am = super().available_methods()
        am.update(cls.__available_methods)
        return am

    @debug
    def __new__(
        cls, velocity, advected_fields_in, advected_fields_out, variables, dt, **kwds
    ):
        return super().__new__(
            cls,
            input_fields=None,
            output_fields=None,
            input_params=None,
            output_params=None,
            **kwds,
        )

    @debug
    def __init__(
        self, velocity, advected_fields_in, advected_fields_out, variables, dt, **kwds
    ):
        """Particle advection of field(s),
        on any backend, with cartesian remeshing.

        Parameters
        ----------
        velocity: Field
            Continuous velocity field (all components)
        advected_fields_in: Field or array like of Fields
            Instance or list of continuous fields to be advected.
        advected_fields_out: Field or array like of Field
            Where input fields should be remeshed.
        variables: dict
            Dictionary of continuous fields as keys and topologies as values.
        dt: ScalarParameter
            Timestep parameter that will be used for time integration.
        kwds:
            Extra parameters passed to generated directional operators.

        Attributes
        ----------
        velocity: Field
            Continuous velocity field (all components)
        advected_fields_in: list
            Tuple of continuous fields to be advected.
        advected_fields_out: list
            Tuple of output continuous fields.

        As HySoP works with C-order of transposition for data, we interface
        Scales library with the Z-Y-X data layout.

        """
        check_instance(velocity, Field)
        check_instance(advected_fields_in, tuple, values=Field)
        check_instance(advected_fields_out, tuple, values=Field)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(dt, ScalarParameter)
        assert len(advected_fields_in) == len(
            advected_fields_out
        ), "|inputs| != |outputs|"

        input_fields = {velocity: variables[velocity]}
        output_fields = {}
        input_params = {dt}
        output_params = set()

        is_inplace = True
        for ifield, ofield in zip(advected_fields_in, advected_fields_out):
            input_fields[ifield] = variables[ifield]
            output_fields[ofield] = variables[ofield]
            if ifield is ofield:
                assert (
                    is_inplace
                ), "Cannot mix inplace and out of place scales advection."
            else:
                is_inplace = False

        super().__init__(
            input_fields=input_fields,
            output_fields=output_fields,
            input_params=input_params,
            output_params=output_params,
            **kwds,
        )

        if (velocity.dim != 3) or (velocity.nb_components != 3):
            raise NotImplementedError("Scales only implements 3D advection.")
        if any((sfield.dim != 3) for sfield in self.fields):
            raise NotImplementedError("Scales only implements 3D advection.")
        for field in self.fields:
            if field.dtype != HYSOP_REAL:
                msg = "Scales only implements advection using precision speficied at "
                msg += "compile time (HYSOP_REAL={}) but field {} has dtype {}."
                msg = msg.format(HYSOP_REAL.__name__, field.name, field.dtype)
                raise RuntimeError(msg)

        self.velocity = velocity
        self.first_scalar = advected_fields_in[0].fields[0]
        self.advected_fields_in = advected_fields_in
        self.advected_fields_out = advected_fields_out
        self.dt = dt

        self.is_inplace = is_inplace

    @debug
    def handle_method(self, method):
        super().handle_method(method)

        # Translate directional split into Scales configuration
        strang_order = method.pop(StrangOrder)
        try:
            self._dim_split = self.__dim_splitting_to_scales[strang_order]
        except KeyError as e:
            print(
                "Unknown dimenstional splitting method for Scales ({} given).".format(
                    strang_order
                )
            )
            raise e

        # Translate hysop remesh kernels into Scales configuration
        self.remesh_kernel = method.pop(Remesh)
        try:
            self._scales_kernel = self.__rmsh_to_scales__[self.remesh_kernel]
        except KeyError as e:
            print(
                "Unknown remesh method for Scales ({} given).".format(
                    self.remesh_kernel
                )
            )
            raise e

        # Translate hysop multi scale interpolation to Scales interpolation
        ms_interp = method.pop(MultiScaleInterpolation)
        try:
            self._scales_interp = self.__interpol_to_scales[ms_interp]
        except KeyError as e:
            print(
                "Unknown multi scale interpolation method for Scales ({} given)".format(
                    ms_interp
                )
            )
            raise e

        self.time_integrator = method.pop(TimeIntegrator)
        assert self.time_integrator is RK2, "Scales uses RK2 time integration only"

        self.interp = method.pop(Interpolation)
        assert (
            self.interp is Interpolation.LINEAR
        ), "Scales uses linear interpolation only."

    @debug
    def get_field_requirements(self):
        requirements = super().get_field_requirements()
        dim = self.domain.dim
        for is_input, (field, td, req) in requirements.iter_requirements():
            req.min_ghosts = (0,) * 3
            req.max_ghosts = (0,) * 3
            can_split = [
                False,
            ] * dim
            can_split[0] = True
            req.can_split = can_split

        return requirements

    @debug
    def discretize(self):
        super().discretize()

        is_inplace = self.is_inplace
        dvelocity = self.get_input_discrete_field(self.velocity)

        # Ravel all tensor fields to scalar fields and get corresponding discrete scalar fields
        dadvected_fields_in = tuple(
            self.get_input_discrete_field(ifield)
            for itfield in self.advected_fields_in
            for ifield in itfield.fields
        )
        dadvected_fields_out = tuple(
            self.get_output_discrete_field(ofield)
            for otfield in self.advected_fields_out
            for ofield in otfield.fields
        )
        assert len(dadvected_fields_in) == len(dadvected_fields_out)
        if is_inplace:
            assert all(
                (din._dfield is dout._dfield)
                for (din, dout) in zip(dadvected_fields_in, dadvected_fields_out)
            )

        # check that every advected field has the same grid size and space step
        dS0 = self.get_input_discrete_field(self.first_scalar)
        for df in set(dadvected_fields_in + dadvected_fields_out):
            if any(df.space_step != dS0.space_step):
                msg = "Space step mismatch between discrete fields {} and {}."
                msg = msg.format(df.name, dS0.name)
                raise ValueError(msg)
            if any(df.resolution != dS0.resolution):
                msg = "Resolution mismatch between discrete fields {} and {}."
                msg = msg.format(df.name, dS0.name)
                raise ValueError(msg)

        # The SCALES library for advection works only with
        # 3D 1-component scalars or 3-components vectors so we
        # merge advected scalars back to 3-component tensors while we can.
        nscalars = len(dadvected_fields_in)
        assert nscalars >= 1

        dSin, dSout, all_buffers = (), (), ()
        # 3-components fields
        for i in range(nscalars // 3):
            dfields_in = dadvected_fields_in[3 * i : 3 * (i + 1)]
            dfields_out = dadvected_fields_out[3 * i : 3 * (i + 1)]
            sin = CartesianDiscreteTensorField.from_dfields(
                name=f"Sin{i}", dfields=dfields_in, shape=(3,)
            )
            sout = CartesianDiscreteTensorField.from_dfields(
                name=f"Sout{i}", dfields=dfields_out, shape=(3,)
            )
            buffers = dvelocity.buffers + sout.buffers
            assert all(b.flags.f_contiguous for b in buffers)

            dSin += (sin,)
            dSout += (sout,)
            all_buffers += (buffers,)

        # 1-components fields
        dSin += dadvected_fields_in[(nscalars // 3) * 3 :]
        dSout += dadvected_fields_out[(nscalars // 3) * 3 :]
        all_buffers += tuple(
            dvelocity.buffers + sout.buffers
            for sout in dadvected_fields_out[(nscalars // 3) * 3 :]
        )
        assert len(dSin) == len(dSout) == len(all_buffers) >= 1

        is_bilevel = None
        if any(dvelocity.compute_resolution != dS0.compute_resolution):
            is_bilevel = dvelocity.compute_resolution

        self.dvelocity = dvelocity
        self.dS0 = dS0
        self.dSin = dSin
        self.dSout = dSout
        self.dadvected_fields_in = dadvected_fields_in
        self.dadvected_fields_out = dadvected_fields_out
        self.is_bilevel = is_bilevel
        self.all_buffers = all_buffers

    @debug
    def setup(self, work):
        super().setup(work)
        v_topo = self.dvelocity.topology
        s_topo = self.dS0.topology
        msg0 = "No ghosts allowed in Scales advection"
        msg1 = "Scales is only for periodic domains."
        assert (v_topo.ghosts == 0).all(), msg0
        assert v_topo.mesh.periodicity.all(), msg1
        for dfi, dfo in zip(self.dadvected_fields_in, self.dadvected_fields_out):
            assert (dfi.topology.ghosts == 0).all(), msg0
            assert dfi.periodicity.all(), msg1
            assert (dfo.topology.ghosts == 0).all(), msg0
            assert dfo.periodicity.all(), msg1

        sresol = s_topo.mesh.grid_resolution
        assert (
            sresol % s_topo.proc_shape == 0
        ).all(), "Scales support only equally sized local resolutions"

        verbosity = __VERBOSE__ or __DEBUG__

        scalesres, global_start = scales.init_advection_solver(
            sresol,
            s_topo.domain.length,
            s_topo.proc_shape,
            self.mpi_params.comm.py2f(),
            verbosity,
            order=self._scales_kernel,
            dim_split=self._dim_split,
        )

        assert (scalesres == s_topo.mesh.local_resolution).all(), (
            "Scales resolution mismatch"
            + str(scalesres)
            + " != "
            + str(s_topo.mesh.local_resolution)
        )
        assert (global_start == s_topo.mesh.global_start).all(), (
            "Scales mesh start mismatch"
            + str(global_start)
            + " != "
            + str(s_topo.mesh.global_start)
        )

        if self.is_bilevel is not None:
            scales.init_multiscale(
                v_topo.mesh.grid_resolution[0],
                v_topo.mesh.grid_resolution[1],
                v_topo.mesh.grid_resolution[2],
                self._scales_interp,
            )

        scales_func = []
        for df in self.dSin:
            assert df.dim == 3
            if df.nb_components == 3:
                if self.is_bilevel is not None:
                    # 3D interpolation of the velocity before advection
                    scales_func.append(scales.solve_advection_inter_basic_vect)
                    # Other interpolation only 2D interpolation first and
                    # 1D interpolations before advections in each direction
                    # (slower than basic): solve_advection_inter
                else:
                    scales_func.append(scales.solve_advection_vect)
            elif df.nb_components == 1:
                if self.is_bilevel is not None:
                    scales_func.append(scales.solve_advection_inter_basic)
                else:
                    scales_func.append(scales.solve_advection)
            else:
                msg = "Scales only can only handle 3D advected field with one or three components."
                raise NotImplementedError(msg)
        self._scales_func = tuple(scales_func)

    @op_apply
    def apply(self, **kwds):
        """Solve advection using Fortran SCALES library"""
        super().apply(**kwds)

        # scales only operates inplace so we copy input to output first
        if not self.is_inplace:
            for dSin, dSout in zip(self.dSin, self.dSout):
                dSout.copy(dSin, compute_slices=True)

        # call scales advection
        dt = self.dt()

        for scales_func, buffers in zip(self._scales_func, self.all_buffers):
            scales_func(dt, *buffers)

    @classmethod
    def supports_mpi(cls):
        return True
