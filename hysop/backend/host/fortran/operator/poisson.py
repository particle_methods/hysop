# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.htypes import check_instance
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import Field
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.backend.host.fortran.operator.fortran_fftw import (
    fftw2py,
    FortranFFTWOperator,
)
from hysop.core.graph.graph import op_apply
from hysop.constants import HYSOP_REAL


class PoissonFFTW(FortranFFTWOperator):

    def __new__(cls, Fin, Fout, variables, extra_input_kwds=None, **kwds):
        return super().__new__(cls, input_fields=None, output_fields=None, **kwds)

    def __init__(self, Fin, Fout, variables, extra_input_kwds=None, **kwds):
        """Operator to solve Poisson equation using FFTW in Fortran.

        Solves:
            Laplacian(Fout) = Fin

        Parameters
        ----------
        Fout: Field
            Input continuous field (rhs).
        Fin: Field
            Output continuous field (lhs), possibly inplace.
        variables: dict
            Dictionary of fields as keys and topology descriptors as values.
        kwds: dict, optional
            Base class arguments.

        """

        check_instance(Fin, Field)
        check_instance(Fout, Field)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        assert Fin.nb_components == Fout.nb_components
        assert (
            Fin.nb_components == 1
        ), "Fortran FFTW poisson operator is implemented only for scalar fields"

        assert Fin.domain is Fout.domain, "only one domain is supported"
        assert variables[Fin] is variables[Fout], "only one topology is supported"

        input_fields = {Fin: variables[Fin]}
        output_fields = {Fout: variables[Fout]}

        super().__init__(input_fields=input_fields, output_fields=output_fields, **kwds)
        self.Fin = Fin
        self.Fout = Fout

    def initialize(self, **kwds):
        super().initialize(**kwds)
        dim = self.dim
        if dim == 2:
            self._solve = fftw2py.solve_laplace_2d
        elif dim == 3:
            self._solve = fftw2py.solve_laplace_3d

    @debug
    def discretize(self):
        if self.discretized:
            return
        super().discretize()
        self.dFin = self.get_input_discrete_field(self.Fin)
        self.dFout = self.get_output_discrete_field(self.Fout)
        assert (
            self.dFin.ghosts == self.dFout.ghosts
        ).all(), "Input and output fields must have the same ghosts."
        self.ghosts = self.dFin.ghosts.astype(HYSOP_REAL)  # prevent f2py copy
        self.buffers = self.dFin.buffers + self.dFout.buffers

    @op_apply
    def apply(self, **kargs):
        """Solves Poisson equation"""
        super().apply(**kargs)
        (buf_in, buf_out) = self.buffers
        self._solve(buf_in, buf_out, self.ghosts)
        self.dFout.exchange_ghosts()
