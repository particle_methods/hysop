# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import hysop

try:
    from hysop.f2hysop import fftw2py
except ImportError:
    raise
    msg = "HySoP fortran fftw bindings are not available "
    msg += "for your hysop install."
    msg += "Try to recompile HySoP with WITH_FFTW=ON"
    raise ImportError(msg)

from hysop.constants import (
    HYSOP_ORDER,
    HYSOP_REAL,
    TranspositionState,
    BoundaryCondition,
)
from hysop.tools.numpywrappers import npw
from hysop.tools.decorators import debug
from hysop.tools.htypes import check_instance
from hysop.core.graph.computational_operator import ComputationalGraphOperator
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.fields.continuous_field import Field
from hysop.backend.host.fortran.fortran_operator import FortranOperator


class FortranFFTWOperator(FortranOperator):
    """Base class for Fortran FFTW interface.

    Defines HySoP compatible fields requirements and initializes
    fftw2py interface.
    """

    @debug
    def __new__(cls, input_fields, output_fields, **kwds):
        return super().__new__(
            cls, input_fields=input_fields, output_fields=output_fields, **kwds
        )

    @debug
    def __init__(self, input_fields, output_fields, **kwds):
        """
        Parameters
        ----------
        input_field : dictionary of fields:topology with
            :class:`~hysop.fields.continuous_field.Field` as keys
        output_field: dictionary of fields:topology with
            :class:`~hysop.fields.continuous_field.Field` as keys
        """
        super().__init__(input_fields=input_fields, output_fields=output_fields, **kwds)
        check_instance(
            input_fields, dict, keys=Field, values=CartesianTopologyDescriptors
        )
        check_instance(
            output_fields, dict, keys=Field, values=CartesianTopologyDescriptors
        )

        nb_components = next(iter(input_fields)).nb_components
        tensor_fields = set(input_fields.keys()).union(output_fields.keys())
        for tf in tensor_fields:
            for fi in tf.fields:
                if fi.dtype != HYSOP_REAL:
                    msg = "FortranFFTW operators only work with HYSOP_REAL precision specified during hysop "
                    msg += "build."
                    msg += "\nHYSOP_REAL is {} but field {} has dtype {}."
                    msg = msg.format(HYSOP_REAL.__name__, fi.name, fi.dtype)
                    raise RuntimeError(msg)
                if any(
                    (bd != BoundaryCondition.PERIODIC) for bd in fi.lboundaries
                ) or any((bd != BoundaryCondition.PERIODIC) for bd in fi.rboundaries):
                    msg = "FortranFFTW operators only work with PERIODIC boundary conditions:"
                    msg += f"\n  operator:    {self.name}"
                    msg += f"\n  field:       {fi.pretty_name}"
                    msg += f"\n  lboundaries: {fi.lboundaries}"
                    msg += f"\n  rboundaries: {fi.rboundaries}"
                    raise RuntimeError(msg)

        # Special case: 3D diffusion of a scalar
        self._scalar_3d = (nb_components == 1) and all(
            tf.nb_components == 1 for tf in tensor_fields
        )

        domain = next(iter(self.input_fields)).domain
        self.dim = domain.dim
        self.domain = domain

    @debug
    def get_field_requirements(self):
        requirements = super().get_field_requirements()
        dim = self.domain.dim

        # Set can_split to True in all directions except the contiguous one
        # for inputs and outputs.
        for is_input, (field, td, req) in requirements.iter_requirements():
            can_split = [
                False,
            ] * dim
            can_split[0] = True
            req.can_split = can_split
        return requirements

    @debug
    def get_node_requirements(self):
        node_reqs = super().get_node_requirements()
        node_reqs.enforce_unique_ghosts = True
        return node_reqs

    def handle_topologies(self, input_topology_states, output_topology_states):
        super().handle_topologies(input_topology_states, output_topology_states)

        topology = next(iter(self.input_fields.values())).topology
        for field, topoview in self.input_fields.items():
            assert all(
                topoview.topology.cart_shape == topology.cart_shape
            ), "topology mismatch"
        for field, topoview in self.output_fields.items():
            assert all(
                topoview.topology.cart_shape == topology.cart_shape
            ), "topology mismatch"
        self.topology = topology

    @debug
    def discretize(self):
        if self.discretized:
            return
        super().discretize()
        topo_view = next(iter(self.input_discrete_fields.values())).topology
        self._fftw_discretize(topo_view)

    @debug
    def get_work_properties(self):
        return super().get_work_properties()

    @debug
    def setup(self, work=None):
        super().setup(work=work)

    @debug
    def finalize(self, clean_fftw_solver=False, **kwds):
        super().finalize(**kwds)
        if clean_fftw_solver:
            fftw2py.clean_fftw_solver(self.dim)

    def _fftw_discretize(self, topo_view):
        """
        fftw specific way to discretize variables for a given
        'reference' resolution.
        It is assumed that in fft case, only one topology shape must be used
        for all variables of all fortran fftw operators.
        """
        self._set_domain_and_tasks()
        topo = topo_view
        comm = self.mpi_params.comm
        size = self.mpi_params.size

        msg = "input topology is not compliant with fftw. {} {}".format(
            topo.cart_dim, topo.proc_shape
        )
        assert topo.cart_dim == 1, msg
        assert topo.proc_shape[-1] == size, msg

        global_resolution = topo.global_resolution.astype(npw.int64)
        length = topo.domain.length.astype(HYSOP_REAL)

        if self._scalar_3d:
            local_resolution, global_start = fftw2py.init_fftw_solver_scalar(
                global_resolution, length, comm=comm.py2f()
            )
        else:
            local_resolution, global_start = fftw2py.init_fftw_solver(
                global_resolution, length, comm=comm.py2f()
            )

        assert (
            topo.mesh.compute_resolution == local_resolution
        ).all(), "Local resolution mismatch ({} != {}).".format(
            topo.mesh.compute_resolution, local_resolution
        )
        assert (
            topo.mesh.global_start == global_start
        ).all(), "Global start mismatch ({} != {}).".format(
            topo.mesh.global_start, global_start
        )

    @classmethod
    def supports_mpi(cls):
        return True
