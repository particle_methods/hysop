# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.backend.host.fortran.operator.fortran_fftw import (
    FortranFFTWOperator,
    fftw2py,
)
from hysop.tools.htypes import check_instance, InstanceOf
from hysop.tools.decorators import debug
from hysop.tools.numpywrappers import npw
from hysop.fields.continuous_field import Field
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.core.graph.graph import op_apply


class DiffusionFFTW(FortranFFTWOperator):

    @debug
    def __new__(cls, Fin, Fout, nu, variables, dt, **kargs):
        return super().__new__(
            cls, input_fields=None, output_fields=None, input_params=None, **kargs
        )

    @debug
    def __init__(self, Fin, Fout, nu, variables, dt, **kargs):
        """Diffusion operator base.

        Parameters
        ----------
        Fin : :class:`~hysop.fields.continuous_field.Field`
            The input field to be diffused.
        Fout:  :class:`~hysop.fields.continuous_field.Field`
            The output field to be diffused.
        variables: dictionary of fields:topology
            The choosed discretizations.
        nu : float or ScalarParameter.
            nu value.
        dt: ScalarParameter
            Timestep parameter that will be used for time integration.
        kargs:
            Base class parameters.

        Notes:
            *Equations:
                dF/dt = nu*Laplacian(F)
                in  = Win
                out = Wout

            *Implicit resolution in Fourier space:
                F_hat(tn+1) = 1/(1-nu*dt*sum(Ki**2)) * F_hat(tn)
        """
        check_instance(Fin, Field)
        check_instance(Fout, Field)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(nu, (float, ScalarParameter))
        check_instance(dt, ScalarParameter)

        assert (
            Fin.nb_components == Fout.nb_components
        ), "input and output components mismatch"
        assert variables[Fin] == variables[Fout], "input and output topology mismatch"
        assert Fin.domain is Fout.domain, "input and output domain mismatch"

        input_fields = {Fin: variables[Fin]}
        output_fields = {Fout: variables[Fout]}
        input_params = {dt}
        if isinstance(nu, ScalarParameter):
            input_params.update({nu})
        else:
            self._real_nu = nu
            nu = lambda: self._real_nu

        super().__init__(
            input_fields=input_fields,
            output_fields=output_fields,
            input_params=input_params,
            **kargs,
        )

        self.Fin = Fin
        self.Fout = Fout
        self.is_inplace = Fin is Fout
        self.nu = nu
        self.dt = dt

    def initialize(self, **kwds):
        super().initialize(**kwds)
        dim = self.dim
        if dim == 2:
            # only 1-component fields are supported in 2D
            self._solve = self._solve_2d
            self._nfields_per_call = 1
        elif dim == 3:
            # only 3-component fields are supported in 3D
            if self.Fin.nb_components % 3 == 0:
                self._solve = self._solve_3d
                self._nfields_per_call = 3
            elif self.Fin.nb_components == 1:
                self._solve = self._solve_3d_scalar
                self._nfields_per_call = 1
            else:
                msg = "Only mutiple of 3-component fields or scalar fields"
                msg += " are supported for 3D FFTW diffusion."
                raise NotImplementedError(msg)
        else:
            raise NotImplementedError(str(dim) + "D case not yet implemented.")

    @debug
    def discretize(self):
        if self.discretized:
            return
        super().discretize()
        dFin = self.get_input_discrete_field(self.Fin)
        dFout = self.get_output_discrete_field(self.Fout)
        assert npw.array_equal(dFin.compute_resolution, dFout.compute_resolution)
        self.dFin = dFin
        self.dFout = dFout
        self.input_buffers = dFin.compute_buffers
        self.output_buffers = dFout.compute_buffers
        if not self.dFout.has_unique_ghosts():
            msg = "Ghosts are not the same in all directions for output field {}."
            msg = msg.format(self.dFout.short_description())
            raise RuntimeError(msg)
        self.buffers = self.dFout.buffers
        self.ghosts = self.dFout.ghosts.astype(npw.int64)
        assert all(b.flags.f_contiguous for b in self.buffers)

    @op_apply
    def apply(self, **kargs):
        super().apply(**kargs)
        nudt = self.dt() * self.nu()
        buffers, ghosts = self.buffers, self.ghosts

        if not self.is_inplace:
            self.dFout.copy(self.dFin, compute_slice=True)

        nfields_per_call = self._nfields_per_call
        for i in range(self.dFout.nb_components // nfields_per_call):
            bufs = buffers[nfields_per_call * i : nfields_per_call * (i + 1)]
            self._solve(nudt, ghosts, *bufs)

        self.dFout.exchange_ghosts()

    def _solve_2d(self, nudt, ghosts, *buffers):
        """
        Solve 2D diffusion problem, 1-component buffer expected
        """
        fftw2py.solve_diffusion_2d(nudt, *(buffers + (ghosts,)))

    def _solve_3d(self, nudt, ghosts, *buffers):
        """
        Solve 3D diffusion problem, 3-component buffers expected
        """
        fftw2py.solve_diffusion_3d(nudt, *(buffers + (ghosts,)))

    def _solve_3d_scalar(self, nudt, ghosts, *buffers):
        """
        Solve 3D diffusion problem, scalar buffers expected
        """
        fftw2py.solve_diffusion_scalar_3d(nudt, buffers, ghosts)
