# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABCMeta
from hysop.tools.decorators import debug
from hysop.operator.directional.directional import DirectionalOperatorBase
from hysop.backend.host.host_operator import HostOperator


class HostDirectionalOperator(DirectionalOperatorBase, HostOperator, metaclass=ABCMeta):
    """
    Abstract class for discrete directional operators working on host backends.

    Field requirements are set such that the current direction will
    be contiguous in memory.
    """

    @debug
    def __init__(self, **kwds):
        """
        Create a directional operator in a given direction, Host version.
        """
        super().__init__(**kwds)
