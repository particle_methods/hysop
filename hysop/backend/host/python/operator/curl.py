# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import functools
from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.decorators import debug
from hysop.tools.numpywrappers import npw
from hysop.backend.host.host_operator import HostOperator
from hysop.core.graph.graph import op_apply
from hysop.operator.base.curl import SpectralCurlOperatorBase

try:
    import numba as nb
    from hysop import __DEFAULT_NUMBA_TARGET__

    @nb.guvectorize(
        [
            nb.void(nb.float32[:, ::-1], nb.float32[::-1], nb.float32[:, ::-1]),
            nb.void(nb.complex64[:, ::-1], nb.float32[::-1], nb.complex64[:, ::-1]),
            nb.void(nb.complex64[:, ::-1], nb.complex64[::-1], nb.complex64[:, ::-1]),
            nb.void(nb.float64[:, ::-1], nb.float64[::-1], nb.float64[:, ::-1]),
            nb.void(nb.complex128[:, ::-1], nb.float64[::-1], nb.complex128[:, ::-1]),
            nb.void(
                nb.complex128[:, ::-1], nb.complex128[::-1], nb.complex128[:, ::-1]
            ),
        ],
        "(n,m),(m)->(n,m)",
        target=__DEFAULT_NUMBA_TARGET__,
        nopython=True,
        cache=True,
    )
    def filter_curl_2d__0(Fin, K, Fout):
        for i in range(0, Fin.shape[0]):
            for j in range(0, Fin.shape[1]):
                Fout[i, j] = +K[j] * Fin[i, j]

    @nb.guvectorize(
        [
            nb.void(nb.float32[:, ::-1], nb.float32[::-1], nb.float32[:, ::-1]),
            nb.void(nb.complex64[:, ::-1], nb.float32[::-1], nb.complex64[:, ::-1]),
            nb.void(nb.complex64[:, ::-1], nb.complex64[::-1], nb.complex64[:, ::-1]),
            nb.void(nb.float64[:, ::-1], nb.float64[::-1], nb.float64[:, ::-1]),
            nb.void(nb.complex128[:, ::-1], nb.float64[::-1], nb.complex128[:, ::-1]),
            nb.void(
                nb.complex128[:, ::-1], nb.complex128[::-1], nb.complex128[:, ::-1]
            ),
        ],
        "(n,m),(m)->(n,m)",
        target=__DEFAULT_NUMBA_TARGET__,
        nopython=True,
        cache=True,
    )
    def filter_curl_2d__1(Fin, K, Fout):
        for i in range(0, Fin.shape[0]):
            for j in range(0, Fin.shape[1]):
                Fout[i, j] = -K[j] * Fin[i, j]

    @nb.guvectorize(
        [
            nb.void(nb.float32[:, :, ::-1], nb.float32[::-1], nb.float32[:, :, ::-1]),
            nb.void(
                nb.complex64[:, :, ::-1], nb.float32[::-1], nb.complex64[:, :, ::-1]
            ),
            nb.void(
                nb.complex64[:, :, ::-1], nb.complex64[::-1], nb.complex64[:, :, ::-1]
            ),
            nb.void(nb.float64[:, :, ::-1], nb.float64[::-1], nb.float64[:, :, ::-1]),
            nb.void(
                nb.complex128[:, :, ::-1], nb.float64[::-1], nb.complex128[:, :, ::-1]
            ),
            nb.void(
                nb.complex128[:, :, ::-1],
                nb.complex128[::-1],
                nb.complex128[:, :, ::-1],
            ),
        ],
        "(n,m,p),(p)->(n,m,p)",
        target=__DEFAULT_NUMBA_TARGET__,
        nopython=True,
        cache=True,
    )
    def filter_curl_3d__0(Fin, K, Fout):
        for i in range(0, Fin.shape[0]):
            for j in range(0, Fin.shape[1]):
                for k in range(0, Fin.shape[2]):
                    Fout[i, j, k] = +K[k] * Fin[i, j, k]

    @nb.guvectorize(
        [
            nb.void(nb.float32[:, :, ::-1], nb.float32[::-1], nb.float32[:, :, ::-1]),
            nb.void(
                nb.complex64[:, :, ::-1], nb.float32[::-1], nb.complex64[:, :, ::-1]
            ),
            nb.void(
                nb.complex64[:, :, ::-1], nb.complex64[::-1], nb.complex64[:, :, ::-1]
            ),
            nb.void(nb.float64[:, :, ::-1], nb.float64[::-1], nb.float64[:, :, ::-1]),
            nb.void(
                nb.complex128[:, :, ::-1], nb.float64[::-1], nb.complex128[:, :, ::-1]
            ),
            nb.void(
                nb.complex128[:, :, ::-1],
                nb.complex128[::-1],
                nb.complex128[:, :, ::-1],
            ),
        ],
        "(n,m,p),(p)->(n,m,p)",
        target=__DEFAULT_NUMBA_TARGET__,
        nopython=True,
        cache=True,
    )
    def filter_curl_3d__1(Fin, K, Fout):
        for i in range(0, Fin.shape[0]):
            for j in range(0, Fin.shape[1]):
                for k in range(0, Fin.shape[2]):
                    Fout[i, j, k] = -K[k] * Fin[i, j, k]

except ModuleNotFoundError:

    def filter_curl_2d__0(Fin, K, Fout):
        for i in range(0, Fin.shape[0]):
            for j in range(0, Fin.shape[1]):
                Fout[i, j] = +K[j] * Fin[i, j]

    def filter_curl_2d__1(Fin, K, Fout):
        for i in range(0, Fin.shape[0]):
            for j in range(0, Fin.shape[1]):
                Fout[i, j] = -K[j] * Fin[i, j]

    def filter_curl_3d__0(Fin, K, Fout):
        for i in range(0, Fin.shape[0]):
            for j in range(0, Fin.shape[1]):
                for k in range(0, Fin.shape[2]):
                    Fout[i, j, k] = +K[k] * Fin[i, j, k]

    def filter_curl_3d__1(Fin, K, Fout):
        for i in range(0, Fin.shape[0]):
            for j in range(0, Fin.shape[1]):
                for k in range(0, Fin.shape[2]):
                    Fout[i, j, k] = -K[k] * Fin[i, j, k]


class PythonSpectralCurl(SpectralCurlOperatorBase, HostOperator):
    """
    Compute the curl by using an python FFT backend.
    """

    def setup(self, work):
        super().setup(work=work)

        dim = self.dim
        Fin, Fout = self.Fin, self.Fout
        K, FIN, FOUT = self.dK, self.FIN, self.FOUT

        msg = "Unsupported number of components {}."
        if dim == 2:
            if Fin.nb_components in (1, 2):
                curl_F0 = functools.partial(filter_curl_2d__0, FIN[0], K[0], FOUT[0])
                curl_F1 = functools.partial(filter_curl_2d__1, FIN[1], K[1], FOUT[1])
                curl_filters = (curl_F0, curl_F1)
            else:
                raise ValueError(msg.format(Fin.nb_components))
        elif dim == 3:
            if Fin.nb_components == 3:
                assert Fout.nb_components == 3, Fout.nb_components
                curl_filters = ()
                for i in range(3):
                    curl_Fi = functools.partial(
                        filter_curl_3d__0, FIN[i], K[i], FOUT[i]
                    )
                    curl_filters += (curl_Fi,)
                for i in range(3):
                    curl_Fi = functools.partial(
                        filter_curl_3d__1, FIN[3 + i], K[3 + i], FOUT[3 + i]
                    )
                    curl_filters += (curl_Fi,)
            else:
                raise ValueError(msg.format(Fin.nb_components))
        else:
            msg = f"Unsupported dimension {dim}."
            raise ValueError(msg)
        assert (
            len(self.forward_transforms)
            == len(self.backward_transforms)
            == len(curl_filters)
        )
        self.curl_filters = curl_filters

    @op_apply
    def apply(self, simulation=None, **kwds):
        """Apply analytic formula."""
        super().apply(**kwds)
        for Ft, curl_filter, Bt in zip(
            self.forward_transforms, self.curl_filters, self.backward_transforms
        ):
            Ft()
            curl_filter()
            Bt()
        self.dFout.exchange_ghosts()
