# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import functools

from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.decorators import debug
from hysop.tools.numpywrappers import npw
from hysop.backend.host.host_operator import HostOperator, OpenClMappable
from hysop.core.graph.graph import op_apply
from hysop.fields.continuous_field import Field
from hysop.parameters.parameter import Parameter
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.operator.base.poisson import PoissonOperatorBase


class PythonPoisson(PoissonOperatorBase, OpenClMappable, HostOperator):
    """
    Solves the poisson equation using one of the host python fft backend (fftw, numpy or scipy).
    """

    @classmethod
    def build_poisson_filter(cls, dim, *args, **kwds):
        assert len(args) == 2 + dim
        try:
            import numba as nb
            from hysop.tools.numba_utils import make_numba_signature, prange
            from hysop import __DEFAULT_NUMBA_TARGET__

            target = kwds.get("target", __DEFAULT_NUMBA_TARGET__)
            signature, _ = make_numba_signature(*args)
            if dim == 1:

                @nb.guvectorize(
                    [signature],
                    "(n),(n)->(n)",
                    target=target,
                    nopython=True,
                    cache=True,
                )
                def filter_poisson_1d(Fin, K0, Fout):
                    Fout[0] = 0.0
                    for i in range(1, Fin.shape[0]):
                        Fout[i] /= K0[i]

                F = filter_poisson_1d
            elif dim == 2:

                @nb.guvectorize(
                    [signature],
                    "(n,m),(n),(m)->(n,m)",
                    target=target,
                    nopython=True,
                    cache=True,
                )
                def filter_poisson_2d(Fin, K0, K1, Fout):
                    for i in prange(1, Fin.shape[0]):
                        for j in range(0, Fin.shape[1]):
                            Fout[i, j] /= K0[i] + K1[j]
                    for j in range(1, Fin.shape[1]):
                        Fout[0, j] /= K0[0] + K1[j]
                    Fout[0, 0] = 0

                F = filter_poisson_2d
            elif dim == 3:

                @nb.guvectorize(
                    [signature],
                    "(n,m,p),(n),(m),(p)->(n,m,p)",
                    target=target,
                    nopython=True,
                    cache=True,
                )
                def filter_poisson_3d(Fin, K0, K1, K2, Fout):
                    for i in prange(1, Fin.shape[0]):
                        for j in prange(0, Fin.shape[1]):
                            for k in range(0, Fin.shape[2]):
                                Fout[i, j, k] /= K0[i] + K1[j] + K2[k]
                    for j in prange(1, Fin.shape[1]):
                        for k in range(0, Fin.shape[2]):
                            Fout[0, j, k] /= K0[0] + K1[j] + K2[k]
                    for k in range(1, Fin.shape[2]):
                        Fout[0, 0, k] /= K0[0] + K1[0] + K2[k]
                    Fout[0, 0, 0] = 0.0

                F = filter_poisson_3d
            elif dim == 4:

                @nb.guvectorize(
                    [signature],
                    "(n,m,p,q),(n),(m),(p),(q)->(n,m,p,q)",
                    target=target,
                    nopython=True,
                    cache=True,
                )
                def filter_poisson_4d(Fin, K0, K1, K2, K3, Fout):
                    for i in prange(1, Fin.shape[0]):
                        for j in prange(0, Fin.shape[1]):
                            for k in prange(0, Fin.shape[2]):
                                for l in range(0, Fin.shape[3]):
                                    Fout[i, j, k, l] /= K0[i] + K1[j] + K2[k] + K3[l]
                    for j in prange(1, Fin.shape[1]):
                        for k in prange(0, Fin.shape[2]):
                            for l in range(0, Fin.shape[3]):
                                Fout[0, j, k, l] /= K0[0] + K1[j] + K2[k] + K3[l]
                    for k in prange(1, Fin.shape[2]):
                        for l in range(0, Fin.shape[3]):
                            Fout[0, 0, k, l] /= K0[0] + K1[0] + K2[k] + K3[l]
                    for l in range(1, Fin.shape[3]):
                        Fout[0, 0, 0, l] /= K0[0] + K1[0] + K2[0] + K3[l]
                    Fout[0, 0, 0, 0] = 0.0

                F = filter_poisson_4d
            else:
                msg = f"{dim}D Poisson filter has not been vectorized yet."
                raise NotImplementedError(msg)
        except ModuleNotFoundError:
            if dim == 1:

                def filter_poisson_1d(Fin, K0, Fout):
                    Fout[0] = 0.0
                    for i in range(1, Fin.shape[0]):
                        Fout[i] /= K0[i]

                F = filter_poisson_1d
            elif dim == 2:

                def filter_poisson_2d(Fin, K0, K1, Fout):
                    for i in range(1, Fin.shape[0]):
                        for j in range(0, Fin.shape[1]):
                            Fout[i, j] /= K0[i] + K1[j]
                    for j in range(1, Fin.shape[1]):
                        Fout[0, j] /= K0[0] + K1[j]
                    Fout[0, 0] = 0

                F = filter_poisson_2d
            elif dim == 3:

                def filter_poisson_3d(Fin, K0, K1, K2, Fout):
                    for i in range(1, Fin.shape[0]):
                        for j in range(0, Fin.shape[1]):
                            for k in range(0, Fin.shape[2]):
                                Fout[i, j, k] /= K0[i] + K1[j] + K2[k]
                    for j in range(1, Fin.shape[1]):
                        for k in range(0, Fin.shape[2]):
                            Fout[0, j, k] /= K0[0] + K1[j] + K2[k]
                    for k in range(1, Fin.shape[2]):
                        Fout[0, 0, k] /= K0[0] + K1[0] + K2[k]
                    Fout[0, 0, 0] = 0.0

                F = filter_poisson_3d
            elif dim == 4:

                def filter_poisson_4d(Fin, K0, K1, K2, K3, Fout):
                    for i in range(1, Fin.shape[0]):
                        for j in range(0, Fin.shape[1]):
                            for k in range(0, Fin.shape[2]):
                                for l in range(0, Fin.shape[3]):
                                    Fout[i, j, k, l] /= K0[i] + K1[j] + K2[k] + K3[l]
                    for j in range(1, Fin.shape[1]):
                        for k in range(0, Fin.shape[2]):
                            for l in range(0, Fin.shape[3]):
                                Fout[0, j, k, l] /= K0[0] + K1[j] + K2[k] + K3[l]
                    for k in range(1, Fin.shape[2]):
                        for l in range(0, Fin.shape[3]):
                            Fout[0, 0, k, l] /= K0[0] + K1[0] + K2[k] + K3[l]
                    for l in range(1, Fin.shape[3]):
                        Fout[0, 0, 0, l] /= K0[0] + K1[0] + K2[0] + K3[l]
                    Fout[0, 0, 0, 0] = 0.0

                F = filter_poisson_4d
            else:
                msg = f"{dim}D Poisson filter has not been vectorized yet."
                raise NotImplementedError(msg)
        return functools.partial(F, *args)

    def setup(self, work):
        super().setup(work=work)
        poisson_filters = ()
        for Fo, Ft, Kd in zip(
            self.dFout.dfields, self.forward_transforms, self.all_dkds
        ):
            args = (Ft.full_output_buffer,) + tuple(Kd) + (Ft.full_output_buffer,)
            F = self.build_poisson_filter(Fo.dim, *args)
            poisson_filters += (F,)
        self.poisson_filters = poisson_filters

    @op_apply
    def apply(self, simulation, **kwds):
        """Solve the Poisson equation."""
        super().apply(**kwds)

        with self.map_objects_to_host():
            for Ft, Bt, filter_poisson in zip(
                self.forward_transforms, self.backward_transforms, self.poisson_filters
            ):
                Ft(simulation=simulation)
                filter_poisson()
                Bt(simulation=simulation)

        for Fo in self.dFout.dfields:
            Fo.exchange_ghosts()
        self.plot(simulation=simulation)
