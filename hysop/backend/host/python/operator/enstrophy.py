# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
@file enstrophy.py
Enstrophy solver python backend.
"""
from hysop.tools.decorators import debug
from hysop.core.graph.graph import op_apply
from hysop.operator.base.enstrophy import EnstrophyBase
from hysop.backend.host.host_operator import HostOperator
from hysop.tools.numpywrappers import npw


class PythonEnstrophy(EnstrophyBase, HostOperator):
    """Compute enstrophy of the given vorticity field."""

    @debug
    def __new__(cls, **kwds):
        return super().__new__(cls, **kwds)

    @debug
    def __init__(self, **kwds):
        super().__init__(**kwds)

    @debug
    def setup(self, work):
        super().setup(work=work)
        self.W_buffers = self.dWin.compute_buffers
        self.WdotW_buffer = self.dWdotW.compute_buffers[0]

    @op_apply
    def apply(self, **kwds):
        W = self.W_buffers
        WdW = self.WdotW_buffer

        WdW[...] = 0.0
        for Wi in W:
            WdW[...] += self.rho_0 * (Wi**2)

        # Compute enstrophy
        local_enstrophy = self.coeff * npw.sum(WdW, dtype=self.enstrophy.dtype)

        # collect enstrophy from all processes
        self.enstrophy.value = self._collect(local_enstrophy)
