# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
@file convergence.py
Convergence python backend.
"""
from hysop.constants import HYSOP_REAL
from hysop.backend.host.host_operator import HostOperator
from hysop.operator.base.convergence import ConvergenceBase
from hysop.tools.decorators import debug
from hysop.core.graph.graph import op_apply
from hysop.tools.numpywrappers import npw
from hysop.constants import ResidualError
import mpi4py.MPI as MPI
import numpy as np


class PythonConvergence(ConvergenceBase, HostOperator):
    """Computes convergence of a field through iterations"""

    @debug
    def __new__(cls, **kwds):
        return super().__new__(cls, **kwds)

    @debug
    def __init__(self, **kwds):
        super().__init__(**kwds)

    @debug
    def setup(self, **kwds):
        super().setup(**kwds)
        self.field_buffers = self.dField.compute_buffers

        self._tmp_convergence = npw.zeros(
            (1 + self.field.nb_components), dtype=self.convergence.dtype
        )
        self._tmp_reduce = npw.zeros(
            (1 + self.field.nb_components), dtype=self.convergence.dtype
        )

        self.__compute_error_absolute = lambda ui, ui_old: npw.max(npw.abs(ui - ui_old))
        self.__compute_error_relative = (
            lambda ui, ui_old, max_ui: npw.max(npw.abs(ui - ui_old)) / max_ui
        )
        if self._residual_computation == ResidualError.ABSOLUTE:
            self.__compute_error = self.__compute_error_absolute
        elif self._residual_computation == ResidualError.RELATIVE:
            self.__compute_error = self.__compute_error_relative
        else:
            raise RuntimeError("Unknown residual computation method.")

        self._eps = npw.finfo(HYSOP_REAL).eps
        self._large_zero = 1e3 * npw.finfo(HYSOP_REAL).eps

    @op_apply
    def apply(self, **kwds):
        u = self.dField.compute_buffers
        u_old = self.dField_old

        self._tmp_convergence[...] = 0.0
        for i, (ui, ui_old) in enumerate(zip(u, u_old)):
            self._tmp_convergence[i] = self.__compute_error_absolute(ui, ui_old)
            ui_old[...] = ui
        self._tmp_convergence[-1] = npw.sum(self._tmp_convergence)
        if self._residual_computation == ResidualError.RELATIVE:
            max_u = npw.max([npw.max(npw.abs(_)) for _ in u])
            self._tmp_convergence /= max_u
        self.mpi_params.comm.Allreduce(
            sendbuf=self._tmp_convergence, recvbuf=self._tmp_reduce, op=MPI.MAX
        )
        self.convergence.value = self._tmp_reduce[-1]

    @classmethod
    def supports_mpi(cls):
        return True
