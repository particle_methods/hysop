# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.decorators import debug
from hysop.tools.htypes import check_instance
from hysop.fields.continuous_field import Field, VectorField
from hysop.parameters.parameter import Parameter
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.backend.host.host_operator import HostOperator
from hysop.core.graph.graph import op_apply
from hysop.operator.base.custom import CustomOperatorBase


class PythonCustomOperator(CustomOperatorBase, HostOperator):

    @debug
    def __new__(cls, forward_apply_kwds=None, **kwds):
        return super().__new__(cls, **kwds)

    @debug
    def __init__(
        self,
        func,
        invars=None,
        outvars=None,
        extra_args=None,
        forward_apply_kwds=False,
        **kwds,
    ):
        super().__init__(
            func, invars=invars, outvars=outvars, extra_args=extra_args, **kwds
        )

        from inspect import signature

        nb_args = len(signature(func).parameters)
        nb_in_f, nb_in_p, nb_out_f, nb_out_p, nb_extra = 0, 0, 0, 0, 0
        if invars is not None:
            for v in invars:
                if isinstance(v, Field):
                    nb_in_f += v.nb_components
                elif isinstance(v, Parameter):
                    nb_in_p += 1
        if outvars is not None:
            for v in outvars:
                if isinstance(v, Field):
                    nb_out_f += v.nb_components
                elif isinstance(v, Parameter):
                    nb_out_p += 1
        if not extra_args is None:
            nb_extra = len(extra_args)
        msg = f"function arguments ({signature(func)}) did not match given in/out "
        msg += f"fields and parameters ({nb_in_f} input fields, {nb_in_p} input params,"
        msg += f" {nb_out_f} output fields, {nb_out_p} output params)."
        assert (
            nb_args
            == nb_in_f + nb_in_p + nb_out_f + nb_out_p + nb_extra + forward_apply_kwds
        ), msg
        self._forward_apply_kwds = forward_apply_kwds

    @op_apply
    def apply(self, **kwds):
        super().apply(**kwds)
        if self._forward_apply_kwds:
            self.func(
                *(
                    self.dinvar
                    + self.dinparam
                    + self.doutvar
                    + self.doutparam
                    + self.extra_args
                ),
                **kwds,
            )
        else:
            self.func(
                *(
                    self.dinvar
                    + self.dinparam
                    + self.doutvar
                    + self.doutparam
                    + self.extra_args
                )
            )
        for gh_exch in self.ghost_exchanger:
            gh_exch.exchange_ghosts()
