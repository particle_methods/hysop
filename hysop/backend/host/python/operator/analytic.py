# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.decorators import debug
from hysop.backend.host.host_operator import HostOperator
from hysop.core.graph.graph import op_apply
from hysop.fields.continuous_field import Field, ScalarField, VectorField
from hysop.parameters.parameter import Parameter
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors


class PythonAnalyticField(HostOperator):
    """
    Applies an analytic formula, given by user, on its field.
    Formula is given as a python method that operates on numpy arrays.
    """

    @debug
    def __new__(cls, field, formula, variables, extra_input_kwds=None, **kwds):
        return super().__new__(
            cls, input_fields=None, output_fields=None, input_params=None, **kwds
        )

    @debug
    def __init__(self, field, formula, variables, extra_input_kwds=None, **kwds):
        """
        Initialize a Analytic operator on the python backend.

        Apply a user-defined formula onto a field, possibly
        dependent on space variables and external fields/parameters.

        Parameters
        ----------
        field: hysop.field.continuous_field.ScalarField
            Continuous field to be modified.
        formula : callable
            The formula to be applied onto the field.
            Should have the following arguments:
                field:  the discrete field being modified.
                coords: the coordinates (x0, ..., xN) of the field being modified.
                **kwds: extra keywords contained in extra_input_kwds.
        variables: dict
            Dictionary of fields as keys and topology descriptors as values.
        extra_input_kwds: dict, optional
            Extra inputs that will be forwarded to the formula.
            Fields and Parameters are handled correctly as input requirements.
            If the output field is modified inplace, it should be added
            to extra_input_kwds.
        kwds: dict, optional
            Base class arguments.
        """
        extra_input_kwds = first_not_None(extra_input_kwds, {})

        check_instance(field, (ScalarField, VectorField))
        assert callable(formula), type(formula)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(extra_input_kwds, dict, keys=str)

        input_fields = {}
        output_fields = {field: self.get_topo_descriptor(variables, field)}
        input_params = set()

        extra_kwds = {}
        map_fields = {}
        for k, v in extra_input_kwds.items():
            if isinstance(v, Field):
                input_fields[v] = self.get_topo_descriptor(variables, v)
                map_fields[v] = k
            elif isinstance(v, Parameter):
                input_params.update({v})
                extra_kwds[k] = v
            else:
                extra_kwds[k] = v

        super().__init__(
            input_fields=input_fields,
            output_fields=output_fields,
            input_params=input_params,
            **kwds,
        )

        self.field = field
        self.formula = formula
        self.extra_kwds = extra_kwds
        self.map_fields = map_fields

    @debug
    def discretize(self):
        if self.discretized:
            return
        super().discretize()
        dfield = self.get_output_discrete_field(self.field)
        extra_kwds = self.extra_kwds
        map_fields = self.map_fields
        assert "data" not in extra_kwds
        assert "coords" not in extra_kwds
        extra_kwds["data"] = dfield.compute_data[0]
        if len(dfield.compute_data) > 1:
            extra_kwds["data"] = dfield.compute_data
        extra_kwds["coords"] = dfield.compute_mesh_coords
        for field, dfield in self.input_discrete_fields.items():
            assert field.name not in extra_kwds, field.name
            extra_kwds[map_fields[field]] = dfield.compute_data
        self.dfield = dfield

    @op_apply
    def apply(self, **kwds):
        """Apply analytic formula."""
        super().apply(**kwds)
        self.formula(**self.extra_kwds)
        self.dfield.exchange_ghosts()

    @classmethod
    def supports_mpi(cls):
        return True
