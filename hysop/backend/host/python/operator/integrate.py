# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.decorators import debug
from hysop.core.graph.graph import op_apply
from hysop.backend.host.host_operator import HostOperator
from hysop.operator.base.integrate import IntegrateBase
import numpy as np


class PythonIntegrate(IntegrateBase, HostOperator):

    @debug
    def __new__(cls, **kwds):
        return super().__new__(cls, **kwds)

    @debug
    def __init__(self, **kwds):
        super().__init__(**kwds)
        assert (
            self.expr is None
        ), "expr not yet implemented for Python backend (see opencl integrate operator)"

    @op_apply
    def apply(self, **kwds):
        value = self.parameter._value.copy()
        for i in range(self.dF.nb_components):
            Pi = np.sum(self.dF.data[i][self.dF.compute_slices])
            if self.scaling_coeff[i] is None:
                self.scaling_coeff[i] = 1.0 / Pi
            value[i] = self.scaling_coeff[i] * Pi

        # compute value from all processes
        self.parameter.value = self._collect(value)
