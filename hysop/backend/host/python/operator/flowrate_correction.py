# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.backend.host.host_operator import HostOperator
from hysop.tools.htypes import check_instance
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import Field
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.parameters.tensor_parameter import TensorParameter
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.core.graph.graph import op_apply
import numpy as np


class PythonFlowRateCorrection(HostOperator):
    """
    Update velocity field (solution of Poisson equation)
    in order to prescribe proper mean flow and ensure
    the desired inlet flowrate.

    Flowrate is corrected from the current flowrate through
    the box input face (X=orgin, normal=(-1,0,0))
    Velocity is corrected in X-direction from prescribed flowrate
    Velocity is corrected in Y and Z-direction from mean vorticity
    """

    @debug
    def __new__(
        cls,
        velocity,
        vorticity,
        dt,
        flowrate,
        variables,
        absorption_start=None,
        implementation=None,
        **kwds,
    ):
        return super().__new__(
            cls, input_fields=None, output_fields=None, input_params=None, **kwds
        )

    @debug
    def __init__(
        self,
        velocity,
        vorticity,
        dt,
        flowrate,
        variables,
        absorption_start=None,
        implementation=None,
        **kwds,
    ):
        check_instance(velocity, Field)
        check_instance(vorticity, Field)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(flowrate, TensorParameter)
        check_instance(dt, ScalarParameter)

        input_fields = {velocity: variables[velocity], vorticity: variables[vorticity]}
        output_fields = {velocity: variables[velocity]}
        input_params = {flowrate}

        self.velocity = velocity
        self.vorticity = vorticity
        self.flowrate = flowrate
        self.dt = dt
        self.absorption_start = absorption_start
        if self.absorption_start is None:
            self.absorption_start = velocity.domain.end[-1]
        # TODO: Correction is taking an absorption start (see absorption operator) only in X dir.
        super().__init__(
            input_fields=input_fields,
            output_fields=output_fields,
            input_params=input_params,
            **kwds,
        )

    @debug
    def get_field_requirements(self):
        requirements = super().get_field_requirements()
        for is_input, (field, td, req) in requirements.iter_requirements():
            req.axes = ((0, 1, 2),)
        return requirements

    @debug
    def discretize(self):
        if self.discretized:
            return
        super().discretize()
        self.dvelocity = self.input_discrete_tensor_fields[self.velocity]
        self.dvorticity = self.input_discrete_tensor_fields[self.vorticity]
        vtopo = self.dvelocity[0].topology
        wtopo = self.dvorticity[0].topology
        vmesh = vtopo.mesh
        wmesh = wtopo.mesh

        # Compute volume and surface integration coefficients
        spaceStep = vmesh.space_step
        lengths = vtopo.domain.length
        self._inv_ds = 1.0 / np.prod(lengths[:-1])
        self._inv_dvol = 1.0 / (
            lengths[0] * lengths[1] * (self.absorption_start - vtopo.domain.origin[-1])
        )
        self.coeff_mean = np.prod(spaceStep) / np.prod(lengths)

        # Compute space coordinates from domain origin
        x0 = vmesh.global_origin[-1]
        self.x_coord = vmesh.local_mesh_coords[0] - x0

        # Compute slices for volume and surface integration
        gstart = vmesh.local_origin.copy()
        gstart[-1] = vmesh.global_origin[-1]
        if vmesh.is_inside_local_domain(gstart):
            ind4integ_v = [_ for _ in vmesh.local_compute_slices]
            ind4integ_v[-1] = vmesh.point_local_indices(gstart)[-1]
            self._ind4integ_v = tuple(ind4integ_v)
        else:
            self._ind4integ_v = None
        self._ds_in = np.prod(spaceStep[0:2])
        ind4integ_w = [_ for _ in wmesh.local_compute_slices]
        box_end = wmesh.local_origin.copy()
        box_end[-1] = self.absorption_start
        ind4integ_w[-1] = slice(
            ind4integ_w[-1].start,
            wmesh.point_local_indices(box_end)[-1],
            ind4integ_w[-1].step,
        )
        self._ind4integ_w = tuple(ind4integ_w)
        self._ds_box = np.prod(spaceStep)

        nbf = self.velocity.nb_components + self.vorticity.nb_components
        self.rates = np.zeros((nbf,))
        self.ghost_exchanger = self.dvelocity.build_ghost_exchanger()

    @classmethod
    def supported_dimensions(cls):
        return (3,)

    @classmethod
    def supports_mpi(cls):
        return True

    @op_apply
    def apply(self, **kwds):
        super().apply(**kwds)
        dv, dw = self.dvelocity, self.dvorticity
        localrates = np.zeros_like(self.rates)

        # Integrate V over input box face (if on proc)
        dsurf = self._ds_in
        if self._ind4integ_v is not None:
            for i in range(dv.dim):
                localrates[i] = np.sum(dv.data[i][self._ind4integ_v]) * dsurf

        # Integrate w over box
        dvol = self._ds_box
        for i in range(dv.dim):
            localrates[dv.dim + i] = np.sum(dw.data[i][self._ind4integ_w]) * dvol

        # MPI reduction for localrates
        comm = self.mpi_params.comm
        comm.Allreduce(localrates, self.rates)

        self.rates[: dv.dim] *= self._inv_ds
        self.rates[dv.dim :] *= self._inv_dvol

        flowrate_req = self.flowrate() * self._inv_ds
        velo_shift = np.zeros((dv.dim,))
        velo_shift = flowrate_req - self.rates[dv.dim - 1 :: -1]

        # Shift velocity Vx
        dv.data[0][...] += velo_shift[2]

        # Update Vy and Vz with mean vorticity
        dv.data[1][...] += velo_shift[1] + self.rates[dv.dim + 2] * self.x_coord
        dv.data[2][...] += velo_shift[0] + self.rates[dv.dim + 1] * self.x_coord
        if self.ghost_exchanger is not None:
            self.ghost_exchanger()
