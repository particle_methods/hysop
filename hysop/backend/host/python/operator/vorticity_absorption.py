# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
import sympy as sm
from sympy.utilities.lambdify import lambdify

from hysop.backend.host.host_operator import HostOperator
from hysop.tools.htypes import check_instance
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import Field
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.parameters.tensor_parameter import TensorParameter
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.core.graph.graph import op_apply
from hysop.core.memory.memory_request import MemoryRequest


class PythonVorticityAbsorption(HostOperator):
    """
    Python implementation of the vorticity absorption
    at domain outlet.
    """

    @debug
    def __new__(
        cls,
        velocity,
        vorticity,
        dt,
        flowrate,
        start_coord,
        variables,
        custom_filter=None,
        implementation=None,
        **kwds,
    ):
        return super().__new__(
            cls, input_fields=None, output_fields=None, input_params=None, **kwds
        )

    @debug
    def __init__(
        self,
        velocity,
        vorticity,
        dt,
        flowrate,
        start_coord,
        variables,
        custom_filter=None,
        implementation=None,
        **kwds,
    ):
        """
        Parameters
        ----------
        velocity: field
            input velocity
        vorticity: field
            input vorticity
        flowrate : ScalarParameter
            penalization factor applied to all geometries. Flowrate is given in XYZ components.
        start_coord : coordinate of the absorption filter start (X coord)
        custom_filter : sympy expression
            The formula to be applied onto the field.
        variables: dict
            dictionary of fields as keys and topologies as values.
        dt: ScalarParameter
            Timestep parameter that will be used for time integration.
        implementation: Implementation, optional, defaults to None
            target implementation, should be contained in
            available_implementations().
            If None, implementation will be set to default_implementation().
        kwds:
            Keywords arguments that will be passed towards implementation
            poisson operator __init__.
        """
        check_instance(velocity, Field)
        check_instance(vorticity, Field)
        check_instance(flowrate, TensorParameter)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(dt, ScalarParameter)
        check_instance(start_coord, float)
        check_instance(custom_filter, sm.Basic, allow_none=True)

        input_fields = {velocity: variables[velocity], vorticity: variables[vorticity]}
        output_fields = {vorticity: variables[vorticity]}
        input_params = {flowrate}

        self.velocity = velocity
        self.vorticity = vorticity
        self.flowrate = flowrate
        self.dt = dt
        self.start_coord = start_coord
        self.custom_filter = custom_filter
        super().__init__(
            input_fields=input_fields,
            output_fields=output_fields,
            input_params=input_params,
            **kwds,
        )

    @debug
    def get_field_requirements(self):
        requirements = super().get_field_requirements()
        dim = self.domain.dim
        for is_input, (field, td, req) in requirements.iter_requirements():
            req.axes = ((0, 1, 2),)
            req.min_ghosts = (0,) * dim
            req.max_ghosts = (0,) * dim
        return requirements

    @debug
    def discretize(self):
        if self.discretized:
            return
        super().discretize()
        self.dvelocity = self.input_discrete_tensor_fields[self.velocity]
        self.dvorticity = self.input_discrete_tensor_fields[self.vorticity]

        def get_mesh_attr(attr, *args):
            if len(args) == 0:
                v = [eval("_.mesh." + attr) for _ in self.dvelocity]
            else:
                v = [eval("_.mesh." + attr)(*args) for _ in self.dvelocity]
            for vv in v[1:]:
                if isinstance(v[0], np.ndarray):
                    assert (v[0] == vv).all(), repr(v)
                else:
                    assert v[0] == vv, repr(v)
            return v[0]

        domain = self.dvelocity.domain
        lengths = domain.length
        self._inv_ds = 1.0 / np.prod(lengths[:-1])

        # Compute slice and x_coords for the absorption region,
        # for given start_coord to domain end.
        # find local slice for intersection between
        # [start_coord; domain end.] and [local_origin; local_end]
        mesh_local_origin = get_mesh_attr("local_origin")
        mesh_local_end = get_mesh_attr("local_end")
        mesh_global_end = get_mesh_attr("global_end")
        lorigin, lend = mesh_local_origin[-1], mesh_local_end[-1]
        coord_start, coord_end = mesh_local_origin.copy(), mesh_local_origin.copy()
        if self.start_coord <= lorigin:
            coord_start[-1] = lorigin
        elif self.start_coord <= lend:
            coord_start[-1] = self.start_coord
        else:
            coord_start = None
        if mesh_global_end[-1] >= lend:
            coord_end[-1] = lend
        elif mesh_global_end[-1] >= lorigin:
            coord_end[-1] = mesh_global_end[-1]
        else:
            coord_end = None
        mesh_local_mesh_coords0 = get_mesh_attr("local_mesh_coords[0]")
        mesh_local_compute_slices = get_mesh_attr("local_compute_slices")
        coord_start_local_indices = get_mesh_attr("point_local_indices", coord_start)
        coord_end_local_indices = get_mesh_attr("point_local_indices", coord_end)
        if coord_start is not None and coord_end is not None:
            ind = [_ for _ in mesh_local_compute_slices]
            ind[-1] = slice(
                coord_start_local_indices[-1], coord_end_local_indices[-1] + 1
            )
            ind = tuple(ind)
            coords = mesh_local_mesh_coords0[:, :, ind[-1]]
        else:
            ind, coords = None, None
        self._ind, self.x_coord = ind, coords

        self._filter, self._filter_diff = None, None
        self._apply = self._false_apply
        if ind is not None:
            x = sm.Symbol("x")
            if self.custom_filter is None:
                # Default filter
                xl = mesh_local_compute_slices[-1].stop - ind[-1].start
                eps = 10.0
                xb, xe = coords[0, 0, 0], coords[0, 0, xl - 1]
                xc = xb + (xe - xb) / 2.0
                filter_f = (sm.tanh(eps * (x - xc)) - sm.tanh(eps * (xe - xc))) / (
                    sm.tanh(eps * (xb - xc)) - sm.tanh(eps * (xe - xc))
                )
            else:
                filter_f = self.custom_filter
            self._filter = lambdify(x, filter_f, "numpy")(coords)
            self._filter_diff = lambdify(x, filter_f.diff(x), "numpy")(coords)
            self._apply = self._true_apply

    @debug
    def get_work_properties(self):
        requests = super().get_work_properties()
        buffers = MemoryRequest.empty_like(
            a=self.dvelocity.data[0], nb_components=1, backend=self.dvelocity.backend
        )
        requests.push_mem_request("tmp", buffers)
        return requests

    def setup(self, work):
        super().setup(work=work)
        tmp = work.get_buffer(self, "tmp", handle=True)
        self.tmp = tmp[0]

    @classmethod
    def supported_dimensions(cls):
        return (3,)

    @classmethod
    def supports_mpi(cls):
        return True

    @op_apply
    def apply(self, **kwds):
        super().apply(**kwds)
        self._apply()

    def _false_apply(self):
        pass

    def _true_apply(self):
        dv, dw, tmp = self.dvelocity.data, self.dvorticity.data, self.tmp

        # 1 - filter * periodic vorticity:
        for w in dw:
            w[self._ind] *= self._filter

        # 2 - nabla filter X periodic velocity + nabla(1-filter) X uinf
        flowrate_req = self.flowrate() * self._inv_ds
        tmp[self._ind] = flowrate_req[2] - dv[2][self._ind]
        tmp[self._ind] *= self._filter_diff
        self.dvorticity.data[1][self._ind] = tmp[self._ind]
        tmp[self._ind] = dv[1][self._ind] - flowrate_req[1]
        tmp[self._ind] *= self._filter_diff
        self.dvorticity.data[2][self._ind] = tmp[self._ind]
