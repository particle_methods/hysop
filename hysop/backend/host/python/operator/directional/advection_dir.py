# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.numpywrappers import npw
from hysop.tools.decorators import debug
from hysop.tools.htypes import check_instance, to_tuple
from hysop.core.graph.graph import op_apply
from hysop.core.memory.memory_request import MemoryRequest
from hysop.fields.discrete_field import DiscreteScalarFieldView

from hysop.constants import BoundaryCondition
from hysop.backend.host.host_operator import ComputeGranularity
from hysop.backend.host.host_directional_operator import HostDirectionalOperator
from hysop.operator.base.advection_dir import DirectionalAdvectionBase
from hysop.methods import Interpolation, PolynomialInterpolation
from hysop.numerics.odesolvers.runge_kutta import (
    ExplicitRungeKutta,
    Euler,
    RK2,
    RK3,
    RK4,
)

DEBUG = False


class PythonDirectionalAdvection(DirectionalAdvectionBase, HostDirectionalOperator):
    counter = 0

    @debug
    def __new__(cls, **kwds):
        return super().__new__(cls, **kwds)

    @debug
    def __init__(self, **kwds):
        super().__init__(**kwds)
        self.is_periodic = False

    def get_work_properties(self):
        requests = super().get_work_properties()

        V = self.dvelocity
        Vr = self.relative_velocity
        check_instance(V, DiscreteScalarFieldView)
        check_instance(Vr, float)

        cr = self.compute_granularity
        shape = tuple(self.dvelocity.compute_resolution[cr:])

        nbuffers = {Euler: 0, RK2: 2, RK3: 3, RK4: 4}
        time_integrator = self.time_integrator
        nb_rcomponents = max(nbuffers[time_integrator], 2)
        nb_icomponents = 1 if (time_integrator is Euler) else 2

        request = MemoryRequest.empty_like(
            a=V, shape=shape, nb_components=nb_rcomponents
        )
        requests.push_mem_request("rtmp", request)

        request = MemoryRequest.empty_like(
            a=V, shape=shape, nb_components=nb_icomponents, dtype=npw.int32
        )
        requests.push_mem_request("itmp", request)

        request = MemoryRequest.empty_like(
            a=V, shape=shape[:-1] + (1,), nb_components=1, dtype=npw.int32
        )
        requests.push_mem_request("ixtmp", request)

        nscalars = sum(field.nb_components for field in self.advected_fields_in)
        if self.is_inplace:
            rg = self.remesh_ghosts
            sshape = shape[:-1] + (shape[-1] + 2 * rg,)
            request = MemoryRequest.empty_like(
                a=V, shape=sshape, nb_components=nscalars
            )
            requests.push_mem_request("stmp", request)
        self.nscalars = nscalars
        self.Vr = Vr

        return requests

    def setup(self, work):
        super().setup(work=work)
        self.dpos = self.dposition.sbuffer
        self.drtmp = work.get_buffer(self, "rtmp", handle=True)
        self.ditmp = work.get_buffer(self, "itmp", handle=True)
        self.dixtmp = work.get_buffer(self, "ixtmp", handle=True)
        if self.is_inplace:
            self.dstmp = work.get_buffer(self, "stmp", handle=True)

        if self.is_bilevel:
            msg = "Python bilevel advection has not been implemented yet."
            raise NotImplementedError(msg)

        self._prepare_apply()

    def _prepare_apply(self):
        cr = self.compute_granularity
        sinputs = self.advected_fields_in
        soutputs = self.advected_fields_out
        dsinputs = self.dadvected_fields_in
        dsoutputs = self.dadvected_fields_out

        velo_mesh = self.dvelocity.mesh
        velo_mesh_iterator = velo_mesh.build_compute_mesh_iterator(cr)
        X0 = velo_mesh.local_compute_coords[-1]
        dx = velo_mesh.space_step[-1]
        inv_dx = 1.0 / dx
        velo_compute_view = velo_mesh.local_compute_slices
        self._velocity_mesh_attributes = (
            velo_mesh_iterator,
            dx,
            inv_dx,
            velo_compute_view,
            X0,
        )

        dsinputs0 = next(iter(dsinputs.values()))
        scalar_mesh = dsinputs0.mesh
        scalar_mesh_iterator = scalar_mesh.build_compute_mesh_iterator(cr)
        N0 = scalar_mesh.global_start[-1]
        sdx = scalar_mesh.space_step[-1]
        inv_sdx = 1.0 / sdx
        scalar_compute_view = scalar_mesh.local_compute_slices
        self._scalar_mesh_attributes = (
            scalar_mesh_iterator,
            sdx,
            inv_sdx,
            scalar_compute_view,
            N0,
        )

        self._last_scalar_axe_mesh_indices = tuple(
            npw.ndindex((dsinputs0.compute_resolution[-1],))
        )

        in_compute_slices, out_compute_slices = {}, {}
        in_ghosts, out_ghosts = {}, {}
        in_shapes, out_shapes = {}, {}

        for ifield, ofield in zip(sinputs, soutputs):
            Sin = dsinputs[ifield]
            Sout = dsoutputs[ofield]
            in_compute_slices[Sin] = Sin.compute_slices
            in_ghosts[Sin] = Sin.ghosts
            in_shapes[Sin] = Sin.resolution
            out_compute_slices[Sout] = Sout.compute_slices
            out_ghosts[Sout] = Sout.ghosts
            out_shapes[Sout] = Sout.resolution
        self._inout_compute_slices = (in_compute_slices, out_compute_slices)
        self._inout_ghosts = (in_ghosts, out_ghosts)
        self._inout_shapes = (in_shapes, out_shapes)

    @op_apply
    def apply(self, simulation=None, debug_dumper=None, **kwds):
        super().apply(**kwds)

        dsoutputs = self.dadvected_fields_out
        dt = self.dt() * self.dt_coeff

        if DEBUG:
            import inspect

            if debug_dumper is not None:

                def dump(dfield, tag):
                    it = simulation.current_iteration
                    t = simulation.t()
                    _file, _line = inspect.stack()[1][1:3]
                    debug_dumper(
                        it,
                        t,
                        tag,
                        tuple(
                            df.sdata.get().handle[df.compute_slices]
                            for df in dfield.dfields
                        ),
                        description=None,
                    )

            else:

                def dump(*args, **kwds):
                    pass

            Sin = next(iter(self.dadvected_fields_in.values()))
            Sout = next(iter(self.dadvected_fields_out.values()))
            P = self.dposition
            print(f"DT= {dt}")
            self._compute_advection(dt)
            print("P")
            print(P.collect_data())
            dump(P, "P")
            print("S (before remesh)")
            print(Sin.collect_data())
            dump(Sin, "Sin before remesh")
            self._compute_remesh()
            print("S (before accumulation)")
            print(Sout[0].sbuffer[Sout[0].local_slices(ghosts=(0, self.remesh_ghosts))])
            dump(Sin, "Sout (after remesh)")
            for sout in dsoutputs.values():
                print(f"Accumulate {sout.short_description()}")
                ghosts = tuple(sout.ghosts[:-1]) + (self.remesh_ghosts,)
                sout.accumulate_ghosts(directions=sout.dim - 1, ghosts=ghosts)
            print("S (after accumulation, before ghost exchange)")
            print(Sout.collect_data())
            dump(Sin, "Sout (after accumulation)")
            for sout in dsoutputs.values():
                print(f"Exchange {sout.short_description()}")
                sout.exchange_ghosts()
            print("S (after ghost exchange)")
            print(Sout.collect_data())
            dump(Sin, "Sout (after exchange)")
        else:
            self._compute_advection(dt)
            self._compute_remesh()
            for sout in dsoutputs.values():
                ghosts = tuple(sout.ghosts[:-1]) + (self.remesh_ghosts,)
                sout.accumulate_ghosts(directions=sout.dim - 1, ghosts=ghosts)
            for sout in dsoutputs.values():
                sout.exchange_ghosts()

        self.counter += 1

    def _interp_velocity(
        self, Vin, Vout, dX, I, Ig, lidx, ridx, inv_dx, is_periodic, Vr
    ):
        """
        Vin:  read only velocity with ghosts only on the last axe
        Vout: interpolated velocity output
        dX:   offset, real input,       destroyed
        lidx: int32 empty input buffer, destroyed
        ridx: int32 empty input buffer, destroyed
        """
        N = dX.shape[-1]

        Iy = I[:-1]
        Ix = Ig[-1]

        dX[...] *= inv_dx
        lidx[...] = npw.floor(dX).astype(npw.int32)
        dX[...] -= lidx
        alpha = dX  # dX now contains alpha

        lidx[...] += Ix
        if is_periodic:
            lidx[...] += N
            lidx[...] %= N
        elif DEBUG:
            min_lidx, max_lidx = lidx.min(), lidx.max()
            M = Vin.shape[-1]
            assert min_lidx >= 0, f"index out of bounds: {min_lidx} < 0"
            assert max_lidx < M - 1, f"index out of bounds: {max_lidx} >= {M-1}"
        ridx[...] = lidx
        ridx[...] += 1
        if is_periodic:
            ridx[...] %= N

        # Vout = Vl + alpha*(Vr-Vl)
        Vout[...] = Vin[Iy + (ridx,)]
        Vout[...] -= Vin[Iy + (lidx,)]

        Vout[...] *= alpha
        Vout[...] += Vin[Iy + (lidx,)]

        # take into account constant relative velocity
        Vout[...] -= Vr

    def _compute_advection(self, dt):
        P = self.dpos
        Vd = self.dvelocity.sbuffer
        rtmp = self.drtmp
        itmp = self.ditmp

        cr = self.compute_granularity
        is_periodic = self.is_periodic
        rk_scheme = self.time_integrator

        (mesh_it, dx, inv_dx, view, X0) = self._velocity_mesh_attributes

        _Vd = Vd[view]
        Vd = Vd[view[:-1] + (slice(None),)]
        Vr = self.Vr  # relative velocity

        if DEBUG:
            # check if CFL condition is met
            cfl = self.velocity_cfl
            Vmin, Vmax = Vd.min(), Vd.max()
            Vinf = max(abs(Vmin), abs(Vmax))
            msg = "Vinf={}, dt={}, dx={} => Vinf*dt/dx={} but cfl={}."
            msg = msg.format(Vinf, dt, dx, Vinf * dt * inv_dx, cfl)
            assert Vinf * dt * inv_dx <= cfl, msg

        # fill position with first direction coordinates on the whole compute domain
        for idx, _, I, Ig in mesh_it.iter_compute_mesh():
            Pi = P[idx]
            Vi = Vd[idx]
            _Vi = _Vd[idx]  # Vi without ghosts
            if rk_scheme.name() == "Euler":
                Pi[...] = _Vi - Vr
                Pi[...] *= dt
                Pi[...] += X0
            elif rk_scheme.name() == "RK2":
                (lidx, ridx) = itmp
                (dX0, V1) = rtmp
                dX0[...] = _Vi - Vr
                dX0[...] *= 0.5 * dt
                self._interp_velocity(
                    Vi, V1, dX0, I, Ig, lidx, ridx, inv_dx, is_periodic, Vr
                )
                Pi[...] = V1
                Pi[...] *= dt
                Pi[...] += X0
            elif rk_scheme.name() == "RK4":
                (lidx, ridx) = itmp
                (dXk, V1, V2, V3) = rtmp
                dXk[...] = _Vi - Vr
                dXk[...] *= 0.5 * dt
                self._interp_velocity(
                    Vi, V1, dXk, I, Ig, lidx, ridx, inv_dx, is_periodic, Vr
                )
                dXk[...] = V1
                dXk[...] *= 0.5 * dt
                self._interp_velocity(
                    Vi, V2, dXk, I, Ig, lidx, ridx, inv_dx, is_periodic, Vr
                )
                dXk[...] = V2
                dXk[...] *= 1.0 * dt
                self._interp_velocity(
                    Vi, V3, dXk, I, Ig, lidx, ridx, inv_dx, is_periodic, Vr
                )

                V0 = dXk
                V0[...] = _Vi - Vr

                V0[...] *= 1.0 / 6.0
                V1[...] *= 2.0 / 6.0
                V2[...] *= 2.0 / 6.0
                V3[...] *= 1.0 / 6.0

                Pi[...] = V0
                Pi[...] += V1
                Pi[...] += V2
                Pi[...] += V3
                Pi[...] *= dt
                Pi[...] += X0
            else:
                msg = f"Unknown Runge-Kutta scheme {rk_scheme}."
                raise ValueError(msg)
        if DEBUG:
            # check min and max positions
            Pmin, Pmax = P.min(), P.max()
            finfo = npw.finfo(P.dtype)
            eps, epsneg = finfo.eps, finfo.epsneg

            msg = "Pmin={}, X0[0]={}, Vmin={}, dt={}, X0[0]+Vmin*dt={}   Pmin < X0[0] + Vmin*dt"
            msg = msg.format(Pmin, X0[0], Vmin, dt, X0[0] + Vmin * dt)
            assert Pmin >= X0[0] + Vmin * dt - 10 * epsneg, msg

            msg = "Pmax={}, X0[-1]={}, Vmax={}, dt={}, X0[-1]+Vmax*dt={}   Pmax > X0[-1] + Vmax*dt"
            msg = msg.format(Pmax, X0[-1], Vmax, dt, X0[-1] + Vmax * dt)
            assert Pmax <= X0[-1] + Vmax * dt + 10 * eps, msg

    def _compute_remesh(self):
        pos = self.dpos
        sinputs = self.advected_fields_in
        soutputs = self.advected_fields_out
        dsinputs = self.dadvected_fields_in
        dsoutputs = self.dadvected_fields_out
        is_inplace = self.is_inplace

        rtmp = self.drtmp
        itmp = self.ditmp
        if self.is_inplace:
            stmp = self.dstmp
        ixtmp = self.dixtmp

        cr = self.compute_granularity
        (_, _, _, _, X0) = self._velocity_mesh_attributes
        (mesh_it, dx, inv_dx, compute_view, N0) = self._scalar_mesh_attributes

        if DEBUG:
            print("GLOBAL START")
            print(f"X0: {X0[0]}")
            print(f"N0: {N0}")
            print("***********")

        scalar_advection_ghosts = self.scalar_advection_ghosts
        remesh_ghosts = self.remesh_ghosts
        remesh_kernel = self.remesh_kernel
        P = 1 + remesh_kernel.n // 2

        is_periodic = self.is_periodic

        R0, R1 = rtmp[:2]
        I0 = itmp[0]
        Ix = ixtmp[0]
        if is_inplace:
            S = stmp

        is_periodic = False
        N = R0.shape[-1]

        (in_compute_slices, out_compute_slices) = self._inout_compute_slices
        (in_ghosts, out_ghosts) = self._inout_ghosts
        (in_shapes, out_shapes) = self._inout_shapes

        input_buffer_views = {}
        output_buffer_views = {}

        last_scalar_axe_mesh_indices = self._last_scalar_axe_mesh_indices

        pos[...] -= X0[0]

        for idx, _, I, _ in mesh_it.iter_compute_mesh():
            Pi = pos[idx]
            R0[...] = Pi
            R0[...] *= inv_dx
            I0[...] = npw.floor(R0).astype(I0.dtype)
            R0[...] -= I0
            R0[...] *= -1  # we need -alpha for the left point

            if DEBUG:
                Imin, Imax = I0.min(), I0.max()
                amin, amax = R0.min(), R0.max()
                assert Imin >= -scalar_advection_ghosts, "{} >= -{}".format(
                    Imin, scalar_advection_ghosts
                )
                assert Imax < N + scalar_advection_ghosts, "{} < {}+{}".format(
                    Imax, N, scalar_advection_ghosts
                )
                assert amin >= -1.0, amin
                assert amax <= 0.0, amax

            R0[...] -= P
            I0[...] -= P

            # prebuild data views
            for ifield, ofield in zip(sinputs, soutputs):
                Sin = dsinputs[ifield]
                Sout = dsoutputs[ofield]

                sin_view = tuple(idx[i] + in_ghosts[Sin][i] for i in range(cr))
                sin_view += in_compute_slices[Sin][cr:]

                dG = out_ghosts[Sout][-1] - remesh_ghosts
                sout_view = tuple(idx[i] + out_ghosts[Sout][i] for i in range(cr))
                sout_view += out_compute_slices[Sout][cr:-1]
                sout_view += (slice(dG, out_shapes[Sout][-1] - dG),)

                in_views = tuple(buf[sin_view] for buf in Sin.buffers)
                out_views = tuple(buf[sout_view] for buf in Sout.buffers)
                input_buffer_views[ifield] = in_views
                output_buffer_views[ofield] = out_views

            for q in range(-P + 1, P + 1):
                I0[...] += 1
                R0[...] += 1
                R1[...] = remesh_kernel.gamma(R0)

                sid = 0
                for ifield, ofield in zip(sinputs, soutputs):
                    for k in range(ifield.nb_components):
                        sin = input_buffer_views[ifield][k]
                        sout = output_buffer_views[ofield][k]
                        Si = S[sid] if is_inplace else sout
                        if q == -P + 1:
                            Si[...] = 0.0
                        for in_idx in last_scalar_axe_mesh_indices:
                            Ix[...] = I0[..., in_idx]
                            Ix += remesh_ghosts
                            if is_periodic:
                                Ix += N
                                Ix %= N
                            if DEBUG:
                                assert npw.all(Ix >= 0), f"ix={Ix}, ix < 0"
                            Si[I[:-1] + (Ix,)] += R1[..., in_idx] * sin[..., in_idx]

                        if (q == P) and is_inplace:
                            sout[...] = Si
                        sid += 1

    @debug
    def handle_method(self, method):
        super().handle_method(method)
        cr = method.pop(ComputeGranularity)
        assert 0 <= cr <= self.velocity.dim - 1
        msg = "Interpolation {}.{} is not supported for operator {}.".format(
            self.interp.__class__.__name__, self.interp, self.__class__.__name__
        )
        assert self.interp in (
            Interpolation.LINEAR,
            PolynomialInterpolation.LINEAR,
        ), msg
        self.compute_granularity = cr

    @classmethod
    def default_method(cls):
        dm0 = super().default_method()
        dm = {ComputeGranularity: 0}
        dm.update(dm0)
        return dm

    @classmethod
    def available_methods(cls):
        dm0 = super().available_methods()
        dm = {ComputeGranularity: (0,) + cls.supported_dimensions()}
        dm.update(dm0)
        return dm

    @classmethod
    def supports_mpi(cls):
        return True
