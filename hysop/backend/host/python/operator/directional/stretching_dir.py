# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
import sympy as sm
from hysop.tools.decorators import debug
from hysop.backend.host.host_directional_operator import HostDirectionalOperator
from hysop.constants import (
    DirectionLabels,
    Implementation,
    StretchingFormulation,
    HYSOP_REAL,
)
from hysop.tools.htypes import check_instance, to_tuple, to_list, first_not_None
from hysop.fields.continuous_field import Field
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.operator.base.stretching_dir import DirectionalStretchingBase
from hysop.core.graph.graph import op_apply
from hysop.core.memory.memory_request import MemoryRequest
from hysop.numerics.odesolvers.runge_kutta import (
    ExplicitRungeKutta,
    Euler,
    RK2,
    RK3,
    RK4,
)


class PythonDirectionalStretching(DirectionalStretchingBase, HostDirectionalOperator):

    @debug
    def __new__(cls, formulation, **kwds):
        return super().__new__(cls, formulation=formulation, **kwds)

    @debug
    def __init__(self, formulation, **kwds):
        super().__init__(formulation=formulation, **kwds)
        if formulation is not StretchingFormulation.CONSERVATIVE:
            msg = "Formulation {} has not been implemented yet."
            msg = msg.format(formulation)
            raise NotImplementedError(msg)

    def discretize(self, **kwds):
        super().discretize(**kwds)
        dv, dw = self.dvelocity, self.dvorticity

        stencil = self.stencil
        stencil.replace_symbols({self.stencil.dx: dw.space_step[-1]})

        G = max(stencil.L, stencil.R)[0]
        S = self.time_integrator.stages

        Wnames = ("W0", "W1", "W2")

        assert dv.ghosts[-1] >= S * G
        view = dv.local_slices(ghosts=(0, 0, S * G))
        V = tuple(Vi[view] for Vi in dv.buffers)
        V = dict(zip(Wnames, V))

        assert dw.ghosts[-1] >= S * G
        view = dw.local_slices(ghosts=(0, 0, S * G))
        W = tuple(Wi[view] for Wi in dw.buffers)
        W = dict(zip(Wnames, W))

        self.Wnames, self.W, self.V = Wnames, W, V
        self.ghost_exchanger = dw.build_ghost_exchanger()

    def get_work_properties(self):
        requests = super().get_work_properties()

        nb_components = 2 + self.time_integrator.stages
        nb_components *= 3
        buffers = MemoryRequest.empty_like(
            a=self.W["W0"], nb_components=nb_components, backend=self.dvorticity.backend
        )
        requests.push_mem_request("Wtmp", buffers)
        return requests

    def setup(self, work):
        super().setup(work=work)
        Wtmp = work.get_buffer(self, "Wtmp", handle=True)

        tmp = {}
        buffers = {}
        N = 2 + self.time_integrator.stages
        for i, wname in enumerate(self.Wnames):
            tmp[wname] = Wtmp[i * N]
            buffers[wname] = Wtmp[i * N + 1 : (i + 1) * N]

        self.buffers = buffers
        self.tmp = tmp

        # Configure C coeff access (vector or scalar)
        try:
            self._C = np.asarray([_ for _ in self.C], dtype=HYSOP_REAL)
        except TypeError:
            self._C = (HYSOP_REAL(self.C),) * 3

    @op_apply
    def apply(self, **kwds):
        super().apply(**kwds)
        dt = self.dt() * self.dt_coeff
        rk_scheme = self.time_integrator

        Wnames, V, W, buffers, tmp = self.Wnames, self.V, self.W, self.buffers, self.tmp
        wdir = Wnames[self.splitting_direction]

        def rhs(out, X, **kwds):
            for i in range(3):
                wn = Wnames[i]
                Vi = V[wn]
                Wd = X[wdir]
                self.stencil(a=Vi * Wd, out=out[wn], axis=-1)
                out[wn][...] *= self._C[i]
            return out

        Wout = rk_scheme(Xin=W, RHS=rhs, Xout=tmp, buffers=buffers, dt=dt)
        for wn in Wnames:
            W[wn][...] = Wout[wn]
        self.ghost_exchanger.exchange_ghosts()

    @classmethod
    def supports_mpi(cls):
        return True
