# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import functools

from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.decorators import debug
from hysop.tools.numpywrappers import npw
from hysop.core.graph.graph import op_apply
from hysop.backend.host.host_operator import HostOperator, OpenClMappable
from hysop.operator.base.poisson_curl import SpectralPoissonCurlOperatorBase

from hysop.backend.host.python.operator.diffusion import PythonDiffusion
from hysop.backend.host.python.operator.poisson import PythonPoisson
from hysop.backend.host.python.operator.solenoidal_projection import (
    PythonSolenoidalProjection,
)
from hysop import __DEFAULT_NUMBA_TARGET__


class PythonPoissonCurl(SpectralPoissonCurlOperatorBase, OpenClMappable, HostOperator):
    """
    Solves the poisson rotational equation using numpy fftw.
    """

    @classmethod
    def build_filter_curl_2d__0_m(cls, FIN, K, FOUT, target=__DEFAULT_NUMBA_TARGET__):
        args = (FIN, K, FOUT)
        try:
            import numba as nb
            from hysop.tools.numba_utils import make_numba_signature, prange

            signature, _ = make_numba_signature(*args)
            layout = "(n,m),(m)->(n,m)"

            @nb.guvectorize(
                [signature], layout, target=target, nopython=True, cache=True
            )
            def filter_curl_2d__0_m(Fin, K1, Fout):
                for i in prange(0, Fin.shape[0]):
                    for j in range(0, Fin.shape[1]):
                        Fout[i, j] = -K1[j] * Fin[i, j]

            return functools.partial(filter_curl_2d__0_m, *args)
        except ModuleNotFoundError:

            def filter_curl_2d__0_m(Fin, K1, Fout):
                for i in range(0, Fin.shape[0]):
                    for j in range(0, Fin.shape[1]):
                        Fout[i, j] = -K1[j] * Fin[i, j]

            return functools.partial(filter_curl_2d__0_m, *args)

    @classmethod
    def build_filter_curl_2d__1_n(cls, FIN, K, FOUT, target=__DEFAULT_NUMBA_TARGET__):
        args = (FIN, K, FOUT)
        try:
            import numba as nb
            from hysop.tools.numba_utils import make_numba_signature, prange

            signature, _ = make_numba_signature(*args)
            layout = "(n,m),(n)->(n,m)"

            @nb.guvectorize(
                [signature], layout, target=target, nopython=True, cache=True
            )
            def filter_curl_2d__1_n(Fin, K0, Fout):
                for i in prange(0, Fin.shape[0]):
                    for j in range(0, Fin.shape[1]):
                        Fout[i, j] = +K0[i] * Fin[i, j]

        except ModuleNotFoundError:

            def filter_curl_2d__1_n(Fin, K0, Fout):
                for i in range(0, Fin.shape[0]):
                    for j in range(0, Fin.shape[1]):
                        Fout[i, j] = +K0[i] * Fin[i, j]

        return functools.partial(filter_curl_2d__1_n, *args)

    @classmethod
    def build_filter_curl_3d__0_n(cls, FIN, K, FOUT, target=__DEFAULT_NUMBA_TARGET__):
        args = (FIN, K, FOUT)
        try:
            import numba as nb
            from hysop.tools.numba_utils import make_numba_signature, prange

            signature, _ = make_numba_signature(*args)
            layout = "(n,m,p),(n)->(n,m,p)"

            @nb.guvectorize(
                [signature], layout, target=target, nopython=True, cache=True
            )
            def filter_curl_3d__0_n(Fin, K, Fout):
                for i in prange(0, Fin.shape[0]):
                    for j in prange(0, Fin.shape[1]):
                        for k in range(0, Fin.shape[2]):
                            Fout[i, j, k] = -K[i] * Fin[i, j, k]

        except ModuleNotFoundError:

            def filter_curl_3d__0_n(Fin, K, Fout):
                for i in range(0, Fin.shape[0]):
                    for j in range(0, Fin.shape[1]):
                        for k in range(0, Fin.shape[2]):
                            Fout[i, j, k] = -K[i] * Fin[i, j, k]

        return functools.partial(filter_curl_3d__0_n, *args)

    @classmethod
    def build_filter_curl_3d__0_m(cls, FIN, K, FOUT, target=__DEFAULT_NUMBA_TARGET__):
        args = (FIN, K, FOUT)
        try:
            import numba as nb
            from hysop.tools.numba_utils import make_numba_signature, prange

            signature, _ = make_numba_signature(*args)
            layout = "(n,m,p),(m)->(n,m,p)"

            @nb.guvectorize(
                [signature], layout, target=target, nopython=True, cache=True
            )
            def filter_curl_3d__0_m(Fin, K, Fout):
                for i in prange(0, Fin.shape[0]):
                    for j in prange(0, Fin.shape[1]):
                        for k in range(0, Fin.shape[2]):
                            Fout[i, j, k] = -K[j] * Fin[i, j, k]

        except ModuleNotFoundError:

            def filter_curl_3d__0_m(Fin, K, Fout):
                for i in range(0, Fin.shape[0]):
                    for j in range(0, Fin.shape[1]):
                        for k in range(0, Fin.shape[2]):
                            Fout[i, j, k] = -K[j] * Fin[i, j, k]

        return functools.partial(filter_curl_3d__0_m, *args)

    @classmethod
    def build_filter_curl_3d__0_p(cls, FIN, K, FOUT, target=__DEFAULT_NUMBA_TARGET__):
        args = (FIN, K, FOUT)
        try:
            import numba as nb
            from hysop.tools.numba_utils import make_numba_signature, prange

            signature, _ = make_numba_signature(*args)
            layout = "(n,m,p),(p)->(n,m,p)"

            @nb.guvectorize(
                [signature], layout, target=target, nopython=True, cache=True
            )
            def filter_curl_3d__0_p(Fin, K, Fout):
                for i in prange(0, Fin.shape[0]):
                    for j in prange(0, Fin.shape[1]):
                        for k in range(0, Fin.shape[2]):
                            Fout[i, j, k] = -K[k] * Fin[i, j, k]

        except ModuleNotFoundError:

            def filter_curl_3d__0_p(Fin, K, Fout):
                for i in range(0, Fin.shape[0]):
                    for j in range(0, Fin.shape[1]):
                        for k in range(0, Fin.shape[2]):
                            Fout[i, j, k] = -K[k] * Fin[i, j, k]

        return functools.partial(filter_curl_3d__0_p, *args)

    @classmethod
    def build_filter_curl_3d__1_n(cls, FIN, K, FOUT, target=__DEFAULT_NUMBA_TARGET__):
        args = (FIN, K, FOUT)
        try:
            import numba as nb
            from hysop.tools.numba_utils import make_numba_signature, prange

            signature, _ = make_numba_signature(*args)
            layout = "(n,m,p),(n)->(n,m,p)"

            @nb.guvectorize(
                [signature], layout, target=target, nopython=True, cache=True
            )
            def filter_curl_3d__1_n(Fin, K, Fout):
                for i in prange(0, Fin.shape[0]):
                    for j in prange(0, Fin.shape[1]):
                        for k in range(0, Fin.shape[2]):
                            Fout[i, j, k] += K[i] * Fin[i, j, k]

        except ModuleNotFoundError:

            def filter_curl_3d__1_n(Fin, K, Fout):
                for i in range(0, Fin.shape[0]):
                    for j in range(0, Fin.shape[1]):
                        for k in range(0, Fin.shape[2]):
                            Fout[i, j, k] += K[i] * Fin[i, j, k]

        return functools.partial(filter_curl_3d__1_n, *args)

    @classmethod
    def build_filter_curl_3d__1_m(cls, FIN, K, FOUT, target=__DEFAULT_NUMBA_TARGET__):
        args = (FIN, K, FOUT)
        try:
            import numba as nb
            from hysop.tools.numba_utils import make_numba_signature, prange

            signature, _ = make_numba_signature(*args)
            layout = "(n,m,p),(m)->(n,m,p)"

            @nb.guvectorize(
                [signature], layout, target=target, nopython=True, cache=True
            )
            def filter_curl_3d__1_m(Fin, K, Fout):
                for i in prange(0, Fin.shape[0]):
                    for j in prange(0, Fin.shape[1]):
                        for k in range(0, Fin.shape[2]):
                            Fout[i, j, k] += K[j] * Fin[i, j, k]

        except ModuleNotFoundError:

            def filter_curl_3d__1_m(Fin, K, Fout):
                for i in range(0, Fin.shape[0]):
                    for j in range(0, Fin.shape[1]):
                        for k in range(0, Fin.shape[2]):
                            Fout[i, j, k] += K[j] * Fin[i, j, k]

        return functools.partial(filter_curl_3d__1_m, *args)

    @classmethod
    def build_filter_curl_3d__1_p(cls, FIN, K, FOUT, target=__DEFAULT_NUMBA_TARGET__):
        args = (FIN, K, FOUT)
        try:
            import numba as nb
            from hysop.tools.numba_utils import make_numba_signature, prange

            signature, _ = make_numba_signature(*args)
            layout = "(n,m,p),(p)->(n,m,p)"

            @nb.guvectorize(
                [signature], layout, target=target, nopython=True, cache=True
            )
            def filter_curl_3d__1_p(Fin, K, Fout):
                for i in prange(0, Fin.shape[0]):
                    for j in prange(0, Fin.shape[1]):
                        for k in range(0, Fin.shape[2]):
                            Fout[i, j, k] += K[k] * Fin[i, j, k]

        except ModuleNotFoundError:

            def filter_curl_3d__1_p(Fin, K, Fout):
                for i in range(0, Fin.shape[0]):
                    for j in range(0, Fin.shape[1]):
                        for k in range(0, Fin.shape[2]):
                            Fout[i, j, k] += K[k] * Fin[i, j, k]

        return functools.partial(filter_curl_3d__1_p, *args)

    def setup(self, work):
        super().setup(work=work)

        dim = self.dim
        WIN, UIN, UOUT = self.WIN, self.UIN, self.UOUT
        K, KK = self.K, self.KK
        UK = self.UK
        assert len(WIN) == len(KK), (len(WIN), len(KK))
        assert len(UIN) == len(UOUT) == len(UK), (len(UIN), len(UOUT), len(UK))

        # diffusion filters
        if self.should_diffuse:
            diffusion_filters = ()
            for Wi, KKi in zip(WIN, KK):
                F = PythonDiffusion.build_diffusion_filter(dim, *((Wi,) + KKi))
                diffusion_filters += (F,)
            self.diffusion_filters = diffusion_filters
        else:
            self.diffusion_filters = None

        # projection filter
        if self.should_project:
            self.projection_filter = PythonSolenoidalProjection.build_projection_filter(
                WIN, WIN, sum(K, ()), sum(KK, ())
            )
        else:
            self.projection_filter = None

        # poisson filter
        poisson_filters = ()
        for Wi, KKi in zip(WIN, KK):
            F = PythonPoisson.build_poisson_filter(dim, *((Wi,) + KKi + (Wi,)))
            poisson_filters += (F,)
        self.poisson_filters = poisson_filters

        # curl filter
        if dim == 2:
            curl0 = self.build_filter_curl_2d__0_m(UIN[0], UK[0], UOUT[0])
            curl1 = self.build_filter_curl_2d__1_n(UIN[1], UK[1], UOUT[1])
            curl_filters = ((curl0,), (curl1,))
        elif dim == 3:
            curl_filters = ()
            filters = (
                (self.build_filter_curl_3d__0_n, self.build_filter_curl_3d__1_p),
                (self.build_filter_curl_3d__0_p, self.build_filter_curl_3d__1_m),
                (self.build_filter_curl_3d__0_m, self.build_filter_curl_3d__1_n),
            )
            for i, (build_filter_curl_3d__0, build_filter_curl_3d__1) in enumerate(
                filters
            ):
                j = 2 * i
                k = 2 * i + 1
                curl0 = build_filter_curl_3d__0(UIN[j], UK[j], UOUT[j])
                curl1 = build_filter_curl_3d__1(UIN[k], UK[k], UOUT[k])
                curl_filters += ((curl0, curl1),)
        else:
            msg = f"Unsupported dimension {dim}."
            raise ValueError(msg)
        self.curl_filters = curl_filters

        assert len(curl_filters) == len(self.U_backward_transforms)

    @op_apply
    def apply(self, simulation=None, **kwds):
        """Apply analytic formula."""
        super().apply(**kwds)

        diffuse = self.should_diffuse
        project = self.do_project(simu=simulation)

        with self.map_objects_to_host():
            for Ft in self.W_forward_transforms:
                Ft(simulation=simulation)
            if diffuse:
                nu_dt = self.nu() * self.dt()
                for Wi, filter_diffusion in zip(self.WIN, self.diffusion_filters):
                    filter_diffusion(nu_dt, Wi)
            if project:
                self.projection_filter()
            if diffuse or project:
                for Bt in self.W_backward_transforms:
                    Bt(simulation=simulation)
            for poisson_filter in self.poisson_filters:
                poisson_filter()
            for curl_filters, Bt in zip(self.curl_filters, self.U_backward_transforms):
                for curl_filter in curl_filters:
                    curl_filter()
                Bt(simulation=simulation)

        self.dU.exchange_ghosts()
        if diffuse or project:
            self.dW.exchange_ghosts()
        self.update_energy(simulation=simulation)
