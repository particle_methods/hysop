# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.constants import MemoryOrdering
from hysop.tools.decorators import debug, profile
from hysop.backend.host.host_operator import HostOperator
from hysop.operator.base.transpose_operator import TransposeOperatorBase
from hysop.core.graph.graph import op_apply

import numpy as np

try:
    import hptt

    HAS_HPTT = True
    # required version is: https://gitlab.com/keckj/hptt
except ImportError:
    HAS_HPTT = False
    import warnings
    from hysop.tools.warning import HysopPerformanceWarning

    msg = "Failed to import HPTT module, falling back to slow numpy transpose. Required version is available at https://gitlab.com/keckj/hptt."
    warnings.warn(msg, HysopPerformanceWarning)


class PythonTranspose(TransposeOperatorBase, HostOperator):
    """
    Inplace and out of place field transposition and permutations in general.
    Uses basic numpy transposition facility.
    """

    @debug
    def __new__(cls, **kwds):
        return super().__new__(cls, **kwds)

    @debug
    def __init__(self, **kwds):
        """Initialize a Transpose operator on the python backend."""
        super().__init__(**kwds)

    def discretize(self):
        super().discretize()
        assert self.din.dtype == self.dout.dtype
        dtype = self.din.dtype
        if HAS_HPTT and (
            dtype in (np.float32, np.float64, np.complex64, np.complex128)
        ):
            if self.is_inplace:
                self.exec_transpose = self.transpose_hptt_inplace
            else:
                self.exec_transpose = self.transpose_hptt_outofplace
        else:
            if self.is_inplace:
                self.exec_transpose = self.transpose_np_inplace
            else:
                self.exec_transpose = self.transpose_np_outofplace

    @debug
    def get_field_requirements(self):
        requirements = super().get_field_requirements()
        for is_input, reqs in requirements.iter_requirements():
            if reqs is None:
                continue
            (field, td, req) = reqs
            req.memory_order = MemoryOrdering.ANY
        return requirements

    @classmethod
    def supports_mpi(cls):
        return True

    @op_apply
    def apply(self, **kwds):
        """Transpose in or out of place."""
        super().apply(**kwds)
        self.exec_transpose(**kwds)

    def transpose_hptt_inplace(self, **kwds):
        axes = self.axes
        din, dout, dtmp = self.din, self.dout, self.dtmp.handle.view(np.ndarray)
        assert self.din.dfield is self.dout.dfield
        for i in range(din.nb_components):
            hptt.transpose(a=din.buffers[i], out=dtmp, axes=axes)
            dout.buffers[i][...] = dtmp

    def transpose_hptt_outofplace(self, **kwds):
        axes = self.axes
        din, dout = self.din, self.dout
        assert self.din.dfield is not self.dout.dfield
        for i in range(din.nb_components):
            hptt.transpose(a=din.buffers[i], out=dout.buffers[i], axes=axes)

    def transpose_np_inplace(self, **kwds):
        axes = self.axes
        din, dout, dtmp = self.din, self.dout, self.dtmp.handle.view(np.ndarray)
        assert self.din.dfield is self.dout.dfield
        for i in range(din.nb_components):
            dtmp[...] = np.transpose(din.buffers[i], axes=axes)
            dout.buffers[i][...] = dtmp

    def transpose_np_outofplace(self, **kwds):
        axes = self.axes
        din, dout = self.din, self.dout
        assert self.din.dfield is not self.dout.dfield
        for i in range(din.nb_components):
            dout.buffers[i][...] = np.transpose(din.buffers[i], axes=axes)
