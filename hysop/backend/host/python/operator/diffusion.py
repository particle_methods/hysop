# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import functools

from hysop.constants import Backend
from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.decorators import debug
from hysop.tools.numpywrappers import npw
from hysop.tools.numerics import is_complex, complex_to_float_dtype
from hysop.backend.host.host_operator import HostOperator, OpenClMappable
from hysop.core.graph.graph import op_apply
from hysop.fields.continuous_field import Field
from hysop.parameters.parameter import Parameter
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.operator.base.diffusion import DiffusionOperatorBase


class PythonDiffusion(DiffusionOperatorBase, OpenClMappable, HostOperator):
    """
    Solves the implicit diffusion equation using numpy fft.
    """

    @classmethod
    def build_diffusion_filter(cls, dim, *args, **kwds):
        assert len(args) == 1 + dim
        try:
            import numba as nb
            from hysop.tools.numba_utils import make_numba_signature, prange
            from hysop import __DEFAULT_NUMBA_TARGET__

            target = kwds.get("target", __DEFAULT_NUMBA_TARGET__)
            dtype = args[0].dtype
            if is_complex(dtype):
                dtype = complex_to_float_dtype(dtype)
            signature, _ = make_numba_signature(*(args + (dtype, args[0])))
            if dim == 1:

                @nb.guvectorize(
                    [signature],
                    "(n),(n),()->(n)",
                    target=target,
                    nopython=True,
                    cache=True,
                )
                def filter_diffusion_1d(Fin, K0, nu_dt, Fout):
                    for i in range(0, Fin.shape[0]):
                        Fout[i] /= 1 - nu_dt * K0[i]

                F = filter_diffusion_1d
            elif dim == 2:

                @nb.guvectorize(
                    [signature],
                    "(n,m),(n),(m),()->(n,m)",
                    target=target,
                    nopython=True,
                    cache=True,
                )
                def filter_diffusion_2d(Fin, K0, K1, nu_dt, Fout):
                    for i in prange(Fin.shape[0]):
                        for j in range(Fin.shape[1]):
                            Fout[i, j] /= 1 - nu_dt * (K0[i] + K1[j])

                F = filter_diffusion_2d
            elif dim == 3:

                @nb.guvectorize(
                    [signature],
                    "(n,m,p),(n),(m),(p),()->(n,m,p)",
                    target=target,
                    nopython=True,
                    cache=True,
                )
                def filter_diffusion_3d(Fin, K0, K1, K2, nu_dt, Fout):
                    for i in prange(Fin.shape[0]):
                        for j in prange(Fin.shape[1]):
                            for k in range(Fin.shape[2]):
                                Fout[i, j, k] /= 1 - nu_dt * (K0[i] + K1[j] + K2[k])

                F = filter_diffusion_3d
            elif dim == 4:

                @nb.guvectorize(
                    [signature],
                    "(n,m,p,q),(n),(m),(p),(q),()->(n,m,p,q)",
                    target=target,
                    nopython=True,
                    cache=True,
                )
                def filter_diffusion_4d(Fin, K0, K1, K2, K3, nu_dt, Fout):
                    for i in prange(Fin.shape[0]):
                        for j in prange(Fin.shape[1]):
                            for k in prange(Fin.shape[2]):
                                for l in range(Fin.shape[3]):
                                    Fout[i, j, k, l] /= 1 - nu_dt * (
                                        K0[i] + K1[j] + K2[k] + K3[l]
                                    )

            else:
                msg = f"{dim}D Diffusion filter has not been vectorized yet."
                raise NotImplementedError(msg)
            return functools.partial(F, *args)
        except ModuleNotFoundError:
            if dim == 1:

                def filter_diffusion_1d(Fin, K0, nu_dt, Fout):
                    for i in range(0, Fin.shape[0]):
                        Fout[i] /= 1 - nu_dt * K0[i]

                F = filter_diffusion_1d
            elif dim == 2:

                def filter_diffusion_2d(Fin, K0, K1, nu_dt, Fout):
                    for i in range(Fin.shape[0]):
                        for j in range(Fin.shape[1]):
                            Fout[i, j] /= 1 - nu_dt * (K0[i] + K1[j])

                F = filter_diffusion_2d
            elif dim == 3:

                def filter_diffusion_3d(Fin, K0, K1, K2, nu_dt, Fout):
                    for i in range(Fin.shape[0]):
                        for j in range(Fin.shape[1]):
                            for k in range(Fin.shape[2]):
                                Fout[i, j, k] /= 1 - nu_dt * (K0[i] + K1[j] + K2[k])

                F = filter_diffusion_3d
            elif dim == 4:

                def filter_diffusion_4d(Fin, K0, K1, K2, K3, nu_dt, Fout):
                    for i in range(Fin.shape[0]):
                        for j in range(Fin.shape[1]):
                            for k in range(Fin.shape[2]):
                                for l in range(Fin.shape[3]):
                                    Fout[i, j, k, l] /= 1 - nu_dt * (
                                        K0[i] + K1[j] + K2[k] + K3[l]
                                    )

            else:
                msg = f"{dim}D Diffusion filter has not been vectorized yet."
                raise NotImplementedError(msg)
            return functools.partial(F, *args)

    def setup(self, work):
        super().setup(work=work)
        diffusion_filters = ()
        for Fo, Ft, Kd in zip(
            self.dFout.dfields, self.forward_transforms, self.all_dkds
        ):
            args = (Ft.full_output_buffer,) + tuple(Kd)
            F = self.build_diffusion_filter(Fo.dim, *args)
            diffusion_filters += (F,)
        self.diffusion_filters = diffusion_filters

    @op_apply
    def apply(self, simulation, **kwds):
        """Solve the implicit diffusion equation."""
        super().apply(**kwds)

        nu_dt = self.nu() * self.dt()

        with self.map_objects_to_host():
            for Ft, Bt, filter_diffusion in zip(
                self.forward_transforms,
                self.backward_transforms,
                self.diffusion_filters,
            ):
                Ft(simulation=simulation)
                filter_diffusion(nu_dt, Ft.full_output_buffer)
                Bt(simulation=simulation)

        for Fo in self.dFout.dfields:
            Fo.exchange_ghosts()
        self.plot(simulation=simulation)
