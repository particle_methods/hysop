# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import cpuinfo
from psutil import virtual_memory

from hysop.constants import default_order
from hysop.core.memory.allocator import AllocatorBase
from hysop.backend.host.host_buffer import HostBuffer


class HostAllocator(AllocatorBase):
    """
    Allocator that allocates HostBuffers
    """

    def __new__(cls, verbose=None):
        return super().__new__(cls, verbose=verbose)

    def __init__(self, verbose=None):
        super().__init__(verbose=verbose)
        self.mem_size = virtual_memory().total

    def max_alloc_size(self):
        """Max allocatable size in bytes."""
        return self.mem_size

    def allocate(self, nbytes, **kwds):
        super().allocate(nbytes=nbytes, **kwds)
        return HostBuffer(size=nbytes)

    def prefix(self):
        return f"{self.full_tag}: "

    def is_on_host(self):
        """
        Return true if buffers are allocated in host memory.
        """
        return True

    def memory_pool(self, name, **kwds):
        """
        Construct a memory pool from this allocator.
        """
        from hysop.backend.host.host_mempool import MemoryPool, HostMemoryPool

        if isinstance(self, MemoryPool):
            msg = "allocator is already a memory pool."
            raise RuntimeError(msg)
        return HostMemoryPool(allocator=self, name=name, **kwds)


def __get_default_name():
    try:
        cpu_name = cpuinfo.cpuinfo.get_cpu_info()["brand"]
        pos = cpu_name.find("@")
        if pos > 0:
            cpu_name = cpu_name[:pos]
    except:
        cpu_name = "CPU0"
    return cpu_name.strip()


default_host_allocator = HostAllocator()
default_host_mempool = default_host_allocator.memory_pool(name=__get_default_name())
