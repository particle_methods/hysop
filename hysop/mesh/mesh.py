# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
topology.py defines topology abstract interfaces for topologies.

:class:`~hysop.mes.mesh.Mesh
:class:`~hysop.mes.mesh.MeshView

See also
--------
* :class:`~hysop.topology.topology.Topology`
* :ref:`topologies` in HySoP user guide.
"""

from abc import ABCMeta, abstractmethod
from hysop.topology.topology import Topology, TopologyState
from hysop.tools.decorators import debug
from hysop.tools.htypes import check_instance
from hysop.tools.handle import TaggedObject, TaggedObjectView


class MeshView(TaggedObjectView, metaclass=ABCMeta):
    """Abstract base class for views on meshes."""

    __slots__ = ("_mesh", "_topology_state")

    @debug
    def __new__(cls, mesh, topology_state, **kwds):
        return super().__new__(cls, obj_view=mesh, **kwds)

    @debug
    def __init__(self, mesh, topology_state, **kwds):
        """Initialize a MeshView."""
        check_instance(mesh, Mesh)
        check_instance(topology_state, TopologyState)
        super().__init__(obj_view=mesh, **kwds)
        self._mesh = mesh
        self._topology_state = topology_state

    def _get_mesh(self):
        """Return the original mesh on which the view is."""
        return self._mesh

    def _get_topology_state(self):
        """Return the topology state"""
        return self._topology_state

    def _get_dim(self):
        """Return the dimension of the domain."""
        return self._mesh._topology.domain.dim

    @abstractmethod
    def short_description(self):
        """Short description of this mesh."""
        pass

    @abstractmethod
    def long_description(self):
        """Long description of this mesh."""
        pass

    def __str__(self):
        """Equivalent to self.long_description()"""
        return self.long_description()

    mesh = property(_get_mesh)
    topology_state = property(_get_topology_state)
    dim = property(_get_dim)


class Mesh(TaggedObject, metaclass=ABCMeta):
    """Abstract base class for local to process meshes."""

    def __new__(cls, topology, **kwds):
        return super().__new__(cls, **kwds)

    @debug
    def __init__(self, topology, **kwds):
        """Initialize a mesh."""
        check_instance(topology, Topology)
        super().__init__(tag_prefix="m", **kwds)
        self._topology = topology

    @abstractmethod
    def view(self, topology_state):
        """Return a view on this mesh using a topology state."""
        pass

    def _get_topology(self):
        """Return a topology view on the original topology that defined this mesh."""
        return self._topology

    topology = property(_get_topology)
