# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
cartesian_mesh.py defines CartesianTopology topology meshes.

:class:`~hysop.mesh.mesh.CartesianMesh`
:class:`~hysop.mesh.mesh.CartesianMeshView`

See also
--------
* :class:`~hysop.topology.topology.CartesianTopology`
* :ref:`topologies` in HySoP user guide.
"""
import itertools as it

from hysop.constants import (
    np,
    BoundaryCondition,
    GhostMask,
    HYSOP_INTEGER,
    HYSOP_REAL,
    MemoryOrdering,
)
from hysop.tools.decorators import debug
from hysop.tools.numpywrappers import npw
from hysop.tools.htypes import check_instance, to_tuple
from hysop.tools.misc import Utils, prod
from hysop.mesh.mesh import MeshView, Mesh


class CartesianMeshView(MeshView):
    """Mesh views on cartesian meshes."""

    __slots__ = ("_mesh", "_topology_state")

    @debug
    def __new__(cls, mesh, topology_state, **kwds):
        """Create a CartesianMeshView."""
        return super().__new__(cls, mesh=mesh, topology_state=topology_state, **kwds)

    @debug
    def __init__(self, mesh, topology_state, **kwds):
        """Initialize a CartesianMeshView."""
        from hysop.topology.cartesian_topology import CartesianTopologyState

        check_instance(mesh, CartesianMesh)
        check_instance(topology_state, CartesianTopologyState)
        super().__init__(mesh=mesh, topology_state=topology_state, **kwds)

    def __get_transposed_mesh_attr(self, *attr_names, **kwds):
        """
        Get one or more transposed mesh attribute.

        If more than one attribute is requested, return transposed
        attributes as a tuple.

        If 'iterable' is set to True (trough kwds), the unique attribute is fetched and
        its values are transposed afterwards (this implies len(attr_names)==1).
        In this case the return value is a tuple.
        """
        check_instance(attr_names, tuple, values=str, minsize=1)

        transpose = self._topology_state.transposed
        if ("iterable" in kwds) and (kwds["iterable"] is True):
            assert len(attr_names) == 1
            attr0 = getattr(self._mesh, attr_names[0])
            res = tuple(transpose(val) for val in attr0)
            return res
        else:
            res = tuple(transpose(getattr(self._mesh, an)) for an in attr_names)
            if len(attr_names) == 1:
                return res[0]
            else:
                return res

    def _get_topology(self):
        """Get a topology view attached to this mesh."""
        return self._mesh._topology.view(self._topology_state)

    def _get_on_proc(self):
        """Return True if the current process has points on the current mesh."""
        return self._mesh._on_proc

    def _get_grid_resolution(self):
        """
        Effective resolution of the global mesh.
        Corresponds to self.global_resolution - topology.is_periodic.
        See global_resolution for topology global resolution.
        """
        return self.__get_transposed_mesh_attr("_grid_resolution")

    def _get_grid_npoints(self):
        """
        Effective size of the global mesh.
        Corresponds to np.prod(self.grid_resolution)
        """
        return prod(self._mesh._grid_resolution)

    def _get_global_resolution(self):
        """
        Global resolution of the associated topology.
        See grid_resolution for effective global mesh size.
        """
        return self.__get_transposed_mesh_attr("_global_resolution")

    def _get_global_start(self):
        """
        Index of the first computational (i.e. exluding ghosts)
        point of this mesh in the global grid.
        """
        return self.__get_transposed_mesh_attr("_global_start")

    def _get_global_stop(self):
        """
        Index of the last computational (i.e. excluding ghosts)
        point of this mesh in the global grid.
        """
        return self.__get_transposed_mesh_attr("_global_stop")

    def _get_global_compute_slices(self):
        """
        Slices of indices with starting point being the first computational point
        and last point being the last computational point in the global grid.
        This is the global computational view (ie. global indices of the computational points).
        """
        return self.__get_transposed_mesh_attr("_global_compute_slices")

    def _get_global_ghost_slices(self):
        """
        Return a list of slices defining the ghosts in this arrays as global indices.
        There are two ghost slices per specified directions.
        For each direction, a pair of tuples of slices is returned, one
        for left and right ghosts.
        """
        return self.__get_transposed_mesh_attr("_global_ghost_slices")

    def _get_global_origin(self):
        """
        Origin of the global mesh on the global physical domain.
        """
        return self.__get_transposed_mesh_attr("_global_origin")

    def _get_global_end(self):
        """
        Last point of the global mesh on the global physical domain.
        """
        return self.__get_transposed_mesh_attr("_global_end")

    def _get_global_length(self):
        """Physical size of the global physical domain."""
        return self.__get_transposed_mesh_attr("_global_length")

    def _get_global_lboundaries(self):
        """Return global domain left boundaries."""
        return self.__get_transposed_mesh_attr("_global_lboundaries")

    def _get_global_rboundaries(self):
        """Return global domain right boundaries."""
        return self.__get_transposed_mesh_attr("_global_rboundaries")

    def _get_global_boundaries(self):
        """Return global domain boundaries as a tuple of left and right boundaries."""
        return (self._get_global_lboundaries(), self._get_global_rboundaries())

    def _get_periodicity(self):
        """
        Get periodicity of the global boundaries.
        This is not to be confused with the cartesian communicator periodicity.
        """
        return self._get_global_lboundaries() == BoundaryCondition.PERIODIC

    def _get_local_resolution(self):
        """
        Local resolution of this mesh including ghosts.
        """
        return self.__get_transposed_mesh_attr("_local_resolution")

    def _get_local_npoints(self):
        """
        Effective size of the local mesh.
        Corresponds to np.prod(self.local_resolution)
        """
        return prod(self._mesh._local_resolution)

    def _get_local_start(self):
        """
        Index of the first computational (i.e. excluding ghosts)
        point of this mesh in the local grid.
        This has the same value as self.ghosts.
        """
        return self.__get_transposed_mesh_attr("_local_start")

    def _get_local_stop(self):
        """
        Index of the last computational (i.e. excluding ghosts)
        point of this mesh in the local grid.
        """
        return self.__get_transposed_mesh_attr("_local_stop")

    def _get_local_indices(self):
        """
        Return a tuple of indices, one per axe, indexing the whole array with ghosts.
        """
        return self.__get_transposed_mesh_attr("_local_indices")

    def _get_local_coords(self):
        """
        Local coordinates as a tuple of numpy linear spaces.
        Those are physical coordinates of each axe including ghosts.
        Coordinates are returned as Xo, ..., Xdim-1.
        where Xo is between domain.origin[0] and domain.origin[0] + domain.length[0].
        """
        return self.__get_transposed_mesh_attr("_local_coords")

    def _get_local_mesh_indices(self):
        """
        Same as self.local_indices but with all sequences as nd arrays.
        See numpy.ix_.
        """
        return np.ix_(*self._get_local_indices())

    def _get_local_mesh_coords(self):
        """
        Same as self.local_coords but with all sequences as nd arrays.
        and reversed (ie. returned coordinates are x,y,z,...) instead
        of (...,z,y,x).
        See numpy.ix_.
        """
        axes = self._topology_state.axes
        ix = np.ix_(*self._get_local_coords())
        if self._topology_state.memory_order == MemoryOrdering.C_CONTIGUOUS:
            return tuple(ix[axes.index(len(axes) - 1 - _)] for _ in range(len(axes)))
        elif self._topology_state.memory_order == MemoryOrdering.F_CONTIGUOUS:
            return tuple(ix[axes.index(_)] for _ in range(len(axes)))

    def _get_local_compute_coords(self):
        """
        Local coordinates as a tuple of numpy linear spaces.
        Those are physical coordinates of each axe excluding ghosts.
        Last coords are the contiguous ones.
        """
        return self.__get_transposed_mesh_attr("_local_compute_coords")

    def _get_local_compute_indices(self):
        """
        Return a tuple of indices, one per axe, indexing the array excluding ghosts.
        """
        return self.__get_transposed_mesh_attr("_local_compute_indices")

    def _get_local_compute_mesh_indices(self):
        """
        Same as self.local_compute_indices but with all sequences as nd arrays.
        See numpy.ix_.
        """
        return np.ix_(*self._get_local_compute_indices())

    def _get_local_compute_mesh_coords(self):
        """
        Same as self.local_compute_coords but with all sequences as nd arrays
        and reversed (ie. returned coordinates are x,y,z,...) instead
        of (...,z,y,x).
        See numpy.ix_.
        """
        return np.ix_(*self._get_local_compute_coords())[::-1]

    def _get_local_compute_slices(self):
        """
        Slices of indices with starting point being the first computational point
        and last point being the last computational point in the local grid.
        This is the local computational view (ie. local indices of the computational points).
        """
        return self.__get_transposed_mesh_attr("_local_compute_slices")

    def get_local_inner_ghost_slices(self, ghosts=None, ghost_mask=GhostMask.CROSS):
        """
        Return a list of slices defining the ghosts in this arrays a local indices.
        Those slices corresponds to neighbour processes overlap on local compute slices.
        Depending on the ghost_mask parameter, one can include or exclude diagonal ghosts:
            GhostMask.FULL:   INCLUDES diagonal ghosts
            GhostMask.CROSS:  EXCLUDES diagonal ghosts
        For each direction, a pair of tuples of slices is returned, one for left and one
        for the right ghosts. If direction has no ghosts (lslice, rslice, shape=None) is returned.
        """
        dim = self.dim
        local_start = self.local_start
        local_stop = self.local_stop
        compute_resolution = self.compute_resolution
        local_resolution = self.local_resolution

        ghosts = to_tuple(ghosts or self.ghosts)
        ghosts = ghosts * dim if len(ghosts) == 1 else ghosts
        assert len(ghosts) == dim

        if ghost_mask is GhostMask.FULL:
            gh_slices = lambda j: slice(None)
            resolution = local_resolution
        elif ghost_mask is GhostMask.CROSS:
            gh_slices = lambda j: slice(ghosts[j], compute_resolution[j] + ghosts[j])
            resolution = compute_resolution
        else:
            msg_mask = "Unknown ghost ghost_mask configuration {}."
            msg_mask = msg_mask.format(ghost_mask)
            raise NotImplementedError(msg_mask)

        local_inner_ghost_slices = []
        for i in range(dim):
            inner_lslices = tuple(
                (
                    gh_slices(j)
                    if j != i
                    else slice(local_start[i], local_start[i] + ghosts[i])
                )
                for j in range(dim)
            )
            inner_rslices = tuple(
                (
                    gh_slices(j)
                    if j != i
                    else slice(local_stop[i] - ghosts[i], local_stop[i])
                )
                for j in range(dim)
            )
            if ghosts[i] > 0:
                inner_shape = resolution.copy()
                inner_shape[i] = ghosts[i]
            else:
                inner_shape = None
            local_inner_ghost_slices.append((inner_lslices, inner_rslices, inner_shape))

        return tuple(local_inner_ghost_slices)

    def get_local_outer_ghost_slices(self, ghosts=None, ghost_mask=GhostMask.CROSS):
        """
        Return a list of slices defining the ghosts in this arrays a local indices.
        Those slices corresponds to local to process ghost slices.
        Depending on the ghost_mask parameter, one can include or exclude diagonal ghosts:
            GhostMask.FULL:   INCLUDES diagonal ghosts
            GhostMask.CROSS:  EXCLUDES diagonal ghosts
        For each direction, a pair of tuples of slices is returned, one for left and one
        for the right ghosts. If direction has no ghosts (lslice, rslice, shape=None) is returned.
        """
        dim = self.dim
        local_start = self.local_start
        local_stop = self.local_stop
        compute_resolution = self.compute_resolution
        local_resolution = self.local_resolution

        ghosts = to_tuple(ghosts or self.ghosts)
        ghosts = ghosts * dim if len(ghosts) == 1 else ghosts
        assert len(ghosts) == dim

        if ghost_mask is GhostMask.FULL:
            gh_slices = lambda j: slice(None)
            resolution = local_resolution
        elif ghost_mask is GhostMask.CROSS:
            gh_slices = lambda j: slice(ghosts[j], compute_resolution[j] + ghosts[j])
            resolution = compute_resolution
        else:
            msg_mask = "Unknown ghost ghost_mask configuration {}."
            msg_mask = msg_mask.format(ghost_mask)
            raise NotImplementedError(msg_mask)

        local_outer_ghost_slices = []
        for i in range(dim):
            outer_lslices = tuple(
                (
                    gh_slices(j)
                    if j != i
                    else slice(local_start[i] - ghosts[i], local_start[i])
                )
                for j in range(dim)
            )
            outer_rslices = tuple(
                (
                    gh_slices(j)
                    if j != i
                    else slice(local_stop[i], local_stop[i] + ghosts[i])
                )
                for j in range(dim)
            )
            if ghosts[i] > 0:
                outer_shape = resolution.copy()
                outer_shape[i] = ghosts[i]
            else:
                outer_shape = None
            local_outer_ghost_slices.append((outer_lslices, outer_rslices, outer_shape))

        return tuple(local_outer_ghost_slices)

    def get_boundary_layer_slices(self, ghosts=None, ghost_mask=GhostMask.CROSS):
        """
        Return a list of slices defining the ghosts in this arrays a local indices.
        Those slices corresponds to non periodic boundary layers (ie. inner + outer ghosts).
        Depending on the ghost_mask parameter, one can include or exclude diagonal ghosts:
            GhostMask.FULL:   INCLUDES diagonal ghosts
            GhostMask.CROSS:  EXCLUDES diagonal ghosts
        For each direction, a pair of tuples of slices is returned, one for left boundary
        for the right boundary. If the process is not at_left or not at_right or if boundaries
        are periodic, None is returned.
        """
        dim = self.dim
        local_start = self.local_start
        local_stop = self.local_stop
        compute_resolution = self.compute_resolution
        local_resolution = self.local_resolution

        local_lboundaries = self.local_lboundaries
        local_rboundaries = self.local_rboundaries
        blacklisted_boundaries = (BoundaryCondition.PERIODIC, BoundaryCondition.NONE)

        ghosts = to_tuple(ghosts or self.ghosts)
        ghosts = ghosts * dim if len(ghosts) == 1 else ghosts
        assert len(ghosts) == dim

        if ghost_mask is GhostMask.FULL:
            gh_slices = lambda j: slice(None)
            resolution = local_resolution
        elif ghost_mask is GhostMask.CROSS:
            gh_slices = lambda j: slice(ghosts[j], compute_resolution[j] + ghosts[j])
            resolution = compute_resolution
        else:
            msg_mask = "Unknown ghost ghost_mask configuration {}."
            msg_mask = msg_mask.format(ghost_mask)
            raise NotImplementedError(msg_mask)

        boundary_layer_slices = []
        for i in range(dim):
            (lbd, rbd) = local_lboundaries[i], local_rboundaries[i]
            has_left_layer = lbd not in blacklisted_boundaries
            has_right_layer = rbd not in blacklisted_boundaries

            if has_left_layer:
                layer_lslices = tuple(
                    (
                        gh_slices(j)
                        if j != i
                        else slice(
                            local_start[i] - ghosts[i], local_start[i] + ghosts[i] + 1
                        )
                    )
                    for j in range(dim)
                )
            else:
                layer_lslices = None

            if has_right_layer:
                layer_rslices = tuple(
                    (
                        gh_slices(j)
                        if j != i
                        else slice(
                            local_stop[i] - ghosts[i] - 1, local_stop[i] + ghosts[i]
                        )
                    )
                    for j in range(dim)
                )
            else:
                layer_rslices = None

            if has_left_layer or has_right_layer:
                layer_shape = resolution.copy()
                layer_shape[i] = 2 * ghosts[i] + 1
            else:
                layer_shape = None

            boundary_layer_slices.append((layer_lslices, layer_rslices, layer_shape))

        return tuple(boundary_layer_slices)

    def get_all_local_inner_ghost_slices(self, ghosts=None):
        """
        Return collection of slices and shapes describing all possible
        combinations of inner ghosts slices in this array as local indices.

        Those slices corresponds to local to process ghosts (ie. ghosts that may
        be sent to other neighbor processes, including diagonal procecess,
        during a ghosts exchange).

        Return a dictionnary {ndirections} (number of directions intersected)
                                -> {directions} (0=first axis, ..., dim-1=last axis)
                                    -> {displacements} (-1=LEFT, 0=CENTER, +1=RIGHT)
                                        -> (view, shape)
        If one of the direction has no ghosts (lslice, rslice, shape=None) is returned.
        """
        lshape = self.local_resolution
        dim = self.dim

        ghosts = to_tuple(ghosts or self.ghosts)
        ghosts = ghosts * dim if len(ghosts) == 1 else ghosts
        assert len(ghosts) == dim

        views = {}
        for ndirections in range(dim + 1):
            all_directions = tuple(it.combinations(range(dim), ndirections))
            for directions in all_directions:
                all_displacements = tuple(it.product((-1, 0, +1), repeat=ndirections))
                for displacements in all_displacements:
                    ai = 0
                    view = ()
                    shape = ()
                    for d in range(dim):
                        if d in directions:
                            if displacements[ai] == -1:
                                view += (slice(1 * ghosts[d], 2 * ghosts[d]),)
                                shape += (ghosts[d],)
                            elif displacements[ai] == 0:
                                view += (
                                    slice(2 * ghosts[d], lshape[d] - 2 * ghosts[d]),
                                )
                                shape += (lshape[d] - 4 * ghosts[d],)
                            else:  # displacements[ai] == +1
                                view += (
                                    slice(
                                        lshape[d] - 2 * ghosts[d],
                                        lshape[d] - 1 * ghosts[d],
                                    ),
                                )
                                shape += (ghosts[d],)
                            ai += 1
                        else:
                            view += (slice(ghosts[d], lshape[d] - ghosts[d]),)
                            shape += (lshape[d] - 2 * ghosts[d],)
                    if any((Si == 0) for Si in shape):
                        shape = None
                    else:
                        shape = npw.asarray(shape, dtype=np.int32)
                    views.setdefault(ndirections, {}).setdefault(
                        directions, {}
                    ).setdefault(displacements, (view, shape))
        return views

    def get_all_local_outer_ghost_slices(self, ghosts=None):
        """
        Return collection of slices and shapes describing all possible combinations
        of outer ghosts slices in this array as local indices.

        Those slices corresponds to local to process ghosts (ie. ghosts that may
        be received from other neighbor processes, including diagonal processes,
        during a ghosts exchange).

        Return a dictionnary {ndirections} (number of directions intersected)
                                -> {directions} (0=first axis, ..., dim-1=last axis)
                                    -> {displacements} (-1=LEFT, 0=CENTER, +1=RIGHT)
                                        -> (view, shape)
        """
        lshape = self.local_resolution
        dim = self.dim

        ghosts = to_tuple(ghosts or self.ghosts)
        ghosts = ghosts * dim if len(ghosts) == 1 else ghosts
        assert len(ghosts) == dim

        views = {}
        for ndirections in range(dim + 1):
            all_directions = tuple(it.combinations(range(dim), ndirections))
            for directions in all_directions:
                all_displacements = tuple(it.product((-1, 0, +1), repeat=ndirections))
                for displacements in all_displacements:
                    ai = 0
                    view = ()
                    shape = ()
                    for d in range(dim):
                        if d in directions:
                            if displacements[ai] == -1:
                                view += (slice(0, ghosts[d]),)
                                shape += (ghosts[d],)
                            elif displacements[ai] == 0:
                                view += (slice(ghosts[d], lshape[d] - ghosts[d]),)
                                shape += (lshape[d] - 2 * ghosts[d],)
                            else:  # displacements[ai] == +1:
                                view += (slice(lshape[d] - ghosts[d], lshape[d]),)
                                shape += (ghosts[d],)
                            ai += 1
                        else:
                            view += (slice(ghosts[d], lshape[d] - ghosts[d]),)
                            shape += (lshape[d] - 2 * ghosts[d],)
                    if any((Si == 0) for Si in shape):
                        shape = None
                    else:
                        shape = npw.asarray(shape, dtype=np.int32)
                    views.setdefault(ndirections, {}).setdefault(
                        directions, {}
                    ).setdefault(displacements, (view, shape))
        return views

    def get_all_local_inner_ghost_slices_per_ncenters(self, ghosts=None):
        """
        Compute the collection of slices describing all possible combinations
        of inner ghosts slice in this array as local indices like
        self.get_all_local_inner_ghost_slices() and sort them by
        number of centers (number of displacement == 0).

        Return a list [ndirections] (number of directions intersected)
                         -> {directions} (0=first axis, ..., dim-1=last axis)
                             [ncenters] (number of null displacements)
                                -> {displacements} (-1=LEFT, 0=CENTER, +1=RIGHT)
                                    -> (view,shape)
                                -> 'all'
                                    -> tuple of all views and shapes up to this depth
                        -> ncenters
                            -> tuple of all views and shapes with given number of centers
        """
        views = self.get_all_local_inner_ghost_slices(ghosts=ghosts)
        return self._sort_by_centers(views)

    def get_all_local_outer_ghost_slices_per_ncenters(self, ghosts=None):
        """
        Compute the collection of slices describing all possible combinations
        of outer ghosts slice in this array as local indices like
        self.get_all_local_outer_ghost_slices() and sort them by
        number of centers (number of displacement == 0).

        Return a dict {ndirections} (number of directions intersected)
                         -> {directions} (0=first axis, ..., dim-1=last axis)
                             {ncenters} (number of null displacements)
                                -> {displacements} (-1=LEFT, 0=CENTER, +1=RIGHT)
                                    -> (view,shape)
                                -> 'all'
                                    -> list of all views and shapes up to this depth
                        -> {ncenters}
                            -> list of all views and shapes with given number of centers
        """
        views = self.get_all_local_outer_ghost_slices(ghosts=ghosts)
        return self._sort_by_centers(views)

    def _sort_by_centers(self, all_views):
        views = {}
        for ndirections in all_views:
            for directions in all_views[ndirections]:
                for ncenters in range(ndirections + 1):
                    for displacements in it.product((-1, 0, +1), repeat=ndirections):
                        if sum(d == 0 for d in displacements) != ncenters:
                            continue

                        data = all_views[ndirections][directions][displacements]

                        views.setdefault(ndirections, {}).setdefault(
                            ncenters, []
                        ).append(data)

                        views.setdefault(ndirections, {}).setdefault(
                            directions, {}
                        ).setdefault(ncenters, {}).setdefault("all", []).append(data)

                        views.setdefault(ndirections, {}).setdefault(
                            directions, {}
                        ).setdefault(ncenters, {}).setdefault(displacements, data)

        return views

    def _get_local_origin(self):
        """
        Origin of the first computational point on the physical box
        including ghosts.
        """
        return self.__get_transposed_mesh_attr("_local_origin")

    def _get_local_end(self):
        """
        Origin of the last computational point on the physical box.
        including ghosts.
        """
        return self.__get_transposed_mesh_attr("_local_end")

    def _get_local_length(self):
        """
        Physical size of the local physical domain including ghosts.
        """
        return self.__get_transposed_mesh_attr("_local_length")

    def _get_local_lboundaries(self):
        """
        Return local domain left boundaries.
        Boundaries on the interior of the global domain have value BoundaryCondition.NONE.
        """
        return self.__get_transposed_mesh_attr("_local_lboundaries")

    def _get_local_rboundaries(self):
        """
        Return local domain right boundaries.
        Boundaries on the interior of the global domain have value BoundaryCondition.NONE.
        """
        return self.__get_transposed_mesh_attr("_local_rboundaries")

    def _get_local_boundaries(self):
        """
        Return local domain boundaries as a tuple of left and right boundaries.
        Boundaries on the interior of the global domain have value BoundaryCondition.NONE.
        """
        return (self._get_local_lboundaries(), self._get_local_rboundaries())

    def _get_compute_resolution(self):
        """
        Local resolution of this mesh excluding ghosts.
        """
        return self.__get_transposed_mesh_attr("_compute_resolution")

    def _get_ghosts(self):
        """
        Return the local to mesh ghosts (on left and right boundaries).
        This has the same value as self.local_start.
        """
        return self.__get_transposed_mesh_attr("_ghosts")

    def _get_space_step(self):
        """Get the space step of the discretized mesh grid."""
        return self.__get_transposed_mesh_attr("_space_step")

    def _get_is_at_left_boundary(self):
        """
        Return a numpy boolean mask to identify meshes that are on the left of the domain.
        ie. is_at_left_boundary[d] = True means that process cartesian coordinates is the first
        on direction d:  proc_coords[d] == 0.
        """
        return self.__get_transposed_mesh_attr("_is_at_left_boundary")

    def _get_is_at_right_boundary(self):
        """
        Return a numpy boolean mask to identify meshes that are on the right of the domain.
        ie. is_at_right_boundary[d] = True means that process cartesian coordinates
            is the lastest on direction d:  proc_coords[d] == proc_shape[d] - 1.
        """
        return self.__get_transposed_mesh_attr("_is_at_right_boundary")

    def _get_is_at_boundary(self):
        """
        Return a numpy boolean mask to identify meshes that are on either on the left or on
        the right of the domain. Meshes can be on the left and the right at the same time on
        direction d if and only if proc_shape[d] == 1.
        ie. is_at_boundary[d] = True means that process cartesian coordinates  is the first or
            the lastest on direction d:(proc_coords[d] in [0, proc_shape[d] - 1]).
        """
        return np.logical_and(
            self._get_is_at_left_boundary(), self._get_is_at_right_boundary()
        )

    def _get_proc_coords(self):
        """
        Return the coordinate of the process that owns this mesh in the
        CartesianTopology topology.
        """
        return self.__get_transposed_mesh_attr("_proc_coords")

    def _get_proc_shape(self):
        """Return the shape of MPI cartesian topology defining the global mesh."""
        return self.__get_transposed_mesh_attr("_proc_shape")

    def iter_axes(self, axes=0):
        """
        Return an iterator over a mesh array that returns indices.
        Iterates 'axes' axes at a time (0=whole array view,
        1=one axe at a time, max value is self.dim).
        Iterated shape indices are also yielded.
        """
        assert 0 <= axes <= self.dim, "bad value for axes"
        iter_shape = tuple(self.local_resolution[:axes])
        I = np.ix_(*self.local_indices[axes:])
        for idx in np.ndindex(*iter_shape):
            yield (idx, I)

    def iter_compute_mesh(self, axes=0):
        """
        Return an iterator over a compute mesh array that returns indices.
        Iterates 'axes' axes at a time (0=whole array view,
        1=one axe at a time, max value is self.dim).
        """
        return self.build_compute_mesh_iterator(axes=axes).iter_compute_mesh()

    def build_compute_mesh_iterator(self, axes):
        """
        Return an object that can iterate over the local
        computational mesh multiple times.
        Usefull for optimizing operator apply methods.
        See self.iter_compute_mesh().
        """
        assert 0 <= axes <= self.dim, "bad value for axes"

        class __CartesianMeshComputeAxeIterator:
            def __init__(self, dim, ghosts, compute_resolution, local_compute_indices):
                iter_shape = tuple(compute_resolution[:axes])
                gI = np.ix_(*local_compute_indices[axes:])
                I = tuple(gI[i] - ghosts[j] for i, j in enumerate(range(axes, dim)))

                self._iter_shape = iter_shape
                self._data = (dim, ghosts, I, gI)

                if axes <= 1:
                    self._generated_indices = tuple(np.ndindex(*self._iter_shape))
                else:
                    self._generated_indices = None

            def _new_ndindex_iterator(self):
                # Return pregenerated nd indices if we are in dim 0 or 1,
                # else just create a new np.ndindex iterator (slower)
                # when iterated thousands of time.
                if axes <= 1:
                    return self._generated_indices
                else:
                    return np.ndindex(*self._iter_shape)

            def iter_compute_mesh(self):
                (dim, ghosts, I, gI) = self._data
                for idx in self._new_ndindex_iterator():
                    gidx = tuple(idx[i] + ghosts[i] for i in range(axes))
                    yield (idx, gidx, I, gI)

        return __CartesianMeshComputeAxeIterator(
            dim=self.dim,
            ghosts=self.ghosts,
            compute_resolution=self.compute_resolution,
            local_compute_indices=self.local_compute_indices,
        )

    def is_inside_local_domain(self, point):
        """
        Return true if point is located inside (or on the boundaries)
        of the local subdomain.
        """
        point = npw.asrealarray(point)
        if (point < self.local_origin).any() or (point > self.local_end).any():
            return False
        else:
            return True

    def is_inside_global_domain(self, point):
        """
        Return true if point is located inside (or on the boundaries)
        of the local subdomain.
        """
        point = npw.asrealarray(point)
        if (point < self.global_origin).any() or (point > self.global_end).any():
            return False
        else:
            return True

    def point_local_indices(self, point):
        """
        Return the local indices of the mesh point that is
        the closest to the given point (which is a position in the
        local subdomain).
        If the input point is not inside the local subdomain, return None.
        Else return a np.ndarray with dtype HYSOP_DIM.
        """
        point = npw.asrealarray(point)
        if self.is_inside_local_domain(point):
            indices = np.rint((point - self.local_origin) / self.space_step)
            return npw.asintegerarray(indices)
        else:
            return None

    def point_global_indices(self, point):
        """
        Return the global indices of the mesh point that is
        the closest to the given point (which is a position in the domain).
        If the input point is not inside the domain, return None.
        Else return a np.ndarray with dtype HYSOP_DIM.
        """
        point = npw.asrealarray(point)
        if self.is_inside_global_domain(point):
            indices = np.rint((point - self.global_origin) / self.space_step)
            return npw.asintegerarray(indices)
        else:
            return None

    def local_to_global(self, local_slices):
        """
        Convert input local index slices to global index slices.

        The input is a list of tuple of slices (one per domain direction)
        of local indices and the result is a tuple of slices
        of the same indicices as global indices.

        If there is no intersection with the local mesh, return None.
        """
        check_instance(local_slices, (list, tuple), values=slice)
        slc = local_slices
        return [
            slice(
                slc[i].start + self.global_start[i] - self.ghosts[i],
                slc[i].stop + self.global_start[i] - self.ghosts[i],
                slc[i].step,
            )
            for i in range(self._dim)
        ]

    def global_to_local(self, global_slices):
        """
        Perform the intersection of input global mesh indices
        with the local mesh indices of thuis mesh and return
        the resulting global indices that intersected as
        local indices.

        The input is a list or tuple of slices (one per domain direction)
        of global indices and the result is a tuple of slices
        of intersected local indices, if the intersection is
        not empty.

        If there is no intersection with the local mesh, return None.
        """
        check_instance(global_slices, (list, tuple), values=slice)

        slc = Utils.intersect_slices(global_slices, self.global_compute_slices)
        if slc is None:
            return None
        else:
            return tuple(
                slice(
                    slc[i].start - self.global_start[i] + self.ghosts[i],
                    slc[i].stop - self.global_start[i] + self.ghosts[i],
                    slc[i].step,
                )
                for i in range(self.dim)
            )

    def local_shift(self, indices):
        """Shift input indices (tuple of integer array as obtained from np.where)
        with ghost layer size
        """
        shift = [self.local_start[d] for d in range(self.dim)]
        return tuple(indices[d] + shift[d] for d in range(self.dim))

    def compute_integ_point(self, is_last, ic, n_dir=None):
        """Compute indices corresponding to integration points
        Parameters
        ----------
        is_last : numpy array of bool
            is_last[d] = True means the process is on the top
            boundary of the mesh, in direction d.
        ic : tuple of indices
            indices of the mesh
        n_dir : array of int
            direction where lengthes of the mesh are null.
        """
        dim = len(ic)
        # We must find which points must be used
        # when we integrate on this submesh
        stops = npw.asdimarray([ic[d].stop for d in range(dim)])
        # when 'is_last', the last point must be removed for integration
        stops[is_last] -= 1
        # and finally, for direction where subset length is zero,
        # we must increase stop, else integral will always be zero!
        if n_dir is not None:
            stops[n_dir] = npw.asdimarray([ic[d].start + 1 for d in n_dir])

        return [slice(ic[d].start, stops[d]) for d in range(dim)]

    def reduce_coords(self, coords, reduced_index):
        """Compute a reduced set of coordinates

        Parameters
        ----------
        coords : tuple of arrays
            the original coordinates
        reduce : list of slices
            indices of points for which reduced coordinates
            are required.

        Returns a tuple-like set of coordinates.

        """
        assert isinstance(coords, tuple)
        assert isinstance(reduced_index, tuple)
        dim = len(coords)
        shapes = [list(coords[i].shape) for i in range(dim)]
        res = [reduced_index[i].stop - reduced_index[i].start for i in range(dim)]
        for i in range(dim):
            shapes[i][i] = res[i]
        shapes = tuple(shapes)
        return [coords[i].flat[reduced_index[i]].reshape(shapes[i]) for i in range(dim)]

    def short_description(self):
        """
        Short description of this mesh as a string.
        """
        s = "CartesianMesh"
        s += "[topo_id={}, proc_coords={}, global_start={}, local_res={}, ghosts={}]"
        s = s.format(
            self.topology.id,
            self.proc_coords,
            self.global_start,
            self.local_resolution,
            self.ghosts,
        )
        return s

    def long_description(self):
        """
        Long description of this mesh as a string.
        """
        s = f"{self.full_tag}:\n"
        s += f"  *proc coords:        {self.proc_coords}\n"
        s += f"  *global start:       {self.global_start}\n"
        s += f"  *local resolution:   {self.local_resolution}\n"
        s += f"  *compute resolution: {self.compute_resolution}\n"
        s += f"  *ghosts:             {self.ghosts}\n"
        s += f"  *local boundaries:   left  => {self.local_boundaries[0]}\n"
        s += f"                       right => {self.local_boundaries[1]}\n"
        return s

    def __eq__(self, other):
        if not isinstance(other, CartesianMeshView):
            return NotImplemented
        eq = self._mesh._topology is other._mesh._topology
        eq &= (self._mesh._local_resolution == other._mesh._local_resolution).all()
        eq &= (self._mesh._global_start == other._mesh._global_start).all()
        eq &= self._topology_state == other._topology_state
        return eq

    def __ne__(self, other):
        if not isinstance(other, CartesianMeshView):
            return NotImplemented
        eq = self._mesh._topology is other._mesh._topology
        eq &= self._mesh._topology is other._mesh._topology
        eq &= (self._mesh._local_resolution == other._mesh._local_resolution).all()
        eq &= self._topology_state == other._topology_state
        return not eq

    def __hash__(self):
        h = id(self._mesh._topology)
        h ^= hash(self._mesh._local_resolution.data)
        h ^= hash(self._mesh._global_start.data)
        h ^= hash(self._topology_state)
        return h

    topology = property(_get_topology)
    on_proc = property(_get_on_proc)

    grid_resolution = property(_get_grid_resolution)
    grid_npoints = property(_get_grid_npoints)

    global_resolution = property(_get_global_resolution)
    global_start = property(_get_global_start)
    global_stop = property(_get_global_stop)
    global_compute_slices = property(_get_global_compute_slices)
    global_ghost_slices = property(_get_global_ghost_slices)
    global_origin = property(_get_global_origin)
    global_end = property(_get_global_end)
    global_length = property(_get_global_length)
    global_boundaries = property(_get_global_boundaries)
    global_lboundaries = property(_get_global_lboundaries)
    global_rboundaries = property(_get_global_rboundaries)
    periodicity = property(_get_periodicity)

    local_resolution = property(_get_local_resolution)
    local_npoints = property(_get_local_npoints)
    local_start = property(_get_local_start)
    local_stop = property(_get_local_stop)
    local_compute_slices = property(_get_local_compute_slices)
    local_inner_ghost_slices = property(get_local_inner_ghost_slices)
    local_outer_ghost_slices = property(get_local_outer_ghost_slices)
    local_origin = property(_get_local_origin)
    local_end = property(_get_local_end)
    local_length = property(_get_local_length)
    local_lboundaries = property(_get_local_lboundaries)
    local_rboundaries = property(_get_local_rboundaries)
    local_boundaries = property(_get_local_boundaries)
    local_indices = property(_get_local_indices)
    local_coords = property(_get_local_coords)
    local_mesh_indices = property(_get_local_mesh_indices)
    local_mesh_coords = property(_get_local_mesh_coords)
    local_compute_indices = property(_get_local_compute_indices)
    local_compute_coords = property(_get_local_compute_coords)
    local_compute_mesh_indices = property(_get_local_compute_mesh_indices)
    local_compute_mesh_coords = property(_get_local_compute_mesh_coords)

    ghosts = property(_get_ghosts)
    compute_resolution = property(_get_compute_resolution)
    space_step = property(_get_space_step)

    is_at_left_boundary = property(_get_is_at_left_boundary)
    is_at_right_boundary = property(_get_is_at_right_boundary)
    is_at_boundary = property(_get_is_at_boundary)

    proc_coords = property(_get_proc_coords)
    proc_shape = property(_get_proc_shape)


class CartesianMesh(CartesianMeshView, Mesh):

    @debug
    def __new__(cls, topology, local_resolution, global_start, **kwds):
        return super().__new__(
            cls,
            topology=topology,
            topology_state=topology.topology_state,
            mesh=None,
            **kwds,
        )

    @debug
    def __init__(self, topology, local_resolution, global_start, **kwds):
        """
        A local cartesian grid, defined on each mpi process.

        Parameters
        -----------
        topology: :class:`hysop.topology.CartesianTopology`
            CartesianTopology topology that creates this mesh.
        local_resolution: array like of ints
            Local resolution of this mesh, *including* ghost points.
        global_start: array like of ints
            Indices in the global mesh of the lowest point of
            the local mesh (excluding ghosts).
        kwds: dict
            Base class arguments.

        Attributes
        ----------
        See :class:`hysop.mesh.cartesian_mesh.CartesianMeshView`.

        Notes
        -----
        This is a class mainly for internal use not supposed to be called
        directly by user but from domain during topology creation.

        Example
        -------
        Consider a 1D domain and a global resolution with N+1 points
        and a ghost layer with 2 points::

            N = 9
            dom = Box(dimension=1)
            discr = Discretization([N + 1], [2])
            m = Mesh(dom, discr, resol, start)

        will describe a grid on the current process, starting at point
        of index start in the global mesh,
        with a local resolution equal to resol.

        Usually, Mesh creation is an internal process
        leaded by the domain and its topologies.
        Just call something like::

            dom = Box(dimension=1)
            dom.create_topology(...)

        and you'll get all the local meshes.

        For example, with 2 procs on a 1D periodic domain
            discretized with 9 points:

        global grid (node number):        0 1 2 3 4 5 6 7
                                          | | | | | | | |
        proc 0 (global indices):      X X 0 1 2 3 X X | |
               (local indices) :      0 1 2 3 4 5 6 7 | |
        proc 1 (global indices):              X X 4 5 6 7 X X
               (local indices):               0 1 2 3 4 5 6 7
        with 'X' for ghost points.

        - Node '8' of the global grid is not represented on local mesh,
        because of periodicity. N8 == N0, left and right box boundaries have the same values.
        This is not true for non periodic boxes.

        - On proc 1, we have:
           - local resolution = 8
           - global_start = 4
           - 'computation nodes' = 2, 3, 4, 5
           - 'ghost nodes' = 0, 1, 6, 7

        Remarks:
            - all 'global' values refer to the discretization parameter.
              For example 'global start' = 2 means a point of index 2 in the
              global resolution.
            - periodic and non periodic grid are considered but only periodic
              boundary conditions have been implemented yet for most operators.
        """

        # Note : all variables with 'global' prefix are related
        # to the global grid, that is the mesh defined on the whole
        # domain. These variables do not depend on the mpi distribution.
        from hysop.topology.cartesian_topology import CartesianTopology

        check_instance(topology, CartesianTopology)
        check_instance(
            local_resolution,
            (list, tuple, np.ndarray),
            values=(int, np.integer),
            minval=1,
        )
        check_instance(
            global_start, (list, tuple, np.ndarray), values=(int, np.integer), minval=0
        )

        super().__init__(
            topology=topology, topology_state=topology.topology_state, mesh=self, **kwds
        )

        # ---
        # Attributes relative to the global mesh
        # i.e common to all MPI processes on the current topology.
        # ---
        domain = topology.domain
        dim = domain.dim

        discretization = topology._topology._discretization
        assert (discretization.grid_resolution >= 1).all()
        assert (discretization.ghosts >= 0).all()
        ghosts = np.asintegerarray(discretization.ghosts).copy()
        grid_resolution = np.asintegerarray(discretization.grid_resolution).copy()
        periodicity = np.asintegerarray(discretization.periodicity).copy()
        global_resolution = np.asintegerarray(discretization.global_resolution).copy()
        # /!\ now we add one point on each periodic axes because the user give grid_resolution
        #     and not global resolution as it used to be.
        assert all(global_resolution == grid_resolution + periodicity)
        space_step = npw.asrealarray(
            domain.length / (grid_resolution + periodicity - 1)
        )

        topo_global_resolution = npw.asintegerarray(topology.global_resolution).copy()
        topo_grid_resolution = npw.asintegerarray(topology.grid_resolution).copy()
        global_start = npw.asintegerarray(global_start).copy()
        assert all(topo_global_resolution == global_resolution)
        assert all(topo_grid_resolution == grid_resolution)
        assert global_start.size == dim
        assert (global_start >= 0).all()

        # global boundaries and local boundaries
        (global_lboundaries, global_rboundaries) = discretization.boundaries
        del discretization

        # ---
        # Attributes relative to the distributed mesh
        # i.e. the part of the mesh on the current mpi process
        # ---
        local_resolution = npw.asintegerarray(local_resolution)
        assert local_resolution.size == dim
        assert (local_resolution > 2 * ghosts).all()
        assert (local_resolution <= grid_resolution + 2 * ghosts).all()
        compute_resolution = local_resolution - 2 * ghosts

        # deduce all other attributes
        global_stop = global_start + compute_resolution
        global_compute_slices = tuple(
            slice(i, j) for (i, j) in zip(global_start, global_stop)
        )
        global_ghost_slices = []
        for i in range(dim):
            lslices = tuple(
                (
                    slice(None)
                    if j != i
                    else slice(global_start[i] - ghosts[i], global_start[i])
                )
                for j in range(dim)
            )
            rslices = tuple(
                (
                    slice(None)
                    if j != i
                    else slice(global_stop[i], global_stop[i] + ghosts[i])
                )
                for j in range(dim)
            )
            global_ghost_slices.append((lslices, rslices))
        global_ghost_slices = tuple(global_ghost_slices)

        global_origin = domain.origin.copy()
        global_end = domain.end.copy()
        global_length = domain.length.copy()

        # Using space_step here can lead to numerical issues (rounding errors)
        local_origin = domain.origin + domain.length * (
            (global_start - ghosts) / npw.asrealarray(grid_resolution + periodicity - 1)
        )
        local_end = domain.origin + domain.length * (
            (global_stop + ghosts) / npw.asrealarray(grid_resolution + periodicity - 1)
        )
        local_length = local_end - local_origin

        local_start = ghosts
        local_stop = local_resolution - ghosts
        local_compute_slices = tuple(
            slice(i, j) for (i, j) in zip(local_start, local_stop)
        )

        local_indices = tuple(
            np.arange(Ni, dtype=HYSOP_INTEGER) for Ni in local_resolution
        )
        local_coords = tuple(
            npw.asrealarray(
                tuple(
                    local_origin[d] + i * space_step[d]
                    for i in range(local_resolution[d])
                )
            )
            for d in range(dim)
        )
        local_compute_indices = tuple(
            local_indices[d][local_start[d] : local_stop[d]] for d in range(dim)
        )
        local_compute_coords = tuple(
            local_coords[d][local_start[d] : local_stop[d]] for d in range(dim)
        )

        # Is this mesh on the last process in some direction in the
        # mpi grid of process?
        proc_coords = topology.proc_coords
        proc_shape = topology.proc_shape
        is_at_left_boundary = proc_coords == 0
        is_at_right_boundary = proc_coords == proc_shape - 1

        global_lboundaries = global_lboundaries.copy()
        global_rboundaries = global_rboundaries.copy()

        local_lboundaries = np.asarray(
            [
                bc if at_left else BoundaryCondition.NONE
                for (bc, at_left) in zip(global_lboundaries, is_at_left_boundary)
            ]
        )
        local_rboundaries = np.asarray(
            [
                bc if at_right else BoundaryCondition.NONE
                for (bc, at_right) in zip(global_rboundaries, is_at_right_boundary)
            ]
        )

        npw.set_readonly(
            global_resolution,
            grid_resolution,
            global_start,
            global_stop,
            global_origin,
            global_end,
            global_length,
            global_lboundaries,
            global_rboundaries,
            local_resolution,
            local_start,
            local_stop,
            local_lboundaries,
            local_rboundaries,
            local_origin,
            local_end,
            local_length,
            compute_resolution,
            ghosts,
            proc_coords,
            proc_shape,
            is_at_left_boundary,
            is_at_right_boundary,
            space_step,
        )
        npw.set_readonly(*local_indices)
        npw.set_readonly(*local_coords)
        npw.set_readonly(*local_compute_indices)
        npw.set_readonly(*local_compute_coords)

        self._grid_resolution = grid_resolution
        self._global_resolution = global_resolution
        self._global_start = global_start
        self._global_stop = global_stop
        self._global_compute_slices = global_compute_slices
        self._global_ghost_slices = global_ghost_slices
        self._global_origin = global_origin
        self._global_end = global_end
        self._global_length = global_length
        self._global_lboundaries = global_lboundaries
        self._global_rboundaries = global_rboundaries

        self._local_resolution = local_resolution
        self._local_start = local_start
        self._local_stop = local_stop
        self._local_origin = local_origin
        self._local_end = local_end
        self._local_length = local_length
        self._local_lboundaries = local_lboundaries
        self._local_rboundaries = local_rboundaries
        self._local_indices = local_indices
        self._local_coords = local_coords
        self._local_compute_indices = local_compute_indices
        self._local_compute_coords = local_compute_coords

        self._local_compute_slices = local_compute_slices

        self._compute_resolution = compute_resolution
        self._ghosts = ghosts
        self._proc_coords = proc_coords
        self._proc_shape = proc_shape
        self._space_step = space_step
        self._is_at_left_boundary = is_at_left_boundary
        self._is_at_right_boundary = is_at_right_boundary
        self._on_proc = True

        if __debug__:
            self.__check_vars(dim)

    def __check_vars(self, dim):
        # check variables and properties at the same time
        from hysop.topology.cartesian_topology import CartesianTopologyView

        check_instance(self.topology, CartesianTopologyView)
        check_instance(
            self.grid_resolution, np.ndarray, dtype=HYSOP_INTEGER, shape=(dim,)
        )
        check_instance(
            self.global_resolution, np.ndarray, dtype=HYSOP_INTEGER, shape=(dim,)
        )
        check_instance(self.global_start, np.ndarray, dtype=HYSOP_INTEGER, shape=(dim,))
        check_instance(self.global_stop, np.ndarray, dtype=HYSOP_INTEGER, shape=(dim,))
        check_instance(self.global_origin, np.ndarray, dtype=HYSOP_REAL, shape=(dim,))
        check_instance(self.global_end, np.ndarray, dtype=HYSOP_REAL, shape=(dim,))
        check_instance(self.global_length, np.ndarray, dtype=HYSOP_REAL, shape=(dim,))
        check_instance(self.global_compute_slices, tuple, values=slice, size=dim)
        check_instance(self.global_ghost_slices, tuple, values=tuple, size=dim)
        for lg, rg in self.global_ghost_slices:
            check_instance(lg, tuple, values=slice, size=dim)
            check_instance(rg, tuple, values=slice, size=dim)
        check_instance(self.global_boundaries, tuple, values=np.ndarray, size=2)
        for i in range(2):
            check_instance(
                self.global_boundaries[i],
                np.ndarray,
                dtype=object,
                size=dim,
                values=BoundaryCondition,
            )
        assert np.array_equal(self.global_boundaries[0], self.global_lboundaries)
        assert np.array_equal(self.global_boundaries[1], self.global_rboundaries)

        check_instance(
            self.local_resolution, np.ndarray, dtype=HYSOP_INTEGER, shape=(dim,)
        )
        check_instance(self.local_start, np.ndarray, dtype=HYSOP_INTEGER, shape=(dim,))
        check_instance(self.local_stop, np.ndarray, dtype=HYSOP_INTEGER, shape=(dim,))
        check_instance(self.local_origin, np.ndarray, dtype=HYSOP_REAL, shape=(dim,))
        check_instance(self.local_end, np.ndarray, dtype=HYSOP_REAL, shape=(dim,))
        check_instance(self.local_length, np.ndarray, dtype=HYSOP_REAL, shape=(dim,))
        check_instance(self.local_compute_slices, tuple, values=slice, size=dim)
        check_instance(self.local_inner_ghost_slices, tuple, values=tuple, size=dim)
        for lg, rg, sh in self.local_inner_ghost_slices:
            check_instance(lg, tuple, values=slice, size=dim)
            check_instance(rg, tuple, values=slice, size=dim)
            check_instance(
                sh, np.ndarray, dtype=np.int32, shape=(dim,), allow_none=True
            )
        check_instance(self.local_outer_ghost_slices, tuple, values=tuple, size=dim)
        for lg, rg, sh in self.local_outer_ghost_slices:
            check_instance(lg, tuple, values=slice, size=dim)
            check_instance(rg, tuple, values=slice, size=dim)
            check_instance(
                sh, np.ndarray, dtype=np.int32, shape=(dim,), allow_none=True
            )
        check_instance(self.local_boundaries, tuple, values=np.ndarray, size=2)
        for i in range(2):
            check_instance(
                self.local_boundaries[i],
                np.ndarray,
                dtype=object,
                size=dim,
                values=BoundaryCondition,
            )
        assert np.array_equal(self.local_boundaries[0], self.local_lboundaries)
        assert np.array_equal(self.local_boundaries[1], self.local_rboundaries)

        check_instance(self.local_indices, tuple, size=dim, values=np.ndarray)
        check_instance(self.local_mesh_indices, tuple, size=dim, values=np.ndarray)
        check_instance(self.local_compute_indices, tuple, size=dim, values=np.ndarray)
        check_instance(
            self.local_compute_mesh_indices, tuple, size=dim, values=np.ndarray
        )
        check_instance(self.local_coords, tuple, size=dim, values=np.ndarray)
        check_instance(self.local_mesh_coords, tuple, size=dim, values=np.ndarray)
        check_instance(self.local_compute_coords, tuple, size=dim, values=np.ndarray)
        check_instance(
            self.local_compute_mesh_coords, tuple, size=dim, values=np.ndarray
        )
        for i in range(dim):
            check_instance(
                self.local_indices[i],
                np.ndarray,
                dtype=HYSOP_INTEGER,
                ndim=1,
                size=self.local_resolution[i],
            )
            check_instance(
                self.local_mesh_indices[i],
                np.ndarray,
                dtype=HYSOP_INTEGER,
                ndim=dim,
                size=self.local_resolution[i],
            )
            check_instance(
                self.local_compute_indices[i],
                np.ndarray,
                dtype=HYSOP_INTEGER,
                ndim=1,
                size=self.compute_resolution[i],
            )
            check_instance(
                self.local_compute_mesh_indices[i],
                np.ndarray,
                dtype=HYSOP_INTEGER,
                ndim=dim,
                size=self.compute_resolution[i],
            )
            check_instance(
                self.local_coords[i],
                np.ndarray,
                dtype=HYSOP_REAL,
                ndim=1,
                size=self.local_resolution[i],
            )
            check_instance(
                self.local_mesh_coords[i],
                np.ndarray,
                dtype=HYSOP_REAL,
                ndim=dim,
                size=self.local_resolution[dim - 1 - i],
            )
            check_instance(
                self.local_compute_coords[i],
                np.ndarray,
                dtype=HYSOP_REAL,
                ndim=1,
                size=self.compute_resolution[i],
            )
            check_instance(
                self.local_compute_mesh_coords[i],
                np.ndarray,
                dtype=HYSOP_REAL,
                ndim=dim,
                size=self.compute_resolution[dim - 1 - i],
            )

        check_instance(
            self.compute_resolution, np.ndarray, dtype=HYSOP_INTEGER, shape=(dim,)
        )
        check_instance(self.ghosts, np.ndarray, dtype=HYSOP_INTEGER, shape=(dim,))
        check_instance(self.space_step, np.ndarray, dtype=HYSOP_REAL, shape=(dim,))
        check_instance(self.proc_coords, np.ndarray, dtype=HYSOP_INTEGER, shape=(dim,))
        check_instance(self.proc_shape, np.ndarray, dtype=HYSOP_INTEGER, shape=(dim,))

        check_instance(self.is_at_left_boundary, np.ndarray, dtype=bool, shape=(dim,))
        check_instance(self.is_at_right_boundary, np.ndarray, dtype=bool, shape=(dim,))
        check_instance(self.is_at_boundary, np.ndarray, dtype=bool, shape=(dim,))
        check_instance(self.on_proc, bool)

    def view(self, topology_state):
        """Return a view on this mesh using a cartesian topology state."""
        from hysop.topology.cartesian_topology import CartesianTopologyState

        check_instance(topology_state, CartesianTopologyState)
        return CartesianMeshView(mesh=self, topology_state=topology_state)
