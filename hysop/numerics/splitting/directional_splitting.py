# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABCMeta, abstractmethod

from hysop.core.graph.graph import not_initialized
from hysop.core.graph.node_generator import ComputationalGraphNodeGenerator
from hysop.operator.directional.directional import DirectionalOperatorGeneratorI
from hysop.tools.decorators import debug
from hysop.tools.htypes import first_not_None


class DirectionalSplitting(ComputationalGraphNodeGenerator):

    @debug
    def __new__(cls, splitting_dim, extra_kwds, **kargs):
        return super().__new__(
            cls, candidate_input_tensors=None, candidate_output_tensors=None, **kargs
        )

    @debug
    def __init__(self, splitting_dim, extra_kwds, **kargs):
        super().__init__(
            candidate_input_tensors=None, candidate_output_tensors=None, **kargs
        )
        self.splitting_dim = splitting_dim
        self.directional_operators = []
        self.extra_kwds = first_not_None(extra_kwds, {})
        self._generated = False
        self._input_fields_to_dump = []
        self._output_fields_to_dump = []

        supported_dimensions = self.supported_dimensions()
        if not isinstance(supported_dimensions, tuple):
            msg = "{}.supported_dimensions() should return a tuple of ints, got a {}."
            msg = msg.format(self.__class__, supported_dimensions.__class__)
            raise TypeError(msg)
        if splitting_dim not in supported_dimensions:
            msg = "{}D is not supported by this splitting, supported dimensions are {}."
            msg = msg.format(splitting_dim, supported_dimensions)
            raise ValueError(msg)

    @debug
    @not_initialized
    def push_operators(self, *operators):
        directional_operators = self.directional_operators
        mpi_params = None
        for op in operators:
            if op is None:
                pass
            elif isinstance(op, DirectionalOperatorGeneratorI):
                self.candidate_input_tensors.update(op.candidate_input_tensors)
                self.candidate_output_tensors.update(op.candidate_output_tensors)
                directional_operators.append(op)
                try:
                    mpi_params = op.mpi_params
                except AttributeError:
                    if "mpi_params" in op._op_kwds:
                        mpi_params = op._op_kwds["mpi_params"]
            else:
                msg = "Given operator is not a DirectionalOperatorGenerator, got {}."
                msg = msg.format(op.__class__)
                raise TypeError(msg)
        self.mpi_params = mpi_params

    @abstractmethod
    def supported_dimensions(cls):
        pass

    @debug
    def _generate(self):
        self._generated = True

    def dump_inputs(self, **kwds):
        """
        Tell the generated operator to dump some of its inputs before
        apply is called.

        Target folder, file, dump frequency and other io pameters
        are passed trough io_params or as keywords.

        See hysop.core.computational_node.ComputationalGraphNode.dump_inputs().
        """
        msg = "Cannot dump inputs after {} has been generated."
        msg = msg.format(self.name)
        assert self._generated is False, msg
        self._input_fields_to_dump.append(kwds)

    def dump_outputs(self, **kwds):
        """e
        Tell the generated operator to dump some of its outputs after
        apply is called.

        Target folder, file, dump frequency and other io pameters
        are passed trough instance io_params of this parameter or
        as keywords.

        See hysop.core.computational_node.ComputationalGraphNode.dump_outputs().
        """
        msg = "Cannot dump outputs after {} has been generated."
        msg = msg.format(self.name)
        assert self._generated is False, msg
        self._output_fields_to_dump.append(kwds)
