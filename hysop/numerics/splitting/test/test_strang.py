# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop import MPIParams
from hysop.domain.box import Box
from hysop.fields.continuous_field import Field
from hysop.methods import *
from hysop.numerics.splitting.strang import StrangSplitting
from hysop.operators import DirectionalAdvection, PoissonCurl
from hysop.problem import Problem
from hysop.simulation import Simulation
from hysop.tools.parameters import CartesianDiscretization
from hysop.topology.cartesian_topology import CartesianTopology


class TestStrang:

    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def setup_method(self, method):
        pass

    def teardown_method(self, method):
        pass

    def make_fields(self, dim, nscalars):
        # Domain
        box = Box(length=[1.0] * dim, origin=[0.0] * dim)

        ## Fields
        velo = Field(domain=box, name="V", is_vector=True)
        if dim == 2:
            vorti = Field(domain=box, name="W")
        else:
            vorti = Field(domain=box, name="W", is_vector=True)

        scalars = [Field(domain=box, name=f"S{i}") for i in range(nscalars)]

        return box, velo, vorti, tuple(scalars)

    def _test_strang_nd(self, dim, n):
        ## Simulation
        dt0 = 0.1
        simu = Simulation(start=0.0, end=1.0, max_iter=100, dt0=dt0)
        simu.initialize()

        ## Domain and continuous fields
        box, velo, vorti, scalars = self.make_fields(dim, 2)

        ## CartesianDiscretizations and topologies
        resolution = (n,) * dim

        ## Operators
        advec = DirectionalAdvection(
            name="advec",
            dt=simu.dt,
            velocity=velo,
            velocity_cfl=1.0 * dt0 * n,
            advected_fields=(vorti,) + scalars,
            variables={
                velo: resolution,
                vorti: resolution,
                scalars[0]: resolution,
                scalars[1]: resolution,
            },
        )

        poisson = PoissonCurl(
            name="poisson",
            velocity=velo,
            vorticity=vorti,
            variables={velo: resolution, vorti: resolution},
        )

        ## Graph
        splitting = StrangSplitting(
            splitting_dim=dim, order=StrangOrder.STRANG_FIRST_ORDER
        )
        splitting.push_operators(advec)

        problem = Problem()
        problem.insert(splitting)
        problem.insert(poisson)
        problem.build()
        problem.finalize()

    def test_strang_2d(self, n=33):
        self._test_strang_nd(dim=2, n=n)

    def test_strang_3d(self, n=33):
        self._test_strang_nd(dim=3, n=n)


if __name__ == "__main__":
    test = TestStrang()
    test.setup_class()
    test.test_strang_2d()
    test.test_strang_3d()
    test.teardown_class()
