# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.numerics.splitting.directional_splitting import DirectionalSplitting
from hysop.tools.decorators import debug
from hysop.tools.henum import EnumFactory
from hysop.tools.htypes import check_instance, to_tuple

StrangOrder = EnumFactory.create(
    "StrangOrder", ["STRANG_FIRST_ORDER", "STRANG_SECOND_ORDER"]
)


class StrangSplitting(DirectionalSplitting):

    @debug
    def __new__(cls, splitting_dim, order, extra_kwds=None, **kargs):
        return super().__new__(
            cls, splitting_dim=splitting_dim, extra_kwds=extra_kwds, **kargs
        )

    @debug
    def __init__(self, splitting_dim, order, extra_kwds=None, **kargs):
        super().__init__(splitting_dim=splitting_dim, extra_kwds=extra_kwds, **kargs)
        self.spatial_order = order

        check_instance(order, (StrangOrder, int))
        if (order == 1) or (order == StrangOrder.STRANG_FIRST_ORDER):
            order = 1
        elif (order == 2) or (order == StrangOrder.STRANG_SECOND_ORDER):
            order = 2
        else:
            msg = "Unsupported spatial order requested {}."
            msg = msg.format(order)
            raise ValueError(msg)

        directions = tuple(range(splitting_dim))
        if order == 1:
            dt_coeff = (1.0,) * splitting_dim
        elif order == 2:
            dt_coeff = (0.5,) * (2 * splitting_dim)
            directions += tuple(range(splitting_dim - 1, -1, -1))
        self.directions = directions
        self.dt_coeff = dt_coeff

    @classmethod
    def supported_dimensions(cls):
        return tuple(range(1, 17))

    @debug
    def _generate(self):
        splitting_dim = self.splitting_dim

        for op in self.directional_operators:
            op.generate(splitting_dim=splitting_dim, **self.extra_kwds)

        nodes = ()
        for dir_, dt_coeff in zip(self.directions, self.dt_coeff):
            for op in self.directional_operators:
                operators = op.get_direction(dir_, dt_coeff)
                nodes += to_tuple(operators)

        for kwds in self._input_fields_to_dump:
            nodes[0].dump_inputs(**kwds)
        for kwds in self._output_fields_to_dump:
            nodes[-1].dump_outputs(**kwds)

        super()._generate()

        return nodes
