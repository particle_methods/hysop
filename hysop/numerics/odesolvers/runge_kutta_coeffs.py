# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
import gmpy2 as gmp

I = gmp.mpz
Q = gmp.mpq
Itype = I(0).__class__
Qtype = Q(1, 2).__class__

Z = I(0)

# For all i in {0,...,M-1}
# Tni = Tn + alpha_i*dt
# Xni = Xn + dt*sum(j=0,i-1,gamma_ij*Kj)
# Kni = F(Tni,Xni)
# X_n+1 = Xn + dt*sum(i=0,M-1,beta_i*Ki)
rk_params = {}

RK1 = {}
RK1["order"] = 1
RK1["stages"] = 1
RK1["alpha"] = [Z]
RK1["beta"] = [I(1)]
RK1["gamma"] = [[]]
rk_params["RK1"] = RK1
rk_params["Euler"] = RK1

RK2 = {}
RK2["order"] = 2
RK2["stages"] = 2
RK2["alpha"] = [Z, Q(1, 2)]
RK2["beta"] = [Z, I(1)]
RK2["gamma"] = [[Q(1, 2)]]
rk_params["RK2"] = RK2

RK2_Heun = {}
RK2_Heun["order"] = 2
RK2_Heun["stages"] = 2
RK2_Heun["alpha"] = [Z, I(1)]
RK2_Heun["beta"] = [Q(1, 2), Q(1, 2)]
RK2_Heun["gamma"] = [[I(1)]]
rk_params["RK2_Heun"] = RK2_Heun

RK2_Ralston = {}
RK2_Ralston["order"] = 2
RK2_Ralston["stages"] = 2
RK2_Ralston["alpha"] = [Z, Q(2, 3)]
RK2_Ralston["beta"] = [Q(1, 4), Q(3, 4)]
RK2_Ralston["gamma"] = [[Q(2, 3)]]
rk_params["RK2_Ralston"] = RK2_Ralston

RK3 = {}
RK3["order"] = 3
RK3["stages"] = 3
RK3["alpha"] = [Z, Q(1, 2), I(1)]
RK3["beta"] = [Q(1, 6), Q(2, 3), Q(1, 6)]
RK3["gamma"] = [[Q(1, 2), Z], [I(-1), I(2)]]
rk_params["RK3"] = RK3

RK4 = {}
RK4["order"] = 4
RK4["stages"] = 4
RK4["alpha"] = [Z, Q(1, 2), Q(1, 2), I(1)]
RK4["beta"] = [Q(1, 6), Q(1, 3), Q(1, 3), Q(1, 6)]
RK4["gamma"] = [[Q(1, 2), Z, Z], [Z, Q(1, 2), Z], [Z, Z, I(1)]]
rk_params["RK4"] = RK4

RK4_38 = {}
RK4_38["order"] = 4
RK4_38["stages"] = 4
RK4_38["alpha"] = [Z, Q(1, 3), Q(2, 3), I(1)]
RK4_38["beta"] = [Q(1, 8), Q(3, 8), Q(3, 8), Q(1, 8)]
RK4_38["gamma"] = [[Q(+1, 3), Z, Z], [Q(-1, 3), I(+1), Z], [I(1), I(-1), I(1)]]
rk_params["RK4_38"] = RK4_38


# check and wrap in numpy arrays
for m in rk_params:
    C = rk_params[m]
    S = C["stages"]

    alpha = np.asarray(C["alpha"], dtype=object)
    beta = np.asarray(C["beta"], dtype=object)
    gamma = np.asarray(C["gamma"], dtype=object)

    assert alpha.shape == (S,)
    assert beta.shape == (S,)
    if S > 1:
        assert gamma.shape == (
            S - 1,
            S - 1,
        )
    else:
        assert gamma.size == 0

    C["alpha"] = alpha
    C["beta"] = beta
    C["gamma"] = gamma

    rk_params[m] = C


def available_methods():
    return rk_params.keys()
