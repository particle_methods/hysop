# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


r"""
FFT interface for fast Fourier Transforms using Intel MKL (numpy interface).
:class:`~hysop.numerics.MklFFT`
:class:`~hysop.numerics.MklFFTPlan`

/!\ -- OPENMP CONFLICT WITH GRAPHTOOLS --
/!\ Only works if MKL_THREADING_LAYER is set to OMP if some
    dependencies are compiled against GNU OpenMP.
/!\ May also work with MKL_THREADING_LAYER=TBB and SEQUENCIAL but not INTEL.

Required version of mkl_fft is: https://gitlab.com/keckj/mkl_fft
If MKL_THREADING_LAYER is not set, or is set to INTEL, FFT tests will fail.
"""

import functools, warnings
import numpy as np
import numba as nb
from mkl_fft import (
    ifft as mkl_ifft,
    fft as mkl_fft,
    rfft_numpy as mkl_rfft,
    irfft_numpy as mkl_irfft,
)

from hysop.numerics.fft.host_fft import HostFFTPlanI, HostFFTI, HostArray
from hysop.numerics.fft.fft import (
    complex_to_float_dtype,
    float_to_complex_dtype,
    mk_view,
    mk_shape,
    simd_alignment,
)

from hysop import __DEFAULT_NUMBA_TARGET__
from hysop.numerics.fft.fft import HysopFFTWarning, bytes2str
from hysop.tools.numba_utils import make_numba_signature, prange
from hysop.tools.warning import HysopWarning
from hysop.tools.numerics import get_itemsize
from hysop.tools.htypes import first_not_None


class HysopMKLFftWarning(HysopWarning):
    pass


def setup_transform(x, axis, transform, inverse, type):
    shape = x.shape
    dtype = x.dtype
    N = shape[axis]
    if inverse:
        type = [1, 3, 2, 4][type - 1]
    if transform == "dct":
        ctype = float_to_complex_dtype(x.dtype)
        if type == 1:
            sin = mk_shape(shape, axis, 2 * N - 2)
            din = dtype
            sout = mk_shape(shape, axis, N)
            dout = ctype
        elif type == 2:
            sin = mk_shape(shape, axis, N)
            din = dtype
            sout = mk_shape(shape, axis, N // 2 + 1)
            dout = ctype
        elif type == 3:
            sin = mk_shape(shape, axis, N // 2 + 1)
            din = ctype
            sout = mk_shape(shape, axis, N)
            dout = dtype
        else:
            raise NotImplementedError
    elif transform == "dst":
        ctype = float_to_complex_dtype(x.dtype)
        if type == 1:
            sin = mk_shape(shape, axis, 2 * N + 2)
            din = dtype
            sout = mk_shape(shape, axis, N + 2)
            dout = ctype
        elif type == 2:
            sin = mk_shape(shape, axis, N)
            din = dtype
            sout = mk_shape(shape, axis, N // 2 + 1)
            dout = ctype
        elif type == 3:
            sin = mk_shape(shape, axis, N // 2 + 1)
            din = ctype
            sout = mk_shape(shape, axis, N)
            dout = dtype
        else:
            raise NotImplementedError
    else:
        sin = None
        din = None
        sout = None
        dout = None

    return (sin, din, sout, dout)


def fft(out=None, **kwds):
    if out is None:
        out = mkl_fft(**kwds)
    else:
        out[...] = mkl_fft(**kwds)
    return out


def ifft(out=None, **kwds):
    if out is None:
        out = mkl_ifft(**kwds)
    else:
        out[...] = mkl_ifft(**kwds)
    return out


def rfft(out=None, **kwds):
    if out is None:
        out = mkl_rfft(**kwds)
    else:
        out[...] = mkl_rfft(**kwds)
    return out


def irfft(out=None, **kwds):
    if out is None:
        out = mkl_irfft(**kwds)
    else:
        out[...] = mkl_irfft(**kwds)
    return out


def dct(x, out=None, type=2, axis=-1, input_tmp=None, output_tmp=None):
    ndim = x.ndim
    shape = x.shape
    N = x.shape[axis]
    (sin, din, sout, dout) = setup_transform(x, axis, "dct", False, type)
    if type == 1:
        # O(sqrt(log(N))) error, O(2N) complexity, O(4*N) memory
        if input_tmp is None:
            input_tmp = np.empty(shape=sin, dtype=din)
        if output_tmp is None:
            output_tmp = np.empty(shape=sout, dtype=dout)
        assert input_tmp.shape == sin
        assert input_tmp.dtype == din
        assert output_tmp.shape == sout
        assert output_tmp.dtype == dout
        slc0 = mk_view(ndim, axis, 1, -1)
        slc1 = mk_view(ndim, axis, None, None, -1)
        np.concatenate((x, x[slc0][slc1]), axis=axis, out=input_tmp)
        rfft(x=input_tmp, out=output_tmp, axis=axis)
        res = output_tmp.real
        if out is None:
            out = res
        else:
            assert out.shape == res.shape
            assert out.dtype == res.dtype
            out[...] = res
    elif type == 2:
        # O(sqrt(log(N))) error, O(N) complexity, O(3N) memory
        n0 = N // 2 + 1
        n1 = (N - 1) // 2 + 1
        slc0 = mk_view(ndim, axis, 1, None, None)
        slc1 = mk_view(ndim, axis, None, None, +2)
        slc2 = mk_view(ndim, axis, 1, None, +2)
        slc3 = mk_view(ndim, axis, None, None, -1)
        slc4 = mk_view(ndim, axis, None, None, None, default=None)
        slc5 = mk_view(ndim, axis, None, n1, None)
        slc6 = mk_view(ndim, axis, n1, None, None)

        if input_tmp is None:
            input_tmp = np.empty(shape=sin, dtype=din)
        if output_tmp is None:
            output_tmp = np.empty(shape=sout, dtype=dout)
        assert input_tmp.shape == sin
        assert input_tmp.dtype == din
        assert output_tmp.shape == sout
        assert output_tmp.dtype == dout

        np.concatenate((x[slc1], x[slc2][slc3]), axis=axis, out=input_tmp)
        rfft(x=input_tmp, out=output_tmp, axis=axis)
        assert output_tmp.shape == sout
        assert output_tmp.dtype == dout
        output_tmp *= (2 * np.exp(-1j * np.pi * np.arange(n0) / (2 * N)))[slc4]

        if out is None:
            out = np.empty_like(x)
        else:
            assert out.shape == x.shape
            assert out.dtype == x.dtype
        out[slc5] = +output_tmp.real[slc5]
        out[slc6] = -output_tmp.imag[slc0][slc3]
    elif type == 3:
        # O(sqrt(log(N))) error, O(N) complexity, O(3N) memory
        n0 = N // 2 + 1
        n1 = (N - 1) // 2 + 1
        slc0 = mk_view(ndim, axis, None, n0, None)
        slc1 = mk_view(ndim, axis, 1, None, None)
        slc2 = mk_view(ndim, axis, n1, None, None)
        slc3 = mk_view(ndim, axis, None, None, -1)
        slc4 = mk_view(ndim, axis, None, None, +2)
        slc5 = mk_view(ndim, axis, None, n1, None)
        slc6 = mk_view(ndim, axis, 1, None, +2)
        slc7 = mk_view(ndim, axis, None, None, None, default=None)
        slc8 = mk_view(ndim, axis, n0, None, None)
        slc9 = mk_view(ndim, axis, None, 1, None)

        if input_tmp is None:
            input_tmp = np.empty(shape=sin, dtype=din)
        if output_tmp is None:
            output_tmp = np.empty(shape=sout, dtype=dout)
        assert input_tmp.shape == sin
        assert input_tmp.dtype == din
        assert output_tmp.shape == sout
        assert output_tmp.dtype == dout
        input_tmp.real[slc0] = +x[slc0]
        input_tmp.real[slc8] = 0.0
        input_tmp.imag[slc1] = -x[slc2][slc3]
        input_tmp.imag[slc9] = 0.0
        input_tmp *= np.exp(+1j * np.pi * np.arange(n0) / (2 * N))[slc7]
        output_tmp = irfft(x=input_tmp, out=output_tmp, axis=axis, n=N)
        output_tmp *= N

        if out is None:
            out = np.empty_like(x)
        else:
            assert out.shape == x.shape
            assert out.dtype == x.dtype
        out[slc4] = output_tmp[slc5]
        out[slc6] = output_tmp[slc2][slc3]
    else:
        stypes = ["I", "II", "III", "IV", "V", "VI", "VII", "VIII"]
        msg = "DCT-{} has not been implemented yet."
        msg = msg.format(stypes[type - 1])
        raise NotImplementedError(msg)
    return out


def dst(x, out=None, type=2, axis=-1, input_tmp=None, output_tmp=None):
    (sin, din, sout, dout) = setup_transform(x, axis, "dst", False, type)
    ndim = x.ndim
    shape = x.shape
    N = x.shape[axis]
    if type == 1:
        # O(sqrt(log(N))) error, O(2N) complexity, O(4*N) memory
        slc0 = mk_view(ndim, axis, None, None, -1)
        slc1 = mk_view(ndim, axis, 1, -1, None)
        s1 = mk_shape(shape, axis, 1)
        if input_tmp is None:
            input_tmp = np.empty(shape=sin, dtype=din)
        if output_tmp is None:
            output_tmp = np.empty(shape=sout, dtype=dout)
        assert input_tmp.shape == sin
        assert input_tmp.dtype == din
        assert output_tmp.shape == sout
        assert output_tmp.dtype == dout

        Z = np.zeros(shape=s1, dtype=x.dtype)
        np.concatenate((Z, -x, Z, x[slc0]), axis=axis, out=input_tmp)
        rfft(x=input_tmp, out=output_tmp, axis=axis)
        res = output_tmp.imag
        if out is None:
            out = np.empty_like(x)
        else:
            assert out.shape == x.shape
            assert out.dtype == x.dtype
        out[...] = res[slc1]
    elif type == 2:
        # O(sqrt(log(N))) error, O(N) complexity, O(3N) memory
        n0 = N // 2 + 1
        n1 = (N - 1) // 2 + 1
        slc0 = mk_view(ndim, axis, 1, None, None)
        slc1 = mk_view(ndim, axis, None, None, +2)
        slc2 = mk_view(ndim, axis, 1, None, +2)
        slc3 = mk_view(ndim, axis, None, None, -1)
        slc4 = mk_view(ndim, axis, None, None, None, default=None)
        slc5 = mk_view(ndim, axis, None, n1 - 1, None)
        slc6 = mk_view(ndim, axis, n1 - 1, None, None)
        slc7 = mk_view(ndim, axis, 1, n1, None)

        if input_tmp is None:
            input_tmp = np.empty(shape=sin, dtype=din)
        if output_tmp is None:
            output_tmp = np.empty(shape=sout, dtype=dout)
        assert input_tmp.shape == sin
        assert input_tmp.dtype == din
        assert output_tmp.shape == sout
        assert output_tmp.dtype == dout

        np.concatenate((x[slc1], -x[slc2][slc3]), axis=axis, out=input_tmp)
        rfft(x=input_tmp, out=output_tmp, axis=axis)
        output_tmp *= (2 * np.exp(-1j * np.pi * np.arange(n0) / (2 * N)))[slc4]

        if out is None:
            out = np.empty_like(x)
        else:
            assert out.shape == x.shape
            assert out.dtype == x.dtype
        out[slc5] = -output_tmp.imag[slc7]
        out[slc6] = +output_tmp.real[slc3]
    elif type == 3:
        # O(sqrt(log(N))) error, O(N) complexity, O(3N) memory
        ctype = float_to_complex_dtype(x.dtype)
        n0 = N // 2 + 1
        n1 = (N - 1) // 2 + 1
        slc0 = mk_view(ndim, axis, None, n0, None)
        slc1 = mk_view(ndim, axis, None, None, -1)
        slc2 = mk_view(ndim, axis, 1, None, None)
        slc3 = mk_view(ndim, axis, None, N - n1, None)
        slc4 = mk_view(ndim, axis, None, None, None, default=None)
        slc5 = mk_view(ndim, axis, None, None, 2)
        slc6 = mk_view(ndim, axis, None, n1, None)
        slc7 = mk_view(ndim, axis, 1, None, 2)
        slc8 = mk_view(ndim, axis, n1, None, None)
        slc9 = mk_view(ndim, axis, n0, None, None)
        slc10 = mk_view(ndim, axis, 0, 1, None)
        s0 = mk_shape(shape, axis, n0)

        if input_tmp is None:
            input_tmp = np.empty(shape=sin, dtype=din)
        if output_tmp is None:
            output_tmp = np.empty(shape=sout, dtype=dout)
        assert input_tmp.shape == sin
        assert input_tmp.dtype == din
        assert output_tmp.shape == sout
        assert output_tmp.dtype == dout

        input_tmp.real[slc0] = +x[slc1][slc0]
        input_tmp.real[slc9] = 0.0
        input_tmp.imag[slc2] = -x[slc3]
        input_tmp.imag[slc10] = 0.0
        input_tmp *= np.exp(+1j * np.pi * np.arange(n0) / (2 * N))[slc4]
        irfft(x=input_tmp, out=output_tmp, axis=axis, n=N)
        output_tmp[...] *= N

        if out is None:
            out = np.empty_like(x)
        else:
            assert out.shape == x.shape
            assert out.dtype == x.dtype
        out[slc5] = +output_tmp[slc6]
        out[slc7] = -output_tmp[slc8][slc1]
    else:
        stypes = ["I", "II", "III", "IV", "V", "VI", "VII", "VIII"]
        msg = "DCT-{} has not been implemented yet."
        msg = msg.format(stypes[type - 1])
        raise NotImplementedError(msg)
    return out


def idct(x, out=None, type=2, axis=-1, **kwds):
    itype = [1, 3, 2, 4][type - 1]
    return dct(x=x, out=out, type=itype, axis=axis, **kwds)


def idst(x, out=None, type=2, axis=-1, **kwds):
    itype = [1, 3, 2, 4][type - 1]
    return dst(x=x, out=out, type=itype, axis=axis, **kwds)


class MklFFTPlan(HostFFTPlanI):
    """
    Wrap a mkl fft call (mkl.fft does not offer real planning capabilities).
    """

    def __init__(self, planner, fn, a, out, axis, scaling=None, **kwds):
        super().__init__()

        self.planner = planner
        self.fn = fn
        self.a = a
        self.out = out
        self.scaling = scaling

        (sin, din, sout, dout) = setup_transform(
            a,
            axis,
            "dct" if fn in (dct, idct) else "dst" if fn in (dst, idst) else None,
            fn in (idct, idst),
            kwds.get("type", None),
        )

        if sin is None:
            self._required_input_tmp = None
        else:
            self._required_input_tmp = {
                "size": np.prod(sin, dtype=np.int64),
                "shape": sin,
                "dtype": din,
            }

        if sout is None:
            self._required_output_tmp = None
        else:
            self._required_output_tmp = {
                "size": np.prod(sout, dtype=np.int64),
                "shape": sout,
                "dtype": dout,
            }

        self._allocated = False

        if isinstance(a, HostArray):
            a = a.handle
        if isinstance(out, HostArray):
            out = out.handle

        self.rescale = self.bake_scaling_plan(out, scaling)

        kwds = kwds.copy()
        kwds["x"] = a
        kwds["out"] = out
        kwds["axis"] = axis
        self.kwds = kwds

    def bake_scaling_plan(self, x, scaling):
        if scaling is None:

            def _rescale():
                pass

            return _rescale
        target = __DEFAULT_NUMBA_TARGET__
        signature, layout = make_numba_signature(x, scaling)
        if x.ndim == 1:

            @nb.guvectorize(
                [signature], layout, target=target, nopython=True, cache=True
            )
            def scale(x, scaling):
                for i in range(0, x.shape[0]):
                    x[i] *= scaling

        elif x.ndim == 2:

            @nb.guvectorize(
                [signature], layout, target=target, nopython=True, cache=True
            )
            def scale(x, scaling):
                for i in prange(0, x.shape[0]):
                    for j in range(0, x.shape[1]):
                        x[i, j] *= scaling

        elif x.ndim == 3:

            @nb.guvectorize(
                [signature], layout, target=target, nopython=True, cache=True
            )
            def scale(x, scaling):
                for i in prange(0, x.shape[0]):
                    for j in prange(0, x.shape[1]):
                        for k in range(0, x.shape[2]):
                            x[i, j, k] *= scaling

        elif x.ndim == 4:

            @nb.guvectorize(
                [signature], layout, target=target, nopython=True, cache=True
            )
            def scale(x, scaling):
                for i in prange(0, x.shape[0]):
                    for j in prange(0, x.shape[1]):
                        for k in prange(0, x.shape[2]):
                            for l in range(0, x.shape[3]):
                                x[i, j, k, l] *= scaling

        else:
            raise NotImplementedError(x.ndim)

        def _rescale(scale=scale, x=x, scaling=scaling):
            scale(x, scaling)

        return _rescale

    @property
    def input_array(self):
        return self.a

    @property
    def output_array(self):
        return self.out

    def execute(self):
        if not self._allocated:
            self.allocate()
        self.fn(**self.kwds)
        self.rescale()

    @property
    def required_buffer_size(self):
        alignment = simd_alignment
        if self._required_input_tmp:
            sin, din = (
                self._required_input_tmp["size"],
                self._required_input_tmp["dtype"],
            )
            sin *= get_itemsize(din)
            Bin = ((sin + alignment - 1) // alignment) * alignment
        else:
            Bin = 0
        if self._required_output_tmp:
            sout, dout = (
                self._required_output_tmp["size"],
                self._required_output_tmp["dtype"],
            )
            sout *= get_itemsize(dout)
            Bout = ((sout + alignment - 1) // alignment) * alignment
        else:
            Bout = 0
        return Bin + Bout

    def allocate(self, buf=None):
        """Allocate plan extra memory, possibly with a custom buffer."""
        if self._allocated:
            msg = "Plan was already allocated."
            raise RuntimeError(msg)

        if buf is not None:
            alignment = simd_alignment
            if self._required_input_tmp:
                sin, din, ssin = (
                    self._required_input_tmp["size"],
                    self._required_input_tmp["dtype"],
                    self._required_input_tmp["shape"],
                )
                sin *= get_itemsize(din)
                Bin = ((sin + alignment - 1) // alignment) * alignment
            else:
                Bin = 0
            if self._required_output_tmp:
                sout, dout, ssout = (
                    self._required_output_tmp["size"],
                    self._required_output_tmp["dtype"],
                    self._required_output_tmp["shape"],
                )
                sout *= get_itemsize(dout)
                Bout = ((sout + alignment - 1) // alignment) * alignment
            else:
                Bout = 0
            assert buf.dtype.itemsize == 1
            assert buf.size == Bin + Bout
            input_buf = buf[:sin].view(dtype=din).reshape(ssin) if Bin else None
            output_buf = (
                buf[Bin : Bin + sout].view(dtype=dout).reshape(ssout) if Bout else None
            )
        else:
            input_buf = None
            output_buf = None

        for k, buf, required_tmp in zip(
            ("input", "output"),
            (input_buf, output_buf),
            (self._required_input_tmp, self._required_output_tmp),
        ):
            if required_tmp is None:
                assert buf is None
                continue
            size = required_tmp["size"]
            shape = required_tmp["shape"]
            dtype = required_tmp["dtype"]
            if size > 0:
                if buf is None:
                    if (
                        self.planner.warn_on_allocation
                        or self.planner.error_on_allocation
                    ):
                        msg = "Allocating temporary buffer of size {} for clFFT::{}."
                        msg = msg.format(bytes2str(size), id(self))
                        if self.planner.error_on_allocation:
                            raise RuntimeError(msg)
                        else:
                            warnings.warn(msg, HysopMKLFftWarning)
                    buf = self.planner.backend.empty(shape=shape, dtype=dtype)
                elif (buf.shape != shape) or (buf.dtype != dtype):
                    msg = "Buffer does not match required shape: {} != {}"
                    msg = msg.format(buf.shape, shape)
                    msg = "Buffer does not match required dtype: {} != {}"
                    msg = msg.format(buf.dtype, dtype)
                    raise ValueError(msg)
            if isinstance(buf, HostArray):
                buf = buf.handle
            setattr(self, f"{k}_tmp_buffer", buf)
            if buf is not None:
                self.kwds[f"{k}_tmp"] = buf
        self._allocated = True
        return self


class MklFFT(HostFFTI):
    """
    Interface to compute local to process FFT-like transforms using the mkl fft backend.

    Mkl fft backend has some disadvantages:
      - creates intermediate temporary buffers at each call (out and tmp for real-to-real transforms)
      - no planning capabilities (mkl.fft methods are just wrapped into fake plans)
    """

    def __init__(
        self,
        backend=None,
        allocator=None,
        warn_on_allocation=True,
        error_on_allocation=False,
        destroy_input=None,
        **kwds,
    ):
        super().__init__(
            backend=backend,
            allocator=allocator,
            warn_on_allocation=warn_on_allocation,
            error_on_allocation=error_on_allocation,
            **kwds,
        )
        self.supported_ftypes = (
            np.float32,
            np.float64,
        )
        self.supported_ctypes = (
            np.complex64,
            np.complex128,
        )

    def fft(self, a, out=None, axis=-1, **kwds):
        (shape, dtype) = super().fft(a=a, out=out, axis=axis, **kwds)
        out = self.allocate_output(out, shape, dtype)
        plan = MklFFTPlan(self, fn=fft, a=a, out=out, axis=axis, **kwds)
        return plan

    def ifft(self, a, out=None, axis=-1, **kwds):
        (shape, dtype, s) = super().ifft(a=a, out=out, axis=axis, **kwds)
        out = self.allocate_output(out, shape, dtype)
        plan = MklFFTPlan(self, fn=ifft, a=a, out=out, axis=axis, **kwds)
        return plan

    def rfft(self, a, out=None, axis=-1, **kwds):
        (shape, dtype) = super().rfft(a=a, out=out, axis=axis, **kwds)
        out = self.allocate_output(out, shape, dtype)
        plan = MklFFTPlan(self, fn=rfft, a=a, out=out, axis=axis, **kwds)
        return plan

    def irfft(self, a, out=None, n=None, axis=-1, **kwds):
        (shape, dtype, s) = super().irfft(a=a, out=out, n=n, axis=axis, **kwds)
        out = self.allocate_output(out, shape, dtype)
        plan = MklFFTPlan(
            self, fn=irfft, a=a, out=out, axis=axis, n=shape[axis], **kwds
        )
        return plan

    def dct(self, a, out=None, type=2, axis=-1, **kwds):
        (shape, dtype) = super().dct(a=a, out=out, type=type, axis=axis, **kwds)
        out = self.allocate_output(out, shape, dtype)
        plan = MklFFTPlan(self, fn=dct, a=a, out=out, axis=axis, type=type, **kwds)
        return plan

    def idct(self, a, out=None, type=2, axis=-1, scaling=None, **kwds):
        (shape, dtype, _, s) = super().idct(a=a, out=out, type=type, axis=axis, **kwds)
        out = self.allocate_output(out, shape, dtype)
        plan = MklFFTPlan(
            self,
            fn=idct,
            a=a,
            out=out,
            axis=axis,
            type=type,
            scaling=first_not_None(scaling, 1.0 / s),
            **kwds,
        )
        return plan

    def dst(self, a, out=None, type=2, axis=-1, **kwds):
        (shape, dtype) = super().dst(a=a, out=out, type=type, axis=axis, **kwds)
        out = self.allocate_output(out, shape, dtype)
        plan = MklFFTPlan(self, fn=dst, a=a, out=out, axis=axis, type=type, **kwds)
        return plan

    def idst(self, a, out=None, type=2, axis=-1, scaling=None, **kwds):
        (shape, dtype, _, s) = super().idst(a=a, out=out, type=type, axis=axis, **kwds)
        out = self.allocate_output(out, shape, dtype)
        plan = MklFFTPlan(
            self,
            fn=idst,
            a=a,
            out=out,
            axis=axis,
            type=type,
            scaling=first_not_None(scaling, 1.0 / s),
            **kwds,
        )
        return plan
