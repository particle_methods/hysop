# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Test of fields defined with an analytic formula.
"""
import random, pyfftw

import numpy as np
import sympy as sm
import itertools as it

from hysop.constants import Implementation, HYSOP_REAL
from hysop.testsenv import __ENABLE_LONG_TESTS__, __HAS_OPENCL_BACKEND__
from hysop.testsenv import opencl_failed, iter_clenv
from hysop.tools.contexts import printoptions
from hysop.tools.numerics import float_to_complex_dtype
from hysop.tools.htypes import check_instance, first_not_None

from hysop.numerics.fft.fft import mk_shape, HysopFFTDataLayoutError
from hysop.numerics.fft.numpy_fft import NumpyFFT
from hysop.numerics.fft.scipy_fft import ScipyFFT
from hysop.numerics.fft.fftw_fft import FftwFFT

try:
    from hysop.numerics.fft.gpyfft_fft import GpyFFT

    HAS_GPYFFT = True
except ImportError:
    HAS_GPYFFT = False
try:
    from hysop.numerics.fft._mkl_fft import MklFFT

    HAS_MKLFFT = True
except ImportError:
    HAS_MKLFFT = False

raise_on_failure = False


class TestFFT:

    implementations = {
        Implementation.PYTHON: {
            "numpy": NumpyFFT(warn_on_allocation=False),
            "scipy": ScipyFFT(warn_on_allocation=False),
            "fftw": FftwFFT(warn_on_allocation=False, warn_on_misalignment=False),
        },
        Implementation.OPENCL: {},
    }

    if HAS_MKLFFT:
        implementations[Implementation.PYTHON]["mkl"] = MklFFT(warn_on_allocation=False)

    print()
    print(":: STARTING FFT BACKEND TESTS ::")
    if HAS_GPYFFT:
        for i, cl_env in enumerate(iter_clenv()):
            print(f"> Registering opencl backend {i} as:\n{cl_env}")
            print()
            name = f"clfft{i}"
            implementations[Implementation.OPENCL][name] = GpyFFT(
                cl_env=cl_env,
                warn_on_allocation=False,
                warn_on_unaligned_output_offset=False,
            )

    msg_shape = "Expected output array shape to be {} but got {} for implementation {}."
    msg_dtype = "Expected output array dtype to be {} but got {} for implementation {}."
    msg_input_modified = "Input array has been modified for implementation {}."
    msg_output_modified = (
        "Output array results are not consistent for implementation {}."
    )

    report_eps = 10
    fail_eps = 100

    stop_on_error = True

    def _test_1d(self, dtype, failures):
        print()
        print(f"::Testing 1D transform, precision {dtype.__name__}::")
        eps = np.finfo(dtype).eps
        ctype = float_to_complex_dtype(dtype)

        def check_distances(results, eps, report_eps, tag, failures):
            if len(results.keys()) == 0:
                print("no support")
                return
            elif len(results.keys()) == 1:
                impl = next(iter(results.keys()))
                print("cannot compare")
                return
            ss = ()
            for r0, r1 in it.combinations(results.keys(), 2):
                E0 = results[r0]
                E1 = results[r1]
                if isinstance(E0, HysopFFTDataLayoutError) or isinstance(
                    E1, HysopFFTDataLayoutError
                ):
                    s = f"|{r0}-{r1}|=N.A."
                    failed = False
                elif not (E0.shape == E1.shape):
                    print()
                    msg = "Output shapes do not match."
                    raise RuntimeError(msg)
                else:
                    E = results[r1] - results[r0]
                    Einf = np.max(np.abs(E))
                    if np.isfinite(Einf):
                        Eeps = int(np.round(Einf / eps))
                        s = f"|{r0}-{r1}|={Eeps}eps"
                        failed = Eeps > report_eps
                    else:
                        Eeps = Einf
                        s = f"|{r0}-{r1}|={str(Eeps).upper()}"
                        failed = True
                ss += (s,)
                if failed:
                    shape = results[r0].shape
                    failures.setdefault(tag, []).append((r0, r1, shape, Einf, Eeps))
            print(", ".join(ss))
            if failed and raise_on_failure:
                raise RuntimeError

        print("\n FORWARD C2C: complex to complex forward transform")
        for shape, cshape, rshape, N, Nc, Nr, ghosts, mk_buffer in self.iter_shapes():
            print(
                "   FFT shape={:12s} ghosts={:12s} ".format(
                    str(shape), str(ghosts) + ":"
                ),
                end=" ",
            )
            Href = np.random.rand(2 * N).astype(dtype).view(dtype=ctype).reshape(shape)
            results = {}
            for kind, implementations in self.implementations.items():
                for name, impl in implementations.items():
                    if dtype not in impl.supported_ftypes:
                        continue
                    try:
                        Bin = mk_buffer(backend=impl.backend, shape=shape, dtype=ctype)
                        plan = impl.fft(a=Bin).setup()
                        Bout = plan.output_array
                        assert Bout.shape == shape, self.msg_shape.format(
                            shape, Bout.shape, name
                        )
                        assert Bout.dtype == ctype, self.msg_dtype.format(
                            ctype, Bout.dtype, name
                        )
                        Bin[...] = Href
                        evt = plan.execute()
                        H0 = Bin.get()
                        H1 = Bout.get()
                        assert np.array_equal(Href, H0), self.msg_input_modified.format(
                            name
                        )
                        Bout[...] = 0
                        evt = plan.execute()
                        assert plan.output_array is Bout
                        H2 = Bout.get()
                        assert np.allclose(
                            H1, H2, atol=eps
                        ), self.msg_output_modified.format(name)
                        results[name] = H1 / shape[-1]  # forward normalization
                    except HysopFFTDataLayoutError as e:
                        results[name] = e
            check_distances(results, eps, self.report_eps, "forward C2C", failures)

        print("\n BACKWARD C2C: complex to complex backward transform")
        for shape, cshape, rshape, N, Nc, Nr, ghosts, mk_buffer in self.iter_shapes():
            print(
                "   IFFT shape={:12s} ghosts={:12s} ".format(
                    str(shape), str(ghosts) + ":"
                ),
                end=" ",
            )
            Href = np.random.rand(2 * N).astype(dtype).view(dtype=ctype).reshape(shape)
            results = {}
            for kind, implementations in self.implementations.items():
                for name, impl in implementations.items():
                    if dtype not in impl.supported_ftypes:
                        continue
                    try:
                        Bin = mk_buffer(backend=impl.backend, shape=shape, dtype=ctype)
                        plan = impl.ifft(a=Bin).setup()
                        Bout = plan.output_array
                        assert Bout.shape == shape, self.msg_shape.format(
                            shape, Bout.shape, name
                        )
                        assert Bout.dtype == ctype, self.msg_dtype.format(
                            ctype, Bout.dtype, name
                        )
                        Bin[...] = Href
                        plan.execute()
                        H0 = Bin.get()
                        H1 = Bout.get()
                        assert np.array_equal(Href, H0), self.msg_input_modified.format(
                            name
                        )
                        Bout[...] = 0
                        evt = plan.execute()
                        assert plan.output_array is Bout
                        H2 = Bout.get()
                        assert np.allclose(
                            H1, H2, atol=eps
                        ), self.msg_output_modified.format(name)
                        results[name] = H1 / shape[-1]  # forward normalization
                    except HysopFFTDataLayoutError as e:
                        results[name] = e
            check_distances(results, eps, self.report_eps, "backward C2C", failures)

        print("\n FORWARD R2C: real to hermitian complex transform")
        for shape, cshape, rshape, N, Nc, Nr, ghosts, mk_buffer in self.iter_shapes():
            print(
                "   RFFT shape={:12s} ghosts={:12s} ".format(
                    str(shape), str(ghosts) + ":"
                ),
                end=" ",
            )
            Href = np.random.rand(*shape).astype(dtype).reshape(shape)
            results = {}
            for kind, implementations in self.implementations.items():
                for name, impl in implementations.items():
                    if dtype not in impl.supported_ftypes:
                        continue
                    try:
                        Bin = mk_buffer(backend=impl.backend, shape=shape, dtype=dtype)
                        plan = impl.rfft(a=Bin).setup()
                        Bout = plan.output_array
                        assert Bout.shape == cshape, self.msg_shape.format(
                            cshape, Bout.shape, name
                        )
                        assert Bout.dtype == ctype, self.msg_dtype.format(
                            ctype, Bout.dtype, name
                        )
                        Bin[...] = Href
                        plan.execute()
                        H0 = Bin.get()
                        H1 = Bout.get()
                        assert np.array_equal(Href, H0), self.msg_input_modified.format(
                            name
                        )
                        Bout[...] = 0
                        evt = plan.execute()
                        assert plan.output_array is Bout
                        H2 = Bout.get()
                        assert np.allclose(
                            H1, H2, atol=eps
                        ), self.msg_output_modified.format(name)
                        results[name] = H1 / shape[-1]  # forward normalization
                    except HysopFFTDataLayoutError as e:
                        results[name] = e
            check_distances(results, eps, self.report_eps, "R2C", failures)

        print("\n BACKWARD C2R: real to hermitian complex transform")
        for shape, cshape, rshape, N, Nc, Nr, ghosts, mk_buffer in self.iter_shapes():
            print(
                "   IRFFT shape={:12s} ghosts={:12s} ".format(
                    str(shape), str(ghosts) + ":"
                ),
                end=" ",
            )
            Href = (
                np.random.rand(2 * Nc).astype(dtype).view(dtype=ctype).reshape(cshape)
            )
            results = {}
            for kind, implementations in self.implementations.items():
                for name, impl in implementations.items():
                    if dtype not in impl.supported_ftypes:
                        continue
                    try:
                        Bin = mk_buffer(backend=impl.backend, shape=cshape, dtype=ctype)
                        plan = impl.irfft(a=Bin).setup()
                        Bout = plan.output_array
                        assert Bout.shape == rshape, self.msg_shape.format(
                            rshape, Bout.shape, name
                        )
                        assert Bout.dtype == dtype, self.msg_dtype.format(
                            dtype, Bout.dtype, name
                        )
                        Bin[...] = Href
                        plan.execute()
                        H0 = Bin.get()
                        H1 = Bout.get()
                        assert np.array_equal(Href, H0), self.msg_input_modified.format(
                            name
                        )
                        Bout[...] = 0
                        evt = plan.execute()
                        assert plan.output_array is Bout
                        H2 = Bout.get()
                        assert np.allclose(
                            H1, H2, atol=eps
                        ), self.msg_output_modified.format(name)
                        results[name] = H1
                    except HysopFFTDataLayoutError as e:
                        results[name] = e
            check_distances(results, eps, self.report_eps, "normal C2R", failures)

        print(
            "\n BACKWARD FORCED C2R: real to hermitian complex transform with specified shape"
        )
        for shape, cshape, rshape, N, Nc, Nr, ghosts, mk_buffer in self.iter_shapes():
            print(
                "   IRFFT shape={:12s} ghosts={:12s} ".format(
                    str(shape), str(ghosts) + ":"
                ),
                end=" ",
            )
            Href = (
                np.random.rand(2 * Nc).astype(dtype).view(dtype=ctype).reshape(cshape)
            )
            results = {}
            for kind, implementations in self.implementations.items():
                for name, impl in implementations.items():
                    if dtype not in impl.supported_ftypes:
                        continue
                    try:
                        Bin = mk_buffer(backend=impl.backend, shape=cshape, dtype=ctype)
                        plan = impl.irfft(a=Bin, n=shape[-1]).setup()
                        Bout = plan.output_array
                        assert Bout.shape == shape, self.msg_shape.format(
                            shape, Bout.shape, name
                        )
                        assert Bout.dtype == dtype, self.msg_dtype.format(
                            dtype, Bout.dtype, name
                        )
                        Bin[...] = Href
                        plan.execute()
                        H0 = Bin.get()
                        H1 = Bout.get()
                        assert np.array_equal(Href, H0), self.msg_input_modified.format(
                            name
                        )
                        Bout[...] = 0
                        evt = plan.execute()
                        assert plan.output_array is Bout
                        H2 = Bout.get()
                        assert np.allclose(
                            H1, H2, atol=eps
                        ), self.msg_output_modified.format(name)
                        results[name] = H1
                    except HysopFFTDataLayoutError as e:
                        results[name] = e
            check_distances(results, eps, self.report_eps, "forced C2R", failures)

        types = ["I  ", "II ", "III", "IV "]
        for itype, stype in enumerate(types, 1):
            print(
                "\n DCT-{}: real to real discrete cosine transform {}".format(
                    stype.strip(), itype
                )
            )
            for (
                shape,
                cshape,
                rshape,
                N,
                Nc,
                Nr,
                ghosts,
                mk_buffer,
            ) in self.iter_shapes():
                print(
                    "   DCT-{} shape={:12s} ghosts={:12s} ".format(
                        stype, str(shape), str(ghosts) + ":"
                    ),
                    end=" ",
                )
                if itype == 1:  # real size is 2*(N-1)
                    shape = mk_shape(shape, -1, shape[-1] + 1)
                Href = np.random.rand(*shape).astype(dtype).reshape(shape)
                results = {}
                for kind, implementations in self.implementations.items():
                    for name, impl in implementations.items():
                        if dtype not in impl.supported_ftypes:
                            continue
                        if itype not in impl.supported_cosine_transforms:
                            continue
                        try:
                            Bin = mk_buffer(
                                backend=impl.backend, shape=shape, dtype=dtype
                            )
                            plan = impl.dct(a=Bin, type=itype).setup()
                            Bout = plan.output_array
                            assert Bout.shape == shape, self.msg_shape.format(
                                shape, Bout.shape, name
                            )
                            assert Bout.dtype == dtype, self.msg_dtype.format(
                                dtype, Bout.dtype, name
                            )
                            Bin[...] = Href
                            plan.execute()
                            H0 = Bin.get()
                            H1 = Bout.get()
                            assert np.array_equal(
                                Href, H0
                            ), self.msg_input_modified.format(name)
                            Bout[...] = 0
                            evt = plan.execute()
                            assert plan.output_array is Bout
                            H2 = Bout.get()
                            assert np.allclose(
                                H1, H2, atol=eps
                            ), self.msg_output_modified.format(name)
                            results[name] = H1 / shape[-1]
                        except HysopFFTDataLayoutError as e:
                            results[name] = e
                check_distances(results, eps, self.report_eps, f"DCT-{stype}", failures)

        for itype, stype in enumerate(types, 1):
            iitype = [1, 3, 2, 4][itype - 1]
            print(
                "\n IDCT-{}: real to real inverse discrete cosine transform {}".format(
                    stype.strip(), itype
                )
            )
            for (
                shape,
                cshape,
                rshape,
                N,
                Nc,
                Nr,
                ghosts,
                mk_buffer,
            ) in self.iter_shapes():
                print(
                    "   IDCT-{} shape={:12s} ghosts={:12s} ".format(
                        stype, str(shape), str(ghosts) + ":"
                    ),
                    end=" ",
                )
                if iitype == 1:  # real size is 2*(N-1)
                    shape = mk_shape(shape, -1, shape[-1] + 1)
                Href = np.random.rand(*shape).astype(dtype).reshape(shape)
                results = {}
                for kind, implementations in self.implementations.items():
                    for name, impl in implementations.items():
                        if dtype not in impl.supported_ftypes:
                            continue
                        if iitype not in impl.supported_cosine_transforms:
                            continue
                        try:
                            Bin = mk_buffer(
                                backend=impl.backend, shape=shape, dtype=dtype
                            )
                            plan = impl.idct(a=Bin, type=itype).setup()
                            Bout = plan.output_array
                            assert Bout.shape == shape, self.msg_shape.format(
                                shape, Bout.shape, name
                            )
                            assert Bout.dtype == dtype, self.msg_dtype.format(
                                dtype, Bout.dtype, name
                            )
                            Bin[...] = Href
                            plan.execute()
                            H0 = Bin.get()
                            H1 = Bout.get()
                            assert np.array_equal(
                                Href, H0
                            ), self.msg_input_modified.format(name)
                            Bout[...] = 0
                            evt = plan.execute()
                            assert plan.output_array is Bout
                            H2 = Bout.get()
                            assert np.allclose(
                                H1, H2, atol=eps
                            ), self.msg_output_modified.format(name)
                            results[name] = H1
                        except HysopFFTDataLayoutError as e:
                            results[name] = e
                check_distances(
                    results, eps, self.report_eps, f"IDCT-{stype}", failures
                )

        types = ["I  ", "II ", "III", "IV "]
        for itype, stype in enumerate(types, 1):
            print(
                "\n DST-{}: real to real discrete sine transform {}".format(
                    stype.strip(), itype
                )
            )
            for (
                shape,
                cshape,
                rshape,
                N,
                Nc,
                Nr,
                ghosts,
                mk_buffer,
            ) in self.iter_shapes():
                print(
                    "   DST-{} shape={:12s} ghosts={:12s} ".format(
                        stype, str(shape), str(ghosts) + ":"
                    ),
                    end=" ",
                )
                if itype == 1:  # real size will be 2*(N+1)
                    shape = mk_shape(shape, -1, shape[-1] - 1)
                Href = np.random.rand(*shape).astype(dtype).reshape(shape)
                results = {}
                for kind, implementations in self.implementations.items():
                    for name, impl in implementations.items():
                        if dtype not in impl.supported_ftypes:
                            continue
                        if itype not in impl.supported_sine_transforms:
                            continue
                        try:
                            Bin = mk_buffer(
                                backend=impl.backend, shape=shape, dtype=dtype
                            )
                            plan = impl.dst(a=Bin, type=itype).setup()
                            Bout = plan.output_array
                            assert Bout.shape == shape, self.msg_shape.format(
                                shape, Bout.shape, name
                            )
                            assert Bout.dtype == dtype, self.msg_dtype.format(
                                dtype, Bout.dtype, name
                            )
                            Bin[...] = Href
                            plan.execute()
                            H0 = Bin.get()
                            H1 = Bout.get()
                            assert np.array_equal(
                                Href, H0
                            ), self.msg_input_modified.format(name)
                            Bout[...] = 0
                            evt = plan.execute()
                            assert plan.output_array is Bout
                            H2 = Bout.get()
                            assert np.allclose(
                                H1, H2, atol=eps
                            ), self.msg_output_modified.format(name)
                            results[name] = H1 / shape[-1]
                        except HysopFFTDataLayoutError as e:
                            results[name] = e
                check_distances(results, eps, self.report_eps, f"DST-{stype}", failures)

        for itype, stype in enumerate(types, 1):
            iitype = [1, 3, 2, 4][itype - 1]
            print(
                "\n IDST-{}: real to real inverse discrete sine transform {}".format(
                    stype.strip(), itype
                )
            )
            for (
                shape,
                cshape,
                rshape,
                N,
                Nc,
                Nr,
                ghosts,
                mk_buffer,
            ) in self.iter_shapes():
                print(
                    "   IDST-{} shape={:12s} ghosts={:12s} ".format(
                        stype, str(shape), str(ghosts) + ":"
                    ),
                    end=" ",
                )
                if iitype == 1:  # real size will be 2*(N+1)
                    shape = mk_shape(shape, -1, shape[-1] - 1)
                Href = np.random.rand(*shape).astype(dtype).reshape(shape)
                results = {}
                for kind, implementations in self.implementations.items():
                    for name, impl in implementations.items():
                        if dtype not in impl.supported_ftypes:
                            continue
                        if iitype not in impl.supported_sine_transforms:
                            continue
                        try:
                            Bin = mk_buffer(
                                backend=impl.backend, shape=shape, dtype=dtype
                            )
                            plan = impl.idst(a=Bin, type=itype).setup()
                            Bout = plan.output_array
                            assert Bout.shape == shape, self.msg_shape.format(
                                shape, Bout.shape, name
                            )
                            assert Bout.dtype == dtype, self.msg_dtype.format(
                                dtype, Bout.dtype, name
                            )
                            Bin[...] = Href
                            plan.execute()
                            H0 = Bin.get()
                            H1 = Bout.get()
                            assert np.array_equal(
                                Href, H0
                            ), self.msg_input_modified.format(name)
                            Bout[...] = 0
                            evt = plan.execute()
                            assert plan.output_array is Bout
                            H2 = Bout.get()
                            assert np.allclose(
                                H1, H2, atol=eps
                            ), self.msg_output_modified.format(name)
                            results[name] = H1
                        except HysopFFTDataLayoutError as e:
                            results[name] = e
                check_distances(
                    results, eps, self.report_eps, f"IDST-{stype}", failures
                )

    def _test_forward_backward_1d(self, dtype):
        print()
        print(f"::Testing 1D forward-backward transforms, precision {dtype.__name__}::")
        eps = np.finfo(dtype).eps
        ctype = float_to_complex_dtype(dtype)

        def check_distances(distances):
            failed = False
            ss = ()
            for name, Einf in distances.items():
                if isinstance(Einf, HysopFFTDataLayoutError):
                    s = f"{name}=UNSUPPORTED_STRIDES"
                elif np.isfinite(Einf):
                    Eeps = int(np.round(Einf / eps))
                    failed |= Eeps >= self.fail_eps
                    s = f"{name}={Eeps}eps"
                else:
                    Eeps = Einf
                    failed |= True
                    s = f"{name}={str(Eeps).upper()}"
                ss += (s,)
            print(", ".join(ss))
            if failed:
                print()
                msg = "Some implementations failed !"
                if raise_on_failure:
                    raise RuntimeError(msg)

        print("\n C2C-C2C transform")
        for shape, cshape, rshape, N, Nc, Nr, ghosts, mk_buffer in self.iter_shapes():
            print(
                "   X - IFFT(FFT(X)) shape={:12s} ghosts={:12s}".format(
                    str(shape), str(ghosts) + ":"
                ),
                end=" ",
            )
            Href = np.random.rand(2 * N).astype(dtype).view(dtype=ctype).reshape(shape)
            results = {}
            for kind, implementations in self.implementations.items():
                for name, impl in implementations.items():
                    if dtype not in impl.supported_ftypes:
                        continue
                    D0 = mk_buffer(backend=impl.backend, shape=shape, dtype=ctype)
                    D1 = mk_buffer(backend=impl.backend, shape=shape, dtype=ctype)
                    try:
                        forward = impl.fft(a=D0, out=D1).setup()
                        backward = impl.ifft(a=D1, out=D0).setup()
                        assert forward.input_array is D0
                        assert forward.output_array is D1
                        assert backward.input_array is D1
                        assert backward.output_array is D0
                        D0[...] = Href
                        D1[...] = np.nan + 1j * np.nan
                        forward.execute()
                        D0[...] = np.nan + 1j * np.nan
                        backward.execute()
                        H0 = D0.get()
                        results[name] = np.max(np.abs(Href - H0))
                    except HysopFFTDataLayoutError as e:
                        results[name] = e
            check_distances(results)

        print("\n R2C-C2R transform")
        for shape, cshape, rshape, N, Nc, Nr, ghosts, mk_buffer in self.iter_shapes():
            print(
                "   X - IRFFT(RFFT(X)) shape={:12s} ghosts={:12s} ".format(
                    str(shape), str(ghosts) + ":"
                ),
                end=" ",
            )
            Href = np.random.rand(*shape).astype(dtype).reshape(shape)
            results = {}
            for kind, implementations in self.implementations.items():
                for name, impl in implementations.items():
                    if dtype not in impl.supported_ftypes:
                        continue
                    D0 = mk_buffer(backend=impl.backend, shape=shape, dtype=dtype)
                    D1 = mk_buffer(backend=impl.backend, shape=cshape, dtype=ctype)
                    try:
                        forward = impl.rfft(a=D0, out=D1).setup()
                        backward = impl.irfft(a=D1, out=D0).setup()
                        assert forward.input_array is D0
                        assert forward.output_array is D1
                        assert backward.input_array is D1
                        assert backward.output_array is D0
                        D0[...] = Href
                        D1[...] = np.nan + 1j * np.nan
                        forward.execute()
                        D0[...] = np.nan
                        backward.execute()
                        H0 = D0.get()
                        results[name] = np.max(np.abs(Href - H0))
                    except HysopFFTDataLayoutError as e:
                        results[name] = e
            check_distances(results)

        print("\n R2R-R2R transforms")

        types = ["I  ", "II ", "III", "IV "]
        for itype, stype in enumerate(types, 1):
            print(
                "\n DCT-{}: real to real discrete cosine transform {}".format(
                    stype.strip(), itype
                )
            )
            ttype = f"COS{itype}"
            for (
                shape,
                cshape,
                rshape,
                N,
                Nc,
                Nr,
                ghosts,
                mk_buffer,
            ) in self.iter_shapes():
                print(
                    "   X - I{}({}(X)) shape={:12s} ghosts={:12s} ".format(
                        ttype, ttype, str(shape), str(ghosts) + ":"
                    ),
                    end=" ",
                )
                if itype == 1:  # real size is 2*(N-1)
                    shape = mk_shape(shape, -1, shape[-1] + 1)
                Href = np.random.rand(*shape).astype(dtype).reshape(shape)
                results = {}
                for kind, implementations in self.implementations.items():
                    for name, impl in implementations.items():
                        iitype = [1, 3, 2, 4][itype - 1]
                        if dtype not in impl.supported_ftypes:
                            continue
                        if itype not in impl.supported_cosine_transforms:
                            continue
                        if iitype not in impl.supported_cosine_transforms:
                            continue
                        D0 = mk_buffer(backend=impl.backend, shape=shape, dtype=dtype)
                        D1 = mk_buffer(backend=impl.backend, shape=shape, dtype=dtype)
                        try:
                            forward = impl.dct(a=D0, out=D1, type=itype).setup()
                            backward = impl.idct(a=D1, out=D0, type=itype).setup()
                            assert forward.input_array is D0
                            assert forward.output_array is D1
                            assert backward.input_array is D1
                            assert backward.output_array is D0
                            D0[...] = Href
                            D1[...] = np.nan
                            forward.execute()
                            D0[...] = np.nan
                            backward.execute()
                            H0 = D0.get()
                            results[name] = np.max(np.abs(Href - H0))
                        except HysopFFTDataLayoutError as e:
                            results[name] = e
                check_distances(results)

        for itype, stype in enumerate(types, 1):
            print(
                "\n DST-{}: real to real discrete sine transform {}".format(
                    stype.strip(), itype
                )
            )
            ttype = f"SIN{itype}"
            for (
                shape,
                cshape,
                rshape,
                N,
                Nc,
                Nr,
                ghosts,
                mk_buffer,
            ) in self.iter_shapes():
                print(
                    "   X - I{}({}(X)) shape={:12s} ghosts={:12s} ".format(
                        ttype, ttype, str(shape), str(ghosts) + ":"
                    ),
                    end=" ",
                )
                if itype == 1:  # real size is 2*(N+1)
                    shape = mk_shape(shape, -1, shape[-1] - 1)
                Href = np.random.rand(*shape).astype(dtype).reshape(shape)
                results = {}
                for kind, implementations in self.implementations.items():
                    for name, impl in implementations.items():
                        iitype = [1, 3, 2, 4][itype - 1]
                        if dtype not in impl.supported_ftypes:
                            continue
                        if itype not in impl.supported_sine_transforms:
                            continue
                        if iitype not in impl.supported_sine_transforms:
                            continue
                        D0 = mk_buffer(backend=impl.backend, shape=shape, dtype=dtype)
                        D1 = mk_buffer(backend=impl.backend, shape=shape, dtype=dtype)
                        try:
                            forward = impl.dst(a=D0, out=D1, type=itype).setup()
                            backward = impl.idst(a=D1, out=D0, type=itype).setup()
                            assert forward.input_array is D0
                            assert forward.output_array is D1
                            assert backward.input_array is D1
                            assert backward.output_array is D0
                            D0[...] = Href
                            D1[...] = np.nan
                            forward.execute()
                            D0[...] = np.nan
                            backward.execute()
                            H0 = D0.get()
                            results[name] = np.max(np.abs(Href - H0))
                        except HysopFFTDataLayoutError as e:
                            results[name] = e
                check_distances(results)

    def iter_shapes(self):
        minj = (3, 3)
        maxj = (6, 6)
        msg = ("EVEN", "ODD")

        def _mk_shape(shape, ghosts):
            assert len(shape) == len(ghosts)
            return tuple(Si + 2 * Gi for (Si, Gi) in zip(shape, ghosts))

        def _mk_view(shape, ghosts):
            assert len(shape) == len(ghosts)
            return tuple(slice(Gi, Si + Gi) for (Si, Gi) in zip(shape, ghosts))

        for i in range(2):
            base = 2 + i
            print("  " + msg[i])
            for ghosts in ((0, 0, 0),):  # (2,0,0),(0,1,0),(0,0,3)):
                for j1 in range(minj[i], maxj[i]):
                    shape = (
                        3,
                        2,
                        base**j1,
                    )
                    cshape = list(shape)
                    cshape[-1] = cshape[-1] // 2 + 1
                    cshape = tuple(cshape)
                    rshape = list(shape)
                    rshape[-1] = (rshape[-1] // 2) * 2
                    rshape = tuple(rshape)
                    N = np.prod(shape, dtype=np.int64)
                    Nc = np.prod(cshape, dtype=np.int64)
                    Nr = np.prod(rshape, dtype=np.int64)

                    def mk_buffer(backend, shape, dtype, ghosts=ghosts):
                        real_shape = _mk_shape(shape=shape, ghosts=ghosts)
                        view = _mk_view(shape=shape, ghosts=ghosts)
                        buf = backend.empty(shape=real_shape, dtype=dtype)
                        buf = buf[view]
                        assert buf.shape == shape
                        assert buf.dtype == dtype
                        return buf

                    yield (shape, cshape, rshape, N, Nc, Nr, ghosts, mk_buffer)

    def report_failures(self, failures):
        print()
        print("== TEST FAILURES REPORT ==")
        print(f"  Report error has been set to {self.report_eps} epsilons.")
        print(f"  Test failure has been set to {self.fail_eps} epsilons.")
        assert self.report_eps <= self.fail_eps
        failed = False
        cnt = 0
        for dtype, fails in failures.items():
            if not fails:
                continue
            print(f"::{dtype} precision tests")
            for tag, tfails in fails.items():
                print(f"  |{tag} transform errors:")
                for r0, r1, shape, Einf, Eeps in sorted(tfails, key=lambda x: -x[-2]):
                    failed |= Eeps >= self.fail_eps
                    cnt += 1
                    msg = "    {} vs {}:\tshape {}\t->\tE={}\t({} eps)"
                    msg = msg.format(r0, r1, shape, Einf, Eeps)
                    print(msg)
        print("===========================")

        if failed:
            msg = ""
            msg += "\n************* FFT TESTS FAILED ***************"
            msg += "\n** One or more test exceeded failure error. **"
            msg += "\n**********************************************"
            msg += "\n"
            print(msg)
            raise RuntimeError
        else:
            msg = ""
            msg += "\n************* ALL FFT TESTS PASSED ****************"
            msg += "\n** Some tests may have exceeded reporting error. **"
            msg += "\n***************************************************"
            msg += "\n"
            print(msg)

    def perform_tests(self):
        # not testing np.longdouble because only one implementation supports it
        # ie. we cannot compare results between different implementations
        failures = {}
        if __ENABLE_LONG_TESTS__:
            dtypes = (
                np.float32,
                np.float64,
            )
        else:
            dtypes = (HYSOP_REAL,)
        for dtype in dtypes:
            self._test_forward_backward_1d(dtype=dtype)
            self._test_1d(dtype=dtype, failures=failures.setdefault(dtype.__name__, {}))
        self.report_failures(failures)

    def test_1d(self):
        failures = {}
        self._test_forward_backward_1d(dtype=HYSOP_REAL)
        self._test_1d(
            dtype=HYSOP_REAL, failures=failures.setdefault(HYSOP_REAL.__name__, {})
        )
        self.report_failures(failures)


if __name__ == "__main__":
    test = TestFFT()

    with printoptions(
        threshold=10000,
        linewidth=240,
        nanstr="nan",
        infstr="inf",
        formatter={"float": lambda x: f"{x:>0.2f}"},
    ):
        test.perform_tests()
