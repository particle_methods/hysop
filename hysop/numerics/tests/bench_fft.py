# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Test of fields defined with an analytic formula.
"""
import os, random, gc, pyfftw, gpyfft

import sympy as sm
import numpy as np
import itertools as it
import pyopencl as cl

from hysop.constants import Implementation, Backend, HYSOP_REAL
from hysop.testsenv import __ENABLE_LONG_TESTS__, __HAS_OPENCL_BACKEND__
from hysop.testsenv import opencl_failed, iter_clenv
from hysop.tools.contexts import printoptions, Timer, stderr_redirected
from hysop.tools.numerics import float_to_complex_dtype
from hysop.tools.htypes import check_instance, first_not_None

from hysop.numerics.fft.fft import mk_shape, HysopFFTDataLayoutError
from hysop.numerics.fft.numpy_fft import NumpyFFT
from hysop.numerics.fft.scipy_fft import ScipyFFT
from hysop.numerics.fft.fftw_fft import FftwFFT
from hysop.numerics.fft.gpyfft_fft import GpyFFT

from hysop.backend.device.opencl.opencl_kernel_statistics import OpenClKernelStatistics

devnull = open(os.devnull, "w")


class BenchFFT:

    implementations = {
        Implementation.PYTHON: {
            #'numpy': NumpyFFT(warn_on_allocation=False),
            #'scipy': ScipyFFT(warn_on_allocation=False),
            "fftw": FftwFFT(warn_on_allocation=False, warn_on_misalignment=True)
        },
        Implementation.OPENCL: {},
    }
    nruns = 2

    print()
    print(":: STARTING FFT BENCH ::")
    for i, cl_env in enumerate(iter_clenv(all_platforms=True)):
        print("> Registering opencl backend {} as:\n{}".format(i, cl_env))
        print()
        name = f"clfft{i}"
        queue = cl.CommandQueue(
            cl_env.context, properties=cl.command_queue_properties.PROFILING_ENABLE
        )
        cl_env.disable_default_queue()
        implementations[Implementation.OPENCL][name] = GpyFFT(
            cl_env=cl_env,
            queue=queue,
            warn_on_allocation=False,
            warn_on_unaligned_output_offset=True,
        )

    def _bench_1d(self, dtype):
        print()
        print(f"::Benching 1D transforms, precision {dtype.__name__}::")
        nruns = self.nruns
        ctype = float_to_complex_dtype(dtype)

        types = ["I", "II", "III"]
        IMPLEMENTATION_ERROR = "impl"
        DATA_LAYOUT_ERROR = "lay"
        ALLOCATION_ERROR = "mem"
        UNKNOWN_ERROR = "err"

        bench = [
            (
                " C2C: complex to complex forward transform",
                ("fft", {}),
                ("shape", "ctype"),
                ("shape", "ctype"),
                {},
            ),
            (
                " R2C: real to hermitian complex transform",
                ("rfft", {}),
                ("shape", "dtype"),
                ("cshape", "ctype"),
                {},
            ),
            (
                " C2R: hermitian complex to real transform",
                ("irfft", {}),
                ("cshape", "ctype"),
                ("rshape", "dtype"),
                {},
            ),
        ]
        for itype, stype in enumerate(types, 1):
            b = (
                f" DCT-{stype}: real to real discrete cosine transform {itype}",
                ("dct", {"type": itype}),
                ("shape", "dtype"),
                ("shape", "dtype"),
                {"offset": +(itype == 1)},
            )
            bench.append(b)
        for itype, stype in enumerate(types, 1):
            b = (
                f" DST-{stype}: real to real discrete sine transform {itype}",
                ("dst", {"type": itype}),
                ("shape", "dtype"),
                ("shape", "dtype"),
                {"offset": -(itype == 1)},
            )
            bench.append(b)

        results = {}
        for descr, fn, Bi, Bo, it_kwds in bench:
            print(descr)
            for kind, implementations in self.implementations.items():
                for name, impl in implementations.items():
                    results[name] = ()
                    if dtype not in impl.supported_ftypes:
                        continue
                    print(f"  {name:<8}: ", end=" ")
                    for shape, cshape, rshape, N, Nc, Nr, mk_buffer in self.iter_shapes(
                        **it_kwds
                    ):
                        print(".", end=" ")
                        with stderr_redirected(
                            devnull
                        ):  # get rid of Intel opencl warnings
                            try:
                                Bin = mk_buffer(
                                    backend=impl.backend,
                                    shape=locals()[Bi[0]],
                                    dtype=locals()[Bi[1]],
                                ).handle
                                Bout = mk_buffer(
                                    backend=impl.backend,
                                    shape=locals()[Bo[0]],
                                    dtype=locals()[Bo[1]],
                                ).handle
                                plan = getattr(impl, fn[0])(
                                    a=Bin, out=Bout, **fn[1]
                                ).setup()
                                gc.disable()  # disable garbage collector for timing (like timeit)
                                if kind == Implementation.OPENCL:
                                    for i in range(nruns):
                                        _ = plan.execute()
                                    _.wait()
                                    evt = plan.execute()
                                    evts = (evt,)
                                    for i in range(nruns - 1):
                                        evt = plan.execute(wait_for=[evt])
                                        evts += (evt,)
                                    evt.wait()
                                    res = (
                                        OpenClKernelStatistics(events=evts).mean / 1.0e6
                                    )  # ms
                                else:
                                    plan.execute()
                                    with Timer() as t:
                                        for i in range(nruns):
                                            plan.execute()
                                    res = (1e3 * t.interval) / float(nruns)  # ms
                                res = round(res, 2)
                                gc.enable()
                            except HysopFFTDataLayoutError as e:
                                res = DATA_LAYOUT_ERROR
                                print(e)
                            except MemoryError as e:
                                print(e)
                                res = ALLOCATION_ERROR
                            except gpyfft.gpyfftlib.GpyFFT_Error as e:
                                if str(e) == "MEM_OBJECT_ALLOCATION_FAILURE":
                                    res = ALLOCATION_ERROR
                                else:
                                    res = UNKNOWN_ERROR
                                print(e)
                            except Exception as e:
                                print(e)
                                res = UNKNOWN_ERROR
                            results[name] += (res,)
                    print(results[name])

    def iter_shapes(self, offset=0):
        minj = 12
        maxj = 27
        for j in range(minj, maxj):
            shape = (2**j + offset,)
            cshape = list(shape)
            cshape[-1] = cshape[-1] // 2 + 1
            cshape = tuple(cshape)
            rshape = list(shape)
            rshape[-1] = (rshape[-1] // 2) * 2
            rshape = tuple(rshape)
            N = np.prod(shape, dtype=np.int64)
            Nc = np.prod(cshape, dtype=np.int64)
            Nr = np.prod(rshape, dtype=np.int64)

            def mk_buffer(backend, shape, dtype):
                buf = backend.empty(shape=shape, dtype=dtype, min_alignment=128)
                assert buf.shape == shape
                assert buf.dtype == dtype
                return buf

            yield (shape, cshape, rshape, N, Nc, Nr, mk_buffer)

    def perform_benchs(self):
        if __ENABLE_LONG_TESTS__:
            dtypes = (
                np.float32,
                np.float64,
            )
        else:
            dtypes = (HYSOP_REAL,)
        for dtype in dtypes:
            self._bench_1d(dtype=dtype)


if __name__ == "__main__":
    bench = BenchFFT()

    with printoptions(
        threshold=10000,
        linewidth=240,
        nanstr="nan",
        infstr="inf",
        formatter={"float": lambda x: f"{x:>0.2f}"},
    ):
        bench.perform_benchs()
