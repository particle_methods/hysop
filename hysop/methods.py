# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""A list of authorized keys that may be used to set methods
in operators.

Usage
------

method = {key: value, ...}

Key must be one of the constants given below. Value is usually a class name.
See details in each operator.

You can fetch available operator method keys by using operator.available_methods().
This will return a distionnary with all the possible configurations.

Default methods can be retrived with operator.default_methods()

Example
--------
For the stretching case, you may use:

.. code::

    method = {TimeIntegrator: RK3, Formulation: Conservative,
              SpaceDiscretization: FDC4}

"""
from hysop.backend import __HAS_OPENCL_BACKEND__

from hysop.constants import (
    Backend,
    Precision,
    BoundaryCondition,
    TimeIntegratorsOptimisationLevel,
    FieldProjection,
    StretchingFormulation,
    SpaceDiscretization,
    ComputeGranularity,
)

from hysop.operator.spatial_filtering import FilteringMethod
from hysop.numerics.interpolation.interpolation import (
    Interpolation,
    MultiScaleInterpolation,
)
from hysop.numerics.interpolation.polynomial import (
    PolynomialInterpolator,
    PolynomialInterpolation,
)
from hysop.numerics.odesolvers.runge_kutta import TimeIntegrator
from hysop.numerics.remesh.remesh import Remesh
from hysop.numerics.splitting.strang import StrangOrder

if __HAS_OPENCL_BACKEND__:
    from hysop.backend.device.opencl.opencl_kernel_config import OpenClKernelConfig
    from hysop.backend.device.opencl.opencl_kernel_autotuner_config import (
        OpenClKernelAutotunerConfig,
    )
