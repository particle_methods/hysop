# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
@file enstrophy.py
Enstrophy solver frontend.
"""
from hysop.constants import Implementation
from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.henum import EnumFactory
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import Field
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.core.graph.computational_node_frontend import ComputationalGraphNodeFrontend
from hysop.parameters.scalar_parameter import ScalarParameter


class Enstrophy(ComputationalGraphNodeFrontend):
    """
    Interface computing enstrophy.
    Available implementations are:
        *OPENCL (gpu based implementation) (default)
        *PYTHON
    """

    @classmethod
    def implementations(cls):
        from hysop.backend.device.opencl.operator.enstrophy import OpenClEnstrophy
        from hysop.backend.host.python.operator.enstrophy import PythonEnstrophy

        implementations = {
            Implementation.OPENCL: OpenClEnstrophy,
            Implementation.PYTHON: PythonEnstrophy,
        }
        return implementations

    @classmethod
    def default_implementation(cls):
        return Implementation.OPENCL

    @debug
    def __new__(
        cls,
        vorticity,
        enstrophy,
        variables,
        rho=None,
        rho_0=1.0,
        WdotW=None,
        implementation=None,
        base_kwds=None,
        **kwds,
    ):
        base_kwds = first_not_None(base_kwds, {})
        return super().__new__(
            cls,
            vorticity=vorticity,
            rho=rho,
            enstrophy=enstrophy,
            WdotW=WdotW,
            rho_0=rho_0,
            variables=variables,
            base_kwds=base_kwds,
            implementation=implementation,
            **kwds,
        )

    @debug
    def __init__(
        self,
        vorticity,
        enstrophy,
        variables,
        rho=None,
        rho_0=1.0,
        WdotW=None,
        implementation=None,
        base_kwds=None,
        **kwds,
    ):
        """
        Initialize a Enstrophy operator frontend.

        Enstrophy is the scaled volume average of rho*(W.W) on the domain where . represents
        the vector dot product).

        in:  W (vorticity field)
             rho (density field, optional, defaults to 1.0 everywhere)
        out: E = 1.0/(2*V*rho_0) * integral(rho*(W.W)) => enstrophy (scalar parameter)
             where V is the domain volume, rho_0 the reference density.

        Parameters
        ----------
        vorticity: Field
            Input continuous vorticity field.
        enstrophy: ScalarParameter
            Enstrophy scalar output parameter.
        rho: Field, optional
            Input continuous density field, if not given,
            defaults to 1.0 on the whole domain.
        rho_0: float, optional
            Reference density, defaults to 1.0.
        WdotW: Field, optional
            Output continuous field if required, will contain rho * (W.W) (term before integration).
            If WdotW is given, WdotW will contain rho * (W.W) else this will be
            a temporary field only usable during this operator's apply method.
            Should have nb_components=1.
        variables: dict
            dictionary of fields as keys and topologies as values.
        implementation: Implementation, optional, defaults to None
            target implementation, should be contained in available_implementations().
            If None, implementation will be set to default_implementation().
        base_kwds: dict, optional, defaults to None
            Base class keywords arguments.
            If None, an empty dict will be passed.
        kwds:
            Extra keywords arguments that will be passed towards implementation
            enstrophy operator __init__.

        Notes
        -----
        An Enstrophy operator implementation should at least support
        the hysop.operator.base.enstrophy.EnstrophyBase interface.
        """
        base_kwds = first_not_None(base_kwds, {})

        check_instance(vorticity, Field)
        check_instance(enstrophy, ScalarParameter)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(base_kwds, dict, keys=str)
        check_instance(WdotW, Field, allow_none=True)
        check_instance(rho, Field, allow_none=True)
        check_instance(rho_0, float)

        super().__init__(
            vorticity=vorticity,
            rho=rho,
            enstrophy=enstrophy,
            WdotW=WdotW,
            rho_0=rho_0,
            variables=variables,
            base_kwds=base_kwds,
            implementation=implementation,
            **kwds,
        )
