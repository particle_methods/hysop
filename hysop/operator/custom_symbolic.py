# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
@file hysop/operator/symbolic.py
CustomSymbolicOperator solver frontend.
"""
from hysop.constants import Implementation
from hysop.tools.htypes import check_instance, to_tuple, first_not_None
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import Field
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.core.graph.computational_node_frontend import ComputationalGraphNodeFrontend
from hysop.backend import __HAS_OPENCL_BACKEND__


class CustomSymbolicOperator(ComputationalGraphNodeFrontend):
    """
    Interface for custom symbolic operators.
    Available implementations are:
        *OPENCL (opencl code generated kernels)
    """

    @classmethod
    def implementations(cls):
        __implementations = dict()
        if __HAS_OPENCL_BACKEND__:
            from hysop.backend.device.opencl.operator.custom_symbolic import (
                OpenClCustomSymbolicOperator,
            )

            __implementations.update(
                {Implementation.OPENCL: OpenClCustomSymbolicOperator}
            )
        return __implementations

    @classmethod
    def default_implementation(cls):
        return Implementation.OPENCL

    @debug
    def __new__(
        cls, name, exprs, variables, implementation=None, base_kwds=None, **kwds
    ):
        return super().__new__(
            cls,
            name=name,
            exprs=exprs,
            variables=variables,
            base_kwds=base_kwds,
            implementation=implementation,
            **kwds,
        )

    @debug
    def __init__(
        self, name, exprs, variables, implementation=None, base_kwds=None, **kwds
    ):
        """
        Initialize a CustomSymbolicOperator operator frontend.

        See hysop.operator.base.CustomSymbolicOperatorBase to see
        how expressions are parsed.

        Parameters
        ----------
        name: str
            Name of this custom operator.
        exprs: hysop.symbolic.exprs or array like of valid expresions
            Expressions that will generate code.
            Valid expressions are defined in SymbolicExpressionParser.
        variables: dict
            dictionary of fields as keys and topologies as values.
        implementation: Implementation, optional, defaults to None
            target implementation, should be contained in available_implementations().
            If None, implementation will be set to default_implementation().
        base_kwds: dict, optional, defaults to None
            Base class keywords arguments.
            If None, an empty dict will be passed.
        kwds:
            Keywords arguments that will be passed towards implementation
            operator __init__.
        """
        base_kwds = first_not_None(base_kwds, {})
        exprs = to_tuple(exprs)

        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(base_kwds, dict, keys=str)

        super().__init__(
            name=name,
            exprs=exprs,
            variables=variables,
            base_kwds=base_kwds,
            implementation=implementation,
            **kwds,
        )
