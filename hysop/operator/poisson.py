# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
@file poisson.py
Poisson solver frontend.
"""
from hysop.constants import Implementation
from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.henum import EnumFactory
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import Field
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors

from hysop.operator.base.spectral_operator import SpectralComputationalGraphNodeFrontend
from hysop.backend.host.python.operator.poisson import PythonPoisson

try:
    from hysop.backend.device.opencl.operator.poisson import OpenClPoisson
except ImportError:
    OpenClPoisson = None
try:
    from hysop.backend.host.fortran.operator.poisson import PoissonFFTW
except ImportError:
    PoissonFFTW = None


class Poisson(SpectralComputationalGraphNodeFrontend):
    """
    Interface the poisson solver.
    Available implementations are: FORTRAN: FFTW based solver (legacy fortran)
                                   PYTHON:  generic python fft based solver (pyfftw, scipy or numpy)
                                   OPENCL:  generic opencl fft based solver (gpyfft)
    """

    @classmethod
    def implementations(cls):
        __implementations = {
            Implementation.PYTHON: PythonPoisson,
        }
        if not OpenClPoisson is None:
            __implementations.update(
                {
                    Implementation.OPENCL: OpenClPoisson,
                }
            )
        if not PoissonFFTW is None:
            __implementations.update(
                {
                    Implementation.FORTRAN: PoissonFFTW,
                }
            )
        return __implementations

    @classmethod
    def default_implementation(cls):
        return Implementation.PYTHON

    @debug
    def __new__(cls, Fin, Fout, variables, implementation=None, base_kwds=None, **kwds):
        return super().__new__(
            cls,
            Fin=Fin,
            Fout=Fout,
            variables=variables,
            base_kwds=base_kwds,
            implementation=implementation,
            **kwds,
        )

    @debug
    def __init__(
        self, Fin, Fout, variables, implementation=None, base_kwds=None, **kwds
    ):
        """
        Initialize a n-dimensional Poisson operator frontend.

        Solves:
            Laplacian(Fout) = Fin


        Parameters
        ----------
        Fout: Field
            Input continuous field (rhs).
        Fin: Field
            Output continuous field (lhs), possibly inplace.
        variables: dict
            dictionary of fields as keys and topologies as values.
        implementation: Implementation, optional, defaults to None
            Target implementation, should be contained in available_implementations().
            If None, implementation will be set to default_implementation().
        base_kwds: dict, optional, defaults to None
            Base class keywords arguments.
            If None, an empty dict will be passed.
        kwds:
            Keywords arguments that will be passed towards implementation
            poisson operator __init__.
        """
        base_kwds = first_not_None(base_kwds, {})

        check_instance(Fout, Field)
        check_instance(Fin, Field)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(base_kwds, dict, keys=str)
        assert Fin.domain.dim != 1, "Bug in 1D [SYM/SYM] Psi case wint CLFFT"

        super().__init__(
            Fin=Fin,
            Fout=Fout,
            variables=variables,
            base_kwds=base_kwds,
            implementation=implementation,
            **kwds,
        )
