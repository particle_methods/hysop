# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
@file penalization.py
.. currentmodule:: hysop.operator.penalization

* :class:`Penalization` : standard penalisation (not implemented yet)
* :class:`PenalizeVorticity`  : vorticity formulation

See details in :ref:`penalisation` section of HySoP user guide.
"""
from hysop.constants import Implementation, PenalizationFormulation
from hysop.tools.htypes import check_instance, to_list
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import Field
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.parameters.tensor_parameter import TensorParameter
from hysop.core.graph.computational_node_frontend import ComputationalGraphNodeFrontend
from hysop.backend.host.python.operator.penalization import (
    PythonPenalizeVorticity,
    PythonPenalizeVelocity,
)


class PenalizeVorticity(ComputationalGraphNodeFrontend):
    r"""
    Solve
    \f{eqnarray*}
    \frac{\partial w}{\partial t} &=& \lambda\chi_s\nabla\times(v_D - v)
    \f}
    using penalization.
    """

    __implementations = {Implementation.PYTHON: PythonPenalizeVorticity}

    @classmethod
    def implementations(cls):
        return cls.__implementations

    @classmethod
    def default_implementation(cls):
        return Implementation.PYTHON

    @debug
    def __new__(
        cls,
        obstacles,
        variables,
        velocity,
        vorticity,
        dt,
        coeff=None,
        ubar=None,
        formulation=None,
        implementation=None,
        **kwds,
    ):
        return super().__new__(
            cls,
            velocity=velocity,
            vorticity=vorticity,
            coeff=coeff,
            ubar=ubar,
            obstacles=obstacles,
            dt=dt,
            formulation=formulation,
            variables=variables,
            implementation=implementation,
            **kwds,
        )

    @debug
    def __init__(
        self,
        obstacles,
        variables,
        velocity,
        vorticity,
        dt,
        coeff=None,
        ubar=None,
        formulation=None,
        implementation=None,
        **kwds,
    ):
        r"""
        Parameters
        ----------
        obstacles : dict or list of :class:`~hysop.Field`
            sets of geometries on which penalization must be applied
        velocity: field
            input velocity
        vorticity: field,
            output vorticity
        coeff : ScalarParameter, optional
            penalization factor (\f\lambda\f) applied to all geometries.
        ubar : TensorParameter, optional
            Solid velocity (default to 0)
        formulation: PenalizationFormulation
            Solving penalization either with IMPLICIT scheme or EXACT solution
        variables: dict
            dictionary of fields as keys and topologies as values.
        dt: ScalarParameter
            Timestep parameter that will be used for time integration.
        implementation: Implementation, optional, defaults to None
            target implementation, should be contained in available_implementations().
            If None, implementation will be set to default_implementation().
        kwds:
            Keywords arguments that will be passed towards implementation
            poisson operator __init__.

        Notes
        -----
        velocity is not modified by this operator.
        vorticity is an in-out parameter.

        Set::

        obstacles = {coeff1: obs1, coeff2: obs2, ...}
        coeff = None

        to apply a different coefficient on each subset.
        Set::

        obstacles = [obs1, obs2, ...]
        coeff = some_value

        Warning : coeff as a function is not yet implemented!!
        """
        if not isinstance(obstacles, dict):
            obstacles = to_list(obstacles)
            assert len(set(obstacles)) == len(obstacles)
            obstacles = tuple(obstacles)

        check_instance(velocity, Field)
        check_instance(vorticity, Field)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(dt, ScalarParameter)
        check_instance(
            coeff, (ScalarParameter, float, type(lambda x: x)), allow_none=True
        )
        check_instance(formulation, PenalizationFormulation, allow_none=True)
        check_instance(ubar, (TensorParameter, Field), allow_none=True)
        check_instance(
            obstacles,
            (tuple, dict),
            values=Field,
            keys=(ScalarParameter, float, type(lambda x: x)),
            check_kwds=False,
        )
        super().__init__(
            velocity=velocity,
            vorticity=vorticity,
            coeff=coeff,
            ubar=ubar,
            obstacles=obstacles,
            dt=dt,
            formulation=formulation,
            variables=variables,
            implementation=implementation,
            **kwds,
        )


class PenalizeVelocity(ComputationalGraphNodeFrontend):
    r"""
    Solve
    \f{eqnarray*}
    \frac{\partial w}{\partial t} &=& \lambda\chi_s\nabla\times(v_D - v)
    \f}
    using penalization.
    """

    __implementations = {Implementation.PYTHON: PythonPenalizeVelocity}

    @classmethod
    def implementations(cls):
        return cls.__implementations

    @classmethod
    def default_implementation(cls):
        return Implementation.PYTHON

    @debug
    def __init__(
        self,
        obstacles,
        variables,
        velocity,
        dt,
        coeff=None,
        ubar=None,
        formulation=None,
        implementation=None,
        **kwds,
    ):
        r"""
        Parameters
        ----------
        obstacles : dict or list of :class:`~hysop.Field`
            sets of geometries on which penalization must be applied
        velocity: field
            input velocity
        coeff : ScalarParameter, optional
            penalization factor (\f\lambda\f) applied to all geometries.
        ubar : TensorParameter, optional
            Solid velocity (default to 0)
        formulation: PenalizationFormulation
            Solving penalization either with IMPLICIT scheme or EXACT solution
        variables: dict
            dictionary of fields as keys and topologies as values.
        dt: ScalarParameter
            Timestep parameter that will be used for time integration.
        implementation: Implementation, optional, defaults to None
            target implementation, should be contained in available_implementations().
            If None, implementation will be set to default_implementation().
        kwds:
            Keywords arguments that will be passed towards implementation
            poisson operator __init__.

        Set::

        obstacles = {coeff1: obs1, coeff2: obs2, ...}
        coeff = None

        to apply a different coefficient on each subset.
        Set::

        obstacles = [obs1, obs2, ...]
        coeff = some_value

        Warning : coeff as a function is not yet implemented!!
        """
        if not isinstance(obstacles, dict):
            obstacles = to_list(obstacles)
            assert len(set(obstacles)) == len(obstacles)
            obstacles = tuple(obstacles)

        check_instance(velocity, Field)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(dt, ScalarParameter)
        check_instance(
            coeff, (ScalarParameter, float, type(lambda x: x)), allow_none=True
        )
        check_instance(formulation, PenalizationFormulation, allow_none=True)
        check_instance(ubar, (TensorParameter, Field), allow_none=True)
        check_instance(
            obstacles,
            (tuple, dict),
            values=Field,
            keys=(ScalarParameter, float, type(lambda x: x)),
            check_kwds=False,
        )
        super().__init__(
            velocity=velocity,
            coeff=coeff,
            ubar=ubar,
            obstacles=obstacles,
            dt=dt,
            formulation=formulation,
            variables=variables,
            implementation=implementation,
            **kwds,
        )
