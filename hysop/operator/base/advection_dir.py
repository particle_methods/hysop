# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.numpywrappers import npw
from hysop.tools.decorators import debug
from hysop.tools.htypes import check_instance, InstanceOf
from hysop.methods import Interpolation, TimeIntegrator, Remesh
from hysop.fields.continuous_field import Field, ScalarField

from hysop.numerics.remesh.remesh import RemeshKernel
from hysop.numerics.odesolvers.runge_kutta import (
    ExplicitRungeKutta,
    Euler,
    RK2,
    RK3,
    RK4,
)

from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.core.memory.memory_request import MemoryRequest
from hysop.core.graph.computational_node import ComputationalGraphNode
from hysop.parameters.scalar_parameter import ScalarParameter


class DirectionalAdvectionBase:

    __default_method = {
        TimeIntegrator: Euler,
        Interpolation: Interpolation.LINEAR,
        Remesh: Remesh.L2_1,
    }

    __available_methods = {
        TimeIntegrator: InstanceOf(ExplicitRungeKutta),
        Interpolation: Interpolation.LINEAR,
        Remesh: (InstanceOf(Remesh), InstanceOf(RemeshKernel)),
    }

    @classmethod
    def default_method(cls):
        dm = super().default_method()
        dm.update(cls.__default_method)
        return dm

    @classmethod
    def available_methods(cls):
        am = super().available_methods()
        am.update(cls.__available_methods)
        return am

    @debug
    def __new__(
        cls,
        velocity,
        relative_velocity,
        splitting_direction,
        advected_fields_in,
        advected_fields_out,
        variables,
        dt,
        velocity_cfl,
        remesh_criteria_eps=None,
        **kwds,
    ):
        return super().__new__(
            cls,
            input_fields=None,
            output_fields=None,
            input_params=None,
            output_params=None,
            splitting_direction=splitting_direction,
            **kwds,
        )

    @debug
    def __init__(
        self,
        velocity,
        relative_velocity,
        splitting_direction,
        advected_fields_in,
        advected_fields_out,
        variables,
        dt,
        velocity_cfl,
        remesh_criteria_eps=None,
        **kwds,
    ):
        """
        Particle advection of field(s) in a given direction,
        on any backend, with cartesian remeshing.

        Parameters
        ----------
        velocity: Field
            Continuous velocity field (all components)
        splitting_direction: int
            The splitting direction.
        advected_fields_in: Field or array like of Fields
            Instance or list of continuous fields to be advected.
        advected_fields_out: Field or array like of Field
            Where input fields should be remeshed.
        relative_velocity: array-like relative velocities, optional
            Relative velocity that has to be taken into account.
            Vi = Ui - Urel[i] for each components.
        variables: dict
            Dictionary of continuous fields as keys and topologies as values.
        dt: ScalarParameter
            Timestep parameter that will be used for time integration.
        velocity_cfl: float or tuple
            Velocity cfl in given direction. In case of tuple given, value is taken from splitting direction.
        remesh_criteria_eps: int
            Minimum number of epsilons (in specified precision) that will trigger
            remeshing. By default every non zero value is remeshed.
        kwds:
            Extra parameters passed to generated directional operators.

        Attributes
        ----------
        velocity: Field
            Continuous velocity field (all components)
        advected_fields_in: list
            Tuple of continuous fields to be advected.
        advected_fields_out: list
            Tuple of output continuous fields.
        """
        check_instance(velocity, Field)
        check_instance(advected_fields_in, tuple, values=Field)
        check_instance(advected_fields_out, tuple, values=Field)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(velocity_cfl, (float, int, tuple))
        if isinstance(velocity_cfl, tuple):
            velocity_cfl = velocity_cfl[velocity.domain.dim - 1 - splitting_direction]
            check_instance(velocity_cfl, (float, int))
        check_instance(dt, ScalarParameter)
        check_instance(
            relative_velocity, tuple, values=(str, float), size=velocity.nb_components
        )

        velocity_cfl = float(velocity_cfl)

        assert len(advected_fields_in) == len(
            advected_fields_out
        ), "|inputs| != |outputs|"
        assert velocity_cfl > 0.0, "velocity_cfl cfl <= 0"

        # expand tensors
        advected_scalars_in, advected_scalars_out = (), ()
        input_tensors, output_tensors = (), ()
        for Sin, Sout in zip(advected_fields_in, advected_fields_out):
            msg = "Input and output field shape mismatch, got field {} of shape {} "
            msg += "and field {} of shape {}."
            if Sin.is_tensor ^ Sout.is_tensor:
                if Sin.is_tensor:
                    msg = msg.format(
                        Sin.short_description(),
                        Sin.shape,
                        Sout.short_description(),
                        "(1,)",
                    )
                else:
                    msg = msg.format(
                        Sin.short_description(),
                        "(1,)",
                        Sout.short_description(),
                        Sout.shape,
                    )
                raise RuntimeError(msg)
            if Sin.is_tensor and Sout.is_tensor:
                if Sin.shape != Sout.shape:
                    msg = msg.format(
                        Sin.short_description(),
                        Sin.shape,
                        Sout.short_description(),
                        Sout.shape,
                    )
                    raise RuntimeError(msg)
                input_tensors += (Sin,)
                output_tensors += (Sout,)
            advected_scalars_in += Sin.fields
            advected_scalars_out += Sout.fields

        check_instance(advected_scalars_in, tuple, values=ScalarField)
        check_instance(advected_scalars_out, tuple, values=ScalarField)
        assert len(set(advected_scalars_in)) == len(
            advected_scalars_in
        ), "|sinputs| != |soutputs|"

        is_inplace = True
        for ifield, ofield in zip(advected_scalars_in, advected_scalars_out):
            if ifield is ofield:
                assert is_inplace, "Cannot mix inplace and out of place advection."
            else:
                is_inplace = False

        if velocity.is_tensor:
            Vd = velocity[splitting_direction]
        else:
            assert splitting_direction == 0
            Vd = velocity
        Ud = relative_velocity[splitting_direction]

        input_fields = {Vd: variables[velocity]}
        output_fields = {}
        input_params = {dt}
        output_params = set()

        for ifield, ofield in zip(advected_fields_in, advected_fields_out):
            input_fields[ifield] = variables[ifield]
            output_fields[ofield] = variables[ofield]

        super().__init__(
            input_fields=input_fields,
            output_fields=output_fields,
            input_params=input_params,
            output_params=output_params,
            splitting_direction=splitting_direction,
            **kwds,
        )

        self.velocity = Vd
        self.relative_velocity = Ud
        self.first_scalar = advected_scalars_in[0]
        self.advected_fields_in = advected_scalars_in
        self.advected_fields_out = advected_scalars_out
        self.dt = dt

        self.velocity_cfl = velocity_cfl
        self.is_inplace = is_inplace

    @debug
    def handle_method(self, method):
        super().handle_method(method)

        remesh_kernel = method.pop(Remesh)
        if isinstance(remesh_kernel, Remesh):
            remesh_kernel = RemeshKernel.from_enum(remesh_kernel)

        self.remesh_kernel = remesh_kernel
        self.interp = method.pop(Interpolation)
        self.time_integrator = method.pop(TimeIntegrator)

    @debug
    def get_field_requirements(self):
        requirements = super().get_field_requirements()

        direction = self.splitting_direction
        is_inplace = self.is_inplace

        velocity = self.velocity
        velocity_cfl = self.velocity_cfl
        v_topo, v_requirements = requirements.get_input_requirement(velocity)
        try:
            v_dx = v_topo.space_step
        except AttributeError:
            v_dx = v_topo.mesh.space_step

        dim = velocity.domain.dim
        ndir = dim - 1 - direction

        scalar = self.first_scalar
        s_topo, _ = requirements.get_input_requirement(scalar)
        try:
            s_dx = s_topo.space_step
        except AttributeError:
            s_dx = s_topo.mesh.space_step
        scalar_cfl = velocity_cfl * (v_dx[ndir] / s_dx[ndir])

        advection_ghosts = self.velocity_advection_ghosts(velocity_cfl)
        min_velocity_ghosts = npw.integer_zeros(v_dx.shape)
        has_bilevel = any(v_dx != s_dx)
        if has_bilevel:
            # add one ghost in each direction for nd interpolation
            min_velocity_ghosts[...] = 1
        min_velocity_ghosts[ndir] = advection_ghosts
        v_requirements.min_ghosts = npw.maximum(
            v_requirements.min_ghosts, min_velocity_ghosts
        )

        scalar_advection_ghosts = self.velocity_advection_ghosts(scalar_cfl)
        remesh_ghosts = self.scalars_out_cache_ghosts(scalar_cfl, self.remesh_kernel)
        assert remesh_ghosts >= scalar_advection_ghosts, "{} : {} {}".format(
            self.name, remesh_ghosts, scalar_advection_ghosts
        )

        min_scalar_ghosts = npw.integer_zeros(s_dx.shape)
        min_scalar_ghosts[ndir] = remesh_ghosts

        for sfield in self.advected_fields_out:
            _s_topo, _s_requirements = requirements.get_output_requirement(sfield)
            try:
                _s_dx = _s_topo.space_step
            except AttributeError:
                _s_dx = _s_topo.mesh.space_step
            assert (_s_dx == s_dx).all()
            _s_requirements.min_ghosts = npw.maximum(
                _s_requirements.min_ghosts, min_scalar_ghosts
            )

        if is_inplace:
            for sfield in self.advected_fields_in:
                _s_topo, _s_requirements = requirements.get_input_requirement(sfield)
                try:
                    _s_dx = _s_topo.space_step
                except AttributeError:
                    _s_dx = _s_topo.mesh.space_step
                assert (_s_dx == s_dx).all()
                _s_requirements.min_ghosts = npw.maximum(
                    _s_requirements.min_ghosts, min_scalar_ghosts
                )

        self.scalar_cfl = scalar_cfl
        self.min_velocity_ghosts = min_velocity_ghosts
        self.min_scalar_ghosts = min_scalar_ghosts
        self.advection_ghosts = advection_ghosts
        self.scalar_advection_ghosts = scalar_advection_ghosts
        self.remesh_ghosts = remesh_ghosts

        return requirements

    @classmethod
    def velocity_advection_ghosts(cls, velocity_cfl):
        assert velocity_cfl > 0.0, "cfl <= 0.0"
        return int(1 + npw.floor(velocity_cfl))

    @classmethod
    def scalars_out_cache_ghosts(cls, scalar_cfl, remesh_kernel):
        """Return the minimum number of ghosts for remeshed scalars."""
        assert scalar_cfl > 0.0, "cfl <= 0.0"
        assert remesh_kernel.n >= 1, "Bad remeshing kernel."
        if remesh_kernel.n > 1:
            assert remesh_kernel.n % 2 == 0, "Odd remeshing kernel moments."
        min_ghosts = int(npw.floor(scalar_cfl) + 1 + remesh_kernel.n // 2)
        return min_ghosts

    @debug
    def discretize(self):
        super().discretize()
        dvelocity = self.input_discrete_fields[self.velocity]

        # scalar field -> discrete_field
        dadvected_fields_in = {
            ifield: self.input_discrete_fields[ifield]
            for ifield in self.advected_fields_in
        }
        dadvected_fields_out = {
            ofield: self.output_discrete_fields[ofield]
            for ofield in self.advected_fields_out
        }

        self.dvelocity = dvelocity
        self.dadvected_fields_in = dadvected_fields_in
        self.dadvected_fields_out = dadvected_fields_out

        self.is_bilevel = None
        if any(
            self.dvelocity.compute_resolution
            != next(iter(self.dadvected_fields_out.values())).compute_resolution
        ):
            self.is_bilevel = self.dvelocity.compute_resolution

    @debug
    def get_work_properties(self):
        requests = super().get_work_properties()
        f = next(iter(self.dadvected_fields_in.values()))
        (pos, request, request_id) = MemoryRequest.cartesian_dfield_like(
            name="position", dfield=f, ghosts=0, nb_components=1, is_read_only=False
        )
        requests.push_mem_request(request_id, request)
        assert all(f.compute_resolution == pos.resolution)
        self.dposition = pos
        return requests

    @debug
    def setup(self, work):
        super().setup(work)
        self._buffer_allocations(work)

    def _buffer_allocations(self, work):
        """
        Allocate Host buffers for advected particle positions and
        advected fields on particles.
        """
        if work is None:
            raise ValueError("work is None.")
        self.dposition.honor_memory_request(work, self)

    # Backend methods
    # DirectionalOperatorBase
    @classmethod
    def supported_dimensions(cls):
        return tuple(range(16))
