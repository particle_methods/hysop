# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.decorators import debug
from hysop.tools.htypes import check_instance
from hysop.fields.continuous_field import Field, VectorField
from hysop.parameters.parameter import Parameter
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.core.graph.graph import op_apply


class CustomOperatorBase:

    @debug
    def __new__(
        cls,
        func,
        invars=None,
        outvars=None,
        extra_args=None,
        variables=None,
        ghosts=None,
        do_update_ghosts=True,
        **kwds,
    ):
        return super().__new__(
            cls,
            input_fields=None,
            output_fields=None,
            input_params=None,
            output_params=None,
            **kwds,
        )

    @debug
    def __init__(
        self,
        func,
        invars=None,
        outvars=None,
        extra_args=None,
        variables=None,
        ghosts=None,
        do_update_ghosts=True,
        **kwds,
    ):
        check_instance(
            invars, (tuple, list), values=(Field, Parameter), allow_none=True
        )
        check_instance(
            outvars, (tuple, list), values=(Field, Parameter), allow_none=True
        )
        check_instance(extra_args, tuple, allow_none=True)
        check_instance(
            variables,
            dict,
            keys=Field,
            values=CartesianTopologyDescriptors,
            allow_none=True,
        )
        check_instance(ghosts, int, allow_none=True)
        check_instance(
            do_update_ghosts,
            bool,
        )
        input_fields, output_fields = {}, {}
        input_params, output_params = set(), set()
        if invars is not None:
            for v in invars:
                if isinstance(v, Field):
                    input_fields[v] = variables[v]
                elif isinstance(v, Parameter):
                    input_params.update({v})
        if outvars is not None:
            for v in outvars:
                if isinstance(v, Field):
                    output_fields[v] = variables[v]
                elif isinstance(v, Parameter):
                    output_params.update({v})
        self.invars, self.outvars = invars, outvars
        self.func = func
        self.extra_args = tuple()
        if not extra_args is None:
            self.extra_args = extra_args
        self._ghosts = ghosts
        self._do_update_ghosts = do_update_ghosts
        super().__init__(
            input_fields=input_fields,
            output_fields=output_fields,
            input_params=input_params,
            output_params=output_params,
            **kwds,
        )

    @debug
    def get_field_requirements(self):
        requirements = super().get_field_requirements()
        if not self._ghosts is None:
            for it in requirements.iter_requirements():
                if not it[1] is None:
                    is_input, (field, td, req) = it
                    min_ghosts = (max(g, self._ghosts) for g in req.min_ghosts.copy())
                    max_ghosts = (min(g, self._ghosts) for g in req.max_ghosts.copy())
                    req.min_ghosts = min_ghosts
                    req.max_ghosts = max_ghosts
        return requirements

    @debug
    def discretize(self):
        if self.discretized:
            return
        super().discretize()
        dinvar, dinparam = [], []
        doutvar, doutparam = [], []
        idf, odf = self.input_discrete_fields, self.output_discrete_fields
        self.ghost_exchanger = []
        if self.invars is not None:
            for v in self.invars:
                if isinstance(v, Field):
                    for _v in v if isinstance(v, VectorField) else (v,):
                        for vd in idf[_v]:
                            dinvar.append(vd)
                elif isinstance(v, Parameter):
                    dinparam.append(v)
        if self.outvars is not None:
            for v in self.outvars:
                if isinstance(v, Field):
                    for _v in v if isinstance(v, VectorField) else (v,):
                        for vd in self.output_discrete_fields[_v]:
                            doutvar.append(vd)
                        gh = self.output_discrete_fields[_v].build_ghost_exchanger()
                        if gh is not None and self._do_update_ghosts:
                            self.ghost_exchanger.append(gh)
                elif isinstance(v, Parameter):
                    doutparam.append(v)
        self.dinvar, self.doutvar = tuple(dinvar), tuple(doutvar)
        self.dinparam, self.doutparam = tuple(dinparam), tuple(doutparam)

    @classmethod
    def supports_mpi(cls):
        return True
