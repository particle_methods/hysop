# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import abstractmethod
from hysop.constants import SpectralTransformAction
from hysop.tools.numpywrappers import npw
from hysop.tools.htypes import check_instance, first_not_None, to_tuple
from hysop.tools.decorators import debug
from hysop.tools.numerics import float_to_complex_dtype
from hysop.fields.continuous_field import Field
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.operator.base.spectral_operator import SpectralOperatorBase
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.fields.continuous_field import Field
from hysop.symbolic.relational import Assignment
from hysop.core.memory.memory_request import MemoryRequest


class CurlOperatorBase(SpectralOperatorBase):
    """
    Compute the curl using a specific implementation.
    """

    @debug
    def __new__(cls, Fin, Fout, variables, **kwds):
        return super().__new__(cls, input_fields=None, output_fields=None, **kwds)

    @debug
    def __init__(self, Fin, Fout, variables, **kwds):
        """
        Create an operator that computes the curl of an input field Fin.

        Given Fin, a 2D ScalarField, a 2D VectorField or a 3D VectorField, compute Fout = curl(Fin).

        Only the following configurations are supported:
                 dim   nb_components  |   dim   nb_components
        Input:    2        (1,2)      |    3          3
        Output:   2        (2,1)      |    3          3

        Parameters
        ----------
        Fin: hysop.field.continuous_field.Field
            Continuous field as input ScalarField or VectorField.
            All contained field have to live on the same domain.
        Fout: hysop.field.continuous_field.Field
            Continuous field as output VectorField.
            All contained field have to live on the same domain.
        variables: dict
            dictionary of fields as keys and topologies as values.
        kwds: dict, optional
            Extra parameters passed towards base class (MultiSpaceDerivatives).
        """

        check_instance(Fin, Field)
        check_instance(Fout, Field)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)

        if Fout.domain is not Fin.domain:
            raise RuntimeError("Only one domain is supported.")
        if variables[Fout] != variables[Fin]:
            raise RuntimeError("Only one topology is supported")
        if Fout.dtype != Fin.dtype:
            raise RuntimeError("Datatype mismatch between Fout and Fin.")

        # check fields
        dim = Fin.dim
        in_components = Fin.nb_components
        out_components = Fout.nb_components
        if dim == 2:
            if in_components == 1:
                if out_components != 2:
                    msg = f"Fout component mistmach, got {out_components} components but expected 2."
                    raise RuntimeError(msg)
            elif in_components == 2:
                if out_components != 1:
                    msg = f"Fout component mistmach, got {out_components} components but expected 1."
                    raise RuntimeError(msg)
            else:
                msg = f"Fin component mistmach, got {in_components} components but expected 1 or 2."
                raise RuntimeError(msg)
        elif dim == 3:
            if in_components != 3:
                msg = f"Fin component mistmach, got {in_components} components but expected 3."
                raise RuntimeError(msg)
            if out_components != 3:
                msg = f"Fout component mistmach, got {out_components} components but expected 3."
                raise RuntimeError(msg)
        else:
            msg = f"Unsupported dimension {dim}."
            raise RuntimeError(msg)

        # input and output fields
        input_fields = {Fin: variables[Fin]}
        output_fields = {Fout: variables[Fout]}

        super().__init__(input_fields=input_fields, output_fields=output_fields, **kwds)

        self.Fin = Fin
        self.Fout = Fout
        self.dim = dim

    @debug
    def discretize(self):
        if self.discretized:
            return
        super().discretize()
        self.dFin = self.get_input_discrete_field(self.Fin)
        self.dFout = self.get_output_discrete_field(self.Fout)


class SpectralCurlOperatorBase(CurlOperatorBase):
    """
    Compute the curl using a specific spectral implementation.
    """

    def __new__(cls, **kwds):
        return super().__new__(cls, **kwds)

    def __init__(self, **kwds):
        super().__init__(**kwds)

        dim = self.dim
        Fin, Fout = self.Fin, self.Fout
        assert Fin is not Fout, "Cannot compute curl inplace!"

        if dim == 2:
            tg0 = self.new_transform_group()
            tg1 = self.new_transform_group()
            if Fin.nb_components == 1:
                assert Fout.nb_components == 2
                F0 = tg0.require_forward_transform(
                    Fin, axes=0, custom_output_buffer="auto"
                )
                B0 = tg0.require_backward_transform(
                    Fout[0],
                    axes=0,
                    custom_input_buffer="auto",
                    matching_forward_transform=F0,
                )

                F1 = tg1.require_forward_transform(
                    Fin, axes=1, custom_output_buffer="auto"
                )
                B1 = tg1.require_backward_transform(
                    Fout[1],
                    axes=1,
                    custom_input_buffer="auto",
                    matching_forward_transform=F1,
                )

                expr0 = Assignment(B0.s, +F0.s.diff(F0.s.frame.coords[1]))
                expr1 = Assignment(B1.s, -F1.s.diff(F1.s.frame.coords[0]))
            elif Fin.nb_components == 2:
                F0 = tg0.require_forward_transform(
                    Fin[1], axes=1, custom_output_buffer="auto"
                )
                B0 = tg0.require_backward_transform(
                    Fout,
                    axes=1,
                    custom_input_buffer="auto",
                    matching_forward_transform=F0,
                )

                F1 = tg1.require_forward_transform(
                    Fin[0], axes=0, custom_output_buffer="auto"
                )
                B1 = tg1.require_backward_transform(
                    Fout,
                    axes=0,
                    custom_input_buffer="auto",
                    matching_forward_transform=F1,
                    action=SpectralTransformAction.ACCUMULATE,
                )

                expr0 = Assignment(B0.s, +F0.s.diff(F0.s.frame.coords[0]))
                expr1 = Assignment(B1.s, -F1.s.diff(F1.s.frame.coords[1]))
            else:
                raise NotImplementedError
            (K0,) = tg0.push_expressions(expr0)
            (K1,) = tg1.push_expressions(expr1)
            K = ((tg0, K0), (tg1, K1))
            forward_transforms = (F0, F1)
            backward_transforms = (B0, B1)
        elif dim == 3:
            tg0 = self.new_transform_group()
            tg1 = self.new_transform_group()
            tg2 = self.new_transform_group()
            if Fin.nb_components == 3:
                assert Fout.nb_components == 3
                F0 = tg1.require_forward_transform(
                    Fin[2], axes=1, custom_output_buffer="auto"
                )
                B0 = tg1.require_backward_transform(
                    Fout[0],
                    axes=1,
                    custom_input_buffer="auto",
                    matching_forward_transform=F0,
                )

                F1 = tg0.require_forward_transform(
                    Fin[0], axes=0, custom_output_buffer="auto"
                )
                B1 = tg0.require_backward_transform(
                    Fout[1],
                    axes=0,
                    custom_input_buffer="auto",
                    matching_forward_transform=F1,
                )

                F2 = tg2.require_forward_transform(
                    Fin[1], axes=2, custom_output_buffer="auto"
                )
                B2 = tg2.require_backward_transform(
                    Fout[2],
                    axes=2,
                    custom_input_buffer="auto",
                    matching_forward_transform=F2,
                )

                F3 = tg0.require_forward_transform(
                    Fin[1], axes=0, custom_output_buffer="auto"
                )
                B3 = tg0.require_backward_transform(
                    Fout[0],
                    axes=0,
                    custom_input_buffer="auto",
                    matching_forward_transform=F3,
                    action=SpectralTransformAction.ACCUMULATE,
                )

                F4 = tg2.require_forward_transform(
                    Fin[2], axes=2, custom_output_buffer="auto"
                )
                B4 = tg2.require_backward_transform(
                    Fout[1],
                    axes=2,
                    custom_input_buffer="auto",
                    matching_forward_transform=F4,
                    action=SpectralTransformAction.ACCUMULATE,
                )

                F5 = tg1.require_forward_transform(
                    Fin[0], axes=1, custom_output_buffer="auto"
                )
                B5 = tg1.require_backward_transform(
                    Fout[2],
                    axes=1,
                    custom_input_buffer="auto",
                    matching_forward_transform=F5,
                    action=SpectralTransformAction.ACCUMULATE,
                )
            else:
                raise NotImplementedError
            expr0 = Assignment(B0.s, +F0.s.diff(F0.s.frame.coords[1]))
            expr1 = Assignment(B1.s, +F1.s.diff(F1.s.frame.coords[2]))
            expr2 = Assignment(B2.s, +F2.s.diff(F2.s.frame.coords[0]))
            expr3 = Assignment(B3.s, -F3.s.diff(F3.s.frame.coords[2]))
            expr4 = Assignment(B4.s, -F4.s.diff(F4.s.frame.coords[0]))
            expr5 = Assignment(B5.s, -F5.s.diff(F5.s.frame.coords[1]))
            (K0,) = tg1.push_expressions(expr0)
            (K1,) = tg0.push_expressions(expr1)
            (K2,) = tg2.push_expressions(expr2)
            (K3,) = tg0.push_expressions(expr3)
            (K4,) = tg2.push_expressions(expr4)
            (K5,) = tg1.push_expressions(expr5)
            K = ((tg1, K0), (tg0, K1), (tg2, K2), (tg0, K3), (tg2, K4), (tg1, K5))
            forward_transforms = (F0, F1, F2, F3, F4, F5)
            backward_transforms = (B0, B1, B2, B3, B4, B5)
        else:
            raise NotImplementedError

        self.forward_transforms = forward_transforms
        self.backward_transforms = backward_transforms
        self.K = K

    @debug
    def discretize(self):
        if self.discretized:
            return
        super().discretize()
        dFin, dFout = self.dFin, self.dFout
        K = self.K

        dK = ()
        for tg, Ki in K:
            _, dKi, _ = tg.discrete_wave_numbers[Ki]
            dK += (dKi,)
        self.dK = dK

    def setup(self, work):
        super().setup(work)

        # extract buffers
        FIN = tuple(Ft.full_output_buffer for Ft in self.forward_transforms)
        FOUT = FIN

        self.FIN = FIN
        self.FOUT = FOUT
