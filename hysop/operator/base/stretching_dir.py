# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
import sympy as sm

from hysop.tools.numpywrappers import npw
from hysop.tools.decorators import debug
from hysop.constants import StretchingFormulation
from hysop.backend.host.host_directional_operator import HostDirectionalOperator
from hysop.constants import DirectionLabels, Implementation, StretchingFormulation
from hysop.tools.htypes import (
    check_instance,
    to_tuple,
    to_list,
    first_not_None,
    InstanceOf,
)
from hysop.fields.continuous_field import Field
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.methods import TimeIntegrator, SpaceDiscretization
from hysop.numerics.odesolvers.runge_kutta import ExplicitRungeKutta, Euler
from hysop.numerics.stencil.stencil_generator import (
    StencilGenerator,
    CenteredStencilGenerator,
    MPQ,
)


class DirectionalStretchingBase:

    __default_method = {
        TimeIntegrator: Euler,
        StretchingFormulation: StretchingFormulation.GRAD_UW,
        SpaceDiscretization: SpaceDiscretization.FDC4,
    }

    __available_methods = {
        TimeIntegrator: InstanceOf(ExplicitRungeKutta),
        StretchingFormulation: InstanceOf(StretchingFormulation),
        SpaceDiscretization: (InstanceOf(SpaceDiscretization), InstanceOf(int)),
    }

    @classmethod
    def default_method(cls):
        dm = super().default_method()
        dm.update(cls.__default_method)
        return dm

    @classmethod
    def available_methods(cls):
        am = super().available_methods()
        am.update(cls.__available_methods)
        return am

    @debug
    def __new__(
        cls,
        formulation,
        velocity,
        vorticity,
        variables,
        dt,
        C=None,
        A=None,
        implementation=None,
        base_kwds=None,
        **kwds,
    ):
        return super().__new__(
            cls,
            input_fields=None,
            output_fields=None,
            input_params=None,
            output_params=None,
            **kwds,
        )

    @debug
    def __init__(
        self,
        formulation,
        velocity,
        vorticity,
        variables,
        dt,
        C=None,
        A=None,
        implementation=None,
        base_kwds=None,
        **kwds,
    ):
        """
        Initialize directional stretching operator python backend.

        Solves dW/dt = C * f(U,W)
        where U is the velocity, W the vorticity and f is one of the following formulation:
            CONSERVATIVE   FORMULATION: f(U,W) = div( U : W )
            MIXED GRADIENT FORMULATION: f(U,W) = (A*grad(U) + (1-A)*grad(U)^T) . W

        where : represents the outer product U:W = (Ui*Wj)ij.
              . represents the matrix-vector product.
             ^T is the transposition operator

        U and W are always three dimensional fields.
        C is scalar or 3d vector-like of symbolic coefficients.
        A is a scalar symbolic coefficient.
        f(U,W) is always directionally splittable.


        Parameters
        ----------
        formulation: hysop.constants.StretchingFormulation
            The formulation of this stretching operator:
            CONSERVATIVE:  f(U,W) = div( U : W )
            GRAD_UW:       f(U,W) = grad(U) . W
            GRAD_UW_T:     f(U,W) = grad(U)^T . W
            MIXED_GRAD_UW: f(U,W) = (A*grad(U) + (1-A)*grad(U)^T) . W
              where A is a user given scalar or 3d vector like that contains
              scalar (floating point) coefficient(s) or parameter(s).
              The most common mixed gradient formulation is A=0.5.

            Only CONSERVATIVE formulation is implemented yet in python.
        velocity: Field
            Velocity U as read-only input three-dimensional continuous field.
        vorticity: Field
            Vorticity W as read-write three-dimensional continuous field.
        variables: dict
            Dictionary of fields as keys and topology descriptors as values.
        dt: ScalarParameter
            Timestep parameter that will be used for time integration.
        C: scalar or vector like of 3 symbolic coefficients, optional
            The stretching leading coefficient C can be scalar or vector like.
            Contained values should be numerical coefficients, parameters or
            generic symbolic expressions such that C*f(U,W) is directionally splittable.
            Here * is the classical elementwise multiplication.
            Default value is 1.
        A: scalar symbolic coefficient, optional
            Should only be given for MIXED_GRAD_UW formulations.
            ValueError will be raised on other formulations.
            The linear combination coefficients A is a scalar.
            Contained value should be a numerical coefficient, a parameter
            (or a generic symbolic expression) such that C*(A*grad(U).W + (1-A)*grad^T(U).W)
            is directionally splittable.
            Here * is the classical elementwise multiplication.
            Default value is 0.5.
        name: str, optional, defaults to 'stretching'.
            Name of this stretching operator.
        implementation: Implementation, optional, defaults to None
            Target implementation, should be contained in available_implementations().
            If None, implementation will be set to default_implementation().
        base_kwds: dict, optional, defaults to None
            Base class keywords arguments.
            If None, an empty dict will be passed.
        kwds:
            Keywords arguments that will be passed towards implementation
            operator __init__.
        """

        check_instance(formulation, StretchingFormulation)
        check_instance(velocity, Field)
        check_instance(vorticity, Field)
        check_instance(dt, ScalarParameter)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(base_kwds, dict, keys=str, allow_none=True)

        if (velocity.dim != 3) or (velocity.nb_components != 3):
            msg = "Given velocity is not a 3d vector field."
            raise ValueError(msg)
        if (vorticity.dim != 3) or (vorticity.nb_components != 3):
            msg = "Given vorticity is not a 3d vector field."
            raise ValueError(msg)

        base_kwds = first_not_None(base_kwds, {})
        C = first_not_None(C, sm.Integer(1))

        if isinstance(C, npw.ndarray):
            assert C.ndim in (0, 1), C.ndim
            assert C.size in (1, 3), C.size

        assert (
            formulation is StretchingFormulation.CONSERVATIVE
        ), "Only CONSERVATIVE formulation is implemented yet."

        input_fields = {velocity: variables[velocity], vorticity: variables[vorticity]}
        output_fields = {vorticity: variables[vorticity]}
        input_params = {dt}
        output_params = set()

        super().__init__(
            input_fields=input_fields,
            output_fields=output_fields,
            input_params=input_params,
            output_params=output_params,
            **kwds,
        )

        self.velocity = velocity
        self.vorticity = vorticity
        self.C = C
        self.dt = dt
        self.formulation = formulation

    @debug
    def handle_method(self, method):
        super().handle_method(method)
        self.time_integrator = method.pop(TimeIntegrator)
        self.space_discretization = method.pop(SpaceDiscretization)
        if StretchingFormulation in method.keys():
            self.formulation = method.pop(StretchingFormulation)
        try:
            self.order = int(str(self.space_discretization)[3:])
        except:
            self.order = self.space_discretization
        csg = CenteredStencilGenerator()
        csg.configure(dtype=MPQ, dim=1)
        stencil = csg.generate_exact_stencil(derivative=1, order=self.order)
        self.stencil = stencil

    @debug
    def get_field_requirements(self):
        requirements = super().get_field_requirements()
        stencil = self.stencil
        G = max(stencil.L, stencil.R)[0]
        S = self.time_integrator.stages
        d = 2 - self.splitting_direction  # assume stretching is always 3D
        for is_input, (field, td, req) in requirements.iter_requirements():
            ghosts = req.min_ghosts.copy()
            min_ghosts = req.min_ghosts.copy()
            min_ghosts[d] = max(min_ghosts[d], G * S)
            req.min_ghosts = min_ghosts
        return requirements

    @debug
    def discretize(self):
        if self.discretized:
            return
        super().discretize()
        self.dvelocity = self.get_input_discrete_field(self.velocity)
        self.dvorticity = self.get_input_discrete_field(self.vorticity)

    @debug
    def get_work_properties(self):
        requests = super().get_work_properties()
        return requests

    ## Backend methods
    # DirectionalOperatorBase
    @classmethod
    def supported_dimensions(cls):
        return (3,)
