# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABCMeta
import numpy as np

from hysop.tools.htypes import check_instance, first_not_None, InstanceOf
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import Field
from hysop.parameters.tensor_parameter import TensorParameter
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.core.memory.memory_request import MemoryRequest
from hysop.constants import ResidualError
from hysop.tools.numpywrappers import npw


class ConvergenceBase:
    """Common implementation interface for Convergence operator"""

    __default_method = {ResidualError: ResidualError.ABSOLUTE}

    __available_methods = {ResidualError: (InstanceOf(ResidualError),)}

    @classmethod
    def default_method(cls):
        dm = super().default_method()
        dm.update(cls.__default_method)
        return dm

    @classmethod
    def available_methods(cls):
        am = super().available_methods()
        am.update(cls.__available_methods)
        return am

    @debug
    def __new__(
        cls, variables, convergence=None, u_old=None, implementation=None, **kwds
    ):
        return super().__new__(cls, input_fields=None, output_params=None, **kwds)

    @debug
    def __init__(
        self, variables, convergence=None, u_old=None, implementation=None, **kwds
    ):
        r"""
        variables: dict
            dictionary of fields as keys and topologies as values.

        convergence: TensorParameter (optional)
            Parameter to store the components of ||u-u_old||_\infty or ||u-u_old||_\infty/||u||_\infty
        implementation: Implementation, optional, defaults to None
            target implementation, should be contained in available_implementations().
            If None, implementation will be set to default_implementation().
        kwds:
            Extra keywords arguments that will be passed towards implementation
            enstrophy operator __init__.
        """
        check_instance(convergence, TensorParameter, allow_none=True)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(u_old, Field, allow_none=True)

        field = next(iter(variables))
        if convergence is None:
            convergence = TensorParameter(
                name="|residual|",
                dtype=field.dtype,
                shape=(field.nb_components,),
                is_read_only=False,
                quiet=False,
                initial_value=np.asarray(
                    [
                        np.finfo(field.dtype).max,
                    ]
                    * field.nb_components
                ),
            )

        input_fields = {field: variables[field]}
        output_params = {convergence}

        self.field = field
        self.convergence = convergence
        self.u_old = u_old

        if not u_old is None:
            input_fields.update({u_old: variables[u_old]})

        super().__init__(input_fields=input_fields, output_params=output_params, **kwds)

    @debug
    def handle_method(self, method):
        super().handle_method(method)
        self._residual_computation = method.pop(ResidualError)

    @debug
    def discretize(self):
        if self.discretized:
            return
        super().discretize()
        self.dField = self.get_input_discrete_field(self.field)
        if not self.u_old is None:
            self.dField_old = self.get_input_discrete_field(self.u_old).compute_buffers

    @debug
    def setup(self, work):
        super().setup(work)
        if self.u_old is None:
            self.dField_old = tuple(
                npw.zeros(_.shape) for _ in self.dField.compute_buffers
            )
