# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABCMeta, abstractmethod
from hysop.constants import SpectralTransformAction
from hysop.tools.numpywrappers import npw
from hysop.tools.htypes import check_instance, first_not_None, to_tuple
from hysop.tools.decorators import debug
from hysop.tools.numerics import float_to_complex_dtype
from hysop.fields.continuous_field import Field, ScalarField
from hysop.operator.base.spectral_operator import SpectralOperatorBase
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.fields.continuous_field import Field
from hysop.symbolic.relational import Assignment
from hysop.core.memory.memory_request import MemoryRequest
from hysop.core.graph.graph import op_apply
from hysop.parameters.tensor_parameter import TensorParameter
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.tools.interface import NamedObjectI


class ExternalForce(NamedObjectI, metaclass=ABCMeta):
    """Interface to implement a custom external force."""

    def __new__(cls, name, dim, Fext, **kwds):
        return super().__new__(cls, name=name, **kwds)

    def __init__(self, name, dim, Fext, **kwds):
        super().__init__(name=name, **kwds)

        check_instance(dim, int)
        self._dim = dim
        self._Fext = Fext

        for f in self.input_fields():
            msg = "Expected field dimension to be {} but got {} from field {}."
            msg = msg.format(dim, f.dim, f.short_description())
            assert f.dim == dim, msg

    @property
    def Fext(self):
        return self._Fext

    @property
    def dim(self):
        return self._dim

    @property
    def diffusion(self):
        return self._diffusion

    @abstractmethod
    def input_fields(self):
        pass

    @abstractmethod
    def output_fields(self):
        pass

    @abstractmethod
    def input_params(self):
        pass

    @abstractmethod
    def output_params(self):
        pass

    @abstractmethod
    def initialize(self, op):
        pass

    @abstractmethod
    def discretize(self, op):
        pass

    @abstractmethod
    def get_mem_requests(self, op):
        pass

    @abstractmethod
    def pre_setup(self, op, work):
        pass

    @abstractmethod
    def post_setup(self, op, work):
        pass

    @abstractmethod
    def apply(self, op, **kwds):
        pass


class SpectralExternalForceOperatorBase(SpectralOperatorBase):
    """
    Compute the curl of a symbolic expression and perfom Euler time integration.
    """

    @debug
    def __new__(
        cls,
        vorticity,
        Fext,
        dt,
        variables,
        Fmin=None,
        Fmax=None,
        Finf=None,
        implementation=None,
        **kwds,
    ):
        return super().__new__(
            cls,
            input_fields=None,
            output_fields=None,
            input_params=None,
            output_params=None,
            **kwds,
        )

    @debug
    def __init__(
        self,
        vorticity,
        Fext,
        dt,
        variables,
        Fmin=None,
        Fmax=None,
        Finf=None,
        implementation=None,
        **kwds,
    ):
        """
        Create an operator that computes the curl of a given input force field Fext.

        Only the following configurations are supported:
                     dim   nb_components  |   dim   nb_components
        vorticity:    2          1        |    3          3

        What is computed:
            force  = curl(Fext) by using a spectral backend
            Fmin = min(force)
            Fmax = max(force)
            Finf = max(abs(Fmin), abs(Fmax))
            W   += dt*force

        where Fext is computed from user given ExternalForce.

        Parameters
        ----------
        vorticity: hysop.field.continuous_field.Field
            Continuous field as input ScalarField or VectorField.
            All contained field have to live on the same domain.
        Fext: hysop.operator.external_force.ExternalForce
            Expression of the external force.
        F...: TensorParameter or boolean, optional
            TensorParameters should match the shape of tmp (see Notes).
            If set to True, the TensorParameter will be generated automatically.
        variables: dict
            dictionary of fields as keys and topologies as values.
        kwds: dict, optional
            Extra parameters passed towards base class (MultiSpaceDerivatives).

        Notes
        -----
        If dim == 2, it is expected that:
            vorticity has only one component
            Fext has 2 components
        Else if dim == 3:
            vorticity has 3 components
            Fext has 3 components
        """
        check_instance(vorticity, Field)
        check_instance(Fext, ExternalForce)
        check_instance(dt, ScalarParameter)
        check_instance(Fmin, (ScalarParameter, TensorParameter), allow_none=True)
        check_instance(Fmax, (ScalarParameter, TensorParameter), allow_none=True)
        check_instance(Finf, (ScalarParameter, TensorParameter), allow_none=True)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)

        if Fmin is not None:
            Fmin.value = npw.asarray((1e8,) * vorticity.nb_components, dtype=Fmin.dtype)
        if Fmax is not None:
            Fmax.value = npw.asarray((1e8,) * vorticity.nb_components, dtype=Fmax.dtype)
        if Finf is not None:
            Finf.value = npw.asarray((1e8,) * vorticity.nb_components, dtype=Finf.dtype)

        # check fields
        dim = vorticity.dim
        w_components = vorticity.nb_components
        f_components = Fext.dim

        if dim == 2:
            if w_components != 1:
                msg = "Vorticity components mistmach, got {} components but expected 1."
                msg = msg.format(w_components)
                raise RuntimeError(msg)
            if f_components != 2:
                msg = "External force components mistmach, got {} components but expected 2."
                msg = msg.format(f_components)
                raise RuntimeError(msg)
            pshape = (1,)
        elif dim == 3:
            if w_components != 3:
                msg = "Vorticity components mistmach, got {} components but expected 3."
                msg = msg.format(w_components)
                raise RuntimeError(msg)
            if f_components != 3:
                msg = "External force components mistmach, got {} components but expected 3."
                msg = msg.format(f_components)
                raise RuntimeError(msg)
            pshape = (3,)
        else:
            msg = f"Unsupported dimension {dim}."
            raise RuntimeError(msg)

        msg = "TensorParameter shape mismatch, expected {} but got {{}} for parameter {{}}."
        msg = msg.format(pshape)
        if isinstance(Fmin, TensorParameter):
            assert Fmin.shape == pshape, msg.format(Fmin.shape, "Fmin")
        if isinstance(Fmax, TensorParameter):
            assert Fmin.shape == pshape, msg.format(Fmax.shape, "Fmax")
        if isinstance(Finf, TensorParameter):
            assert Fmin.shape == pshape, msg.format(Finf.shape, "Finf")

        compute_statistics = Fmin is not None
        compute_statistics |= Fmax is not None
        compute_statistics |= Finf is not None

        # input and output fields
        input_fields = Fext.input_fields()
        check_instance(input_fields, set, values=ScalarField)
        input_fields.add(vorticity)

        output_fields = Fext.output_fields()
        check_instance(output_fields, set, values=ScalarField)
        output_fields.add(vorticity)

        input_params = Fext.input_params()
        input_params.add(dt)

        output_params = Fext.output_params()
        output_params.update({Fmin, Fmax, Finf})
        output_params = output_params.difference({None})

        input_fields = {f: self.get_topo_descriptor(variables, f) for f in input_fields}
        output_fields = {
            f: self.get_topo_descriptor(variables, f) for f in output_fields
        }

        # TODO share tmp buffers for the whole tensor
        force = vorticity.tmp_like(name="Fext", ghosts=0, mem_tag="tmp_fext")
        for Fi, Wi in zip(force.fields, vorticity.fields):
            input_fields[Fi] = self.get_topo_descriptor(variables, Wi)
            output_fields[Fi] = self.get_topo_descriptor(variables, Wi)

        super().__init__(
            input_fields=input_fields,
            output_fields=output_fields,
            input_params=input_params,
            output_params=output_params,
            **kwds,
        )

        self.vorticity = vorticity
        self.Fext = Fext
        self.force = force
        self.dt = dt

        self.dim = dim
        self.w_components = w_components
        self.f_components = f_components

        self.Fmin = Fmin
        self.Fmax = Fmax
        self.Finf = Finf
        self.compute_statistics = compute_statistics

    ###################
    # from now on, we delegate everything to the ExternalForce implementation
    ###################
    def initialize(self, **kwds):
        if self.initialized:
            return
        self.Fext.initialize(self)
        return super().initialize(**kwds)

    @debug
    def discretize(self):
        if self.discretized:
            return
        super().discretize()
        self.dW = self.get_input_discrete_field(self.vorticity)
        self.dF = self.get_input_discrete_field(self.force)
        self.Fext.discretize(self)

    def get_work_properties(self):
        requests = super().get_work_properties()
        for name, request in self.Fext.get_mem_requests(self).items():
            requests.push_mem_request(name, request)
        return requests

    def setup(self, work):
        self.Fext.pre_setup(self, work)
        super().setup(work)
        self.Fext.post_setup(self, work)

    @op_apply
    def apply(self, **kwds):
        self.Fext.apply(self, **kwds)
