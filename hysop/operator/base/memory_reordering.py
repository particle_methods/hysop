# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABCMeta

from hysop.tools.htypes import check_instance, to_tuple, first_not_None
from hysop.tools.decorators import debug
from hysop.constants import MemoryOrdering
from hysop.fields.continuous_field import ScalarField

from hysop.core.memory.memory_request import MemoryRequest
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors


class MemoryReorderingBase(metaclass=ABCMeta):
    """
    Common implementation interface for memory reordering operators.
    """

    @debug
    def __new__(
        cls,
        input_field,
        output_field,
        variables,
        target_memory_order,
        name=None,
        pretty_name=None,
        **kwds,
    ):
        return super().__new__(
            cls,
            input_fields=None,
            output_fields=None,
            name=name,
            pretty_name=pretty_name,
            **kwds,
        )

    @debug
    def __init__(
        self,
        input_field,
        output_field,
        variables,
        target_memory_order,
        name=None,
        pretty_name=None,
        **kwds,
    ):
        """
        Initialize a memory reordering operator operating on CartesianTopologyDescriptors.

        Parameters
        ----------
        input_field: ScalarField
            Scalar input continuous field to be memory reordered.
        output_field: ScalarField
            Scalar output continuous field to be memory reordered.
        variables: dict
            Dictionary of ScalarField as keys and CartesianTopologyDescriptors as values.
            Should contain input and output scalar fields.
        target_memory_order: MemoryOrdering
            Target memory order to achieve.
        kwds:
            Keywords arguments that will be passed towards base.
        """
        check_instance(input_field, ScalarField)
        check_instance(output_field, ScalarField)
        check_instance(
            variables, dict, keys=ScalarField, values=CartesianTopologyDescriptors
        )
        check_instance(target_memory_order, MemoryOrdering)

        assert {input_field, output_field} == set(variables.keys())
        assert input_field.domain is output_field.domain
        assert target_memory_order in (
            MemoryOrdering.C_CONTIGUOUS,
            MemoryOrdering.F_CONTIGUOUS,
        )

        input_fields = {input_field: variables[input_field]}
        output_fields = {output_field: variables[output_field]}

        if target_memory_order is MemoryOrdering.C_CONTIGUOUS:
            mr = "F2C"
        elif target_memory_order is MemoryOrdering.F_CONTIGUOUS:
            mr = "C2F"
        else:
            raise NotImplementedError(target_memory_order)

        default_name = f"{mr}_{input_field.name}"
        default_pname = f"{mr}_{input_field.pretty_name}"
        if output_field.name != input_field.name:
            default_name += f"_{output_field.name}"
            default_pname += f"_{output_field.pretty_name}"

        name = first_not_None(name, default_name)
        pname = first_not_None(pretty_name, default_pname)

        super().__init__(
            input_fields=input_fields,
            output_fields=output_fields,
            name=name,
            pretty_name=pname,
            **kwds,
        )

        self.input_field = input_field
        self.output_field = output_field
        self.target_memory_order = target_memory_order

    @debug
    def get_field_requirements(self):
        requirements = super().get_field_requirements()
        for is_input, reqs in requirements.iter_requirements():
            if reqs is None:
                continue
            (field, td, req) = reqs
            if is_input:
                req.memory_order = MemoryOrdering.ANY
            else:
                req.memory_order = self.target_memory_order
        return requirements

    @debug
    def get_node_requirements(self):
        from hysop.core.graph.node_requirements import OperatorRequirements

        reqs = super().get_node_requirements()
        reqs.enforce_unique_memory_order = False
        return reqs

    def output_topology_state(self, output_field, input_topology_states):
        ostate = super().output_topology_state(
            output_field=output_field, input_topology_states=input_topology_states
        )
        assert len(input_topology_states) == 1
        istate = next(iter(input_topology_states.values()))
        ostate.memory_order = self.target_memory_order
        return ostate

    @classmethod
    def supports_mpi(cls):
        return True
