# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import abstractmethod
from hysop.tools.numpywrappers import npw
from hysop.tools.htypes import check_instance, first_not_None, to_tuple
from hysop.tools.decorators import debug
from hysop.tools.numerics import float_to_complex_dtype
from hysop.constants import FieldProjection
from hysop.core.memory.memory_request import MemoryRequest
from hysop.operator.base.spectral_operator import SpectralOperatorBase
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.fields.continuous_field import Field
from hysop.symbolic.field import laplacian, div, curl, grad


class SolenoidalProjectionOperatorBase(SpectralOperatorBase):
    """
    Solves solenoidal projection (project a 3d field F such that div(F)=0)
    """

    @debug
    def __new__(
        cls,
        input_field,
        output_field,
        variables,
        input_field_div=None,
        output_field_div=None,
        **kwds,
    ):
        return super().__new__(
            cls, input_fields=input_field, output_fields=output_field, **kwds
        )

    @debug
    def __init__(
        self,
        input_field,
        output_field,
        variables,
        input_field_div=None,
        output_field_div=None,
        **kwds,
    ):
        """
        SolenoidalProjection projects a 3D vector field
        onto the space of divergence free fields.

        Parameters
        ----------
        input_field:  :class:`~hysop.fields.continuous_field.Field`
            Field to be projected.
        output_field : :class:`~hysop.fields.continuous_field.Field
            Solution field.
        input_field_div: Field, optional
            Optionally compute input field divergence.
        output_field_div: Field, optional
            Optionally compute output field divergence.
        variables: dict
            dictionary of fields as keys and topologies as values.
        kwds :
            base class parameters.
        """

        check_instance(output_field, Field)
        check_instance(input_field, Field)
        check_instance(input_field_div, Field, allow_none=True)
        check_instance(output_field_div, Field, allow_none=True)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)

        assert output_field.domain is input_field.domain, "only one domain is supported"
        assert (
            variables[output_field] is variables[input_field]
        ), "only one topology is supported"

        out_topo = variables[output_field]
        in_topo = variables[input_field]
        input_fields = {input_field: in_topo}
        output_fields = {output_field: out_topo}
        if input_field_div is not None:
            output_fields[input_field_div] = variables[input_field_div]
        if output_field_div is not None:
            output_fields[output_field_div] = variables[output_field_div]

        dim = input_field.domain.dim
        icomp = input_field.nb_components
        ocomp = output_field.nb_components

        if input_field.domain != output_field.domain:
            msg = "input_field and output_field do not share the same domain."
            raise ValueError(msg)
        if dim != 3:
            msg = f"input_field dimension should be 3, got a {dim}D vector field."
            raise ValueError(msg)
        if icomp != 3:
            msg = f"input_field component mistmach, got {icomp} components but expected 3."
            raise RuntimeError(msg)
        if ocomp != 3:
            msg = f"output_field component mistmach, got {ocomp} components but expected 3."
            raise RuntimeError(msg)
        if (input_field_div is not None) and (input_field_div.nb_components != 1):
            msg = (
                "input_field_div component mistmach, got {} components but expected 1."
            )
            msg = msg.format(input_field_div.nb_components)
            raise RuntimeError(msg)
        if (output_field_div is not None) and (output_field_div.nb_components != 1):
            msg = (
                "input_field_div component mistmach, got {} components but expected 1."
            )
            msg = msg.format(output_field_div.nb_components)
            raise RuntimeError(msg)

        super().__init__(input_fields=input_fields, output_fields=output_fields, **kwds)

        Fin = input_field
        Fout = output_field
        compute_divFin = input_field_div is not None
        compute_divFout = output_field_div is not None

        tg = self.new_transform_group()
        forward_transforms = tg.require_forward_transform(Fin)
        backward_transforms = tg.require_backward_transform(Fout)

        Fts = npw.asarray([x.s for x in forward_transforms])
        Bts = npw.asarray([x.s for x in backward_transforms])

        kd1s, kd2s = (), ()
        for Wi in Fts:
            expr = grad(Wi, Wi.frame)
            kd1 = tuple(
                sorted(tg.push_expressions(*to_tuple(expr)), key=lambda k: k.axis)
            )
            expr = laplacian(Wi, Wi.frame)
            kd2 = tuple(sorted(tg.push_expressions(expr), key=lambda k: k.axis))
            kd1s += (kd1,)
            kd2s += (kd2,)

        if compute_divFin:
            backward_divFin_transform = tg.require_backward_transform(
                input_field_div, custom_input_buffer="B0"
            )
        else:
            backward_divFin_transform = None

        if compute_divFout:
            backward_divFout_transform = tg.require_backward_transform(
                output_field_div, custom_input_buffer="B0"
            )
        else:
            backward_divFout_transform = None

        self.Fin = input_field
        self.Fout = output_field
        self.divFin = input_field_div
        self.divFout = output_field_div

        self.compute_divFin = compute_divFout
        self.compute_divFout = compute_divFout

        self.tg = tg
        self.forward_transforms = forward_transforms
        self.backward_transforms = backward_transforms
        self.backward_divFin_transform = backward_divFin_transform
        self.backward_divFout_transform = backward_divFout_transform

        self.Bts = Bts
        self.Fts = Fts
        self.kd1s = kd1s
        self.kd2s = kd2s

    @debug
    def discretize(self):
        if self.discretized:
            return
        super().discretize()
        dFin = self.get_input_discrete_field(self.Fin)
        dFout = self.get_output_discrete_field(self.Fout)

        if self.compute_divFin:
            ddivFin = self.output_discrete_fields[self.divFin]
        else:
            ddivFin = None

        if self.compute_divFout:
            ddivFout = self.output_discrete_fields[self.divFout]
        else:
            ddivFout = None

        kd1s, kd2s = self.kd1s, self.kd2s
        dkd1s = ()
        for kd1 in kd1s:
            dkd1 = [
                None,
            ] * len(kd1)
            for wi in kd1:
                idx, dwi, _ = self.tg.discrete_wave_numbers[wi]
                dkd1[idx] = dwi
            dkd1s += (tuple(dkd1),)

        dkd2s = ()
        for kd2 in kd2s:
            dkd2 = [
                None,
            ] * len(kd1)
            for wi in kd2:
                idx, dwi, _ = self.tg.discrete_wave_numbers[wi]
                dkd2[idx] = dwi
            dkd2s += (tuple(dkd2),)

        self.dFin = dFin
        self.dFout = dFout
        self.ddivFin = ddivFin
        self.ddivFout = ddivFout
        self.dkd1s = tuple(dkd1s)
        self.dkd2s = tuple(dkd2s)

    def get_work_properties(self):
        requests = super().get_work_properties()
        for i, (Ft, Bt) in enumerate(
            zip(self.forward_transforms, self.backward_transforms)
        ):
            assert Ft.backend == Bt.backend
            assert Ft.output_dtype == Bt.input_dtype, (Ft.output_dtype, Bt.input_dtype)
            assert Ft.output_shape == Bt.input_shape, (Ft.output_shape, Bt.input_shape)
            shape = Ft.output_shape
            dtype = Ft.output_dtype
            request = MemoryRequest(
                backend=self.tg.backend,
                dtype=dtype,
                shape=shape,
                nb_components=1,
                alignment=self.min_fft_alignment,
            )
            requests.push_mem_request(f"fft_buffer_{i}", request)
        return requests

    def setup(self, work):
        dkd1s, dkd2s = self.dkd1s, self.dkd2s

        output_axes = self.forward_transforms[0].output_axes
        for i, (Ft, Bt) in enumerate(
            zip(self.forward_transforms, self.backward_transforms)
        ):
            (dtmp,) = work.get_buffer(self, f"fft_buffer_{i}")
            Ft.configure_output_buffer(dtmp)
            Bt.configure_input_buffer(dtmp)
            assert output_axes == Ft.output_axes
            assert output_axes == Bt.input_axes

        super().setup(work)

        reorder_fields = tuple(2 - i for i in output_axes)
        self.FIN = tuple(
            self.forward_transforms[i].full_output_buffer for i in reorder_fields
        )
        self.FOUT = tuple(
            self.backward_transforms[i].full_input_buffer for i in reorder_fields
        )

        self.K = sum((dkd1s[i] for i in reorder_fields), ())
        self.KK = sum((dkd2s[i] for i in reorder_fields), ())

        if self.compute_divFin:
            self.DIV_IN = (self.backward_divFin_transform.full_input_buffer,)
        else:
            self.DIV_IN = None

        if self.compute_divFout:
            self.DIV_OUT = (self.backward_divFout_transform.full_input_buffer,)
        else:
            self.DIV_OUT = None
