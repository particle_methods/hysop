# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABCMeta

from hysop.tools.htypes import check_instance, to_tuple, first_not_None
from hysop.tools.decorators import debug
from hysop.tools.numpywrappers import npw
from hysop.fields.continuous_field import Field

from hysop.core.memory.memory_request import MemoryRequest
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.parameters.scalar_parameter import ScalarParameter, TensorParameter
from hysop.parameters.default_parameters import VolumicIntegrationParameter


class IntegrateBase(metaclass=ABCMeta):
    """
    Common implementation interface for field integration.
    """

    @debug
    def __new__(
        cls,
        field,
        variables,
        name=None,
        pretty_name=None,
        cst=1,
        parameter=None,
        scaling=None,
        expr=None,
        **kwds,
    ):
        return super().__new__(
            cls,
            name=name,
            pretty_name=pretty_name,
            input_fields=None,
            output_params=None,
            **kwds,
        )

    @debug
    def __init__(
        self,
        field,
        variables,
        name=None,
        pretty_name=None,
        cst=1,
        parameter=None,
        scaling=None,
        expr=None,
        **kwds,
    ):
        """
        Initialize a Integrate operator base.

        Integrate a field on it compute domain and put the result in a parameter.

        in:  field
            Possibly as multi-component field that should be integrated.
        out: parameter
             P = scaling * integral_V(field)
             where V is the field domain volume
             and scaling depends on specified scaling method.

        parameter
        ----------
        field: Field
            Input continuous field to be integrated.
        variables: dict
            dictionary of fields as keys and topologies as values.
        parameter: ScalarParameter or TensorParameter
            The output parameter that will contain the integral.
            Should match field.nb_components.
            A default parameter will be created if not specified.
        scaling: None, float, str or array-like of str, optional
            Scaling method used after integration.
            'volumic':   scale by domain size (product of mesh space steps)
            'normalize': scale by first integration (first value will be 1.0)
            Can also be a custom float value of tuple of float values.
            Defaults to volumic integration.
        cst: float, optional
            Extra scaling constant for volumic mode.
        kwds:
            Extra keywords arguments that will be passed towards implementation
            enstrophy operator __init__.
        """

        check_instance(field, Field)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)

        scaling = first_not_None(scaling, "volumic")
        if isinstance(scaling, float):
            scaling = (scaling,) * field.nb_components

        # Generate parameter if not supplied.
        if parameter is None:
            parameter = VolumicIntegrationParameter(field=field)
        if parameter.size != field.nb_components:
            msg = "Expected a parameter of size {} but got a parameter of size {}."
            msg = msg.format(field.nb_components, parameter.size)
            raise RuntimeError(msg)

        check_instance(parameter, (ScalarParameter, TensorParameter))
        check_instance(scaling, (str, tuple))
        if isinstance(scaling, tuple):
            check_instance(scaling, tuple, values=float, size=field.nb_components)

        input_fields = {field: variables[field]}
        output_params = {parameter}

        default_name = f"integrate_{field.name}"
        default_pname = f"∫{field.pretty_name}"

        pretty_name = first_not_None(pretty_name, name, default_pname)
        name = first_not_None(name, default_name)

        self.field = field
        self.parameter = parameter
        self.scaling = scaling
        self.expr = expr
        self.cst = cst
        self.scaling_coeff = None

        super().__init__(
            name=name,
            pretty_name=pretty_name,
            input_fields=input_fields,
            output_params=output_params,
            **kwds,
        )

    @debug
    def discretize(self):
        if self.discretized:
            return
        super().discretize()
        dF = self.input_discrete_fields[self.field]

        scaling = self.scaling
        if scaling == "volumic":
            scaling_coeff = (
                self.cst * npw.prod(dF.space_step) / npw.prod(dF.domain.length)
            )
            scaling_coeff = (scaling_coeff,) * dF.nb_components
        elif scaling == "normalize":
            scaling_coeff = [
                None,
            ] * dF.nb_components
        elif isinstance(scaling, tuple):
            scaling_coeff = tuple(scaling)
        else:
            msg = f"Unknown scaling method {self.scaling}"
            raise ValueError(msg)

        assert len(scaling_coeff) == dF.nb_components

        self.dF = dF
        self.scaling_coeff = scaling_coeff

        # Collect values from all MPI process
        if self.mpi_params.size == 1:
            self._collect = lambda e: e
        else:
            comm = self.mpi_params.comm
            self._sendbuff = npw.zeros(
                shape=(self.parameter.size,), dtype=self.parameter.dtype
            )
            self._recvbuff = npw.zeros(
                shape=(self.parameter.size,), dtype=self.parameter.dtype
            )

            def _collect(v):
                self._sendbuff[...] = v
                comm.Allreduce(self._sendbuff, self._recvbuff)
                return self._recvbuff

            self._collect = _collect

    @classmethod
    def supports_mpi(cls):
        return True
