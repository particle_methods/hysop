# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import Field
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.operator.base.poisson import PoissonOperatorBase
from hysop.symbolic.relational import Assignment
from hysop.symbolic.field import laplacian
from hysop.parameters.scalar_parameter import ScalarParameter


class DiffusionOperatorBase(PoissonOperatorBase):
    """
    Common base for spectral diffusion operator.
    """

    @debug
    def __new__(cls, Fin, Fout, variables, nu, dt, name=None, pretty_name=None, **kwds):
        return super().__new__(
            cls,
            Fin=Fin,
            Fout=Fout,
            variables=variables,
            name=name,
            pretty_name=pretty_name,
            input_params=None,
            **kwds,
        )

    @debug
    def __init__(
        self, Fin, Fout, variables, nu, dt, name=None, pretty_name=None, **kwds
    ):
        """
        Diffusion operator base.

        Parameters
        ----------
        Fin : :class:`~hysop.fields.continuous_field.Field`
            The input field to be diffused.
        Fout:  :class:`~hysop.fields.continuous_field.Field`
            The output field to be diffused.
        variables: dictionary of fields:topology
            The choosed discretizations.
        nu: ScalarParameter.
            Diffusion coefficient.
        dt: ScalarParameter
            Timestep parameter that will be used for time integration.
        kargs:
            Base class parameters.

        Notes:
            *Equations:
                dF/dt = nu*Laplacian(F)
                in  = Win
                out = Wout

            *Implicit resolution in spectral space:
                F_hat(tn+1) = 1/(1-nu*dt*sum(Ki**2)) * F_hat(tn)
        """
        check_instance(nu, ScalarParameter)
        check_instance(dt, ScalarParameter)

        input_params = {dt, nu}

        default_name = f"Diffusion_{Fin.name}_{Fout.name}"
        default_pretty_name = f"Diffusion_{Fin.pretty_name}_{Fout.pretty_name}"
        name = first_not_None(name, default_name)
        pretty_name = first_not_None(name, default_pretty_name)

        super().__init__(
            Fin=Fin,
            Fout=Fout,
            variables=variables,
            name=name,
            pretty_name=pretty_name,
            input_params=input_params,
            **kwds,
        )

        self.nu = nu
        self.dt = dt
