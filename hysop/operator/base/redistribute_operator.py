# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABCMeta, abstractmethod
from hysop.tools.decorators import debug
from hysop.tools.htypes import check_instance, to_set, first_not_None
from hysop.tools.sympy_utils import subscript
from hysop.constants import Backend
from hysop.core.graph.computational_operator import ComputationalGraphOperator
from hysop.topology.topology import Topology
from hysop.fields.continuous_field import ScalarField


class RedistributeOperatorBase(ComputationalGraphOperator, metaclass=ABCMeta):
    """
    Abstract interface to redistribute operators.
    """

    @abstractmethod
    def can_redistribute(cls, source_topo, target_topo):
        """
        Return true if this RedistributeOperatorBase can be applied
        to redistribute a variable from source_topo to target_topo,
        else return False.
        """
        pass

    @classmethod
    def supported_backends(cls):
        """
        return the backends that this operator's topologies can support.
        """
        return Backend.all

    def __new__(
        cls,
        variable,
        source_topo,
        target_topo,
        components=None,
        name=None,
        pretty_name=None,
        **kwds,
    ):
        return super().__new__(
            cls,
            name=name,
            pretty_name=pretty_name,
            input_fields=None,
            output_fields=None,
            **kwds,
        )

    def __init__(
        self,
        variable,
        source_topo,
        target_topo,
        components=None,
        name=None,
        pretty_name=None,
        **kwds,
    ):
        """
        Parameters
        ----------
        variable: :class:`~hysop.field.continuous.ScalarField`
            the variable to be distributed
        source_topo: :class:`~hysop.topology.topology.Topology`
            source mesh topology
        target_topo: :class:`~hysop.topology.topology.Topology`
            target mesh topology
        name: str, optional
            name of this operator
        pretty_name: str, optional
            pretty name of this operator
        """
        # allow none only for Intertask fake init (see ComputationalGraph.push_nodes)
        check_instance(variable, ScalarField, allow_none=True)
        check_instance(source_topo, Topology, allow_none=True)
        check_instance(target_topo, Topology, allow_none=True)

        input_fields = {variable: source_topo}
        output_fields = {variable: target_topo}
        source_topo_id, target_topo_id = None, None
        try:
            source_topo_id = source_topo.id
        except AttributeError:
            pass
        try:
            target_topo_id = target_topo.id
        except AttributeError:
            pass

        default_name = f"R{source_topo_id},{target_topo_id}_{variable.name}"

        default_pname = "R{}{}{}_{}".format(
            subscript(source_topo_id),
            "→",
            subscript(target_topo_id),
            variable.pretty_name,
        )

        pretty_name = first_not_None(pretty_name, name, default_pname)
        name = first_not_None(name, default_name)

        if not "mpi_params" in kwds:
            assert source_topo.mpi_params == target_topo.mpi_params
            kwds.update({"mpi_params": target_topo.mpi_params})

        super().__init__(
            name=name,
            pretty_name=pretty_name,
            input_fields=input_fields,
            output_fields=output_fields,
            **kwds,
        )

        self.variable = variable
        self.dtype = variable.dtype

        self.source_topo = source_topo
        self.target_topo = target_topo

        # Set domain and mpi params
        self._set_domain_and_tasks()

    @debug
    def get_field_requirements(self):
        reqs = super().get_field_requirements()

        for field, ftopo in self.input_fields.items():
            if ftopo is not None:
                _, req = reqs.get_input_requirement(field)
                req.axes = None
                req.memory_order = None

        for field, ftopo in self.output_fields.items():
            if ftopo is not None:
                _, req = reqs.get_output_requirement(field)
                req.axes = None
                req.memory_order = None

        return reqs

    @debug
    def get_node_requirements(self):
        from hysop.core.graph.node_requirements import OperatorRequirements

        reqs = super().get_node_requirements()
        reqs.enforce_unique_topology_shape = False
        return reqs

    @debug
    def initialize(self, topgraph_method=None, **kwds):
        super().initialize(topgraph_method)

    @classmethod
    def supports_multiple_field_topologies(cls):
        return True

    @classmethod
    def supports_multiple_topologies(cls):
        return True

    @classmethod
    def supports_mpi(cls):
        return True

    def get_preserved_input_fields(self):
        return set(self.input_fields.keys())

    def available_methods(self):
        return {}

    def default_method(self):
        return {}

    def handle_method(self, method):
        super().handle_method(method)
