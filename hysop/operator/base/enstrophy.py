# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABCMeta

from hysop.tools.htypes import check_instance, to_tuple, first_not_None
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import Field
from hysop.tools.numpywrappers import npw

from hysop.core.memory.memory_request import MemoryRequest
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.parameters.scalar_parameter import ScalarParameter


class EnstrophyBase(metaclass=ABCMeta):
    """
    Common implementation interface for enstrophy.
    """

    @debug
    def __new__(
        cls,
        vorticity,
        enstrophy,
        WdotW,
        rho,
        rho_0,
        variables,
        name=None,
        pretty_name=None,
        **kwds,
    ):
        return super().__new__(
            cls,
            input_fields=None,
            output_fields=None,
            output_params=None,
            name=name,
            pretty_name=pretty_name,
            **kwds,
        )

    @debug
    def __init__(
        self,
        vorticity,
        enstrophy,
        WdotW,
        rho,
        rho_0,
        variables,
        name=None,
        pretty_name=None,
        **kwds,
    ):
        """
        Initialize a enstrophy operator operating on CartesianTopologyDescriptors.

        vorticity: Field
            Input continuous vorticity field.
        enstrophy: ScalarParameter
            Enstrophy scalar output parameter.
        rho: Field, optional
            Input continuous density field, if not given,
            defaults to 1.0 on the whole domain.
        rho_0: float, optional
            Reference density, defaults to 1.0.
        WdotW: Field, optional
            Output continuous field, will contain W.W (term before integration).
            If WdotW is given, WdotW will contain W.W else this will be
            a temporary field only usable during this operator's apply method.
            Should have nb_components=1 and the same domain and discretization
            as the vorticity.
        variables: dict
            dictionary of fields as keys and topologies as values.
        kwds:
            Keywords arguments that will be passed towards implementation
            poisson operator __init__.
        """

        check_instance(vorticity, Field)
        check_instance(enstrophy, ScalarParameter)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(WdotW, Field, allow_none=True)
        check_instance(rho, Field, allow_none=True)
        check_instance(rho_0, float)

        W = vorticity.pretty_name
        wdotw = f"{W}⋅{W}"
        if WdotW is None:
            assert vorticity in variables, variables
            WdotW = vorticity.tmp_like(name="WdotW", pretty_name=wdotw, nb_components=1)
            variables[WdotW] = variables[vorticity]
            assert WdotW.dtype == vorticity.dtype

        input_fields = {vorticity: variables[vorticity]}
        output_fields = {WdotW: variables[WdotW]}
        output_params = {enstrophy}

        if rho is not None:
            input_fields[rho] = variables[rho]

        default_name = "enstrophy"
        default_pname = f"∫{wdotw}"

        pretty_name = first_not_None(pretty_name, name, default_pname)
        name = first_not_None(name, default_name)

        self.vorticity = vorticity
        self.rho = rho
        self.rho_0 = rho_0
        self.WdotW = WdotW
        self.enstrophy = enstrophy

        super().__init__(
            input_fields=input_fields,
            output_fields=output_fields,
            output_params=output_params,
            name=name,
            pretty_name=pretty_name,
            **kwds,
        )

    @debug
    def discretize(self):
        if self.discretized:
            return
        super().discretize()
        self.dWin = self.get_input_discrete_field(self.vorticity)
        self.dWdotW = self.get_output_discrete_field(self.WdotW)

        self.coeff = npw.prod(self.dWdotW.space_step)
        self.coeff /= self.rho_0 * npw.prod(self.dWdotW.domain.length)

        # Collect values from all MPI process
        if self.mpi_params.size == 1:
            self._collect = lambda e: e
        else:
            comm = self.mpi_params.comm
            self._sendbuff = npw.zeros((1,))
            self._recvbuff = npw.zeros((1,))

            def _collect(local_enstrophy):
                self._sendbuff[0] = local_enstrophy
                comm.Allreduce(self._sendbuff, self._recvbuff)
                return self._recvbuff[0]

            self._collect = _collect

    @classmethod
    def supports_mpi(cls):
        return True
