# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABCMeta

from hysop.constants import DirectionLabels
from hysop.tools.htypes import check_instance, to_tuple, first_not_None
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import ScalarField

from hysop.core.memory.memory_request import MemoryRequest
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors


class TransposeOperatorBase(metaclass=ABCMeta):
    """
    Common implementation interface for transposition operators.
    """

    @debug
    def __new__(
        cls,
        input_field,
        output_field,
        variables,
        axes,
        name=None,
        pretty_name=None,
        **kwds,
    ):
        return super().__new__(
            cls,
            input_fields=None,
            output_fields=None,
            name=name,
            pretty_name=pretty_name,
            **kwds,
        )

    @debug
    def __init__(
        self,
        input_field,
        output_field,
        variables,
        axes,
        name=None,
        pretty_name=None,
        **kwds,
    ):
        """
        Initialize a transposition operator operating on CartesianTopologyDescriptors.

        input_field: ScalarField
            Input continuous scalar field to be transposed, at least 2D.
        output_field: ScalarField
            Output continuous scalar field where the result is stored
            Transposed shape should match the input.
            output_field can be the same as input_field resulting in
            an inplace transposition.
        variables: dict
            Dictionary of fields as keys and CartesianTopologyDescriptors as values.
            Should contain input and output field.
        axes: tuple or list of ints
            Permutation of axes in numpy notations
            Axe dim-1 is the contiguous axe, axe 0 has the greatest stride in memory.
        kwds: dict
            Base class keyword arguments.
        """
        check_instance(input_field, ScalarField)
        check_instance(output_field, ScalarField)
        check_instance(
            variables, dict, keys=ScalarField, values=CartesianTopologyDescriptors
        )
        check_instance(axes, (list, tuple), values=int)

        assert {input_field, output_field} == set(variables.keys())
        assert input_field.domain is output_field.domain

        dim = input_field.domain.dim
        nb_components = input_field.nb_components
        assert dim >= 2
        assert set(axes) == set(range(dim))
        assert tuple(axes) != tuple(range(dim))

        input_fields = {input_field: variables[input_field]}
        output_fields = {output_field: variables[output_field]}

        saxes = "".join([DirectionLabels[i] for i in axes]).lower()
        default_name = f"T{saxes}_{input_field.name}"
        default_pname = f"T{saxes}_{input_field.pretty_name}"
        if output_field.name != input_field.name:
            default_name += f"_{output_field.name}"
            default_pname += f"_{output_field.pretty_name}"

        name = first_not_None(name, default_name)
        pname = first_not_None(pretty_name, default_pname)

        super().__init__(
            input_fields=input_fields,
            output_fields=output_fields,
            name=name,
            pretty_name=pname,
            **kwds,
        )

        self.input_field = input_field
        self.output_field = output_field
        self.nb_components = nb_components
        self.dim = dim
        self.axes = axes

    @debug
    def get_node_requirements(self):
        from hysop.core.graph.node_requirements import OperatorRequirements

        reqs = super().get_node_requirements()
        reqs.enforce_unique_transposition_state = False
        return reqs

    def output_topology_state(self, output_field, input_topology_states):
        ostate = super().output_topology_state(
            output_field=output_field, input_topology_states=input_topology_states
        )
        assert len(input_topology_states) == 1
        istate = next(iter(input_topology_states.values()))
        axes = self.axes
        ostate.axes = tuple(istate.axes[i] for i in axes)
        return ostate

    @debug
    def discretize(self):
        if self.discretized:
            return
        super().discretize()
        self.din = self.get_input_discrete_field(self.input_field)
        self.dout = self.get_output_discrete_field(self.output_field)
        self.is_inplace = self.din.dfield is self.dout.dfield

    @debug
    def get_work_properties(self):
        requests = super().get_work_properties()

        if self.is_inplace:
            request = MemoryRequest.empty_like(a=self.dout, nb_components=1)
            requests.push_mem_request("tmp", request)

        return requests

    @debug
    def setup(self, work):
        super().setup(work)
        if work is None:
            raise ValueError("work is None.")
        if self.is_inplace:
            (self.dtmp,) = work.get_buffer(self, "tmp")

    @staticmethod
    def get_preferred_axes(src_topo, dst_topo, candidate_axes):
        """
        Return preferred transposition scheme (performance-wise)
        given source and destination topology and possible
        candidate transposition schemes.

        Candidate_axes is a dictionnary containing permutation
        as keys (tuple of ints), and target transposition state
        (hysop.constants.TranspositionState) as values.

        Target transposition states may be None when not provided
        by user.

        By default, return first axes candidate.
        If some permutation can reset the field to its default
        transposition state it is returned instead.
        """
        from hysop.tools.transposition_states import TranspositionState

        assert candidate_axes, "candidate axes is None or empty."
        dim = len(next(iter(candidate_axes)))
        tstates = TranspositionState[dim]
        check_instance(candidate_axes, dict, keys=tuple, values=(tstates, type(None)))

        if tstates.default in candidate_axes.values():
            idx = candidate_axes.values().index(tstates.default)
        else:
            idx = 0

        axes = tuple(candidate_axes.keys())[idx]
        return axes
