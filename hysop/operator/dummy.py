# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Dummy operator.
To be used for topology fixing.
"""
from hysop.core.graph.computational_node_frontend import ComputationalGraphNodeFrontend
from hysop.constants import Implementation
from hysop.backend.host.host_operator import HostOperator
from hysop.testsenv import __HAS_OPENCL_BACKEND__

if __HAS_OPENCL_BACKEND__:
    from hysop.backend.device.opencl.opencl_operator import OpenClOperator
from hysop.tools.htypes import check_instance
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.fields.continuous_field import Field
from hysop.tools.decorators import debug
from hysop.core.graph.graph import op_apply


class PythonDummy(HostOperator):

    @debug
    def __new__(cls, variables, **kwds):
        return super().__new__(cls, input_fields=None, output_fields=None, **kwds)

    @debug
    def __init__(self, variables, **kwds):
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        input_fields = variables
        output_fields = variables
        super().__init__(input_fields=input_fields, output_fields=output_fields, **kwds)

    @debug
    def get_field_requirements(self):
        requirements = super().get_field_requirements()
        for is_input, reqs in requirements.iter_requirements():
            if reqs is None:
                continue
            (field, td, req) = reqs
            req.axes = (tuple(range(field.dim)),)
        return requirements

    @op_apply
    def apply(self, **kwds):
        super().apply(**kwds)
        # Here doing nothing

    @classmethod
    def supports_mpi(cls):
        return True


if __HAS_OPENCL_BACKEND__:

    class OpenClDummy(OpenClOperator):

        @debug
        def __new__(cls, variables, **kwds):
            return super().__new__(cls, input_fields=None, output_fields=None, **kwds)

        @debug
        def __init__(self, variables, **kwds):
            check_instance(
                variables, dict, keys=Field, values=CartesianTopologyDescriptors
            )
            input_fields = variables
            output_fields = variables
            super().__init__(
                input_fields=input_fields, output_fields=output_fields, **kwds
            )

        @debug
        def get_field_requirements(self):
            requirements = super().get_field_requirements()
            for is_input, reqs in requirements.iter_requirements():
                if reqs is None:
                    continue
                (field, td, req) = reqs
                req.axes = ((0, 1, 2),)
            return requirements

        @op_apply
        def apply(self, **kwds):
            super().apply(**kwds)
            # Here doing nothing

        @classmethod
        def supports_mpi(cls):
            return True

else:
    OpenClDummy = PythonDummy


class Dummy(ComputationalGraphNodeFrontend):

    __implementations = {
        Implementation.PYTHON: PythonDummy,
        Implementation.OPENCL: OpenClDummy,
    }

    @classmethod
    def implementations(cls):
        return cls.__implementations

    @classmethod
    def default_implementation(cls):
        return Implementation.PYTHON
