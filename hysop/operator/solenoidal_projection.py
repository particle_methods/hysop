# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
@file solenoidal_projection.py
SolenoidalProjection solver frontend.
"""
from hysop.constants import Implementation
from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.henum import EnumFactory
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import Field
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.testsenv import __HAS_OPENCL_BACKEND__

from hysop.operator.base.spectral_operator import SpectralComputationalGraphNodeFrontend
from hysop.backend.host.python.operator.solenoidal_projection import (
    PythonSolenoidalProjection,
)


class SolenoidalProjection(SpectralComputationalGraphNodeFrontend):
    """
    Interface for solenoidal projection (project a 3d field F such that div(F)=0)
    Available implementations are:
        *PYTHON (numpy fft based solver)
    """

    @classmethod
    def implementations(cls):
        __implementations = {
            Implementation.PYTHON: PythonSolenoidalProjection,
        }
        if __HAS_OPENCL_BACKEND__:
            from hysop.backend.device.opencl.operator.solenoidal_projection import (
                OpenClSolenoidalProjection,
            )

            __implementations.update(
                {
                    Implementation.OPENCL: OpenClSolenoidalProjection,
                }
            )
        return __implementations

    @classmethod
    def default_implementation(cls):
        return Implementation.PYTHON

    @debug
    def __new__(
        cls,
        input_field,
        output_field,
        variables,
        input_field_div=None,
        output_field_div=None,
        implementation=None,
        base_kwds=None,
        **kwds,
    ):
        return super().__new__(
            cls,
            input_field=input_field,
            output_field=output_field,
            input_field_div=input_field_div,
            output_field_div=output_field_div,
            variables=variables,
            base_kwds=base_kwds,
            implementation=implementation,
            **kwds,
        )

    @debug
    def __init__(
        self,
        input_field,
        output_field,
        variables,
        input_field_div=None,
        output_field_div=None,
        implementation=None,
        base_kwds=None,
        **kwds,
    ):
        """
        Initialize a SolenoidalProjection operator frontend for 3D solenoidal projection.

        Fin  (3d input_field)
        Fout (3d output_field)

        Solves
           laplacian(Fout) = laplacian(Fin) - grad(div(Fin))

        Parameters
        ----------
        input_field: Field
            input continuous field (all components)
        output_field: Field
            output continuous field (all components)
        input_field_div: Field, optional
            Optionally compute input field divergence.
        output_field_div: Field, optional
            Optionally compute output field divergence.
        variables: dict
            dictionary of fields as keys and topologies as values.
        implementation: Implementation, optional, defaults to None
            target implementation, should be contained in available_implementations().
            If None, implementation will be set to default_implementation().
        base_kwds: dict, optional, defaults to None
            Base class keywords arguments.
            If None, an empty dict will be passed.
        kwds:
            Keywords arguments that will be passed towards implementation
            poisson operator __init__.

        Notes
        -----
        A SolenoidalProjection operator implementation should inherit from
        hysop.operator.base.solenoidal_projection.SolenoidalProjectionBase.
        """
        base_kwds = first_not_None(base_kwds, {})

        check_instance(input_field, Field)
        check_instance(output_field, Field)
        check_instance(input_field_div, Field, allow_none=True)
        check_instance(output_field_div, Field, allow_none=True)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(base_kwds, dict, keys=str)

        dim = input_field.domain.dim
        icomp = input_field.nb_components
        ocomp = output_field.nb_components

        if input_field.domain != output_field.domain:
            msg = "input_field and output_field do not share the same domain."
            raise ValueError(msg)
        if dim != 3:
            msg = f"input_field dimension should be 3, got a {dim}D vector field."
            raise ValueError(msg)
        if icomp != 3:
            msg = f"input_field component mistmach, got {icomp} components but expected 3."
            raise RuntimeError(msg)
        if ocomp != 3:
            msg = f"output_field component mistmach, got {ocomp} components but expected 3."
            raise RuntimeError(msg)
        if (input_field_div is not None) and (input_field_div.nb_components != 1):
            msg = (
                "input_field_div component mistmach, got {} components but expected 1."
            )
            msg = msg.format(input_field_div.nb_components)
            raise RuntimeError(msg)
        if (output_field_div is not None) and (output_field_div.nb_components != 1):
            msg = (
                "input_field_div component mistmach, got {} components but expected 1."
            )
            msg = msg.format(output_field_div.nb_components)
            raise RuntimeError(msg)

        super().__init__(
            input_field=input_field,
            output_field=output_field,
            input_field_div=input_field_div,
            output_field_div=output_field_div,
            variables=variables,
            base_kwds=base_kwds,
            implementation=implementation,
            **kwds,
        )
