# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""I/O operators

.. currentmodule:: hysop.operator.hdf_io

* :class:`~HDF_Writer` : operator to write fields into an hdf file
* :class:`~HDF_Reader` : operator to read fields from an hdf file
* :class:`~ComputeMeanField` abstract interface for hdf io classes

"""
import os
from hysop.tools.decorators import debug
from hysop.tools.htypes import check_instance, first_not_None, to_tuple, to_list
from hysop.tools.numpywrappers import npw
from hysop.tools.io_utils import IO, IOParams
from hysop.constants import Backend, TranspositionState
from hysop.core.graph.graph import op_apply
from hysop.core.graph.computational_graph import ComputationalGraphOperator
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.fields.continuous_field import Field
from hysop.backend.host.host_operator import HostOperatorBase


class ComputeMeanField(HostOperatorBase):
    """
    Interface to compute the mean of a field in chosen axes.
    """

    @debug
    def __new__(cls, fields, variables, io_params, **kwds):
        return super().__new__(cls, input_fields=None, io_params=io_params, **kwds)

    @debug
    def __init__(self, fields, variables, io_params, **kwds):
        """
        Compute and write the mean of fields in given direction, possiblity on a subview.

        Parameters
        ----------
        fields: dict Field: (view, axes)
            Keys are the field, values represent underlying view and axes where to take the mean.
            View = array_like of slices, Ellipsis, or 2D-tuples.
            A tuple represent minimal and maximal physical coordinates.
        variables: dict
            dictionary of fields as keys and topologies as values.
        io_params : :class:`hysop.tools.io_utils.IOParams`
            File i/o config (filename, format ...)
            Filename is used as a subfolder.
        kwds: dict
            Base class arguments.
        """
        check_instance(fields, dict, keys=Field, values=tuple)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)

        input_fields = {field: variables[field] for field in fields.keys()}
        super().__init__(input_fields=input_fields, io_params=io_params, **kwds)
        self.averaged_fields = fields

    @debug
    def get_field_requirements(self):
        # set good transposition state and no ghosts (because of OPENCL mean)
        requirements = super().get_field_requirements()
        for is_input, ireq in requirements.iter_requirements():
            if ireq is None:
                continue
            (field, td, req) = ireq
            req.axes = (TranspositionState[field.dim].default_axes(),)
            req.min_ghosts = (0,) * field.dim
            req.max_ghosts = (0,) * field.dim
        return requirements

    def discretize(self):
        super().discretize()
        averaged_dfields = {}
        for field, (view, axes) in self.averaged_fields.items():
            dfield = self.input_discrete_fields[field]

            axes = to_tuple(first_not_None(axes, range(dfield.dim)))
            assert tuple(set(axes)) == axes, axes
            check_instance(axes, tuple, values=int, minsize=0, maxsize=field.dim)

            view = first_not_None(view, Ellipsis)
            if view is Ellipsis:
                view[field] = Ellipsis
                continue
            view = to_list(view)
            check_instance(
                view, list, values=(tuple, type(Ellipsis), slice), size=dfield.dim
            )
            for i, vi in enumerate(view):
                if isinstance(vi, tuple):
                    assert len(vi) == 2
                    Xmin, Xmax = vi
                    coords = dfield.mesh.local_coords[i]
                    try:
                        Imin = npw.where(coords >= Xmin)[0][0]
                        Imax = npw.where(coords < Xmax)[0][-1]
                        slc = slice(Imin, Imax)
                        view[i] = slc
                    except:
                        raise
                        raise ValueError("Wrong physical range.")
            check_instance(view, list, values=(type(Ellipsis), slice), size=dfield.dim)
            view = tuple(view[i] for i in range(dfield.dim) if (i not in axes))
            check_instance(
                view, tuple, values=(type(Ellipsis), slice), size=dfield.dim - len(axes)
            )
            averaged_dfields[dfield] = (view, axes)
        self.averaged_dfields = averaged_dfields

    def setup(self, work=None):
        super().setup(work=work)
        path = self.io_params.filename
        if not os.path.exists(path):
            os.makedirs(path)
        self.path = path
        self.write_counter = 0

    def filename(self, field, i):
        return f"{self.path}/{field.name}_{i}"

    @op_apply
    def apply(self, simulation, **kwds):
        if simulation is None:
            raise ValueError("Missing simulation value for monitoring.")
        if self.io_params.should_dump(simulation=simulation):
            for dfield, (view, axes) in self.averaged_dfields.items():
                filename = self.filename(dfield, self.write_counter)
                arrays = {}
                for i, data in enumerate(dfield.data):
                    data = data.mean(axis=axes)
                    data = data[view]
                    if dfield.nb_components == 1:
                        arrays["data"] = data
                    else:
                        arrays["data_" + str(i)] = data
                npw.savez_compressed(file=filename, **arrays)
            self.write_counter += 1

    @classmethod
    def supported_backends(cls):
        return Backend.all
