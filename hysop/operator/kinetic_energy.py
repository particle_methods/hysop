# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
@file kinetic energy.py
KineticEnergy solver frontend.
"""
from hysop.constants import Implementation
from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.henum import EnumFactory
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import Field
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.operator.enstrophy import Enstrophy


class KineticEnergy(Enstrophy):
    """
    Interface computing kinetic energy using the kinetic energy operators.
    Available implementations are:
        *OPENCL (gpu based implementation)
    """

    @debug
    def __new__(
        cls,
        velocity,
        kinetic_energy,
        variables,
        rho=None,
        rho_0=1.0,
        UdotU=None,
        implementation=None,
        base_kwds=None,
        **kwds,
    ):
        return super().__new__(
            cls,
            vorticity=velocity,
            rho=rho,
            enstrophy=kinetic_energy,
            WdotW=UdotU,
            rho_0=rho_0,
            variables=variables,
            base_kwds=base_kwds,
            implementation=implementation,
            **kwds,
        )

    @debug
    def __init__(
        self,
        velocity,
        kinetic_energy,
        variables,
        rho=None,
        rho_0=1.0,
        UdotU=None,
        implementation=None,
        base_kwds=None,
        **kwds,
    ):
        """
        Initialize a KineticEnergy operator frontend.

        KineticEnergy is the scaled volume average of rho*(U.U) on the domain where . represents
        the vector dot product).

        in:  U (velocity field)
             rho (density field, optional, defaults to 1.0 everywhere)
        out: E = 1.0/(2*V*rho_0) * integral(rho*(U.U)) => kinetic energy (scalar parameter)
             where V is the domain volume, rho_0 the reference density.

        Parameters
        ----------
        velocity: Field
            Input continuous velocity field.
        kinetic_energy: ScalarParameter
            KineticEnergy scalar output parameter.
        rho: Field, optional
            Input continuous density field, if not given,
            defaults to 1.0 on the whole domain.
        rho_0: float, optional
            Reference density, defaults to 1.0.
        UdotU: Field, optional
            Output continuous field if required, will contain rho * (U.U) (term before integration).
            If UdotU is given, UdotU will contain rho * (U.U) else this will be
            a temporary field only usable during this operator's apply method.
            Should have nb_components=1.
        variables: dict
            dictionary of fields as keys and topologies as values.
        implementation: Implementation, optional, defaults to None
            target implementation, should be contained in available_implementations().
            If None, implementation will be set to default_implementation().
        base_kwds: dict, optional, defaults to None
            Base class keywords arguments.
            If None, an empty dict will be passed.
        kwds:
            Extra keywords arguments that will be passed towards implementation
            operator __init__.
        """
        base_kwds = first_not_None(base_kwds, {})

        check_instance(velocity, Field)
        check_instance(kinetic_energy, ScalarParameter)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(base_kwds, dict, keys=str)
        check_instance(UdotU, Field, allow_none=True)
        check_instance(rho, Field, allow_none=True)
        check_instance(rho_0, float)

        super().__init__(
            vorticity=velocity,
            rho=rho,
            enstrophy=kinetic_energy,
            WdotW=UdotU,
            rho_0=rho_0,
            variables=variables,
            base_kwds=base_kwds,
            implementation=implementation,
            **kwds,
        )
