# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
@file advection.py
Advection operator generator.
"""
from hysop import __SCALES_ENABLED__
from hysop.constants import Implementation
from hysop.tools.htypes import check_instance, to_list, first_not_None
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import Field
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.core.graph.computational_node_frontend import ComputationalGraphNodeFrontend


class Advection(ComputationalGraphNodeFrontend):
    """
    Interface the Scales fortran advection solver.
    Available implementations are: Fortran

    Available remeshing formulas:

    * 'p_O2' : order 4 method, corrected to allow large CFL number,
      untagged particles
    * 'p_O4' : order 4 method, corrected to allow large CFL number,
      untagged particles
    * 'p_L2' : limited and corrected lambda 2
    * 'p_M4' : Lambda_2,1 (=M'4) 4 point formula
    * 'p_M6' (default) : Lambda_4,2 (=M'6) 6 point formula
    * 'p_M8' : M8prime formula
    * 'p_44' : Lambda_4,4 formula
    * 'p_64' : Lambda_6,4 formula
    * 'p_66' : Lambda_6,6 formula
    * 'p_84' : Lambda_8,4 formula

    Time integration:

    * Runge-Kutta 2nd order

    Splitting:

    Computations are performed with a dimensional splitting as follows:

    * 'strang' (2nd order):

      * X-dir, half time step
      * Y-dir, half time step
      * Z-dir, full time step
      * Z-dir, full time step
      * Y-dir, half time step
      * X-dir, half time step

    * 'classic' (1st order):

      * X-dir, full time step
      * Y-dir, full time step
      * Z-dir, full time step


    """

    @classmethod
    def implementations(cls):
        _implementations = dict()
        if __SCALES_ENABLED__:
            from hysop.backend.host.fortran.operator.scales_advection import (
                ScalesAdvection,
            )

            _implementations.update({Implementation.FORTRAN: ScalesAdvection})
        return _implementations

    @classmethod
    def default_implementation(cls):
        return Implementation.FORTRAN

    @debug
    def __new__(
        cls,
        velocity,
        advected_fields,
        variables,
        dt,
        advected_fields_out=None,
        implementation=None,
        base_kwds=None,
        **kwds,
    ):
        return super().__new__(
            cls,
            velocity=velocity,
            dt=dt,
            advected_fields_in=None,
            advected_fields_out=None,
            variables=variables,
            implementation=implementation,
            base_kwds=base_kwds,
            **kwds,
        )

    @debug
    def __init__(
        self,
        velocity,
        advected_fields,
        variables,
        dt,
        advected_fields_out=None,
        implementation=None,
        base_kwds=None,
        **kwds,
    ):
        """
        Initialize a DirectionalAdvectionFrontend.

        Parameters
        ----------
        velocity: Field
            continuous velocity field (all components)
        advected_fields: Field or array like of Fields
            instance or list of continuous fields to be advected.
        advected_fields_out: Field or array like of Field, optional, defaults to None
            advection output, if set to None, advection is done inplace
            on a per variable basis.
        variables: dict
            Dictionary of continuous fields as keys and topologies as values.
        dt: ScalarParameter
            Timestep parameter that will be used for time integration.
        implementation: implementation, optional, defaults to None
            target implementation, should be contained in available_implementations().
            If None, implementation will be set to default_implementation().
        base_kwds: dict, optional, defaults to None
            Base class keywords arguments.
            If None, an empty dict will be passed.
        kwds:
            Extra parameters passed to generated directional operators.

        Notes
        -----
        An implementation should at least support the following __init__ parameters:
            velocity, advected_fields, advected_fields_out, variables
            direction, splitting_dim
        Extra keywords parameters are passed through kwds.
        """
        advected_fields = to_list(advected_fields)
        assert len(set(advected_fields)) == len(advected_fields)

        if advected_fields_out is None:
            advected_fields_out = advected_fields
        else:
            advected_fields_out = to_list(advected_fields_out)
            assert len(advected_fields) == len(advected_fields_out)
            assert len(set(advected_fields_out)) == len(advected_fields_out)
            for i, field in enumerate(advected_fields_out):
                if field is None:
                    advected_fields_out[i] = advected_fields[i]

        advected_fields = tuple(advected_fields)
        advected_fields_out = tuple(advected_fields_out)
        base_kwds = first_not_None(base_kwds, {})

        check_instance(velocity, Field)
        check_instance(advected_fields, tuple, values=Field)
        check_instance(advected_fields_out, tuple, values=Field)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(base_kwds, dict, keys=str)
        check_instance(dt, ScalarParameter)

        super().__init__(
            velocity=velocity,
            dt=dt,
            advected_fields_in=advected_fields,
            advected_fields_out=advected_fields_out,
            variables=variables,
            implementation=implementation,
            base_kwds=base_kwds,
            **kwds,
        )
