# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
@file vorticity_absorption.py
"""
import sympy as sm

from hysop.constants import Implementation
from hysop.core.graph.computational_node_frontend import ComputationalGraphNodeFrontend
from hysop.tools.htypes import check_instance
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.parameters.tensor_parameter import TensorParameter
from hysop.fields.continuous_field import Field
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.backend.host.python.operator.vorticity_absorption import (
    PythonVorticityAbsorption,
)


class VorticityAbsorption(ComputationalGraphNodeFrontend):
    """
    The periodic boundary condition is modified at the outlet
    in the flow direction in order to discard
    in the dowstream region the eddies coming
    periodically from the outlet.
    The far field velocity is set to u_inf at the inlet.
    """

    __implementations = {Implementation.PYTHON: PythonVorticityAbsorption}

    @classmethod
    def implementations(cls):
        return cls.__implementations

    @classmethod
    def default_implementation(cls):
        return Implementation.PYTHON

    def __new__(
        cls,
        velocity,
        vorticity,
        dt,
        flowrate,
        start_coord,
        variables,
        custom_filter=None,
        implementation=None,
        **kwds,
    ):
        return super().__new__(
            cls,
            velocity=velocity,
            vorticity=vorticity,
            dt=dt,
            flowrate=flowrate,
            start_coord=start_coord,
            variables=variables,
            custom_filter=custom_filter,
            implementation=implementation,
            **kwds,
        )

    def __init__(
        self,
        velocity,
        vorticity,
        dt,
        flowrate,
        start_coord,
        variables,
        custom_filter=None,
        implementation=None,
        **kwds,
    ):
        """
        Parameters
        ----------
        velocity: field
            input velocity
        vorticity: field
            input vorticity
        flowrate : ScalarParameter
            penalization factor applied to all geometries.
        start_coord : coordinate of the absorption filter start (X coord)
        custom_filter : sympy expression
            The formula to be applied onto the field.
        variables: dict
            dictionary of fields as keys and topologies as values.
        dt: ScalarParameter
            Timestep parameter that will be used for time integration.
        implementation: Implementation, optional, defaults to None
            target implementation, should be contained in
            available_implementations().
            If None, implementation will be set to default_implementation().
        kwds:
            Keywords arguments that will be passed towards implementation
            poisson operator __init__.
        """
        check_instance(velocity, Field)
        check_instance(vorticity, Field)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(dt, ScalarParameter)
        check_instance(start_coord, float)
        check_instance(custom_filter, sm.Basic, allow_none=True)
        check_instance(flowrate, TensorParameter)
        super().__init__(
            velocity=velocity,
            vorticity=vorticity,
            dt=dt,
            flowrate=flowrate,
            start_coord=start_coord,
            custom_filter=custom_filter,
            variables=variables,
            implementation=implementation,
            **kwds,
        )
