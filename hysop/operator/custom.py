# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.constants import Implementation
from hysop.core.graph.computational_node_frontend import ComputationalGraphNodeFrontend
from hysop.parameters.parameter import Parameter
from hysop.tools.htypes import check_instance
from hysop.fields.continuous_field import Field
from hysop.tools.decorators import debug
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.backend import __HAS_OPENCL_BACKEND__


class CustomOperator(ComputationalGraphNodeFrontend):
    """
    Function should take parameters in the following order:
      1. all input fields
      2. all input parameters
      3. all output fields
      4. all output parameters
    Note that discrete fields are passed as arguments to the custom function.
    """

    @classmethod
    def implementations(cls):
        from hysop.backend.host.python.operator.custom import PythonCustomOperator

        __implementations = {
            Implementation.PYTHON: PythonCustomOperator,
        }
        if __HAS_OPENCL_BACKEND__:
            from hysop.backend.device.opencl.operator.custom import OpenClCustomOperator

            __implementations.update(
                {
                    Implementation.OPENCL: OpenClCustomOperator,
                }
            )
        return __implementations

    @classmethod
    def default_implementation(cls):
        return Implementation.PYTHON

    @debug
    def __new__(
        cls, func, invars=None, outvars=None, extra_args=None, ghosts=None, **kwds
    ):
        return super().__new__(
            cls,
            func=func,
            invars=invars,
            outvars=outvars,
            extra_args=extra_args,
            ghosts=ghosts,
            **kwds,
        )

    @debug
    def __init__(
        self, func, invars=None, outvars=None, extra_args=None, ghosts=None, **kwds
    ):
        check_instance(
            invars, (tuple, list), values=(Field, Parameter), allow_none=True
        )
        check_instance(
            outvars, (tuple, list), values=(Field, Parameter), allow_none=True
        )
        check_instance(extra_args, tuple, allow_none=True)
        check_instance(ghosts, int, allow_none=True)

        super().__init__(
            func=func,
            invars=invars,
            outvars=outvars,
            extra_args=extra_args,
            ghosts=ghosts,
            **kwds,
        )
