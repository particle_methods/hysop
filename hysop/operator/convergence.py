# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
@file convergence.py
Convergence operator.
"""

from hysop.tools.decorators import debug
from hysop.constants import Implementation
from hysop.tools.htypes import check_instance
from hysop.fields.continuous_field import Field
from hysop.parameters.tensor_parameter import TensorParameter
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.core.graph.computational_node_frontend import ComputationalGraphNodeFrontend


class Convergence(ComputationalGraphNodeFrontend):
    """
    Computes the convergence citeria for a given field.

    Available implementations are:
        *PYTHON
    """

    @classmethod
    def implementations(cls):
        from hysop.backend.host.python.operator.convergence import PythonConvergence

        implementations = {Implementation.PYTHON: PythonConvergence}
        return implementations

    @classmethod
    def default_implementation(cls):
        return Implementation.PYTHON

    @debug
    def __new__(
        cls,
        variables,
        error=None,
        convergence=None,
        u_old=None,
        implementation=None,
        **kwds,
    ):
        return super().__new__(
            cls,
            error=error,
            convergence=convergence,
            variables=variables,
            implementation=implementation,
            **kwds,
        )

    @debug
    def __init__(
        self,
        variables,
        error=None,
        convergence=None,
        u_old=None,
        implementation=None,
        **kwds,
    ):
        r"""Initialize a convergence operator.

        Computes ||u-u_old||_\infty or ||u-u_old||_\infty/||u||_\infty
        depending on method absolute or relative.


        variables: dict
            dictionary of fields as keys and topologies as values.
        u_old: Field (optional)
            Field to store u_old. This field is created if not given.
        convergence: TensorParameter (optional)
            Parameter to store the components of ||u-u_old||_\infty or ||u-u_old||_\infty/||u||_\infty
        implementation: Implementation, optional, defaults to None
            target implementation, should be contained in available_implementations().
            If None, implementation will be set to default_implementation().
        kwds:
            Extra keywords arguments that will be passed towards implementation
            enstrophy operator __init__.
        """
        check_instance(u_old, Field, allow_none=True)
        check_instance(convergence, TensorParameter, allow_none=True)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)

        super().__init__(
            u_old=u_old,
            convergence=convergence,
            variables=variables,
            implementation=implementation,
            **kwds,
        )
