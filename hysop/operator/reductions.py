# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import functools

from hysop.tools.htypes import to_tuple, check_instance, first_not_None
from hysop.tools.numpywrappers import npw
from hysop.tools.io_utils import IO
from hysop.core.graph.graph import op_apply
from hysop.core.graph.node_requirements import OperatorRequirements
from hysop.core.graph.computational_graph import ComputationalGraphOperator
from hysop.constants import TranspositionState, MemoryOrdering
from hysop.fields.continuous_field import ScalarField, TensorField
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.parameters.tensor_parameter import TensorParameter
from hysop.backend.host.host_operator import HostOperatorBase


class ParameterReduction(HostOperatorBase):
    """Perform an elementwise reduction operation accross multiple input parameters of the same shape."""

    @classmethod
    def supports_mpi(cls):
        return True

    def __new__(
        cls,
        input_parameters,
        output_parameter,
        operation,
        dtype=None,
        quiet=False,
        **kwds,
    ):
        return super().__new__(cls, **kwds)

    def __init__(
        self,
        input_parameters,
        output_parameter,
        operation,
        dtype=None,
        quiet=False,
        **kwds,
    ):
        check_instance(
            input_parameters,
            tuple,
            values=(ScalarParameter, TensorParameter),
            minsize=2,
        )
        check_instance(output_parameter, (str, ScalarParameter, TensorParameter))
        check_instance(operation, str)

        parameter_shapes = tuple(p.shape for p in input_parameters)
        pshape = parameter_shapes[0]
        if all(s != pshape for s in parameter_shapes):
            msg = "Expected parameters with the same shape as input but got shapes "
            msg += ", ".join(map(str, parameter_shapes))
            raise RuntimeError(msg)

        if isinstance(output_parameter, str):
            ParameterType = type(input_parameters[0])
            if dtype is None:
                dtype = npw.result_type(*tuple(p.dtype for p in input_parameters))
            output_parameter = ParameterType(
                name=output_parameter,
                pretty_name=output_parameter,
                dtype=dtype,
                shape=pshape,
                quiet=quiet,
            )

        if output_parameter.shape != pshape:
            msg = "Expected an output parameter with the same shape as inputs but got shapes "
            msg += f"{pshape} and {output_parameter.shape}."
            raise RuntimeError(msg)

        if operation == "min":
            reduction_operation = npw.minimum
        elif operation == "max":
            reduction_operation = npw.maximum
        elif operation == "sum":
            reduction_operation = npw.add
        else:
            raise NotImplementedError(operation)

        super().__init__(
            input_params=input_parameters, output_params=(output_parameter,), **kwds
        )
        self._reduction_operation = reduction_operation
        self._pvals = tuple(p.value for p in input_parameters)
        self._pout = output_parameter

    @property
    def reduced_parameter(self):
        return self._pout

    @op_apply
    def apply(self, **kwds):
        value = functools.reduce(
            self._reduction_operation, self._pvals[1:], self._pvals[0]
        )
        self._pout.set_value(value)
