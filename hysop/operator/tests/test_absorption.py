# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Test of vorticity absorption
"""
import random
import itertools as it
import sympy as sm

from hysop.constants import HYSOP_REAL
from hysop.testsenv import __ENABLE_LONG_TESTS__, __HAS_OPENCL_BACKEND__
from hysop.testsenv import opencl_failed, iter_clenv
from hysop.tools.contexts import printoptions
from hysop.tools.numerics import is_fp, is_integer
from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.io_utils import IO
from hysop.tools.numpywrappers import npw
from hysop.parameters.scalar_parameter import ScalarParameter, TensorParameter
from hysop.operator.vorticity_absorption import VorticityAbsorption
from hysop.constants import Implementation
import numpy as np

from hysop import Field, Box


class TestVorticityAbsorption:

    @classmethod
    def setup_class(
        cls, enable_extra_tests=__ENABLE_LONG_TESTS__, enable_debug_mode=False
    ):

        IO.set_default_path("/tmp/hysop_tests/test_vorticity_absorption")

        if enable_debug_mode:
            cls.size_min = 15
            cls.size_max = 16
        else:
            cls.size_min = 23
            cls.size_max = 87

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_debug_mode = enable_debug_mode

        cls.t = ScalarParameter(name="t", dtype=HYSOP_REAL)
        cls.dt = ScalarParameter(name="dt", dtype=HYSOP_REAL)
        cls.flowrate = TensorParameter(name="flowrate", dtype=HYSOP_REAL, shape=(3,))

    @classmethod
    def teardown_class(cls):
        pass

    @staticmethod
    def __random_init(data, coords, component):
        dtype = data.dtype
        if is_fp(dtype):
            data[...] = npw.random.random(size=data.shape).astype(dtype=dtype)
        else:
            msg = f"Unknown dtype {dtype}."
            raise NotImplementedError(msg)

    @staticmethod
    def __velo_init(data, coords, component):
        data[...] = [1, 0, 0][component]

    def _test(self, dim, dtype, size_min=None, size_max=None):
        enable_extra_tests = self.enable_extra_tests

        size_min = first_not_None(size_min, self.size_min)
        size_max = first_not_None(size_max, self.size_max)

        shape = tuple(
            npw.random.randint(low=size_min, high=size_max + 1, size=dim).tolist()
        )

        domain = Box(length=(1,) * dim)
        velo = Field(domain=domain, name="velo", dtype=dtype, nb_components=3)
        vorti = Field(domain=domain, name="vorti", dtype=dtype, nb_components=3)

        self._test_one(
            shape=shape,
            dim=dim,
            dtype=dtype,
            domain=domain,
            velo=velo,
            vorti=vorti,
            start_coord=random.random() * 0.7 + 0.1,
        )

    def _test_one(self, shape, dim, dtype, domain, velo, vorti, start_coord):
        print(
            "\nTesting {}D VorticityAbsorption: dtype={} shape={}".format(
                dim, dtype.__name__, shape
            )
        )

        self.t.value = random.random()
        self.dt.value = random.random()
        self.flowrate.value = npw.random.random(size=(3,))
        print(f" >Parameter t has been set to {self.t()}.")
        print(f" >Parameter dt has been set to {self.dt()}.")
        print(f" >Start coord : {start_coord}.")
        print(f" >Flowrate : {self.flowrate()}.")
        print(" >Testing all implementations:")

        implementations = VorticityAbsorption.implementations()
        variables = {velo: shape, vorti: shape}

        def iter_impl(impl):
            base_kwds = dict(
                velocity=velo,
                vorticity=vorti,
                dt=self.dt,
                variables=variables,
                implementation=impl,
                name=f"vorticity_absorption_{str(impl).lower()}",
            )
            if impl is Implementation.PYTHON:
                msg = "   *Python: "
                print(msg, end=" ")
                yield VorticityAbsorption(
                    start_coord=start_coord, flowrate=self.flowrate, **base_kwds
                )
                print()
            else:
                msg = f"Unknown implementation to test {impl}."
                raise NotImplementedError(msg)

        Fref = None
        for impl in implementations:
            for op in iter_impl(impl):
                op = op.build()
                dvelo = op.input_discrete_tensor_fields[velo]
                dvorti = op.input_discrete_tensor_fields[vorti]

                dvelo.initialize(self.__velo_init)
                dvorti.initialize(self.__random_init)
                if Fref is None:
                    Fref = tuple(data.get().handle.copy() for data in dvorti.data)
                    topo = dvorti.topology
                    mesh = topo.mesh
                    ind = [_ for _ in mesh.local_compute_slices]
                    start = mesh.local_origin.copy()
                    start[-1] = start_coord
                    ind[-1] = slice(
                        mesh.point_local_indices(start)[-1],
                        mesh.point_local_indices(mesh.global_end)[-1] + 1,
                    )
                    ind = tuple(ind)
                    coords = mesh.local_mesh_coords[0][:, :, ind[-1]]
                    eps = 10.0
                    xb, xe = coords[0, 0, 0], coords[0, 0, -1]
                    xc = xb + (xe - xb) / 2.0
                    wfilter = (
                        np.tanh(eps * (coords - xc)) - np.tanh(eps * (xe - xc))
                    ) / (np.tanh(eps * (xb - xc)) - np.tanh(eps * (xe - xc)))
                    wfilter_diff = (
                        eps
                        * (1.0 - (np.tanh(eps * (coords - xc))) ** 2)
                        / (np.tanh(eps * (xb - xc)) - np.tanh(eps * (xe - xc)))
                    )
                    # Apply filter
                    for w in Fref:
                        w[ind] *= wfilter
                    # flowrate correction
                    flowrate_req = self.flowrate()
                    Fref[1][ind] = flowrate_req[2] - dvelo.data[2][ind]
                    Fref[1][ind] *= wfilter_diff
                    Fref[2][ind] = dvelo.data[1][ind] - flowrate_req[1]
                    Fref[2][ind] *= wfilter_diff

                op.apply()
                Fout = tuple(data.get().handle.copy() for data in dvorti.data)
                self._check_output(impl, op, Fref, Fout)

    @classmethod
    def _check_output(cls, impl, op, dvorti, dout):
        check_instance(dvorti, tuple, values=npw.ndarray)
        check_instance(dout, tuple, values=npw.ndarray, size=len(dvorti))

        msg0 = "Reference field {} is not finite."
        for i, field in enumerate(dout):
            iname = f"F{i}"
            mask = npw.isfinite(field)
            if not mask.all():
                print()
                print(field)
                print()
                print(field[~mask])
                print()
                msg = msg0.format(iname)
                raise ValueError(msg)

        for i, (fout, fref) in enumerate(zip(dout, dvorti)):
            assert fout.dtype == fref.dtype
            assert fout.shape == fref.shape

            eps = npw.finfo(fout.dtype).eps
            dist = npw.abs(fout - fref)
            dinf = npw.max(dist)
            deps = int(npw.ceil(dinf / eps))
            if deps < 200:
                print(f"{deps}eps, ", end=" ")
                continue
            has_nan = npw.any(npw.isnan(fout))
            has_inf = npw.any(npw.isinf(fout))
            print()
            print()
            print(f"Test output comparisson failed for component {i}:")
            print(f" *has_nan: {has_nan}")
            print(f" *has_inf: {has_inf}")
            print(f" *dinf={dinf} ({deps} eps)")
            print()
            msg = f"Test failed on component {i} for implementation {impl}."
            raise RuntimeError(msg)

    def perform_tests(self):
        self._test(dim=3, dtype=npw.float32)
        self._test(dim=3, dtype=npw.float64)

    def test_3D_float32(self):
        self._test(dim=3, dtype=npw.float32)

    def test_3D_float64(self):
        self._test(dim=3, dtype=npw.float64)


if __name__ == "__main__":
    TestVorticityAbsorption.setup_class(
        enable_extra_tests=False, enable_debug_mode=False
    )

    test = TestVorticityAbsorption()

    with printoptions(
        threshold=10000,
        linewidth=240,
        nanstr="nan",
        infstr="inf",
        formatter={"float": lambda x: f"{x:>6.2f}"},
    ):
        test.perform_tests()

    TestVorticityAbsorption.teardown_class()
