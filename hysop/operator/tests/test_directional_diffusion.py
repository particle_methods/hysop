# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys

from hysop.tools.numpywrappers import npw
from hysop.testsenv import __ENABLE_LONG_TESTS__
from hysop.testsenv import iter_clenv
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.tools.contexts import printoptions
from hysop.tools.htypes import first_not_None, to_tuple
from hysop.tools.numerics import is_integer, is_fp
from hysop.tools.io_utils import IO
from hysop.tools.sympy_utils import enable_pretty_printing
from hysop.operator.directional.diffusion_dir import DirectionalDiffusion

from hysop import Field, Box, Simulation
from hysop.methods import Remesh, TimeIntegrator
from hysop.constants import (
    Implementation,
    DirectionLabels,
    ComputeGranularity,
    SpaceDiscretization,
    HYSOP_REAL,
)
from hysop.numerics.splitting.strang import StrangSplitting, StrangOrder
from hysop.numerics.remesh.remesh import RemeshKernel
from hysop.numerics.stencil.stencil_generator import (
    StencilGenerator,
    CenteredStencilGenerator,
    MPQ,
)
from hysop.numerics.odesolvers.runge_kutta import (
    TimeIntegrator,
    Euler,
    RK2,
    RK3,
    RK4,
    RK4_38,
)


class TestDirectionalDiffusionOperator:

    @classmethod
    def setup_class(
        cls, enable_extra_tests=__ENABLE_LONG_TESTS__, enable_debug_mode=False
    ):

        IO.set_default_path("/tmp/hysop_tests/test_directional_diffusion")

        if enable_debug_mode:
            cls.size_min = 8
            cls.size_max = 9
        else:
            cls.size_min = 11
            cls.size_max = 23

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_debug_mode = enable_debug_mode

    @classmethod
    def teardown_class(cls):
        pass

    def _test(self, dim, is_inplace, size_min=None, size_max=None):
        assert dim > 0

        # periodic boundaries removes one computational point
        # so we add one here.
        size_min = first_not_None(size_min, self.size_min) + 1
        size_max = first_not_None(size_max, self.size_max) + 1

        shape = tuple(
            npw.random.randint(low=size_min, high=size_max, size=dim).tolist()
        )

        if self.enable_extra_tests:
            flt_types = (npw.float32, npw.float64)
            time_integrators = (RK2, RK4)
            orders = (2, 4, 6)
        else:
            flt_types = (HYSOP_REAL,)
            time_integrators = (RK2,)
            orders = (4,)

        domain = Box(length=(2 * npw.pi,) * dim)
        for dtype in flt_types:
            Fin = Field(domain=domain, name="Fin", dtype=dtype, nb_components=3)
            Fout = Field(domain=domain, name="Fout", dtype=dtype, nb_components=3)
            coeffs = npw.random.rand(Fin.nb_components, dim).astype(dtype)
            for order in orders:
                for time_integrator in time_integrators:
                    self._test_one(
                        time_integrator=time_integrator,
                        order=order,
                        shape=shape,
                        dim=dim,
                        dtype=dtype,
                        is_inplace=is_inplace,
                        domain=domain,
                        Fin=Fin,
                        Fout=Fout,
                        coeffs=coeffs,
                    )
                    print()

    @staticmethod
    def __random_init(data, coords, dtype, component):
        shape = data.shape
        if is_integer(dtype):
            data[...] = npw.random.random_integers(low=0, high=255, size=shape)
        elif is_fp(dtype):
            data[...] = npw.random.random(size=data.shape)
        else:
            msg = f"Unknown dtype {dtype}."
            raise NotImplementedError(msg)

    @staticmethod
    def __scalar_init(data, coords, dtype, component):
        if is_fp(dtype):
            data[...] = 1
            for xi in coords:
                data[...] *= npw.cos(xi * (component + 1))
        else:
            msg = f"Unknown dtype {dtype}."
            raise NotImplementedError(msg)

    def _test_one(
        self,
        time_integrator,
        order,
        shape,
        dim,
        dtype,
        is_inplace,
        domain,
        Fin,
        Fout,
        coeffs,
    ):

        print(
            "\nTesting {}D DirectionalDiffusion_{}_FCD{}: inplace={} dtype={} shape={}".format(
                dim, time_integrator.name(), order, is_inplace, dtype.__name__, shape
            ),
            end=" ",
        )

        dt = ScalarParameter("dt0", initial_value=0.1, const=True, dtype=dtype)

        fin = Fin
        if is_inplace:
            fout = Fin
            variables = {Fin: shape}
        else:
            fout = Fout
            variables = {Fin: shape, Fout: shape}
            msg = "Out of place diffusion has not been implemented yet."
            raise NotImplementedError(msg)

        implementations = DirectionalDiffusion.implementations().keys()

        method = {TimeIntegrator: time_integrator, SpaceDiscretization: order}

        def iter_impl(impl):
            base_kwds = dict(
                fields=fin,
                coeffs=coeffs,
                dt=dt,
                variables=variables,
                implementation=impl,
                laplacian_formulation=False,
                method=method,
                name=f"test_diffusion_{str(impl).lower()}",
            )
            da = DirectionalDiffusion(**base_kwds)
            if impl is Implementation.OPENCL:
                for cl_env in iter_clenv():
                    msg = "platform {}, device {}".format(
                        cl_env.platform.name.strip(), cl_env.device.name.strip()
                    )
                    split = StrangSplitting(
                        splitting_dim=dim,
                        extra_kwds=dict(cl_env=cl_env),
                        order=StrangOrder.STRANG_SECOND_ORDER,
                    )
                    yield msg, da, split
            else:
                msg = f"Unknown implementation to test {impl}."
                raise NotImplementedError(msg)

        # Compare to other implementations
        reference_fields = {}

        for impl in implementations:
            print(f"\n >Implementation {impl}:", end=" ")
            for sop, op, split in iter_impl(impl):
                print(f"\n   *{sop}: ", end=" ")
                split.push_operators(op)
                if not is_inplace:
                    split.push_copy(dst=fin, src=fout)
                split = split.build()

                dfin = split.get_input_discrete_field(fin)
                dfout = split.get_output_discrete_field(fout)

                view = dfin.compute_slices

                reference = None

                sys.stdout.write(".")
                sys.stdout.flush()
                try:
                    dfout.initialize(self.__random_init, dtype=dfout.dtype)
                    dfin.initialize(self.__scalar_init, dtype=dfin.dtype)
                    S0 = dfin.integrate()

                    if not npw.all(npw.isfinite(S0)):
                        msg = f"Integral is not finite. Got {S0}."
                        raise RuntimeError(msg)

                    _input = tuple(
                        dfin.data[i].get().handle.copy()
                        for i in range(dfin.nb_components)
                    )

                    if reference is None:
                        reference = self._compute_reference(
                            fin=dfin,
                            dfin=_input,
                            dt=dt(),
                            dim=dim,
                            time_integrator=time_integrator,
                            order=order,
                            coeffs=coeffs,
                            view=view,
                        )
                    split.apply()

                    output = tuple(
                        dfout.data[i].get().handle.copy()[view]
                        for i in range(dfout.nb_components)
                    )

                    for i in range(dfout.nb_components):
                        if npw.may_share_memory(reference[i], output[i]):
                            msg = (
                                "Output and reference arrays may share the same memory."
                            )
                            raise RuntimeError(msg)
                        if reference[i].dtype != output[i].dtype:
                            msg = "Output dtype {} differs from the reference one {}."
                            msg = msg.format(output[i].dtype, reference[i].dtype)
                            raise RuntimeError(msg)
                        if reference[i].shape != output[i].shape:
                            msg = "Output shape {} differs from the reference one {}."
                            msg = msg.format(output[i].shape, reference[i].shape)
                            raise RuntimeError(msg)
                        mask = npw.isfinite(output[i])
                        if not mask.all():
                            msg = f"\nFATAL ERROR: Output is not finite on axis {i}.\n"
                            print(msg)
                            npw.fancy_print(
                                output[i],
                                replace_values={(lambda a: npw.isfinite(a)): "."},
                            )
                            raise RuntimeError(msg)
                        di = npw.abs(reference[i] - output[i])
                        max_di = npw.max(di)
                        neps = 1000
                        max_tol = neps * npw.finfo(dfout.dtype).eps
                        if max_di > max_tol:
                            print()
                            print("SCALAR INPUT")
                            print(_input[i])
                            print("SCALAR REFERENCE")
                            print(reference[i])
                            print("SCALAR OUTPUT")
                            print(output[i])
                            print("ABS(REF - OUT)")
                            npw.fancy_print(
                                di, replace_values={(lambda a: a < max_tol): "."}
                            )
                            print()
                            msg = f"Output did not match reference output for component {i}"
                            msg += f"\n > max computed distance was {max_di}."
                            msg += (
                                f"\n > max tolerence was set to {max_tol} ({neps} eps)."
                            )
                            raise RuntimeError(msg)

                    S1 = dfout.integrate()
                    if not npw.all(npw.isfinite(S1)):
                        msg = f"Integral is not finite. Got {S1}."
                        raise RuntimeError(msg)
                    if (npw.abs(S0 - S1) > max_tol).any():
                        msg = "Scalar was not conserved expected {} but got {}."
                        msg = msg.format(
                            to_tuple(S0, cast=float), to_tuple(S1, cast=float)
                        )
                        raise RuntimeError(msg)
                except:
                    sys.stdout.write("\bx\n\n")
                    sys.stdout.flush()
                    raise

    def _compute_reference(
        self, fin, dfin, dt, time_integrator, order, coeffs, dim, view
    ):

        dfin = tuple(f.copy() for f in dfin)

        csg = CenteredStencilGenerator()
        csg.configure(dtype=MPQ, dim=1)
        D2 = csg.generate_exact_stencil(derivative=2, order=order)

        directions = tuple(range(dim)) + tuple(range(dim))[::-1]
        dt_coeffs = (0.5,) * (2 * dim)
        Xin = {f"F{i}": dfin[i] for i in range(len(dfin))}
        views = {f"F{i}": view for i in range(len(dfin))}

        ghost_exchangers = tuple(
            fin.build_ghost_exchanger(directions=i, data=dfin) for i in range(dim)
        )

        for j, dt_coeff in zip(directions, dt_coeffs):
            ndir = dim - j - 1

            def rhs(out, X, **kwds):
                coeff = coeffs[:, j]
                for i in range(len(dfin)):
                    fi = f"F{i}"
                    Fi_in = X[fi]
                    d2Fi_dxj = out[fi]
                    d2Fi_dxj[...] = (
                        D2(
                            a=Fi_in,
                            out=None,
                            axis=ndir,
                            symbols={D2.dx: fin.space_step[ndir]},
                        )
                        * coeff[i]
                    )

            out = time_integrator(Xin=Xin, RHS=rhs, dt=(dt_coeff * dt))
            for i in range(len(dfin)):
                fi = f"F{i}"
                dfin[i][...] = out[fi]
            ghost_exchangers[ndir].exchange_ghosts()

        out = tuple(f[view] for f in dfin)
        return out

    # def test_diffusion_1D_out_of_place(self):
    #     self._test(dim=1, is_inplace=False)
    # def test_diffusion_2D_out_of_place(self):
    #     self._test(dim=2, is_inplace=False)
    # def test_diffusion_3D_out_of_place(self):
    #     self._test(dim=3, is_inplace=False)

    def test_diffusion_1D_inplace(self):
        self._test(dim=1, is_inplace=True)

    def test_diffusion_2D_inplace(self):
        self._test(dim=2, is_inplace=True)

    def test_diffusion_3D_inplace(self):
        self._test(dim=3, is_inplace=True)

    def perform_tests(self):
        self.test_diffusion_1D_inplace()
        self.test_diffusion_2D_inplace()
        self.test_diffusion_3D_inplace()

        # self.test_diffusion_1D_out_of_place()
        # self.test_diffusion_2D_out_of_place()
        # self.test_diffusion_3D_out_of_place()
        print()


if __name__ == "__main__":
    TestDirectionalDiffusionOperator.setup_class(
        enable_extra_tests=False, enable_debug_mode=False
    )

    test = TestDirectionalDiffusionOperator()

    enable_pretty_printing()

    with printoptions(
        threshold=10000,
        linewidth=240,
        nanstr="nan",
        infstr="inf",
        formatter={"float": lambda x: f"{x:>6.2f}"},
    ):
        test.perform_tests()

    TestDirectionalDiffusionOperator.teardown_class()
