# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import itertools as it
import random

import primefac
import sympy as sm
from hysop import Box, Field
from hysop.constants import HYSOP_REAL, BoundaryCondition
from hysop.operator.poisson import Implementation, Poisson
from hysop.testsenv import (
    __ENABLE_LONG_TESTS__,
    __HAS_OPENCL_BACKEND__,
    domain_boundary_iterator,
    iter_clenv,
    opencl_failed,
    test_context,
)
from hysop.tools.contexts import printoptions
from hysop.tools.io_utils import IO
from hysop.tools.numerics import is_fp, is_integer
from hysop.tools.numpywrappers import npw
from hysop.tools.spectral_utils import (
    make_multivariate_polynomial,
    make_multivariate_trigonometric_polynomial,
)
from hysop.tools.sympy_utils import round_expr, truncate_expr
from hysop.tools.htypes import check_instance, first_not_None


class TestPoissonOperator:

    @classmethod
    def setup_class(
        cls, enable_extra_tests=__ENABLE_LONG_TESTS__, enable_debug_mode=False
    ):

        IO.set_default_path("/tmp/hysop_tests/test_poisson")

        cls.size_min = 8
        cls.size_max = 16

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_debug_mode = enable_debug_mode

        from hysop.tools.sympy_utils import enable_pretty_printing

        enable_pretty_printing()

    @classmethod
    def teardown_class(cls):
        pass

    @classmethod
    def build_analytic_solutions(
        cls, polynomial, dim, nb_components, lboundaries, rboundaries, origin, end
    ):
        from hysop.symbolic.base import TensorBase
        from hysop.symbolic.field import laplacian
        from hysop.symbolic.frame import SymbolicFrame

        frame = SymbolicFrame(dim=dim)
        coords = frame.coords

        def gen_psi():
            psis = ()
            for i in range(nb_components):
                if polynomial:
                    psi, y = make_multivariate_polynomial(
                        origin, end, lboundaries, rboundaries, 10, 4
                    )
                else:
                    psi, y = make_multivariate_trigonometric_polynomial(
                        origin, end, lboundaries, rboundaries, 2
                    )
                psi = psi.xreplace({yi: xi for (yi, xi) in zip(y, frame.coords)})
                psis += (psi,)
            return npw.asarray(psis).view(TensorBase)

        Psis = gen_psi()
        Ws = npw.atleast_1d(laplacian(Psis, frame))

        fWs = tuple(sm.lambdify(coords, W) for W in Ws)
        fPsis = tuple(sm.lambdify(coords, Psi) for Psi in Psis)

        analytic_expressions = {"Psi": Psis, "W": Ws}
        analytic_functions = {"Psi": fPsis, "W": fWs}
        return (analytic_expressions, analytic_functions)

    @staticmethod
    def __random_init(data, coords, component, dtype):
        if is_fp(dtype):
            data[...] = npw.random.random(size=data.shape).astype(dtype=dtype)
        else:
            msg = f"Unknown dtype {dtype}."
            raise NotImplementedError(msg)

    @staticmethod
    def __analytic_init(data, coords, fns, component, dtype):
        fn = fns[component]
        data[...] = fn(*coords).astype(data.dtype)

    def _test(
        self, dim, dtype, max_runs=5, polynomial=False, size_min=None, size_max=None
    ):

        if dtype == HYSOP_REAL:
            nb_components = 1  # enable fortran poisson test
        else:
            nb_components = 2

        size_min = first_not_None(size_min, self.size_min)
        size_max = first_not_None(size_max, self.size_max)

        valid_factors = {2, 3, 5, 7, 11, 13}
        factors = {1}
        while factors - valid_factors:
            factors.clear()
            shape = tuple(
                npw.random.randint(low=size_min, high=size_max + 1, size=dim).tolist()
            )
            for Si in shape:
                factors.update(set(primefac.primefac(int(Si))))

        domain_boundaries = list(domain_boundary_iterator(dim=dim))
        periodic = domain_boundaries[0]
        domain_boundaries = domain_boundaries[1:]
        random.shuffle(domain_boundaries)
        domain_boundaries.insert(0, periodic)

        for i, (lboundaries, rboundaries) in enumerate(domain_boundaries, 1):
            domain = Box(
                origin=(npw.random.rand(dim) - 0.5),
                length=(npw.random.rand(dim) + 0.5) * 2 * npw.pi,
                lboundaries=lboundaries,
                rboundaries=rboundaries,
            )

            Psi = Field(
                domain=domain, name="Psi", dtype=dtype, nb_components=nb_components
            )
            W = Field(domain=domain, name="W", dtype=dtype, nb_components=nb_components)

            self._test_one(
                shape=shape,
                dim=dim,
                dtype=dtype,
                domain=domain,
                Psi=Psi,
                W=W,
                polynomial=polynomial,
                nb_components=nb_components,
            )
            if (max_runs is not None) and (i == max_runs):
                missing = ((4 ** (dim + 1) - 1) // 3) - i
                print()
                print(
                    ">> MAX RUNS ACHIEVED FOR {}D DOMAINS -- SKIPING {} OTHER BOUNDARY CONDITIONS <<".format(
                        dim, missing
                    )
                )
                print()
                print()
                break
        else:
            assert i == (4 ** (dim + 1) - 1) // 3, (i + 1, (4 ** (dim + 1) - 1) // 3)
            print()
            print(f">> TESTED ALL {dim}D BOUNDARY CONDITIONS <<")
            print()
            print()

    def _test_one(self, shape, dim, dtype, domain, Psi, W, polynomial, nb_components):

        (analytic_expressions, analytic_functions) = self.build_analytic_solutions(
            dim=dim,
            nb_components=nb_components,
            polynomial=polynomial,
            lboundaries=W.lboundaries[
                ::-1
            ],  # => boundaries in variable order x0,...,xn
            rboundaries=W.rboundaries[::-1],
            origin=domain.origin[::-1],
            end=domain.end[::-1],
        )

        def format_expr(e):
            return truncate_expr(round_expr(e, 3), 80)

        msg = "\nTesting {}D Poisson: dtype={} nb_components={} shape={} polynomial={}, bc=[{}]".format(
            dim,
            dtype.__name__,
            nb_components,
            shape,
            polynomial,
            W.domain.format_boundaries(),
        )
        msg += "\n >Corresponding field boundary conditions are [{}].".format(
            W.fields[0].format_boundaries()
        )
        msg += "\n >Input analytic functions are (truncated):"
        for Wi, Wis in zip(W.fields, analytic_expressions["W"]):
            msg += f"\n   *{Wi.pretty_name}(x,t) = {format_expr(Wis)}"
        msg += "\n >Expected output solutions:"
        for Psi_i, Psis_i in zip(Psi.fields, analytic_expressions["Psi"]):
            msg += f"\n   *{Psi_i.pretty_name}(x,t) = {format_expr(Psis_i)}"
        msg += "\n >Testing all implementations:"
        print(msg)

        implementations = Poisson.implementations()
        variables = {Psi: shape, W: shape}

        def iter_impl(impl):
            base_kwds = dict(
                Fout=Psi,
                Fin=W,
                variables=variables,
                implementation=impl,
                name=f"poisson_{str(impl).lower()}",
            )
            if impl is Implementation.FORTRAN:
                msg = "   *Fortran FFTW: "
                print(msg, end=" ")
                yield Poisson(**base_kwds)
            elif impl is Implementation.PYTHON:
                msg = "   *Python FFTW: "
                print(msg, end=" ")
                yield Poisson(**base_kwds)
            elif impl is Implementation.OPENCL:
                from hysop.backend.device.opencl import cl

                msg = "   *OpenCl CLFFT: "
                print(msg)
                for cl_env in iter_clenv():
                    msg = "     |platform {}, device {}".format(
                        cl_env.platform.name.strip(), cl_env.device.name.strip()
                    )
                    print(msg, end=" ")
                    yield Poisson(cl_env=cl_env, **base_kwds)
                msg = "   *OpenCl FFTW: "
                print(msg)
                cpu_envs = tuple(iter_clenv(device_type="cpu"))
                if cpu_envs:
                    for cl_env in cpu_envs:
                        msg = "     |platform {}, device {}".format(
                            cl_env.platform.name.strip(), cl_env.device.name.strip()
                        )
                        print(msg, end=" ")
                        yield Poisson(
                            cl_env=cl_env, enforce_implementation=False, **base_kwds
                        )
            else:
                msg = f"Unknown implementation to test {impl}."
                raise NotImplementedError(msg)

        # Compare to analytic solution
        Psiref = None
        Wref = None
        for impl in implementations:
            if impl is Implementation.FORTRAN:
                if (
                    (nb_components > 1)
                    or (dim != 3)
                    or (not dtype is HYSOP_REAL)
                    or any((bd != BoundaryCondition.PERIODIC) for bd in W.lboundaries)
                    or any((bd != BoundaryCondition.PERIODIC) for bd in W.rboundaries)
                ):
                    print("   *Fortran FFTW: NO SUPPORT")
                    continue
            for op in iter_impl(impl):
                op = op.build()
                dw = op.get_input_discrete_field(W).as_contiguous_dfield()
                dpsi = op.get_output_discrete_field(Psi).as_contiguous_dfield()

                dw.initialize(
                    self.__analytic_init, dtype=dtype, fns=analytic_functions["W"]
                )
                if Psiref is None:
                    dpsi.initialize(
                        self.__analytic_init, dtype=dtype, fns=analytic_functions["Psi"]
                    )
                    Wref = tuple(data.get().handle.copy() for data in dw.data)
                    Psiref = tuple(data.get().handle.copy() for data in dpsi.data)
                dpsi.initialize(self.__random_init, dtype=dtype)

                op.apply(simulation=None)

                Wout = tuple(data.get().handle.copy() for data in dw.data)
                Psiout = tuple(data.get().handle.copy() for data in dpsi.data)
                self._check_output(impl, op, Wref, Psiref, Wout, Psiout)
                if impl is Implementation.FORTRAN:
                    op.finalize(clean_fftw_solver=True)
                print()

    @classmethod
    def _check_output(cls, impl, op, Wref, Psiref, Wout, Psiout):
        check_instance(Wref, tuple, values=npw.ndarray)
        check_instance(Psiref, tuple, values=npw.ndarray, size=len(Wref))
        check_instance(Wout, tuple, values=npw.ndarray, size=len(Wref))
        check_instance(Psiout, tuple, values=npw.ndarray, size=len(Wref))

        msg0 = "Reference field {} is not finite."
        for fields, name in zip((Wref, Psiref), ("Wref", "Psiref")):
            for i, field in enumerate(fields):
                iname = f"{name}{i}"
                mask = npw.isfinite(field)
                if not mask.all():
                    print()
                    print(field)
                    print()
                    print(field[~mask])
                    print()
                    msg = msg0.format(iname)
                    raise ValueError(msg)

        for out_buffers, ref_buffers, name in zip(
            (Wout, Psiout), (Wref, Psiref), ("W", "Psi")
        ):
            for i, (fout, fref) in enumerate(zip(out_buffers, ref_buffers)):
                iname = f"{name}{i}"
                assert fout.dtype == fref.dtype, iname
                assert fout.shape == fref.shape, iname
                assert fout.flags.c_contiguous
                assert fref.flags.c_contiguous

                has_nan = npw.any(npw.isnan(fout))
                has_inf = npw.any(npw.isinf(fout))
                if has_nan:
                    deps = "nan"
                elif has_inf:
                    deps = "inf"
                else:
                    eps = npw.finfo(fout.dtype).eps
                    dist = npw.abs(fout - fref)
                    dinf = npw.max(dist)
                    deps = int(npw.ceil(dinf / eps))
                    if deps < 10000:
                        print(f"{deps}eps, ", end=" ")
                        continue

                print()
                print()
                print(f"Test output comparisson for {name} failed for component {i}:")
                print(f" *has_nan: {has_nan}")
                print(f" *has_inf: {has_inf}")
                print(f" *dinf={dinf} ({deps} eps)")
                print()
                if cls.enable_debug_mode:
                    print("REFERENCE INPUTS:")
                    for i, w in enumerate(Wref):
                        print(f"W{i}")
                        print(w)
                        print()
                    if name == "Psi":
                        print("REFERENCE OUTPUT:")
                        for i, u in enumerate(Psiref):
                            print(f"Psi{i}")
                            print(u)
                            print()
                        print()
                        print(f"OPERATOR {op.name.upper()} OUTPUT:")
                        print()
                        for i, u in enumerate(Psiout):
                            print(f"Psi{i}")
                            print(u)
                            print()
                    else:
                        print("MODIFIED INPUTS:")
                        for i, w in enumerate(Wout):
                            print(f"W{i}")
                            print(w)
                            print()
                    print()

                msg = "Test failed for {} on component {} for implementation {}."
                msg = msg.format(name, i, impl)
                raise RuntimeError(msg)

    # NG 15 may 2024: remove test 1D because bug in [SYM/SYM] Psi case.
    #    def test_1d_float32(self, **kwds):
    #        if HYSOP_REAL == npw.float32:
    #            self._test(dim=1, dtype=npw.float32, **kwds)

    def test_2d_float32(self, **kwds):
        if HYSOP_REAL == npw.float32:
            self._test(dim=2, dtype=npw.float32, **kwds)

    def test_3d_float32(self, **kwds):
        if HYSOP_REAL == npw.float32:
            self._test(dim=3, dtype=npw.float32, **kwds)

    def test_long_4d_float32(self, **kwds):
        if HYSOP_REAL == npw.float32:
            self._test(dim=4, dtype=npw.float32, **kwds)

    # NG 15 may 2024: remove test 1D because bug in [SYM/SYM] Psi case.
    #    def test_1d_float64(self, **kwds):
    #        if HYSOP_REAL == npw.float64:
    #            self._test(dim=1, dtype=npw.float64, **kwds)

    def test_2d_float64(self, **kwds):
        if HYSOP_REAL == npw.float64:
            self._test(dim=2, dtype=npw.float64, **kwds)

    def test_3d_float64(self, **kwds):
        if HYSOP_REAL == npw.float64:
            self._test(dim=3, dtype=npw.float64, **kwds)

    def test_long_4d_float64(self, **kwds):
        if HYSOP_REAL == npw.float64:
            self._test(dim=4, dtype=npw.float64, **kwds)

    # def test_polynomial_1d_float32(self, **kwds):
    #     self._test(dim=1, dtype=npw.float32, polynomial=True, **kwds)
    # def test_polynomial_2d_float32(self, **kwds):
    #     self._test(dim=2, dtype=npw.float32, polynomial=True, **kwds)
    # def test_polynomial_3d_float32(self, **kwds):
    #     self._test(dim=3, dtype=npw.float32, polynomial=True, **kwds)

    def perform_tests(self):
        max_1d_runs = None
        max_2d_runs = 2
        max_3d_runs = 2
        max_4d_runs = 2

        # NG 15 may 2024: remove test 1D because bug in [SYM/SYM] Psi case.
        #        self.test_1d_float32(max_runs=max_1d_runs)
        self.test_2d_float32(max_runs=max_2d_runs)
        self.test_3d_float32(max_runs=max_3d_runs)
        if __ENABLE_LONG_TESTS__:
            self.test_long_4d_float32(max_runs=max_4d_runs)
        # NG 15 may 2024: remove test 1D because bug in [SYM/SYM] Psi case.
        #        self.test_1d_float64(max_runs=max_1d_runs)
        self.test_2d_float64(max_runs=max_2d_runs)
        self.test_3d_float64(max_runs=max_3d_runs)
        if __ENABLE_LONG_TESTS__:
            self.test_long_4d_float64(max_runs=max_4d_runs)


if __name__ == "__main__":
    TestPoissonOperator.setup_class(enable_extra_tests=False, enable_debug_mode=False)

    test = TestPoissonOperator()

    with test_context():
        test.perform_tests()

    TestPoissonOperator.teardown_class()
