# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import itertools as it
import random

import primefac
import sympy as sm
from hysop import Box, Field
from hysop.constants import HYSOP_REAL, BoxBoundaryCondition
from hysop.defaults import VelocityField, VorticityField
from hysop.operator.curl import Implementation, SpectralCurl
from hysop.testsenv import (
    __ENABLE_LONG_TESTS__,
    __HAS_OPENCL_BACKEND__,
    domain_boundary_iterator,
    iter_clenv,
    opencl_failed,
    test_context,
)
from hysop.tools.contexts import printoptions
from hysop.tools.io_utils import IO
from hysop.tools.numerics import is_fp, is_integer
from hysop.tools.numpywrappers import npw
from hysop.tools.spectral_utils import (
    make_multivariate_polynomial,
    make_multivariate_trigonometric_polynomial,
)
from hysop.tools.sympy_utils import round_expr, truncate_expr
from hysop.tools.htypes import check_instance, first_not_None


class TestSpectralCurl:

    @classmethod
    def setup_class(
        cls, enable_extra_tests=__ENABLE_LONG_TESTS__, enable_debug_mode=False
    ):

        IO.set_default_path("/tmp/hysop_tests/test_spectral_curl")

        cls.size_min = 8
        cls.size_max = 16

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_debug_mode = enable_debug_mode

        from hysop.tools.sympy_utils import enable_pretty_printing

        enable_pretty_printing()

    @classmethod
    def teardown_class(cls):
        pass

    @classmethod
    def build_analytic_solutions(
        cls, polynomial, dim, nb_components, lboundaries, rboundaries, origin, end
    ):
        from hysop.symbolic.base import TensorBase
        from hysop.symbolic.field import curl, laplacian
        from hysop.symbolic.frame import SymbolicFrame

        assert len(lboundaries) == nb_components
        assert len(rboundaries) == nb_components

        frame = SymbolicFrame(dim=dim)
        coords = frame.coords

        def gen_Fin():
            Fins = ()
            for i in range(nb_components):
                if polynomial:
                    fin, y = make_multivariate_polynomial(
                        origin, end, lboundaries[i], rboundaries[i], 10, 4
                    )
                else:
                    fin, y = make_multivariate_trigonometric_polynomial(
                        origin, end, lboundaries[i], rboundaries[i], 2
                    )
                fin = fin.xreplace({yi: xi for (yi, xi) in zip(y, coords)})
                Fins += (fin,)
            return npw.asarray(Fins).view(TensorBase)

        Fins = gen_Fin()
        Fouts = npw.atleast_1d(curl(Fins, frame))

        fFins = tuple(sm.lambdify(coords, Fin) for Fin in Fins)
        fFouts = tuple(sm.lambdify(coords, Fout) for Fout in Fouts)

        analytic_expressions = {"Fin": Fins, "Fout": Fouts}
        analytic_functions = {"Fin": fFins, "Fout": fFouts}
        return (analytic_expressions, analytic_functions)

    @staticmethod
    def __random_init(data, coords, component, dtype):
        if is_fp(dtype):
            data[...] = npw.random.random(size=data.shape).astype(dtype=dtype)
        else:
            msg = f"Unknown dtype {dtype}."
            raise NotImplementedError(msg)

    @staticmethod
    def __analytic_init(data, coords, fns, component, dtype):
        fn = fns[component]
        data[...] = npw.asarray(fn(*coords)).astype(dtype)

    def _test(
        self,
        dim,
        dtype,
        nb_components,
        max_runs=5,
        polynomial=False,
        size_min=None,
        size_max=None,
    ):
        enable_extra_tests = self.enable_extra_tests

        size_min = first_not_None(size_min, self.size_min)
        size_max = first_not_None(size_max, self.size_max)

        valid_factors = {2, 3, 5, 7, 11, 13}
        factors = {1}
        while factors - valid_factors:
            factors.clear()
            shape = tuple(
                npw.random.randint(low=size_min, high=size_max + 1, size=dim).tolist()
            )
            for Si in shape:
                factors.update(set(primefac.primefac(int(Si))))

        domain_boundaries = list(domain_boundary_iterator(dim=dim))
        periodic = domain_boundaries[0]
        domain_boundaries = domain_boundaries[1:]
        random.shuffle(domain_boundaries)
        domain_boundaries.insert(0, periodic)

        for i, (lboundaries, rboundaries) in enumerate(domain_boundaries, 1):

            domain = Box(
                origin=(npw.random.rand(dim) - 0.5),
                length=(0.5 + npw.random.rand(dim) * 2 * npw.pi),
                lboundaries=lboundaries,
                rboundaries=rboundaries,
            )

            if dim == nb_components:
                Fin = VelocityField(name="Fin", domain=domain)
                Fout = VorticityField(name="Fout", velocity=Fin)
            else:
                Fin = Field(
                    name="Fin", domain=domain, dtype=dtype, nb_components=nb_components
                )
                Fout = Fin.curl(name="Fout")

            self._test_one(
                shape=shape,
                dim=dim,
                dtype=dtype,
                domain=domain,
                Fin=Fin,
                Fout=Fout,
                polynomial=polynomial,
            )
            if (max_runs is not None) and (i == max_runs):
                missing = ((4 ** (dim + 1) - 1) // 3) - i
                print()
                print(
                    ">> MAX RUNS ACHIEVED FOR {}D DOMAINS -- SKIPING {} OTHER BOUNDARY CONDITIONS <<".format(
                        dim, missing
                    )
                )
                print()
                print()
                break
        else:
            assert i == (4 ** (dim + 1) - 1) // 3, (i + 1, (4 ** (dim + 1) - 1) // 3)
            print()
            print(f">> TESTED ALL {dim}D BOUNDARY CONDITIONS <<")
            print()
            print()

    def _test_one(self, shape, dim, dtype, domain, Fout, Fin, polynomial):

        (analytic_expressions, analytic_functions) = self.build_analytic_solutions(
            dim=dim,
            nb_components=Fin.nb_components,
            polynomial=polynomial,
            lboundaries=[
                fin.lboundaries[::-1] for fin in Fin.fields
            ],  # => boundaries in variable order x0,...,xn
            rboundaries=[fin.rboundaries[::-1] for fin in Fin.fields],
            origin=domain.origin[::-1],
            end=domain.end[::-1],
        )

        def format_expr(e):
            return truncate_expr(round_expr(e, 3), 80)

        msg = "\nTesting {}D Curl: dtype={} shape={} polynomial={}, bc=[{}]".format(
            dim, dtype.__name__, shape, polynomial, domain.format_boundaries()
        )
        print(msg)
        print(" >Input analytic field is (truncated):")
        for fin, fins in zip(Fin.fields, analytic_expressions["Fin"]):
            print(f"   *{fin.pretty_name}(x) = {format_expr(fins)}")
        print(" >Expected output analytic field is:")
        for fout, fouts in zip(Fout.fields, analytic_expressions["Fout"]):
            print(f"   *{fout.pretty_name}(x) = {format_expr(fouts)}")
        print(" >Testing all implementations:")

        implementations = SpectralCurl.implementations().keys()
        variables = {Fout: shape, Fin: shape}

        def iter_impl(impl):
            base_kwds = dict(
                Fin=Fin,
                Fout=Fout,
                variables=variables,
                implementation=impl,
                name=f"curl_{str(impl).lower()}",
            )
            if impl is Implementation.PYTHON:
                msg = "   *Python FFTW: "
                print(msg, end=" ")
                yield SpectralCurl(**base_kwds)
            elif impl is Implementation.OPENCL:
                msg = "   *OpenCl CLFFT: "
                print(msg)
                for cl_env in iter_clenv():
                    msg = "     |platform {}, device {}:".format(
                        cl_env.platform.name.strip(), cl_env.device.name.strip()
                    )
                    print(msg, end=" ")
                    yield SpectralCurl(cl_env=cl_env, **base_kwds)
            else:
                msg = f"Unknown implementation to test {impl}."
                raise NotImplementedError(msg)

        # Compare to analytic solution
        Fout_ref = None
        Fin_ref = None
        for impl in implementations:
            for i, op in enumerate(iter_impl(impl)):
                from hysop.tools.debug_dumper import DebugDumper

                name = f"{impl}_{i}"

                op = op.build()

                dFin = op.get_input_discrete_field(Fin).as_contiguous_dfield()
                dFout = op.get_output_discrete_field(Fout).as_contiguous_dfield()

                dFin.initialize(
                    self.__analytic_init, dtype=dtype, fns=analytic_functions["Fin"]
                )

                if Fout_ref is None:
                    dFout.initialize(
                        self.__analytic_init,
                        dtype=dtype,
                        fns=analytic_functions["Fout"],
                    )
                    Fin_ref = tuple(data.get().handle.copy() for data in dFin.data)
                    Fout_ref = tuple(data.get().handle.copy() for data in dFout.data)
                dFout.initialize(self.__random_init, dtype=dtype)

                op.apply(simulation=None)

                Wout = tuple(data.get().handle.copy() for data in dFin.data)
                Uout = tuple(data.get().handle.copy() for data in dFout.data)
                self._check_output(impl, op, Fin_ref, Fout_ref, Wout, Uout)
                print()

    @classmethod
    def _check_output(cls, impl, op, Fin_ref, Fout_ref, Wout, Uout):
        check_instance(Fin_ref, tuple, values=npw.ndarray)
        check_instance(Fout_ref, tuple, values=npw.ndarray)
        check_instance(Wout, tuple, values=npw.ndarray, size=len(Fin_ref))
        check_instance(Uout, tuple, values=npw.ndarray, size=len(Fout_ref))

        msg0 = "Reference field {} is not finite."
        for fields, name in zip((Fin_ref, Fout_ref), ("Fin_ref", "Fout_ref")):
            for i, field in enumerate(fields):
                iname = f"{name}{i}"
                mask = npw.isfinite(field)
                if not mask.all():
                    print()
                    print(field)
                    print()
                    print(field[~mask])
                    print()
                    msg = msg0.format(iname)
                    raise ValueError(msg)

        for out_buffers, ref_buffers, name in zip(
            (Wout, Uout), (Fin_ref, Fout_ref), ("Fin", "Fout")
        ):
            for i, (fout, fref) in enumerate(zip(out_buffers, ref_buffers)):
                iname = f"{name}{i}"
                assert fout.dtype == fref.dtype, iname
                assert fout.shape == fref.shape, iname
                assert not npw.any(npw.isnan(fref))
                assert not npw.any(npw.isinf(fref))

                has_nan = npw.any(npw.isnan(fout))
                has_inf = npw.any(npw.isinf(fout))
                if has_nan:
                    deps = "nan"
                elif has_inf:
                    deps = "inf"
                else:
                    eps = npw.finfo(fout.dtype).eps
                    dist = npw.abs(fout - fref)
                    dinf = npw.max(dist)
                    try:
                        deps = int(npw.ceil(dinf / eps))
                    except:
                        deps = "inf"
                if deps < 10000:
                    print(f"{deps}eps, ", end=" ")
                    continue

                print()
                print()
                print(f"Test output comparisson for {name} failed for component {i}:")
                print(f" *has_nan: {has_nan}")
                print(f" *has_inf: {has_inf}")
                print(f" *dinf={dinf} ({deps} eps)")
                print()
                if cls.enable_debug_mode:
                    print("REFERENCE INPUTS:")
                    for i, w in enumerate(Fin_ref):
                        print(f"Fin{i}")
                        print(w)
                        print()
                    if name == "Fout":
                        print("REFERENCE OUTPUT:")
                        for i, u in enumerate(Fout_ref):
                            print(f"Fout{i}")
                            print(u)
                            print()
                        print()
                        print(f"OPERATOR {op.name.upper()} OUTPUT:")
                        print()
                        for i, u in enumerate(Uout):
                            print(f"Fout{i}")
                            print(u)
                            print()
                    else:
                        print("MODIFIED INPUTS:")
                        for i, w in enumerate(Wout):
                            print(f"Fin{i}")
                            print(w)
                            print()
                    print()

                msg = "Test failed for {} on component {} for implementation {}."
                msg = msg.format(name, i, impl)
                raise RuntimeError(msg)

    def test_2d_float32__1(self, **kwds):
        if HYSOP_REAL == npw.float32:
            self._test(dim=2, dtype=npw.float32, nb_components=1, **kwds)

    def test_2d_float32__2(self, **kwds):
        if HYSOP_REAL == npw.float32:
            self._test(dim=2, dtype=npw.float32, nb_components=2, **kwds)

    def test_3d_float32(self, **kwds):
        if HYSOP_REAL == npw.float32:
            self._test(dim=3, dtype=npw.float32, nb_components=3, **kwds)

    def test_2d_float64__1(self, **kwds):
        if HYSOP_REAL == npw.float64:
            self._test(dim=2, dtype=npw.float64, nb_components=1, **kwds)

    def test_2d_float64__2(self, **kwds):
        if HYSOP_REAL == npw.float64:
            self._test(dim=2, dtype=npw.float64, nb_components=2, **kwds)

    def test_3d_float64(self, **kwds):
        if HYSOP_REAL == npw.float64:
            self._test(dim=3, dtype=npw.float64, nb_components=3, **kwds)

    def perform_tests(self):
        max_2d_runs = None if __ENABLE_LONG_TESTS__ else 2
        max_3d_runs = None if __ENABLE_LONG_TESTS__ else 2

        self.test_2d_float32__1(max_runs=max_2d_runs)
        self.test_2d_float32__2(max_runs=max_2d_runs)
        self.test_3d_float32(max_runs=max_3d_runs)
        self.test_2d_float64__1(max_runs=max_2d_runs)
        self.test_2d_float64__2(max_runs=max_2d_runs)
        self.test_3d_float64(max_runs=max_3d_runs)


if __name__ == "__main__":
    TestSpectralCurl.setup_class(enable_extra_tests=False, enable_debug_mode=False)

    test = TestSpectralCurl()

    with printoptions(
        threshold=10000,
        linewidth=240,
        nanstr="nan",
        infstr="inf",
        formatter={"float": lambda x: f"{x:>6.2f}"},
    ):
        test.perform_tests()

    TestSpectralCurl.teardown_class()
