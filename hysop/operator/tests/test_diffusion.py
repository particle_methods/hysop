# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
import sys, random
from hysop.constants import HYSOP_REAL, BoundaryCondition
from hysop.tools.numpywrappers import npw
from hysop.tools.contexts import printoptions
from hysop.tools.htypes import first_not_None, to_tuple
from hysop.tools.numerics import is_integer, is_fp
from hysop.tools.io_utils import IO
from hysop.testsenv import __ENABLE_LONG_TESTS__, iter_clenv
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.operator.diffusion import Diffusion

from hysop import Field, Box, Simulation
from hysop.constants import (
    Implementation,
    DirectionLabels,
    ComputeGranularity,
    SpaceDiscretization,
)


class TestDiffusionOperator:

    @classmethod
    def setup_class(
        cls, enable_extra_tests=__ENABLE_LONG_TESTS__, enable_debug_mode=False
    ):

        IO.set_default_path("/tmp/hysop_tests/test_diffusion")

        cls.size_min = 8
        cls.size_max = 16

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_debug_mode = enable_debug_mode

    @classmethod
    def teardown_class(cls):
        pass

    def _test(self, dim, is_inplace, size_min=None, size_max=None):
        assert dim > 0

        size_min = first_not_None(size_min, self.size_min)
        size_max = first_not_None(size_max, self.size_max)

        shape = tuple(
            npw.random.randint(low=size_min, high=size_max, size=dim).tolist()
        )

        if __ENABLE_LONG_TESTS__:
            flt_types = (npw.float32, npw.float64)
        else:
            flt_types = (HYSOP_REAL,)

        domain = Box(length=(2 * npw.pi,) * dim)
        for dtype in flt_types:
            nu = ScalarParameter(
                "nu", dtype=dtype, initial_value=random.random(), const=True
            )
            nb_components = 5 if (dim == 2) else 6
            Fin = Field(
                domain=domain, name="Fin", dtype=dtype, nb_components=nb_components
            )
            Fout = Field(
                domain=domain, name="Fout", dtype=dtype, nb_components=nb_components
            )
            self._test_one(
                shape=shape,
                dim=dim,
                dtype=dtype,
                is_inplace=is_inplace,
                domain=domain,
                Fin=Fin,
                Fout=Fout,
                nu=nu,
            )

    @staticmethod
    def __random_init(data, coords, dtype, component):
        shape = data.shape
        if is_integer(dtype):
            data[...] = npw.random.random_integers(low=0, high=255, size=shape)
        elif is_fp(dtype):
            data[...] = npw.random.random(size=data.shape)
        else:
            msg = f"Unknown dtype {dtype}."
            raise NotImplementedError(msg)

    @staticmethod
    def __scalar_init(data, coords, dtype, component):
        if is_fp(dtype):
            data[...] = 1
            for xi in coords:
                data[...] *= npw.cos(xi * (component + 1))
        else:
            msg = f"Unknown dtype {dtype}."
            raise NotImplementedError(msg)

    def _test_one(self, shape, dim, dtype, is_inplace, domain, Fin, Fout, nu):
        print()
        print(
            "\nTesting {}D Diffusion: inplace={} dtype={} shape={}".format(
                dim, is_inplace, dtype.__name__, shape
            ),
            end=" ",
        )

        dt = ScalarParameter("dt0", initial_value=0.5, const=True, dtype=dtype)

        fin = Fin
        if is_inplace:
            fout = Fin
            variables = {Fin: shape}
        else:
            fout = Fout
            variables = {Fin: shape, Fout: shape}
            msg = "Out of place diffusion has not been implemented yet."
            raise NotImplementedError(msg)

        implementations = Diffusion.implementations()
        ref_impl = Implementation.PYTHON  # ref impl is always the first

        def iter_impl(impl):
            base_kwds = dict(
                Fin=fin,
                nu=nu,
                dt=dt,
                variables=variables,
                implementation=impl,
                name=f"test_diffusion_{str(impl).lower()}",
            )
            if not is_inplace:
                base_kwds["Fout"] = fout
            if impl is Implementation.PYTHON:
                msg = "   *Python FFTW: "
                print(msg, end=" ")
                diff = Diffusion(**base_kwds)
                yield diff.to_graph()
            elif impl is Implementation.FORTRAN:
                msg = "   *Fortran FFTW: "
                print(msg, end=" ")
                diff = Diffusion(**base_kwds)
                yield diff.to_graph()
            elif impl is Implementation.OPENCL:
                msg = "   *OpenCl CLFFT: "
                print(msg)
                for cl_env in iter_clenv():
                    msg = "     |platform {}, device {}: ".format(
                        cl_env.platform.name.strip(), cl_env.device.name.strip()
                    )
                    print(msg, end=" ")
                    sys.stdout.flush()
                    diff = Diffusion(cl_env=cl_env, **base_kwds)
                    yield diff.to_graph()
                msg = "   *OpenCl FFTW: "
                print(msg)
                cpu_envs = tuple(iter_clenv(device_type="cpu"))
                if cpu_envs:
                    for cl_env in cpu_envs:
                        msg = "     |platform {}, device {}".format(
                            cl_env.platform.name.strip(), cl_env.device.name.strip()
                        )
                        print(msg, end=" ")
                        diff = Diffusion(
                            cl_env=cl_env, enforce_implementation=False, **base_kwds
                        )
                        yield diff.to_graph()
            else:
                msg = f"Unknown implementation to test {impl}."
                raise NotImplementedError(msg)

        # Compare to other implementations
        reference_fields = {}
        outputs = {}

        print("\n >Testing all Implementations:")
        for impl in implementations:
            if impl is Implementation.FORTRAN:
                if (
                    (dim not in (2, 3))
                    or ((dim == 3) and (Fin.nb_components % 3 != 0))
                    or (dtype != HYSOP_REAL)
                    or any((bd != BoundaryCondition.PERIODIC) for bd in Fin.lboundaries)
                    or any((bd != BoundaryCondition.PERIODIC) for bd in Fin.rboundaries)
                ):
                    print("   *Fortran FFTW: NO SUPPORT")
                    continue
            elif impl is Implementation.OPENCL:
                if dim > 3:
                    print("   *OpenCl: NO SUPPORT")
                    continue
            for op in iter_impl(impl):
                if impl is ref_impl:
                    print("REF IMPL", end=" ")
                    sys.stdout.flush()
                if not is_inplace:
                    op.push_copy(dst=fin, src=fout)
                op.build()

                dfin = op.get_input_discrete_field(fin)
                dfout = op.get_output_discrete_field(fout)

                dfin_c = dfin.as_contiguous_dfield()
                dfout_c = dfout.as_contiguous_dfield()

                input_tstate = dfin.topology_state.copy()
                input_tstate.memory_order = "c"
                dfin_c = dfin.view(input_tstate, dfin.name + "_C")

                output_tstate = dfout.topology_state.copy()
                output_tstate.memory_order = "c"
                dfout_c = dfout.view(output_tstate, dfout.name + "_C")

                view = dfin_c.compute_slices

                try:
                    dfout_c.initialize(self.__random_init, dtype=dfout.dtype)
                    dfin_c.initialize(self.__scalar_init, dtype=dfin.dtype)
                    S0 = dfin.integrate()

                    if not npw.all(npw.isfinite(S0)):
                        msg = f"Integral is not finite. Got {S0}."
                        raise RuntimeError(msg)

                    _input = tuple(d.get().handle.copy() for d in dfin.data)

                    op.apply(simulation=None)

                    outputs[impl] = tuple(
                        d.get().handle.copy()[view] for d in dfout_c.data
                    )

                    if ref_impl in outputs.keys() and not impl is ref_impl:
                        reference = outputs[ref_impl]
                        output = outputs[impl]
                        for i in range(dfout.nb_components):
                            if npw.may_share_memory(reference[i], output[i]):
                                msg = "Output and reference arrays may share the same memory."
                                raise RuntimeError(msg)
                            if reference[i].dtype != output[i].dtype:
                                msg = (
                                    "Output dtype {} differs from the reference one {}."
                                )
                                msg = msg.format(output[i].dtype, reference[i].dtype)
                                raise RuntimeError(msg)
                            if reference[i].shape != output[i].shape:
                                msg = (
                                    "Output shape {} differs from the reference one {}."
                                )
                                msg = msg.format(output[i].shape, reference[i].shape)
                                raise RuntimeError(msg)
                            mask = npw.isfinite(output[i])
                            if not mask.all():
                                msg = f"\nFATAL ERROR: Output is not finite on axis {i}.\n"
                                print(msg)
                                npw.fancy_print(
                                    output[i],
                                    replace_values={(lambda a: npw.isfinite(a)): "."},
                                )
                                raise RuntimeError(msg)
                            di = npw.abs(reference[i] - output[i])
                            max_di = npw.max(di)
                            neps = 100
                            max_tol = neps * npw.finfo(dfout.dtype).eps
                            if max_di > max_tol:
                                print()
                                print("SCALAR INPUT")
                                print(_input[i])
                                print("SCALAR REFERENCE")
                                print(reference[i])
                                print("SCALAR OUTPUT")
                                print(output[i])
                                print("ABS(REF - OUT)")
                                npw.fancy_print(
                                    di, replace_values={(lambda a: a < max_tol): "."}
                                )
                                msg = "Output did not match reference output for component {}"
                                msg += "\n > max computed distance was {}."
                                msg += "\n > max tolerence was set to {} ({} eps)."
                                msg = msg.format(i, max_di, max_tol, neps)
                                raise RuntimeError(msg)
                            sys.stdout.write(".")
                            sys.stdout.flush()
                        S1 = dfout.integrate()
                        if not npw.all(npw.isfinite(S1)):
                            msg = f"Integral is not finite. Got {S1}."
                            raise RuntimeError(msg)
                        if (npw.abs(S0 - S1) > max_tol).any():
                            msg = "Scalar was not conserved expected {} but got {}."
                            msg = msg.format(
                                to_tuple(S0, cast=float), to_tuple(S1, cast=float)
                            )
                            raise RuntimeError(msg)
                except:
                    sys.stdout.write("\bx\n\n")
                    sys.stdout.flush()
                    raise
                print()

    def test_diffusion_1D_inplace(self):
        self._test(dim=1, is_inplace=True)

    def test_diffusion_2D_inplace(self):
        self._test(dim=2, is_inplace=True)

    def test_diffusion_3D_inplace(self):
        self._test(dim=3, is_inplace=True)

    def perform_tests(self):
        self.test_diffusion_1D_inplace()
        self.test_diffusion_2D_inplace()
        self.test_diffusion_3D_inplace()
        print()


if __name__ == "__main__":
    TestDiffusionOperator.setup_class(enable_extra_tests=False, enable_debug_mode=False)

    test = TestDiffusionOperator()

    with printoptions(
        threshold=10000,
        linewidth=240,
        nanstr="nan",
        infstr="inf",
        formatter={"float": lambda x: f"{x:>6.2f}"},
    ):
        test.perform_tests()

    TestDiffusionOperator.teardown_class()
