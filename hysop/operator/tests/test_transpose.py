# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import random
import itertools as it
import numpy as np

from hysop.testsenv import __ENABLE_LONG_TESTS__, __HAS_OPENCL_BACKEND__
from hysop.testsenv import opencl_failed, iter_clenv
from hysop.tools.contexts import printoptions
from hysop.tools.numerics import is_fp, is_complex, is_integer
from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.io_utils import IO
from hysop.operator.transpose import Transpose, Implementation

from hysop import Field, Box


class TestTransposeOperator:

    @classmethod
    def setup_class(
        cls, enable_extra_tests=__ENABLE_LONG_TESTS__, enable_debug_mode=False
    ):

        IO.set_default_path("/tmp/hysop_tests/test_transpose")

        if enable_debug_mode:
            cls.size_min = 3
            cls.size_max = 4
        else:
            cls.size_min = 4
            cls.size_max = 23

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_debug_mode = enable_debug_mode

    @classmethod
    def teardown_class(cls):
        pass

    def _test(self, dim, dtype, is_inplace, size_min=None, size_max=None, naxes=None):
        enable_extra_tests = self.enable_extra_tests
        assert dim > 1

        nshapes = 5 if enable_extra_tests else 2
        size_min = first_not_None(size_min, self.size_min)
        size_max = first_not_None(size_max, self.size_max)
        assert ((size_max - size_min + 1) ** dim) >= nshapes

        shapes = ((size_min,) * dim,)
        while len(shapes) < nshapes:
            shape = tuple(
                np.random.randint(low=size_min, high=size_max + 1, size=dim).tolist()
            )
            if (shape in shapes) or all((si == shape[0]) for si in shape):
                continue
            shapes += (shape,)

        all_axes = set(it.permutations(range(dim)))
        all_axes.remove(tuple(range(dim)))
        all_axes = list(all_axes)
        if naxes is not None:
            random.shuffle(all_axes)
            all_axes = all_axes[: min(naxes, len(all_axes))]

        if dtype is None:
            types = [np.float32, np.float64, np.complex64, np.complex128]
            dtype = types[0]

        domain = Box(length=(1.0,) * dim)
        for nb_components in (2,):
            Fin = Field(
                domain=domain, name="Fin", dtype=dtype, nb_components=nb_components
            )
            Fout = Field(
                domain=domain, name="Fout", dtype=dtype, nb_components=nb_components
            )
            for axes in all_axes:
                for shape in shapes:
                    self._test_one(
                        shape=shape,
                        axes=axes,
                        dim=dim,
                        dtype=dtype,
                        is_inplace=is_inplace,
                        domain=domain,
                        Fin=Fin,
                        Fout=Fout,
                    )

    @classmethod
    def __field_init(cls, data, coords, component, dtype):
        shape = data.shape
        if is_integer(dtype):
            data[...] = np.random.random_integers(low=0, high=255, size=shape)
        elif is_fp(dtype):
            data[...] = np.random.random(size=shape)
        elif is_complex(dtype):
            real = np.random.random(size=shape)
            imag = np.random.random(size=shape)
            data[...] = real + 1j * imag
        else:
            msg = f"Unknown dtype {dtype}."
            raise NotImplementedError(msg)

    def _test_one(self, shape, axes, dim, dtype, is_inplace, domain, Fin, Fout):

        print(
            "\nTesting {}D Transpose: inplace={} dtype={} shape={} axes={}".format(
                dim, is_inplace, dtype.__name__, shape, axes
            )
        )
        if is_inplace:
            fin, fout = Fin, Fin
            variables = {fin: shape}
        else:
            fin, fout = Fin, Fout
            variables = {fin: shape, fout: shape}

        implementations = Transpose.implementations()
        ref_impl = Implementation.PYTHON
        assert ref_impl in implementations

        # Compute reference solution
        print("  *reference PYTHON implementation.")
        transpose = Transpose(
            fields=fin,
            output_fields=fout,
            variables=variables,
            axes=axes,
            implementation=ref_impl,
        ).build()
        dfin = transpose.get_input_discrete_field(fin)
        dfout = transpose.get_output_discrete_field(fout)
        dfin.initialize(self.__field_init, dtype=dtype)

        if is_inplace:
            refin = tuple(df.copy() for df in dfin.buffers)
        else:
            refin = tuple(df for df in dfin.buffers)

        transpose.apply()

        refout = tuple(df.copy() for df in dfout.buffers)

        for i, (in_, out_) in enumerate(zip(refin, refout)):
            ref = np.transpose(in_, axes=axes)
            if (ref != out_).any():
                print()
                print(np.transpose(in_, axes=axes))
                print()
                print(out_)
                msg = f"Reference did not match numpy for component {i}."
                raise RuntimeError(msg)

        def iter_impl(impl):
            base_kwds = dict(
                fields=fin,
                output_fields=fout,
                variables=variables,
                axes=axes,
                implementation=impl,
                name=f"test_transpose_{str(impl).lower()}",
            )
            if impl is ref_impl:
                return
            elif impl is Implementation.OPENCL:
                for cl_env in iter_clenv():
                    msg = "  *platform {}, device {}".format(
                        cl_env.platform.name.strip(), cl_env.device.name.strip()
                    )
                    print(msg)
                    yield Transpose(cl_env=cl_env, **base_kwds)
            else:
                msg = f"Unknown implementation to test {impl}."
                raise NotImplementedError(msg)

        # Compare to other implementations
        for impl in implementations:
            for op in iter_impl(impl):
                op = op.build()
                dfin = op.get_input_discrete_field(fin)
                dfout = op.get_output_discrete_field(fout)
                dfin.copy(refin)
                op.apply()
                out = tuple(data.get().handle for data in dfout.data)
                self._check_output(impl, op, refin, refout, out)

    @classmethod
    def _check_output(cls, impl, op, refin_buffers, refout_buffers, out_buffers):
        check_instance(out_buffers, tuple, values=np.ndarray)
        check_instance(refout_buffers, tuple, values=np.ndarray)
        check_instance(refin_buffers, tuple, values=np.ndarray)

        for i, (out, refin, refout) in enumerate(
            zip(out_buffers, refin_buffers, refout_buffers)
        ):
            assert refout.dtype == out.dtype
            assert refout.shape == out.shape

            if np.all(out == refout):
                continue

            if cls.enable_debug_mode:
                has_nan = np.any(np.isnan(out))
                has_inf = np.any(np.isinf(out))

                print()
                print(f"Test output comparisson failed for component {i}:")
                print(f" *has_nan: {has_nan}")
                print(f" *has_inf: {has_inf}")
                print()
                print("REFERENCE INPUT:")
                print(refin)
                print()
                print("REFERENCE OUTPUT:")
                print(refout)
                print()
                print(f"OPERATOR {op.name.upper()} OUTPUT:")
                print(out)
                print()
                print()

            msg = f"Test failed on component {i} for implementation {impl}."
            raise RuntimeError(msg)

    def test_2d_out_of_place(self):
        self._test(dim=2, is_inplace=False, dtype=np.float32)

    def test_3d_out_of_place(self):
        self._test(dim=3, is_inplace=False, dtype=np.complex64)

    def test_4d_out_of_place(self):
        if __ENABLE_LONG_TESTS__:
            self._test(dim=4, is_inplace=False, dtype=np.float64)

    def test_upper_dimensions_out_of_place(self):
        if __ENABLE_LONG_TESTS__:
            for i in range(5, 9):
                self._test(
                    dim=i, dtype=None, is_inplace=False, size_min=3, size_max=4, naxes=1
                )

    def test_2d_inplace(self):
        self._test(dim=2, is_inplace=True, dtype=np.float32)

    def test_3d_inplace(self):
        self._test(dim=3, is_inplace=True, dtype=np.float32)

    def test_4d_inplace(self):
        if __ENABLE_LONG_TESTS__:
            self._test(dim=4, is_inplace=True, dtype=np.float32)

    def test_upper_dimensions_inplace(self):
        if __ENABLE_LONG_TESTS__:
            for i in range(5, 9):
                self._test(
                    dim=i, dtype=None, is_inplace=True, size_min=3, size_max=4, naxes=1
                )

    def perform_tests(self):
        self._test(dim=2, is_inplace=False, dtype=np.float32)
        self._test(dim=3, is_inplace=False, dtype=np.float64)
        self._test(dim=3, is_inplace=False, dtype=np.complex64)
        if __ENABLE_LONG_TESTS__:
            self._test(dim=4, is_inplace=False, dtype=np.float64)
            for i in range(5, 9):
                self._test(
                    dim=i, dtype=None, is_inplace=False, size_min=3, size_max=4, naxes=1
                )

        self._test(dim=2, is_inplace=True, dtype=np.float32)
        self._test(dim=3, is_inplace=True, dtype=np.float64)
        self._test(dim=3, is_inplace=True, dtype=np.complex128)
        if __ENABLE_LONG_TESTS__:
            self._test(dim=4, is_inplace=True, dtype=np.float32)
            for i in range(5, 9):
                self._test(
                    dim=i, dtype=None, is_inplace=True, size_min=3, size_max=4, naxes=1
                )


if __name__ == "__main__":
    TestTransposeOperator.setup_class(enable_extra_tests=False, enable_debug_mode=False)

    test = TestTransposeOperator()
    test.perform_tests()

    TestTransposeOperator.teardown_class()
