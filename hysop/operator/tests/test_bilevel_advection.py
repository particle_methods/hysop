# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys

from hysop import __SCALES_ENABLED__
from hysop.testsenv import __ENABLE_LONG_TESTS__
from hysop.testsenv import iter_clenv
from hysop.tools.numpywrappers import npw
from hysop.tools.contexts import printoptions
from hysop.tools.htypes import first_not_None, to_tuple
from hysop.tools.io_utils import IO
from hysop.core.graph.computational_graph import ComputationalGraph
from hysop.operator.directional.advection_dir import DirectionalAdvection
from hysop.operator.misc import ForceTopologyState
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.operator.advection import Advection

from hysop import Field, Box
from hysop.methods import Remesh, TimeIntegrator, Interpolation
from hysop.constants import (
    Implementation,
    DirectionLabels,
    Backend,
    HYSOP_REAL,
    Implementation,
)
from hysop.numerics.splitting.strang import StrangSplitting, StrangOrder
from hysop.numerics.odesolvers.runge_kutta import Euler, RK2, RK4
from hysop.numerics.remesh.remesh import RemeshKernel
from hysop.numerics.interpolation.polynomial import PolynomialInterpolation


class TestBilevelAdvectionOperator:

    @classmethod
    def setup_class(
        cls, enable_extra_tests=__ENABLE_LONG_TESTS__, enable_debug_mode=False
    ):

        IO.set_default_path("/tmp/hysop_tests/test_bilevel_advection")

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_debug_mode = enable_debug_mode

    @classmethod
    def teardown_class(cls):
        pass

    def _test(self, dim, is_inplace, size_min=None, size_max=None):
        assert dim > 0

        shape = npw.asarray((16, 32, 64))[:dim]
        npw.random.shuffle(shape)
        shape = tuple(shape.tolist())
        shape_s = tuple(2 * s for s in shape)

        if self.enable_extra_tests:
            flt_types = (npw.float32, npw.float64)
            time_integrators = (
                Euler,
                RK2,
                RK4,
            )
            remesh_kernels = (
                Remesh.L2_1,
                Remesh.L2_1s,
                Remesh.L4_2,
                Remesh.L4_2s,
                Remesh.L8_4,
                Remesh.L8_4s,
            )
            velocity_cfls = (0.62, 1.89)
        else:
            flt_types = (HYSOP_REAL,)
            time_integrators = (RK2,)
            remesh_kernels = (Remesh.L4_2,)
            velocity_cfls = (1.89,)

        domain = Box(length=(2 * npw.pi,) * dim)
        for dtype in flt_types:
            Vin = Field(domain=domain, name="Vin", dtype=dtype, nb_components=dim)
            Sin = Field(domain=domain, name="Sin", dtype=dtype, nb_components=1)
            Sout = Field(domain=domain, name="Sout", dtype=dtype, nb_components=1)
            for time_integrator in time_integrators:
                for remesh_kernel in remesh_kernels:
                    for velocity_cfl in velocity_cfls:
                        print()
                        self._test_one(
                            time_integrator=time_integrator,
                            remesh_kernel=remesh_kernel,
                            shape=shape,
                            shape_s=shape_s,
                            dim=dim,
                            dtype=dtype,
                            is_inplace=is_inplace,
                            domain=domain,
                            Vin=Vin,
                            Sin=Sin,
                            Sout=Sout,
                            velocity_cfl=velocity_cfl,
                        )

    @classmethod
    def __velocity_init(cls, data, coords, component, axes):
        if component in axes:
            data[...] = +1.0
        else:
            data[...] = 0.0

    @classmethod
    def __scalar_init(cls, data, coords, component, offsets=None):
        offsets = first_not_None(offsets, (0.0,) * len(coords))
        assert len(coords) == len(offsets)
        data[...] = 1.0 / (component + 1)
        for c, o in zip(coords, offsets):
            data[...] *= npw.cos(c + o)

    def _test_one(
        self,
        time_integrator,
        remesh_kernel,
        shape,
        shape_s,
        dim,
        dtype,
        is_inplace,
        domain,
        velocity_cfl,
        Vin,
        Sin,
        Sout,
    ):

        print(
            "\nTesting {}D BilevelAdvection_{}_{}: inplace={} dtype={} shape={}, cfl={}".format(
                dim,
                time_integrator.name(),
                remesh_kernel,
                is_inplace,
                dtype.__name__,
                shape,
                velocity_cfl,
            ),
            end=" ",
        )
        if is_inplace:
            vin = Vin
            sin, sout = Sin, Sin
            variables = {vin: shape, sin: shape_s}
        else:
            vin = Vin
            sin, sout = Sin, Sout
            variables = {vin: shape, sin: shape_s, sout: shape_s}

        # Use optimal timestep, ||Vi||_inf is 1 on a per-axis basis
        dt = ScalarParameter("dt", initial_value=npw.nan)
        dt.value = (0.99 * velocity_cfl) / (max(shape) - 1)

        ref_impl = Implementation.FORTRAN
        implementations = set(DirectionalAdvection.implementations().keys()).union(
            Advection.implementations().keys()
        )
        implementations = list(implementations)
        assert ref_impl in implementations
        implementations.remove(ref_impl)
        implementations = [ref_impl] + implementations

        implementations.remove(Implementation.PYTHON)  # no bilevel support in python

        method = {
            TimeIntegrator: time_integrator,
            Remesh: remesh_kernel,
        }

        def iter_impl(impl, method=method):
            graph = ComputationalGraph(name="test_graph")
            method[Interpolation] = Interpolation.LINEAR
            if impl is Implementation.OPENCL:
                for interp_method in (
                    Interpolation.LINEAR,
                ):  # PolynomialInterpolation.LINEAR):
                    graph = ComputationalGraph(name="test_graph")
                    method[Interpolation] = interp_method
                    for cl_env in iter_clenv():
                        msg = "platform {}, device {}, {}::{}".format(
                            cl_env.platform.name.strip(),
                            cl_env.device.name.strip(),
                            type(interp_method),
                            interp_method,
                        )
                        da = DirectionalAdvection(
                            velocity=vin,
                            advected_fields=sin,
                            advected_fields_out=sout,
                            dt=dt,
                            velocity_cfl=velocity_cfl,
                            variables=variables,
                            implementation=impl,
                            method=method,
                            name=f"advection_{str(impl).lower()}",
                        )
                        split = StrangSplitting(
                            splitting_dim=dim,
                            extra_kwds=dict(cl_env=cl_env),
                            order=StrangOrder.STRANG_SECOND_ORDER,
                        )
                        force_tstate = ForceTopologyState(
                            fields=variables.keys(),
                            variables=variables,
                            backend=Backend.OPENCL,
                            extra_kwds={"cl_env": cl_env},
                        )
                        split.push_operators(da)
                        graph.push_nodes(split, force_tstate)
                        yield msg, graph
            elif impl is Implementation.FORTRAN:
                assert dim == 3, "Scales is only 3D"
                adv = Advection(
                    velocity=vin,
                    advected_fields=sin,
                    advected_fields_out=sout,
                    dt=dt,
                    variables=variables,
                    implementation=impl,
                    method=method,
                    name=f"advection_{str(impl).lower()}",
                )
                graph.push_nodes(adv)
                yield "SCALES", graph
            else:
                msg = f"Unknown implementation to test {impl}."
                raise NotImplementedError(msg)

        # Compare to other implementations
        advec_axes = (tuple(),)
        advec_axes += tuple((x,) for x in range(dim))
        if dim > 1:
            advec_axes += (tuple(range(dim)),)

        reference_fields = {}
        for impl in implementations:
            print(f"\n >Implementation {impl}:", end=" ")
            is_ref = impl == ref_impl
            for sop, graph in iter_impl(impl):
                print(f"\n   *{sop}: ", end=" ")

                graph.build()

                for axes in advec_axes:
                    ref_outputs = reference_fields.setdefault(axes, {})
                    napplies = 10
                    Vi = npw.asarray(
                        [+1.0 if (i in axes) else +0.0 for i in range(dim)], dtype=dtype
                    )

                    dvin = graph.get_input_discrete_field(vin).as_contiguous_dfield()
                    dsin = graph.get_input_discrete_field(sin).as_contiguous_dfield()
                    dsout = graph.get_output_discrete_field(sout).as_contiguous_dfield()
                    dsref = dsout.clone()

                    sys.stdout.write(".")
                    sys.stdout.flush()
                    try:
                        dvin.initialize(self.__velocity_init, axes=axes)
                        dsin.initialize(self.__scalar_init)
                        _input = tuple(
                            dsin.data[i].get().handle.copy()
                            for i in range(dsin.nb_components)
                        )
                        S0 = dsin.integrate()

                        for k in range(napplies + 1):
                            if k > 0:
                                graph.apply()

                            output = tuple(
                                df.sdata[df.compute_slices].get().handle.copy()
                                for df in dsout.dfields
                            )

                            for i in range(dsout.nb_components):
                                mask = npw.isfinite(output[i][dsout.compute_slices])
                                if not mask.all():
                                    msg = f"\nFATAL ERROR: Output is not finite on axis {i}.\n"
                                    print(msg)
                                    npw.fancy_print(
                                        output[i],
                                        replace_values={
                                            (lambda a: npw.isfinite(a)): "."
                                        },
                                    )
                                    raise RuntimeError(msg)

                            if is_ref:
                                dxk = -Vi * (k + 0) * dt()
                                dsref.initialize(
                                    self.__scalar_init, offsets=dxk.tolist()
                                )
                                d = dsout.distance(dsref, p=2)
                                if npw.any(d > 1e-1):
                                    print(
                                        "FATAL ERROR: Could not match analytic advection."
                                    )
                                    print("DSOUT")
                                    print(dsout.sdata[dsout.compute_slices])
                                    print("DSREF")
                                    print(dsref.sdata[dsref.compute_slices])
                                    print("DSREF - DSOUT")
                                    print(
                                        dsout.sdata[dsout.compute_slices].get()
                                        - dsref.sdata[dsref.compute_slices].get()
                                    )
                                    msg = "Test failed with V={}, k={}, dxk={}, inter-field L2 distances are {}."
                                    msg = msg.format(
                                        Vi,
                                        k,
                                        to_tuple(dxk, cast=float),
                                        to_tuple(d, cast=float),
                                    )
                                    raise RuntimeError(msg)
                                ref_outputs[k] = output
                            else:
                                assert k in ref_outputs
                                reference = ref_outputs[k]
                                for i in range(dsout.nb_components):
                                    di = npw.abs(reference[i] - output[i])
                                    max_di = npw.max(di)
                                    neps = 10000
                                    max_tol = neps * npw.finfo(dsout.dtype).eps
                                    if max_di > max_tol:
                                        print(
                                            "FATAL ERROR: Could not match other implementation results."
                                        )
                                        print(
                                            f"\nComparisson failed at step {k} and component {i}:"
                                        )
                                        for j, dv in dvin.iter_fields():
                                            print(
                                                f"VELOCITY INPUT {DirectionLabels[j]}"
                                            )
                                            print(dv.sdata[dv.compute_slices])
                                        print("SCALAR INPUT")
                                        print(_input[i])
                                        print("SCALAR REFERENCE")
                                        print(reference[i])
                                        print("SCALAR OUTPUT")
                                        print(output[i])
                                        print("ABS(REF - OUT)")
                                        npw.fancy_print(
                                            di,
                                            replace_values={
                                                (lambda a: a < max_tol): "."
                                            },
                                        )
                                        print()
                                        msg = "Output did not match reference output for component {} at time step {}."
                                        msg += (
                                            f"\n > max computed distance was {max_di}."
                                        )
                                        msg += f"\n > max tolerence was set to {max_tol} ({neps} eps)."
                                        msg = msg.format(i, k)
                                        raise RuntimeError(msg)
                            Si = dsout.integrate()
                            if not npw.all(npw.isfinite(Si)):
                                msg = f"Integral is not finite. Got {Si}."
                                raise RuntimeError(msg)
                            if (npw.abs(Si - S0) > 1e-3).any():
                                msg = "Scalar was not conserved on iteration {}, expected {} but got {}."
                                msg = msg.format(
                                    k,
                                    to_tuple(S0, cast=float),
                                    to_tuple(Si, cast=float),
                                )
                                raise RuntimeError(msg)
                    except:
                        sys.stdout.write("\bx\n\n")
                        sys.stdout.flush()
                        raise

    def test_3D(self):
        if __SCALES_ENABLED__:
            self._test(dim=3, is_inplace=True)

    def perform_tests(self):
        # Scales is only 3D
        if __SCALES_ENABLED__:
            self._test(dim=3, is_inplace=True)
        print()


if __name__ == "__main__":
    import hysop

    TestBilevelAdvectionOperator.setup_class(
        enable_extra_tests=False, enable_debug_mode=False
    )

    test = TestBilevelAdvectionOperator()

    with printoptions(
        threshold=100000,
        linewidth=240,
        nanstr="nan",
        infstr="inf",
        formatter={"float": lambda x: f"{x:>6.2f}"},
    ):
        test.perform_tests()

    TestBilevelAdvectionOperator.teardown_class()
