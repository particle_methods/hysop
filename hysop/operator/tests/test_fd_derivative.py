# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Test gradient of fields.
"""
import itertools as it
import random

import sympy as sm
from hysop import Box, Field
from hysop.constants import HYSOP_REAL, Backend
from hysop.methods import SpaceDiscretization
from hysop.operator.derivative import FiniteDifferencesSpaceDerivative, Implementation
from hysop.operator.gradient import Gradient
from hysop.operator.misc import ForceTopologyState
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.testsenv import (
    __ENABLE_LONG_TESTS__,
    __HAS_OPENCL_BACKEND__,
    iter_clenv,
    opencl_failed,
)
from hysop.tools.contexts import printoptions
from hysop.tools.io_utils import IO
from hysop.tools.numerics import is_fp, is_integer
from hysop.tools.numpywrappers import npw
from hysop.tools.spectral_utils import (
    make_multivariate_polynomial,
    make_multivariate_trigonometric_polynomial,
)
from hysop.tools.sympy_utils import round_expr
from hysop.tools.htypes import check_instance, first_not_None


class TestFiniteDifferencesDerivative:

    @classmethod
    def setup_class(
        cls, enable_extra_tests=__ENABLE_LONG_TESTS__, enable_debug_mode=False
    ):

        IO.set_default_path("/tmp/hysop_tests/test_fd_derivative")

        cls.size_min = 23
        cls.size_max = 35

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_debug_mode = enable_debug_mode

        cls.t = ScalarParameter(name="t", dtype=HYSOP_REAL)

        cls.build_analytic_expressions()

    @classmethod
    def build_analytic_expressions(cls):
        from hysop.symbolic.base import TensorBase
        from hysop.symbolic.field import curl, laplacian
        from hysop.symbolic.frame import SymbolicFrame
        from hysop.tools.sympy_utils import enable_pretty_printing

        enable_pretty_printing()

        # at this moment we just test a periodic solver
        analytic_expressions = {}
        analytic_functions = {}
        for dim in (1, 2, 3):
            frame = SymbolicFrame(dim=dim)
            coords = frame.coords

            def gen_Fi():
                kis = tuple(random.randint(1, 5) * 2 * sm.pi for _ in range(dim))
                qis = tuple(npw.random.rand(dim).round(decimals=3).tolist())
                basis = tuple(
                    (sm.cos(ki * xi + qi), sm.sin(ki * xi + qi))
                    for (ki, qi, xi) in zip(kis, qis, coords)
                )
                F0 = sm.Integer(1) / (
                    sm.Integer(1) + npw.random.randint(1, 5) * cls.t.s
                )
                for i in range(dim):
                    F0 *= random.choice(basis[i])
                return F0

            def gen_F():
                F = (
                    npw.round(random.random(), 2) * gen_Fi()
                    - npw.round(random.random(), 2) * gen_Fi()
                )
                return F

            params = coords + (cls.t.s,)
            Fs = npw.asarray([gen_F(), gen_F(), gen_F()], dtype=object).view(TensorBase)
            fFs = tuple(sm.lambdify(params, F) for F in Fs)

            dFs = npw.empty(shape=(3, dim), dtype=object)
            fdFs = npw.empty_like(dFs)
            for i, Fi in enumerate(Fs):
                for j, xj in enumerate(coords):
                    dFij = Fi.diff(xj)
                    dFs[i, j] = dFij
                    fdFs[i, j] = sm.lambdify(params, dFij)

            analytic_expressions[dim] = {"F": Fs, "gradF": dFs}
            analytic_functions[dim] = {"F": fFs, "gradF": fdFs}

        cls.analytic_expressions = analytic_expressions
        cls.analytic_functions = analytic_functions

    @classmethod
    def teardown_class(cls):
        pass

    @staticmethod
    def __random_init(data, coords, component):
        dtype = data.dtype
        if is_fp(dtype):
            data[...] = npw.random.random(size=data.shape).astype(dtype=dtype)
        else:
            msg = f"Unknown dtype {dtype}."
            raise NotImplementedError(msg)

    @classmethod
    def __analytic_init(cls, data, coords, component, fns, t):
        data[...] = fns[component](*(coords + (t(),))).astype(data.dtype)

    def _test(self, dim, dtype, size_min=None, size_max=None):
        enable_extra_tests = self.enable_extra_tests

        size_min = first_not_None(size_min, self.size_min)
        size_max = first_not_None(size_max, self.size_max)

        shape = tuple(
            npw.random.randint(low=size_min, high=size_max + 1, size=dim).tolist()
        )

        domain = Box(length=(1,) * dim)
        F = Field(domain=domain, name="F", dtype=dtype, nb_components=3)
        gradF = F.gradient()

        self._test_one(
            shape=shape, dim=dim, dtype=dtype, domain=domain, F=F, gradF=gradF
        )

    def _test_one(self, shape, dim, dtype, domain, F, gradF):

        print(
            "\nTesting {}D Gradient: dtype={} shape={}".format(
                dim, dtype.__name__, shape
            )
        )
        print(" >Input analytic functions:")
        for i, Fi in enumerate(self.analytic_expressions[dim]["F"]):
            print(f"   *F{i}(x,t) = {Fi}")
        print(" >Expected analytic outputs:")
        from hysop.tools.sympy_utils import partial, subscript

        for i, j in npw.ndindex(*self.analytic_expressions[dim]["gradF"].shape):
            dFij = self.analytic_expressions[dim]["gradF"][i, j]
            print(
                "   *{p}F{}/{p}x{}(x,t) = {}".format(
                    subscript(i), subscript(j), dFij, p=partial
                )
            )
        self.t.value = random.random()
        print(f" >Parameter t has been set to {self.t()}.")
        print(" >Testing all implementations:")

        implementations = FiniteDifferencesSpaceDerivative.implementations()

        variables = {F: shape, gradF: shape}
        fns = self.analytic_functions[dim]["F"]
        dfns = tuple(self.analytic_functions[dim]["gradF"].ravel().tolist())
        exprs = self.analytic_expressions[dim]["F"]
        dexprs = self.analytic_expressions[dim]["gradF"]

        def iter_impl(impl):
            base_kwds = dict(
                F=F,
                gradF=gradF,
                variables=variables,
                implementation=impl,
                method={SpaceDiscretization: 8},
            )
            if impl is Implementation.PYTHON:
                msg = "   *Python: "
                print(msg, end=" ")
                op = Gradient(**base_kwds)
                yield op.to_graph()
                print()
            elif impl is Implementation.OPENCL:
                msg = "   *Opencl: "
                print(msg)
                for cl_env in iter_clenv():
                    msg = "      >platform {}, device {}:".format(
                        cl_env.platform.name.strip(), cl_env.device.name.strip()
                    )
                    print(msg, end=" ")
                    op0 = Gradient(cl_env=cl_env, **base_kwds)
                    op1 = ForceTopologyState(
                        fields=gradF,
                        variables={gradF: shape},
                        backend=Backend.OPENCL,
                        extra_kwds={"cl_env": cl_env},
                    )
                    op = op0.to_graph()
                    op.push_nodes(op1)
                    yield op
                    print()
                print()
            else:
                msg = f"Unknown implementation to test {impl}."
                raise NotImplementedError(msg)

        # Compare to analytic solution
        Fref = None
        for impl in implementations:
            for op in iter_impl(impl):
                op.build(outputs_are_inputs=True)

                dF = op.get_input_discrete_field(F)
                dgradF = op.get_output_discrete_field(gradF)

                if Fref is None:
                    dgradF.initialize(self.__analytic_init, fns=dfns, t=self.t)
                    gradFref = tuple(data.get().handle.copy() for data in dgradF.data)

                dF.initialize(self.__analytic_init, fns=fns, t=self.t)
                Fref = tuple(data.get().handle.copy() for data in dF.data)

                dgradF.initialize(self.__random_init)
                op.apply()

                Fout = tuple(data.get().handle.copy() for data in dF.data)
                gradFout = tuple(data.get().handle.copy() for data in dgradF.data)

                self._check_output(impl, op, Fref, gradFref, Fout, gradFout)

    @classmethod
    def _check_output(cls, impl, op, Fref, gradFref, Fout, gradFout):
        check_instance(Fref, tuple, values=npw.ndarray)
        check_instance(gradFref, tuple, values=npw.ndarray)
        check_instance(Fout, tuple, values=npw.ndarray, size=len(Fref))
        check_instance(gradFout, tuple, values=npw.ndarray, size=len(gradFref))

        for out_buffers, ref_buffers, name in zip(
            (Fout, gradFout), (Fref, gradFref), ("F", "gradF")
        ):
            for i, (fout, fref) in enumerate(zip(out_buffers, ref_buffers)):
                iname = f"{name}{i}"
                assert fout.dtype == fref.dtype, iname
                assert fout.shape == fref.shape, iname

                eps = npw.finfo(fout.dtype).eps
                dist = npw.abs(fout - fref)
                dL1 = npw.nansum(dist) / dist.size
                if dL1 < 2e-1:
                    print(f"{dL1}, ", end=" ")
                    continue
                has_nan = npw.any(npw.isnan(fout))
                has_inf = npw.any(npw.isinf(fout))

                print()
                print()
                print(f"Test output comparisson for {name} failed for component {i}:")
                print(f" *has_nan: {has_nan}")
                print(f" *has_inf: {has_inf}")
                print(f" *dL1={dL1}")
                print()
                if cls.enable_debug_mode:
                    print("REFERENCE INPUTS:")
                    for i, w in enumerate(Fref):
                        print(f"F{i}")
                        print(w)
                        print()
                    if name == "gradF":
                        print("REFERENCE OUTPUT:")
                        for i, u in enumerate(gradFref):
                            print(f"gradF{i}")
                            print(u)
                            print()
                        print()
                        print(f"OPERATOR {op.name.upper()} OUTPUT:")
                        print()
                        for i, u in enumerate(gradFout):
                            print(f"gradF{i}")
                            print(u)
                            print()
                    else:
                        print("MODIFIED INPUTS:")
                        for i, w in enumerate(Fout):
                            print(f"F{i}")
                            print(w)
                            print()
                    print()

                msg = f"Test failed for {name} on component {i} for implementation {impl}."
                raise RuntimeError(msg)

    def test_1d_float32(self):
        self._test(dim=1, dtype=npw.float32)

    def test_2d_float32(self):
        self._test(dim=2, dtype=npw.float32)

    def test_3d_float32(self):
        self._test(dim=3, dtype=npw.float32)

    def test_1d_float64(self):
        self._test(dim=1, dtype=npw.float64)

    def test_2d_float64(self):
        self._test(dim=2, dtype=npw.float64)

    def test_3d_float64(self):
        self._test(dim=3, dtype=npw.float64)

    def perform_tests(self):
        self.test_1d_float32()
        self.test_2d_float32()
        self.test_3d_float32()

        self.test_1d_float64()
        self.test_2d_float64()
        self.test_3d_float64()


if __name__ == "__main__":
    TestFiniteDifferencesDerivative.setup_class(
        enable_extra_tests=False, enable_debug_mode=False
    )

    test = TestFiniteDifferencesDerivative()

    with printoptions(
        threshold=10000,
        linewidth=1000,
        nanstr="nan",
        infstr="inf",
        formatter={"float": lambda x: f"{x:>6.2f}"},
    ):
        test.perform_tests()

    TestFiniteDifferencesDerivative.teardown_class()
