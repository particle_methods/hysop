# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Test of multiresolution filter
"""
from hysop.testsenv import __ENABLE_LONG_TESTS__
from hysop.tools.io_utils import IO
from hysop.tools.numpywrappers import npw
from hysop.tools.htypes import first_not_None
from hysop.operator.spatial_filtering import SpatialFilter
from hysop.methods import FilteringMethod
from hysop.topology.cartesian_topology import CartesianTopology
from hysop.constants import implementation_to_backend, Implementation, HYSOP_REAL
from hysop.tools.parameters import CartesianDiscretization
from hysop.tools.contexts import printoptions
from hysop.tools.numerics import is_fp, is_integer
from hysop import Field, Box, MPIParams


class TestMultiresolutionFilter:

    @staticmethod
    def __f_init(data, coords, component):
        from numpy import sin, cos

        (x, y, z) = coords
        data[...] = -cos(x) * sin(y) * sin(z)

    @staticmethod
    def __random_init(data, coords, component):
        dtype = data.dtype
        if is_fp(dtype):
            data[...] = npw.random.random(size=data.shape).astype(dtype=dtype)
        else:
            msg = f"Unknown dtype {dtype}."
            raise NotImplementedError(msg)

    @classmethod
    def setup_class(
        cls, enable_extra_tests=__ENABLE_LONG_TESTS__, enable_debug_mode=False
    ):

        IO.set_default_path("/tmp/hysop_tests/test_multiresolution_filter")

        if enable_debug_mode:
            cls.size_min = 15
            cls.size_max = 16
        else:
            cls.size_min = 23
            cls.size_max = 87

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_debug_mode = enable_debug_mode

    def _test(self, dim, dtype, factor=2, size_min=None, size_max=None):
        enable_extra_tests = self.enable_extra_tests

        size_min = first_not_None(size_min, self.size_min)
        size_max = first_not_None(size_max, self.size_max)
        # shape_c = tuple(npw.random.randint(low=size_min,
        #                                    high=size_max+1,
        #                                    size=dim).tolist())
        # shape_f = tuple([_*factor for _ in shape_c])
        shape_c = (32,) * 3
        shape_f = (64,) * 3

        domain = Box(length=(1,) * dim)
        f = Field(domain=domain, name="velo", dtype=dtype, nb_components=1)

        self._test_one(
            shape_f=shape_f,
            shape_c=shape_c,
            dim=dim,
            dtype=dtype,
            domain=domain,
            f=f,
            factor=factor,
        )

    def _test_one(self, shape_f, shape_c, dim, dtype, domain, f, factor):
        print(
            "\nTesting {}D SpatialFilter: dtype={} shape_f={} shape_c={}".format(
                dim, dtype.__name__, shape_f, shape_c
            )
        )
        implementations = (Implementation.PYTHON,)
        mpi_params = MPIParams(comm=domain.task_comm, task_id=domain.current_task())

        def iter_impl(impl):
            topo_f = CartesianTopology(
                domain=domain,
                backend=implementation_to_backend(impl),
                discretization=CartesianDiscretization(
                    shape_f, default_boundaries=True
                ),
                mpi_params=mpi_params,
            )
            topo_c = CartesianTopology(
                domain=domain,
                backend=implementation_to_backend(impl),
                discretization=CartesianDiscretization(
                    shape_c, default_boundaries=True
                ),
                mpi_params=mpi_params,
            )
            base_kwds = dict(
                implementation=impl,
                name=f"multiresolution_filter_{str(impl).lower()}",
                filtering_method=FilteringMethod.SUBGRID,
            )
            if impl is Implementation.PYTHON:
                msg = "   *Python: "
                print(msg, end=" ")
                yield SpatialFilter(
                    input_variables={f: topo_f},
                    output_variables={f: topo_c},
                    **base_kwds,
                )
                print()
            else:
                msg = f"Unknown implementation to test {impl}."
                raise NotImplementedError(msg)

        Fref = None
        for impl in implementations:
            for op in iter_impl(impl):
                op = op.build()
                din = op.input_discrete_fields[f]
                dout = op.output_discrete_fields[f]
                din.initialize(self.__f_init)
                dout.initialize(self.__f_init)
                Fref = tuple(data.get().handle.copy() for data in dout.data)
                dout.initialize(self.__random_init)
                op.apply(simulation=None)
                Fout = tuple(data.get().handle.copy() for data in dout.data)

                msg0 = "Reference field {} is not finite."
                for i, field in enumerate(Fout):
                    iname = f"F{i}"
                    mask = npw.isfinite(field)
                    if not mask.all():
                        print()
                        print(field)
                        print()
                        print(field[~mask])
                        print()
                        msg = msg0.format(iname)
                        raise ValueError(msg)

                for i, (fout, fref) in enumerate(zip(Fout, Fref)):
                    assert fout.dtype == fref.dtype
                    assert fout.shape == fref.shape

                eps = npw.finfo(din.data[0].dtype).eps
                dist = npw.abs(Fout[0] - Fref[0])
                dinf = npw.max(dist)
                deps = int(npw.ceil(dinf / eps))
                if deps < 10:
                    print(f"{deps}eps, ", end=" ")
                    return
                print()
                print()
                print("Test output comparisson failed for flowrate:")
                print(f" *dinf={dinf} ({deps} eps)")
                print()
                msg = f"Test failed on flowrate for implementation {impl}."
                raise RuntimeError(msg)

    @classmethod
    def teardown_class(cls):
        pass

    def perform_tests(self):
        self._test(dim=3, dtype=HYSOP_REAL)
        print()

    def test_3d(self):
        self._test(dim=3, dtype=HYSOP_REAL)


if __name__ == "__main__":
    TestMultiresolutionFilter.setup_class(
        enable_extra_tests=False, enable_debug_mode=False
    )

    test = TestMultiresolutionFilter()

    with printoptions(
        threshold=10000,
        linewidth=240,
        nanstr="nan",
        infstr="inf",
        formatter={"float": lambda x: f"{x:>6.2f}"},
    ):
        test.perform_tests()

    TestMultiresolutionFilter.teardown_class()
