# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Test gradient of fields.
"""
import itertools as it
import random

import sympy as sm
from hysop import Box, Field
from hysop.constants import HYSOP_REAL, Backend, BoundaryCondition, BoxBoundaryCondition
from hysop.methods import SpaceDiscretization
from hysop.operator.derivative import Implementation, SpectralSpaceDerivative
from hysop.operator.gradient import Gradient
from hysop.operator.misc import ForceTopologyState
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.testsenv import (
    __ENABLE_LONG_TESTS__,
    __HAS_OPENCL_BACKEND__,
    domain_boundary_iterator,
    iter_clenv,
    opencl_failed,
)
from hysop.tools.contexts import printoptions
from hysop.tools.io_utils import IO
from hysop.tools.numerics import is_fp, is_integer
from hysop.tools.numpywrappers import npw
from hysop.tools.spectral_utils import (
    make_multivariate_polynomial,
    make_multivariate_trigonometric_polynomial,
)
from hysop.tools.sympy_utils import round_expr, truncate_expr
from hysop.tools.htypes import check_instance, first_not_None


class TestSpectralDerivative:

    @classmethod
    def setup_class(
        cls, enable_extra_tests=__ENABLE_LONG_TESTS__, enable_debug_mode=False
    ):

        IO.set_default_path("/tmp/hysop_tests/test_spectral_derivative")

        cls.size_min = 8
        cls.size_max = 16

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_debug_mode = enable_debug_mode

        cls.t = ScalarParameter(name="t", dtype=HYSOP_REAL)

    @classmethod
    def build_analytic_expressions(
        cls, polynomial, dim, max_derivative, lboundaries, rboundaries, origin, end
    ):
        from hysop.symbolic.base import TensorBase
        from hysop.symbolic.field import curl, laplacian
        from hysop.symbolic.frame import SymbolicFrame
        from hysop.tools.sympy_utils import enable_pretty_printing

        enable_pretty_printing()

        frame = SymbolicFrame(dim=dim)
        coords = frame.coords
        params = coords + (cls.t.s,)

        def gen_F():
            if polynomial:
                f, y = make_multivariate_polynomial(
                    origin, end, lboundaries, rboundaries, 10, 4
                )
            else:
                f, y = make_multivariate_trigonometric_polynomial(
                    origin, end, lboundaries, rboundaries, 2
                )
            f = f.xreplace({yi: xi for (yi, xi) in zip(y, frame.coords)})
            f *= sm.Integer(1) / (sm.Integer(1) + npw.random.randint(1, 5) * cls.t.s)
            return f

        F = gen_F()
        fF = sm.lambdify(params, F)

        dFs = {}
        fdFs = {}
        symbolic_dvars = {}
        for idx in it.product(range(max_derivative + 1), repeat=dim):
            if sum(idx) > max_derivative:
                continue
            xvars = tuple((ci, i) for (i, ci) in zip(idx, coords))
            symbolic_dvars[idx] = xvars
            dF = F
            for ci, i in xvars:
                if i == 0:
                    continue
                dF = dF.diff(ci, i)
            dFs[idx] = dF
            fdFs[idx] = sm.lambdify(params, dF)

        analytic_expressions = {"F": F, "dF": dFs}
        analytic_functions = {"F": fF, "dF": fdFs}
        return (symbolic_dvars, analytic_expressions, analytic_functions)

    @classmethod
    def teardown_class(cls):
        pass

    @staticmethod
    def __random_init(data, coords, component):
        dtype = data.dtype
        if is_fp(dtype):
            data[...] = npw.random.random(size=data.shape).astype(dtype=dtype)
        else:
            msg = f"Unknown dtype {dtype}."
            raise NotImplementedError(msg)

    @staticmethod
    def __analytic_init(data, coords, fns, t, component):
        fn = fns[component]
        data[...] = npw.asarray(fn(*(coords + (t(),)))).astype(data.dtype)

    def _test(
        self,
        dim,
        dtype,
        polynomial,
        max_derivative=2,
        size_min=None,
        size_max=None,
        max_runs=None,
    ):
        enable_extra_tests = self.enable_extra_tests

        size_min = first_not_None(size_min, self.size_min)
        size_max = first_not_None(size_max, self.size_max)

        shape = tuple(
            npw.random.randint(low=size_min, high=size_max + 1, size=dim).tolist()
        )

        domain_boundaries = list(domain_boundary_iterator(dim=dim))
        periodic = domain_boundaries[0]
        domain_boundaries = domain_boundaries[1:]
        if max_runs is not None:
            random.shuffle(domain_boundaries)
        domain_boundaries.insert(0, periodic)

        for i, (lboundaries, rboundaries) in enumerate(domain_boundaries, 1):
            domain = Box(
                origin=(npw.random.rand(dim) - 0.5),
                length=(npw.random.rand(dim) + 0.5) * 2 * npw.pi,
                lboundaries=lboundaries,
                rboundaries=rboundaries,
            )

            F = Field(domain=domain, name="F", dtype=dtype)

            self._test_one(
                shape=shape,
                dim=dim,
                dtype=dtype,
                domain=domain,
                F=F,
                polynomial=polynomial,
                max_derivative=max_derivative,
            )

            if (max_runs is not None) and (i == max_runs):
                missing = ((4 ** (dim + 1) - 1) // 3) - i
                print()
                print(
                    ">> MAX RUNS ACHIEVED FOR {}D DOMAINS -- SKIPING {} OTHER BOUNDARY CONDITIONS <<".format(
                        dim, missing
                    )
                )
                print()
                print()
                break
        else:
            assert i == (4 ** (dim + 1) - 1) // 3, (i + 1, (4 ** (dim + 1) - 1) // 3)
            print()
            print(f">> TESTED ALL {dim}D BOUNDARY CONDITIONS <<")
            print()
            print()

    def _test_one(self, shape, dim, dtype, domain, F, polynomial, max_derivative):

        implementations = SpectralSpaceDerivative.implementations()

        (symbolic_dvars, analytic_expressions, analytic_functions) = (
            self.build_analytic_expressions(
                dim=dim,
                polynomial=polynomial,
                max_derivative=max_derivative,
                lboundaries=F.lboundaries[
                    ::-1
                ],  # => boundaries in variable order x0,...,xn
                rboundaries=F.rboundaries[::-1],
                origin=domain.origin[::-1],
                end=domain.end[::-1],
            )
        )

        Fs = analytic_expressions["F"]
        fFs = analytic_functions["F"]

        def format_expr(e):
            return truncate_expr(round_expr(e, 3), 80)

        msg = "\nTesting {}D SpectralDerivative: dtype={} shape={}, polynomial={}, bc=[{}]"
        msg = msg.format(
            dim, dtype.__name__, shape, polynomial, F.domain.format_boundaries()
        )
        msg += f"\n >Corresponding field boundary conditions are [{F.format_boundaries()}]."
        msg += "\n >Input analytic functions (truncated):"
        msg += f"\n   *{F.pretty_name}(x,t) = {format_expr(Fs)}"
        msg += "\n >Testing derivatives:"
        print(msg)

        for idx in sorted(symbolic_dvars.keys(), key=lambda x: sum(x)):
            xvars = symbolic_dvars[idx]
            dFe = F.s()
            for ci, i in xvars:
                if i == 0:
                    continue
                dFe = dFe.diff(ci, i)
            dF = F.from_sympy_expression(expr=dFe, space_symbols=domain.frame.coords)
            dFs = analytic_expressions["dF"][idx]
            fdFs = analytic_functions["dF"][idx]
            print(f"   *{dF.pretty_name}")

            variables = {F: shape, dF: shape}

            def iter_impl(impl):
                base_kwds = dict(
                    F=F,
                    dF=dF,
                    derivative=idx,
                    variables=variables,
                    implementation=impl,
                    testing=True,
                )
                if impl is Implementation.PYTHON:
                    msg = "     |Python: "
                    print(msg, end=" ")
                    op = SpectralSpaceDerivative(**base_kwds)
                    yield op.to_graph()
                    print()
                elif impl is Implementation.OPENCL:
                    msg = "     |Opencl: "
                    print(msg)
                    for cl_env in iter_clenv():
                        msg = "        >platform {}, device {}:".format(
                            cl_env.platform.name.strip(), cl_env.device.name.strip()
                        )
                        print(msg, end=" ")
                        op = SpectralSpaceDerivative(cl_env=cl_env, **base_kwds)
                        yield op.to_graph()
                        print()
                else:
                    msg = f"Unknown implementation to test {impl}."
                    raise NotImplementedError(msg)

            # Compare to analytic solution
            Fref = None
            for impl in implementations:
                for op in iter_impl(impl):
                    op.build(outputs_are_inputs=False)

                    Fd = op.get_input_discrete_field(F)
                    dFd = op.get_output_discrete_field(dF)

                    if Fref is None:
                        dFd.initialize(self.__analytic_init, fns=(fdFs,), t=self.t)
                        dFref = tuple(data.get().handle.copy() for data in dFd.data)

                    Fd.initialize(self.__analytic_init, fns=(fFs,), t=self.t)
                    Fref = tuple(data.get().handle.copy() for data in Fd.data)

                    dFd.initialize(self.__random_init)
                    op.apply()

                    Fout = tuple(data.get().handle.copy() for data in Fd.data)
                    dFout = tuple(data.get().handle.copy() for data in dFd.data)

                    self._check_output(impl, op, Fref, dFref, Fout, dFout, idx)

    @classmethod
    def _check_output(cls, impl, op, Fref, dFref, Fout, dFout, idx):
        nidx = sum(idx)
        check_instance(Fref, tuple, values=npw.ndarray)
        check_instance(dFref, tuple, values=npw.ndarray)
        check_instance(Fout, tuple, values=npw.ndarray, size=len(Fref))
        check_instance(dFout, tuple, values=npw.ndarray, size=len(dFref))

        for j, (out_buffers, ref_buffers, name) in enumerate(
            zip((Fout, dFout), (Fref, dFref), ("F", "dF"))
        ):
            for i, (fout, fref) in enumerate(zip(out_buffers, ref_buffers)):
                iname = f"{name}{i}"
                assert fout.dtype == fref.dtype, iname
                assert fout.shape == fref.shape, iname

                assert not npw.any(npw.isnan(fref))
                assert not npw.any(npw.isinf(fref))
                has_nan = npw.any(npw.isnan(fout))
                has_inf = npw.any(npw.isinf(fout))

                if has_nan or has_inf:
                    pass
                else:
                    eps = npw.finfo(fout.dtype).eps
                    dist = npw.abs(fout - fref)
                    dinf = npw.max(dist)
                    try:
                        deps = int(dinf / eps)
                    except OverflowError:
                        import numpy as np

                        deps = np.inf
                    if deps <= 5 * 10 ** (nidx + 2):
                        if j == 1:
                            print(f"{deps}eps ({dinf})", end=" ")
                        else:
                            print(f"{deps}eps, ", end=" ")
                        continue

                print()
                print()
                print(f"Test output comparisson for {name} failed for component {i}:")
                print(f" *has_nan: {has_nan}")
                print(f" *has_inf: {has_inf}")
                print(f" *dinf={dinf}")
                print(f" *deps={deps}")
                print()
                if cls.enable_debug_mode:
                    print("REFERENCE INPUTS:")
                    for i, w in enumerate(Fref):
                        print(f"F{i}")
                        print(w)
                        print()
                    if name == "dF":
                        print("REFERENCE OUTPUT:")
                        for i, u in enumerate(dFref):
                            print(f"dF{i}")
                            print(u)
                            print()
                        print()
                        print(f"OPERATOR {op.name.upper()} OUTPUT:")
                        print()
                        for i, u in enumerate(dFout):
                            print(f"dF{i}")
                            print(u)
                            print()
                    else:
                        print("MODIFIED INPUTS:")
                        for i, w in enumerate(Fout):
                            print(f"F{i}")
                            print(w)
                            print()
                    print()

                msg = (
                    "Test failed for {} on component {} for implementation {}.".format(
                        name, i, impl
                    )
                )
                raise RuntimeError(msg)

    def test_1d_trigonometric_float32(self, **kwds):
        kwds.update({"max_derivative": 3})
        if __ENABLE_LONG_TESTS__:
            self._test(dim=1, dtype=npw.float32, polynomial=False, **kwds)

    def test_2d_trigonometric_float32(self, **kwds):
        kwds.update({"max_derivative": 1, "max_runs": None})
        if __ENABLE_LONG_TESTS__:
            self._test(dim=2, dtype=npw.float32, polynomial=False, **kwds)

    def test_3d_trigonometric_float32(self, **kwds):
        kwds.update({"max_derivative": 1, "max_runs": 4})
        self._test(dim=3, dtype=npw.float32, polynomial=False, **kwds)

    def test_1d_trigonometric_float64(self, **kwds):
        kwds.update({"max_derivative": 3})
        if __ENABLE_LONG_TESTS__:
            self._test(dim=1, dtype=npw.float64, polynomial=False, **kwds)

    def test_2d_trigonometric_float64(self, **kwds):
        kwds.update({"max_derivative": 1, "max_runs": None})
        if __ENABLE_LONG_TESTS__:
            self._test(dim=2, dtype=npw.float64, polynomial=False, **kwds)

    def test_3d_trigonometric_float64(self, **kwds):
        kwds.update({"max_derivative": 1, "max_runs": 4})
        self._test(dim=3, dtype=npw.float64, polynomial=False, **kwds)

    def test_1d_polynomial_float32(self, **kwds):
        if __ENABLE_LONG_TESTS__:
            self._test(dim=1, dtype=npw.float32, polynomial=True, **kwds)

    def test_2d_polynomial_float32(self, **kwds):
        if __ENABLE_LONG_TESTS__:
            self._test(dim=2, dtype=npw.float32, polynomial=True, **kwds)

    def test_3d_polynomial_float32(self, **kwds):
        kwds.update({"max_derivative": 1})
        if __ENABLE_LONG_TESTS__:
            self._test(dim=3, dtype=npw.float32, polynomial=True, **kwds)

    def perform_tests(self):
        self.test_1d_trigonometric_float32(max_derivative=3)
        self.test_2d_trigonometric_float32(max_derivative=1, max_runs=None)
        self.test_3d_trigonometric_float32(max_derivative=1, max_runs=4)

        self.test_1d_trigonometric_float64(max_derivative=3)
        self.test_2d_trigonometric_float64(max_derivative=1, max_runs=None)
        self.test_3d_trigonometric_float64(max_derivative=1, max_runs=4)

        self.test_1d_polynomial_float32(max_derivative=3)
        self.test_2d_polynomial_float32(max_derivative=2)
        self.test_3d_polynomial_float32(max_derivative=1)


if __name__ == "__main__":
    TestSpectralDerivative.setup_class(
        enable_extra_tests=False, enable_debug_mode=False
    )

    test = TestSpectralDerivative()

    with printoptions(
        threshold=10000,
        linewidth=1000,
        nanstr="nan",
        infstr="inf",
        formatter={"float": lambda x: f"{x:>6.2f}"},
    ):
        test.perform_tests()

    TestSpectralDerivative.teardown_class()
