# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import itertools as it

import numpy as np
import sympy as sm
from hysop import Box, Field
from hysop.constants import (
    HYSOP_REAL,
    ComputeGranularity,
    Implementation,
    SpaceDiscretization,
)
from hysop.operator.custom_symbolic import CustomSymbolicOperator
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.parameters.tensor_parameter import TensorParameter
from hysop.symbolic import dtime_symbol
from hysop.symbolic.relational import Assignment
from hysop.testsenv import (
    __ENABLE_LONG_TESTS__,
    __HAS_OPENCL_BACKEND__,
    iter_clenv,
    opencl_failed,
)
from hysop.tools.contexts import printoptions
from hysop.tools.io_utils import IO
from hysop.tools.numerics import is_fp, is_integer
from hysop.tools.sympy_utils import enable_pretty_printing
from hysop.tools.htypes import check_instance, first_not_None, to_tuple

if __HAS_OPENCL_BACKEND__:
    from hysop.symbolic.array import OpenClSymbolicArray, OpenClSymbolicBuffer

from hysop.numerics.odesolvers.runge_kutta import (
    RK2,
    RK3,
    RK4,
    RK4_38,
    Euler,
    TimeIntegrator,
)
from hysop.numerics.stencil.stencil_generator import (
    MPQ,
    CenteredStencilGenerator,
    StencilGenerator,
)


class TestCustomSymbolic:

    @classmethod
    def setup_class(
        cls, enable_extra_tests=__ENABLE_LONG_TESTS__, enable_debug_mode=False
    ):

        IO.set_default_path("/tmp/hysop_tests/test_custom_symbolic")

        if enable_debug_mode:
            cls.size_min0 = 20
            cls.size_max0 = 20
            cls.size_min = 4
            cls.size_max = 6
        else:
            cls.size_min0 = 16
            cls.size_max0 = 16
            cls.size_min = 16
            cls.size_max = 16

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_debug_mode = enable_debug_mode

        cls.dtypes = [
            np.int8,
            np.int16,
            np.int32,
            np.int64,
            np.uint8,
            np.uint16,
            np.uint32,
            np.uint64,
            np.float32,
            np.float64,
        ]

    @classmethod
    def teardown_class(cls):
        pass

    @staticmethod
    def __field_init(data, coords, dtype, component, pollute=False):
        shape = data[0].shape
        if is_integer(dtype):
            data[...] = np.random.random_integers(low=0, high=255, size=shape)
        elif is_fp(dtype):
            if pollute:
                data[...] = np.nan
            else:
                data[...] = np.random.random(size=data.shape)
        else:
            msg = f"Unknown dtype {dtype}."
            raise NotImplementedError(msg)

    @staticmethod
    def iter_implementations(op_cls, base_kwds):
        for impl in op_cls.implementations():
            base_kwds = {k: v for (k, v) in base_kwds.items()}
            base_kwds["implementation"] = impl
            if impl is Implementation.OPENCL:
                for cl_env in iter_clenv():
                    print(
                        "      *platform {}, device {}: ".format(
                            cl_env.platform.name.strip(), cl_env.device.name.strip()
                        ),
                        end=" ",
                    )
                    yield impl, op_cls(cl_env=cl_env, **base_kwds)
            else:
                print(f"      *implementation {impl}: ", end=" ")
                yield impl, op_cls(**base_kwds)

    @classmethod
    def _check_output(
        cls, impl, op, in_names, refin_buffers, out_names, refout_buffers, out_buffers
    ):
        check_instance(out_buffers, tuple, values=np.ndarray)
        check_instance(refout_buffers, tuple, values=np.ndarray)
        check_instance(refin_buffers, tuple, values=np.ndarray)

        for i, (oname, out, refout) in enumerate(
            zip(out_names, out_buffers, refout_buffers)
        ):
            assert refout.dtype == out.dtype, f"{refout.dtype} vs {out.dtype}"
            assert refout.shape == out.shape, f"{refout.shape} vs {out.shape}"

            has_nan = np.any(np.isnan(refout))
            has_inf = np.any(np.isinf(refout))
            if out.dtype != refout.dtype:
                raise RuntimeError(f"Types do not match: {out.dtype} vs {refout.dtype}")
            if has_nan:
                raise RuntimeError("Reference output contains nan values.")
            if has_inf:
                raise RuntimeError("Reference output contains inf. values.")

            has_nan = np.any(np.isnan(out))
            has_inf = np.any(np.isinf(out))

            distances = np.abs(out - refout) / np.max(refout)
            if is_integer(out.dtype):
                mask = out == refout
                max_dist = np.max(distances)
                msg = f"{max_dist}  "
            elif is_fp(out.dtype):
                eps = np.finfo(out.dtype).eps
                eps_distances = np.ceil(distances / eps).astype(np.int64)
                mask = eps_distances < 8192
                max_dist = np.max(distances)
                max_eps = np.max(eps_distances)
                msg = f"{max_eps}eps  "
            else:
                msg = f"Unknown dtype {out.dtype}."
                raise NotImplementedError(msg)

            if (not has_nan) and (not has_inf) and mask.all():
                print(msg, end=" ")
                continue
            print()
            print(f"Failed to match output of {oname}:")
            print()
            print(f"Test output comparisson failed for component {i}:")
            print(f" *has_nan: {has_nan}")
            print(f" *has_inf: {has_inf}")
            print()
            if cls.enable_debug_mode:
                mask[...] = False
            print("REFERENCE INPUTS:")
            for name, _in in zip(in_names, refin_buffers):
                print(name)
                try:
                    print(_in[~mask])
                except IndexError:
                    print(_in)
                print()
            print()
            print(f"{oname} REFERENCE OUTPUT:")
            print(refout[~mask])
            print()
            print(f"{oname} OPERATOR OUTPUT:")
            print(out[~mask])
            print()
            if is_fp(out.dtype):
                print(f"{oname} DISTANCES:")
                print(distances[~mask])
                print()
                print(f"{oname} DISTANCES (EPS):")
                print(eps_distances[~mask])
                print()

            msg = f"Test failed on component {i} for implementation {impl}."
            raise RuntimeError(msg)
        print()

    def _test_simple(self, dim, _size_min=None, _size_max=None):
        enable_extra_tests = self.enable_extra_tests
        assert dim >= 1

        print("TEST SIMPLE")
        print(f" DIM {dim}")

        size_min = first_not_None(_size_min, self.size_min)
        size_max = first_not_None(_size_max, self.size_max)
        size_min0 = first_not_None(_size_min, self.size_min0)
        size_max0 = first_not_None(_size_max, self.size_max0)

        domain = Box(length=(1.0,) * dim)

        discretization = tuple(
            np.random.randint(low=size_min, high=size_max + 1, size=dim - 1).tolist()
        )
        discretization0 = tuple(
            np.random.randint(low=size_min0, high=size_max0 + 1, size=1).tolist()
        )
        discretization = discretization + discretization0

        print(f" DISCRETIZATION {discretization}")

        if __ENABLE_LONG_TESTS__:
            dtypes = (np.float32, np.float64)
        else:
            dtypes = (HYSOP_REAL,)

        for dtype in dtypes:
            print(f"  DTYPE {dtype.__name__}")
            A = Field(domain=domain, name="A", dtype=dtype, nb_components=1)
            B = Field(domain=domain, name="B", dtype=dtype, nb_components=1)
            C = Field(domain=domain, name="C", dtype=dtype, nb_components=3)

            P0 = ScalarParameter("P0", dtype=np.float32, initial_value=1.0)
            P1 = ScalarParameter("P1", dtype=np.float64, initial_value=2.0, const=True)
            T0 = TensorParameter(
                "T0",
                shape=(13,),
                dtype=np.int32,
                initial_value=np.arange(13, dtype=np.int32),
            )
            T1 = TensorParameter("T1", shape=(3, 3), dtype=np.int32)

            As = A.s()
            Bs = B.s()
            Cs = C.s()
            P0s = P0.s
            T0s = T0.s

            for granularity in range(dim):
                print(f"   GRANULARITY {granularity}")

                self._test_affect((A,), (42,), discretization, granularity)
                self._test_affect((A, B, C), (1, 2, 3), discretization, granularity)

                if __ENABLE_LONG_TESTS__:
                    expr = Assignment(As, 1 + P0s)

                    def compute_outputs(ifields, iparams, dfields, ovar, i):
                        return np.full(
                            fill_value=1 + iparams["P0"],
                            shape=ovar.resolution,
                            dtype=ovar.dtype,
                        )

                    self._test_expr(
                        expr,
                        compute_outputs,
                        variables={A: discretization},
                        method={ComputeGranularity: granularity},
                    )
                    P0.value = 4.0

                    def compute_outputs(ifields, iparams, dfields, ovar, i):
                        return np.full(
                            fill_value=5, shape=ovar.resolution, dtype=ovar.dtype
                        )

                    self._test_expr(
                        expr,
                        compute_outputs,
                        variables={A: discretization},
                        method={ComputeGranularity: granularity},
                    )

                    expr = Assignment(Bs, np.sum(T0s[1::2]))

                    def compute_outputs(ifields, iparams, dfields, ovar, i):
                        return np.full(
                            fill_value=1 + 3 + 5 + 7 + 9 + 11,
                            shape=ovar.resolution,
                            dtype=ovar.dtype,
                        )

                    self._test_expr(
                        expr,
                        compute_outputs,
                        variables={B: discretization},
                        method={ComputeGranularity: granularity},
                    )

                    def compute_outputs(ifields, iparams, dfields, ovar, i):
                        return ifields[C[0]]

                    self._test_expr(
                        Assignment(As, C.s[0]()),
                        compute_outputs,
                        variables={A: discretization, C: discretization},
                        method={ComputeGranularity: granularity},
                    )

                def compute_outputs(ifields, iparams, dfields, ovar, i):
                    return 2 * ifields[C[0]] + 8

                self._test_expr(
                    Assignment(As, 2 * C.s[0]() + 8),
                    compute_outputs,
                    variables={A: discretization, C: discretization},
                    method={ComputeGranularity: granularity},
                )

                if dim == 3:

                    def compute_outputs(ifields, iparams, dfields, ovar, i):
                        return ifields[C[0]] * ifields[C[1]] * ifields[C[2]]

                    self._test_expr(
                        Assignment(As, C.s[0]() * C.s[1]() * C.s[2]()),
                        compute_outputs,
                        variables={A: discretization, C: discretization},
                        method={ComputeGranularity: granularity},
                    )

                    x0 = domain.frame.coords[0]

                    def compute_outputs(ifields, iparams, dfields, ovar, i):
                        return (
                            2 * ifields[A]
                            - 3 * ifields[C[0]] * ifields[C[1]] * ifields[C[2]]
                        )

                    self._test_expr(
                        Assignment(As, 2 * As - 3 * C.s[0]() * C.s[1]() * C.s[2]()),
                        compute_outputs,
                        variables={A: discretization, C: discretization},
                        method={ComputeGranularity: granularity},
                    )

                    def compute_outputs(ifields, iparams, dfields, ovar, i):
                        return (
                            2
                            * np.cos(ifields[C[0]])
                            * np.sin(ifields[C[1]])
                            * np.tan(ifields[C[2]])
                        ).astype(ovar.dtype)

                    self._test_expr(
                        Assignment(
                            As,
                            2 * sm.cos(C.s[0]()) * sm.sin(C.s[1]()) * sm.tan(C.s[2]()),
                        ),
                        compute_outputs,
                        variables={A: discretization, C: discretization},
                        method={ComputeGranularity: granularity},
                    )

    def _test_expr(
        self,
        exprs,
        compute_outputs,
        variables,
        method,
        apply_kwds=None,
        no_ref_view=False,
        dt=None,
    ):
        exprs = to_tuple(exprs)
        print("    CustomExpr: {}".format(" || ".join(str(e) for e in exprs)))

        assert ComputeGranularity in method
        base_kwds = dict(
            name="array_affect", exprs=exprs, variables=variables, method=method, dt=dt
        )
        apply_kwds = first_not_None(apply_kwds, {})

        for impl, op in self.iter_implementations(CustomSymbolicOperator, base_kwds):
            problem = op.build()

            for field, dfield in problem.iter_output_discrete_fields():
                dfield.initialize(
                    self.__field_init,
                    dtype=dfield.dtype,
                    pollute=True,
                    only_finite=False,
                )
            for field, dfield in problem.iter_input_discrete_fields():
                dfield.initialize(self.__field_init, dtype=dfield.dtype)

            refin = {"f": {}, "p": {}, "dfields": {}}
            in_names = []
            for field, ifield in problem.iter_input_discrete_fields(as_scalars=True):
                refin["f"][field] = ifield.sdata.get().handle.copy()
                refin["dfields"][field] = ifield
                in_names.append(field.name)
            for param in problem.input_params.keys():
                refin["p"][param.name] = param.value
                in_names.append(param.name)

            refout = {"f": {}, "p": {}, "dfields": {}}
            out_names = []
            for field, ofield in problem.iter_output_discrete_fields(as_scalars=True):
                refout["f"][field] = ()
                refout["dfields"][field] = ofield
                for i in range(ofield.nb_components):
                    res = compute_outputs(
                        refin["f"], refin["p"], refin["dfields"], ofield, i
                    ).copy()
                    check_instance(res, np.ndarray)
                    if not no_ref_view:
                        res = res[ofield.compute_slices]
                    refout["f"][field] += (res,)
                    out_names.append(field.name + f"::{i}")
            for param in problem.output_params.keys():
                refout["p"][param.name] = compute_outputs(
                    refin["f"], refin["p"], refin["dfields"], param, None
                )
                out_names.append(param.name)

            problem.apply(**apply_kwds)

            out = {"f": {}, "p": {}, "dfields": {}}
            for field, ofield in problem.iter_output_discrete_fields(as_scalars=True):
                out["f"][field] = ()
                out["dfields"][field] = ofield
                for i in range(ofield.nb_components):
                    res = ofield.data[i].get()
                    res = res.handle[ofield.compute_slices].copy()
                    out["f"][field] += (res,)
            for pname, param in problem.output_params.items():
                out["p"][pname] = param.value

            for field in refin["f"]:
                view = refin["dfields"][field].compute_slices
                refin["f"][field] = refin["f"][field][view]

            view = dfield.compute_slices

            def flatten(x):
                out = ()
                for _ in x["f"].values():
                    if not isinstance(_, tuple):
                        _ = (_,)
                    for __ in _:
                        out += (__,)
                for _ in x["p"].values():
                    out += (np.asarray(_),)
                return out

            self._check_output(
                impl,
                op,
                in_names,
                flatten(refin),
                out_names,
                flatten(refout),
                flatten(out),
            )

    def _test_affect(self, fields, rhs, discretization, granularity):
        exprs = ()
        variables = {}
        for f, c in zip(fields, rhs):
            variables[f] = discretization
            for i in range(f.nb_components):
                e = Assignment(f.s[i](), c)
                exprs += (e,)

        print("    CustomExpr: {}".format(" || ".join(str(e) for e in exprs)))

        method = {ComputeGranularity: granularity}
        base_kwds = dict(
            name="custom_affect", exprs=exprs, variables=variables, method=method
        )
        for impl, op in self.iter_implementations(CustomSymbolicOperator, base_kwds):

            problem = op.build()

            refin = ()
            in_names = []
            for ofield, dfield in problem.iter_output_discrete_fields():
                dfield.initialize(self.__field_init, dtype=dfield.dtype)
                view = dfield.compute_slices
                for i in range(dfield.nb_components):
                    refin += (dfield.data[i].get().handle[view],)
                    in_names.append(ofield.name + f"::{i}")

            problem.apply()

            out = ()
            refout = ()
            out_names = []
            for f, c in zip(fields, rhs):
                dfield = problem.get_output_discrete_field(f)
                view = dfield.compute_slices
                for i in range(dfield.nb_components):
                    res = dfield.data[i].get().handle[view]
                    try:
                        ref = np.full_like(res, fill_value=c)
                    except:
                        ref = (
                            problem.get_input_discrete_field(c).sdata.get().handle[view]
                        )
                    out += (res,)
                    refout += (ref,)
                    out_names.append(f.name + f"::{i}")

            self._check_output(impl, op, in_names, refin, out_names, refout, out)

    def _test_stencil(self, dim, _size_min=None, _size_max=None):
        enable_extra_tests = self.enable_extra_tests
        assert dim >= 1
        print("TEST STENCIL")
        print(f" DIM {dim}")

        size_min = first_not_None(_size_min, self.size_min)
        size_max = first_not_None(_size_max, self.size_max)
        size_min0 = first_not_None(_size_min, self.size_min0)
        size_max0 = first_not_None(_size_max, self.size_max0)

        domain = Box(length=(1.0,) * dim)
        frame = domain.frame

        discretization = tuple(
            np.random.randint(low=size_min, high=size_max + 1, size=dim - 1).tolist()
        )
        discretization0 = tuple(
            np.random.randint(low=size_min0, high=size_max0 + 1, size=1).tolist()
        )
        discretization = discretization + discretization0

        print(f" DISCRETIZATION {discretization}")
        A = Field(domain=domain, name="A", dtype=np.float32, nb_components=1)
        B = Field(domain=domain, name="B", dtype=np.float32, nb_components=2)
        C = Field(domain=domain, name="C", dtype=np.float64, nb_components=3)

        P0 = ScalarParameter("P0", dtype=np.float32, initial_value=3.14)
        T0 = TensorParameter("T", shape=(3,), dtype=np.int32, initial_value=[4, 3, 2])

        As = A.s()
        Bs = B.s()
        Cs = C.s()
        P0s = P0.s
        T0s = T0.s

        x0 = frame.coords[0]

        csg = CenteredStencilGenerator()
        csg.configure(dtype=MPQ, dim=1)

        if __ENABLE_LONG_TESTS__:
            orders = (2, 4)
        else:
            orders = (4,)

        for order in orders:
            print(f"  ORDER {order}")
            for granularity in range(dim):
                print(f"   GRANULARITY {granularity}")

                if __ENABLE_LONG_TESTS__:
                    expr = Assignment(As, B.s[0]().diff(x0, x0))
                    stencil = csg.generate_exact_stencil(derivative=2, order=order)

                    def compute_outputs(fields, iparams, ifields, ovar, i):
                        return stencil.apply(
                            fields[B[0]],
                            symbols={stencil.dx: ovar.space_step[-1]},
                            axis=-1,
                        )[ifields[B[0]].compute_slices]

                    self._test_expr(
                        expr,
                        compute_outputs,
                        variables={A: discretization, B: discretization},
                        method={
                            ComputeGranularity: granularity,
                            SpaceDiscretization: order,
                        },
                    )

                    expr = Assignment(As, 1 + 2 * B.s[0]().diff(x0))
                    stencil = csg.generate_exact_stencil(derivative=1, order=order)

                    def compute_outputs(fields, iparams, ifields, ovar, i):
                        res = stencil.apply(
                            fields[B[0]],
                            symbols={stencil.dx: ovar.space_step[-1]},
                            axis=-1,
                        )[ifields[B[0]].compute_slices]
                        return 1 + 2 * res

                    self._test_expr(
                        expr,
                        compute_outputs,
                        variables={A: discretization, B: discretization},
                        method={
                            ComputeGranularity: granularity,
                            SpaceDiscretization: order,
                        },
                    )

                    expr = Assignment(
                        Bs[0](), (1 + np.dot([3, -2], Bs.diff(x0))) * As.diff(x0)
                    )

                    stencil = csg.generate_exact_stencil(derivative=1, order=order)

                    def compute_outputs(fields, iparams, ifields, ovar, i):
                        if i == 0:
                            res0 = stencil.apply(
                                fields[B[0]],
                                symbols={stencil.dx: ovar.space_step[-1]},
                                axis=-1,
                            )
                            res1 = stencil.apply(
                                fields[B[1]],
                                symbols={stencil.dx: ovar.space_step[-1]},
                                axis=-1,
                            )
                            res2 = stencil.apply(
                                fields[A],
                                symbols={stencil.dx: ovar.space_step[-1]},
                                axis=-1,
                            )
                            return (1 + (3 * res0) - (2 * res1)) * res2
                        else:
                            return ovar.data[i].get().handle

                    self._test_expr(
                        expr,
                        compute_outputs,
                        variables={A: discretization, B: discretization},
                        method={
                            ComputeGranularity: granularity,
                            SpaceDiscretization: order,
                        },
                    )

                expr0 = Assignment(
                    B.s[0](), (1 + np.dot([3, -2], Bs.diff(x0))) * As.diff(x0)
                )
                expr1 = Assignment(
                    B.s[1](), (1 - np.dot([3, -2], Bs.diff(x0))) * As.diff(x0)
                )
                expr2 = Assignment(
                    A.s[0](),
                    0.27 + 1.57 * sm.cos(Bs[0].diff(x0)) * sm.sin(Bs[1].diff(x0)),
                )
                expr = (expr0, expr1, expr2)
                stencil = csg.generate_exact_stencil(derivative=1, order=order)

                def compute_outputs(fields, iparams, ifields, ovar, i):
                    res0 = stencil.apply(
                        fields[B[0]], symbols={stencil.dx: ovar.space_step[-1]}, axis=-1
                    )
                    res1 = stencil.apply(
                        fields[B[1]], symbols={stencil.dx: ovar.space_step[-1]}, axis=-1
                    )
                    res2 = stencil.apply(
                        fields[A], symbols={stencil.dx: ovar.space_step[-1]}, axis=-1
                    )
                    if ovar._dfield._field is B[0]:
                        return (1 + (3 * res0) - (2 * res1)) * res2
                    elif ovar._dfield._field is B[1]:
                        return (1 - (3 * res0) + (2 * res1)) * res2
                    else:
                        return 0.27 + 1.57 * np.cos(res0) * np.sin(res1)

                self._test_expr(
                    expr,
                    compute_outputs,
                    variables={A: discretization, B: discretization},
                    method={
                        ComputeGranularity: granularity,
                        SpaceDiscretization: order,
                    },
                )

                expr = Assignment(
                    B.s[0](),
                    T0s[2]
                    * ((4 + P0s) * As * B.s[0]().diff(x0) + T0s[1] * Bs[1]).diff(x0)
                    + P0s * T0s[0],
                )
                stencil0 = csg.generate_exact_stencil(derivative=1, order=order)
                stencil1 = csg.generate_exact_stencil(derivative=2, order=order)

                def compute_outputs(fields, iparams, ifields, ovar, i):
                    B0 = fields[B[0]]
                    B1 = fields[B[1]]
                    A0 = fields[A]
                    dB0 = stencil0.apply(
                        B0, symbols={stencil0.dx: ovar.space_step[-1]}, axis=-1
                    )
                    dB1 = stencil0.apply(
                        B1, symbols={stencil0.dx: ovar.space_step[-1]}, axis=-1
                    )
                    dA0 = stencil0.apply(
                        A0, symbols={stencil0.dx: ovar.space_step[-1]}, axis=-1
                    )
                    d2B0 = stencil1.apply(
                        B0, symbols={stencil1.dx: ovar.space_step[-1]}, axis=-1
                    )
                    d2B1 = stencil1.apply(
                        B1, symbols={stencil1.dx: ovar.space_step[-1]}, axis=-1
                    )
                    d2A0 = stencil1.apply(
                        A0, symbols={stencil1.dx: ovar.space_step[-1]}, axis=-1
                    )
                    if ovar.dfield._field is B[0]:
                        return P0[0] * T0[0] + T0[2] * (
                            (4 + P0[0]) * (A0 * d2B0 + dA0 * dB0) + T0[1] * dB1
                        )
                    else:
                        return ovar.dfield.data[i].get().handle

                self._test_expr(
                    expr,
                    compute_outputs,
                    variables={A: discretization, B: discretization},
                    method={
                        ComputeGranularity: granularity,
                        SpaceDiscretization: order,
                    },
                )

    def _test_time_integrator(self, dim, _size_min=None, _size_max=None):
        enable_extra_tests = self.enable_extra_tests
        assert dim >= 1
        print("TEST INTEGRATOR")
        print(f" DIM {dim}")

        size_min = first_not_None(_size_min, self.size_min)
        size_max = first_not_None(_size_max, self.size_max)
        size_min0 = first_not_None(_size_min, self.size_min0)
        size_max0 = first_not_None(_size_max, self.size_max0)

        domain = Box(length=(1.0,) * dim)
        frame = domain.frame

        discretization = tuple(
            np.random.randint(low=size_min, high=size_max + 1, size=dim - 1).tolist()
        )
        discretization0 = tuple(
            np.random.randint(low=size_min0, high=size_max0 + 1, size=1).tolist()
        )
        discretization = discretization + discretization0

        print(f" DISCRETIZATION {discretization}")
        A = Field(domain=domain, name="A", dtype=np.float32, nb_components=1)
        B = Field(domain=domain, name="B", dtype=np.float32, nb_components=2)
        C = Field(domain=domain, name="C", dtype=np.float32, nb_components=3)

        P0 = ScalarParameter("P0", dtype=np.float32, initial_value=3.14)
        T0 = TensorParameter("T", shape=(3,), dtype=np.int32, initial_value=[4, 3, 2])

        As = A.s()
        Bs = B.s()
        Cs = C.s()
        P0s = P0.s
        T0s = T0.s

        T = ScalarParameter(name=frame.time, dtype=np.float32, initial_value=0.0)
        DT = ScalarParameter(name=dtime_symbol, dtype=np.float32, initial_value=0.1)

        t = T.s
        x0 = frame.coords[0]

        csg = CenteredStencilGenerator()
        csg.configure(dtype=MPQ, dim=1)

        order = 4
        granularity = 0
        print(f"  GRANULARITY {granularity}")
        print(f"  ORDER {order}")

        D1 = csg.generate_exact_stencil(derivative=1, order=order)
        D2 = csg.generate_exact_stencil(derivative=2, order=order)
        D3 = csg.generate_exact_stencil(derivative=3, order=order)

        if __ENABLE_LONG_TESTS__:
            integrators = [Euler, RK2, RK4, RK4_38]
        else:
            integrators = (RK2,)

        for integrator in integrators:
            print(f"   INTEGRATOR {integrator}")

            if __ENABLE_LONG_TESTS__:
                expr = Assignment(As.diff(t), 0)

                def compute_outputs(fields, iparams, ifields, ovar, i):
                    A0 = fields[A]
                    if (ovar.dfield._field is A) and (i == 0):
                        return fields[A].copy()
                    else:
                        return ovar.dfield.data[i].get().handle

                self._test_expr(
                    expr,
                    compute_outputs,
                    variables={A: discretization, B: discretization, C: discretization},
                    method={
                        ComputeGranularity: granularity,
                        SpaceDiscretization: order,
                        TimeIntegrator: integrator,
                    },
                    dt=DT,
                )

                expr = Assignment(As.diff(t), 1)

                def compute_outputs(fields, iparams, ifields, ovar, i):
                    A0 = fields[A]
                    if (ovar.dfield._field is A) and (i == 0):
                        Xin = {"a": fields[A]}
                        Xout = {"a": np.empty_like(Xin["a"][ifields[A].compute_slices])}

                        def rhs(out, X, t, **kwds):
                            out["a"][...] = 1

                        integrator(Xin=Xin, Xout=Xout, RHS=rhs, t=0.0, dt=DT())
                        return Xout["a"]
                    else:
                        return ovar.dfield.data[i].get().handle

                self._test_expr(
                    expr,
                    compute_outputs,
                    variables={A: discretization, B: discretization, C: discretization},
                    method={
                        ComputeGranularity: granularity,
                        SpaceDiscretization: order,
                        TimeIntegrator: integrator,
                    },
                    dt=DT,
                )

                expr = Assignment(As.diff(t), np.dot([2, -3], Bs))

                def compute_outputs(fields, iparams, ifields, ovar, i):
                    A0 = fields[A]
                    B0 = fields[B[0]]
                    B1 = fields[B[1]]
                    if (ovar.dfield._field is A) and (i == 0):
                        Xin = {"a": A0}
                        Xout = {"a": np.empty_like(Xin["a"][ifields[A].compute_slices])}

                        def rhs(out, X, t, **kwds):
                            out["a"][...] = 2 * B0 - 3 * B1

                        integrator(Xin=Xin, Xout=Xout, RHS=rhs, t=0.0, dt=DT())
                        return Xout["a"]
                    else:
                        return ovar.dfield.data[i].get().handle

                self._test_expr(
                    expr,
                    compute_outputs,
                    variables={A: discretization, B: discretization, C: discretization},
                    method={
                        ComputeGranularity: granularity,
                        SpaceDiscretization: order,
                        TimeIntegrator: integrator,
                    },
                    dt=DT,
                )

                expr = Assignment(As.diff(t), As * Bs[0] + Bs[1])

                def compute_outputs(fields, iparams, ifields, ovar, i):
                    A0 = fields[A]
                    B0 = fields[B[0]]
                    B1 = fields[B[1]]
                    if (ovar.dfield._field is A) and (i == 0):
                        Xin = {"a": A0}
                        Xout = {"a": np.empty_like(Xin["a"][ifields[A].compute_slices])}

                        def rhs(out, X, t, **kwds):
                            out["a"][...] = X["a"] * B0 + B1

                        integrator(Xin=Xin, Xout=Xout, RHS=rhs, t=0.0, dt=DT())
                        return Xout["a"]
                    else:
                        return ovar.dfield.data[i].get().handle

                self._test_expr(
                    expr,
                    compute_outputs,
                    variables={A: discretization, B: discretization, C: discretization},
                    method={
                        ComputeGranularity: granularity,
                        SpaceDiscretization: order,
                        TimeIntegrator: integrator,
                    },
                    dt=DT,
                )

                expr0 = Assignment(Bs[0].diff(t), 1 + Bs[1])
                expr1 = Assignment(Bs[1].diff(t), 1 - Bs[0])
                expr = (expr0, expr1)

                def compute_outputs(fields, iparams, ifields, ovar, i):
                    B0 = fields[B[0]]
                    B1 = fields[B[1]]
                    if ovar.dfield._field in B.fields:
                        Xin = {"b0": B0, "b1": B1}
                        Xout = {
                            "b0": np.empty_like(B0[ifields[B[0]].compute_slices]),
                            "b1": np.empty_like(B1[ifields[B[1]].compute_slices]),
                        }

                        def rhs(out, X, t, **kwds):
                            out["b0"][...] = 1 + X["b1"]
                            out["b1"][...] = 1 - X["b0"]

                        integrator(Xin=Xin, Xout=Xout, RHS=rhs, t=0.0, dt=DT())
                        varname = "b0" if (ovar.dfield._field is B[0]) else "b1"
                        return Xout[varname]
                    else:
                        return ovar.dfield.data[i].get().handle

                self._test_expr(
                    expr,
                    compute_outputs,
                    variables={A: discretization, B: discretization, C: discretization},
                    method={
                        ComputeGranularity: granularity,
                        SpaceDiscretization: order,
                        TimeIntegrator: integrator,
                    },
                    dt=DT,
                )

                expr = Assignment(As.diff(t), Bs[0].diff(x0))

                def compute_outputs(fields, iparams, ifields, ovar, i):
                    A0 = fields[A]
                    B0 = fields[B[0]]
                    dB0 = D1.apply(B0, symbols={D1.dx: ovar.space_step[-1]}, axis=-1)
                    if ovar.dfield._field is A:
                        Xin = {"a": A0}
                        Xout = {"a": np.empty_like(Xin["a"][ifields[A].compute_slices])}

                        def rhs(out, X, t, **kwds):
                            out["a"][...] = dB0[ifields[B[0]].compute_slices]

                        integrator(Xin=Xin, Xout=Xout, RHS=rhs, t=0.0, dt=DT())
                        return Xout["a"]
                    else:
                        return ovar.dfield.data[i].get().handle

                self._test_expr(
                    expr,
                    compute_outputs,
                    variables={A: discretization, B: discretization},
                    method={
                        ComputeGranularity: granularity,
                        SpaceDiscretization: order,
                        TimeIntegrator: integrator,
                    },
                    dt=DT,
                )

                expr = Assignment(As.diff(t), As.diff(x0))

                def compute_outputs(fields, iparams, ifields, ovar, i):
                    A0 = fields[A]
                    if ovar.dfield._field is A:
                        Xin = {"a": A0}
                        Xout = {"a": np.empty_like(Xin["a"][ifields[A].compute_slices])}
                        views = {"a": ifields[A].compute_slices}

                        def rhs(out, X, t, **kwds):
                            dA0 = D1.apply(
                                X["a"], symbols={D1.dx: ovar.space_step[-1]}, axis=-1
                            )
                            out["a"][...] = dA0

                        integrator(
                            Xin=Xin, Xout=Xout, views=views, RHS=rhs, t=0.0, dt=DT()
                        )
                        return Xout["a"]
                    else:
                        return ovar.dfield.data[i].get().handle

                self._test_expr(
                    expr,
                    compute_outputs,
                    variables={A: discretization},
                    method={
                        ComputeGranularity: granularity,
                        SpaceDiscretization: order,
                        TimeIntegrator: integrator,
                    },
                    dt=DT,
                    no_ref_view=True,
                )

                expr = Assignment(
                    As.diff(t), P0s * Bs[0] * As.diff(x0) + Bs[1].diff(x0, x0) * As
                )

                def compute_outputs(fields, iparams, ifields, ovar, i):
                    A0 = fields[A]
                    B0 = fields[B[0]]
                    B1 = fields[B[1]]
                    d2B1 = D2.apply(B1, symbols={D2.dx: ovar.space_step[-1]}, axis=-1)
                    assert order % 2 == 0
                    bg = order // 2
                    v = (Ellipsis, slice(bg, -bg))
                    if ovar.dfield._field is A:
                        Xin = {"a": A0}
                        Xout = {"a": np.empty_like(Xin["a"][ifields[A].compute_slices])}
                        views = {"a": ifields[A].compute_slices}

                        def rhs(out, X, t, **kwds):
                            A0 = X["a"]
                            dA0 = D1.apply(
                                A0, symbols={D1.dx: ovar.space_step[-1]}, axis=-1
                            )
                            out["a"][v] = P0() * B0 * dA0[v] + d2B1[v] * A0[v]

                        integrator(
                            Xin=Xin, Xout=Xout, views=views, RHS=rhs, t=0.0, dt=DT()
                        )
                        return Xout["a"]
                    else:
                        return ovar.dfield.data[i].get().handle

                self._test_expr(
                    expr,
                    compute_outputs,
                    variables={A: discretization, B: discretization},
                    method={
                        ComputeGranularity: granularity,
                        SpaceDiscretization: order,
                        TimeIntegrator: integrator,
                    },
                    dt=DT,
                    no_ref_view=True,
                )

            expr0 = Assignment(Bs[0].diff(t), 1 - As * Bs[1].diff(x0))
            expr1 = Assignment(Bs[1].diff(t), 1 + As * Bs[0].diff(x0))
            expr = (expr0, expr1)

            def compute_outputs(fields, iparams, ifields, ovar, i):
                A0 = fields[A]
                B0 = fields[B[0]]
                B1 = fields[B[1]]
                b0_view = ifields[B[0]].compute_slices
                b1_view = ifields[B[1]].compute_slices
                assert order % 2 == 0
                bg = order // 2
                v = (Ellipsis, slice(bg, -bg))
                if ovar.dfield._field in B.fields:
                    Xin = {"b0": B0, "b1": B1}
                    Xout = {
                        "b0": np.empty_like(Xin["b0"][ifields[B[0]].compute_slices]),
                        "b1": np.empty_like(Xin["b1"][ifields[B[1]].compute_slices]),
                    }
                    views = {"b0": b0_view, "b1": b1_view}

                    def rhs(out, X, t, step, steps, **kwds):
                        B0 = X["b0"]
                        B1 = X["b1"]
                        dB0 = D1.apply(
                            B0, symbols={D1.dx: ovar.space_step[-1]}, axis=-1
                        )
                        dB1 = D1.apply(
                            B1, symbols={D1.dx: ovar.space_step[-1]}, axis=-1
                        )
                        dB0[v] *= A0
                        dB1[v] *= A0
                        out["b0"] = 1 - dB1
                        out["b1"] = 1 + dB0

                    integrator(Xin=Xin, Xout=Xout, views=views, RHS=rhs, t=0.0, dt=DT())
                    varname = "b0" if (ovar.dfield._field is B[0]) else "b1"
                    return Xout[varname]
                else:
                    return ovar.dfield.data[i].get().handle

            self._test_expr(
                expr,
                compute_outputs,
                variables={A: discretization, B: discretization},
                method={
                    ComputeGranularity: granularity,
                    SpaceDiscretization: order,
                    TimeIntegrator: integrator,
                },
                dt=DT,
                no_ref_view=True,
            )

    def test_simple_1d(self):
        self._test_simple(dim=1)

    def test_simple_2d(self):
        self._test_simple(dim=2)

    def test_simple_3d(self):
        self._test_simple(dim=3)

    def test_stencil_1d(self):
        self._test_stencil(dim=1)

    def test_stencil_2d(self):
        self._test_stencil(dim=2)

    def test_stencil_3d(self):
        self._test_stencil(dim=3)

    def test_time_integrator_1d(self):
        self._test_time_integrator(dim=1)

    def test_time_integrator_2d(self):
        self._test_time_integrator(dim=2)

    def test_time_integrator_3d(self):
        self._test_time_integrator(dim=3)

    def perform_tests(self):
        self.test_simple_1d()
        self.test_simple_2d()
        self.test_simple_3d()

        self.test_stencil_1d()
        self.test_stencil_2d()
        self.test_stencil_3d()

        self.test_time_integrator_1d()
        self.test_time_integrator_2d()
        self.test_time_integrator_3d()


if __name__ == "__main__":
    TestCustomSymbolic.setup_class(enable_extra_tests=False, enable_debug_mode=False)

    enable_pretty_printing()

    test = TestCustomSymbolic()
    test.perform_tests()

    TestCustomSymbolic.teardown_class()
