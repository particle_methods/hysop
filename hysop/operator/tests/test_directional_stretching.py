# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys
import sympy as sm

from hysop.testsenv import __ENABLE_LONG_TESTS__, iter_clenv
from hysop.tools.numpywrappers import npw
from hysop.tools.contexts import printoptions
from hysop.tools.htypes import first_not_None, to_tuple
from hysop.tools.numerics import is_integer, is_fp
from hysop.tools.io_utils import IO
from hysop.tools.sympy_utils import enable_pretty_printing
from hysop.operator.directional.stretching_dir import (
    DirectionalStretching,
    StaticDirectionalStretching,
)
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.testsenv import __HAS_OPENCL_BACKEND__

from hysop import Field, Box, Simulation
from hysop.methods import Remesh, TimeIntegrator
from hysop.constants import (
    Implementation,
    DirectionLabels,
    ComputeGranularity,
    SpaceDiscretization,
    StretchingFormulation,
    HYSOP_REAL,
)
from hysop.numerics.splitting.strang import StrangSplitting, StrangOrder
from hysop.numerics.stencil.stencil_generator import (
    StencilGenerator,
    CenteredStencilGenerator,
    MPQ,
)
from hysop.numerics.odesolvers.runge_kutta import (
    TimeIntegrator,
    Euler,
    RK2,
    RK3,
    RK4,
    RK4_38,
)


class TestDirectionalStretchingOperator:

    @classmethod
    def setup_class(
        cls, enable_extra_tests=__ENABLE_LONG_TESTS__, enable_debug_mode=False
    ):

        IO.set_default_path("/tmp/hysop_tests/test_directional_stretching")

        if enable_debug_mode:
            cls.size_min = 6
            cls.size_max = 7
        else:
            cls.size_min = 11
            cls.size_max = 23

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_debug_mode = enable_debug_mode

    @classmethod
    def teardown_class(cls):
        print()

    def _test(self, formulation, is_inplace, As=None, size_min=None, size_max=None):

        dim = 3
        assert (formulation is StretchingFormulation.MIXED_GRAD_UW) ^ (As is None)
        As = first_not_None(As, (None,))

        # periodic boundaries removes one computational point
        # so we add one here.
        size_min = first_not_None(size_min, self.size_min) + 1
        size_max = first_not_None(size_max, self.size_max) + 1

        shape = tuple(
            npw.random.randint(low=size_min, high=size_max, size=dim).tolist()
        )

        if self.enable_extra_tests:
            flt_types = (npw.float32, npw.float64)
            time_integrators = (
                Euler,
                RK2,
            )
            orders = (2, 4, 6)
        else:
            flt_types = (HYSOP_REAL,)
            time_integrators = (RK2,)
            orders = (4,)

        domain = Box(length=(2 * npw.pi,) * dim)
        for dtype in flt_types:
            Vin = Field(domain=domain, name="Vin", dtype=dtype, nb_components=dim)
            Win = Field(domain=domain, name="Win", dtype=dtype, nb_components=dim)
            Wout = Field(domain=domain, name="Wout", dtype=dtype, nb_components=dim)
            C = npw.random.rand(dim).astype(dtype)
            for A in As:
                for order in orders:
                    for time_integrator in time_integrators:
                        self._test_one(
                            time_integrator=time_integrator,
                            order=order,
                            shape=shape,
                            dim=dim,
                            dtype=dtype,
                            is_inplace=is_inplace,
                            domain=domain,
                            Vin=Vin,
                            Win=Win,
                            Wout=Wout,
                            C=C,
                            A=A,
                            formulation=formulation,
                        )
                        print()

    @staticmethod
    def __random_init(data, coords, component):
        shape = data.shape
        dtype = data.dtype
        if is_integer(dtype):
            data[...] = npw.random.random_integers(low=0, high=255, size=shape)
        elif is_fp(dtype):
            data[...] = npw.random.random(size=shape)
        else:
            msg = f"Unknown dtype {dtype}."
            raise NotImplementedError(msg)

    def _test_one(
        self,
        time_integrator,
        order,
        shape,
        dim,
        dtype,
        is_inplace,
        domain,
        Vin,
        Win,
        Wout,
        C,
        A,
        formulation,
    ):

        print(
            "\nTesting DirectionalStretching::{}_{}_FCD{}: is_inplace={} dtype={} shape={} C={} A={}".format(
                formulation,
                time_integrator.name(),
                order,
                is_inplace,
                dtype.__name__,
                shape,
                C,
                A,
            ),
            end=" ",
        )

        dt = ScalarParameter("dt0", initial_value=1.0, const=False, dtype=dtype)

        vin = Vin
        win = Win
        if is_inplace:
            wout = Win
            variables = {Win: shape, Vin: shape}
        else:
            wout = Wout
            variables = {Win: shape, Wout: shape, Vin: shape}
            msg = "Out of place stretching has not been implemented yet."
            raise NotImplementedError(msg)

        implementations = (Implementation.PYTHON,)
        if __HAS_OPENCL_BACKEND__:
            implementations = (Implementation.PYTHON, Implementation.OPENCL)
        # currently we have to different implementations:
        #  DirectionalStretching and StaticDirectionalStretching

        method = {TimeIntegrator: time_integrator, SpaceDiscretization: order}

        def iter_impl(impl):
            base_kwds = dict(
                formulation=formulation,
                velocity=vin,
                vorticity=win,
                variables=variables,
                C=C,
                A=A,
                implementation=impl,
                dt=dt,
                method=method,
                name=f"stretching_{str(impl).lower()}",
            )
            if impl is Implementation.OPENCL:
                for cl_env in iter_clenv():
                    msg = "platform {}, device {}".format(
                        cl_env.platform.name.strip(), cl_env.device.name.strip()
                    )
                    ds = DirectionalStretching(**base_kwds)
                    split = StrangSplitting(
                        splitting_dim=dim,
                        extra_kwds=dict(cl_env=cl_env),
                        order=StrangOrder.STRANG_SECOND_ORDER,
                    )
                    yield msg, ds, split
            elif impl is Implementation.PYTHON:
                ds = StaticDirectionalStretching(**base_kwds)
                split = StrangSplitting(
                    splitting_dim=dim, order=StrangOrder.STRANG_SECOND_ORDER
                )
                yield "", ds, split
            else:
                msg = f"Unknown implementation to test {impl}."
                raise NotImplementedError(msg)

        # Compare to other implementations
        (V_input, W_input, W_reference) = (None, None, None)
        for impl in implementations:
            print(f"\n >Implementation {impl}: ", end=" ")
            for sop, op, split in iter_impl(impl):
                if sop != "":
                    print(f"\n   *{sop}: ", end=" ")
                split.push_operators(op)
                if not is_inplace:
                    split.push_copy(dst=win, src=wout)
                split = split.build()

                dvin = split.get_input_discrete_field(vin)
                dwin = split.get_input_discrete_field(win)
                dwout = split.get_output_discrete_field(wout)

                sys.stdout.write(".")
                sys.stdout.flush()
                try:
                    if W_input is None:
                        dwin.initialize(self.__random_init)
                        dvin.initialize(self.__random_init)

                        V_input = tuple(
                            df.sdata.get()[df.compute_slices].handle.copy()
                            for df in dvin
                        )
                        W_input = tuple(
                            df.sdata.get()[df.compute_slices].handle.copy()
                            for df in dwin
                        )

                        V0 = dvin.integrate()
                        W0 = dwin.integrate()
                        if not npw.all(npw.isfinite(V0)):
                            msg = f"V0 integral is not finite. Got {V0}."
                            raise RuntimeError(msg)
                        if not npw.all(npw.isfinite(W0)):
                            msg = f"W0 integral is not finite. Got {W0}."
                            raise RuntimeError(msg)

                        W_reference = self._compute_reference(
                            dvin=dvin,
                            dwin=dwin,
                            dt=dt(),
                            dim=dim,
                            time_integrator=time_integrator,
                            order=order,
                            C=C,
                            A=A,
                            formulation=formulation,
                        )

                    dwout.initialize(self.__random_init)
                    dwin.initialize(W_input, only_finite=False)
                    dvin.initialize(V_input, only_finite=False)

                    split.apply(dt=dt)

                    W_output = tuple(
                        df.sdata.get()[df.compute_slices].handle.copy() for df in dwout
                    )

                    V1 = dvin.integrate()
                    W1 = dwin.integrate()
                    assert npw.all(V0 == V1), V1 - V0
                    if not is_inplace:
                        assert npw.all(W0 == W1), W1 - W0

                    for i in range(dwout.nb_components):
                        if npw.may_share_memory(W_reference[i], W_output[i]):
                            msg = "W_output and W_reference arrays may share the same memory."
                            raise RuntimeError(msg)
                        if W_reference[i].dtype != W_output[i].dtype:
                            msg = (
                                "W_output dtype {} differs from the W_reference one {}."
                            )
                            msg = msg.format(W_output[i].dtype, W_reference[i].dtype)
                            raise RuntimeError(msg)
                        if W_reference[i].shape != W_output[i].shape:
                            msg = (
                                "W_output shape {} differs from the W_reference one {}."
                            )
                            msg = msg.format(W_output[i].shape, W_reference[i].shape)
                            raise RuntimeError(msg)
                        mask = npw.isfinite(W_output[i][dwout[i].compute_slices])
                        if not mask.all():
                            msg = (
                                f"\nFATAL ERROR: W_output is not finite on axis {i}.\n"
                            )
                            npw.fancy_print(
                                W_output[i],
                                replace_values={(lambda a: npw.isfinite(a)): "."},
                            )
                            raise RuntimeError(msg)
                        di = npw.abs(W_reference[i] - W_output[i])
                        max_di = npw.max(di)
                        neps = 500
                        eps = npw.finfo(dwout.dtype).eps
                        max_tol = neps * eps
                        if max_di > max_tol:
                            if self.enable_debug_mode:
                                print()
                                print("V INPUT")
                                print(V_input[i])
                                print("W INPUT")
                                print(W_input[i])
                                print("W_REFERENCE")
                                print(W_reference[i])
                                print("W_OUTPUT")
                                print(W_output[i])
                                print("ABS(REF - OUT)")
                                npw.fancy_print(
                                    di, replace_values={(lambda a: a < max_tol): "."}
                                )
                                print()
                            msg = (
                                f"W_OUTPUT did not match W_REFERENCE for component {i}"
                            )
                            msg += "\n > max computed distance was {} ({} eps).".format(
                                max_di, int(npw.ceil(max_di / eps))
                            )
                            msg += "\n > max tolerence was set to {} ({} eps).".format(
                                max_tol, neps
                            )
                            raise RuntimeError(msg)

                    W2 = dwout.integrate()
                    if not npw.all(npw.isfinite(W2)):
                        msg = f"W2 integral is not finite. Got {W2}."
                        raise RuntimeError(msg)
                except:
                    sys.stdout.write("\bx\n\n")
                    sys.stdout.flush()
                    raise

    def _compute_reference(
        self, dvin, dwin, dt, dim, time_integrator, order, C, A, formulation
    ):

        csg = CenteredStencilGenerator()
        csg.configure(dtype=MPQ, dim=1)
        D1 = csg.generate_exact_stencil(derivative=1, order=order)

        Vin = tuple(d.get().handle.copy() for d in dvin.data)
        Win = tuple(d.get().handle.copy() for d in dwin.data)

        directions = tuple(range(dim)) + tuple(range(dim))[::-1]
        dt_coeffs = (0.5,) * (2 * dim)

        wis = tuple(f"W{i}" for i in range(3))
        Xin = dict(zip(wis, Win))
        views = {f"W{i}": Wi.compute_slices for (i, Wi) in enumerate(dwin)}

        ghost_exchanger = dwin.build_ghost_exchanger(data=Win)

        for j, dt_coeff in zip(directions, dt_coeffs):
            ndir = dim - j - 1
            V = Vin
            D1_V = tuple(
                D1(a=Vi, out=None, axis=ndir, symbols={D1.dx: dvin.space_step[ndir]})
                for Vi in Vin
            )

            def rhs(out, X, **kwds):
                W = tuple(X[wi] for wi in wis)
                D1_W = tuple(
                    D1(
                        a=Wi,
                        out=None,
                        axis=ndir,
                        symbols={D1.dx: dwin.space_step[ndir]},
                    )
                    for Wi in W
                )
                dW_dt = tuple(out[wi] for wi in wis)
                if formulation is StretchingFormulation.CONSERVATIVE:
                    D1_VW = ()
                    for i, Vi in enumerate(V):
                        ghosts = [
                            0,
                        ] * 3
                        ghosts[2 - j] = dvin.ghosts[-1]
                        a = Vi * W[j]
                        d1_vw = D1(
                            a=a, axis=ndir, symbols={D1.dx: dwin.space_step[ndir]}
                        )
                        D1_VW += (d1_vw,)
                    assert A is None, A
                    for i in range(3):
                        ghosts = [
                            0,
                        ] * 3
                        # if (i==j):
                        # ghosts[2-j] = dwin[j].ghosts[j]
                        view = dwin[j].local_slices(ghosts)
                        dW_dt[i][...] = C[i] * D1_VW[i]
                elif formulation is StretchingFormulation.GRAD_UW:
                    assert A is None, A
                    for i in range(3):
                        V_view = dvin[i].compute_slices
                        dW_dt[i][...] = C[i] * D1_V[i][V_view] * W[j]
                elif formulation is StretchingFormulation.GRAD_UW_T:
                    assert A is None, A
                    for i in range(3):
                        if i == j:
                            V_view = dvin[i].compute_slices
                            dW_dt[j][...] = C[j] * sum(
                                D1_V[k][V_view] * W[k] for k in range(3)
                            )
                        else:
                            dW_dt[i][...] = 0
                elif formulation is StretchingFormulation.MIXED_GRAD_UW:
                    a = first_not_None(A, sm.Rational(1, 2))
                    a = float(a)
                    for i in range(3):
                        V_view = dvin[i].compute_slices
                        dW_dt[i][...] = a * D1_V[i][V_view] * W[j]
                        if i == j:
                            dW_dt[i][...] += (1.0 - a) * sum(
                                D1_V[k][V_view] * W[k] for k in range(3)
                            )
                        dW_dt[i][...] *= C[i]
                else:
                    msg = f"Unknown stretching formulation {formulation}."
                    raise NotImplementedError(msg)

            out = time_integrator(Xin=Xin, RHS=rhs, views=views, dt=(dt_coeff * dt))
            for i in range(3):
                Win[i][views[wis[i]]] = out[wis[i]]
            ghost_exchanger.exchange_ghosts()
        views = dvin.get_attributes("compute_slices")
        out = tuple(f[v] for (v, f) in zip(views, Win))
        return out

    def test_stretching_3D_inplace_conservative(self):
        self._test(formulation=StretchingFormulation.CONSERVATIVE, is_inplace=True)

    # def test_stretching_3D_inplace_gradU_W(self):
    #     self._test(formulation=StretchingFormulation.GRAD_UW, is_inplace=True)
    # def test_stretching_3D_inplace_gradU_T_W(self):
    #     self._test(formulation=StretchingFormulation.GRAD_UW_T, is_inplace=True)
    # def test_stretching_3D_inplace_mixed_gradient(self):
    #     As = (None, sm.Rational(1,3))
    #     self._test(formulation=StretchingFormulation.MIXED_GRAD_UW, is_inplace=True, As=As)

    # def test_stretching_3D_out_of_place_conservative(self):
    #     self._test(formulation=StretchingFormulation.CONSERVATIVE, is_inplace=False)
    # def test_stretching_3D_out_of_place_gradU_W(self):
    #     self._test(formulation=StretchingFormulation.GRAD_UW, is_inplace=False)
    # def test_stretching_3D_out_of_place_gradU_T_W(self):
    #     self._test(formulation=StretchingFormulation.GRAD_UW_T, is_inplace=False)
    # def test_stretching_3D_out_of_place_mixed_gradient(self):
    #     As = (None, sm.Rational(1,3))
    #     self._test(formulation=StretchingFormulation.MIXED_GRAD_UW, is_inplace=False, As=As)

    def perform_tests(self):
        # self.test_stretching_3D_inplace_gradU_W()
        # self.test_stretching_3D_inplace_gradU_T_W()
        # self.test_stretching_3D_inplace_mixed_gradient()
        self.test_stretching_3D_inplace_conservative()

        # self.test_stretching_3D_out_of_place_conservative()
        # self.test_stretching_3D_out_of_place_gradU_W()
        # self.test_stretching_3D_out_of_place_gradU_T_W()
        # self.test_stretching_3D_out_of_place_mixed_gradient()


if __name__ == "__main__":
    TestDirectionalStretchingOperator.setup_class(
        enable_extra_tests=False, enable_debug_mode=False
    )

    test = TestDirectionalStretchingOperator()

    enable_pretty_printing()

    with printoptions(
        threshold=10000,
        linewidth=240,
        nanstr="nan",
        infstr="inf",
        formatter={"float": lambda x: f"{x:>6.2f}"},
    ):
        test.perform_tests()

    TestDirectionalStretchingOperator.teardown_class()
