# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import itertools as it
import random

import numpy as np
import primefac
import scipy
import sympy as sm
from hysop import Box, Field
from hysop.constants import HYSOP_REAL, BoxBoundaryCondition, Implementation
from hysop.defaults import VelocityField, VorticityField
from hysop.operator.solenoidal_projection import Implementation, SolenoidalProjection
from hysop.testsenv import (
    __ENABLE_LONG_TESTS__,
    __HAS_OPENCL_BACKEND__,
    domain_boundary_iterator,
    iter_clenv,
    opencl_failed,
    test_context,
)
from hysop.tools.contexts import printoptions
from hysop.tools.io_utils import IO
from hysop.tools.numerics import is_fp, is_integer
from hysop.tools.numpywrappers import npw
from hysop.tools.spectral_utils import (
    make_multivariate_polynomial,
    make_multivariate_trigonometric_polynomial,
)
from hysop.tools.sympy_utils import round_expr, truncate_expr
from hysop.tools.htypes import check_instance, first_not_None, to_list, to_tuple


class TestSolenoidalProjectionOperator:

    @classmethod
    def setup_class(
        cls, enable_extra_tests=__ENABLE_LONG_TESTS__, enable_debug_mode=False
    ):

        IO.set_default_path("/tmp/hysop_tests/test_solenoidal_projection")

        cls.size_min = 8
        cls.size_max = 16

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_debug_mode = enable_debug_mode

        from hysop.tools.sympy_utils import enable_pretty_printing

        enable_pretty_printing()

    def build_analytic_solutions(
        self, polynomial, lboundaries, rboundaries, origin, end
    ):
        from hysop.symbolic.base import TensorBase
        from hysop.symbolic.field import curl, div, grad
        from hysop.symbolic.frame import SymbolicFrame

        assert len(lboundaries) == 4
        assert len(rboundaries) == 4

        frame = SymbolicFrame(dim=3)
        coords = frame.coords

        def gen_fn(nb_components, left_boundaries, right_boundaries):
            assert len(left_boundaries) == len(right_boundaries) == nb_components
            fns = ()
            for i in range(nb_components):
                if polynomial:
                    fn, y = make_multivariate_polynomial(
                        origin, end, left_boundaries[i], right_boundaries[i], 10, 2
                    )
                else:
                    fn, y = make_multivariate_trigonometric_polynomial(
                        origin, end, left_boundaries[i], right_boundaries[i], 2
                    )
                fn = fn.xreplace({yi: xi for (yi, xi) in zip(y, coords)})
                fns += (fn,)
            return npw.asarray(fns).view(TensorBase)

        analytic_solutions = {}
        analytic_functions = {}

        psis = gen_fn(
            nb_components=3,
            left_boundaries=lboundaries[:3],
            right_boundaries=rboundaries[:3],
        )
        phis = gen_fn(
            nb_components=1,
            left_boundaries=lboundaries[3:],
            right_boundaries=rboundaries[3:],
        )

        U0s = curl(psis, frame)
        U1s = grad(phis[0], frame)
        Us = U0s + U1s

        divU0s = to_tuple(div(U0s, frame))
        divU1s = to_tuple(div(U1s, frame))
        divUs = to_tuple(div(Us, frame))

        fU0s = tuple(sm.lambdify(coords, Ui) for Ui in U0s)
        fU1s = tuple(sm.lambdify(coords, Ui) for Ui in U1s)
        fUs = tuple(sm.lambdify(coords, Ui) for Ui in Us)
        fdivUs = tuple(sm.lambdify(coords, Ui) for Ui in divUs)

        sols = {
            "U0s": U0s,
            "U1s": U1s,
            "Us": Us,
            "divUs": divUs,
            "divU0s": divU0s,
            "divU1s": divU1s,
        }
        lambdified_sols = {"U0s": fU0s, "U1s": fU1s, "Us": fUs, "divUs": fdivUs}

        analytic_solutions = sols
        analytic_functions = lambdified_sols

        return (analytic_solutions, analytic_functions)

    @classmethod
    def teardown_class(cls):
        pass

    def _test(self, dtype, max_runs=5, polynomial=False, size_min=None, size_max=None):
        enable_extra_tests = self.enable_extra_tests

        size_min = first_not_None(size_min, self.size_min)
        size_max = first_not_None(size_max, self.size_max)

        dim = 3
        valid_factors = {2, 3, 5, 7, 11, 13}
        factors = {1}
        while factors - valid_factors:
            factors.clear()
            shape = tuple(
                npw.random.randint(low=size_min, high=size_max + 1, size=dim).tolist()
            )
            for Si in shape:
                factors.update(set(primefac.primefac(int(Si))))

        domain_boundaries = list(domain_boundary_iterator(dim=dim))
        periodic = domain_boundaries[0]
        domain_boundaries = domain_boundaries[1:]
        random.shuffle(domain_boundaries)
        domain_boundaries.insert(0, periodic)

        for i, (lboundaries, rboundaries) in enumerate(domain_boundaries, 1):
            domain = Box(
                origin=(npw.random.rand(dim) - 0.5),
                length=(0.5 + npw.random.rand(dim)) * 2 * npw.pi,
                lboundaries=lboundaries,
                rboundaries=rboundaries,
            )

            U = VelocityField(domain=domain, name="U", dtype=dtype)
            U0 = VelocityField(domain=domain, name="U0", dtype=dtype)
            U1 = VelocityField(domain=domain, name="U1", dtype=dtype)
            divU = U.div(name="divU")
            divU0 = U0.div(name="divU0")
            divU1 = U1.div(name="divU1")

            self._test_one(
                shape=shape,
                dtype=dtype,
                polynomial=polynomial,
                domain=domain,
                U=U,
                U0=U0,
                U1=U1,
                divU=divU,
                divU0=divU0,
                divU1=divU1,
            )
            if (max_runs is not None) and (i == max_runs):
                missing = ((4 ** (dim + 1) - 1) // 3) - i
                print()
                print(
                    f">> MAX RUNS ACHIEVED FOR {dim}D DOMAINS -- SKIPING {missing} OTHER BOUNDARY CONDITIONS <<"
                )
                print()
                print()
                break
        else:
            assert i == (4 ** (dim + 1) - 1) // 3, (i + 1, (4 ** (dim + 1) - 1) // 3)
            print()
            print(f">> TESTED ALL {dim}D BOUNDARY CONDITIONS <<")
            print()
            print()

    @staticmethod
    def __random_init(data, coords, component, dtype):
        if is_fp(dtype):
            data[...] = npw.random.random(size=data.shape).astype(dtype=dtype)
        else:
            msg = f"Unknown dtype {dtype}."
            raise NotImplementedError(msg)

    @staticmethod
    def __analytic_init(data, coords, fns, component, dtype):
        fn = fns[component]
        data[...] = npw.asarray(fn(*coords)).astype(dtype)

    @classmethod
    def __zero_init(cls, data, coords, dtype, component):
        data[...] = 0

    def _test_one(
        self, shape, dtype, polynomial, domain, U, U0, U1, divU, divU0, divU1
    ):

        print(
            "\nTesting {}D SolenoidalProjection: dtype={} shape={}, polynomial={}, bc=[{}]".format(
                3, dtype.__name__, shape, polynomial, domain.format_boundaries()
            )
        )
        print(" >Building U = U0 + U1 = curl(Psi) + grad(Phi)...")

        # U = curl(Psi) + grad(Phi)
        Psi = VorticityField(velocity=U, name="Psi")
        assert all(
            all(psic.lboundaries == u.lboundaries)
            for (psic, u) in zip(Psi.curl().fields, U.fields)
        )
        assert all(
            all(psic.rboundaries == u.rboundaries)
            for (psic, u) in zip(Psi.curl().fields, U.fields)
        )

        Phi = U.div(name="Phi")
        assert all(
            all(phig.lboundaries == u.lboundaries)
            for (phig, u) in zip(Phi.gradient().fields, U.fields)
        )
        assert all(
            all(phig.rboundaries == u.rboundaries)
            for (phig, u) in zip(Phi.gradient().fields, U.fields)
        )

        lboundaries = tuple(psi.lboundaries[::-1] for psi in Psi.fields) + (
            Phi.lboundaries[::-1],
        )
        rboundaries = tuple(psi.rboundaries[::-1] for psi in Psi.fields) + (
            Phi.rboundaries[::-1],
        )

        (analytic_solutions, analytic_functions) = self.build_analytic_solutions(
            polynomial=polynomial,
            lboundaries=lboundaries,  # => boundaries in variable order x0,...,xn
            rboundaries=rboundaries,
            origin=domain.origin[::-1],
            end=domain.end[::-1],
        )

        del Psi
        del Phi
        del lboundaries
        del rboundaries

        def format_expr(e):
            return truncate_expr(round_expr(e, 3), 80)

        print(" >Input analytic velocity:")
        for Ui, ui in zip(U, analytic_solutions["Us"]):
            print(f"   *{Ui.pretty_name} = {format_expr(ui)}")
        print("   *div(U) = {}".format(format_expr(analytic_solutions["divUs"][0])))
        print(" >Expected velocity vector potential (solenoidal projection) is:")
        for Ui, ui in zip(U0, analytic_solutions["U0s"]):
            print(f"   *{Ui.pretty_name} = {format_expr(ui)}")
        print("   *div(U0) = {}".format(format_expr(analytic_solutions["divU0s"][0])))
        print(" >Expected velocity scalar potential is:")
        for Ui, ui in zip(U1, analytic_solutions["U1s"]):
            print(f"   *{Ui.pretty_name} = {format_expr(ui)}")
        print("   *div(U1) = {}".format(format_expr(analytic_solutions["divU1s"][0])))
        print(" >Testing all available implementations:")

        implementations = SolenoidalProjection.implementations()

        # Compute reference solution
        variables = {U: shape, divU: shape, U0: shape, divU0: shape}

        def iter_impl(impl):
            base_kwds = dict(
                input_field=U,
                input_field_div=divU,
                output_field=U0,
                output_field_div=divU0,
                variables=variables,
                implementation=impl,
                name=f"projection_{str(impl).lower()}",
            )
            if impl is Implementation.PYTHON:
                msg = "   *Python FFTW: "
                print(msg, end=" ")
                yield SolenoidalProjection(**base_kwds)
            elif impl is Implementation.OPENCL:
                msg = "   *OpenCl CLFFT: "
                print(msg)
                for cl_env in iter_clenv():
                    msg = "     |platform {}, device {}".format(
                        cl_env.platform.name.strip(), cl_env.device.name.strip()
                    )
                    print(msg, end=" ")
                    yield SolenoidalProjection(cl_env=cl_env, **base_kwds)
                msg = "   *OpenCl FFTW: "
                print(msg)
                cpu_envs = tuple(iter_clenv(device_type="cpu"))
                if cpu_envs:
                    for cl_env in cpu_envs:
                        msg = "     |platform {}, device {}".format(
                            cl_env.platform.name.strip(), cl_env.device.name.strip()
                        )
                        print(msg, end=" ")
                        yield SolenoidalProjection(
                            cl_env=cl_env, enforce_implementation=False, **base_kwds
                        )
            else:
                msg = f"Unknown implementation to test {impl}."
                raise NotImplementedError(msg)

        # Compare to analytic solution
        Uref, U0ref = None, None
        for impl in implementations:
            for op in iter_impl(impl):
                op = op.build()

                du = op.get_input_discrete_field(U)
                du0 = op.get_output_discrete_field(U0)
                du_div = op.get_output_discrete_field(divU)
                du0_div = op.get_output_discrete_field(divU0)

                du.initialize(
                    self.__analytic_init, dtype=dtype, fns=analytic_functions["Us"]
                )

                if Uref is None:
                    du0.initialize(
                        self.__analytic_init, dtype=dtype, fns=analytic_functions["U0s"]
                    )
                    du_div.initialize(
                        self.__analytic_init,
                        dtype=dtype,
                        fns=analytic_functions["divUs"],
                    )
                    du0_div.initialize(self.__zero_init, dtype=dtype)
                    Uref = tuple(data.get().handle.copy() for data in du.data)
                    U0ref = tuple(data.get().handle.copy() for data in du0.data)
                    divUref = tuple(data.get().handle.copy() for data in du_div.data)
                    divU0ref = tuple(data.get().handle.copy() for data in du0_div.data)

                du0.initialize(self.__random_init, dtype=dtype)
                du_div.initialize(self.__random_init, dtype=dtype)
                du0_div.initialize(self.__random_init, dtype=dtype)

                op.apply(simulation=None)

                Uout = tuple(data.get().handle.copy() for data in du.data)
                U0out = tuple(data.get().handle.copy() for data in du0.data)
                divUout = tuple(data.get().handle.copy() for data in du_div.data)
                divU0out = tuple(data.get().handle.copy() for data in du0_div.data)

                s = npw.prod(du.space_step)
                print(
                    "[divU={}, divU0={}]".format(
                        npw.sqrt(npw.sum(divUout[0] ** 2) * s),
                        npw.sqrt(npw.sum(divU0out[0] ** 2) * s),
                    )
                )

                self._check_output(
                    impl,
                    op,
                    Uref,
                    divUref,
                    U0ref,
                    divU0ref,
                    Uout,
                    divUout,
                    U0out,
                    divU0out,
                )
                print()

    @classmethod
    def _check_output(
        cls, impl, op, Uref, divUref, U0ref, divU0ref, Uout, divUout, U0out, divU0out
    ):
        check_instance(Uref, tuple, values=npw.ndarray, size=3)
        check_instance(U0ref, tuple, values=npw.ndarray, size=3)
        check_instance(Uout, tuple, values=npw.ndarray, size=3)
        check_instance(U0out, tuple, values=npw.ndarray, size=3)
        check_instance(divUref, tuple, values=npw.ndarray, size=1)
        check_instance(divU0ref, tuple, values=npw.ndarray, size=1)
        check_instance(divUout, tuple, values=npw.ndarray, size=1)
        check_instance(divU0out, tuple, values=npw.ndarray, size=1)

        msg0 = "Reference field {} is not finite."
        for fields, name in zip(
            (Uref, U0ref, divUref, divU0ref), ("Uref", "U0ref", "divUref", "divU0ref")
        ):
            for i, field in enumerate(fields):
                iname = f"{name}{i}"
                mask = npw.isfinite(field)
                if not mask.all():
                    print()
                    print(field)
                    print()
                    print(field[~mask])
                    print()
                    msg = msg0.format(iname)
                    raise ValueError(msg)

        for out_buffers, ref_buffers, name in zip(
            (Uout, U0out, divUout, divU0out),
            (Uref, U0ref, divUref, divU0ref),
            ("U", "U0", "divU", "divU0"),
        ):
            print(f"     | {name}=(", end=" ")
            for i, (fout, fref) in enumerate(zip(out_buffers, ref_buffers)):
                iname = f"{name}{i}"
                assert fout.dtype == fref.dtype, iname
                assert fout.shape == fref.shape, iname

                eps = npw.finfo(fout.dtype).eps
                dist = npw.abs(fout - fref)
                dinf = npw.max(dist)
                try:
                    deps = int(npw.ceil(dinf / eps))
                except ValueError:
                    deps = npw.nan
                if deps < 10000:
                    print(f"{deps}eps", end=" ")
                    if i != len(out_buffers) - 1:
                        print(",", end=" ")
                    continue

                has_nan = npw.any(npw.isnan(fout))
                has_inf = npw.any(npw.isinf(fout))

                print()
                print()
                print(f"Test output comparisson for {name} failed for component {i}:")
                print(f" *has_nan: {has_nan}")
                print(f" *has_inf: {has_inf}")
                print(f" *dinf={dinf} ({deps} eps)")
                print()
                if cls.enable_debug_mode:
                    print("REFERENCE INPUTS:")
                    for j, w in enumerate(Uref):
                        print(f"U_{j}")
                        print(w)
                        print()
                    if name in ("divU", "U0", "divU0"):
                        print(f"REFERENCE OUTPUT FOR {name}:")
                        print(f"{name}_{i}")
                        print(fref)
                        print()
                        print(f"OPERATOR {op.name.upper()} OUTPUT FOR {name}:")
                        print()
                        print(f"{name}_{i}")
                        print(fout)
                        print()
                    else:
                        print("MODIFIED INPUTS:")
                        for j, w in enumerate(out_buffers):
                            print(f"{name}_{j}")
                            print(w)
                            print()
                        print()
                    print()

                msg = "Test failed for {} on component {} for implementation {}."
                msg = msg.format(name, i, impl)
                raise RuntimeError(msg)
            print(")", end=" ")

    def test_long_3d_float32(self, **kwds):
        self._test(dtype=npw.float32, **kwds)

    def test_3d_float64(self, max_runs=1, **kwds):
        self._test(dtype=npw.float64, max_runs=max_runs, **kwds)

    def perform_tests(self):
        max_3d_runs = None if __ENABLE_LONG_TESTS__ else 3
        self.test_long_3d_float32(max_runs=max_3d_runs)
        self.test_3d_float64(max_runs=max_3d_runs)


if __name__ == "__main__":
    TestSolenoidalProjectionOperator.setup_class(
        enable_extra_tests=False, enable_debug_mode=False
    )

    test = TestSolenoidalProjectionOperator()

    with printoptions(
        threshold=10000,
        linewidth=240,
        nanstr="nan",
        infstr="inf",
        formatter={"float": lambda x: f"{x:>6.2f}"},
    ):
        test.perform_tests()

    TestSolenoidalProjectionOperator.teardown_class()
