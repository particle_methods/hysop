# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Test of vorticity penalization
"""
import random

from hysop.constants import HYSOP_REAL
from hysop.testsenv import __ENABLE_LONG_TESTS__
from hysop.tools.contexts import printoptions
from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.io_utils import IO
from hysop.tools.numpywrappers import npw
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.operator.penalization import PenalizeVorticity
from hysop.constants import Implementation
from hysop.numerics.stencil.stencil_generator import CenteredStencilGenerator, MPQ
import numpy as np

from hysop import Field, Box


class TestPenalizeVorticity:

    @classmethod
    def setup_class(
        cls, enable_extra_tests=__ENABLE_LONG_TESTS__, enable_debug_mode=False
    ):

        IO.set_default_path("/tmp/hysop_tests/test_penalize_vorticity")

        if enable_debug_mode:
            cls.size_min = 15
            cls.size_max = 16
        else:
            cls.size_min = 23
            cls.size_max = 87

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_debug_mode = enable_debug_mode

        cls.t = ScalarParameter(name="t", dtype=HYSOP_REAL)
        cls.dt = ScalarParameter(name="dt", dtype=HYSOP_REAL)

    @classmethod
    def teardown_class(cls):
        pass

    @staticmethod
    def __velo_init(data, coords, component):
        data[...] = 1.0

    @staticmethod
    def __zero_init(data, coords, component):
        (x, y, z) = coords
        if component == 0:
            data[...] = np.cos(x) + np.sin(y) + np.cos(z)
        if component == 1:
            data[...] = np.sin(x) + np.cos(y)
        if component == 2:
            data[...] = np.cos(z) + np.sin(y) + np.cos(x)

    @staticmethod
    def __sphere_init(data, coords, component):
        assert component == 0
        (x, y, z) = coords
        data[...] = 0.0
        pos = (random.random(), random.random(), random.random())
        radius = random.random()
        rr = np.sqrt(
            (x - pos[0]) * (x - pos[0])
            + (y - pos[1]) * (y - pos[1])
            + (z - pos[2]) * (z - pos[2])
        )
        data[rr <= radius] = 1.0

    def _test(self, dim, dtype, size_min=None, size_max=None):
        enable_extra_tests = self.enable_extra_tests

        size_min = first_not_None(size_min, self.size_min)
        size_max = first_not_None(size_max, self.size_max)

        shape = tuple(
            npw.random.randint(low=size_min, high=size_max + 1, size=dim).tolist()
        )

        domain = Box(length=(1,) * dim)
        velo = Field(domain=domain, name="velo", dtype=dtype, nb_components=3)
        vorti = Field(domain=domain, name="vorti", dtype=dtype, nb_components=3)
        obstacle = Field(domain=domain, name="sphere", dtype=dtype, nb_components=1)

        self._test_one(
            shape=shape,
            dim=dim,
            dtype=dtype,
            domain=domain,
            velo=velo,
            vorti=vorti,
            obstacle=obstacle,
        )

    def _test_one(self, shape, dim, dtype, domain, velo, vorti, obstacle):
        print(
            "\nTesting {}D PenalizeVorticity: dtype={} shape={}".format(
                dim, dtype.__name__, shape
            )
        )

        self.t.value = random.random()
        self.dt.value = random.random()
        print(f" >Parameter t has been set to {self.t()}.")
        print(f" >Parameter dt has been set to {self.dt()}.")
        print(" >Testing all implementations:")

        implementations = PenalizeVorticity.implementations()
        variables = {velo: shape, vorti: shape, obstacle: shape}

        def iter_impl(impl):
            base_kwds = dict(
                velocity=velo,
                vorticity=vorti,
                dt=self.dt,
                variables=variables,
                implementation=impl,
                name=f"penalize_vorticity_{str(impl).lower()}",
            )
            if impl is Implementation.PYTHON:
                msg = "   *Python: "
                print(msg, end=" ")
                yield PenalizeVorticity(obstacles=[obstacle], coeff=1e8, **base_kwds)
                print()
            else:
                msg = f"Unknown implementation to test {impl}."
                raise NotImplementedError(msg)

        Fref = None
        for impl in implementations:
            for op in iter_impl(impl):
                op = op.build()
                du = op.input_discrete_tensor_fields[velo]
                dw = op.input_discrete_tensor_fields[vorti]
                dobstacle = op.input_discrete_fields[obstacle]

                dobstacle.initialize(self.__sphere_init)
                du.initialize(self.__velo_init)
                dw.initialize(self.__zero_init)
                if Fref is None:
                    csg = CenteredStencilGenerator()
                    csg.configure(dtype=MPQ, dim=1)
                    stencil = tuple(
                        csg.generate_exact_stencil(derivative=1, order=4)
                        for _ in range(3)
                    )
                    for s, dx in zip(stencil, dw.space_step):
                        s.replace_symbols({s.dx: dx})
                    Fref = tuple(data.get().handle.copy() for data in dw.data)
                    tmp_v = tuple(npw.zeros(data.shape) for data in dw.data)
                    tmp_w = npw.zeros(tmp_v[0].shape)
                    o = dobstacle.data[0]
                    dtcoeff = self.dt() * 1e8
                    for f, tmp in zip(du.data, tmp_v):
                        tmp[...] = -f * dtcoeff * o / (1.0 + dtcoeff * o)

                    tmp_w = stencil[0](a=tmp_v[2], out=tmp_w, axis=2)
                    Fref[1][...] += -tmp_w
                    tmp_w = stencil[0](a=tmp_v[1], out=tmp_w, axis=2)
                    Fref[2][...] += tmp_w

                    # Y direction
                    tmp_w = stencil[1](a=tmp_v[0], out=tmp_w, axis=1)
                    Fref[2][...] += -tmp_w
                    tmp_w = stencil[1](a=tmp_v[2], out=tmp_w, axis=1)
                    Fref[0][...] += tmp_w

                    # Z direction
                    tmp_w = stencil[2](a=tmp_v[1], out=tmp_w, axis=0)
                    Fref[0][...] += -tmp_w
                    tmp_w = stencil[2](a=tmp_v[0], out=tmp_w, axis=0)
                    Fref[1][...] += tmp_w

                op.apply()
                Fout = tuple(data.get().handle.copy() for data in dw.data)
                self._check_output(impl, op, Fref, Fout, dw.compute_slices)

    @classmethod
    def _check_output(cls, impl, op, dvorti, dout, sl):
        check_instance(dvorti, tuple, values=npw.ndarray)
        check_instance(dout, tuple, values=npw.ndarray, size=len(dvorti))

        msg0 = "Reference field {} is not finite."
        for i, field in enumerate(dout):
            iname = f"F{i}"
            mask = npw.isfinite(field)
            if not mask.all():
                print()
                print(field)
                print()
                print(field[~mask])
                print()
                msg = msg0.format(iname)
                raise ValueError(msg)

        for i, (fout, fref) in enumerate(zip(dout, dvorti)):
            assert fout.dtype == fref.dtype
            assert fout.shape == fref.shape

            eps = npw.finfo(fout.dtype).eps
            dist = npw.abs(fout[sl] - fref[sl])
            dinf = npw.max(dist)
            deps = int(npw.ceil(dinf / eps))
            if deps < 200:
                print(f"{deps}eps, ", end=" ")
                continue
            has_nan = npw.any(npw.isnan(fout))
            has_inf = npw.any(npw.isinf(fout))
            print()
            print()
            print(f"Test output comparisson failed for component {i}:")
            print(f" *has_nan: {has_nan}")
            print(f" *has_inf: {has_inf}")
            print(f" *dinf={dinf} ({deps} eps)")
            print()
            msg = f"Test failed on component {i} for implementation {impl}."
            raise RuntimeError(msg)

    def perform_tests(self):
        self._test(dim=3, dtype=npw.float32)
        self._test(dim=3, dtype=npw.float64)

    def test_3d_float64(self):
        self._test(dim=3, dtype=npw.float64)

    def test_3d_float32(self):
        self._test(dim=3, dtype=npw.float32)


if __name__ == "__main__":
    TestPenalizeVorticity.setup_class(enable_extra_tests=False, enable_debug_mode=False)

    test = TestPenalizeVorticity()

    with printoptions(
        threshold=10000,
        linewidth=240,
        nanstr="nan",
        infstr="inf",
        formatter={"float": lambda x: f"{x:>6.2f}"},
    ):
        test.perform_tests()

    TestPenalizeVorticity.teardown_class()
