# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys
import numpy as np

from hysop.testsenv import __ENABLE_LONG_TESTS__
from hysop.testsenv import iter_clenv
from hysop.tools.numpywrappers import npw
from hysop.tools.contexts import printoptions
from hysop.tools.htypes import first_not_None, to_tuple
from hysop.tools.io_utils import IO
from hysop.core.graph.computational_graph import ComputationalGraph
from hysop.operator.directional.advection_dir import DirectionalAdvection
from hysop.operator.misc import ForceTopologyState
from hysop.parameters.scalar_parameter import ScalarParameter

from hysop import Field, Box
from hysop.methods import Remesh, TimeIntegrator
from hysop.constants import Implementation, DirectionLabels, Backend, HYSOP_REAL
from hysop.numerics.splitting.strang import StrangSplitting, StrangOrder
from hysop.numerics.odesolvers.runge_kutta import Euler, RK2, RK4
from hysop.numerics.remesh.remesh import RemeshKernel


class TestDirectionalAdvectionOperator:

    @classmethod
    def setup_class(
        cls, enable_extra_tests=__ENABLE_LONG_TESTS__, enable_debug_mode=False
    ):

        IO.set_default_path("/tmp/hysop_tests/test_directional_advection")

        if enable_debug_mode:
            cls.size_min = 8
            cls.size_max = 9
        else:
            cls.size_min = 10
            cls.size_max = 23

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_debug_mode = enable_debug_mode

    @classmethod
    def teardown_class(cls):
        pass

    def _test(self, dim, is_inplace, size_min=None, size_max=None):
        assert dim > 0

        # periodic boundaries removes one computational point
        # so we add one here.
        size_min = first_not_None(size_min, self.size_min) + 1
        size_max = first_not_None(size_max, self.size_max) + 1

        shape = tuple(
            npw.random.randint(low=size_min, high=size_max, size=dim).tolist()
        )

        if self.enable_extra_tests:
            flt_types = (npw.float32, npw.float64)
            time_integrators = (
                Euler,
                RK2,
                RK4,
            )
            remesh_kernels = (
                Remesh.L2_1,
                Remesh.L2_1s,
                Remesh.L4_2,
                Remesh.L4_2s,
                Remesh.L8_4,
                Remesh.L8_4s,
            )
            velocity_cfls = (0.62, 1.89)
        else:
            flt_types = (HYSOP_REAL,)
            time_integrators = (RK2,)
            remesh_kernels = (Remesh.L4_2,)
            velocity_cfls = (1.89,)

        domain = Box(length=(2 * npw.pi,) * dim)
        for dtype in flt_types:
            Vin = Field(domain=domain, name="Vin", dtype=dtype, nb_components=dim)
            Sin = Field(domain=domain, name="Sin", dtype=dtype, nb_components=2)
            Sout = Field(domain=domain, name="Sout", dtype=dtype, nb_components=2)
            for time_integrator in time_integrators:
                for remesh_kernel in remesh_kernels:
                    for velocity_cfl in velocity_cfls:
                        self._test_one(
                            time_integrator=time_integrator,
                            remesh_kernel=remesh_kernel,
                            shape=shape,
                            dim=dim,
                            dtype=dtype,
                            is_inplace=is_inplace,
                            domain=domain,
                            Vin=Vin,
                            Sin=Sin,
                            Sout=Sout,
                            velocity_cfl=velocity_cfl,
                        )
                        print()

    @classmethod
    def __velocity_init(cls, data, coords, component, axes):
        if component in axes:
            data[...] = +1.0
        else:
            data[...] = 0.0

    @classmethod
    def __scalar_init(cls, data, coords, component, offsets=None):
        offsets = first_not_None(offsets, (0.0,) * len(coords))
        assert len(coords) == len(offsets)
        data[...] = 1.0 / (component + 1)
        for c, o in zip(coords, offsets):
            data[...] *= npw.cos(c + o)

    def _test_one(
        self,
        time_integrator,
        remesh_kernel,
        shape,
        dim,
        dtype,
        is_inplace,
        domain,
        velocity_cfl,
        Vin,
        Sin,
        Sout,
    ):

        print(
            "\nTesting {}D DirectionalAdvection_{}_{}: inplace={} dtype={} shape={}, cfl={}".format(
                dim,
                time_integrator.name(),
                remesh_kernel,
                is_inplace,
                dtype.__name__,
                shape,
                velocity_cfl,
            ),
            end=" ",
        )
        if is_inplace:
            vin = Vin
            sin, sout = Sin, Sin
            variables = {vin: shape, sin: shape}
        else:
            vin = Vin
            sin, sout = Sin, Sout
            variables = {vin: shape, sin: shape, sout: shape}

        # Use optimal timestep, ||Vi||_inf is 1 on a per-axis basis
        dt = velocity_cfl * np.divide(domain.length, shape).min()
        dt = ScalarParameter("dt", initial_value=dt, const=True)

        ref_impl = Implementation.PYTHON
        implementations = list(DirectionalAdvection.implementations().keys())
        assert ref_impl in implementations
        implementations.remove(ref_impl)
        implementations = [ref_impl] + implementations

        method = {TimeIntegrator: time_integrator, Remesh: remesh_kernel}

        def iter_impl(impl):
            base_kwds = dict(
                velocity=vin,
                advected_fields=sin,
                advected_fields_out=sout,
                dt=dt,
                velocity_cfl=velocity_cfl,
                variables=variables,
                implementation=impl,
                method=method,
                name=f"advection_{str(impl).lower()}",
            )
            if impl is Implementation.PYTHON:
                da = DirectionalAdvection(**base_kwds)
                split = StrangSplitting(
                    splitting_dim=dim, order=StrangOrder.STRANG_SECOND_ORDER
                )
                split.push_operators(da)
                force_tstate = ForceTopologyState(
                    fields=variables.keys(), variables=variables, backend=Backend.HOST
                )
                graph = ComputationalGraph(name="test_graph")
                graph.push_nodes(split)
                yield "Python", graph
            elif impl is Implementation.OPENCL:
                for cl_env in iter_clenv():
                    msg = "platform {}, device {}".format(
                        cl_env.platform.name.strip(), cl_env.device.name.strip()
                    )
                    da = DirectionalAdvection(cl_env=cl_env, **base_kwds)
                    split = StrangSplitting(
                        splitting_dim=dim, order=StrangOrder.STRANG_SECOND_ORDER
                    )
                    split.push_operators(da)
                    force_tstate = ForceTopologyState(
                        fields=variables.keys(),
                        variables=variables,
                        backend=Backend.OPENCL,
                        cl_env=cl_env,
                    )
                    graph = ComputationalGraph(name="test_graph")
                    graph.push_nodes(split)
                    yield msg, graph
            else:
                msg = f"Unknown implementation to test {impl}."
                raise NotImplementedError(msg)

        # Compare to other implementations
        advec_axes = (tuple(),)
        advec_axes += tuple((x,) for x in range(dim))
        if dim > 1:
            advec_axes += (tuple(range(dim)),)

        reference_fields = {}
        for impl in implementations:
            print(f"\n >Implementation {impl}:", end=" ")
            is_ref = impl == ref_impl
            for sop, graph in iter_impl(impl):
                print(f"\n   *{sop}: ", end=" ")

                graph.build()

                for axes in advec_axes:
                    ref_outputs = reference_fields.setdefault(axes, {})
                    napplies = 10
                    Vi = npw.asarray(
                        [+1.0 if (i in axes) else +0.0 for i in range(dim)], dtype=dtype
                    )

                    dvin = graph.get_input_discrete_field(vin)
                    dsin = graph.get_input_discrete_field(sin)
                    dsout = graph.get_output_discrete_field(sout)
                    dsref = dsout.clone()

                    sys.stdout.write(".")
                    sys.stdout.flush()
                    try:
                        dvin.initialize(self.__velocity_init, axes=axes)
                        dsin.initialize(self.__scalar_init)
                        _input = tuple(
                            dsin.data[i].get().handle.copy()
                            for i in range(dsin.nb_components)
                        )
                        S0 = dsin.integrate()

                        for k in range(napplies + 1):
                            graph.apply()

                            output = tuple(
                                dsout.data[i].get().handle.copy()
                                for i in range(dsout.nb_components)
                            )

                            for i in range(dsout.nb_components):
                                mask = npw.isfinite(output[i][dsout.compute_slices])
                                if not mask.all():
                                    msg = f"\nFATAL ERROR: Output is not finite on axis {i}.\n"
                                    print(msg)
                                    npw.fancy_print(
                                        output[i],
                                        replace_values={
                                            (lambda a: npw.isfinite(a)): "."
                                        },
                                    )
                                    raise RuntimeError(msg)

                            if is_ref:
                                dxk = -Vi * (k + 1) * dt()
                                dsref[0].sdata[...] = npw.nan
                                dsref[1].sdata[...] = npw.inf
                                dsref.initialize(
                                    self.__scalar_init, offsets=dxk.tolist()
                                )
                                d = dsout.distance(dsref, p=2)
                                if npw.any(d > 1e-1):
                                    print(
                                        "\nFATAL ERROR: Could not match analytic advection."
                                    )
                                    print("DSOUT")
                                    for output in dsout:
                                        print(output.sdata[output.compute_slices])
                                    print("DSREF")
                                    for ref in dsref:
                                        print(ref.sdata[ref.compute_slices])
                                    print("DSREF - DSOUT")
                                    for output, ref in zip(dsout, dsref):
                                        print(
                                            output.sdata[output.compute_slices].get()
                                            - ref.sdata[ref.compute_slices].get()
                                        )
                                    msg = "Test failed with V={}, k={}, dxk={}, inter-field L2 distances are {}."
                                    msg = msg.format(
                                        Vi,
                                        k,
                                        to_tuple(dxk, cast=float),
                                        to_tuple(d, cast=float),
                                    )
                                    raise RuntimeError(msg)
                                ref_outputs[k] = output
                            else:
                                assert k in ref_outputs
                                reference = ref_outputs[k]
                                for i in range(dsout.nb_components):
                                    di = npw.abs(reference[i] - output[i])
                                    max_di = npw.max(di)
                                    neps = 1000
                                    max_tol = neps * npw.finfo(dsout.dtype).eps
                                    if max_di > max_tol:
                                        print(
                                            "\nFATAL ERROR: Could not match other implementation results."
                                        )
                                        print(
                                            f"\nComparisson failed at step {k} and component {i}:"
                                        )
                                        for j, dv in enumerate(dvin):
                                            print(
                                                f"VELOCITY INPUT {DirectionLabels[j]}"
                                            )
                                            print(dv.sdata[dv.compute_slices])
                                        print("SCALAR INPUT")
                                        print(_input[i])
                                        print("SCALAR REFERENCE")
                                        print(reference[i])
                                        print("SCALAR OUTPUT")
                                        print(output[i])
                                        print("ABS(REF - OUT)")
                                        npw.fancy_print(
                                            di,
                                            replace_values={
                                                (lambda a: a < max_tol): "."
                                            },
                                        )
                                        print()
                                        msg = "Output did not match reference output for component {} at time step {}."
                                        msg += (
                                            f"\n > max computed distance was {max_di}."
                                        )
                                        msg += f"\n > max tolerence was set to {max_tol} ({neps} eps)."
                                        msg = msg.format(i, k)
                                        raise RuntimeError(msg)
                            Si = dsout.integrate()
                            if not npw.all(npw.isfinite(Si)):
                                msg = f"Integral is not finite. Got {Si}."
                                raise RuntimeError(msg)
                            if (npw.abs(Si - S0) > 1e-3).any():
                                msg = "Scalar was not conserved on iteration {}, expected {} but got {}."
                                msg = msg.format(
                                    k,
                                    to_tuple(S0, cast=float),
                                    to_tuple(Si, cast=float),
                                )
                                raise RuntimeError(msg)
                    except:
                        sys.stdout.write("\bx\n\n")
                        sys.stdout.flush()
                        raise

    def test_advec_1D_inplace(self):
        self._test(dim=1, is_inplace=True)

    def test_advec_2D_inplace(self):
        self._test(dim=2, is_inplace=True)

    def test_advec_3D_inplace(self):
        self._test(dim=3, is_inplace=True)

    def perform_tests(self):
        self.test_advec_1D_inplace()
        self.test_advec_2D_inplace()
        self.test_advec_3D_inplace()
        print()


if __name__ == "__main__":
    import hysop

    TestDirectionalAdvectionOperator.setup_class(
        enable_extra_tests=False, enable_debug_mode=True
    )

    test = TestDirectionalAdvectionOperator()

    with printoptions(
        threshold=10000,
        linewidth=240,
        nanstr="nan",
        infstr="inf",
        formatter={"float": lambda x: f"{x:>6.2f}"},
    ):
        test.perform_tests()

    TestDirectionalAdvectionOperator.teardown_class()
