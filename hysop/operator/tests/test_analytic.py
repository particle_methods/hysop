# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Test of fields defined with an analytic formula.
"""
import itertools as it
import random

import sympy as sm
from hysop import Box, Field
from hysop.constants import HYSOP_REAL
from hysop.operator.analytic import AnalyticField, AnalyticScalarField, Implementation
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.testsenv import (
    __ENABLE_LONG_TESTS__,
    __HAS_OPENCL_BACKEND__,
    iter_clenv,
    opencl_failed,
)
from hysop.tools.contexts import printoptions
from hysop.tools.io_utils import IO
from hysop.tools.numerics import is_fp, is_integer
from hysop.tools.numpywrappers import npw
from hysop.tools.htypes import check_instance, first_not_None


class TestAnalyticField:

    @classmethod
    def setup_class(
        cls, enable_extra_tests=__ENABLE_LONG_TESTS__, enable_debug_mode=False
    ):

        IO.set_default_path("/tmp/hysop_tests/test_analytic")

        if enable_debug_mode:
            cls.size_min = 15
            cls.size_max = 16
        else:
            cls.size_min = 23
            cls.size_max = 87

        cls.enable_extra_tests = enable_extra_tests
        cls.enable_debug_mode = enable_debug_mode

        cls.t = ScalarParameter(name="t", dtype=HYSOP_REAL)

        cls.build_analytic_expressions()

    @classmethod
    def build_analytic_expressions(cls):
        from hysop.symbolic.base import TensorBase
        from hysop.symbolic.field import curl, laplacian
        from hysop.symbolic.frame import SymbolicFrame
        from hysop.tools.sympy_utils import enable_pretty_printing

        enable_pretty_printing()

        # at this moment we just test a periodic solver
        analytic_expressions = {}
        analytic_functions = {}
        for dim in (1, 2, 3):
            frame = SymbolicFrame(dim=dim)
            coords = frame.coords

            def gen_F0():
                kis = tuple(random.randint(1, 5) for _ in range(dim))
                qis = tuple(npw.random.rand(dim).round(decimals=3).tolist())
                basis = tuple(
                    (sm.cos(ki * xi + qi), sm.sin(ki * xi + qi))
                    for (ki, qi, xi) in zip(kis, qis, coords)
                )
                F0 = sm.Integer(1) / (sm.Integer(1) + cls.t.s)
                for i in range(dim):
                    F0 *= random.choice(basis[i])
                return F0

            def gen_F1():
                base = tuple(
                    tuple(coords[i] ** k for k in range(5)) for i in range(dim)
                )
                base = tuple({npw.prod(x) for x in it.product(*base)})
                base = tuple({random.choice(base) for _ in range(3)})
                F1 = sm.Integer(1) / (sm.Integer(1) + cls.t.s)
                F1 *= (base * (npw.random.rand(len(base)).round(2))).sum()
                return F1

            params = coords + (cls.t.s,)
            Fs = npw.asarray([gen_F0(), gen_F1()], dtype=object).view(TensorBase)
            fFs = tuple(sm.lambdify(params, F) for F in Fs)

            analytic_expressions[dim] = {"F": Fs}
            analytic_functions[dim] = {"F": fFs}

        cls.analytic_expressions = analytic_expressions
        cls.analytic_functions = analytic_functions

    @classmethod
    def teardown_class(cls):
        pass

    @staticmethod
    def __random_init(data, coords, component):
        dtype = data.dtype
        if is_fp(dtype):
            data[...] = npw.random.random(size=data.shape).astype(dtype=dtype)
        else:
            msg = f"Unknown dtype {dtype}."
            raise NotImplementedError(msg)

    @staticmethod
    def __analytic_init(data, coords, fns, t, component):
        fn = fns[component]
        data[...] = npw.asarray(fn(*(coords + (t(),)))).astype(data.dtype)

    def _test(self, dim, dtype, size_min=None, size_max=None):
        enable_extra_tests = self.enable_extra_tests

        size_min = first_not_None(size_min, self.size_min)
        size_max = first_not_None(size_max, self.size_max)

        shape = tuple(
            npw.random.randint(low=size_min, high=size_max + 1, size=dim).tolist()
        )

        domain = Box(length=(1,) * dim)
        F = Field(domain=domain, name="F", dtype=dtype, nb_components=2)

        self._test_one(shape=shape, dim=dim, dtype=dtype, domain=domain, F=F)

    def _test_one(self, shape, dim, dtype, domain, F):

        print(
            "\nTesting {}D AnalyticField: dtype={} shape={}".format(
                dim, dtype.__name__, shape
            )
        )
        print(" >Input analytic functions:")
        for i, Fi in enumerate(self.analytic_expressions[dim]["F"]):
            print(f"   *F{i}(x,t) = {Fi}")
        self.t.value = random.random()
        print(f" >Parameter t has been set to {self.t()}.")
        print(" >Testing all implementations:")

        implementations = AnalyticScalarField.implementations()

        variables = {F: shape}
        fns = self.analytic_functions[dim]["F"]
        exprs = self.analytic_expressions[dim]["F"]

        def iter_impl(impl):
            base_kwds = dict(
                field=F,
                variables=variables,
                implementation=impl,
                name=f"analytic_{str(impl).lower()}",
            )
            if impl is Implementation.PYTHON:
                msg = "   *Python: "
                print(msg, end=" ")
                yield AnalyticField(
                    formula=self.__analytic_init,
                    extra_input_kwds={"fns": fns, "t": self.t},
                    **base_kwds,
                )
                print()
            elif impl is Implementation.OPENCL:
                assert base_kwds["field"] is F
                msg = "   *Opencl: "
                print(msg)
                for cl_env in iter_clenv():
                    msg = "      >platform {}, device {}:".format(
                        cl_env.platform.name.strip(), cl_env.device.name.strip()
                    )
                    print(msg, end=" ")
                    yield AnalyticField(cl_env=cl_env, formula=exprs, **base_kwds)
                    print()
                print()
            else:
                msg = f"Unknown implementation to test {impl}."
                raise NotImplementedError(msg)

        # Compare to analytic solution
        Fref = None
        for impl in implementations:
            for op in iter_impl(impl):
                op = op.build()
                dF = op.get_output_discrete_field(F)

                if Fref is None:
                    dF.initialize(self.__analytic_init, fns=fns, t=self.t)
                    Fref = tuple(data.get().handle.copy() for data in dF.data)

                dF.initialize(self.__random_init)
                op.apply()

                Fout = tuple(data.get().handle.copy() for data in dF.data)
                self._check_output(impl, op, Fref, Fout)

    @classmethod
    def _check_output(cls, impl, op, Fref, Fout):
        check_instance(Fref, tuple, values=npw.ndarray)
        check_instance(Fout, tuple, values=npw.ndarray, size=len(Fref))

        msg0 = "Reference field {} is not finite."
        for i, field in enumerate(Fref):
            iname = f"F{i}"
            mask = npw.isfinite(field)
            if not mask.all():
                print()
                print(field)
                print()
                print(field[~mask])
                print()
                msg = msg0.format(iname)
                raise ValueError(msg)

        for i, (fout, fref) in enumerate(zip(Fout, Fref)):
            iname = "{}{}".format("F", i)
            assert fout.dtype == fref.dtype, iname
            assert fout.shape == fref.shape, iname

            eps = npw.finfo(fout.dtype).eps
            dist = npw.abs(fout - fref)
            dinf = npw.max(dist)
            deps = int(npw.ceil(dinf / eps))
            if deps < 10:
                print(f"{deps}eps, ", end=" ")
                continue
            has_nan = npw.any(npw.isnan(fout))
            has_inf = npw.any(npw.isinf(fout))

            print()
            print()
            print(f"Test output comparisson for {iname} failed for component {i}:")
            print(f" *has_nan: {has_nan}")
            print(f" *has_inf: {has_inf}")
            print(f" *dinf={dinf} ({deps} eps)")
            print()
            msg = f"Test failed for {iname} on component {i} for implementation {impl}."
            raise RuntimeError(msg)

    def test_1d_float32(self):
        self._test(dim=1, dtype=npw.float32)

    def test_2d_float32(self):
        self._test(dim=2, dtype=npw.float32)

    def test_3d_float32(self):
        self._test(dim=3, dtype=npw.float32)

    def test_1d_float64(self):
        self._test(dim=1, dtype=npw.float64)

    def test_2d_float64(self):
        self._test(dim=2, dtype=npw.float64)

    def test_3d_float64(self):
        self._test(dim=3, dtype=npw.float64)

    def perform_tests(self):
        self.test_1d_float32()
        self.test_2d_float32()
        self.test_3d_float32()
        self.test_1d_float64()
        self.test_2d_float64()
        self.test_3d_float64()


if __name__ == "__main__":
    TestAnalyticField.setup_class(enable_extra_tests=False, enable_debug_mode=False)

    test = TestAnalyticField()

    with printoptions(
        threshold=10000,
        linewidth=240,
        nanstr="nan",
        infstr="inf",
        formatter={"float": lambda x: f"{x:>6.2f}"},
    ):
        test.perform_tests()

    TestAnalyticField.teardown_class()
