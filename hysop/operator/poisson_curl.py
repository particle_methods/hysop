# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
@file poisson.py
PoissonCurl solver frontend.
"""
from hysop.constants import Implementation
from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.henum import EnumFactory
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import Field
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors

from hysop.operator.base.spectral_operator import SpectralComputationalGraphNodeFrontend
from hysop.backend.host.python.operator.poisson_curl import PythonPoissonCurl

try:
    from hysop.backend.device.opencl.operator.poisson_curl import OpenClPoissonCurl
except ImportError:
    OpenClPoissonCurl = None
try:
    from hysop.backend.host.fortran.operator.poisson_curl import FortranPoissonCurl
except ImportError:
    FortranPoissonCurl = None


class PoissonCurl(SpectralComputationalGraphNodeFrontend):
    """
    Interface the poisson solver.
    Available implementations are:
       - FORTRAN (fftw based solver)
       - PYTHON (gpyfft based solver)
       - OPENCL (clfft based solver)
    """

    @classmethod
    def implementations(cls):
        __implementations = {
            Implementation.PYTHON: PythonPoissonCurl,
        }
        if not OpenClPoissonCurl is None:
            __implementations.update(
                {
                    Implementation.OPENCL: OpenClPoissonCurl,
                }
            )
        if not FortranPoissonCurl is None:
            __implementations.update(
                {
                    Implementation.FORTRAN: FortranPoissonCurl,
                }
            )
        return __implementations

    @classmethod
    def default_implementation(cls):
        return Implementation.PYTHON

    @debug
    def __new__(
        cls, velocity, vorticity, variables, implementation=None, base_kwds=None, **kwds
    ):
        base_kwds = first_not_None(base_kwds, {})
        return super().__new__(
            cls,
            velocity=velocity,
            vorticity=vorticity,
            variables=variables,
            base_kwds=base_kwds,
            implementation=implementation,
            **kwds,
        )

    @debug
    def __init__(
        self,
        velocity,
        vorticity,
        variables,
        implementation=None,
        base_kwds=None,
        **kwds,
    ):
        """
        Initialize a PoissonCurl operator frontend for 2D or 3D
        streamfunction-vorticity formulations.

        in  = W (vorticity)
        out = U (velocity)

        Vorticity also becomes an output if projection or diffusion is enabled.

        PoissonCurl does more than just solving the Poisson equation for velocity:
          - a/ diffusion of W                        | optional step (enabled with diffusion and dt)
          - b/ projection of W (such that div(U)=0)  | optional step (enabled with projection)
          - c/ poisson solver to recover U from W

        About dimensions:
           - if velocity is a 2D vector field, vorticity should have only one component Wx.
           - if velocity is a 3D vector field, vortcitiy should have three components (Wx,Wy,Wz).

        In 2D:

           laplacian(psi) = -W
           Ux = +dpsi/dy
           Uy = -dpsi/dx

        In 3D:

           laplacian(psi) = -W
           U = rot(psi)

        Parameters
        ----------
        velocity: Field
            output continuous velocity field (all components)
        vorticity: Field
            input continuous vorticity field (all components)
        variables: dict
            dictionary of fields as keys and topologies as values.
        diffusion: ScalarParameter, optional, defaults to None.
            Diffuse the vorticity field before applying projection and poisson operators.
            If diffusion is specified, a timestep has to be specified.
        dt: ScalarParameter, optional, defaults to None
            Timestep is only required for diffusion.
            If diffusion is not enabled, this parameter is ignored.
        projection: hysop.constants.FieldProjection or positive integer, optional
            Project vorticity such that resolved velocity is divergence free (for 3D fields).
            When active, projection is done prior to every solve, unless projection is
            an integer in which case it is done every given steps.
            This parameter is ignored for 2D fields and defaults to no projection.
        implementation: Implementation, optional, defaults to None
            target implementation, should be contained in available_implementations().
            If None, implementation will be set to default_implementation().
        base_kwds: dict, optional, defaults to None
            Base class keywords arguments.
            If None, an empty dict will be passed.
        kwds:
            Keywords arguments that will be passed towards implementation
            poisson operator __init__.

        Notes
        -----
        A PoissonCurl operator implementation should at least support
        the following __init__ attributes: velocity, vorticity, variables
        """
        base_kwds = first_not_None(base_kwds, {})

        check_instance(velocity, Field)
        check_instance(vorticity, Field)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(base_kwds, dict, keys=str)

        dim = velocity.domain.dim
        vcomp = velocity.nb_components
        wcomp = vorticity.nb_components

        if velocity.domain != vorticity.domain:
            msg = "Velocity and vorticity do not share the same domain."
            raise ValueError(msg)
        if dim not in (2, 3):
            msg = f"Velocity dimension should be 2 or 3, got a {dim}D vector field."
            raise ValueError(msg)
        if vcomp != dim:
            msg = "Velocity is a {}D vector field but has {} components."
            msg = msg.format(dim, vcomp)
            raise ValueError(msg)
        if (dim == 2) and (wcomp != 1):
            msg = (
                f"Vorticity component mistmach, got {wcomp} components but expected 1."
            )
            raise RuntimeError(msg)
        if (dim == 3) and (wcomp != 3):
            msg = (
                f"Vorticity component mistmach, got {wcomp} components but expected 3."
            )
            raise RuntimeError(msg)

        if "nu" in kwds:
            msg = "Diffusion is enabled with the 'diffusion' parameter, not 'nu'."
            raise ValueError(msg)

        super().__init__(
            velocity=velocity,
            vorticity=vorticity,
            variables=variables,
            base_kwds=base_kwds,
            implementation=implementation,
            **kwds,
        )
