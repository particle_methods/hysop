# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
@file enstrophy.py
Enstrophy solver frontend.
"""
from hysop.constants import Implementation
from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.henum import EnumFactory
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import Field
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.core.graph.computational_node_frontend import ComputationalGraphNodeFrontend
from hysop.parameters.scalar_parameter import ScalarParameter, TensorParameter
from hysop.parameters.default_parameters import VolumicIntegrationParameter


class Integrate(ComputationalGraphNodeFrontend):
    """
    Interface for integrating fields on their domain
    Available implementations are:
        *OPENCL (gpu based implementation)
    """

    @classmethod
    def implementations(cls):
        from hysop.backend.device.opencl.operator.integrate import OpenClIntegrate
        from hysop.backend.host.python.operator.integrate import PythonIntegrate

        implementations = {
            Implementation.OPENCL: OpenClIntegrate,
            Implementation.PYTHON: PythonIntegrate,
        }
        return implementations

    @classmethod
    def default_implementation(cls):
        return Implementation.PYTHON

    @debug
    def __new__(
        cls,
        field,
        variables,
        parameter=None,
        scaling=None,
        base_kwds=None,
        expr=None,
        **kwds,
    ):
        base_kwds = first_not_None(base_kwds, {})
        return super().__new__(
            cls,
            field=field,
            variables=variables,
            parameter=parameter,
            scaling=scaling,
            expr=expr,
            base_kwds=base_kwds,
            **kwds,
        )

    @debug
    def __init__(
        self,
        field,
        variables,
        parameter=None,
        scaling=None,
        base_kwds=None,
        expr=None,
        **kwds,
    ):
        """
        Initialize a Integrate operator frontend.

        Integrate a field on it compute domain and put the result in a parameter.

        in:  field
            Possibly as multi-component field that should be integrated.
        out: parameter
             P = scaling * integral_V(field)
             where V is the field domain volume
             and scaling depends on specified scaling method.

        parameter
        ----------
        field: Field
            Input continuous field to be integrated.
        variables: dict
            dictionary of fields as keys and topologies as values.
        parameter: ScalarParameter or TensorParameter
            The output parameter that will contain the integral.
            Should match field.nb_components.
            A default parameter will be created if not specified.
        scaling: None, float, str or array-like of str, optional
            Scaling method used after integration.
            'volumic':   scale by domain size (product of mesh space steps)
            'normalize': scale by first integration (first value will be 1.0)
            Defaults to volumic integration.
        expr: None, str, optional
            expression performed on each entry of the array before sum, elements are referenced as `x[i]`
        implementation: Implementation, optional, defaults to None
            target implementation, should be contained in available_implementations().
            If None, implementation will be set to default_implementation().
        base_kwds: dict, optional, defaults to None
            Base class keywords arguments.
            If None, an empty dict will be passed.
        kwds:
            Extra keywords arguments that will be passed towards implementation
            enstrophy operator __init__.

        Notes
        -----
        An Integrate operator implementation should at least support
        the hysop.operator.base.integrate.IntegrateBase interface.
        """
        base_kwds = first_not_None(base_kwds, {})

        check_instance(field, Field)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(parameter, (ScalarParameter, TensorParameter), allow_none=True)
        check_instance(scaling, (str, float), allow_none=True)
        check_instance(expr, str, allow_none=True)
        check_instance(base_kwds, dict, keys=str)

        if expr is not None:
            assert expr.find("x[i]") >= 0, f"expression '{expr}' does not contain x[i]"

        # Pregenerate parameter so that we can directly store it in self.
        if parameter is None:
            parameter = VolumicIntegrationParameter(field=field)
        if parameter.size != field.nb_components:
            msg = "Expected a parameter of size {} but got a parameter of size {}."
            msg = msg.format(field.nb_components, parameter.size)
            raise RuntimeError(msg)

        super().__init__(
            field=field,
            variables=variables,
            parameter=parameter,
            scaling=scaling,
            expr=expr,
            base_kwds=base_kwds,
            **kwds,
        )
