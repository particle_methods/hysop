# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
@file stretching_dir.py
Directional stretching frontend (operator generator).
"""
import sympy as sm

from hysop.constants import DirectionLabels, Implementation, StretchingFormulation
from hysop.tools.htypes import check_instance, to_tuple, to_list, first_not_None
from hysop.tools.decorators import debug
from hysop.tools.numpywrappers import npw
from hysop.parameters.scalar_parameter import ScalarParameter
from hysop.core.graph.graph import generated
from hysop.fields.continuous_field import Field
from hysop.topology.topology import Topology
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.operator.directional.stretching_dir import DirectionalStretching
from hysop.symbolic.relational import Assignment


class DirectionalStretchingDiffusion(DirectionalStretching):
    """
    Directional stretching + diffusion using the symbolic code generation framework.
    """

    @debug
    def __new__(cls, viscosity, **kwds):
        return super().__new__(cls, **kwds)

    @debug
    def __init__(self, viscosity, **kwds):
        """
        Initialize directional stretching + diffusion operator frontend.

        Solves dW/dt = C * f(U,W) + viscosity * laplacian(W)
        where U is the velocity, W the vorticity and f is one of the following formulation:
            CONSERVATIVE   FORMULATION: f(U,W) = div( U : W )
            MIXED GRADIENT FORMULATION: f(U,W) = (A*grad(U) + (1-A)*grad(U)^T) . W

        where : represents the outer product U:W = (Ui*Wj)ij.
              . represents the matrix-vector product.
             ^T is the transposition operator

        U and W are always three dimensional fields.
        C is scalar or 3d vector-like of symbolic coefficients.
        A is a scalar symbolic coefficient.
        viscosity is a scalar.
        f(U,W) is always directionally splittable.

        Parameters
        ----------
        viscosity: scalar
            Value of the diffusion coefficient.
        kwds:
            Base class keywords arguments.
            See hysop.operator.directional.stretching_dir.DirectionalStretching.
        """
        check_instance(viscosity, (float, Field))
        self.viscosity = viscosity
        super().__init__(**kwds)

    def _gen_expressions(self, formulation, velocity, vorticity, C, A):
        from hysop.symbolic.field import laplacian

        viscosity = self.viscosity
        if isinstance(viscosity, Field):
            viscosity = viscosity.s(*viscosity.domain.frame.vars)
        _exprs = super()._gen_expressions(
            formulation=formulation, velocity=velocity, vorticity=vorticity, C=C, A=A
        )
        exprs = ()
        d2W = laplacian(vorticity.s(), vorticity.domain.frame)
        for i, e in enumerate(_exprs):
            lhs, rhs = e.args
            rhs += viscosity * d2W[i]
            e = type(e)(lhs, rhs)
            exprs += (e,)
        return exprs
