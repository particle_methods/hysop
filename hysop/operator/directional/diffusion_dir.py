# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
@file diffusion_dir.py
Directional diffusion frontend (operator generator).
"""
import sympy as sm

from hysop.constants import DirectionLabels, Implementation
from hysop.tools.htypes import check_instance, to_tuple, to_list, first_not_None
from hysop.tools.decorators import debug
from hysop.tools.numpywrappers import npw
from hysop.core.graph.graph import generated
from hysop.fields.continuous_field import Field
from hysop.parameters.parameter import Parameter
from hysop.topology.topology import Topology
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.operator.directional.symbolic_dir import DirectionalSymbolic
from hysop.symbolic.relational import Assignment


class DirectionalDiffusion(DirectionalSymbolic):
    """
    Directional diffusion using the symbolic code generation framework.
    """

    @debug
    def __new__(
        cls,
        fields,
        coeffs,
        variables,
        dt,
        laplacian_formulation=True,
        name=None,
        implementation=None,
        base_kwds=None,
        **kwds,
    ):
        base_kwds = first_not_None(base_kwds, {})
        return super().__new__(
            cls,
            name=name,
            variables=variables,
            dt=dt,
            base_kwds=base_kwds,
            implementation=implementation,
            exprs=None,
            candidate_input_tensors=None,
            candidate_output_tensors=None,
            **kwds,
        )

    @debug
    def __init__(
        self,
        fields,
        coeffs,
        variables,
        dt,
        laplacian_formulation=True,
        name=None,
        implementation=None,
        base_kwds=None,
        **kwds,
    ):
        """
        Initialize directional diffusion frontend.

        Diffusion is the net movement of regions of high concentration to neighbour regions of lower
        concentration, ie the evolution of a field down its gradient.

        Solves
               dFi/dt = Di * laplacian(Fi)
            or
               dFi/dt = div( Di * grad(Fi) )

        for multiple fields Fi, where * represents elementwise multiplication,
        using a given time integrator, inplace.

        Di can be a scalar, a vector or a matrix, see Notes.

        Parameters
        ----------
        fields: array like of continuous fields.
            The fields Fi that will be diffused.
        coeffs: array like of coefficients
            The diffusion coefficients Di can be scalar or tensor like (ie. possibly anisotropic).
            Contained values should be numerical coefficients, parameters or symbolic
            expressions such that Di*grad(Fi) is directionally splittable.
        variables: dict
            Dictionary of fields as keys and topology descriptors as values.
        dt: ScalarParameter
            Timestep parameter that will be used for time integration.
        laplacian_formulation: bool, optional, defaults to True
            Use the Di*laplacian(Fi) formulation instead of the div(Di*grad(Fi)) formulation.
        name: str, optional
            Name of this diffusion operator, defaults to 'diffusion'.
        implementation: Implementation, optional, defaults to None
            Target implementation, should be contained in available_implementations().
            If None, implementation will be set to default_implementation().
        base_kwds: dict, optional, defaults to None
            Base class keywords arguments.
            If None, an empty dict will be passed.
        kwds:
            Keywords arguments that will be passed towards implementation
            operator __init__.

        Notes
        -----
        Solves dFi/dt = div( Di * grad(Fi) ) with the following notations:

        Fi = (Fi0(x0,...,xMi),
              Fi1(x0,...,xMi),
                    ...
              FiNi(x0,...,xMi))

        Mi = Fi.dim - 1
        Ni = Fi.nb_components - 1

        grad(Fi) = (dFi0/dx0, ..., dFi0/dxM),
                   (dFi1/dx0, ..., dFi1/dxM),
                              ...
                   (dFiNi/dx0, ..., dFiNi/dxM)

             (d00,  ...,  d0Ni)
             ( .     .    .   )
        Di = ( .     .    .   )   or  (d0, ..., dN)  or  d0
             ( .     .    .   )
             (dNi0, ..., dNiNi)

        if Di.ndim is 1, Di[:,None] will be used ie:

             (d0,  ...,  d0)
             ( .    .    . )
        Di = ( .    .    . )
             ( .    .    . )
             (dNi, ..., dNi)

        if Di.ndim is 0, Di[None,None] will be used ie:

             (d0, ..., d0)
             (.    .    .)
        Di = (.    .    .)
             (.    .    .)
             (d0, ..., d0)
        """

        name = first_not_None(name, "diffusion")
        base_kwds = first_not_None(base_kwds, {})

        if isinstance(coeffs, npw.ndarray):
            coeffs = (coeffs,)
        elif isinstance(coeffs, Parameter):
            coeffs = (coeffs.s,)
        elif isinstance(coeffs, Field):
            coeffs = (coeffs.s(*coeffs.domain.frame.vars),)

        fields = to_tuple(fields)
        coeffs = to_tuple(coeffs)

        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(base_kwds, dict, keys=str)
        check_instance(name, str)
        check_instance(fields, (tuple,), values=Field)
        check_instance(
            coeffs,
            (tuple,),
            values=(int, float, npw.ndarray, sm.Basic),
            size=len(fields),
        )
        check_instance(laplacian_formulation, bool)

        exprs = self._gen_expressions(fields, coeffs, laplacian_formulation)

        super().__init__(
            name=name,
            variables=variables,
            dt=dt,
            base_kwds=base_kwds,
            implementation=implementation,
            exprs=exprs,
            candidate_input_tensors=fields,
            candidate_output_tensors=fields,
            **kwds,
        )

        from hysop.operator.base.custom_symbolic_operator import (
            CustomSymbolicOperatorBase,
        )

        if not issubclass(self._operator, CustomSymbolicOperatorBase):
            msg = "Class {} does not inherit from the directional operator interface "
            msg += "({})."
            msg = msg.format(self._operator, CustomSymbolicOperatorBase)
            raise TypeError(msg)

        self.coeffs = coeffs

    def _gen_expressions(self, fields, coeffs, laplacian_formulation):
        from hysop.symbolic.field import div, grad, laplacian

        exprs = ()
        for field, coeff in zip(fields, coeffs):
            frame = field.domain.frame
            fs = field.s()
            lhs = fs.diff(frame.time)
            if laplacian_formulation:
                if isinstance(coeff, npw.ndarray):
                    assert coeff.ndim == 1, coeff
                    assert coeff.size == frame.dim, coeff
                rhs = coeff * laplacian(fs, frame)
            else:
                if isinstance(coeff, npw.ndarray) and (coeff.ndim == 1):
                    coeff = coeff[:, None]
                rhs = div(coeff * grad(fs, frame), frame)
            exprs += Assignment.assign(lhs, rhs)
        return exprs
