# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
@file symbolic_dir.py
Directional symbolic frontends (operator generator).
"""

from hysop.constants import DirectionLabels, Implementation
from hysop.tools.htypes import check_instance, to_tuple, to_list, first_not_None
from hysop.tools.decorators import debug
from hysop.core.graph.graph import generated
from hysop.fields.continuous_field import Field
from hysop.topology.topology import Topology
from hysop.operator.directional.directional import DirectionalOperatorFrontend
from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.operator.base.custom_symbolic_operator import CustomSymbolicOperatorBase
from hysop.symbolic.relational import Assignment
from hysop.testsenv import __HAS_OPENCL_BACKEND__


class DirectionalSymbolic(DirectionalOperatorFrontend):
    """
    Custom symbolic expression directional splitting.
    Available implementations are:
        *OpenCL
    """

    @classmethod
    def implementations(cls):
        if __HAS_OPENCL_BACKEND__:
            from hysop.backend.device.opencl.operator.custom_symbolic import (
                OpenClCustomSymbolicOperator,
            )

            __implementations = {Implementation.OPENCL: OpenClCustomSymbolicOperator}
            return __implementations
        return dict()

    @classmethod
    def default_implementation(cls):
        return Implementation.OPENCL

    @debug
    def __new__(
        cls,
        name,
        exprs,
        variables,
        no_split=False,
        fixed_residue=0,
        force_residue=None,
        implementation=None,
        base_kwds=None,
        candidate_input_tensors=None,
        candidate_output_tensors=None,
        **kwds,
    ):
        base_kwds = first_not_None(base_kwds, {})
        return super().__new__(
            cls,
            name=name,
            variables=variables,
            base_kwds=base_kwds,
            implementation=implementation,
            candidate_input_tensors=candidate_input_tensors,
            candidate_output_tensors=candidate_output_tensors,
            **kwds,
        )

    @debug
    def __init__(
        self,
        name,
        exprs,
        variables,
        no_split=False,
        fixed_residue=0,
        force_residue=None,
        implementation=None,
        base_kwds=None,
        candidate_input_tensors=None,
        candidate_output_tensors=None,
        **kwds,
    ):
        """
        Initialize a DirectionalSymbolic operator frontend.

        Expressions are first splitted using contiguous memory
        accesses imposed by the discretization of field derivatives
        with finite differences.

        If the splitting is not possible (ie. an expression contains
        multiple derivatives in different directions that cannot be
        splitted), a ValueError is raised.

        See hysop.operator.base.custom_symbolic.CustomSymbolicOperatorBase
        to see how expressions are parsed.

        Parameters
        ----------
        name: str
            Name of this custom operator.
        exprs: hysop.symbolic.exprs or array like of valid expresions
            Expressions that will generate code.
            Valid expressions are defined in SymbolicExpressionParser.
        variables: dict
            dictionary of fields as keys and topology descriptors as values.
        fixed_residue: sm.Expr or array like sm.Expr
            Add a direction agnostic (unsplittable) extra residue to each right hand sides
            Defaults to 0.
        force_residue: Expr, defaults to None
            Disable residue computation and use this value instead.
            fixed_residue is added to this force_residue.
        no_split: bool, optional, defaults to False
            Skip the directional expression splitting step and broadcast expressions
            to each axes.
        implementation: Implementation, optional, defaults to None
            target implementation, should be contained in available_implementations().
            If None, implementation will be set to default_implementation().
        base_kwds: dict, optional, defaults to None
            Base class keywords arguments.
            If None, an empty dict will be passed.
        kwds:
            Keywords arguments that will be passed towards implementation
            operator __init__.

        Notes
        -----
        A valid implementation should at least support the
        hysop.base.custom_symbolic.CustomSymbolicOperatorBase interface.
        """
        base_kwds = first_not_None(base_kwds, {})
        exprs = to_tuple(exprs)

        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(base_kwds, dict, keys=str)
        check_instance(exprs, tuple, values=Assignment)

        fixed_residue = to_tuple(first_not_None(fixed_residue, 0))
        if len(fixed_residue) == 1:
            fixed_residue *= len(exprs)
        check_instance(fixed_residue, tuple, size=len(exprs))

        force_residue = to_tuple(force_residue)
        if len(force_residue) == 1:
            force_residue *= len(exprs)
        check_instance(force_residue, tuple, size=len(exprs))

        candidate_input_tensors = first_not_None(
            candidate_input_tensors, variables.keys()
        )
        candidate_output_tensors = first_not_None(
            candidate_output_tensors, variables.keys()
        )

        super().__init__(
            name=name,
            variables=variables,
            base_kwds=base_kwds,
            implementation=implementation,
            candidate_input_tensors=candidate_input_tensors,
            candidate_output_tensors=candidate_output_tensors,
            **kwds,
        )

        if not issubclass(self._operator, CustomSymbolicOperatorBase):
            msg = "Class {} does not inherit from the directional operator interface "
            msg += "({})."
            msg = msg.format(self._operator, CustomSymbolicOperatorBase)
            raise TypeError(msg)

        self._split_expression(exprs, no_split, fixed_residue, force_residue)

    def _split_expression(self, exprs, no_split, fixed_residue, force_residue):
        from hysop.symbolic.directional import split_assignement

        if no_split:
            directional_exprs = exprs
        else:
            directional_exprs = {}
            for e, fixed_res, forced_res in zip(exprs, fixed_residue, force_residue):
                se = split_assignement(e, fixed_res, forced_res, None)
                for k, v in se.items():
                    if v == 0:
                        continue
                    if k in directional_exprs:
                        directional_exprs[k] += (v,)
                    else:
                        directional_exprs[k] = (v,)
        self._directional_exprs = directional_exprs
        self._no_split = no_split

    @generated
    def generate_direction(self, i, dt_coeff):
        """Generate only directions were expression is non zero."""
        if (self._no_split) or (i in self._directional_exprs):
            return super().generate_direction(i=i, dt_coeff=dt_coeff)
        else:
            return None

    def custom_directional_kwds(self, i):
        """Specify custom keyword arguments that will be passed only for direction i."""
        kwds = super().custom_directional_kwds(i)
        if self._no_split:
            kwds["exprs"] = tuple(self._directional_exprs)
        else:
            kwds["exprs"] = self._directional_exprs.get(i, {})
        return kwds
