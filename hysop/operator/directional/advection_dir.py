# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
@file advection_dir.py
Directional advection frontends (operator generator).
"""

import itertools as it
from hysop.constants import DirectionLabels, Implementation
from hysop.tools.htypes import check_instance, to_tuple, to_list, first_not_None
from hysop.tools.decorators import debug
from hysop.fields.continuous_field import Field
from hysop.topology.topology import Topology
from hysop.operator.directional.directional import DirectionalOperatorFrontend
from hysop.parameters.scalar_parameter import ScalarParameter

from hysop.topology.cartesian_descriptor import CartesianTopologyDescriptors
from hysop.testsenv import __HAS_OPENCL_BACKEND__


class DirectionalAdvection(DirectionalOperatorFrontend):
    """
    Interface for particle advection and remeshing method of a list
    of fields in a given direction.

    Available implementations are:
        *Python
        *OpenCL
    """

    @classmethod
    def implementations(cls):
        from hysop.backend.host.python.operator.directional.advection_dir import (
            PythonDirectionalAdvection,
        )

        _implementations = {
            Implementation.PYTHON: PythonDirectionalAdvection,
        }
        if __HAS_OPENCL_BACKEND__:
            from hysop.backend.device.opencl.operator.directional.advection_dir import (
                OpenClDirectionalAdvection,
            )

            _implementations.update({Implementation.OPENCL: OpenClDirectionalAdvection})
        return _implementations

    @classmethod
    def default_implementation(cls):
        return Implementation.PYTHON

    @debug
    def __new__(
        cls,
        velocity,
        advected_fields,
        variables,
        dt,
        advected_fields_out=None,
        relative_velocity=None,
        implementation=None,
        base_kwds=None,
        **kwds,
    ):
        return super().__new__(
            cls,
            velocity=velocity,
            relative_velocity=relative_velocity,
            dt=dt,
            advected_fields_in=advected_fields,
            advected_fields_out=advected_fields_out,
            variables=variables,
            implementation=implementation,
            base_kwds=base_kwds,
            candidate_input_tensors=None,
            candidate_output_tensors=None,
            **kwds,
        )

    @debug
    def __init__(
        self,
        velocity,
        advected_fields,
        variables,
        dt,
        advected_fields_out=None,
        relative_velocity=None,
        implementation=None,
        base_kwds=None,
        **kwds,
    ):
        """
        Initialize a DirectionalAdvectionFrontend.

        Parameters
        ----------
        velocity: Field
            continuous velocity field (all components)
        advected_fields: Field or array like of Fields
            instance or list of continuous fields to be advected.
        advected_fields_out: Field or array like of Field, optional, defaults to None
            advection output, if set to None, advection is done inplace
            on a per variable basis.
        relative_velocity: array_like of constants representing relative velocity, optional
            Relative velocity that has to be taken into account.
            Vi = Ui - Urel[i] for each components.
        variables: dict
            Dictionary of continuous fields as keys and topologies as values.
        dt: ScalarParameter
            Timestep parameter that will be used for time integration.
        implementation: implementation, optional, defaults to None
            target implementation, should be contained in available_implementations().
            If None, implementation will be set to default_implementation().
        base_kwds: dict, optional, defaults to None
            Base class keywords arguments.
            If None, an empty dict will be passed.
        kwds:
            Extra parameters passed to generated directional operators.

        Notes
        -----
        An implementation should at least support the following __init__ parameters:
            velocity, advected_fields, advected_fields_out, variables
            direction, splitting_dim
        Extra keywords parameters are passed through kwds.
        """
        base_kwds = first_not_None(base_kwds, {})
        check_instance(velocity, Field)
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(base_kwds, dict, keys=str)
        check_instance(dt, ScalarParameter)

        advected_fields = to_list(advected_fields)
        nb_fields = len(advected_fields)
        assert len(set(advected_fields)) == len(advected_fields)

        relative_velocity = to_list(first_not_None(relative_velocity, [0]))
        relative_velocity = tuple(
            map(lambda x: x if isinstance(x, str) else float(x), relative_velocity)
        )
        if len(relative_velocity) == 1:
            relative_velocity *= velocity.nb_components
        relative_velocity = tuple(relative_velocity)

        if advected_fields_out is None:
            advected_fields_out = advected_fields
        else:
            advected_fields_out = to_list(advected_fields_out)
            assert len(advected_fields) == len(advected_fields_out)
            assert len(set(advected_fields_out)) == len(advected_fields_out)
            for i, field in enumerate(advected_fields_out):
                if field is None:
                    advected_fields_out[i] = advected_fields[i]
        advected_fields = tuple(advected_fields)
        advected_fields_out = tuple(advected_fields_out)

        check_instance(advected_fields, tuple, values=Field)
        check_instance(
            advected_fields_out, tuple, values=Field, size=len(advected_fields)
        )
        check_instance(velocity, Field)
        check_instance(
            relative_velocity, tuple, values=(str, float), size=velocity.nb_components
        )
        check_instance(variables, dict, keys=Field, values=CartesianTopologyDescriptors)
        check_instance(base_kwds, dict, keys=str)
        check_instance(dt, ScalarParameter)

        candidate_input_tensors = advected_fields + (velocity,)
        candidate_output_tensors = advected_fields_out

        super().__init__(
            velocity=velocity,
            relative_velocity=relative_velocity,
            dt=dt,
            advected_fields_in=advected_fields,
            advected_fields_out=advected_fields_out,
            variables=variables,
            implementation=implementation,
            base_kwds=base_kwds,
            candidate_input_tensors=candidate_input_tensors,
            candidate_output_tensors=candidate_output_tensors,
            **kwds,
        )
