# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sympy as sm
from hysop.tools.numerics import is_complex


class ComplexExpr(sm.Expr):
    pass


class ComplexUnaryExpr(ComplexExpr):
    def __new__(cls, a, **kwds):
        obj = super().__new__(cls, a, **kwds)
        obj.a = a
        return obj

    def __repr__(self):
        return f"{self.__class__.__name__}({repr(self.a)})"

    def __str__(self):
        return f"{self.fname}({self.a})"

    def _sympystr(self, printer):
        return f"{self.fname}({printer._print(self.a)})"


class ComplexBinaryExpr(ComplexExpr):
    def __new__(cls, lhs, rhs, **kwds):
        obj = super().__new__(cls, lhs, rhs, **kwds)
        obj.lhs, obj.rhs = lhs, rhs
        return obj

    def __repr__(self):
        return f"{self.__class__.__name__}({repr(self.lhs)},{repr(self.rhs)})"

    def __str__(self):
        return f"{self.fname}({self.rhs},{self.rhs})"

    def _sympystr(self, printer):
        return f"{self.fname}({printer._print(self.lhs)},{printer._print(self.rhs)})"


class ComplexMul(ComplexBinaryExpr):
    """Tag for complex multiplication."""

    fname = "cmul"


class ComplexModulus(ComplexUnaryExpr):
    """Tag for complex modulus."""

    fname = "cabs"


class ComplexModulus2(ComplexUnaryExpr):
    """Tag for squared complex modulus."""

    fname = "cabs2"


def cabs(a):
    return ComplexModulus(a) if is_complex(a) else sm.Abs(a)


def cabs2(a):
    return ComplexModulus2(a) if is_complex(a) else a * a
