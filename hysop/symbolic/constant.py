# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.numpywrappers import npw
from hysop.symbolic.base import DummySymbolicScalar, sm


class SymbolicConstant(DummySymbolicScalar):
    """Temporary constant variables."""

    def __new__(cls, name, **kwds):
        if "dtype" not in kwds:
            msg = "dtype has not been specified for SymbolicConstant {}."
            msg = msg.format(name)
            raise RuntimeError(msg)
        dtype = kwds.pop("dtype")
        value = kwds.pop("value", None)
        obj = super().__new__(cls, name=name, **kwds)
        obj._dtype = dtype
        obj._value = None
        if value is not None:
            obj.bind_value(value)
        return obj

    @property
    def is_bound(self):
        return self._value is not None

    def assert_bound(self):
        if not self.is_bound:
            msg = "{}::{} value has not been bound yet."
            msg = msg.format(self.__class__.__name__, self.name)
            raise RuntimeError(msg)

    def bind_value(self, value, force=False):
        if (not force) and (self._value is not None):
            msg = f"A value has already been bound to SymbolicConstant {self.name}."
            raise RuntimeError(msg)
        if isinstance(value, npw.ndarray):
            assert value.size == 1, value.size
            value = value.item()
        value = self._dtype(value)
        self._value = value
        return self

    @property
    def value(self):
        self.assert_bound()
        return self._value

    @property
    def dtype(self):
        self.assert_bound()
        return self._dtype

    @property
    def ctype(self):
        from hysop.backend.device.codegen.base.variables import dtype_to_ctype

        self.assert_bound()
        return dtype_to_ctype(self._dtype)

    def short_description(self):
        self.assert_bound()
        return "{}[dtype={}, ctype={}]".format(
            self.__class__.__name__, self.dtype, self.ctype
        )

    def __eq__(self, other):
        return id(self) == id(other)

    def __hash__(self):
        return id(self)

    def _hashable_content(self):
        """See sympy.core.basic.Basic._hashable_content()"""
        hc = super()._hashable_content()
        hc += (str(id(self)),)
        return hc
