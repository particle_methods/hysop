# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.symbolic.base import SymbolicTensor, SymbolicScalar
from hysop.tools.htypes import check_instance, to_tuple, first_not_None
from hysop.tools.numpywrappers import npw


class SymbolicTensorParameter(SymbolicTensor):
    def __new__(
        cls,
        parameter,
        name=None,
        pretty_name=None,
        value=None,
        shape=None,
        scalar_cls=None,
        **kwds,
    ):
        from hysop.parameters.tensor_parameter import TensorParameter

        check_instance(parameter, TensorParameter)
        value = first_not_None(value, parameter)
        shape = first_not_None(shape, parameter.shape)
        name = first_not_None(name, parameter.name)
        pname = first_not_None(pretty_name, parameter.pretty_name)
        scalar_cls = first_not_None(scalar_cls, SymbolicScalarParameter)
        scalar_kwds = dict(parameter=parameter)

        def make_scalar_kwds(idx):
            return dict(view=lambda parameter: parameter._value[idx])

        return super().__new__(
            cls,
            name=name,
            pretty_name=pname,
            shape=shape,
            value=value,
            scalar_cls=scalar_cls,
            make_scalar_kwds=make_scalar_kwds,
            scalar_kwds=scalar_kwds,
            **kwds,
        )

    def __init__(
        self,
        parameter,
        name=None,
        pretty_name=None,
        value=None,
        shape=None,
        scalar_cls=None,
        **kwds,
    ):
        super().__init__(
            name=name,
            pretty_name=pretty_name,
            shape=shape,
            value=value,
            scalar_cls=scalar_cls,
            make_scalar_kwds=None,
            scalar_kwds=None,
            **kwds,
        )


class SymbolicScalarParameter(SymbolicScalar):
    def __new__(cls, parameter, name=None, pretty_name=None, value=None, **kwds):
        from hysop.parameters.tensor_parameter import TensorParameter

        check_instance(parameter, TensorParameter)
        value = first_not_None(value, parameter._value)
        name = first_not_None(name, parameter.name)
        pname = first_not_None(pretty_name, parameter.pretty_name)
        obj = super().__new__(cls, name=name, pretty_name=pname, value=value, **kwds)
        obj.parameter = parameter
        return obj

    def __init__(self, parameter, name=None, pretty_name=None, value=None, **kwds):
        super().__init__(name=name, pretty_name=pretty_name, value=value, **kwds)


if __name__ == "__main__":
    import sympy as sm
    from hysop.parameters.tensor_parameter import TensorParameter
    from hysop.parameters.scalar_parameter import ScalarParameter

    a = ScalarParameter("A", dtype=npw.int32, initial_value=4)
    b = TensorParameter("B", shape=(3,), dtype=npw.int32, initial_value=8)
    c = TensorParameter("C", shape=(3, 3), dtype=npw.float32, initial_value=12)
    A = a.s
    B = b.s
    C = c.s
    D = C.copy()
    with D.write_context():
        D[0, 0] = B[0]
        D[1, 1] = B[1]
        D[2, 2] = B[2]

    print(A)
    print(B)
    print(C)
    print(D)
    print()
    print(A.vreplace())
    print(B.vreplace())
    print(C.vreplace())
    print(D.vreplace())
    print()
    a.set_value(1)
    b.set_value(npw.asarray([-1, 0, 1], dtype=b.dtype))
    c.set_value(npw.full_like(c.value, 3))
    print(A)
    print(B)
    print(C)
    print(D)
    print()
    print(A.vreplace())
    print(B.vreplace())
    print(C.vreplace())
    print(D.vreplace())
    print()
