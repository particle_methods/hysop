# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.tools.htypes import first_not_None, check_instance, to_tuple
from hysop.symbolic import (
    dspace_symbols,
    space_symbols,
    freq_symbols,
    time_symbol,
    dtime_symbol,
)


class SymbolicFrame:
    """n-dimensional symbolic frame."""

    def __init__(self, dim, freq_axes=None, **kwds):
        """Initialize a frame with given dimension."""
        super().__init__(**kwds)
        assert dim > 0, "Incompatible dimension."

        coords = list(space_symbols[:dim])
        if freq_axes is not None:
            freq_axes = to_tuple(freq_axes)
            for i in freq_axes:
                coords[dim - i - 1] = freq_symbols[dim - 1 - i]
        self._coords = tuple(coords)
        self._dim = dim

    @property
    def dim(self):
        """Get the dimension of this frame."""
        return self._dim

    @property
    def coords(self):
        """Return the symbolic spatial coordinates associated to this frame."""
        return self._coords

    @property
    def freqs(self):
        """Return the symbolic (spatial) frequency coordinates associated to this frame."""
        return freq_symbols[: self.dim]

    @property
    def dcoords(self):
        """Return the spatial coordinates infinitesimals associated to this frame."""
        return dspace_symbols[: self.dim]

    @property
    def time(self):
        """Get the time variable for conveniance."""
        return time_symbol

    @property
    def dtime(self):
        """Get the infinitesimal time variable for conveniance."""
        return dtime_symbol

    @property
    def vars(self):
        return self.coords + (self.time,)

    def __getitem__(self, key):
        """Extract coords variables with given key slice."""
        return self.coords[key]

    def __str__(self):
        ss = "SymbolicFrame:"
        ss += "\n  *dim:     {}"
        ss += "\n  *coords:  {}"
        ss += "\n  *dcoords: {}"
        ss += "\n  *vars:    {}"
        ss = ss.format(self.dim, self.coords, self.dcoords, self.vars)
        return ss


if __name__ == "__main__":
    A = SymbolicFrame(8)
    print(A.dim)
    print(A.coords)
    print(A[3])
    print(A.time)
    print(A)
