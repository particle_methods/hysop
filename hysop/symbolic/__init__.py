# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sympy as sm
from hysop.tools.sympy_utils import Symbol, Dummy, Expr, AppliedUndef, UndefinedFunction
from hysop.tools.sympy_utils import subscript, subscripts, xsymbol, freq_symbol


class TimeSymbol(Dummy):
    """Tag for space symbols."""

    pass


class SpaceSymbol(Dummy):
    """Tag for space symbols."""

    pass


time_symbol = TimeSymbol("t")
"""Dummy symbol representing time."""

dtime_symbol = TimeSymbol("dt")
"""Dummy symbol representing infinitesimal time."""

space_symbols = tuple(
    SpaceSymbol(
        name=f"x{i}",
        pretty_name=xsymbol + subscript(i),
        var_name=f"x{i}",
        latex_name=f"x_{{{i}}}",
    )
    for i in range(16)
)
"""Dummy symbols representing space."""

freq_symbols = tuple(
    SpaceSymbol(
        name=f"nu{i}",
        pretty_name=freq_symbol + subscript(i),
        var_name=f"nu{i}",
        latex_name=f"{{\nu}}_{{{i}}}",
    )
    for i in range(16)
)
"""Dummy symbols representing wave numbers."""

dspace_symbols = tuple(
    SpaceSymbol(
        name=f"dx_{i}",
        pretty_name="d" + xsymbol + subscript(i),
        var_name=f"dx_{i}",
        latex_name=f"dx_{{{i}}}",
    )
    for i in range(16)
)
"""Dummy symbols representing space infinitesimals."""

local_indices_symbols = tuple(
    SpaceSymbol(
        name=f"i{i}",
        pretty_name="i" + subscript(i),
        var_name=f"i{i}",
        latex_name=f"i_{{{i}}}",
    )
    for i in range(16)
)
"""Dummy symbols local array indices."""

global_indices_symbols = tuple(
    SpaceSymbol(
        name=f"I{i}",
        pretty_name="I" + subscript(i),
        var_name=f"I{i}",
        latex_name=f"I_{{{i}}}",
    )
    for i in range(16)
)
"""Dummy symbols global array indices."""
