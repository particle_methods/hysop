# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.symbolic.base import SymbolicScalar


class TmpScalar(SymbolicScalar):
    """Temporary scalar variables."""

    def __new__(cls, *args, **kwds):
        if "dtype" not in kwds:
            msg = "dtype has not been specified for TmpScalar {}."
            msg = msg.format(kwds.get("name", "unknown"))
            raise RuntimeError(msg)
        dtype = kwds.pop("dtype")
        obj = super().__new__(cls, *args, **kwds)
        obj._dtype = dtype
        return obj

    @property
    def dtype(self):
        return self._dtype

    @property
    def ctype(self):
        from hysop.backend.device.codegen.base.variables import dtype_to_ctype

        return dtype_to_ctype(self._dtype)
