!> Select float precision for the whole code.
!! This is a generated file, do not edit.
!! Usage :
!! cmake -DDOUBLEPREC=ON ...
!! with value = simple or value = double
module parameters

  use, intrinsic :: iso_c_binding
  use precision
  implicit none

  !> alias to 1. in wp
  real(kind=wp), parameter :: ONE = 1.
  !> alias to 0. in wp
  real(kind=wp), parameter :: ZERO = 0.

  !>  trick to identify coordinates in a more user-friendly way
  integer, parameter :: c_X=1,c_Y=2,c_Z=3

  !> MPI main communicator
  integer :: main_comm

  !> Rank of the mpi current process
  integer :: rank ! current mpi-processus rank

  !> i (sqrt(-1) ...)
! NG  complex(C_DOUBLE_COMPLEX), parameter :: Icmplx = cmplx(zero, one, kind=wp)
  complex(dp), parameter :: Icmplx = cmplx(zero, one, kind=wp)
  
  !> Pi constant
  real(wp), parameter :: pi = 4.0*atan(one)

end module parameters
