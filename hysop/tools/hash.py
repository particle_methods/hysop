# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import hashlib
import numpy as np


def hash_communicator(comm, h=None):
    from hysop.core.mpi import processor_hash

    phashes = comm.allgather(processor_hash)
    s = "{}[{}]".format(comm.Get_size(), ",".join(str(phash) for phash in phashes))
    if h is not None:
        h.update(s)
    else:
        return s


def hash_nd_array(array, h=None):
    if h is None:
        h = hashlib.sha1()
    h.update(array.view(np.uint8))
    return h.hexdigest()


def hash_id(obj, length=4):
    return hashlib.sha1(str(id(obj))).hexdigest()[:length]
