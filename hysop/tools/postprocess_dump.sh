#!/bin/bash
##
## Copyright (c) HySoP 2011-2024
##
## This file is part of HySoP software.
## See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
## for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

# Example of I/O post processing script.
# Input arguments are:
#    OP_NAME
#    ACTUAL_FILEPATH  DISK_FILEPATH
#    XMF_FILE  HDF5_FILE
#    IS_TMP  ITERATION  TIME
# See example hysop/examples/example_utils.py interface and '--dump-postprocess' argument.

set -e
if [ "$#" -ne 8 ]; then
    echo "Script expected 8 parameters."
    exit 1
fi

OP_NAME=${1}
ACTUAL_FILEPATH=${2}
DISK_FILEPATH=${3}
XMF_FILE=${4}
HDF5_FILE=${5}
IS_TMP=${6}
ITERATION=${7}
TIME=${8}

echo ">Successfully postprocessed dump '$OP_NAME', iteration ${ITERATION} at t=${TIME}."
exit 0
