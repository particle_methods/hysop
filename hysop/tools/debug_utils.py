# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import traceback, inspect
import numpy as np
import matplotlib

# matplotlib.use('Agg')

import matplotlib.pyplot as plt

# plt.ion()

from hysop.fields.discrete_field import DiscreteScalarFieldView
from hysop.tools.htypes import check_instance, first_not_None


class ImshowDebugger:
    def __init__(
        self, data, ntimes=1, cmap="coolwarm", enable_on_op_apply=False, **kwds
    ):

        check_instance(data, dict, keys=str)
        ndata = len(data)
        assert ndata >= 1, ndata
        assert ntimes >= 1, ntimes

        fig, _axes = plt.subplots(ndata, ntimes, **kwds)
        _axes = np.asarray(_axes).reshape(ndata, ntimes)

        axes = {}
        for i, k in enumerate(data.keys()):
            axes[k] = _axes[i]

        imgs = {}
        for k, v in data.items():
            v = self.get_data(v)
            for j in range(ntimes):
                axes[k][j].set_title(f"{k} at t=Tn-{j}")
                axes[k][j].set_xlim(0, v.shape[1] - 1)
                axes[k][j].set_ylim(0, v.shape[0] - 1)
                img = axes[k][j].imshow(
                    self.normalize(v),
                    cmap=cmap,
                    vmin=0.0,
                    vmax=1.0,
                    interpolation="bilinear",
                )
                imgs.setdefault(k, []).append(img)

        fig.canvas.mpl_connect("key_press_event", self.on_key_press)
        fig.canvas.mpl_connect("close_event", self.on_close)
        fig.suptitle("HySoP Visual Debugger v1.0")

        self.fig = fig
        self.axes = axes
        self.data = data
        self.imgs = imgs
        self.ntimes = ntimes
        self.enable_on_op_apply = enable_on_op_apply

        self.cl_queues = []
        self.running = True

        mng = plt.get_current_fig_manager()
        # mng.window.maximize()

        plt.draw()
        fig.show()

    def get_data(self, data):
        if isinstance(data, tuple):
            (field, component) = data
            check_instance(field, DiscreteScalarFieldView)
            check_instance(component, int)
            data = field.data[component].get()[field.compute_slices].handle
        check_instance(data, np.ndarray)
        return data

    def normalize(self, data):
        check_instance(data, np.ndarray)
        assert data.ndim == 2
        dmin, dmax = np.min(data), np.max(data)
        data = data.astype(np.float32)
        if (dmax - dmin) < 1e-4:
            data = np.clip(data, 0.0, 1.0)
        else:
            data = (data - dmin) / (dmax - dmin)
        return data

    def update(self):
        for queue in self.cl_queues:
            queue.flush()
            queue.finish()
        imgs = self.imgs
        ntimes = self.ntimes
        for k, data in self.data.items():
            data = self.get_data(data)
            for j in range(ntimes - 1, 0, -1):
                imgs[k][j].set_data(imgs[k][j - 1].get_array())
            imgs[k][0].set_array(self.normalize(data))
        self.fig.canvas.draw()
        plt.pause(0.01)

    def _break(self, msg=None, nostack=False):
        if not self.running:
            return
        msg = first_not_None(msg, "")
        if not nostack:
            _file, _line = inspect.stack()[2][1:3]
            msg = f"{_file}::{_line} {msg}"
        self.fig.suptitle(msg)
        self.update()
        self.blocking = True
        while self.blocking:
            plt.pause(0.01)

    def __call__(self, msg=None, nostack=False):
        self._break(msg=msg, nostack=nostack)

    def on_close(self, event):
        self.blocking = False
        self.running = False

    def on_key_press(self, event):
        key = event.key
        if key == "n":
            self.blocking = False
        elif key == "q":
            plt.close(self.fig)
            self.blocking = False
            self.running = False

    def synchronize_queue(self, queue):
        self.cl_queues.append(queue)


if __name__ == "__main__":
    import random

    shape0 = (100, 100)
    shape1 = (200, 100)
    img0 = np.zeros(shape=shape0, dtype=np.float32)
    img1 = np.ones(shape=shape1, dtype=np.float32)
    dbg = ImshowDebugger({"F0": img0, "F1": img1}, ntimes=3)
    dbg("init")
    i = 0
    while dbg.running:
        img0[...] = np.random.rand(*shape0).astype(np.float32)
        img1[...] = np.random.rand(*shape1).astype(np.float32)
        dbg(f"msg{str(i)}")
        i += 1
