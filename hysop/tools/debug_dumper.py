# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import shutil
import datetime
import inspect
import argparse
import sys
import numpy as np

from hysop import MPI
from hysop.tools.htypes import check_instance
from hysop.fields.discrete_field import DiscreteScalarFieldView


class DebugDumper:
    def __init__(
        self,
        name,
        path,
        force_overwrite=False,
        enable_on_op_apply=False,
        dump_precision=12,
        comm=MPI.COMM_WORLD,
        io_leader=0,
        dump_data=False,
    ):
        assert isinstance(name, str), name
        assert isinstance(path, str), path

        directory = os.path.join(path, name)

        blobs_directory = os.path.join(directory, "data")
        if dump_data:
            if not os.path.isdir(blobs_directory) and comm.rank == 0:
                os.makedirs(blobs_directory)
        self.blobs_directory = blobs_directory
        self.dump_data = dump_data

        self.name = name
        self.directory = directory
        self.dump_id = 0
        self.enable_on_op_apply = enable_on_op_apply
        self.dump_data = dump_data
        self.dump_precision = dump_precision

        self.comm = comm
        self.io_leader = io_leader
        self.comm_size = comm.Get_size()
        self.comm_rank = comm.Get_rank()
        assert 0 <= self.io_leader < self.comm_size
        self.is_io_leader = self.comm_rank == self.io_leader

        if self.is_io_leader:
            if os.path.exists(directory):
                if force_overwrite:
                    shutil.rmtree(directory)
                else:
                    msg = f"Directory '{directory}' already exists."
                    raise RuntimeError(msg)
            os.makedirs(blobs_directory)
            runfile = f"{directory}/run.txt"
            runfile = open(runfile, "a")
            self.runfile = runfile
            self.print_header()
        else:
            self.runfile = None

    def __del__(self):
        if hasattr(self, "runfile") and (self.runfile is not None):
            self.runfile.close()
            self.runfile = None

    @classmethod
    def lformat(
        cls,
        id_,
        iteration,
        time,
        tag,
        min_,
        max_,
        mean,
        variance,
        dtype,
        shape,
        description="",
        dump_precision=None,
    ):
        try:
            return "{:<4}  {:<10}  {:<20.{p}f}  {:<40}  {:<+20.{p}f}  {:<+20.{p}f}  {:<+20.{p}f}  {:<+20.{p}f}  {:<20}  {:<20}  {}".format(
                id_,
                iteration,
                time,
                tag,
                min_,
                max_,
                mean,
                variance,
                str(dtype),
                str(shape),
                description,
                p=dump_precision,
            )
        except:
            return "{:<4}  {:<10}  {:<20}  {:<40}  {:<20}  {:<20}  {:<20}  {:<20}  {:<20}  {:<20}  {}".format(
                id_,
                iteration,
                time,
                tag,
                min_,
                max_,
                mean,
                variance,
                str(dtype),
                str(shape),
                description,
            )

    def print_header(self, with_datetime=False):
        now = datetime.datetime.now()
        self.runfile.write(
            "DEBUG DUMP {}{}\n".format(
                self.name,
                " ({})".format(now.strftime("%Y-%m-%d %H:%M")) if with_datetime else "",
            )
        )
        self.runfile.write(
            self.lformat(
                "id",
                "iteration",
                "time",
                "tag",
                "min",
                "max",
                "mean",
                "variance",
                "dtype",
                "shape",
                "description",
            )
        )

    @classmethod
    def get_arrays(self, arrays):
        if isinstance(arrays, DiscreteScalarFieldView):
            return tuple(d.get()[arrays.compute_slices] for d in arrays.data)
        elif isinstance(arrays, np.ndarray):
            return (arrays,)
        else:
            check_instance(arrays, tuple, values=np.ndarray)
            return arrays

    def __call__(self, iteration, time, tag, arrays, description=""):
        check_instance(iteration, int)
        arrays = self.get_arrays(arrays)
        N = len(arrays)
        comm = self.comm
        comm_size = self.comm_size
        for i, data in enumerate(arrays):
            if N > 1:
                tag_ = f"{tag}_{i}"
            else:
                tag_ = tag
            if description != "":
                if N > 1:
                    description_ = f"{description} (component {i})"
                else:
                    description_ = description
            else:
                _file, _line = inspect.stack()[1][1:3]
                description_ = f"{_file}:{_line}"
            dtype = data.dtype
            if dtype in (np.complex64, np.complex128):
                data = (data.real, data.imag)
                tags = (tag_ + "_real", tag_ + "_imag")
            else:
                data = (data,)
                tags = (tag_,)
            for d, tag_ in zip(data, tags):
                dtype = d.dtype
                shape = None
                id_ = self.dump_id
                _min = comm.allreduce(np.nanmin(d), op=MPI.MIN)
                _max = comm.allreduce(np.nanmax(d), op=MPI.MAX)
                mean = comm.allreduce(np.nanmean(d), op=MPI.SUM) / comm_size
                size = float(comm.allreduce(d.size))
                variance = comm.allreduce((d.size / size) * np.nanmean((d - mean) ** 2))
                entry = "\n" + self.lformat(
                    id_,
                    iteration,
                    time,
                    tag_,
                    _min,
                    _max,
                    mean,
                    variance,
                    dtype,
                    shape,
                    description_,
                    self.dump_precision,
                )

                if self.is_io_leader:
                    self.runfile.write(entry)

                if (self.dump_data) and (comm_size == 1):
                    dst = f"{self.blobs_directory}/{self.dump_id}"
                    np.savez_compressed(dst, data=d)

                self.dump_id += 1
