# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Tool to create enums compatible with C (32 bit signed integers)
* :class:`~EnumFactory`
"""
import numpy as np
import keyword
import re

registered_enums = {}


class _EnumInstanceGenerator:
    # pickle instance generator
    def __call__(self, enum_name, enum_field):
        if enum_name not in registered_enums:
            msg = "Pickle: trying to create an instance of {} but "
            msg += "this enum has not been registered yet."
            msg = msg.format(enum_name)
            raise RuntimeError(msg)
        enum_cls = registered_enums[enum_name]
        assert enum_field in enum_cls.fields().keys()
        return enum_cls(enum_field)


class EnumFactory:
    """
    Class with utilities to create enums.
    """

    class MetaEnum(type):
        pass

    @staticmethod
    def create(name, fields, dtype=np.int32, base_cls=object):
        """Create a static enum with corresponding dtype.

        Parameters
        -----------
        name : str
            Type name of the enumeration.
        fields: dict or list
            Dictionary of enum fields as string and corresponding values.
            If this is a list, the first element will the value 0 and so on.
        dtype: np.int32, np.int64, np.uint32, np.uint64
            Underlying storage type for enum values. For C compatibility use np.int32.
            For C++ you can use any of signed or unsigned integer or long integer.

        Returns
        -------
        generated_enum
            An enum *class* based on input fields and dtype.

        Examples
        --------
        fields = {'X':0, 'Y':1, 'Z':42}
        TestEnum = EnumFactory.create('Test', fields)

        X = TestEnum()
        Y = TestEnum('Y')
        Z = TestEnum.Z

        print(TestEnum.dtype)
        print(TestEnum.fields())
        print(TestEnum['X'], TestEnum['Y'], TestEnum['Z'])

        print(TestEnum.rfields())
        print(TestEnum[0], TestEnum[1], TestEnum[42])
        print()
        print(TestEnum.X, TestEnum.Y, TestEnum.Z)
        print(X, Y, Z)
        print()
        print(X.__class__.__name__)
        print(X.value(), X())
        print(X.svalue(), str(X))
        print(repr(X))

        See Also
        -------
        Generate corresponding enum C or C++ code with
        `hysop.backend.device.codegen.base.enum_codegen.EnumCodeGenerator`.

        """

        if not fields:
            msg = "fields should contain at least one entry, got {}."
            msg = msg.format(fields)
            raise ValueError(msg)

        if isinstance(fields, dict):
            if len(set(fields.values())) != len(fields.values()):
                msg = f"Field values collision: {fields}."
                raise ValueError(msg)
        elif isinstance(fields, (list, set, tuple)):
            if len(set(fields)) != len(fields):
                msg = f"Field names collision: {fields}."
                raise ValueError(msg)
            fields = dict(zip(fields, range(len(fields))))
        else:
            msg = "fields have to be of type list,set,tuple or dict but got {}."
            msg = msg.format(fields.__class__.__name__)
            raise TypeError(msg)

        if name in registered_enums.keys():
            enum = registered_enums[name]
            if any([(f not in fields) for f in enum.fields().keys()]):
                msg = "Enum '{}' was already created with different entries:"
                msg += "\n\tregistered enum: {}\n\tnew values: {}"
                msg = msg.format(name, enum.fields().keys(), fields.keys())
                raise ValueError(msg)
            elif any([fields[k] != v for (k, v) in enum.fields().items()]):
                msg = "Enum '{}' was already created with different values:"
                msg += "\n\tregistered enum: {}\n\tnew values: {}"
                msg = msg.format(name, enum.fields(), fields)
                raise ValueError(msg)
            elif enum.dtype != dtype:
                msg = "Enum '{}' was already created with different dtype!"
                msg += "\n\tregistered enum dtype: {}\n\tnew dtype: {}"
                msg = msg.format(name, enum.dtype, dtype)
                raise ValueError(msg)
            else:
                return enum

        for k in fields.keys():
            if not isinstance(k, str):
                msg = "Enum keys should be strings, got {} of type {}."
                msg = msg.format(k, k.__class__.__name__)
                raise TypeError(msg)
            if keyword.iskeyword(k):
                msg = "Enum entry ({}) cannot be a python keyword."
                msg = msg.format(k)
                raise ValueError(msg)
            if not re.match("[_A-Za-z][_a-zA-Z0-9]*$", k):
                msg = "Enum entry ({}) has to be a valid python identifier."
                msg = msg.format(k)
                raise ValueError(msg)

        fields = dict(
            zip(fields.keys(), np.asarray(tuple(fields.values())).astype(dtype))
        )
        rfields = dict(zip(fields.values(), fields.keys()))

        def __fields(cls):
            return cls._fields

        def __rfields(cls):
            return cls._rfields

        def __getitem__(cls, val):
            if isinstance(val, str) and (val in cls._fields.keys()):
                return cls._fields[val]
            elif (isinstance(val, int) or isinstance(val, dtype)) and (
                val in cls._rfields.keys()
            ):
                return getattr(cls, cls._rfields[val])
            else:
                raise RuntimeError(f"Unknown entry {val} of type {type(val)}.")

        def __str__(cls):
            return name

        def __repr__(cls):
            return name

        def __value(cls, val):
            if field in cls._fields.keys():
                return cls.dtype(cls._fields[val])
            else:
                raise RuntimeError(f"Unknown field {val} of type {type(val)}.")

        def __svalue(cls, val):
            if val in cls._rfields.keys():
                return cls._rfields[val]
            else:
                raise RuntimeError(f"Unknown value {val} of type {type(val)}.")

        def __variable(cls, name, typegen, val=None, **kwds):
            from hysop.backend.device.codegen.base.variables import (
                CodegenVariable,
                dtype_to_ctype,
            )

            if val is None:
                val = cls.fields().values()[0]
            value = cls.value(val)
            svalue = cls.svalue(val)
            return CodegenVariable(
                name=name,
                typegen=typegen,
                ctype=dtype_to_ctype(cls.dtype),
                value=value,
                svalue=svalue,
                **kwds,
            )

        def __array_variable(cls, name, typegen, vals, **kwds):
            from hysop.backend.device.codegen.base.variables import (
                dtype_to_ctype,
                CodegenVariable,
                CodegenArray,
            )

            assert vals is not None
            size = len(vals)
            value = [
                getattr(cls, cls.svalue(v)) if isinstance(v, int) else v for v in vals
            ]
            svalue = [cls.svalue(v) if isinstance(v, int) else str(v) for v in vals]
            if len(vals) == 1:
                return CodegenVariable(
                    name=name,
                    typegen=typegen,
                    ctype=dtype_to_ctype(cls.dtype),
                    value=value[0],
                    svalue=svalue[0],
                    **kwds,
                )
            else:
                return CodegenArray(
                    name=name,
                    typegen=typegen,
                    ctype=dtype_to_ctype(cls.dtype),
                    value=value,
                    svalue=svalue,
                    dim=1,
                    **kwds,
                )

        mcls_dic = {
            "name": name,
            "dtype": dtype,
            "_fields": fields,
            "_rfields": rfields,
            "fields": __fields,
            "rfields": __rfields,
            "value": __value,
            "svalue": __svalue,
            "variable": __variable,
            "array_variable": __array_variable,
            "__getitem__": __getitem__,
            "__str__": __str__,
            "__repr__": __repr__,
        }
        mcls = type(name + "MetaEnum", (EnumFactory.MetaEnum,), mcls_dic)

        class Enum(base_cls, metaclass=mcls):
            def __init__(self, field=tuple(sorted(fields.keys()))[0]):
                assert isinstance(field, str) and len(field) > 0
                self._field = field
                self._value = self.__class__.dtype(self.__class__._fields[field])

            def svalue(self):
                return self._field

            def value(self):
                return self._value

            def __call__(self):
                return self.value()

            def __int__(self):
                return int(self.value())

            def __float__(self):
                return float(self.value())

            def __str__(self):
                return self.svalue()

            def __repr__(self):
                return f"{self.svalue()}({self.value()})"

            def __eq__(self, other):
                if not isinstance(other, self.__class__):
                    return NotImplemented
                return self._value == other._value

            def __ne__(self, other):
                if not isinstance(other, self.__class__):
                    return NotImplemented
                return self._value != other._value

            def __hash__(self):
                return hash(self._field)

            # pickling
            def __reduce__(self):
                return (_EnumInstanceGenerator(), (name, self._field))

        generated_enum = type(name + "Enum", (Enum,), {})
        _all = []
        for k, v in fields.items():
            instance = generated_enum(field=k)
            setattr(mcls, k, instance)
            _all.append(instance)
        setattr(generated_enum, "all", _all)

        registered_enums[name] = generated_enum
        return generated_enum


if __name__ == "__main__":
    fields = {"X": 0, "Y": 1, "Z": 42}
    TestEnum = EnumFactory.create("Test", fields)

    X = TestEnum()
    Y = TestEnum("Y")
    Z = TestEnum.Z

    print(type(TestEnum))
    print(TestEnum.__class__)
    print(TestEnum.__class__.__name__)
    print()
    print(TestEnum.fields())
    print(TestEnum["X"], TestEnum["Y"], TestEnum["Z"])
    print()
    print(TestEnum.rfields())
    print(TestEnum[0], TestEnum[1], TestEnum[42])
    print()
    print(TestEnum.X, TestEnum.Y, TestEnum.Z)
    print(X, Y, Z)
    print()
    print(X.__class__.__name__)
    print(X.value(), X())
    print(X.svalue(), str(X))
    print(repr(X), repr(Y), repr(Z))
    print()
    print(TestEnum.dtype, type(X.value()))
