# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np


def binary_unit2str(b, unit, rounded):
    try:
        bb = int(b)
        prefix = ["", "ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi", "Yi"]
        i = 0
        while bb >= 1024:
            bb >>= 10
            i += 1
        return f"{round(b/2.0**(10*i),rounded)}{prefix[i]}{unit}"
    except:
        return f"{b}{unit}"


def decimal_unit2str(b, unit, rounded):
    try:
        bb = int(b)
        prefix = ["", "k", "M", "G", "T", "P", "E", "Z", "Y"]
        i = 0
        while bb >= 1000:
            bb /= 1000
            i += 1
        return f"{round(b/10.0**(3*i),rounded)}{prefix[i]}{unit}"
    except:
        return f"{b}{unit}"


def unit2str(b, unit, decimal, rounded=2):
    if not isinstance(b, (int, float, np.integer, np.floating)):
        return b

    if decimal:
        return decimal_unit2str(b, unit, rounded)
    else:
        return binary_unit2str(b, unit, rounded)


def bytes2str(b, decimal=True, rounded=2):
    return unit2str(b, "B", decimal)


def bdw2str(bdw, decimal=True, rounded=2):
    return unit2str(bdw, "B/s", decimal, rounded)


def flops2str(flops, decimal=True, rounded=2):
    return unit2str(flops, "FLOPS", decimal, rounded)


def iops2str(iops, decimal=True, rounded=2):
    return unit2str(iops, "IOPS", decimal, rounded)


def freq2str(freq, decimal=True, rounded=2):
    return unit2str(freq, "Hz", decimal, rounded)


def time2str(t, on_zero=None, on_none=None):
    if not isinstance(t, (float, int, np.dtype)):
        return t
    if t is None:
        return on_none or f"{-1:5d}"
    elif t < 1e-9:
        return on_zero or f"{0:5d}"
    elif t < 1e-6:
        return f"{t*1e9:>5.1f}ns"
    elif t < 1e-3:
        return f"{t*1e6:>5.1f}us"
    elif t < 1e1:
        return f"{t*1e3:>5.1f}ms"
    else:
        return f"{t:>6.1f}s"
