# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np

try:
    import hptt

    HAS_HPTT = True
    # required version is: https://gitlab.com/keckj/hptt
except ImportError:
    hptt = None
    HAS_HPTT = False
    import warnings
    from hysop.tools.warning import HysopPerformanceWarning

    msg = "Failed to import HPTT module, falling back to slow numpy transpose. Required version is available at https://gitlab.com/keckj/hptt."
    warnings.warn(msg, HysopPerformanceWarning)


def array_share_data(a, b):
    abeg, aend = np.byte_bounds(a)
    bbeg, bend = np.byte_bounds(b)
    beg, end = max(abeg, bbeg), min(aend, bend)
    return end - beg > 0


if HAS_HPTT:

    def can_exec_hptt(src, dst):
        if src is dst:
            return False
        if src.dtype != dst.dtype:
            return False
        if src.dtype not in (np.float32, np.float64, np.complex64, np.complex128):
            return False
        if src.flags.c_contiguous != dst.flags.c_contiguous:
            return False
        if src.flags.f_contiguous != dst.flags.f_contiguous:
            return False
        if not (src.flags.c_contiguous ^ src.flags.f_contiguous):
            return False
        return not array_share_data(src, dst)

else:

    def can_exec_hptt(src, dst):
        return False
