# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABCMeta
from hysop.tools.henum import EnumFactory
from hysop.tools.htypes import check_instance, first_not_None
from hysop.tools.decorators import debug

Variable = EnumFactory.create("Variable", ["DISCRETE_FIELD", "PARAMETER"])


class VariableTag(metaclass=ABCMeta):
    """Tag for HySoP variables."""

    @debug
    def __new__(cls, variable_kind=None, **kwds):
        check_instance(variable_kind, Variable, allow_none=True)
        obj = super().__new__(cls, **kwds)
        obj.__variable_kind = variable_kind
        return obj

    @debug
    def __init__(self, variable_kind=None, **kwds):
        check_instance(variable_kind, Variable, allow_none=True)
        super().__init__(**kwds)
        self.__variable_kind = first_not_None(self.__variable_kind, variable_kind)
        assert (
            self.__variable_kind is not None
        ), "variable_kind has not been setup correctly."

    def _get_variable_kind(self):
        """Get variable kind."""
        return self.__variable_kind

    kind = property(_get_variable_kind)
