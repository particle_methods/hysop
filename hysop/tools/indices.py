# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np


def condition2Slice(cond):
    dim = len(cond.shape)
    ilist = np.where(cond)
    if ilist[0].size == 0:
        isEmpty = True
        sl = [slice(0, 0) for i in range(dim)]
        resol = np.asarray([0] * dim)
    else:
        start = np.asarray([ilist[i].min() for i in range(dim)])
        end = np.asarray([ilist[i].max() + 1 for i in range(dim)])
        sl = [slice(start[i], end[i]) for i in range(dim)]
        resol = end - start
        isEmpty = False

    return sl, isEmpty, resol


def removeLastPoint(cond):

    shape = cond.shape
    dim = len(shape)
    ilist = np.where(cond)
    end = [ilist[i].max() for i in range(dim)]
    subl = [np.where(ilist[i] == end[i]) for i in range(dim)]
    for sl in subl:
        sublist = [ilist[i][sl] for i in range(dim)]
        sublist = tuple(sublist)
        cond[sublist] = False
    return cond
