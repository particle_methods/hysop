# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np

# import scitools.filetable as ft
import matplotlib.pyplot as plt


def _ft_read(fileobj, commentchar="#"):
    """
    Load a table with numbers into a two-dim. NumPy array.
    @param fileobj: open file object.
    @param commentchar: lines starting with commentchar are skipped
    (a blank line is an array data delimiter and stops reading).
    @return: two-dimensional (row-column) NumPy array.
    """
    # Function taken from scitools
    # based on a version by Mario Pernici <Mario.Pernici@mi.infn.it>
    location = fileobj.tell()
    import re

    commentchar = re.escape(commentchar)
    while True:
        line = fileobj.readline()
        if not line:
            break  # end of file
        elif line.isspace():
            break  # blank line
        elif re.match(commentchar, line):
            continue  # treat next line
        else:
            break

    shape1 = len(line.split())
    if shape1 == 0:
        return None
    fileobj.seek(location)

    blankline = re.compile(r"\n\s*\n", re.M)
    commentline = re.compile(r"^%s[^\n]*\n" % commentchar, re.M)
    filestr = fileobj.read()
    # remove lines after a blank line
    m = re.search(blankline, filestr)
    if m:
        filestr = filestr[: m.start() + 1]
    # skip lines starting with the comment character
    filestr = re.sub(commentline, "", filestr)
    a = [float(x) for x in filestr.split()]
    data = np.array(a)
    data.shape = (len(a) // shape1, shape1)
    return data


# Lambda comparison
# Results in Softs/MethodesParticulaires/Resultats_simu/Comp_lmanda
fileDt = (("drag129_fixe"), ("drag129_var"), ("drag65_fixe"), ("drag65_var"))
fileListLayer = (("drag_01"), ("drag_02"), ("drag_03"))
fileListLambda = (("drag_05"), ("drag_06"), ("drag_07"), ("drag_09"), ("drag_11"))
fileListLambda2 = (("d129_4"), ("d129_5"), ("drag_06"), ("d129_7"), ("d129_12"))
fileListLambda3 = (("d129_5"), ("d257_5"))  # ,('d257_7'))
fileListLambda4 = (("drag_06"), ("d257_6"))  # ,('d257_7'))

legendLayer = ("layer=0.1", "layer=0.2", "layer=0.3")
legendLambda = (
    "lambda=1e5",
    "lambda=1e6",
    "lambda=1e7",
    "lambda=1e9",
    "lambda=1e11",
    "Ref from Folke : 0.6726",
)
legendLambda2 = (
    "lambda=1e4",
    "lambda=1e5",
    "lambda=1e6",
    "lambda=1e7",
    "lambda=1e12",
    "Ref from Folke : 0.6726",
)
legendLambda3 = ("lambda=1e5", "257 - lambda=1e5", "Ref from Folke : 0.6726")
legendLambda3 = ("lambda=1e6", "257 - lambda=1e6", "Ref from Folke : 0.6726")
plt.hold("off")
plt.xlabel("time")
plt.hold("on")
plt.ylabel("drag")
plt.axis([0, 70, 0.3, 1])
plt.grid("on")

for filename in fileListLambda3:
    print("my file is ", filename)
    file = open(filename)
    table = _ft_read(file)
    time = table[:, 0]
    drag = table[:, 1]
    file.close()
    plt.plot(time, drag, "--")
plt.axhline(y=0.6726, xmin=0, xmax=22, color="r")
plt.legend(legendLambda3)
# plt.hold('on')

plt.savefig("DragRe133_CompLambda3.pdf")
plt.show()
