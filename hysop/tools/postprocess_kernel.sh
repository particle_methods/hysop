#!/bin/bash
##
## Copyright (c) HySoP 2011-2024
##
## This file is part of HySoP software.
## See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
## for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

# Example of autotuner post processing script.
# Input arguments are:
#    FILE_BASENAME  FROM_CACHE
#    AUTOTUNER_DUMP_DIR  AUTOTUNER_NAME  KERNEL_NAME
#    MEAN_EXECUTION_TIME_NS  MIN_EXECUTION_TIME_NS  MAX_EXECUTION_TIME_NS
#    KERNEL_SOURCE_FILE  KERNEL_ISOLATION_FILE  KERNEL_HASH_LOGS
#    VENDOR_NAME  DEVICE_NAME
#    WORK_SIZE  WORK_LOAD
#    GLOBAL_WORK_SIZE  LOCAL_WORK_SIZE
#    EXTRA_PARAMETERS  EXTRA_KWDS_HASH  SRC_HASH
# See example hysop/examples/example_utils.py interface and '--autotuner-postprocess-kernels' argument.

set -e
if [ "$#" -ne 20 ]; then
    echo "Script expected 20 parameters."
    exit 1
fi

FILE_BASENAME=${1}
FROM_CACHE=${2}
AUTOTUNER_DUMP_DIR=${3}
AUTOTUNER_NAME=${4}
KERNEL_NAME=${5}
MEAN_EXECUTION_TIME_NS=${6}
MIN_EXECUTION_TIME_NS=${7}
MAX_EXECUTION_TIME_NS=${8}
KERNEL_SOURCE_FILE=${9}
KERNEL_ISOLATION_FILE=${10}
KERNEL_HASH_LOGS_FILE=${11}
VENDOR_NAME=${12}
DEVICE_NAME=${13}
WORK_SIZE=${14}
WORK_LOAD=${15}
GLOBAL_WORK_SIZE=${16}
LOCAL_WORK_SIZE=${17}
EXTRA_PARAMETERS=${18}
EXTRA_KWDS_HASH=${19}
SRC_HASH=${20}

echo "Successfully postprocessed kernel '$AUTOTUNER_NAME'."
exit 0
