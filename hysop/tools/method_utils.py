# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.constants import SpaceDiscretization
from hysop.tools.htypes import check_instance, to_list, first_not_None, InstanceOf
from hysop.tools.decorators import debug
from hysop.numerics.interpolation.polynomial import (
    PolynomialInterpolator,
    PolynomialInterpolation,
)


class SpaceDiscretizationMethod:
    """
    Operator helper to handle space discretization method.
    """

    __default_method = {SpaceDiscretization: 2}

    __available_methods = {
        SpaceDiscretization: (InstanceOf(int), InstanceOf(SpaceDiscretization)),
    }

    @classmethod
    def default_method(cls):
        dm = super().default_method()
        dm.update(cls.__default_method)
        return dm

    @classmethod
    def available_methods(cls):
        am = super().available_methods()
        am.update(cls.__available_methods)
        return am

    @debug
    def handle_method(self, method):
        super().handle_method(method)
        sdm = method.pop(SpaceDiscretization)
        if isinstance(sdm, int):
            sd = sdm
        elif isinstance(sdm, SpaceDiscretization):
            assert str(sdm).startswith("FDC")
            sd = int(str(sdm)[3:])
        else:
            raise NotImplementedError(sdm)
        self.space_discretization_method = sdm
        self.space_discretization = sd


class PolynomialInterpolationMethod(SpaceDiscretizationMethod):
    """
    Operator helper to handle polynomial interpolation method.
    """

    __default_method = {
        PolynomialInterpolator: PolynomialInterpolation.LINEAR,
    }

    __available_methods = {
        PolynomialInterpolator: InstanceOf(PolynomialInterpolation),
    }

    @classmethod
    def default_method(cls):
        dm = super().default_method()
        dm.update(cls.__default_method)
        return dm

    @classmethod
    def available_methods(cls):
        am = super().available_methods()
        am.update(cls.__available_methods)
        return am

    @debug
    def handle_method(self, method):
        super().handle_method(method)
        fd = self.space_discretization
        pi = method.pop(PolynomialInterpolator)
        self.polynomial_interpolation_method = pi
        self.polynomial_interpolator = PolynomialInterpolator.build_interpolator(
            pi=pi, fd=fd, dim=self.dim
        )
