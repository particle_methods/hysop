# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import warnings


class HysopWarning(RuntimeWarning):
    """
    Custom warning class for hysop warnings.
    """

    pass


class HysopDeprecationWarning(DeprecationWarning):
    """
    Custom warning class for hysop deprecation.
    """

    pass


class HysopPerformanceWarning(HysopWarning):
    """
    Custom warning class for hysop performance.
    """

    pass


class HysopDumpWarning(HysopWarning):
    """
    Custom warning class for hysop I/O dumps.
    """

    pass


class HysopCacheWarning(HysopWarning):
    """
    Custom warning class for hysop caching.
    """

    pass


def configure_hysop_warnings(action):
    """
    Configure hysop warnings.
    Action can be error, ignore, always, default, module, once.
    See https://docs.python.org/2/library/warnings.html#warning-filter.
    """
    warnings.filterwarnings(action=action, category=HysopWarning)
    warnings.filterwarnings(action=action, category=HysopDeprecationWarning)


def enable_hysop_warnings():
    """
    Configure hysop warnings to default.
    Print the first occurrence of matching warnings for each location where the warning is issued.
    """
    configure_hysop_warnings(action="default")


def disable_hysop_warnings():
    """
    Configure hysop warnings to ignored.
    Never print matching warnings.
    """
    configure_hysop_warnings(action="ignore")


enable_hysop_warnings()
