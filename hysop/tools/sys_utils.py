# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Tools related to global config.
"""
import sys, os


class SysUtils:
    """
    Global system check and other utilities
    """

    @staticmethod
    def in_ipython():
        """True if current session is run under ipython"""
        try:
            __IPYTHON__
        except NameError:
            return False
        else:
            return True

    @staticmethod
    def is_interactive():
        """True if run under interactive session, else
        False. Warning : return true in any case if ipython is
        used.
        """
        return hasattr(sys, "ps1") or hasattr(sys, "ipcompleter")

    @staticmethod
    def cmd_exists(cmd):
        """
        Returns True if cmd exists.
        """
        return any(
            os.access(os.path.join(path, cmd), os.X_OK)
            for path in os.environ["PATH"].split(os.pathsep)
        )
