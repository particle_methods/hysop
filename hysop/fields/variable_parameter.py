# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Tools to define parameters that do not depend on space but may
depend on simulation parameters.

Example : the flow rate through the inlet

.. code::

    def frate(simu):
        res = np.zeros(problem.dimension)
        res[...] = np.sin(simu.time)
        return res

    # define the parameter ...
    rate = VariableParameter(formula=frate)
    ...
    # compute its value for the current simulation state
    rate.update(simu)


Notes
-----

* frate must be a python function with a simulation object as input.
* update function is usually called during operator apply
  (for instance adaptive time step or absorption_BC).

"""


class VariableParameter:
    """User-defined parameter (a dictionnary indeed)
    that may depend on simulation parameters values and so
    needs some update during simulation process.

    """

    def __init__(self, data=0.0, formula=None):
        """
        Parameters
        ----------
        data: real or integer, optional
            default or initial value of the parameter
        formula : python function, optional
             how to compute the variable
        """
        self.data = data
        # Formula used to compute data (a python function)
        if formula is None:

            def _formula(simu):
                return self.data

            self.formula = _formula
        else:
            self.formula = formula

    def update(self, simu=None):
        """Update parameter value for the current simulation

        Parameter
        ---------
        simu : `:class::~hysop.problem.simulation.Simulation`

        """
        self.data = self.formula(simu)

    def __str__(self):
        return str(self.data)
