# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
from hysop import Box, CartesianDiscretization, CartesianTopology
from hysop.constants import HYSOP_REAL
from hysop.defaults import VelocityField, VorticityField
from hysop.fields.cartesian_discrete_field import (
    CartesianDiscreteField,
    CartesianDiscreteScalarFieldView,
)
from hysop.fields.continuous_field import Field, ScalarField, TensorField
from hysop.fields.discrete_field import (
    DiscreteField,
    DiscreteScalarFieldView,
    DiscreteTensorField,
)
from hysop.testsenv import domain_boundary_iterator
from hysop.tools.htypes import check_instance


def test_field():
    domain = Box(dim=3)
    F0 = Field(domain, "F0")
    F1 = Field(domain, "F1", pretty_name="test", dtype=np.uint8)
    F2 = Field(domain, "F0")
    F3 = F0.field_like("F3", pretty_name="test2")
    F4 = F0.field_like("F4", dtype=np.uint16)
    F5 = F0.field_like("F5", is_tmp=True)
    F6 = F0.gradient()
    F7 = F5.field_like("F7")

    D0 = CartesianDiscretization(resolution=(5, 5, 5), default_boundaries=True)
    T0 = CartesianTopology(domain=domain, discretization=D0)

    DF0 = F0.discretize(T0)
    DF1 = F1.discretize(T0)
    DF2 = F2.discretize(T0)
    DF3 = F3.discretize(T0)
    DF4 = F4.discretize(T0)
    DF5 = F5.discretize(T0)
    DF6 = F6.discretize(T0)
    DF7 = F7.discretize(T0)

    requests = DF5._dfield.memory_request(
        op=DF5.name, request_identifier=DF5.memory_request_id
    )
    requests += DF7._dfield.memory_request(
        op=DF7.name, request_identifier=DF7.memory_request_id
    )
    work = requests.allocate(True)
    DF5.honor_memory_request(work)
    DF7.honor_memory_request(work)

    assert isinstance(F0, Field)
    assert isinstance(F1, Field)
    assert isinstance(F2, Field)
    assert isinstance(F3, Field)
    assert isinstance(F4, Field)
    assert isinstance(F5, Field)
    assert isinstance(F6, TensorField)
    assert isinstance(F7, Field)

    assert not F0.is_tmp
    assert not F1.is_tmp
    assert not F2.is_tmp
    assert not F3.is_tmp
    assert not F4.is_tmp
    assert F5.is_tmp
    assert F7.is_tmp

    assert F0.domain is domain
    assert F0.dim is domain.dim
    assert F0.name == "F0"
    assert F0.pretty_name == "F0"
    assert F0 is not F1
    assert F0 == F0
    assert F0 != F1
    assert hash(F0) == id(F0)
    assert hash(F0) != hash(F1)
    assert hash(F0) != hash(F2)
    assert str(F0) == F0.long_description()
    assert len(F0.short_description()) < len(F0.long_description())
    assert F1.name == "F1"
    assert F1.pretty_name == "test"
    assert F0.nb_components == 1
    assert len(F0.fields) == 1
    assert F0.fields[0] is F0
    assert F0.tag == "f0"
    assert F1.tag == "f1"
    assert F0.dtype == HYSOP_REAL
    assert F1.dtype == np.uint8

    assert F3.dtype == F0.dtype
    assert F3.name == "F3"
    assert F3.pretty_name == "test2"

    assert F4.dtype == np.uint16
    assert F4.name == "F4"
    assert F4.pretty_name == "F4"

    def func(data, coords, component):
        (x, y, z) = coords
        data[...] = x * y * z

    DF7.initialize(func)
    DF7.initialize(DF5.data)
    DF7.fill(4)
    DF7.randomize()
    DF7.copy(DF0)
    DF7.integrate()

    DF7.has_ghosts()
    DF7.exchange_ghosts()
    DF7.accumulate_ghosts()
    exchanger = DF7.build_ghost_exchanger()
    try:
        exchanger()
    except TypeError:
        assert exchanger is None

    DF7.match(DF5)
    DF7 == DF5
    DF7 != DF5
    hash(DF7)
    DF7.view(DF1.topology_state)

    DF7.short_description()
    DF7.long_description()
    str(DF7)


def test_tensor_field():
    domain = Box(dim=3)
    T0 = TensorField(domain, "T0", (3, 3))
    T0_bis = Field(domain, "T0b", shape=(3, 3))
    T1 = T0.field_like("T1", dtype=np.float16)
    T2 = T0[1:, 1:]
    T2.rename(name="T2")
    T3 = T0[1, 1]
    T4 = TensorField.from_fields(
        name="T4", fields=(T0[0, 0], T0[1, 1], T1[0, 1], T1[1, 0]), shape=(2, 2)
    )
    T5 = TensorField.from_field_array("T5", T0._fields)
    T6 = T5.field_like(name="T6")
    T7 = T6.field_like(name="T7", is_tmp=True)
    assert isinstance(T0, TensorField)
    assert isinstance(T1, TensorField)
    assert isinstance(T2, TensorField)
    assert isinstance(T3, Field)
    assert isinstance(T4, TensorField)
    assert isinstance(T5, TensorField)
    assert isinstance(T6, TensorField)
    assert isinstance(T7, TensorField)
    assert T0.name == "T0"
    assert T1.name == "T1"
    assert T2.name == "T2"
    assert T3.name == "T0_1_1", T3.name
    assert T4.name == "T4"
    assert T5.name == "T5"
    assert T6.name == "T6"
    assert np.array_equal(T0.shape, (3, 3))
    assert np.array_equal(T1.shape, (3, 3))
    for idx, t0 in T0.nd_iter():
        t1 = T1[idx]
        assert t0.domain is domain
        assert t1.domain is domain
        assert t1.dtype != t0.dtype
        assert t1.name.replace("1", "0", 1) == t0.name
        assert t1.pretty_name.replace("1", "0", 1) == t0.pretty_name
        assert t0.dim == 3
        assert t1.dim == 3
    assert np.array_equal(T0._fields[1:, 1:], T2._fields)
    assert T0._fields[1, 1] is T3
    assert T0._fields[0, 0] is T4[0, 0]
    assert T0._fields[1, 1] is T4[0, 1]
    assert T1._fields[0, 1] is T4[1, 0]
    assert T1._fields[1, 0] is T4[1, 1]
    assert np.array_equal(T0._fields, T5._fields)
    for f in T0:
        assert f in T0

    D0 = CartesianDiscretization(resolution=(5, 5, 5), default_boundaries=True)
    topo = CartesianTopology(domain=domain, discretization=D0)

    DT0 = T0.discretize(topo)
    DT1 = T1.discretize(topo)
    DT2 = T2.discretize(topo)
    DT3 = T3.discretize(topo)
    DT4 = T4.discretize(topo)
    DT5 = T5.discretize(topo)
    DT6 = T6.discretize(topo)
    DT7 = T7.discretize(topo)

    DT8 = DiscreteTensorField.from_dfields(
        name="DT8",
        dfields=(DT0.dfields[0], DT1.dfields[0], DT2.dfields[0], DT3.dfields[0]),
        shape=(2, 2),
    )
    DT9 = DT0[:2, :2]
    DT10 = DT9.clone()
    DT11, requests11 = DT10.tmp_dfield_like(name="DT11")
    DT12, requests12 = DT10.tmp_dfield_like(name="DT11")

    work = requests11().allocate(True)
    DT11.honor_memory_request(work)

    work = requests12().allocate(True)
    DT12.honor_memory_request(work)

    for df in DT11.data:
        df[...] = 11
    for df in DT12.data:
        df[...] = 12
    for df in DT11.data:
        assert np.all(df == 11)
    for df in DT12.data:
        assert np.all(df == 12)

    str(DT0)
    DT0.short_description()
    assert str(DT0) == DT0.long_description()
    for df in DT0:
        assert df in DT0

    def func(data, coords, component):
        (x, y, z) = coords
        data[...] = x * y * z

    DT9.rename("foo")
    DT9.initialize(func)
    DT10.randomize()
    DT9.initialize(DT10.data)
    DT9.fill(4)
    DT9.copy(DT0[1:, 1:])

    DT9.has_ghosts()
    DT9.exchange_ghosts()
    DT9.accumulate_ghosts()
    exchanger = DT9.build_ghost_exchanger()
    try:
        exchanger()
    except TypeError:
        assert exchanger is None

    DT9.match(DT10)
    DT9 == DT10
    DT9 != DT10
    hash(DT9)
    DT9.view(DT1.topology_state, "test")

    DT9.short_description()
    DT9.long_description()

    DT9.integrate()

    assert DT0.nb_components == 9
    check_instance(
        DT0.discrete_field_views(),
        tuple,
        values=CartesianDiscreteScalarFieldView,
        size=9,
    )
    check_instance(DT0.dfields, tuple, values=CartesianDiscreteScalarFieldView, size=9)
    check_instance(DT0.discrete_fields(), tuple, values=CartesianDiscreteField, size=9)
    check_instance(DT0.continuous_fields(), tuple, values=Field, size=9)

    assert DT0.has_unique_backend()
    assert DT0.has_unique_backend_kind()
    assert DT0.has_unique_domain()
    assert DT0.has_unique_topology()
    assert DT0.has_unique_topology_state()
    assert DT0.has_unique_mesh()
    assert DT0.has_unique_dtype()
    assert DT0.has_unique_compute_resolution()
    assert DT0.has_unique_resolution()
    assert DT0.has_unique_ghosts()
    assert DT0.has_unique_space_step()
    assert DT0.has_unique_coords()
    assert DT0.has_unique_mesh_coords()
    assert DT0.has_unique_compute_slices()
    assert DT0.has_unique_inner_ghost_slices()
    assert DT0.has_unique_outer_ghost_slices()
    assert DT0.has_unique_grid_npoints()
    assert DT0.has_unique_axes()
    assert DT0.has_unique_tstate()
    assert DT0.has_unique_memory_order()
    assert DT0.has_unique_local_boundaries()
    assert DT0.has_unique_local_lboundaries()
    assert DT0.has_unique_local_rboundaries()
    assert DT0.has_unique_global_boundaries()
    assert DT0.has_unique_global_lboundaries()
    assert DT0.has_unique_global_rboundaries()
    assert DT0.has_unique_is_at_boundary()
    assert DT0.has_unique_is_at_left_boundary()
    assert DT0.has_unique_is_at_right_boundary()

    dfield = DT0[1, 0]
    assert DT0.backend == dfield.backend
    assert DT0.backend_kind == dfield.backend_kind
    assert DT0.domain == dfield.domain
    assert DT0.topology == dfield.topology
    assert DT0.topology_state == dfield.topology_state
    assert DT0.mesh == dfield.mesh
    assert DT0.dtype == dfield.dtype
    assert DT0.ctype == dfield.ctype
    assert (DT0.compute_resolution == dfield.compute_resolution).all()
    assert (DT0.resolution == dfield.resolution).all()
    assert (DT0.ghosts == dfield.ghosts).all()
    assert (DT0.local_boundaries[0] == dfield.local_boundaries[0]).all()
    assert (DT0.local_boundaries[1] == dfield.local_boundaries[1]).all()
    assert (DT0.global_boundaries[0] == dfield.global_boundaries[0]).all()
    assert (DT0.global_boundaries[1] == dfield.global_boundaries[1]).all()
    assert (DT0.space_step == dfield.space_step).all()
    for i in range(DT0.dim):
        assert (DT0.coords[i] == dfield.coords[i]).all()
        assert (DT0.mesh_coords[i] == dfield.mesh_coords[i]).all()
    assert DT0.compute_slices == dfield.compute_slices
    assert DT0.inner_ghost_slices == dfield.inner_ghost_slices
    assert DT0.inner_ghost_slices == dfield.inner_ghost_slices
    assert DT0.outer_ghost_slices == dfield.outer_ghost_slices
    assert DT0.grid_npoints == dfield.grid_npoints
    assert DT0.axes == dfield.axes
    assert DT0.tstate == dfield.tstate
    assert DT0.memory_order == dfield.memory_order

    assert DT8.has_unique_backend()
    assert DT8.has_unique_backend_kind()
    assert DT8.has_unique_domain()
    assert DT8.has_unique_topology()
    assert DT8.has_unique_topology_state()
    assert DT8.has_unique_mesh()
    assert not DT8.has_unique_dtype()
    assert DT8.has_unique_compute_resolution()
    assert DT8.has_unique_resolution()
    assert DT8.has_unique_ghosts()
    assert DT8.has_unique_space_step()
    assert DT8.has_unique_coords()
    assert DT8.has_unique_mesh_coords()
    assert DT8.has_unique_compute_slices()
    assert DT8.has_unique_inner_ghost_slices()
    assert DT8.has_unique_outer_ghost_slices()
    assert DT8.has_unique_grid_npoints()
    assert DT8.has_unique_axes()
    assert DT8.has_unique_tstate()
    assert DT8.has_unique_memory_order()
    assert DT8.has_unique_local_boundaries()
    assert DT8.has_unique_local_lboundaries()
    assert DT8.has_unique_local_rboundaries()
    assert DT8.has_unique_global_boundaries()
    assert DT8.has_unique_global_lboundaries()
    assert DT8.has_unique_global_rboundaries()
    assert DT8.has_unique_is_at_boundary()
    assert DT8.has_unique_is_at_left_boundary()
    assert DT8.has_unique_is_at_right_boundary()
    try:
        DT8.dtype
        raise RuntimeError
    except AttributeError:
        pass


def test_boundaries():
    """This test checks that all boundaries are compatible for velocity and vorticity."""
    for dim in (
        1,
        2,
    ):
        i = 0
        for lbd, rbd in domain_boundary_iterator(dim):
            domain = Box(dim=dim, lboundaries=lbd, rboundaries=rbd)
            V = VelocityField(domain)
            S = ScalarField(name="S0", domain=domain)
            divV = V.div()
            gradV = V.gradient()
            lapV = V.laplacian()
            print()
            print("DOMAIN BOUNDARIES:")
            print(f" *boundaries=[{domain.format_boundaries()}]")
            print("SCALAR BOUNDARIES:")
            print(f" *{S.pretty_name} boundaries=[{S.format_boundaries()}]")
            print("VELOCITY BOUNDARIES:")
            for Vi in V.fields:
                print(f" *{Vi.pretty_name} boundaries=[{Vi.format_boundaries()}]")
            print(f"{divV.pretty_name} BOUNDARIES:")
            print(f" *{divV.pretty_name} boundaries=[{divV.format_boundaries()}]")
            print(f"{gradV.pretty_name} BOUNDARIES:")
            for gVi in gradV.fields:
                print(f" *{gVi.pretty_name} boundaries=[{gVi.format_boundaries()}]")
            print(f"{lapV.pretty_name} BOUNDARIES:")
            for lVi in lapV.fields:
                print(f" *{lVi.pretty_name} boundaries=[{lVi.format_boundaries()}]")
            if dim > 1:
                rotV = V.curl()
                print(f"{rotV.pretty_name} (VORTICITY) BOUNDARIES:")
                for Wi in rotV.fields:
                    print(f" *{Wi.pretty_name} boundaries=[{Wi.format_boundaries()}]")


if __name__ == "__main__":
    test_field()
    test_tensor_field()
    test_boundaries()
