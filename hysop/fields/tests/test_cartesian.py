# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import itertools as it
import os
import subprocess
import sys
import time

import numpy as np
from hysop import __ENABLE_LONG_TESTS__
from hysop.constants import (
    Backend,
    BoundaryCondition,
    DirectionLabels,
    ExchangeMethod,
    GhostMask,
    GhostOperation,
)
from hysop.domain.box import Box
from hysop.fields.continuous_field import Field
from hysop.testsenv import (
    __HAS_OPENCL_BACKEND__,
    domain_boundary_iterator,
    iter_clenv,
    test_context,
)
from hysop.tools.numerics import is_fp, is_integer
from hysop.tools.numpywrappers import npw
from hysop.tools.parameters import CartesianDiscretization
from hysop.topology.cartesian_topology import CartesianTopology, CartesianTopologyState


def __random_init(data, coords, component):
    shape = data.shape
    dtype = data.dtype
    if is_integer(dtype):
        data[...] = npw.random.random_integers(low=0, high=255, size=shape).astype(
            dtype
        )
    elif is_fp(dtype):
        data[...] = npw.random.random(size=shape).astype(dtype)
    else:
        msg = f"Unknown dtype {dtype}."
        raise NotImplementedError(msg)


def __zero_init(data, coords, component):
    data[...] = 0


def __cst_init(cst):
    def __init(data, coords, component):
        data[...] = cst

    return __init


def test_serial_initialization_1d():
    print()
    print("test_serial_initialization_1d()")
    dim = 1
    npts = (10,)
    nghosts = (2,)

    for lbd, rbd in domain_boundary_iterator(dim):
        domain = Box(dim=dim, lboundaries=lbd, rboundaries=rbd)
        F0 = Field(domain=domain, name="F0", nb_components=1)
        F1 = Field(domain=domain, name="F1", nb_components=2)
        F2 = Field(domain=domain, name="F2", shape=(2, 2))
        print(f"[{F0.format_boundaries()}]")

        discretization = CartesianDiscretization(
            npts, nghosts, lboundaries=F0.lboundaries, rboundaries=F0.rboundaries
        )

        topo0 = CartesianTopology(
            domain=domain, discretization=discretization, backend=Backend.HOST
        )
        if __HAS_OPENCL_BACKEND__:
            topos = (topo0,) + tuple(
                CartesianTopology(
                    domain=domain,
                    discretization=discretization,
                    backend=Backend.OPENCL,
                    cl_env=cl_env,
                )
                for cl_env in iter_clenv()
            )
        else:
            topos = (topo0,)

        assert all(
            t.mesh.global_lboundaries == t.mesh.local_lboundaries == F0.lboundaries
            for t in topos
        )
        assert all(
            t.mesh.global_rboundaries == t.mesh.local_rboundaries == F0.rboundaries
            for t in topos
        )

        for topo in topos:
            sys.stdout.write(f"  {topo.full_pretty_tag}::{topo.backend.kind} ")
            sys.stdout.flush()

            dF0 = F0.discretize(topo)
            dF1 = F1.discretize(topo)
            dF2 = F2.discretize(topo)

            dfields = dF0.dfields + dF1.dfields + dF2.dfields
            assert all(
                d.global_lboundaries == d.local_lboundaries == F0.lboundaries
                for d in dfields
            )
            assert all(
                d.global_rboundaries == d.local_rboundaries == F0.rboundaries
                for d in dfields
            )

            dF0.initialize(__random_init)
            dF1.initialize(__random_init)
            dF2.initialize(__random_init)

            data = dF0.data + dF1.data + dF2.data

            (Nx,) = npts
            (Gx,) = nghosts
            (Lx,) = tuple(discretization.lboundaries)
            (Rx,) = tuple(discretization.rboundaries)
            try:
                for i, d in enumerate(data):
                    b = d.get().handle
                    assert b.shape == (Nx + 2 * Gx,)
                    if Lx == BoundaryCondition.PERIODIC:
                        assert (b[Gx : 2 * Gx] == b[Gx + Nx :]).all(), b
                    elif Lx == BoundaryCondition.HOMOGENEOUS_DIRICHLET:
                        assert (b[Gx] == 0).all(), b
                        assert (b[:Gx] == -b[Gx + 1 : 2 * Gx + 1][::-1]).all(), b
                    elif Lx == BoundaryCondition.HOMOGENEOUS_NEUMANN:
                        assert (b[:Gx] == +b[Gx + 1 : 2 * Gx + 1][::-1]).all(), b
                    else:
                        raise NotImplementedError(f"Unknown boundary condition {Lx}.")
                    if Rx == BoundaryCondition.PERIODIC:
                        assert (b[:Gx] == b[Nx : Gx + Nx]).all(), b
                    elif Rx == BoundaryCondition.HOMOGENEOUS_DIRICHLET:
                        assert (b[Nx + Gx - 1] == 0).all(), b
                        assert (b[Nx - 1 : Nx + Gx - 1] == -b[Nx + Gx :][::-1]).all(), b
                    elif Rx == BoundaryCondition.HOMOGENEOUS_NEUMANN:
                        assert (b[Nx - 1 : Nx + Gx - 1] == +b[Nx + Gx :][::-1]).all(), b
                    else:
                        raise NotImplementedError(f"Unknown boundary condition {Rx}.")
                    sys.stdout.write(".")
                    sys.stdout.flush()
            finally:
                print()


def test_serial_initialization_2d():
    print()
    print("test_serial_initialization_2d()")
    dim = 2
    npts = (4, 8)
    nghosts = (1, 2)
    for lbd, rbd in domain_boundary_iterator(dim):
        domain = Box(dim=dim, lboundaries=lbd, rboundaries=rbd)
        F0 = Field(domain=domain, name="F0", nb_components=1)
        F1 = Field(domain=domain, name="F1", nb_components=2)
        print(f"[{F0.format_boundaries()}]")

        discretization = CartesianDiscretization(
            npts, nghosts, lboundaries=F0.lboundaries, rboundaries=F0.rboundaries
        )

        topo0 = CartesianTopology(
            domain=domain, discretization=discretization, backend=Backend.HOST
        )
        if __HAS_OPENCL_BACKEND__:
            topos = (topo0,) + tuple(
                CartesianTopology(
                    domain=domain,
                    discretization=discretization,
                    backend=Backend.OPENCL,
                    cl_env=cl_env,
                )
                for cl_env in iter_clenv()
            )
        else:
            topos = (topo0,)

        assert all(np.all(t.mesh.local_lboundaries == F0.lboundaries) for t in topos)
        assert all(np.all(t.mesh.local_rboundaries == F0.rboundaries) for t in topos)

        for topo in topos:
            sys.stdout.write(f"  {topo.full_pretty_tag}::{topo.backend.kind}\n")
            sys.stdout.flush()

            dF0 = F0.discretize(topo)
            dF1 = F1.discretize(topo)

            dfields = dF0.dfields + dF1.dfields
            assert all(np.all(d.local_lboundaries == F0.lboundaries) for d in dfields)
            assert all(np.all(d.local_rboundaries == F0.rboundaries) for d in dfields)

            Ny, Nx = npts
            Gy, Gx = nghosts
            Ly, Lx = tuple(discretization.lboundaries)
            Ry, Rx = tuple(discretization.rboundaries)
            Xo = (slice(0, Gx), slice(Gx, Gx + Nx), slice(Gx + Nx, None))
            Yo = (slice(0, Gy), slice(Gy, Gy + Ny), slice(Gy + Ny, None))
            Xi = (slice(Gx, 2 * Gx), slice(2 * Gx, Nx), slice(Nx, Gx + Nx))
            Yi = (slice(Gy, 2 * Gy), slice(2 * Gy, Ny), slice(Ny, Gy + Ny))
            Ix = (slice(None, None, +1), slice(None, None, -1))
            Iy = (slice(None, None, -1), slice(None, None, +1))
            data = dF0.data + dF1.data

            for ghost_mask in GhostMask.all:
                dF0.initialize(__random_init, exchange_kwds=dict(ghost_mask=ghost_mask))
                dF1.initialize(__random_init, exchange_kwds=dict(ghost_mask=ghost_mask))

                sys.stdout.write("    *{:<6} ".format(str(ghost_mask) + ":"))
                sys.stdout.flush()

                if ghost_mask is GhostMask.FULL:
                    Fx = slice(None, None)
                    Fy = slice(None, None)
                elif ghost_mask is GhostMask.CROSS:
                    # we exclude exterior ghosts because pattern is CROSS
                    # we exclude interior ghost because of boundary clashes:
                    # (dirichlet boundary conditions forces 0)
                    # For the moments zeroes are in the compute domain
                    # and the grid is fully colocated...
                    Fx = Xi[1]
                    Fy = Yi[1]
                else:
                    raise NotImplementedError(ghost_mask)

                try:
                    for i, d in enumerate(data):
                        b = d.get().handle

                        assert b.shape == (Ny + 2 * Gy, Nx + 2 * Gx)
                        assert b[Yo[1], Xo[1]].shape == npts

                        assert b[Yo[0], Xo[0]].shape == nghosts
                        assert b[Yo[0], Xo[2]].shape == nghosts
                        assert b[Yo[2], Xo[0]].shape == nghosts
                        assert b[Yo[2], Xo[2]].shape == nghosts

                        assert b[Yi[0], Xi[0]].shape == nghosts
                        assert b[Yi[0], Xi[2]].shape == nghosts
                        assert b[Yi[2], Xi[0]].shape == nghosts
                        assert b[Yi[2], Xi[2]].shape == nghosts

                        if ghost_mask is GhostMask.FULL:
                            assert b[Fy, Fx].shape == b.shape
                        elif ghost_mask is GhostMask.CROSS:
                            assert b[Fy, Fx].shape == (Ny - 2 * Gy, Nx - 2 * Gx)
                        else:
                            raise NotImplementedError(ghost_mask)

                        if Lx == BoundaryCondition.PERIODIC:
                            assert np.all(b[Fy, Xo[0]] == b[Fy, Xi[2]]), "\n" + str(d)
                        elif Lx == BoundaryCondition.HOMOGENEOUS_DIRICHLET:
                            assert (b[Fy, Gx] == 0).all(), "\n" + str(d)
                            assert (
                                b[Fy, :Gx] == -b[Fy, Gx + 1 : 2 * Gx + 1][Ix]
                            ).all(), "\n" + str(d)
                        elif Lx == BoundaryCondition.HOMOGENEOUS_NEUMANN:
                            assert (
                                b[Fy, :Gx] == +b[Fy, Gx + 1 : 2 * Gx + 1][Ix]
                            ).all(), "\n" + str(d)
                        else:
                            raise NotImplementedError(
                                f"Unknown boundary condition {Lx}."
                            )

                        if Rx == BoundaryCondition.PERIODIC:
                            assert np.all(b[Fy, Xo[2]] == b[Fy, Xi[0]]), "\n" + str(d)
                        elif Rx == BoundaryCondition.HOMOGENEOUS_DIRICHLET:
                            assert (b[Fy, Nx + Gx - 1] == 0).all(), "\n" + str(d)
                            assert (
                                b[Fy, Nx - 1 : Nx + Gx - 1] == -b[Fy, Nx + Gx :][Ix]
                            ).all(), "\n" + str(d)
                        elif Rx == BoundaryCondition.HOMOGENEOUS_NEUMANN:
                            assert (
                                b[Fy, Nx - 1 : Nx + Gx - 1] == +b[Fy, Nx + Gx :][Ix]
                            ).all(), "\n" + str(d)
                        else:
                            raise NotImplementedError(
                                f"Unknown boundary condition {Rx}."
                            )

                        if Ly == BoundaryCondition.PERIODIC:
                            assert np.all(b[Yo[0], Fx] == b[Yi[2], Fx]), "\n" + str(d)
                        elif Ly == BoundaryCondition.HOMOGENEOUS_DIRICHLET:
                            assert (b[Gy, Fx] == 0).all(), "\n" + str(d)
                            assert (
                                b[:Gy, Fx] == -b[Gy + 1 : 2 * Gy + 1, Fx][Iy]
                            ).all(), "\n" + str(d)
                        elif Ly == BoundaryCondition.HOMOGENEOUS_NEUMANN:
                            assert (
                                b[:Gy, Fx] == +b[Gy + 1 : 2 * Gy + 1, Fx][Iy]
                            ).all(), "\n" + str(d)
                        else:
                            raise NotImplementedError(
                                f"Unknown boundary condition {Ly}."
                            )

                        if Ry == BoundaryCondition.PERIODIC:
                            assert np.all(b[Yo[2], Fx] == b[Yi[0], Fx]), "\n" + str(d)
                        elif Ry == BoundaryCondition.HOMOGENEOUS_DIRICHLET:
                            assert (b[Ny + Gy - 1, Fx] == 0).all(), "\n" + str(d)
                            assert (
                                b[Ny - 1 : Ny + Gy - 1, Fx] == -b[Ny + Gy :, Fx][Iy]
                            ).all(), "\n" + str(d)
                        elif Ry == BoundaryCondition.HOMOGENEOUS_NEUMANN:
                            assert (
                                b[Ny - 1 : Ny + Gy - 1, Fx] == +b[Ny + Gy :, Fx][Iy]
                            ).all(), "\n" + str(d)
                        else:
                            raise NotImplementedError(
                                f"Unknown boundary condition {Ry}."
                            )

                        if ghost_mask is GhostMask.FULL:
                            if Lx == Ly == Rx == Ry == BoundaryCondition.PERIODIC:
                                assert np.all(
                                    b[Yo[0], Xo[0]] == b[Yi[2], Xi[2]]
                                ), "\n" + str(d)
                                assert np.all(
                                    b[Yo[2], Xo[0]] == b[Yi[0], Xi[2]]
                                ), "\n" + str(d)
                                assert np.all(
                                    b[Yo[2], Xo[2]] == b[Yi[0], Xi[0]]
                                ), "\n" + str(d)
                                assert np.all(
                                    b[Yo[0], Xo[2]] == b[Yi[2], Xi[0]]
                                ), "\n" + str(d)
                        elif ghost_mask is GhostMask.CROSS:
                            assert np.all(np.isnan(b[Yo[0], Xo[0]])), "\n" + str(d)
                            assert np.all(np.isnan(b[Yo[2], Xo[0]])), "\n" + str(d)
                            assert np.all(np.isnan(b[Yo[2], Xo[2]])), "\n" + str(d)
                            assert np.all(np.isnan(b[Yo[0], Xo[2]])), "\n" + str(d)
                        else:
                            msg = f"Unknown ghost mask {ghost_mask}."
                            raise NotImplementedError(msg)
                        sys.stdout.write(".")
                        sys.stdout.flush()
                finally:
                    print()


def test_serial_initialization_3d():
    print()
    print("test_serial_initialization_3d()")
    dim = 3
    npts = (8, 5, 5)
    nghosts = (3, 1, 2)
    for lbd, rbd in domain_boundary_iterator(dim):
        domain = Box(dim=dim, lboundaries=lbd, rboundaries=rbd)
        F0 = Field(domain=domain, name="F0", nb_components=1)
        F1 = Field(domain=domain, name="F1", nb_components=3)
        print(f"[{F0.format_boundaries()}]")

        discretization = CartesianDiscretization(
            npts, nghosts, lboundaries=F0.lboundaries, rboundaries=F0.rboundaries
        )

        topo0 = CartesianTopology(
            domain=domain, discretization=discretization, backend=Backend.HOST
        )
        if __HAS_OPENCL_BACKEND__:
            topos = (topo0,) + tuple(
                CartesianTopology(
                    domain=domain,
                    discretization=discretization,
                    backend=Backend.OPENCL,
                    cl_env=cl_env,
                )
                for cl_env in iter_clenv()
            )
        else:
            topos = (topo0,)

        assert all(np.all(t.mesh.local_lboundaries == F0.lboundaries) for t in topos)
        assert all(np.all(t.mesh.local_rboundaries == F0.rboundaries) for t in topos)

        for topo in topos:
            sys.stdout.write(f"  {topo.full_pretty_tag}::{topo.backend.kind}\n")
            sys.stdout.flush()

            dF0 = F0.discretize(topo)
            dF1 = F1.discretize(topo)

            dfields = dF0.dfields + dF1.dfields
            assert all(np.all(d.local_lboundaries == F0.lboundaries) for d in dfields)
            assert all(np.all(d.local_rboundaries == F0.rboundaries) for d in dfields)

            Nz, Ny, Nx = npts
            Gz, Gy, Gx = nghosts
            Lz, Ly, Lx = tuple(discretization.lboundaries)
            Rz, Ry, Rx = tuple(discretization.rboundaries)
            Xo = (slice(0, Gx), slice(Gx, Gx + Nx), slice(Gx + Nx, None))
            Yo = (slice(0, Gy), slice(Gy, Gy + Ny), slice(Gy + Ny, None))
            Zo = (slice(0, Gz), slice(Gz, Gz + Nz), slice(Gz + Nz, None))
            Xi = (slice(Gx, 2 * Gx), slice(2 * Gx, Nx), slice(Nx, Gx + Nx))
            Yi = (slice(Gy, 2 * Gy), slice(2 * Gy, Ny), slice(Ny, Gy + Ny))
            Zi = (slice(Gz, 2 * Gz), slice(2 * Gz, Nz), slice(Nz, Gz + Nz))
            Ix = (slice(None, None, +1), slice(None, None, +1), slice(None, None, -1))
            Iy = (slice(None, None, +1), slice(None, None, -1), slice(None, None, +1))
            Iz = (slice(None, None, -1), slice(None, None, +1), slice(None, None, +1))
            data = dF0.data + dF1.data

            for ghost_mask in GhostMask.all:
                dF0.initialize(__random_init, exchange_kwds=dict(ghost_mask=ghost_mask))
                dF1.initialize(__random_init, exchange_kwds=dict(ghost_mask=ghost_mask))

                sys.stdout.write("    *{:<6} ".format(str(ghost_mask) + ":"))
                sys.stdout.flush()

                if ghost_mask is GhostMask.FULL:
                    Fx = slice(None, None)
                    Fy = slice(None, None)
                    Fz = slice(None, None)
                elif ghost_mask is GhostMask.CROSS:
                    # we exclude exterior ghosts because pattern is CROSS
                    # we exclude interior ghost because of boundary clashes:
                    # (dirichlet boundary conditions forces 0)
                    # For the moments zeroes are in the compute domain
                    # and the grid is fully colocated...
                    Fx = Xi[1]
                    Fy = Yi[1]
                    Fz = Zi[1]
                else:
                    raise NotImplementedError(ghost_mask)

                try:
                    for i, d in enumerate(data):
                        b = d.get().handle
                        assert b.shape == (Nz + 2 * Gz, Ny + 2 * Gy, Nx + 2 * Gx)
                        assert b[Zo[1], Yo[1], Xo[1]].shape == npts

                        assert b[Zo[0], Yo[0], Xo[0]].shape == nghosts
                        assert b[Zo[0], Yo[0], Xo[2]].shape == nghosts
                        assert b[Zo[0], Yo[2], Xo[0]].shape == nghosts
                        assert b[Zo[0], Yo[2], Xo[2]].shape == nghosts
                        assert b[Zo[2], Yo[0], Xo[0]].shape == nghosts
                        assert b[Zo[2], Yo[0], Xo[2]].shape == nghosts
                        assert b[Zo[2], Yo[2], Xo[0]].shape == nghosts
                        assert b[Zo[2], Yo[2], Xo[2]].shape == nghosts

                        assert b[Zi[0], Yi[0], Xi[0]].shape == nghosts
                        assert b[Zi[0], Yi[0], Xi[2]].shape == nghosts
                        assert b[Zi[0], Yi[2], Xi[0]].shape == nghosts
                        assert b[Zi[0], Yi[2], Xi[2]].shape == nghosts
                        assert b[Zi[2], Yi[0], Xi[0]].shape == nghosts
                        assert b[Zi[2], Yi[0], Xi[2]].shape == nghosts
                        assert b[Zi[2], Yi[2], Xi[0]].shape == nghosts
                        assert b[Zi[2], Yi[2], Xi[2]].shape == nghosts

                        if ghost_mask is GhostMask.FULL:
                            assert b[Fz, Fy, Fx].shape == b.shape
                        elif ghost_mask is GhostMask.CROSS:
                            assert b[Fz, Fy, Fx].shape == (
                                Nz - 2 * Gz,
                                Ny - 2 * Gy,
                                Nx - 2 * Gx,
                            )
                        else:
                            raise NotImplementedError(ghost_mask)

                        if Lx == BoundaryCondition.PERIODIC:
                            assert np.all(
                                b[Fz, Fy, Xo[0]] == b[Fz, Fy, Xi[2]]
                            ), "\n" + str(d)
                        elif Lx == BoundaryCondition.HOMOGENEOUS_DIRICHLET:
                            assert (b[Fz, Fy, Gx] == 0).all(), "\n" + str(d)
                            assert (
                                b[Fz, Fy, :Gx] == -b[Fz, Fy, Gx + 1 : 2 * Gx + 1][Ix]
                            ).all(), "\n" + str(d)
                        elif Lx == BoundaryCondition.HOMOGENEOUS_NEUMANN:
                            assert (
                                b[Fz, Fy, :Gx] == +b[Fz, Fy, Gx + 1 : 2 * Gx + 1][Ix]
                            ).all(), "\n" + str(d)
                        else:
                            raise NotImplementedError(
                                f"Unknown boundary condition {Lx}."
                            )

                        if Rx == BoundaryCondition.PERIODIC:
                            assert np.all(
                                b[Fz, Fy, Xo[2]] == b[Fz, Fy, Xi[0]]
                            ), "\n" + str(d)
                        elif Rx == BoundaryCondition.HOMOGENEOUS_DIRICHLET:
                            assert (b[Fz, Fy, Nx + Gx - 1] == 0).all(), "\n" + str(d)
                            assert (
                                b[Fz, Fy, Nx - 1 : Nx + Gx - 1]
                                == -b[Fz, Fy, Nx + Gx :][Ix]
                            ).all(), "\n" + str(d)
                        elif Rx == BoundaryCondition.HOMOGENEOUS_NEUMANN:
                            assert (
                                b[Fz, Fy, Nx - 1 : Nx + Gx - 1]
                                == +b[Fz, Fy, Nx + Gx :][Ix]
                            ).all(), "\n" + str(d)
                        else:
                            raise NotImplementedError(
                                f"Unknown boundary condition {Rx}."
                            )

                        if Ly == BoundaryCondition.PERIODIC:
                            assert np.all(
                                b[Fz, Yo[0], Fx] == b[Fz, Yi[2], Fx]
                            ), "\n" + str(d)
                        elif Ly == BoundaryCondition.HOMOGENEOUS_DIRICHLET:
                            assert (b[Fz, Gy, Fx] == 0).all(), "\n" + str(d)
                            assert (
                                b[Fz, :Gy, Fx] == -b[Fz, Gy + 1 : 2 * Gy + 1, Fx][Iy]
                            ).all(), "\n" + str(d)
                        elif Ly == BoundaryCondition.HOMOGENEOUS_NEUMANN:
                            assert (
                                b[Fz, :Gy, Fx] == +b[Fz, Gy + 1 : 2 * Gy + 1, Fx][Iy]
                            ).all(), "\n" + str(d)
                        else:
                            raise NotImplementedError(
                                f"Unknown boundary condition {Ly}."
                            )

                        if Ry == BoundaryCondition.PERIODIC:
                            assert np.all(
                                b[Fz, Yo[2], Fx] == b[Fz, Yi[0], Fx]
                            ), "\n" + str(d)
                        elif Ry == BoundaryCondition.HOMOGENEOUS_DIRICHLET:
                            assert (b[Fz, Ny + Gy - 1, Fx] == 0).all(), "\n" + str(d)
                            assert (
                                b[Fz, Ny - 1 : Ny + Gy - 1, Fx]
                                == -b[Fz, Ny + Gy :, Fx][Iy]
                            ).all(), "\n" + str(d)
                        elif Ry == BoundaryCondition.HOMOGENEOUS_NEUMANN:
                            assert (
                                b[Fz, Ny - 1 : Ny + Gy - 1, Fx]
                                == +b[Fz, Ny + Gy :, Fx][Iy]
                            ).all(), "\n" + str(d)
                        else:
                            raise NotImplementedError(
                                f"Unknown boundary condition {Ry}."
                            )

                        if Lz == BoundaryCondition.PERIODIC:
                            assert np.all(
                                b[Zo[0], Fy, Fx] == b[Zi[2], Fy, Fx]
                            ), "\n" + str(d)
                        elif Lz == BoundaryCondition.HOMOGENEOUS_DIRICHLET:
                            assert (b[Gz, Fy, Fx] == 0).all(), "\n" + str(d)
                            assert (
                                b[:Gz, Fy, Fx] == -b[Gz + 1 : 2 * Gz + 1, Fy, Fx][Iz]
                            ).all(), "\n" + str(d)
                        elif Lz == BoundaryCondition.HOMOGENEOUS_NEUMANN:
                            assert (
                                b[:Gz, Fy, Fx] == +b[Gz + 1 : 2 * Gz + 1, Fy, Fx][Iz]
                            ).all(), "\n" + str(d)
                        else:
                            raise NotImplementedError(
                                f"Unknown boundary condition {Lz}."
                            )

                        if Rz == BoundaryCondition.PERIODIC:
                            assert np.all(
                                b[Zo[2], Fy, Fx] == b[Zi[0], Fy, Fx]
                            ), "\n" + str(d)
                        elif Rz == BoundaryCondition.HOMOGENEOUS_DIRICHLET:
                            assert (b[Nz + Gz - 1, Fy, Fx] == 0).all(), "\n" + str(d)
                            assert (
                                b[Nz - 1 : Nz + Gz - 1, Fy, Fx]
                                == -b[Nz + Gz :, Fy, Fx][Iz]
                            ).all(), "\n" + str(d)
                        elif Rz == BoundaryCondition.HOMOGENEOUS_NEUMANN:
                            assert (
                                b[Nz - 1 : Nz + Gz - 1, Fy, Fx]
                                == +b[Nz + Gz :, Fy, Fx][Iz]
                            ).all(), "\n" + str(d)
                        else:
                            raise NotImplementedError(
                                f"Unknown boundary condition {Rz}."
                            )

                        if ghost_mask is GhostMask.FULL:
                            if (
                                Lx
                                == Ly
                                == Lz
                                == Rx
                                == Ry
                                == Rz
                                == BoundaryCondition.PERIODIC
                            ):
                                assert np.all(
                                    b[Zo[0], Yo[0], Xo[0]] == b[Zi[2], Yi[2], Xi[2]]
                                )
                                assert np.all(
                                    b[Zo[2], Yo[0], Xo[0]] == b[Zi[0], Yi[2], Xi[2]]
                                )
                                assert np.all(
                                    b[Zo[2], Yo[2], Xo[0]] == b[Zi[0], Yi[0], Xi[2]]
                                )
                                assert np.all(
                                    b[Zo[0], Yo[2], Xo[0]] == b[Zi[2], Yi[0], Xi[2]]
                                )
                                assert np.all(
                                    b[Zo[0], Yo[0], Xo[2]] == b[Zi[2], Yi[2], Xi[0]]
                                )
                                assert np.all(
                                    b[Zo[2], Yo[0], Xo[2]] == b[Zi[0], Yi[2], Xi[0]]
                                )
                                assert np.all(
                                    b[Zo[2], Yo[2], Xo[2]] == b[Zi[0], Yi[0], Xi[0]]
                                )
                                assert np.all(
                                    b[Zo[0], Yo[2], Xo[2]] == b[Zi[2], Yi[0], Xi[0]]
                                )
                        elif ghost_mask is GhostMask.CROSS:
                            assert np.all(np.isnan(b[Zo[0], Yo[0], Xo[0]]))
                            assert np.all(np.isnan(b[Zo[2], Yo[0], Xo[0]]))
                            assert np.all(np.isnan(b[Zo[2], Yo[2], Xo[0]]))
                            assert np.all(np.isnan(b[Zo[0], Yo[2], Xo[0]]))
                            assert np.all(np.isnan(b[Zo[0], Yo[0], Xo[2]]))
                            assert np.all(np.isnan(b[Zo[2], Yo[0], Xo[2]]))
                            assert np.all(np.isnan(b[Zo[2], Yo[2], Xo[2]]))
                            assert np.all(np.isnan(b[Zo[0], Yo[2], Xo[2]]))
                        else:
                            msg = f"Unknown ghost mask {ghost_mask}."
                            raise NotImplementedError(msg)
                        sys.stdout.write(".")
                        sys.stdout.flush()
                finally:
                    print()


def iter_backends():
    yield (Backend.HOST, None)
    if __HAS_OPENCL_BACKEND__:
        for cl_env in iter_clenv():
            yield (Backend.OPENCL, cl_env)


def test_mpi_ghost_exchange_periodic(comm=None):
    if comm is None:
        from mpi4py import MPI

        comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
    dtypes = (
        np.float32,
        np.float32,
        np.float64,
        np.int16,
        np.int32,
        np.int64,
        np.uint16,
        np.uint32,
        np.uint64,
    )
    assert size - 1 < len(dtypes)
    if rank == 0:
        print()
        msg = f"*** COMM_WORLD_SIZE {size} ***"
        print()
        print("*" * len(msg))
        print(msg)
        print("*" * len(msg))
        print(f"test_mpi_ghost_exchange_periodic()")
    for dim in range(1, 3 + __ENABLE_LONG_TESTS__):
        if rank == 0:
            print(f"  >DIM={dim}")

        npts = (53, 47, 59, 23)[:dim]
        nghosts = (2, 1, 0, 3)[:dim]
        discretization = CartesianDiscretization(npts, nghosts, default_boundaries=True)
        domain = Box(dim=dim)

        for dtype in dtypes[size - 1 : size]:
            if rank == 0:
                print(f"    >DTYPE={dtype}")

            F0 = Field(domain=domain, name="F0", nb_components=1, dtype=dtype)
            F1 = Field(domain=domain, name="F1", nb_components=2, dtype=dtype)
            F2 = Field(domain=domain, name="F2", shape=(2, 2), dtype=dtype)

            for backend, cl_env in iter_backends():
                if rank == 0:
                    print(
                        "      >BACKEND.{}{}".format(
                            backend,
                            (
                                ""
                                if (cl_env is None)
                                else "::{}.{}".format(
                                    cl_env.platform.name.strip(),
                                    cl_env.device.name.strip(),
                                )
                            ),
                        )
                    )
                for shape in it.product(range(0, size + 1), repeat=dim):
                    if np.prod(shape, dtype=np.uint32) != size:
                        continue
                    if rank == 0:
                        print(f"         *cart_shape: {shape}")
                    topo = CartesianTopology(
                        domain=domain,
                        discretization=discretization,
                        backend=backend,
                        cart_shape=shape,
                        cl_env=cl_env,
                    )
                    assert (topo.proc_shape == shape).all()

                    def ghost_base(i, d, rank, local_dir):
                        return (
                            (i + 1) * 17
                            + (d + 1) * 13
                            + (local_dir + 1) * 11
                            + (rank + 1) * 7
                        )

                    def ghost_vals(shape, dtype, i, d, rank, local_dir):
                        if (shape is None) or (len(shape) == 0):
                            raise ValueError(
                                f"Shape is None or an empty tuple: {shape}"
                            )
                        base = np.full(
                            shape=shape,
                            dtype=dtype,
                            fill_value=ghost_base(i, d, rank, local_dir),
                        )
                        I = np.ix_(
                            *tuple(
                                np.arange(shape[direction], dtype=dtype)
                                for direction in range(len(shape))
                            )
                        )
                        return base + I[d]

                    for F in (F0, F1, F2):
                        dF = F.discretize(topo)
                        if rank == 0:
                            print(f"          |{F.nb_components} COMPONENT(S)")
                        for exchange_method in (
                            ExchangeMethod.ISEND_IRECV,
                            ExchangeMethod.NEIGHBOR_ALL_TO_ALL_V,
                            ExchangeMethod.NEIGHBOR_ALL_TO_ALL_W,
                        ):
                            if rank == 0:
                                print(
                                    f"             ExchangeMethod.{exchange_method} |",
                                    end=" ",
                                )
                            sys.stdout.flush()
                            for d in range(dim):
                                dF.initialize(__zero_init, exchange_ghosts=False)
                                if rank == 0:
                                    print(DirectionLabels[dim - 1 - d], end=" ")
                                sys.stdout.flush()
                                lghosts, rghosts, shape = dF.inner_ghost_slices[d]
                                _lghosts, _rghosts, shape = dF.outer_ghost_slices[d]

                                if shape is not None:
                                    for i, data in enumerate(dF.data):
                                        data[lghosts] = ghost_vals(
                                            shape, dtype, i, d, rank, 0
                                        )
                                        data[_lghosts] = -10
                                        data[rghosts] = ghost_vals(
                                            shape, dtype, i, d, rank, 1
                                        )
                                        data[_rghosts] = +10

                                dF.exchange_ghosts(
                                    directions=d, exchange_method=exchange_method
                                )

                                lghosts, rghosts, shape = dF.outer_ghost_slices[d]
                                left_rank, right_rank = topo.proc_neighbour_ranks[:, d]
                                if left_rank == -1:
                                    assert right_rank == -1
                                    left_rank, right_rank = rank, rank

                                if shape is not None:
                                    for i, data in enumerate(dF.data):
                                        ldata = data[lghosts]
                                        rdata = data[rghosts]

                                        ldata = np.atleast_1d(ldata.get())
                                        target_vals = ghost_vals(
                                            ldata.shape, dtype, i, d, left_rank, 1
                                        )
                                        assert np.allclose(ldata, target_vals), (
                                            rank,
                                            target_vals,
                                        )
                                        rdata = np.atleast_1d(rdata.get())
                                        target_vals = ghost_vals(
                                            rdata.shape, dtype, i, d, right_rank, 0
                                        )
                                        assert np.allclose(rdata, target_vals), (
                                            rank,
                                            target_vals,
                                        )
                            if rank == 0:
                                print()


def test_mpi_ghost_exchange_runtime(comm=None):
    """Just bruteforce all exchange possibilities to see if nothing crashes in 1D and 2D."""
    if comm is None:
        from mpi4py import MPI

        comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
    dtype = np.float32

    if rank == 0:
        print()
        msg = f"*** COMM_WORLD_SIZE {size} ***"
        print()
        print("*" * len(msg))
        print(msg)
        print("*" * len(msg))
        print(f"test_mpi_ghost_exchange_runtime()")

    for dim in range(1, 3 + __ENABLE_LONG_TESTS__):
        if rank == 0:
            sys.stdout.write(f">DIM={dim}\n")

        npts = (17, 16, 19)[:dim]
        nghosts = (2, 1, 3)[:dim]

        for shape in it.product(range(0, size + 1), repeat=dim):
            if np.prod(shape, dtype=np.uint32) != size:
                continue
            if rank == 0:
                sys.stdout.write(f"  >CART SHAPE: {shape}\n")

            for backend, cl_env in iter_backends():
                if rank == 0:
                    sys.stdout.write("    >BACKEND.{:<7} ".format(str(backend) + ":"))

                def breakline(i):
                    if (rank == 0) and ((i + 1) % 63 == 0):
                        sys.stdout.write("\n" + " " * 21)
                        sys.stdout.flush()
                        return True
                    return False

                i = 0
                brk = False
                try:
                    for lbd, rbd in domain_boundary_iterator(dim):
                        domain = Box(dim=dim, lboundaries=lbd, rboundaries=rbd)

                        F = Field(domain=domain, name="F", nb_components=1, dtype=dtype)

                        discretization = CartesianDiscretization(
                            npts,
                            nghosts,
                            lboundaries=F.lboundaries,
                            rboundaries=F.rboundaries,
                        )

                        topo = CartesianTopology(
                            domain=domain,
                            discretization=discretization,
                            backend=backend,
                            cart_shape=shape,
                            cl_env=cl_env,
                        )
                        assert (topo.proc_shape == shape).all()

                        dF = F.discretize(topo)
                        dF.initialize(__random_init, exchange_ghosts=False)
                        for exchange_method in (
                            ExchangeMethod.ISEND_IRECV,
                            ExchangeMethod.NEIGHBOR_ALL_TO_ALL_V,
                            ExchangeMethod.NEIGHBOR_ALL_TO_ALL_W,
                        ):
                            for d in range(dim):
                                dF.exchange_ghosts(
                                    directions=d, exchange_method=exchange_method
                                )
                                if rank == 0:
                                    sys.stdout.write(".")
                                    brk = breakline(i)
                                    i += 1

                                dF.accumulate_ghosts(
                                    directions=d, exchange_method=exchange_method
                                )
                                if rank == 0:
                                    sys.stdout.write(".")
                                    brk = breakline(i)
                                    i += 1
                                    sys.stdout.flush()
                finally:
                    if rank == 0:
                        sys.stdout.write("\n")
                        sys.stdout.flush()


def test_mpi_ghost_accumulate_periodic(comm=None):
    if comm is None:
        from mpi4py import MPI

        comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
    if rank == 0:
        msg = f"*** COMM_WORLD_SIZE {size} ***"
        print()
        print("*" * len(msg))
        print(msg)
        print("*" * len(msg))
        print(f"test_mpi_ghost_accumulate_periodic()")

    dtypes = (
        np.float32,
        np.float32,
        np.float64,
        np.complex64,
        np.complex128,
        np.int16,
        np.int32,
        np.int64,
        np.uint16,
        np.uint32,
        np.uint64,
    )
    assert size - 1 < len(dtypes)
    for dim in range(1, 3 + __ENABLE_LONG_TESTS__):
        if rank == 0:
            print(f"  >DIM={dim}")

        npts = (53, 57, 51, 49)[:dim]
        nghosts = (1, 3, 0, 2)[:dim]
        discretization = CartesianDiscretization(npts, nghosts, default_boundaries=True)
        domain = Box(dim=dim)

        for dtype in dtypes[size - 1 : size]:
            if rank == 0:
                print(f"    >DTYPE={dtype}")

            F0 = Field(domain=domain, name="F0", nb_components=1, dtype=dtype)
            F1 = Field(domain=domain, name="F1", nb_components=2, dtype=dtype)
            F2 = Field(domain=domain, name="F2", shape=(2, 2), dtype=dtype)

            for backend, cl_env in iter_backends():
                if rank == 0:
                    print(
                        "      >BACKEND.{}{}".format(
                            backend,
                            (
                                ""
                                if (cl_env is None)
                                else "::{}.{}".format(
                                    cl_env.platform.name.strip(),
                                    cl_env.device.name.strip(),
                                )
                            ),
                        )
                    )
                for shape in it.product(range(0, size + 1), repeat=dim):
                    if np.prod(shape, dtype=np.uint32) != size:
                        continue
                    if rank == 0:
                        print(f"        *cart_shape: {shape}")
                    topo = CartesianTopology(
                        domain=domain,
                        discretization=discretization,
                        backend=backend,
                        cart_shape=shape,
                        cl_env=cl_env,
                    )
                    assert (topo.proc_shape == shape).all()

                    def ghost_base(rank, directions, displacements, i):
                        disweight = np.asarray([19, 23, 29, 31], dtype=np.int32)
                        dirweight = np.asarray([37, 41, 43, 51], dtype=np.int32)
                        directions = np.asarray(directions, dtype=np.int32) + 1
                        displacements = np.asarray(displacements, dtype=np.int32) + 2
                        tag = (rank + 1) * 17 + (i + 1) * 13
                        tag += dirweight[: directions.size].dot(directions)
                        tag += disweight[: displacements.size].dot(displacements)
                        return tag

                    def ghost_vals(shape, dtype, rank, directions, displacements, i):
                        if (shape is None) or (len(shape) == 0):
                            raise ValueError(
                                f"Shape is None or an empty tuple: {shape}"
                            )
                        base_value = ghost_base(rank, directions, displacements, i)
                        vals = np.full(shape=shape, fill_value=base_value, dtype=dtype)
                        return vals

                    for F in (F0, F1, F2):
                        dF = F.discretize(topo)
                        dF.initialize(__random_init, exchange_ghosts=False)
                        proc_ranks = topo.proc_ranks
                        proc_shape = topo.proc_shape
                        proc_coords = tuple(topo.proc_coords.tolist())
                        assert proc_ranks[proc_coords] == rank
                        all_inner_ghost_slices = dF.all_inner_ghost_slices
                        all_outer_ghost_slices = dF.all_outer_ghost_slices
                        if rank == 0:
                            print(f"          |{F.nb_components} COMPONENT(S)")
                        for exchange_method in (
                            ExchangeMethod.ISEND_IRECV,
                            ExchangeMethod.NEIGHBOR_ALL_TO_ALL_V,
                            ExchangeMethod.NEIGHBOR_ALL_TO_ALL_W,
                        ):
                            if rank == 0:
                                print(
                                    f"             ExchangeMethod.{str(exchange_method):<25} |",
                                    end=" ",
                                )
                            sys.stdout.flush()

                            # test one direction at a time
                            max_displacements = 1
                            for ndirections in range(1, dim + 1):
                                all_displacements = tuple(
                                    it.product((-1, 0, +1), repeat=ndirections)
                                )
                                all_directions = tuple(
                                    it.combinations(range(dim), ndirections)
                                )
                                masks = tuple(it.product((0, 1), repeat=ndirections))
                                for directions in all_directions:
                                    if rank == 0:
                                        if directions:
                                            print(
                                                "".join(
                                                    DirectionLabels[dim - 1 - d]
                                                    for d in directions
                                                ),
                                                end=" ",
                                            )
                                        else:
                                            print("--", end=" ")
                                        sys.stdout.flush()

                                    for i, data in enumerate(dF.data):
                                        data[...] = (rank + 1) * (i + 1)
                                        for displacements in all_displacements:
                                            if sum(d != 0 for d in displacements) == 0:
                                                continue
                                            (iview, ishape) = all_inner_ghost_slices[
                                                ndirections
                                            ][directions][displacements]
                                            (oview, oshape) = all_outer_ghost_slices[
                                                ndirections
                                            ][directions][displacements]
                                            if oshape is not None:
                                                assert ishape is not None
                                                data[oview] = ghost_vals(
                                                    oshape,
                                                    dtype,
                                                    rank,
                                                    directions,
                                                    displacements,
                                                    i,
                                                )

                                    dF.accumulate_ghosts(
                                        directions=directions,
                                        exchange_method=exchange_method,
                                    )

                                    for i, data in enumerate(dF.data):
                                        for displacements in all_displacements:
                                            ndisplacements = sum(
                                                d != 0 for d in displacements
                                            )
                                            (iview, ishape) = all_inner_ghost_slices[
                                                ndirections
                                            ][directions][displacements]
                                            (oview, oshape) = all_outer_ghost_slices[
                                                ndirections
                                            ][directions][displacements]

                                            if ishape is None:
                                                assert oshape is None
                                                continue

                                            assert np.array_equal(
                                                data[iview].shape, ishape
                                            )
                                            assert np.array_equal(
                                                data[oview].shape, oshape
                                            )

                                            overlaping_neighbours = {
                                                tuple(
                                                    (
                                                        np.asarray(mask, dtype=np.int32)
                                                        * displacements
                                                    ).tolist()
                                                )
                                                for mask in masks
                                            }
                                            overlaping_neighbours = filter(
                                                lambda x: 0
                                                < sum(_ != 0 for _ in x)
                                                <= max_displacements,
                                                overlaping_neighbours,
                                            )  # diagonals
                                            overlaping_neighbours = tuple(
                                                overlaping_neighbours
                                            )

                                            expected_values = (rank + 1) * (i + 1)
                                            for disp in overlaping_neighbours:
                                                ncoords = ()
                                                j = 0
                                                for _ in range(dim):
                                                    if _ in directions:
                                                        ci = (
                                                            proc_shape[_]
                                                            + proc_coords[_]
                                                            + disp[j]
                                                        ) % proc_shape[_]
                                                        j += 1
                                                    else:
                                                        ci = proc_coords[_]
                                                    ncoords += (ci,)
                                                nrank = proc_ranks[ncoords]
                                                neg_disp = tuple(-_ for _ in disp)
                                                expected_values += ghost_vals(
                                                    ishape,
                                                    dtype,
                                                    nrank,
                                                    directions,
                                                    neg_disp,
                                                    i,
                                                )
                                            idata = data[iview].get()
                                            odata = data[oview].get()
                                            assert np.allclose(idata, expected_values)
                                            if ndisplacements > 0:
                                                assert np.allclose(
                                                    odata,
                                                    ghost_vals(
                                                        oshape,
                                                        dtype,
                                                        rank,
                                                        directions,
                                                        displacements,
                                                        i,
                                                    ),
                                                )
                                continue
                            if rank == 0:
                                print()


if __name__ == "__main__":
    from mpi4py import MPI

    comm = MPI.COMM_WORLD
    size = comm.Get_size()

    from hysop.tools.warning import disable_hysop_warnings

    with test_context():
        if size == 1:
            test_serial_initialization_1d()
            test_serial_initialization_2d()
            test_serial_initialization_3d()

        disable_hysop_warnings()
        test_mpi_ghost_exchange_runtime(comm=comm)
        test_mpi_ghost_exchange_periodic(comm=comm)
        test_mpi_ghost_accumulate_periodic(comm=comm)
