# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABCMeta, abstractmethod
import numpy as np

from hysop.tools.htypes import check_instance
from hysop.tools.units import bytes2str


class Buffer:
    """
    Base class for releasable buffers.
    A buffer should just offer a release method to be compatible with allocators.

    Host buffers are numpy buffers.
    Cuda and OpenCl buffers are the one provided by pycuda and pyopencl.

    Buffers should be obtained through allocators or memory pools.
    """

    # /!\ ptr, size and int_ptr properties should be redefined in child classes.
    _DEBUG = False

    def __init__(self, size, **kwds):
        assert size is not None
        self._size = size
        if self._DEBUG:
            print(f"Initializing {self.__class__.__name__} of size {bytes2str(size)}.")
        try:
            super().__init__(size=size, **kwds)
        except TypeError:
            try:
                super().__init__(**kwds)
            except:
                pass

    def __del__(self):
        if self._DEBUG:
            print(f"Releasing {self.__class__.__name__}[{id(self)}].")

    @abstractmethod
    def release(self):
        """
        Release this buffer (decrease internal implementation defined reference counter).
        """
        super().release()

    @classmethod
    def from_int_ptr(cls, int_ptr_value, **kargs):
        """
        Constructs a pyopencl handle from a C-level pointer (given as the integer int_ptr_value).
        If the previous owner of the object owns the handle and will not release it,
        on can set retain to False, to effectively transfer ownership.
        Setting retain to True should increase an implementation specific reference counter.
        The buffer will be freed when buffer reference counter is 0.
        """
        msg = "Buffer.from_int_ptr() is not implemented and should never have been called."
        raise RuntimeError(msg)

    @abstractmethod
    def aligned_view(self, alignment, size=None):
        """
        Return a view of this buffer with an offset such that
        the returned buffer is now aligned on min_alignment
        and has now given size.
        """
        pass

    def get_buffer(self):
        return self

    def get_size(self):
        return self._size

    buf = property(get_buffer)
    size = property(get_size)


class PooledBuffer(Buffer):
    """
    Memory pool allocated buffer wrapper.
    """

    def __init__(self, pool, buf, alloc_sz, size, alignment, **kwds):
        """
        Create a memory pool wrapped buffer.
        Input buffer buf was allocated by given pool with real size alloc_sz.
        Buffer is resized and aligned to given size and alignment.
        On destruction, buffer will be given back to the pool.
        """
        from hysop.core.memory.mempool import MemoryPool

        super().__init__(size=size, **kwds)
        if PooledBuffer._DEBUG:
            print(
                f"pooled buffer size={size}, alignment={alignment}, real_size={alloc_sz}, id={id(self)}, (src_buffer={type(buf)}, src_id={id(buf)})"
            )
        assert alloc_sz >= size + alignment - 1
        check_instance(pool, MemoryPool)
        check_instance(buf, Buffer)
        self._alloc_size = alloc_sz
        self._pool = pool
        self._buf = buf
        self._bufview = buf.aligned_view(size=size, alignment=alignment)

    def get_buf(self):
        """
        Get wrapped buffer handle.
        """
        return self._bufview

    def get_pool(self):
        """
        Get pool that allocated this buffer.
        """
        return self._pool

    def get_int_ptr(self):
        """
        Get wrapped buffer handle pointer as an int.
        """
        return self._bufview.get_int_ptr()

    def aligned_view(self, alignment, size=None):
        return self._bufview.aligned_view(alignment=alignment, size=size)

    def release(self):
        """
        Tells the memory pool that this buffer has no longer to be held.
        """
        if PooledBuffer._DEBUG:
            print(f"pooled buffer release() (id={id(self)})")
        self._pool.free(self._buf, self._size)
        self._buf = None
        self._bufview = None

    def __del__(self):
        if PooledBuffer._DEBUG:
            print(f"pooled buffer __del__() (id={id(self)})")
        if hasattr(self, "_buf") and (self._buf is not None):
            self.release()

    buf = property(get_buf)
    pool = property(get_pool)
    int_ptr = property(get_int_ptr)
