# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np

from hysop.testsenv import (
    opencl_failed,
    iter_clenv,
    __HAS_OPENCL_BACKEND__,
    __ENABLE_LONG_TESTS__,
)
from hysop.core.memory.mempool import MemoryPool

import random

max_bytes_per_alloc = 1024 * 1024 * 128  # 128MB


def free():
    return bool(random.random() > 0.1)  # 80% probability of free


def nbytes():
    return int(2.0 ** (np.log2(max_bytes_per_alloc) * random.random()))


def test_mempool_python_allocator():
    from hysop.backend.host.host_allocator import HostAllocator

    allocator = HostAllocator()
    _test_mempool_allocator("python", allocator)


# @opencl_failed
# def test_mempool_opencl_immediate_allocator():
#     from hysop.backend.device.opencl.opencl_allocator import OpenClImmediateAllocator

#     for cl_env in iter_clenv():
#         allocator = OpenClImmediateAllocator(queue=cl_env.default_queue)
#         _test_mempool_allocator(cl_env.platform.name, allocator)


def _test_mempool_allocator(name, allocator):
    pool = allocator.memory_pool(name=name)
    buffers = []
    try:
        for _ in range(10000):
            size = nbytes()
            buf = pool.allocate(size)
            if not free():
                buffers.append(buf)
    except MemoryError:
        pass


if __name__ == "__main__":
    test_mempool_python_allocator()
    # if __HAS_OPENCL_BACKEND__:
    #     test_mempool_opencl_immediate_allocator()
