# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.backend import __HAS_CUDA_BACKEND__, __HAS_OPENCL_BACKEND__
from hysop.backend.host.host_allocator import HostAllocator
from hysop.core.mpi import default_mpi_params

sizes = (1, 89, 7919, 32 * 1024 * 1024)
alignments = (1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024)


def test_host_allocator():
    allocator = HostAllocator()

    for nbytes in sizes:
        for alignment in alignments:
            buf = allocator.allocate(nbytes)
            assert buf.size == nbytes
            buf = allocator(nbytes)
            assert buf.size == nbytes
            buf = allocator.allocate_aligned(nbytes, alignment)
            assert buf.size == nbytes
            assert buf.int_ptr % alignment == 0
            buf = allocator(nbytes, alignment)
            assert buf.size == nbytes
            assert buf.int_ptr % alignment == 0


if __HAS_OPENCL_BACKEND__:
    from hysop.backend.device.opencl import cl
    from hysop.backend.device.opencl.opencl_allocator import (
        OpenClDeferredAllocator,
        OpenClImmediateAllocator,
    )
    from hysop.backend.device.opencl.opencl_tools import get_or_create_opencl_env

    cl_env = get_or_create_opencl_env(mpi_params=default_mpi_params())

    def test_opencl_immediate_allocator():
        allocator = OpenClImmediateAllocator(queue=cl_env.default_queue)
        for nbytes in sizes:
            buf = allocator.allocate(nbytes)
            assert buf.size == nbytes

    def test_opencl_deferred_allocator():
        allocator = OpenClDeferredAllocator(queue=cl_env.default_queue)
        for nbytes in sizes:
            buf = allocator.allocate(nbytes)
            cl.enqueue_barrier(cl_env.default_queue)
            assert buf.size == nbytes


if __name__ == "__main__":
    test_host_allocator()
    if __HAS_OPENCL_BACKEND__:
        test_opencl_immediate_allocator()
        test_opencl_deferred_allocator()
