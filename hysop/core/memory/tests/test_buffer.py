# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np

from hysop.backend.host.host_buffer import HostBuffer
from hysop.core.mpi import default_mpi_params
from hysop.backend import __HAS_OPENCL_BACKEND__, __HAS_CUDA_BACKEND__

size = 32 * 1024 * 1024  # 32Mo


def test_host_buffer():
    buf1 = HostBuffer(size=size)
    assert buf1.get_int_ptr() == buf1.int_ptr
    assert buf1.get_size() == buf1.size
    assert buf1.get_buffer() is buf1.buf

    buf2 = HostBuffer(size=size)
    assert buf1.int_ptr != buf2.int_ptr
    assert buf1.size == size
    assert buf1.size == buf2.size

    arr1 = np.frombuffer(buf1, dtype=np.uint8)
    arr2 = np.frombuffer(buf2, dtype=np.uint8)
    arr1.fill(0)
    arr2.fill(1)
    assert (arr1 == 0).all()
    assert (arr2 == 1).all()

    buf2 = HostBuffer.from_buffer(buf1)
    assert buf1.int_ptr == buf2.int_ptr
    assert buf1.size == buf2.size

    buf3 = HostBuffer.from_int_ptr(buf2.int_ptr, buf2.size)
    assert buf1.int_ptr == buf3.int_ptr
    assert buf1.size == buf3.size

    arr1 = np.frombuffer(buf1, dtype=np.uint8)
    arr2 = np.frombuffer(buf2, dtype=np.uint8)
    arr3 = np.frombuffer(buf3, dtype=np.uint8)
    arr1.fill(0)
    assert (arr1 == 0).all()
    assert (arr2 == 0).all()
    assert (arr3 == 0).all()
    arr2.fill(1)
    assert (arr1 == 1).all()
    assert (arr2 == 1).all()
    assert (arr3 == 1).all()
    arr3.fill(2)
    assert (arr1 == 2).all()
    assert (arr2 == 2).all()
    assert (arr3 == 2).all()
    buf1.release()
    arr1.fill(0)
    assert (arr1 == 0).all()
    assert (arr2 == 0).all()
    assert (arr3 == 0).all()
    buf2.release()
    arr2.fill(1)
    assert (arr1 == 1).all()
    assert (arr2 == 1).all()
    assert (arr3 == 1).all()
    buf3.release()
    arr3.fill(2)
    assert (arr1 == 2).all()
    assert (arr2 == 2).all()
    assert (arr3 == 2).all()
    del arr1
    arr3.fill(12)
    assert (arr2 == 12).all()
    assert (arr3 == 12).all()
    del arr3
    arr2.fill(42)
    assert (arr2 == 42).all()
    del arr2


if __HAS_OPENCL_BACKEND__:
    from hysop.backend.device.opencl import cl
    from hysop.backend.device.opencl.opencl_buffer import OpenClBuffer
    from hysop.backend.device.opencl.opencl_tools import get_or_create_opencl_env

    cl_env = get_or_create_opencl_env(mpi_params=default_mpi_params())

    def test_opencl_buffer():
        mf = cl.mem_flags

        buf = cl.Buffer(cl_env.context, size=size, flags=mf.READ_ONLY)

        buf1 = OpenClBuffer(cl_env.context, size=size, mem_flags=mf.READ_WRITE)
        assert buf1.int_ptr == buf1.get_int_ptr()
        cl.enqueue_copy(
            queue=cl_env.default_queue, src=buf, dest=buf1, byte_count=size
        ).wait()

        assert buf1.ref_count() == 1
        buf1.release()

        buf1 = OpenClBuffer(cl_env.context, size=size, mem_flags=mf.READ_WRITE)
        buf2 = buf1.get_sub_region(0, buf1.size, buf1.flags)
        buf3 = buf1.get_sub_region(0, buf1.size, buf1.flags)
        assert buf1.int_ptr != buf2.int_ptr
        assert buf1.int_ptr != buf3.int_ptr
        cl.enqueue_copy(
            queue=cl_env.default_queue, src=buf, dest=buf1, byte_count=size
        ).wait()
        cl.enqueue_copy(
            queue=cl_env.default_queue, src=buf, dest=buf2, byte_count=size
        ).wait()
        cl.enqueue_copy(
            queue=cl_env.default_queue, src=buf, dest=buf3, byte_count=size
        ).wait()
        buf1.release()
        cl.enqueue_copy(
            queue=cl_env.default_queue, src=buf, dest=buf2, byte_count=size
        ).wait()
        cl.enqueue_copy(
            queue=cl_env.default_queue, src=buf, dest=buf3, byte_count=size
        ).wait()
        buf3.release()
        cl.enqueue_copy(
            queue=cl_env.default_queue, src=buf, dest=buf2, byte_count=size
        ).wait()
        buf2.release()

    def test_opencl_host_device_buffer():
        mf = cl.mem_flags

        # try to alloc pinned memory
        buf = OpenClBuffer(
            cl_env.context, size=size, mem_flags=mf.ALLOC_HOST_PTR, hostbuf=None
        )

        # try to use unified memory (or something like zero copy memory)
        hbuf = np.ndarray(shape=size, dtype=np.float32)
        buf = OpenClBuffer(cl_env.context, mem_flags=mf.USE_HOST_PTR, hostbuf=hbuf)

        # copy host buf at creation
        hbuf = np.ndarray(
            shape=(
                1024,
                1024,
            ),
            dtype=np.int64,
        )
        buf = OpenClBuffer(cl_env.context, mem_flags=mf.COPY_HOST_PTR, hostbuf=hbuf)


if __name__ == "__main__":
    test_host_buffer()
    if __HAS_OPENCL_BACKEND__:
        test_opencl_buffer()
        test_opencl_host_device_buffer()
