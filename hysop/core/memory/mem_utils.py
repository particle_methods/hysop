# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import resource, psutil
from psutil import virtual_memory


def disable_vmem():
    physical_memory = psutil.virtual_memory().total
    _, vmem_hard = resource.getrlimit(resource.RLIMIT_AS)
    vmem_soft = max(0.95 * physical_memory, physical_memory - 512 * 1024 * 1024)
    if vmem_hard != resource.RLIM_INFINITY:
        vmem_soft = min(vmem_soft, vmem_hard)
    vmem_hard = vmem_soft
    resource.setrlimit(resource.RLIMIT_AS, (vmem_soft, vmem_hard))
    return (vmem_soft, vmem_hard)


def memory_repport():
    from hysop.tools.units import bytes2str

    vmem = psutil.virtual_memory()
    ss = "== Memory Repport =="
    ss += "\n  Virtual memory mapping:"
    for attr in [
        "total",
        "available",
        "used",
        "free",
        "active",
        "inactive",
        "buffers",
        "cached",
        "shared",
    ]:
        val = getattr(vmem, attr)
        ss += f"\n   *{attr:<10} {bytes2str(val)}"
    ss += "\n"
    ss += "\n  Memory resource limits (soft/hard):"
    for attr in [
        "RLIMIT_AS",
        "RLIMIT_DATA",
        "RLIMIT_MEMLOCK",
        "RLIMIT_RSS",
        "RLIMIT_STACK",
    ]:
        soft, hard = resource.getrlimit(getattr(resource, attr))
        soft = "INFTY" if soft == resource.RLIM_INFINITY else bytes2str(soft)
        hard = "INFTY" if hard == resource.RLIM_INFINITY else bytes2str(hard)
        ss += f"\n   *{attr:<15} {soft:<8} / {hard:<8}"
    ss += "\n===================="
    return ss
