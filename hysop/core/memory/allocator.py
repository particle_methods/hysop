# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import traceback
from abc import ABCMeta, abstractmethod
from hysop import __VERBOSE__, __TRACE_MEMALLOCS__, __BACKTRACE_BIG_MEMALLOCS__
from hysop.tools.htypes import check_instance
from hysop.tools.units import bytes2str
from hysop.tools.htypes import first_not_None
from hysop.tools.handle import TaggedObject


class AllocatorBase(TaggedObject, metaclass=ABCMeta):
    """
    Base class for allocators.
    """

    is_deferred = False

    def __new__(cls, verbose, **kwds):
        return super().__new__(cls, tag_prefix="al", **kwds)

    def __init__(self, verbose, **kwds):
        super().__init__(tag_prefix="al", **kwds)
        verbose = first_not_None(verbose, __TRACE_MEMALLOCS__)
        self._verbose = verbose

    def __eq__(self, other):
        return self is other

    def __ne__(self, other):
        return self is not other

    def __hash__(self):
        return id(self)

    def __call__(self, size, alignment=None):
        """
        Allocate nbytes aligned on min_alignment.
        If the first allocation fails, call the garbage collector and try again.
        It this fails a second time, raise a MemoryError.
        """

        try_count = 0
        while try_count < 2:
            try:
                if alignment is None or alignment == 1:
                    buf = self.allocate(nbytes=size)
                else:
                    buf = self.allocate_aligned(size=size, alignment=alignment)
                if buf is None:
                    msg = f"{self.__class__}.allocate(): returned allocation is None."
                    raise ValueError(msg)
                return buf
            except MemoryError:
                try_count += 1
                if try_count == 2:
                    raise

            self.try_release_blocks()

    @abstractmethod
    def prefix(self):
        """Prefix for logs"""
        pass

    @abstractmethod
    def is_on_host(self):
        """
        Return true if buffers are allocated in host memory.
        """
        pass

    @abstractmethod
    def max_alloc_size(self):
        """Max allocatable size in bytes."""
        pass

    @abstractmethod
    def allocate(self, nbytes, verbose=True):
        """
        Allocate nbytes bytes of memory.
        No minimal memory alignment is guaranteed.
        If allocation fails, this method has to raise a MemoryError.
        """
        if (self._verbose or __BACKTRACE_BIG_MEMALLOCS__) and verbose:
            print(
                "{}allocating block of size {}.".format(
                    self.prefix(), bytes2str(nbytes)
                )
            )
        if __BACKTRACE_BIG_MEMALLOCS__ and nbytes > 64 * 1024 * 1024:
            print("[BIG ALLOCATION BACKTRACE]")
            print("".join(traceback.format_stack()))
            print("[END OF TRACE]")
            print()

    def allocate_aligned(self, size, alignment):
        """
        Allocate size bytes aligned on alignment.
        """
        assert alignment > 0
        assert (alignment & (alignment - 1)) == 0, "alignment is not a power of 2."
        nbytes = size + alignment - 1
        if self._verbose:
            print(
                "{}allocating block of size {}, to satisfy {} aligned on {} bytes.".format(
                    self.prefix(), bytes2str(nbytes), bytes2str(size), alignment
                )
            )
        return self.allocate(nbytes, verbose=False).aligned_view(
            alignment=alignment, size=size
        )

    def try_release_blocks(self):
        """
        Try to release memory blocks by calling the garbage collector.
        """
        import gc

        gc.collect()

    def free(self, buf):
        """
        Release the allocated buffer.
        """
        buf.release()
