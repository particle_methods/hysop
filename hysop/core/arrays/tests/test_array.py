# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import warnings
from contextlib import contextmanager
from random import randint

import numpy as np
from hysop.constants import (
    HYSOP_BOOL,
    HYSOP_INTEGER,
    HYSOP_REAL,
    MemoryOrdering,
    TranspositionState,
    default_order,
)
from hysop.core.arrays.all import Array, ArrayBackend, HostArray, HostArrayBackend
from hysop.testsenv import (
    __ENABLE_LONG_TESTS__,
    __HAS_OPENCL_BACKEND__,
    iter_clenv,
    opencl_failed,
)
from hysop.tools.contexts import printoptions
from hysop.tools.numerics import is_complex, is_integer, is_unsigned, match_float_type
from hysop.tools.htypes import to_list

if __HAS_OPENCL_BACKEND__:
    from hysop.core.arrays.all import OpenClArray, OpenClArrayBackend

from hysop.backend.host.host_allocator import HostAllocator
from hysop.core.memory.mempool import MemoryPool


class TestArray:

    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def setup_method(self, method):
        pass

    def teardown_method(self, method):
        pass

    def _test_array_creation_routines(self, backend):
        a = backend.empty(shape=10, dtype=np.int16)
        a.fill(-1)
        b = backend.zeros_like(a)
        c = backend.ones_like(b)
        d = backend.full_like(c, fill_value=2)
        e = backend.empty_like(d)
        e.fill(3)
        for buf, val in zip([a, b, c, d, e], [-1, 0, 1, 2, 3]):
            assert buf.ndim == 1
            assert buf.size == 10
            assert buf.shape == (10,)
            assert buf.itemsize == 2
            assert buf.nbytes == 20
            assert buf.dtype == np.int16
            assert buf.sum().get() == val * 10
            assert buf.prod() == val**10
            assert buf.is_c_contiguous()
            assert buf.is_fortran_contiguous()
            assert buf.is_hysop_contiguous()

        a = backend.empty(
            shape=(
                10,
                10,
            ),
            dtype=np.float64,
            order=MemoryOrdering.C_CONTIGUOUS,
        )
        a.fill(15)
        b = backend.zeros_like(a)
        c = backend.ones_like(b)
        d = backend.full_like(c, fill_value=2)
        for buf, val in zip([a, b, c, d], [15, 0, 1, 2]):
            assert buf.ndim == 2
            assert buf.size == 100
            assert buf.shape == (
                10,
                10,
            )
            assert buf.itemsize == 8
            assert buf.nbytes == 800
            assert buf.dtype == np.float64
            assert np.allclose(buf.sum().get(), val * 100.0)
            assert buf.is_c_contiguous()

        a = backend.empty(
            shape=(
                10,
                10,
            ),
            dtype=np.float32,
            order=MemoryOrdering.F_CONTIGUOUS,
        )
        a.fill(15)
        b = backend.zeros_like(a)
        c = backend.ones_like(b)
        d = backend.full_like(c, fill_value=2)
        for buf, val in zip([a, b, c, d], [15, 0, 1, 2]):
            assert buf.ndim == 2
            assert buf.size == 100
            assert buf.shape == (
                10,
                10,
            )
            assert buf.itemsize == 4
            assert buf.nbytes == 400
            assert buf.dtype == np.float32
            assert buf.sum().get() == val * 100
            assert buf.is_fortran_contiguous()

        a.fill(5)
        b.copy_from(a)
        c.copy_from(b.handle)
        d = c.copy()
        for buf in [a, b, c, d]:
            assert buf.ndim == 2
            assert buf.size == 100
            assert buf.shape == (
                10,
                10,
            )
            assert buf.itemsize == 4
            assert buf.nbytes == 400
            assert buf.dtype == np.float32
            assert buf.sum().get() == 5 * 100
            assert buf.is_fortran_contiguous()

    def _test_transpose_routines(self, backend):

        # ensure strides are working as intended
        # (strides are in bytes but itemsize == 1 byte here)
        A = backend.arange(2 * 4 * 8, dtype=np.int8).reshape(
            (2, 4, 8), order=MemoryOrdering.C_CONTIGUOUS
        )
        B = backend.arange(2 * 4 * 8, dtype=np.int8).reshape(
            (2, 4, 8), order=MemoryOrdering.F_CONTIGUOUS
        )
        i1, j1, k1 = (randint(0, A.shape[i] - 1) for i in range(3))
        i0, j0, k0 = (randint(0, A.shape[i] - 1) for i in range(3))

        assert A.dtype.itemsize == 1
        assert A.shape == (2, 4, 8)
        assert A[0][0][0] == 0
        assert A[0][0][1] == 1
        assert A[0][1][0] == 8
        assert A[1][0][0] == 8 * 4
        assert A.strides == (8 * 4, 8, 1)
        assert A[1][1][1] == np.sum(np.asarray(A.strides) // A.dtype.itemsize)
        assert A[i1][j1][k1] == np.sum(np.asarray(A.strides) * (i1, j1, k1))
        assert A[i0][j0][k0] == np.sum(np.asarray(A.strides) * (i0, j0, k0))
        assert (A[i1][j1][k1] - A[i0][j0][k0]) == np.dot(
            A.strides, (i1 - i0, j1 - j0, k1 - k0)
        )

        assert B.dtype.itemsize == 1
        assert B.shape == (2, 4, 8)
        assert B[0][0][0] == 0
        assert B[0][0][1] == 2 * 4
        assert B[0][1][0] == 2
        assert B[1][0][0] == 1
        assert B.strides == (1, 2, 2 * 4)
        assert B[1][1][1] == np.sum(np.asarray(B.strides) // B.dtype.itemsize)
        assert B[i1][j1][k1] == np.sum(np.asarray(B.strides) * (i1, j1, k1))
        assert B[i0][j0][k0] == np.sum(np.asarray(B.strides) * (i0, j0, k0))
        assert (B[i1][j1][k1] - B[i0][j0][k0]) == np.dot(
            B.strides, (i1 - i0, j1 - j0, k1 - k0)
        )

        # ensure permutations are working as intended
        A = backend.arange(6, dtype=np.int8).reshape(
            (1, 2, 3), order=MemoryOrdering.C_CONTIGUOUS
        )
        B = backend.arange(6, dtype=np.int8).reshape(
            (1, 2, 3), order=MemoryOrdering.F_CONTIGUOUS
        )
        for arr in (A, B):
            # all 3d permutations
            assert backend.transpose(arr, axes=(0, 1, 2)).shape == (1, 2, 3)
            assert backend.transpose(arr, axes=(0, 2, 1)).shape == (1, 3, 2)
            assert backend.transpose(arr, axes=(1, 0, 2)).shape == (2, 1, 3)
            assert backend.transpose(arr, axes=(2, 1, 0)).shape == (3, 2, 1)
            assert backend.transpose(arr, axes=(2, 0, 1)).shape == (3, 1, 2)
            assert backend.transpose(arr, axes=(1, 2, 0)).shape == (2, 3, 1)

            # transpositions (cycles of length 2)
            assert backend.transpose(
                backend.transpose(arr, axes=(0, 1, 2)), axes=(0, 1, 2)
            ).shape == (1, 2, 3)
            assert backend.transpose(
                backend.transpose(arr, axes=(0, 2, 1)), axes=(0, 2, 1)
            ).shape == (1, 2, 3)
            assert backend.transpose(
                backend.transpose(arr, axes=(1, 0, 2)), axes=(1, 0, 2)
            ).shape == (1, 2, 3)
            assert backend.transpose(
                backend.transpose(arr, axes=(2, 1, 0)), axes=(2, 1, 0)
            ).shape == (1, 2, 3)
            assert backend.transpose(
                backend.transpose(arr, axes=(2, 0, 1)), axes=(1, 2, 0)
            ).shape == (1, 2, 3)
            assert backend.transpose(
                backend.transpose(arr, axes=(1, 2, 0)), axes=(2, 0, 1)
            ).shape == (1, 2, 3)

            # cycles of length 3
            assert backend.transpose(
                backend.transpose(
                    backend.transpose(arr, axes=(2, 0, 1)), axes=(2, 0, 1)
                ),
                axes=(2, 0, 1),
            ).shape == (1, 2, 3)
            assert backend.transpose(
                backend.transpose(
                    backend.transpose(arr, axes=(1, 2, 0)), axes=(1, 2, 0)
                ),
                axes=(1, 2, 0),
            ).shape == (1, 2, 3)

            # roll, swap and move axes
            assert backend.rollaxis(arr, axis=0, start=0).shape == (1, 2, 3)
            assert backend.rollaxis(arr, axis=1, start=0).shape == (2, 1, 3)
            assert backend.rollaxis(arr, axis=2, start=0).shape == (3, 1, 2)
            assert backend.rollaxis(arr, axis=0, start=1).shape == (1, 2, 3)
            assert backend.rollaxis(arr, axis=1, start=1).shape == (1, 2, 3)
            assert backend.rollaxis(arr, axis=2, start=1).shape == (1, 3, 2)
            assert backend.rollaxis(arr, axis=0, start=2).shape == (2, 1, 3)
            assert backend.rollaxis(arr, axis=1, start=2).shape == (1, 2, 3)
            assert backend.rollaxis(arr, axis=2, start=2).shape == (1, 2, 3)
            assert backend.rollaxis(arr, axis=0, start=3).shape == (2, 3, 1)
            assert backend.rollaxis(arr, axis=1, start=3).shape == (1, 3, 2)
            assert backend.rollaxis(arr, axis=2, start=3).shape == (1, 2, 3)

            assert backend.swapaxes(arr, axis1=0, axis2=0).shape == (1, 2, 3)
            assert backend.swapaxes(arr, axis1=1, axis2=0).shape == (2, 1, 3)
            assert backend.swapaxes(arr, axis1=2, axis2=0).shape == (3, 2, 1)
            assert backend.swapaxes(arr, axis1=0, axis2=1).shape == (2, 1, 3)
            assert backend.swapaxes(arr, axis1=1, axis2=1).shape == (1, 2, 3)
            assert backend.swapaxes(arr, axis1=2, axis2=1).shape == (1, 3, 2)
            assert backend.swapaxes(arr, axis1=0, axis2=2).shape == (3, 2, 1)
            assert backend.swapaxes(arr, axis1=1, axis2=2).shape == (1, 3, 2)
            assert backend.swapaxes(arr, axis1=2, axis2=2).shape == (1, 2, 3)

            assert backend.moveaxis(arr, source=0, destination=0).shape == (1, 2, 3)
            assert backend.moveaxis(arr, source=1, destination=0).shape == (2, 1, 3)
            assert backend.moveaxis(arr, source=2, destination=0).shape == (3, 1, 2)
            assert backend.moveaxis(arr, source=0, destination=1).shape == (2, 1, 3)
            assert backend.moveaxis(arr, source=1, destination=1).shape == (1, 2, 3)
            assert backend.moveaxis(arr, source=2, destination=1).shape == (1, 3, 2)
            assert backend.moveaxis(arr, source=0, destination=2).shape == (2, 3, 1)
            assert backend.moveaxis(arr, source=1, destination=2).shape == (1, 3, 2)
            assert backend.moveaxis(arr, source=2, destination=2).shape == (1, 2, 3)

    def _test_array_manipulation_routines(self, backend):
        t3d = TranspositionState[3]

        a = backend.empty(
            shape=(8, 4, 2), dtype=np.int8, order=MemoryOrdering.C_CONTIGUOUS
        )
        b = backend.empty(
            shape=(2, 4, 8), dtype=np.int8, order=MemoryOrdering.F_CONTIGUOUS
        )
        a.copy_from(backend.arange(0, a.size, dtype=a.dtype))
        b.copy_from(backend.arange(0, b.size, dtype=b.dtype))

        assert a.transposition_state() == t3d.ZYX
        assert b.transposition_state() == t3d.XYZ
        assert (a.ravel() == b.ravel(order=MemoryOrdering.C_CONTIGUOUS)).all()

        a = a.transpose_to_state(t3d.XZY)
        b = b.transpose_to_state(t3d.XZY)
        assert a.order == MemoryOrdering.OUT_OF_ORDER
        assert b.order == MemoryOrdering.OUT_OF_ORDER
        assert a.transposition_state() == t3d.XZY
        assert b.transposition_state() == t3d.XZY

        a = a.transpose([2, 0, 1])
        b = b.transpose([1, 2, 0])
        assert a.transposition_state() == t3d.YXZ
        assert b.transposition_state() == t3d.ZYX

        a = a.transpose_to_state(t3d.ZYX)
        b = b.transpose_to_state(t3d.XYZ)
        assert (a.ravel() == b.ravel(order=MemoryOrdering.C_CONTIGUOUS)).all()

        a = a.reshape(8 * 4 * 2)
        b = a.reshape(8 * 4 * 2)
        assert a.order == default_order
        assert b.order == default_order
        assert (a == b).all()

        a = a.reshape((8, 4, 2), order=MemoryOrdering.C_CONTIGUOUS)
        b = b.reshape((8, 4, 2), order=MemoryOrdering.F_CONTIGUOUS)
        assert a.order == MemoryOrdering.C_CONTIGUOUS
        assert b.order == MemoryOrdering.F_CONTIGUOUS

        assert a.transposition_state() == t3d.ZYX
        a = backend.moveaxis(a, 0, 1)
        assert a.transposition_state() == t3d.YZX
        a = backend.swapaxes(a, 1, 2)
        assert a.transposition_state() == t3d.YXZ
        a = backend.rollaxis(a, 2)
        assert a.transposition_state() == t3d.ZYX
        a = backend.rollaxis(a, 2, 1)
        assert a.transposition_state() == t3d.ZXY

        a0 = backend.asfortranarray(a)
        a1 = backend.ascontiguousarray(a)
        a2 = backend.asarray_chkfinite(a)

        assert a0.order == MemoryOrdering.F_CONTIGUOUS
        assert a1.order == MemoryOrdering.C_CONTIGUOUS
        assert a2.order == default_order

        assert a0.transposition_state() == t3d.XYZ
        assert a1.transposition_state() == t3d.ZYX

        assert (a0 == a).all()
        assert (a1 == a).all()
        assert (a2 == a).all()

    def _test_binary_operations(self, backend):
        a = backend.rand((10, 10))
        b = backend.rint(a)
        a = backend.rint(backend.rand((10, 10))).astype(np.uint8)
        b = backend.rint(backend.rand((10, 10))).astype(np.uint8)
        c = backend.rint(backend.rand((10, 10))).astype(np.uint8)
        d = backend.rint(backend.rand((10, 10))).astype(np.uint8)

        assert ((~(~a)) == a).all()
        assert (((a << 2 << 3) >> 5) == a).all()
        assert ((a >> 1) == 0).all()
        assert ((a | b | c | d) == (d | c | b | a)).all()
        assert ((a & b & c & d) == (d & c & b & a)).all()
        assert ((a ^ b) == (b ^ a)).all()
        assert ((~(a | b)) == ((~a) & (~b))).all()

        a = backend.rint(10000 * backend.rand((10, 10))).astype(np.uint64)
        b = backend.rint(10000 * backend.rand((10, 10))).astype(np.uint64)
        c = backend.rint(10000 * backend.rand((10, 10))).astype(np.uint64)
        d = backend.rint(10000 * backend.rand((10, 10))).astype(np.uint64)

        assert ((~(~a)) == a).all()
        assert (((a << 2 << 3) >> 5) == a).all()
        assert ((a | b | c | d) == (d | c | b | a)).all()
        assert ((a & b & c & d) == (d & c & b & a)).all()
        assert ((a ^ b) == (b ^ a)).all()
        assert ((~(a | b)) == ((~a) & (~b))).all()

    def _test_arithmetic_operations(self, backend):
        a = backend.rand((10, 10)).astype(np.float64).clip(0.1, 0.9)
        a = (a - 0.5) * 10

        b = 10 * backend.rand((10, 10)).astype(np.float64).clip(0.1, 0.9)
        c = 10 * backend.rand((10, 10)).astype(np.float64).clip(0.1, 0.9)
        d = 10 * backend.rand((10, 10)).astype(np.float64).clip(0.1, 0.9)

        assert backend.allclose(4.0 + a, a + 4.0)
        assert backend.allclose(4.0 - a, -(a - 4.0))
        assert backend.allclose(4.0 * a, a * 4.0)
        assert backend.allclose(4.0 / a, 1.0 / (a / 4.0))

        f, i = backend.modf(a)
        assert backend.allclose(backend.trunc(a), i)
        f, i = backend.modf(b)
        assert backend.allclose(backend.fmod(b, 1), f)

        assert backend.allclose(b // 1, i)
        assert backend.allclose(b % 1, f)

        assert backend.allclose(a - b, -(b - a))
        assert backend.allclose(a + b - c - d, -c - d + b + a)
        assert backend.allclose(a * b * c * d, d * c * a * b)
        a = a % b
        a = a // b

        a = c.copy()
        assert backend.allclose(c, a)
        a += 1
        assert backend.allclose(c + 1, a)
        a -= 3
        assert backend.allclose(c + 1 - 3, a)
        a *= 2
        assert backend.allclose(2 * (c + 1 - 3), a)
        a /= 3
        assert backend.allclose(2 * (c + 1 - 3) / 3, a)
        a //= 4
        assert backend.allclose((2 * (c + 1 - 3) / 3) // 4, a)
        a %= 2
        assert backend.allclose(((2 * (c + 1 - 3) / 3) // 4) % 2, a)

    def _test_backend_versus_numpy_operations(self, backend):
        npb = backend

        atol = [None]

        # pollute array with +inf, -inf and NaNs values
        def pollute(arr):
            def mask(p):
                return np.random.rand(*arr.shape) < p

            arr[mask(0.20)] = -np.inf
            arr[mask(0.20)] = +np.inf
            arr[mask(0.20)] = np.nan

        def allclose(
            np_array,
            backend_array,
            equal_nan=True,
            atol=atol,
            relaxed_precision=False,
            ignore_mask=None,
        ):
            atol = atol[0]  # 1*epsilon
            if relaxed_precision:
                atol = 1e-2
            if backend_array is None:
                msg = "Backend returned nothing (got None)."
                raise ValueError(msg)
            if not np.isscalar(np_array) and not isinstance(np_array, np.ndarray):
                msg = "first arg is not a np.ndarray (got {})"
                msg = msg.format(np_array)
                raise ValueError(msg)
            if isinstance(backend_array, Array):
                backend_array = backend_array.get().handle
            if ignore_mask is not None:
                np_array = np_array[~ignore_mask]
                backend_array = backend_array[~ignore_mask]
            return np.allclose(np_array, backend_array, equal_nan=equal_nan, atol=atol)

        unary_ops = [
            "reciprocal",
            "negative",
            "absolute",
            "fabs",
            "sin",
            "cos",
            "arcsin",
            "arccos",
            "arctan",
            "tan",
            "degrees",
            "radians",
            "deg2rad",
            "rad2deg",
            "sinh",
            "cosh",
            "arcsinh",
            "arccosh",
            "arctanh",
            # 'tanh', 'around',  #FIXME
            "rint",
            "fix",
            "floor",
            "ceil",
            "trunc",
            "exp",
            "expm1",
            "exp2",
            "log",
            "log10",
            "log2",
            "log1p",
            "signbit",
            "reciprocal",
            "negative",
            "sqrt",
            "cbrt",
            "square",
            "sign",
            "nan_to_num",
            "prod",
            "sum",
            "nanprod",
            "nansum",
            "cumprod",
            "cumsum",
            "nancumprod",
            "nancumsum",
            "isfinite",
            "isinf",
            "isnan",
            "isneginf",
            "isposinf",
            "real",
            "imag",
            "angle",
            "conj",
            "real_if_close",
        ]

        binary_ops = [
            "minimum",
            "maximum",
            "fmin",
            "fmax",
            "add",
            "subtract",
            "multiply",
            "power",
            "divide",
            "floor_divide",
            "true_divide",
            "equal",
            "not_equal",
            "less_equal",
            "greater_equal",
            "less",
            "greater",
            "mod",
            "fmod",
            "remainder",
            "hypot",
            "arctan2",
            "logaddexp",
            "logaddexp2",
            "copysign",
        ]

        array_unary_ops = ["__neg__", "__abs__"]

        array_binary_ops = [
            "__eq__",
            "__ne__",
            "__le__",
            "__ge__",
            "__lt__",
            "__gt__",
            "__add__",
            "__sub__",
            "__mul__",
            "__pow__",
            "__floordiv__",
            "__truediv__",
            "__mod__",
            "__radd__",
            "__rsub__",
            "__rmul__",
            "__rpow__",
            "__rfloordiv__",
            "__rtruediv__",
            "__rmod__",
        ]

        # real_skip_list = ['angle', 'real', 'imag', 'conj', 'real_if_close']
        # NG 26 sep 2023: add 'floor_divide', '__floordiv__', '__rfloordiv__'
        complex_skip_list = [
            "fabs",
            "floor",
            "ceil",
            "trunc",
            "fix",
            "degrees",
            "radians",
            "deg2rad",
            "rad2deg",
            "signbit",
            "cbrt",
            "isneginf",
            "isposinf",
            "remainder",
            "mod",
            "fmod",
            "modf",
            "hypot",
            "arctan2",
            "logaddexp",
            "logaddexp2",
            "copysign",
            "frexp",
            "__mod__",
            "__rmod__",
            "floor_divide",
            "__floordiv__",
            "__rfloordiv__",
        ]

        splitting_ops = ["frexp", "modf"]

        def ignore_infty(ref_out, backend_out, **kargs):
            mask = np.isinf(ref_out)
            mask |= np.isinf(backend_out)
            return mask

        def positive_int_rhs(variables):
            assert "b" in variables  # b is rhs
            rhs = variables["b"]
            dtype = rhs[0].dtype
            if is_integer(dtype):
                for i, v in enumerate(rhs):
                    rhs[i] = abs(v)

        def clamp(_amin, _amax):
            def _filter(variables):
                for k, _vars in variables.items():
                    for i, var in enumerate(_vars):
                        if is_complex(var):
                            if isinstance(var, np.ndarray):
                                np.clip(var.real, _amin, _amax, variables[k][i].real)
                                np.clip(var.imag, _amin, _amax, variables[k][i].imag)
                            else:
                                amin = _amin + 1j * _amin
                                amax = _amax + 1j * _amax
                                var.backend.clip_components(
                                    var, amin, amax, variables[k][i]
                                )
                        else:
                            if isinstance(var, np.ndarray):
                                np.clip(var.real, _amin, _amax, variables[k][i])
                            else:
                                var.backend.clip(var, _amin, _amax, variables[k][i])

            return _filter

        pow_constraints = [positive_int_rhs]
        pow_constraints.append(clamp(+0, +3))

        # Extra contraints on inputs
        # should be a list of functions taking variables as inputsq
        input_constraints = {
            "power": pow_constraints,
            "__pow__": pow_constraints,
            "__rpow__": pow_constraints,
            "cumprod": clamp(0.1, 1.1),
            "nancumprod": clamp(0.1, 1.1),
        }

        # Extra contraints on outputs
        # Generate a mask of values thats should not
        # be compared to numpy solution in allclose check)
        #  all keys are operator names
        #  all values are function of dtype and backend,
        output_constraints = {"cumprod": [ignore_infty], "nancumprod": [ignore_infty]}

        class TestContext:
            def __init__(self, opname, input_constraints, variables):
                self.opname = opname

                # if there is a specific constraint we copy everything
                dtypes = {}
                if opname in input_constraints:
                    for vname, vargs in variables.items():
                        for i, var in enumerate(vargs):
                            variables[vname][i] = variables[vname][i].copy()
                    filters = to_list(input_constraints[opname])
                    for f in filters:
                        f(variables)
                self.dtypes = dtypes

                for vname, vargs in variables.items():
                    dtypes[vname] = variables[vname][0].dtype
                    for i, var in enumerate(vargs):
                        varname = f"{vname}{i}"
                        setattr(self, varname, var)
                    setattr(self, vname, vargs)

            def __enter__(self):
                return self

            def __exit__(self, exception, e, traceback):
                if e is None:
                    return True
                msg = "\nTESTING: Test failed in at {}::{}() with dtypes {}\n"
                msg = msg.format(backend.__class__.__name__, self.opname, self.dtypes)
                print(msg)
                return False

        def check_inputs(name, _in):
            isclose = np.isclose(_in[0], _in[1].get(handle=True), equal_nan=True)
            if not isclose.all():
                print(f"{name} inputs mismatch...")
                print(f"{name.upper()} NUMPY INPUT:")
                print(_in[0][~isclose])
                print(f"{name.upper()} BACKEND INPUT:")
                print(_in[1].get()[~isclose])
                raise RuntimeError("Inputs did not match... Fix your input filters.")

        def check_close(lhs, rhs, r0, r1, opname):
            if (r0 is None) and (r1 is None):
                return
            elif r0 is None:
                msg = f"numpy::{opname} returned None."
                raise TypeError(msg)
            elif r1 is None:
                msg = f"{backend.__class__.__name__}::{opname} returned None."
                raise TypeError(msg)
            else:
                if isinstance(r1, Array):
                    r1 = r1.get(handle=True)
                    if __HAS_OPENCL_BACKEND__ and isinstance(
                        backend, OpenClArrayBackend
                    ):
                        # FIXME OpenCl support for float16
                        if r0.dtype == np.float16:
                            r1 = r1.astype(np.float16)
                    if r0.dtype == np.bool_:
                        r1 = r1.astype(np.bool_)

                if (r0.dtype == np.bool_) and (r1.dtype == np.bool_):
                    l2 = np.sqrt(np.nansum(r0 ^ r1)) / r0.size
                    linf = np.nanmax(r0 ^ r1)
                else:
                    m0 = np.isfinite(r0)
                    m1 = np.isfinite(r1)
                    if (m0 != m1).any():
                        l2 = np.inf
                        linf = np.inf
                    else:
                        try:
                            R0, R1 = r0[m0], r1[m0]
                            l2 = np.sqrt(np.sum((R0 - R1) * np.conj(R0 - R1)) / R0.size)
                            linf = np.max(np.abs(R0 - R1))
                        except ValueError:
                            l2 = 0
                            linf = 0
                msg1 = "(l2={}, linf={})."
                msg1 = msg1.format(l2, linf)

                if r0.dtype == r1.dtype:
                    mask = None
                    if opname in output_constraints:
                        mask_generators = to_list(output_constraints[opname])
                        mask = mask_generators[0](ref_out=r0, backend_out=r1)
                        for mask_gen in mask_generators[1:]:
                            mask |= mask_gen(ref_out=r0, backend_out=r1)

                    close = allclose(r0, r1, ignore_mask=mask)
                    tol = atol[0]
                    if not close:
                        close = allclose(
                            r0, r1, relaxed_precision=True, ignore_mask=mask
                        )
                        tol = 1e-2
                        if close:
                            msg = "WARNING: test passed with relaxed precision for {}::{}."
                            msg = msg.format(backend.__class__.__name__, opname)
                            print(msg)
                    if not close:
                        msg = "\n{}::{} returned dtypes did match (got {}) "
                        msg += "but failed to match numpy output,"
                        msg += "\n absolute tolerance was set to {}."
                        msg = msg.format(
                            backend.__class__.__name__, opname, r1.dtype, tol
                        )
                        print(msg)
                        if isinstance(r0, np.ndarray) and isinstance(r1, np.ndarray):
                            failed = ~np.isclose(r0, r1, equal_nan=True, atol=atol[0])
                            if lhs is not None:
                                check_inputs("lhs", lhs)
                                print("LHS_INPUT")
                                print(lhs[0][failed])
                            if rhs is not None:
                                check_inputs("rhs", rhs)
                                print("RHS INPUT")
                                print(rhs[0][failed])
                            print("EXPECTED")
                            print(r0[failed])
                            print("GOT")
                            print(r1[failed])
                        else:
                            print(f"r0 => {r0.__class__}")
                            print(f"r1 => {r1.__class__}")
                        msg0 = "Method {}::{} failed to match numpy output"
                        msg0 = msg0.format(backend.__class__.__name__, opname)
                        msg = msg0 + msg1
                        print()
                        print(msg)
                        raise ValueError(msg)
                    else:
                        msg0 = "{}::{} matched numpy output "
                        msg0 = msg0.format(backend.__class__.__name__, opname)
                        msg = msg0 + msg1
                        print(msg)
                else:
                    msg = "\n{}::{} returned dtypes didn't match (expected {} but got {})."
                    msg = msg.format(
                        backend.__class__.__name__, opname, r0.dtype, r1.dtype
                    )
                    print(msg)

                    msg = (
                        "{}::{} returned dtypes did not match, "
                        "got {} but numpy returned {}."
                    )
                    msg = msg.format(
                        backend.__class__.__name__, opname, r1.dtype, r0.dtype
                    )
                    raise ValueError(msg)

        def test_operators(a, b, A, B, skip=[]):
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")

                for opname in unary_ops:
                    if opname in skip:
                        continue
                    f0 = getattr(np, opname)
                    f1 = getattr(npb, opname)
                    with TestContext(
                        opname, input_constraints, variables={"a": [a, A]}
                    ) as ctx:
                        r0 = f0(ctx.a0)
                        r1 = f1(ctx.a1)
                        check_close(ctx.a, None, r0, r1, opname)

                for opname in binary_ops:
                    if opname in skip:
                        continue
                    f0 = getattr(np, opname)
                    f1 = getattr(npb, opname)
                    with TestContext(
                        opname, input_constraints, variables={"a": [a, A], "b": [b, B]}
                    ) as ctx:
                        r0 = f0(ctx.a0, ctx.b0)
                        r1 = f1(ctx.a1, ctx.b1)
                        check_close(ctx.a, ctx.b, r0, r1, opname)

                for opname in splitting_ops:
                    if opname in skip:
                        continue
                    with TestContext(
                        opname, input_constraints, variables={"a": [a, A], "b": [b, B]}
                    ) as ctx:
                        f0 = getattr(np, opname)
                        f1 = getattr(npb, opname)
                        r00, r01 = f0(ctx.a0)
                        r10, r11 = f1(ctx.a1)
                        check_close(ctx.a, None, r00, r10, opname)
                        check_close(ctx.a, None, r01, r11, opname)

                for opname in array_unary_ops:
                    if opname in skip:
                        continue
                    with TestContext(
                        opname, input_constraints, variables={"a": [a, A]}
                    ) as ctx:
                        f0 = getattr(ctx.a0, opname)
                        f1 = getattr(ctx.a1, opname)
                        r0 = f0()
                        r1 = f1()
                        check_close(ctx.a, None, r0, r1, opname)

                for opname in array_binary_ops:
                    if opname in skip:
                        continue
                    with TestContext(
                        opname, input_constraints, variables={"a": [a, A], "b": [b, B]}
                    ) as ctx:
                        if opname.find("__r") == 0:
                            f0 = getattr(ctx.b0, opname)
                            f1 = getattr(ctx.b1, opname)
                            r0 = f0(ctx.a0)
                            r1 = f1(ctx.a1)
                        else:
                            f0 = getattr(ctx.a0, opname)
                            f1 = getattr(ctx.a1, opname)
                            r0 = f0(ctx.b0)
                            r1 = f1(ctx.b1)
                        check_close(ctx.a, ctx.b, r0, r1, opname)

        def make_arrays(dtype):
            ftype = match_float_type(dtype)
            atol[0] = np.finfo(ftype).eps
            print(f"::atol switched to {ftype} epsilon: {atol}.")

            cnbe = 8192  # NG 8192

            a = (np.random.rand(cnbe) - 0.5) * 100
            b = (np.random.rand(cnbe) - 0.5) * 100
            if is_unsigned(dtype):
                a = abs(a)
                b = abs(b)
            a = a.astype(dtype)  # <= negative number to unsigned dtype conversion wraps
            b = b.astype(dtype)
            if is_complex(dtype):
                a += (np.random.rand(cnbe) - 0.5) * 100j
                b += (np.random.rand(cnbe) - 0.5) * 100j

            A, B = npb.asarray(a), npb.asarray(b)
            assert allclose(a, A)
            assert allclose(b, B)
            assert npb.allclose(npb.asarray(a), A, equal_nan=True)
            assert npb.allclose(npb.asarray(b), B, equal_nan=True)

            return a, b, A, B

        # FIXME numpy quad float support (gcc __float128), not implemented yet as
        if __ENABLE_LONG_TESTS__:
            signed_types = (
                np.int8,
                np.int16,
                np.int32,
                np.int64,
            )
            unsigned_types = (
                np.uint8,
                np.uint16,
                np.uint32,
                np.uint64,
            )
            float_types = (np.float16, np.float32, np.float64, np.longdouble)
            complex_types = (np.complex64, np.complex128, np.clongdouble)
        else:
            signed_types = ()
            unsigned_types = ()
            float_types = (np.float64,)
            complex_types = (np.complex64,)

        for dtype in signed_types:
            print(f"\n== SIGNED INTEGER OPS {dtype} ==")
            a, b, A, B = make_arrays(dtype)
            test_operators(a, b, A, B)

        for dtype in unsigned_types:
            print(f"\n== UNSIGNED INTEGER OPS {dtype} ==")
            a, b, A, B = make_arrays(dtype)
            test_operators(a, b, A, B)

        # FIXME OpenCl backend half float and long double support
        for dtype in float_types:
            print(f"\n== FLOAT OPS {dtype} ==")
            if __HAS_OPENCL_BACKEND__:
                if isinstance(backend, OpenClArrayBackend) and (
                    dtype in [np.float16, np.longdouble]
                ):
                    print("  -- NO SUPPORT PROVIDED BY BACKEND --")
                    continue

            a, b, A, B = make_arrays(dtype)
            test_operators(a, b, A, B)

            print(f"\n== POLLUTED FLOAT OPS {dtype} ==")
            pollute(a)
            pollute(b)

            A, B = npb.asarray(a), npb.asarray(b)
            test_operators(a, b, A, B)

        # FIXME OpenCL complex functions: arcsin, arccos, floordix, pow, ...
        for dtype in complex_types:
            print(f"\n== COMPLEX OPS {dtype} ==")
            if __HAS_OPENCL_BACKEND__:
                if isinstance(backend, OpenClArrayBackend):
                    if dtype in [np.clongdouble]:
                        print("  -- NO SUPPORT PROVIDED BY BACKEND --")
                        continue

                skip_list = [x for x in complex_skip_list]
                ##NG 26 sep 2023:: add 'power','__rpow__','__pow__',
                skip_list += [
                    "arcsin",
                    "arccos",
                    "arctan",
                    "arcsinh",
                    "arccosh",
                    "arctanh",
                    "exp2",
                    "expm1",
                    "power",
                    "__rpow__",
                    "__pow__",
                    "log2",
                    "log10",
                    "log1p",
                    "floor_divide",
                    "__floordiv__",
                    "__rfloordiv__",
                ]
            else:
                skip_list = complex_skip_list

            a, b, A, B = make_arrays(dtype)
            test_operators(a, b, A, B, skip=skip_list)

            print(f"\n== POLLUTED COMPLEX OPS {dtype} ==")
            pollute(a)
            pollute(b)

            if __HAS_OPENCL_BACKEND__:
                if isinstance(backend, OpenClArrayBackend):
                    skip_list += ["power", "__rpow__", "__pow__"]

            A, B = npb.asarray(a), npb.asarray(b)
            test_operators(a, b, A, B, skip=skip_list)

    def _test_backend_versus_numpy(self, backend):
        self._test_backend_versus_numpy_operations(backend)

    def _test_backend(self, backend):
        with printoptions(linewidth=240, edgeitems=4, threshold=20):
            # self._test_array_creation_routines(backend)
            # self._test_transpose_routines(backend)
            # self._test_binary_operations(backend)
            # self._test_arithmetic_operations(backend)
            self._test_backend_versus_numpy(backend)
            # self._test_array_manipulation_routines(backend)

    def test_host_array_backend_allocator(self):
        allocator = HostAllocator()
        backend = HostArrayBackend(allocator=allocator)
        self._test_backend(backend)

    @opencl_failed
    def test_opencl_array_backend_allocator(self):
        if __HAS_OPENCL_BACKEND__:
            from hysop.backend.device.opencl.opencl_allocator import (
                OpenClImmediateAllocator,
            )

            for cl_env in iter_clenv():
                print()
                print(f"TESTING OPENCL PLATFORM {cl_env.platform.name}")
                allocator = OpenClImmediateAllocator(queue=cl_env.default_queue)
                backend = OpenClArrayBackend(cl_env=cl_env, allocator=allocator)
                self._test_backend(backend)


if __name__ == "__main__":
    test = TestArray()
    test.test_host_array_backend_allocator()
    if __HAS_OPENCL_BACKEND__:
        test.test_opencl_array_backend_allocator()
