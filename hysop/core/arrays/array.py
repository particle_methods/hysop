# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
from abc import ABCMeta, abstractmethod
from hysop.constants import MemoryOrdering
from hysop.constants import DirectionLabels, default_order
from hysop.tools.misc import prod
from hysop.tools.htypes import check_instance
from hysop.tools.numpywrappers import slices_empty
from hysop.tools.decorators import required_property, optional_property


class Array(metaclass=ABCMeta):
    """
    Interface of an abstract array.
    An array is a numpy.ndarray work-alike that stores its data and performs
    its computations on various devices, depending on the backend.
    All exposed functions should work exactly as in numpy.
    Arithmetic methods in Array, when available, should at least support the
    broadcasting of scalars.

    For Fortran users, reverse the usual order of indices when accessing elements of an array.
    to be in line with Python semantics and the natural order of the data.
    The fact is that Python indexing on lists and other sequences naturally leads to an
    outside-to inside ordering (the first index gets the largest grouping, and the last
    gets the smallest element).

    See https://docs.scipy.org/doc/numpy-1.10.0/reference/internals.html for more information
    about C versus Fortran ordering in numpy.

    Numpy notation are used for axes, axe 0 is the slowest varying index and last axe is
    the fastest varying index.
    By default:
        3D C-ordering       is [0,1,2] which corresponds to ZYX transposition state.
        3D Fortran-ordering is [2,1,0] which corresponds to XYZ transposition state.
    The last axe is the memory contiguous one (X in C-ordering and Z in Fortran-ordering).
    This means that when taking array byte strides, in the axis order, the strides are
    decreasing until the last stride wich is the size of array dtype in bytes.
    """

    def __init__(self, handle, backend, **kwds):
        """
        Build an Array instance.

        Parameters
        ----------
        handle:  buffer backend implementation
        backend: backend used to build this andle
        kwds: arguments for base classes.

        Notes
        -----
        This should never be called directly by the user.
        Arrays should be constructed using array backend facilities, like zeros or empty.
        The parameters given here refer to a low-level method for instantiating an array.
        """
        from hysop.core.arrays.all import ArrayBackend

        check_instance(backend, ArrayBackend)

        super().__init__(**kwds)

        self._handle = handle
        self._backend = backend

        if hasattr(handle, "__len__"):
            setattr(self, "__len__", handle.__len__)

    def get_handle(self):
        """
        Return underlying implementation of this buffer.
        """
        if not hasattr(self, "_handle"):
            msg = "{} has no _handle defined."
            msg = msg.format(self.__class__)
            raise RuntimeError(msg)
        return self._handle

    def get_backend(self):
        """
        Return the backend corresponding to this array.
        """
        if not hasattr(self, "_backend"):
            msg = "{} has no _backend defined."
            msg = msg.format(self.__class__)
            raise RuntimeError(msg)
        return self._backend

    handle = property(get_handle)
    backend = property(get_backend)

    def __int__(self):
        """Return scalar value as an int."""
        assert self.size == 1
        return int(self.get())

    def __float__(self):
        assert self.size == 1
        """Return scalar value as a float."""
        assert self.size == 1
        return float(self.get())

    def __complex__(self):
        """Return scalar value as a complex."""
        assert self.size == 1
        return complex(self.get())

    def __nonzero__(self):
        """Called to implement truth value testing and the built-in operation bool()."""
        if self.shape == ():
            return self.handle.__nonzero__()
        else:
            return True

    @classmethod
    def _not_implemented_yet(cls, funname):
        msg = "{}::{} has not been implemented yet."
        msg = msg.format(cls.__name__, funname)
        raise NotImplementedError(msg)

    @classmethod
    def _unsupported_argument(cls, fname, argname, arg, default_value=None):
        if arg != default_value:
            msg = "{}::{}() has been implemented but argument '{}' is not "
            msg += "supported and should be set to {}."
            msg = msg.format(cls.__name__, fname, argname, default_value)
            raise NotImplementedError(msg)

    def wrap(self, handle):
        """
        Wrap handle with the same initialization arguments as this instance.
        """
        return self.backend.wrap(handle=handle)

    def _call(self, fname, *args, **kargs):
        """
        Calls a handle function.
        """
        f = getattr(self.handle, fname)
        return self.backend._call(f, *args, **kargs)

    @abstractmethod
    def as_symbolic_array(self, name, **kwds):
        """
        Return a symbolic array variable that contain a reference to this array.
        """
        pass

    @abstractmethod
    def as_symbolic_buffer(self, name, **kwds):
        """
        Return a symbolic buffer variable that contain a reference to this array.
        """
        pass

    @abstractmethod
    @required_property
    def get_ndim(self):
        """
        Number of array dimensions.
        """
        pass

    @abstractmethod
    @required_property
    def get_int_ptr(self):
        """
        Return the underlying buffer pointer as an int.
        """
        pass

    @abstractmethod
    @required_property
    def get_shape(self):
        """
        The real shape of this buffer.
        """
        pass

    @abstractmethod
    @required_property
    def set_shape(self):
        """
        Set the shape of this buffer.
        From the numpy doc: It is not always possible to change the shape of an array without
        copying the data. If you want an error to be raised if the data is copied, you should
        assign the new shape to the shape attribute of the array.
        """
        pass

    @abstractmethod
    @required_property
    def get_offset(self):
        """
        Offset of array data in buffer.
        """
        pass

    @abstractmethod
    @required_property
    def get_strides(self):
        """
        Tuple of ints that represents the byte step in each dimension when traversing an array.
        """
        pass

    @abstractmethod
    @required_property
    def get_data(self):
        """
        Buffer object pointing to the start of the array's data
        """
        pass

    @abstractmethod
    @required_property
    def get_base(self):
        """
        Base object if memory is from some other object.
        """
        pass

    @abstractmethod
    @required_property
    def get_dtype(self):
        """
        numpy.dtype representing the type stored into this buffer.
        """
        pass

    @optional_property
    def get_flags(self):
        """
        Information about the memory layout of the array.
        """
        pass

    @optional_property
    def get_imag(self):
        """
        The imaginary part of the array.
        """
        pass

    @optional_property
    def get_real(self):
        """
        The real part of the array.
        """
        pass

    @optional_property
    def get_ctypes(self):
        """
        An object to simplify the interaction of the array with the ctypes module.
        """
        pass

    def get_T(self):
        """
        Same as self.transpose(), except that self is returned if self.ndim < 2.
        """
        if self.ndim < 2:
            return self
        else:
            return self.transpose()

    def get_size(self):
        """
        Number of elements in the array.
        """
        return prod(self.get_shape())

    def get_itemsize(self):
        """
        Number of bytes per element.
        """
        return self.get_dtype().itemsize

    def get_nbytes(self):
        """
        Number of bytes in the whole buffer.
        """
        return self.itemsize * self.size

    # array properties to be (re)defined
    ndim = property(get_ndim)
    shape = property(get_shape, set_shape)
    offset = property(get_offset)
    strides = property(get_strides)
    data = property(get_data)
    base = property(get_base)
    dtype = property(get_dtype)
    int_ptr = property(get_int_ptr)

    # optional array properties
    flags = property(get_flags)
    imag = property(get_imag)
    real = property(get_real)
    ctypes = property(get_ctypes)

    # deduced array properties, may be redefined
    size = property(get_size)
    itemsize = property(get_itemsize)
    nbytes = property(get_nbytes)
    T = property(get_T)

    @abstractmethod
    def get(self, handle=False):
        """
        Returns a HostArray, view or copy of this array in the host memory.
        Usefull for backends thats do not share physical memory with the host.
        """
        pass

    def get_data_base(self):
        """
        For a given HostArray, finds the
        base array that 'owns' the actual data.
        """
        base = self
        while isinstance(base.base, self.__class__):
            base = base.base
        return base

    def share_data(self, other):
        """
        Returns true if self may share the same
        physical memory as other.
        """
        return self.get_data_base() is other.get_data_base()

    def ctype(self):
        """
        Equivalent C type corresponding to the numpy.dtype.
        """
        self.__class__.not_implemented_yet("ctype")

    def get_order(self):
        """
        Memory ordering.
        Determine whether the array view is written in C-contiguous order
        (last index varies the fastest), or FORTRAN-contiguous order
        in memory (first index varies the fastest).
        If dimension is one, default_order is returned.
        """
        dim = self.ndim
        if dim == 1:
            return default_order
        else:
            axes = self.logical_axes()
            c_axes = np.arange(dim)
            if (axes == c_axes).all():
                return MemoryOrdering.C_CONTIGUOUS
            elif (axes == c_axes[::-1]).all():
                return MemoryOrdering.F_CONTIGUOUS
            else:
                return MemoryOrdering.OUT_OF_ORDER

    order = property(get_order)

    def logical_axes(self):
        """
        Logical axes state ids, in numpy convention, as a tuple.
        Axe 0 is the slowest varying index, last axe is the fastest varying index.
        ie 3D C-ordering       is [2,1,0]
           3D fortran-ordering is [0,1,2]

        Thoses are the axes seen as a numpy view on memory, *only* strides are permutated for access,
        Those axes are found by reverse argsorting the array strides, using a stable sorting algorithm.

        Logical permutations can be achieved through numpy-like routines present in ArrayBackend:
            *np.transpose
            *np.rollaxis
            *np.moveaxis
            *np.swapaxes
        and Array property:
            *Array.T

        See Array.physical_axes() for physical permutations of axes.
        See Array.axes() for overall array permutation.
        See https://docs.scipy.org/doc/numpy-1.10.0/reference/internals.html for more information
        about C versus Fortran ordering in numpy.
        """
        strides = np.asarray(self.strides, dtype=np.int64)
        axes = np.argsort(-strides, kind="mergesort")
        return tuple(axes.tolist())

    def is_fp(self):
        """
        Return true if dtype is a floatting point type.
        """
        return self.dtype in [np.float16, np.float32, np.float64]

    def is_c_contiguous(self):
        """
        Return true if dimension is one or if current order is C_CONTIGUOUS.
        """
        return (self.ndim == 1) or (self.order == MemoryOrdering.C_CONTIGUOUS)

    def is_fortran_contiguous(self):
        """
        Return true if dimension is one or if current order is F_CONTIGUOUS.
        """
        return (self.ndim == 1) or (self.order == MemoryOrdering.F_CONTIGUOUS)

    def is_hysop_contiguous(self):
        """
        Return true if dimension is one or if current order corresponds to hysop default order.
        """
        return self.order == default_order

    def copy_from(self, src, **kargs):
        """
        Copy data from buffer src
        """
        self.backend.memcpy(self, src, **kargs)

    def copy_to(self, dst, **kargs):
        """
        Copy data from buffer to dst
        """
        self.backend.memcpy(dst, self, **kargs)

    def transpose_to_state(self, state, **kargs):
        """
        Transpose buffer to specified transposition state.
        """
        origin = str(self.transposition_state())
        target = str(state)
        if origin == target:
            return

        axes = []
        for axe in target:
            axes.append(origin.index(axe))
        return self.transpose(axes=axes)

    # np.ndarray like methods

    def all(self, axis=None, out=None, **kargs):
        """
        Returns True if all elements evaluate to True.
        """
        return self.backend.all(a=self, axis=axis, out=out, **kargs)

    def any(self, axis=None, out=None, **kargs):
        """
        Returns True if any of the elements of a evaluate to True.
        """
        return self.backend.any(a=self, axis=axis, out=out, **kargs)

    def argmax(self, axis=None, out=None, **kargs):
        """
        Return indices of the maximum values along the given axis.
        """
        return self.backend.argmax(a=self, axis=axis, out=out, **kargs)

    def argmin(self, axis=None, out=None, **kargs):
        """
        Return indices of the minimum values along the given axis of a.
        """
        return self.backend.argmin(a=self, axis=axis, out=out, **kargs)

    def argpartition(self, kth, axis=-1, kind="quicksort", order=None, **kargs):
        """
        Returns the indices that would partition this array.
        """
        return self.backend.argpartition(
            a=self, kth=kth, axis=axis, kind=kind, order=order, **kargs
        )

    def argsort(self, axis=-1, kind="quicksort", order=None, **kargs):
        """
        Returns the indices that would sort this array.
        """
        return self.backend.argsort(a=self, axis=axis, kind=kind, order=order, **kargs)

    def astype(
        self,
        dtype,
        order=MemoryOrdering.SAME_ORDER,
        casting="unsafe",
        subok=True,
        copy=True,
        **kargs,
    ):
        """
        Copy of the array, cast to a specified type.
        """
        return self.backend.astype(
            a=self,
            dtype=dtype,
            order=order,
            casting=casting,
            subok=subok,
            copy=copy,
            **kargs,
        )

    def byteswap(self, inplace=False, **kargs):
        """
        Swap the bytes of the array elements
        Toggle between low-endian and big-endian data representation by returning
        a byteswapped array, optionally swapped in-place.
        """
        return self.backend.byteswap(a=self, inplace=inplace, **kargs)

    def choose(self, choices, out=None, mode="raise", **kargs):
        """
        Use an index array to construct a new array from a set of choices.
        """
        return self.backend.choose(choices, out=out, mode="raise", **kargs)

    def clip(self, min=None, max=None, out=None, **kargs):
        """
        Return an array whose values are limited to min, max.
        """
        return self.backend.clip(a=self, a_min=min, a_max=max, out=out, **kargs)

    def compress(self, condition, axis=None, out=None, **kargs):
        """
        Return selected slices of this array along given axis.
        """
        return self.backend.compress(
            a=self, condition=condition, axis=axis, out=out, **kargs
        )

    def conj(self, out=None, **kargs):
        """
        Complex-conjugate of all elements.
        """
        return self.backend.conj(x=self, out=out, **kargs)

    def conjugate(self, out=None, **kargs):
        """
        Return the complex conjugate, element-wise.
        """
        return self.backend.conj(x=self, out=out, **kargs)

    def cumprod(self, axis=None, dtype=None, out=None, **kargs):
        """
        Return the cumulative product of the elements along the given axis.
        """
        return self.backend.cumprod(a=self, axis=axis, dtype=dtype, out=out, **kargs)

    def cumsum(self, axis=None, dtype=None, out=None, **kargs):
        """
        Return the cumulative sum of the elements along the given axis.
        """
        return self.backend.cumsum(a=self, axis=axis, dtype=dtype, out=out, **kargs)

    def copy(self, order=MemoryOrdering.SAME_ORDER, **kargs):
        """
        Return a copy of the array.
        """
        return self.backend.copy(a=self, order=order, **kargs)

    def diagonal(self, offset=0, axis1=0, axis2=1, **kargs):
        """
        Return specified diagonals.
        """
        return self.backend.diagonal(offset=offset, axis1=axis1, axis2=axis2, **kargs)

    def dot(self, b, out=None, **kargs):
        """
        Dot product of two arrays.
        """
        return self.backend.dot(a=self, b=b, out=out, **kargs)

    def dump(self, file, **kargs):
        """
        Dump a pickle of the array to the specified file.
        """
        return self.backend.save(arr=self, file=file, **kargs)

    def dumps(self, **kargs):
        """
        Returns the pickle of the array as a string.
        """
        return self.backend.array2string(a=self, **kargs)

    def fill(self, value, **kargs):
        """
        Fill the array with a scalar value.
        """
        return self.backend.fill(a=self, value=value, **kargs)

    def flatten(self, order=MemoryOrdering.SAME_ORDER, **kargs):
        """
        Return a copy of the array collapsed into one dimension.
        """
        return self.backend.flatten(a=self, order=order, **kargs)

    def max(self, axis=None, out=None, **kargs):
        """
        Return the maximum along a given axis.
        """
        return self.backend.amax(a=self, axis=axis, out=out, **kargs)

    def mean(self, axis=None, dtype=None, out=None, **kargs):
        """
        Returns the average of the array elements along given axis.
        """
        return self.backend.mean(a=self, axis=axis, dtype=dtype, out=out, **kargs)

    def min(self, axis=None, out=None, **kargs):
        """
        Return the minimum along a given axis.
        """
        return self.backend.amin(a=self, axis=axis, out=out, **kargs)

    def nonzero(self, **kargs):
        """
        Return the indices of the elements that are non-zero.
        """
        return self.backend.nonzero(a=self, **kargs)

    def partition(self, kth, axis=-1, kind="quicksort", order=None, **kargs):
        """
        Rearranges the elements in the array in such a way that value of the element i
        in kth position is in the position it would be in a sorted array.
        """
        return self.backend.partition(
            a=self, kth=kth, axis=axis, kind=kind, order=order, **kargs
        )

    def prod(self, axis=None, dtype=None, out=None, **kargs):
        """
        Return the product of the array elements over the given axis
        """
        return self.backend.prod(a=self, axis=axis, dtype=dtype, out=out, **kargs)

    def ptp(self, axis=None, out=None, **kargs):
        """
        Peak to peak (maximum - minimum) value along a given axis.
        """
        return self.backend.ptp(a=self, axis=axis, out=out, **kargs)

    def ravel(self, order=MemoryOrdering.SAME_ORDER, **kargs):
        """
        Return a flattened array.
        """
        return self.backend.ravel(a=self, order=order, **kargs)

    def repeat(self, repeats, axis=None, **kargs):
        """
        Repeat elements of an array.
        """
        return self.backend.repeat(a=self, repeats=repeats, axis=axis, **kargs)

    def reshape(self, new_shape, order=default_order, **kargs):
        """
        Returns an array containing the same data with a new shape.
        """
        return self.backend.reshape(a=self, newshape=new_shape, order=order, **kargs)

    def resize(self, new_shape, refcheck=True, **kargs):
        """
        Change shape and size of array in-place.
        """
        return self.backend.resize(
            a=self, new_shape=new_shape, refcheck=refcheck, **kargs
        )

    def round(self, decimals=0, out=None, **kargs):
        """
        Return a with each element rounded to the given number of decimals.
        """
        return self.backend.around(a=self, decimals=decimals, out=out, **kargs)

    def searchsorted(self, v, side="left", sorter=None, **kargs):
        """
        Find indices where elements of v should be inserted in a to maintain order.
        """
        return self.backend.searchsorted(a=self, v=v, side=side, sorter=sorter, **kargs)

    def sort(self, axis=-1, kind="quicksort", order=None, **kargs):
        """
        Sort an array, in-place.
        """
        return self.backend.sort(a=self, axis=axis, kind=kind, order=order, **kargs)

    def squeeze(self, axis=None, **kargs):
        """
        Remove single-dimensional entries from the shape of a.
        """
        return self.backend.squeeze(a=self, axis=axis, **kargs)

    def std(self, axis=None, dtype=None, out=None, ddof=0, **kargs):
        """
        Returns the standard deviation of the array elements along given axis.
        """
        return self.backend.std(a=self, axis=axis, dtype=dtype, out=out, ddof=ddof)

    def sum(self, axis=None, dtype=None, out=None, **kargs):
        """
        Return the sum of the array elements over the given axis.
        """
        return self.backend.sum(a=self, axis=axis, dtype=dtype, out=out, **kargs)

    def swapaxes(self, axis1, axis2, **kargs):
        """
        Return a view of the array with axis1 and axis2 interchanged.
        """
        return self.backend.swapaxes(axis1=axis1, axis2=axis2, **kargs)

    def trace(self, offset=0, axis1=0, axis2=1, dtype=None, out=None, **kargs):
        """
        Return the sum along diagonals of the array.
        """
        return self.backend.trace(
            a=self,
            offset=offset,
            axis1=axis1,
            axis2=axis2,
            dtype=dtype,
            out=out,
            **kargs,
        )

    def transpose(self, axes=None, **kargs):
        """
        Returns a view of the array with axes transposed.
        """
        return self.backend.transpose(a=self, axes=axes, **kargs)

    def var(self, axis=None, dtype=None, out=None, ddof=0, **kargs):
        """
        Returns the variance of the array elements, along given axis.
        """
        return self.backend.var(
            a=self, axis=axis, dtype=dtype, out=out, ddof=ddof, **kargs
        )

    # Array restricted methods

    def setflags(self, write=None, align=None, uic=None):
        """
        Set array flags WRITEABLE, ALIGNED, and UPDATEIFCOPY, respectively.
        """
        msg = "{}::set_flags() should not be called."
        msg = msg.format(self.__class__.__name__)
        raise RuntimeError(msg)

    # Array specific unimplemented methods

    def tofile(self, fid, sep="", format="%s", **kargs):
        """
        Write array to a file as text or binary (default).
        This is a convenience function for quick storage of array data.
        Information on endianness and precision is lost.
        """
        self.__class__.not_implemented_yet("tofile")

    def tolist(self, **kargs):
        """
        Return the array as a possibly nested list.
        """
        self.__class__not_implemented_yet("tolist")

    def tostring(self, order=MemoryOrdering.SAME_ORDER, **kargs):
        """
        Construct Python bytes containing the raw data bytes in the array.
        """
        self.__class__not_implemented_yet("tostring")

    def getfield(self, dtype, offset=0):
        """
        Returns a field of the given array as a certain type.
        """
        self._not_implemented_yet("get_field")

    def setfield(self, val, dtype, offset=0):
        """
        Put a value into a specified place in a field defined by a data-type.
        Place val into a's field defined by dtype and beginning offset bytes into the field.
        """
        self._not_implemented_yet("set_field")

    def item(self, *args):
        """
        Copy an element of an array to a standard Python scalar and return it.
        """
        self._not_implemented_yet("item")

    def itemset(self, *args):
        """
        Insert scalar into an array (scalar is cast to array's dtype, if possible)
        """
        self._not_implemented_yet("itemset")

    def put(self, indices, values, mode="raise", **kargs):
        """
        Set a.flatn = valuesn for all n in indices.
        """
        self._not_implemented_yet("put")

    def take(self, indices, axis=None, out=None, mode="raise", **kargs):
        """
        Return an array formed from the elements of a at the given indices.
        """
        self._not_implemented_yet("take")

    def view(self, dtype=None, **kargs):
        """
        New view of array with the same data.
        """
        self._not_implemented_yet("view")

    def tobytes(self, order=MemoryOrdering.C_CONTIGUOUS):
        """
        Construct Python bytes containing the raw data bytes in the array.
        """
        self._not_implemented_yet("tobytes")

    # logical operators

    def __eq__(self, other):
        return self.backend.equal(self, other)

    def __ne__(self, other):
        return self.backend.not_equal(self, other)

    def __le__(self, other):
        return self.backend.less_equal(self, other)

    def __ge__(self, other):
        return self.backend.greater_equal(self, other)

    def __lt__(self, other):
        return self.backend.less(self, other)

    def __gt__(self, other):
        return self.backend.greater(self, other)

    # arithmetic operators
    def __neg__(self):
        return self.backend.negative(self)

    def __abs__(self):
        return self.backend.absolute(self)

    def __invert__(self):
        return self.backend.invert(self)

    def __add__(self, other):
        return self.backend.add(self, other)

    def __sub__(self, other):
        return self.backend.subtract(self, other)

    def __mul__(self, other):
        return self.backend.multiply(self, other)

    def __pow__(self, other):
        return self.backend.power(self, other)

    def __floordiv__(self, other):
        return self.backend.floor_divide(self, other)

    def __truediv__(self, other):
        return self.backend.divide(self, other)

    def __mod__(self, other):
        return self.backend.mod(self, other)

    def __and__(self, other):
        return self.backend.bitwise_and(self, other)

    def __xor__(self, other):
        return self.backend.bitwise_xor(self, other)

    def __or__(self, other):
        return self.backend.bitwise_or(self, other)

    def __lshift__(self, other):
        return self.backend.left_shift(self, other)

    def __rshift__(self, other):
        return self.backend.right_shift(self, other)

    def __radd__(self, other):
        return self.backend.add(other, self)

    def __rsub__(self, other):
        return self.backend.subtract(other, self)

    def __rmul__(self, other):
        return self.backend.multiply(other, self)

    def __rpow__(self, other):
        return self.backend.power(other, self)

    def __rfloordiv__(self, other):
        return self.backend.floor_divide(other, self)

    def __rtruediv__(self, other):
        return self.backend.divide(other, self)

    def __rmod__(self, other):
        return self.backend.mod(other, self)

    def __rand__(other, self):
        return self.backend.bitwise_and(other, self)

    def __rxor__(other, self):
        return self.backend.bitwise_xor(other, self)

    def __ror__(other, self):
        return self.backend.bitwise_or(other, self)

    def __rlshift__(other, self):
        return self.backend.left_shift(other, self)

    def __rrshift__(other, self):
        return self.backend.right_shift(other, self)

    def __iadd__(self, other):
        return self.backend.add(self, other, out=self)

    def __isub__(self, other):
        return self.backend.subtract(self, other, out=self)

    def __imul__(self, other):
        return self.backend.multiply(self, other, out=self)

    def __ipow__(self, other):
        return self.backend.power(self, other, a=self)

    def __ifloordiv__(self, other):
        return self.backend.floor_divide(self, other, out=self)

    def __idiv__(self, other):
        return self.backend.divide(self, other, out=self)

    def __imod__(self, other):
        return self.backend.mod(self, other, out=self)

    def __str__(self):
        return self._handle.__str__()

    def __repr__(self):
        return self._handle.__repr__()

    def __setitem__(self, slices, value):
        if any((s == 0) for s in self[slices].shape):
            return
        self._call("__setitem__", slices, value)

    def __getitem__(self, slices):
        return self._call("__getitem__", slices)
