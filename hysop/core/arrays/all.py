# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.core.arrays.array_backend import ArrayBackend
from hysop.core.arrays.array import Array

from hysop.backend.host.host_array_backend import HostArrayBackend
from hysop.backend.host.host_array import HostArray

from hysop.backend import __HAS_OPENCL_BACKEND__

if __HAS_OPENCL_BACKEND__:
    from hysop.backend.device.opencl.opencl_array_backend import OpenClArrayBackend
    from hysop.backend.device.opencl.opencl_array import OpenClArray

from hysop.backend.host.host_allocator import (
    default_host_allocator,
    default_host_mempool,
)

default_host_array_backend = HostArrayBackend(default_host_allocator)
