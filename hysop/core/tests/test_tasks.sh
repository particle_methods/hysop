#!/usr/bin/env bash
##
## Copyright (c) HySoP 2011-2024
##
## This file is part of HySoP software.
## See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
## for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -x
PYTHON_EXECUTABLE=${PYTHON_EXECUTABLE:-python3}
MPIRUN_EXECUTABLE=${MPIRUN_EXECUTABLE:-mpirun}
MPIRUN_TASKS_OPTION='-np'
if [ "${MPIRUN_EXECUTABLE}" = "srun" ]; then MPIRUN_TASKS_OPTION='-n'; fi
if [[ ${MPIRUN_EXECUTABLE} == *"mpirun"* ]]; then MPIRUN_TASKS_OPTION='--oversubscribe '${MPIRUN_TASKS_OPTION}; fi
MPIRUN_FAIL_EARLY="-mca orte_abort_on_non_zero_status 1"
MPIRUN_ARGS="${MPIRUN_FAIL_EARLY} ${MPIRUN_TASKS_OPTION} 4"

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
EXAMPLE_DIR="$(realpath ${SCRIPT_DIR}/../../../hysop_examples/examples)"

EXAMPLE_FILE="${EXAMPLE_DIR}/tasks/tasks.py"
TEST_DIR='/tmp/hysop_tests/tasks/'
COMMON_OPTIONS="-NC -impl python -cp fp32 "

echo
echo "TEST TASKS"
if [[ ! -f "${EXAMPLE_FILE}" ]]; then
    echo "Cannot find example file '${EXAMPLE_FILE}'."
    exit 1
fi

echo ' Running simulations...'
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS}
if [[ $? -ne 0 ]] ; then exit 1; fi


${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} --proc-tasks "(111,111,222,222)"
if [[ $? -ne 0 ]] ; then exit 1; fi
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} --proc-tasks "(111,222,222,222)"
if [[ $? -ne 0 ]] ; then exit 1; fi
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} --proc-tasks "((111,),(222,),(222,),(222,))"
if [[ $? -ne 0 ]] ; then exit 1; fi
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} --proc-tasks "((111,222),(222,),(222,),(222,))"
if [[ $? -ne 0 ]] ; then exit 1; fi
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} --proc-tasks "((111,222),(222,),(222,111),(222,))"
if [[ $? -ne 0 ]] ; then exit 1; fi


# Followings should fail (not handled yet)
echo ' Running simulations that MUST fail'
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} --proc-tasks "((111,),(222,),(222,111),(222,))"
if [[ $? -eq 0 ]] ; then exit 1; fi
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} --proc-tasks "((222,),(222,),(222,111),(222,))"
if [[ $? -eq 0 ]] ; then exit 1; fi

echo ' Done'
exit 0
