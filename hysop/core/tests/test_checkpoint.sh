#!/usr/bin/env bash
##
## Copyright (c) HySoP 2011-2024
##
## This file is part of HySoP software.
## See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
## for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -feu -o pipefail
PYTHON_EXECUTABLE=${PYTHON_EXECUTABLE:-python3}
MPIRUN_EXECUTABLE=${MPIRUN_EXECUTABLE:-mpirun}
MPIRUN_ARGS='-np'
MPIRUN_FAIL_EARLY="-mca orte_abort_on_non_zero_status 1"
if [ "${MPIRUN_EXECUTABLE}" = "srun" ]; then MPIRUN_ARGS='-n'; fi
if [[ ${MPIRUN_EXECUTABLE} == *"mpirun"* ]]; then MPIRUN_ARGS='--oversubscribe '${MPIRUN_ARGS}; fi
MPIRUN_ARGS="${MPIRUN_FAIL_EARLY} ${MPIRUN_ARGS}"

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
EXAMPLE_DIR="$(realpath ${SCRIPT_DIR}/../../../hysop_examples/examples)"

function compare_files {
    if [[ ! -f "$1" ]]; then
        echo "File '${1}' does not exist."
        exit 1
    fi
    if [[ ! -f "$2" ]]; then
        echo "File '${2}' does not exist."
        exit 1
    fi

    # see https://stackoverflow.com/questions/3679296/only-get-hash-value-using-md5sum-without-filename
    # for the bash array assignment trick (solution proposed by Peter O.)
    # we also remove signs in front of zeros
    A=($(sha1sum <(sed 's/[+-]\(0\.0*\s\)/+\1/g' ${1})))
    B=($(sha1sum <(sed 's/[+-]\(0\.0*\s\)/+\1/g' ${2})))
    if [[ "${A}" != "${B}" ]]; then
        echo "Could not match checksums between ${1} and ${2}."
        exit 1
    fi
}

#
# Basic test with analytic (serial)
#
EXAMPLE_FILE="${EXAMPLE_DIR}/analytic/analytic.py"
TEST_DIR='/tmp/hysop_tests/checkpoints/analytic'
COMMON_OPTIONS="-NC -impl python -cp fp32 -d64 --debug-dump-target dump -niter 20 -te 0.1 --dump-tstart 0.05 --dump-freq 1 "

echo
echo "TEST ANALYTIC CHECKPOINT (SERIAL)"
if [[ ! -f "${EXAMPLE_FILE}" ]]; then
    echo "Cannot find example file '${EXAMPLE_FILE}'."
    exit 1
fi

echo ' Running simulations...'
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} 1  ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} -S "${TEST_DIR}/checkpoint0.tar" --dump-dir "${TEST_DIR}/run0" --checkpoint-dump-time 0.05 --checkpoint-dump-freq 0
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} 1  ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} -S "${TEST_DIR}/checkpoint1.tar" --dump-dir "${TEST_DIR}/run1" --checkpoint-dump-time 0.05 --checkpoint-dump-freq 0

echo ' Comparing solutions...'
echo "  >debug dumps match"
compare_files "${TEST_DIR}/run0/dump/run.txt" "${TEST_DIR}/run1/dump/run.txt"
for f0 in $(find "${TEST_DIR}/run0" -name '*.h5' | sort -n); do
    f1=$(echo "${f0}" | sed 's/run0/run1/')
    compare_files "${f0}" "${f1}"
    echo "  >$(basename ${f0}) match"
done

echo
echo ' Running simulations from checkpoints...'
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} 1  ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} -L "${TEST_DIR}/checkpoint0.tar" --dump-dir "${TEST_DIR}/run2"
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} 1  ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} -L "${TEST_DIR}/checkpoint1.tar" --dump-dir "${TEST_DIR}/run3"

echo ' Comparing solutions...'
compare_files "${TEST_DIR}/run2/dump/run.txt" "${TEST_DIR}/run3/dump/run.txt"
echo "  >debug dumps match"
for f0 in $(find "${TEST_DIR}/run2" -name '*.h5' | sort -n); do
    f1=$(echo "${f0}" | sed 's/run2/run3/')
    f2=$(echo "${f0}" | sed 's/run2/run0/')
    f3=$(echo "${f0}" | sed 's/run2/run1/')
    compare_files "${f0}" "${f1}"
    compare_files "${f0}" "${f2}"
    compare_files "${f0}" "${f3}"
    echo "  >$(basename ${f0}) match"
done


#
# Basic test with analytic (MPI)
#
EXAMPLE_FILE="${EXAMPLE_DIR}/analytic/analytic.py"
TEST_DIR='/tmp/hysop_tests/checkpoints/analytic_mpi'
COMMON_OPTIONS="-NC -impl python -cp fp64 -d64 --debug-dump-target dump  -niter 20 -te 0.1 --dump-tstart 0.05 --dump-freq 1 "

echo
echo "TEST ANALYTIC CHECKPOINT (MPI)"
if [[ ! -f "${EXAMPLE_FILE}" ]]; then
    echo "Cannot find example file '${EXAMPLE_FILE}'."
    exit 1
fi

echo ' Running simulations...'
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} 4 ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} -S "${TEST_DIR}/checkpoint0.tar" --dump-dir "${TEST_DIR}/run0" --checkpoint-dump-time 0.05 --checkpoint-dump-freq 0
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} 4 ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} -S "${TEST_DIR}/checkpoint1.tar" --dump-dir "${TEST_DIR}/run1" --checkpoint-dump-time 0.05 --checkpoint-dump-freq 0

echo ' Comparing solutions...'
echo "  >debug dumps match"
compare_files "${TEST_DIR}/run0/dump/run.txt" "${TEST_DIR}/run1/dump/run.txt"
for f0 in $(find "${TEST_DIR}/run0" -name '*.h5' | sort -n); do
    f1=$(echo "${f0}" | sed 's/run0/run1/')
    h5diff -d '1e-15' "${f0}" "${f1}"
    echo "  >$(basename ${f0}) match"
done

echo
echo ' Running simulations from checkpoints...'
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} 4 ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} -L "${TEST_DIR}/checkpoint0.tar" --dump-dir "${TEST_DIR}/run2"
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} 4 ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} -L "${TEST_DIR}/checkpoint1.tar" --dump-dir "${TEST_DIR}/run3"

echo ' Comparing solutions...'
compare_files "${TEST_DIR}/run2/dump/run.txt" "${TEST_DIR}/run3/dump/run.txt"
echo "  >debug dumps match"
for f0 in $(find "${TEST_DIR}/run2" -name '*.h5' | sort -n); do
    f1=$(echo "${f0}" | sed 's/run2/run3/')
    f2=$(echo "${f0}" | sed 's/run2/run0/')
    f3=$(echo "${f0}" | sed 's/run2/run1/')
    h5diff -d '1e-15' "${f0}" "${f1}"
    h5diff -d '1e-15' "${f0}" "${f2}"
    h5diff -d '1e-15' "${f0}" "${f3}"
    echo "  >$(basename ${f0}) match"
done

#
# Test 3D with taylor green (Fortran MPI backend). This test is skipped when no Fortran or OpenCL implementation is available
#
if [ $(python3 -c 'import hysop;from hysop import __FFTW_ENABLED__, __SCALES_ENABLED__; from hysop.testsenv import  __HAS_OPENCL_BACKEND__; print("true" if not (__FFTW_ENABLED__ and __SCALES_ENABLED__ and __HAS_OPENCL_BACKEND__) else "false")') = true ]; then
    exit 0
fi
EXAMPLE_FILE="${EXAMPLE_DIR}/taylor_green/taylor_green.py"
TEST_DIR='/tmp/hysop_tests/checkpoints/taylor_green'
COMMON_OPTIONS="-NC -d24 --tend 0.3 --dump-tstart 0.15 --dump-freq 1 --hdf5-disable-slicing --hdf5-disable-compression"

echo
echo "TEST TAYLOR-GREEN CHECKPOINT (Fortran-MPI)"
if [[ ! -f "${EXAMPLE_FILE}" ]]; then
    echo "Cannot find example file '${EXAMPLE_FILE}'."
    exit 1
fi
if [[ -d "${TEST_DIR}" ]]; then
    rm -rf "${TEST_DIR}"
fi

# Fortran FFTW does not yield exactly the same results in parallel so we use h5diff with an absolute tolerance of 10^-12
echo ' Running simulations...'
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} 1 ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} -impl fortran -cp fp64 -S "${TEST_DIR}/checkpoint0.tar" --dump-dir "${TEST_DIR}/run0" --checkpoint-dump-time 0.15 --checkpoint-dump-freq 0
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} 2 ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} -impl fortran -cp fp64 -S "${TEST_DIR}/checkpoint1.tar" --dump-dir "${TEST_DIR}/run1" --checkpoint-dump-time 0.15 --checkpoint-dump-freq 0
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} 3 ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} -impl fortran -cp fp64 -S "${TEST_DIR}/checkpoint2.tar" --dump-dir "${TEST_DIR}/run2" --checkpoint-dump-time 0.15 --checkpoint-dump-freq 0
echo ' Comparing solutions...'
for f0 in $(find "${TEST_DIR}/run0" -name '*.h5' | sort -n); do
    f1=$(echo "${f0}" | sed 's/run0/run1/')
    f2=$(echo "${f0}" | sed 's/run0/run2/')
    h5diff -d '1e-12' "${f0}" "${f1}"
    h5diff -d '1e-12' "${f0}" "${f2}"
    echo "  >$(basename ${f0}) match"
done

echo ' Running simulations from checkpoints using different MPI topologies...'
COMMON_OPTIONS="-NC -d24 --tend 0.3 --dump-tstart 0.15 --dump-freq 1 --hdf5-disable-slicing --hdf5-disable-compression --checkpoint-relax-constraints"
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} 3 ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} -impl fortran -cp fp64 -L "${TEST_DIR}/checkpoint0.tar" --dump-dir "${TEST_DIR}/run3"
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} 2 ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} -impl fortran -cp fp64 -L "${TEST_DIR}/checkpoint1.tar" --dump-dir "${TEST_DIR}/run4"
${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} 1 ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} -impl fortran -cp fp64 -L "${TEST_DIR}/checkpoint2.tar" --dump-dir "${TEST_DIR}/run5"
echo ' Comparing solutions...'
for f3 in $(find "${TEST_DIR}/run3" -name '*.h5' | sort -n); do
    f0=$(echo "${f3}" | sed 's/run3/run0/')
    f4=$(echo "${f3}" | sed 's/run3/run4/')
    f5=$(echo "${f3}" | sed 's/run3/run5/')
    h5diff -d '1e-12' "${f0}" "${f3}"
    h5diff -d '1e-12' "${f0}" "${f4}"
    h5diff -d '1e-12' "${f0}" "${f5}"
    echo "  >$(basename ${f0}) match"
done

# echo ' Running simulations from checkpoints using OpenCL and different datatypes...'
# ${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} 1 ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} -cp fp64 -impl opencl -L "${TEST_DIR}/checkpoint0.tar" --dump-dir "${TEST_DIR}/run6"
# ${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} 1 ${PYTHON_EXECUTABLE} "${EXAMPLE_FILE}" ${COMMON_OPTIONS} -cp fp32 -impl opencl -L "${TEST_DIR}/checkpoint1.tar" --dump-dir "${TEST_DIR}/run7"
# echo ' Comparing solutions...'
# for f6 in $(find "${TEST_DIR}/run6" -name '*.h5' | sort -n); do
#     f7=$(echo "${f6}" | sed 's/run0/run7/')
#     h5diff -d '5e-5' "${f6}" "${f7}"
#     echo "  >$(basename ${f6}) match"
# done
