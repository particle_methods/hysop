# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Tools hysop topologies

* :class:`~hysop.topology.topology.TopoTools`
"""

from itertools import count
from abc import ABCMeta, abstractmethod
from hysop.constants import np, math, Backend
from hysop.constants import BoundaryCondition
from hysop.constants import HYSOP_MPI_REAL
from hysop.mesh.mesh import Mesh
from hysop.core.mpi import MPI
from hysop.tools.htypes import check_instance, to_tuple, first_not_None
from hysop.tools.mpi_utils import (
    dtype_to_mpi_type,
    iter_mpi_requests,
    create_sized,
    get_mpi_order,
)
from hysop.tools.parameters import MPIParams
from hysop.tools.misc import Utils
from hysop.tools.decorators import debug
from hysop.tools.numpywrappers import npw
from hysop.tools.string_utils import prepend


class TopoTools:
    """Static class providing tools to handle set of indices, message tags,
    and so on.
    """

    @staticmethod
    def gather_global_indices(topo, toslice=True, root=None, comm=None):
        """Collect global indices of local meshes on each process of topo

        Parameters
        ----------
        topo : :class:`~ hysop.topology.topology.CartesianTopology`
            topology on which indices are collected.
        toslice : boolean, optional
            true to return the result a dict of slices, else
            return a numpy array. See notes below.
        root : int, optional
            rank of the root mpi process. If None, reduce operation\
            is done on all processes.
        comm : mpi communicator, optional
            Communicator used to reduce indices. Default = topo.parent

        Returns
        -------
        either :
        * a dictionnary which maps rank number with
        a list of slices such that res[rank][i] = a slice
        defining the indices of the points of the local mesh,
        in direction i, in global notation.
        * or a numpy array where each column corresponds to a rank number,
        with column = [start_x, end_x, start_y, end_y ...]
        Ranks number are the processes numbers in comm.

        """
        if comm is None:
            comm = topo.parent
        size = comm.size
        start = topo.mesh.global_start
        end = topo.mesh.global_stop - 1
        # communicator that owns the topology
        rank = comm.Get_rank()
        dimension = topo.domain.dim
        iglob = npw.integer_zeros((dimension * 2, size), order="F")
        iglob_res = npw.integer_zeros((dimension * 2, size), order="F")
        iglob[0::2, rank] = start
        iglob[1::2, rank] = end
        # iglob is saved as a numpy array and then transform into
        # a dict of slices since mpi send operations are much
        # more efficient with numpy arrays.
        # iglob is stored in 'F' order in order to send contiguous buffers in
        # the following gather operations
        if root is None:
            comm.Allgather([iglob[:, rank], MPI.INT], [iglob_res, MPI.INT])
        else:
            comm.Gather([iglob[:, rank], MPI.INT], [iglob_res, MPI.INT], root=root)
        if toslice:
            return Utils.array_to_dict(iglob_res)
        else:
            return iglob_res

    @staticmethod
    def gather_global_indices_overlap(
        topo=None, comm=None, dom=None, toslice=True, root=None
    ):
        """This functions does the same thing as gather_global_indices but
        may also work when topo is None.

        The function is usefull if you need to collect global indices
        on a topo defined only on a subset of comm,
        when for the procs not in this subset, topo will be
        equal to None. In such a case, comm and dom are required.
        This may happen when you want to build a bridge between two topologies
        that do not handle the same number of processes but with an overlap
        between the two groups of processes of the topologies.

        In that case, a call to
        gather_global_indices(topo, comm, dom)
        will work on all processes belonging to comm, topo being None or not.
        The values corresponding to ranks not in topo will be empty slices.

        Parameters
        ----------
        topo : :class:`~ hysop.topology.topology.CartesianTopology`, optional
            topology on which indices are collected.
        toslice : boolean, optional
            true to return the result a dict of slices, else
            return a numpy array. See notes below.
        root : int, optional
            rank of the root mpi process. If None, reduce operation
            is done on all processes.
        comm : mpi communicator, optional
            Communicator used to reduce indices. Default = topo.parent
        dom : :class:`~hysop.domain.domain.Domain`
            current domain.

        Returns
        -------
        either :
        * a dictionnary which maps rank number with
        a list of slices such that res[rank][i] = a slice
        defining the indices of the points of the local mesh,
        in direction i, in global notation.
        * or a numpy array where each column corresponds to a rank number,
        with column = [start_x, end_x, start_y, end_y ...]
        Ranks number are the processes numbers in comm.

        """
        if topo is None:
            assert comm is not None and dom is not None
            size = comm.Get_size()
            rank = comm.Get_rank()
            dimension = dom.dim
            iglob = npw.integer_zeros((dimension * 2, size), order="F")
            iglob_res = npw.integer_zeros((dimension * 2, size), order="F")
            iglob[0::2, rank] = 0
            iglob[1::2, rank] = -1
            if root is None:
                comm.Allgather([iglob[:, rank], MPI.INT], [iglob_res, MPI.INT])
            else:
                comm.Gather([iglob[:, rank], MPI.INT], [iglob_res, MPI.INT], root=root)
            if toslice:
                return Utils.array_to_dict(iglob_res)
            else:
                return iglob_res
        else:
            return TopoTools.gather_global_indices(topo, toslice, root, comm)

    @staticmethod
    def is_parent(child, parent):
        """
        Return true if all mpi processes of child belong to parent
        """
        # Get the list of processes
        assert child is not None
        assert parent is not None
        child_group = child.Get_group()
        parent_group = parent.Get_group()
        inter_group = MPI.Group.Intersect(child_group, parent_group)
        return child_group.Get_size() == inter_group.Get_size()

    @staticmethod
    def intersection_size(comm_1, comm_2):
        """Number of processess common to comm_1 and comm_2"""
        if comm_1 == MPI.COMM_NULL or comm_2 == MPI.COMM_NULL:
            return None
        group_1 = comm_1.Get_group()
        group_2 = comm_2.Get_group()
        inter_group = MPI.Group.Intersect(group_1, group_2)
        return inter_group.Get_size()

    @staticmethod
    def compare_comm(comm_1, comm_2):
        """Compare two mpi communicators.

        Returns true if the two communicators are handles for the same
        group of proc and for the same communication context.

        Warning : if comm_1 or comm_2 is invalid, the
        function will fail.
        """
        assert comm_1 != MPI.COMM_NULL
        assert comm_2 != MPI.COMM_NULL
        result = MPI.Comm.Compare(comm_1, comm_2)
        res = [MPI.IDENT, MPI.CONGRUENT, MPI.SIMILAR, MPI.UNEQUAL]
        return result == res[0]

    @staticmethod
    def compare_groups(comm_1, comm_2):
        """Compare the groups of two mpi communicators.

        Returns true if each comm handles the
        same group of mpi processes.

        Warning : if comm_1 or comm_2 is invalid, the
        function will fail.
        """
        assert comm_1 != MPI.COMM_NULL
        assert comm_2 != MPI.COMM_NULL
        result = MPI.Comm.Compare(comm_1, comm_2)
        res = [MPI.IDENT, MPI.CONGRUENT, MPI.SIMILAR, MPI.UNEQUAL]
        return result in res[:-1]

    @staticmethod
    def convert_ranks(source, target):
        """Find the values of ranks in target from ranks in source.

        Parameters
        ----------
        source, target : mpi communicators

        Returns a list 'ranks' such that ranks[i] = rank in target
        of process of rank i in source.
        """
        assert source != MPI.COMM_NULL and target != MPI.COMM_NULL
        g_source = source.Get_group()
        g_target = target.Get_group()
        size_source = g_source.Get_size()
        r_source = [i for i in range(size_source)]
        res = MPI.Group.Translate_ranks(g_source, r_source, g_target)
        return {r_source[i]: res[i] for i in range(size_source)}

    @staticmethod
    def create_subarray(sl_dict, data_shape, order, mpi_type=None, dtype=None):
        """Create a MPI subarray mask to be used in send/recv operations
        between some topologies.

        Parameters
        ----------
        sl_dict : dictionnary
            indices of the subarray for each rank,
            such that sl_dict[rk] = (slice(...), slice(...), ...)
        data_shape : shape (numpy-like) of the original array

        :Returns : dictionnary of MPI derived types.
        Keys = ranks in parent communicator.
        """
        mpi_type = first_not_None(mpi_type, dtype_to_mpi_type(dtype))
        if mpi_type is None:
            msg = "Either specify mpi_type or dtype."
            raise ValueError(msg)

        def _create_subarray(slc, data_shape):
            dim = len(data_shape)
            slc = tuple(slc[i].indices(data_shape[i]) for i in range(dim))
            subvshape = tuple(slc[i][1] - slc[i][0] for i in range(dim))
            substart = tuple(slc[i][0] for i in range(dim))
            substep = tuple(slc[i][2] for i in range(dim))
            assert all(substep[i] == 1 for i in range(dim))
            subtype = mpi_type.Create_subarray(
                data_shape, subvshape, substart, order=order
            )
            subtype.Commit()
            return subtype

        if isinstance(sl_dict, dict):
            subtypes = {}
            for rk, slc in sl_dict.items():
                subtypes[rk] = _create_subarray(slc, data_shape)
            return subtypes
        else:
            return _create_subarray(sl_dict, data_shape)

    @staticmethod
    def create_subarray_from_buffer(data, slices):
        from hysop.core.arrays.array import Array

        dim = data.ndim
        shape = data.shape
        dtype = data.dtype
        order = get_mpi_order(data)

        assert len(slices) == dim
        slices = tuple(slices[i].indices(shape[i]) for i in range(dim))
        subshape = tuple(slices[i][1] - slices[i][0] for i in range(dim))
        substart = tuple(slices[i][0] for i in range(dim))
        substep = tuple(slices[i][2] for i in range(dim))
        assert all(0 <= substart[i] < shape[i] for i in range(dim))
        assert all(0 < subshape[i] <= shape[i] for i in range(dim))
        assert all(substep[i] == 1 for i in range(dim))

        basetype = dtype_to_mpi_type(dtype)
        subtype = basetype.Create_subarray(shape, subshape, substart, order=order)
        subtype.Commit()
        return subtype

    @staticmethod
    def set_group_size(topo):
        """Set default size for groups of lines of particles,
        depending on the local resolution

        Parameters
        ----------
        topo: :class:`~hysop.topology.topology.CartesianTopology`

        Notes
        -----
        group_size shape == (problem dim - 1, problem dim)
        group_size[i, d] = size of groups in direction i when
        advection is performed in direction d.

        """
        resolution = topo.mesh.local_resolution - topo.ghosts()
        size_ref = [8, 5, 4, 2, 1]
        group_size = npw.int_ones((topo.domain.dim - 1, topo.domain.dim))
        # Default --> same size in each dir.
        # Note that a more complex mechanism is provided in scales
        # to compute group size.
        for i in size_ref:
            if (resolution % i == 0).all():
                group_size[...] = i
                return group_size

    @staticmethod
    def initialize_tag_parameters(topo, group_size):
        """Initialize tag_rank and tag_size, some
        parameters used to compute unique tag/ids for MPI messages.

        Parameters
        ----------
        topo: :class:`~hysop.topology.topology.CartesianTopology`
        group_size : numpy array
            size of groups of lines of particles in other directions
            than the current advection dir.

        Notes
        -----
        group_size shape == (problem dim - 1, problem dim)
        group_size[i, d] = size of groups in direction i when
        advection is performed in direction d.
        """
        # tag_size = smallest power of ten to ensure tag_size > max ind_group
        reduced_res = np.asarray(topo.mesh.local_resolution - topo.ghosts())
        n_group = npw.zeros_like(group_size)
        dimension = topo.domain.dim
        for i in range(dimension):
            ind = [j for j in range(len(reduced_res)) if j != i]
            n_group[:, i] = reduced_res[ind] // group_size[:, i]

        tag_size = npw.asintegerarray(np.ceil(np.log10(n_group)))
        tag_rank = max(2, math.ceil(math.log10(3 * max(topo.shape))))

        return tag_size, tag_rank
