# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop import vprint, dprint, __DEBUG__
from hysop.constants import Backend, MemoryOrdering
from hysop.tools.decorators import debug
from hysop.tools.htypes import check_instance
from hysop.core.graph.computational_node import ComputationalGraphNode
from hysop.core.graph.graph import initialized, discretized
from hysop.core.memory.memory_request import OperatorMemoryRequests
from hysop.topology.topology_descriptor import TopologyDescriptor
from hysop.fields.field_requirements import DiscreteFieldRequirements
from abc import ABCMeta


class ComputationalGraphOperator(ComputationalGraphNode, metaclass=ABCMeta):
    """
    Interface of an abstract computational graph operator.
    An operator is a single graph node with its own inputs and outputs.

    For more complex operators that combines multiple operators, see
      *hysop.core.graph.computational_graph.ComputationalGraph
      *hysop.core.graph.node_generator.ComputationalGraphNodeGenerator

    For operators that offers different implementation backend, see
      *hysop.core.graph.computational_node_frontend.ComputationalGraphNodeFrontend

    About inputs and outputs fields (continuous fields):
        *An operator may be working on multiple topologies exposed by different
         fields. In this case supports_multiple_topologies() should return True.

        *An continuous field may have different discretization (topologies)
         as input and output. In this case supports_multiple_field_topologies()
         should return True.

        *An operator may support distribution along mpi processes.
         In this case supports_mpi() should return True.
         Distribution constraints along process can be specified
         by redefining get_field_requirements() for inputs and outputs.

        *Min and max ghosts and input/output state (base or transposition state for exemple)
         can be specified on a per variable basis in get_field_requirements()

    An operator may thus *override* the following class methods:
        supports_multiple_topologies()
        supports_multiple_field_topologies()
        supports_mpi()

    By default all those methods return False,
    meaning that it can only have input and output variables
    defined on one unique topology that is not distributed
    along multiple processes.

    To support the 'method' keyword, *override* the following methods:
        available_methods()
        default_method()
        handle method()

    To add field contraints on variables and their topologies, *override* the following method:
        get_field_requirements()

    To set or relax global contraints on all operator variables and their topologies,
    *override* the following method:
        get_operator_requirements()

    To declare that an operator does not invalidate an input_field when the
    same field is written, *override* the get_preserved_input_fields method.

    An operator has to *extend* the following abstract method:
        apply(simulation, **kwds)

    An operator may *extend* the definition the following methods:
        pre_initialize()
        initialize()
        post_initialize()
        get_work_properties()
        setup()
        finalize()

    When extending methods pay attention to *always* call base class methods first.
    Not calling base methods may lead to unexpected behaviours in present/future
    releases. When overriding methods, you can just redefine it.

    See hysop.core.computational_node.ComputationalGraphNode for more information
    about those methods.

    Order of operation of an operator:
        __init__()
            self.initialized is False
            self.discretized is False
            self.ready       is False
            -> topologies/topology descriptors are checked
        initialize()
            pre_initialize()
            handle method()             -> self.method is set
            get_field_requirements()    -> self.input_field_requirements it set
            get_operator_requirements() -> self.operator_requirements is set
            check_requirements_compatibility() -> updates
            self._initialized is set
            post_intialize()
        check() (requires self.initialized to be set)
        discretize() (requires self.initialized to be set)
            self.discretized is set
        get_work_properties() (requires self.discretized to be set)
            returns required work (extra temporary buffers required for the computations)
        setup() (requires self.discretized to be set)
            self.ready is set
        apply() (requires self.ready to be set)
            abtract method to be implemented by each operator
        finalize() (requires self.ready to be set)
            sets self.ready to False

    Operators support checkpointing, ie. continuing a simulation state from a checkpoint stored on disk.
    In order for this to work, some operators may store data during checkpoint export and reload data
    during checkpoint import. In order for this to work, operators have to override the following methods:
        checkpoint_required() should return True
        save_checkpoint(self, datagroup, mpi_params, io_params, compressor)
        load_checkpoint(self, datagroup, mpi_params, io_params, relax_constraints)
    By default, datagroup are saved and retrieved based on operator name.
    When this is not sufficient, operators can override checkpoint_datagroup_key() to pass a custom key.

    Nothing besides __init__ should be called explicitely by the user as a
    ComputationalGraphOperator should always be embedded into a hysop.problem.Problem,
    or at least, a child class of hysop.core.graph.computational_graph.ComputationalGraph.
    """

    @debug
    def __new__(cls, input_fields=None, output_fields=None, **kwds):
        return super().__new__(
            cls, input_fields=input_fields, output_fields=output_fields, **kwds
        )

    @debug
    def __init__(self, input_fields=None, output_fields=None, **kwds):
        """
        Parameters
        ----------
        input_fields: dict, optional
            input fields as a dictionnary (see Notes).
        output_fields: dict, optional
            output fields as a dictionnary (see Notes).
        kwds: dict (see Notes)
            arguments for base classes.

        Attributes
        ----------
        input_discrete_fields: dict
            Dictionary containing continuous input fields as keys and discrete
            variables as values.
        output_discrete_fields: dict
            Dictionary containing continuous output fields as keys and discrete
            variables as values.

        Notes
        -----
        For the input and output fields, the keys of the dicts have to be of
        type :class:`hysop.fields.continuous_field.Field`.
        and the values should consist of
        :class:`hysop.topology.topology_descriptor.TopologyDescriptors` instances
        ie. an already defined topology or a topology descriptor.

        The following base class variables cannot be specified in kwds
            input_vars, output_vars, variables, rwork, iwork, work, backend.

        Nothing besides __init__ should be called manually by the user as it
        should be embedded into a hysop.problem.Problem, or at least in some
        hysop.core.graph.computational_graph.ComputationalGraph derivative.
        """
        self._format_topology_descriptors(input_fields, output_fields)
        super().__init__(input_fields=input_fields, output_fields=output_fields, **kwds)

    def _format_topology_descriptors(self, input_fields, output_fields):
        """
        Format input_fields and output_fields values to
        usable TopologyDescriptors before ComputationalGraphNode
        is initialized.
        """
        pass

    @debug
    def create_topology_descriptors(self):
        """
        Called in get_field_requirements, just after handle_method
         Topology requirements (or descriptors) are:
             1) min and max ghosts for each input and output variables
             2) allowed splitting directions for cartesian topologies
        """
        # by default we create HOST (cpu) TopologyDescriptors
        for field, topo_descriptor in self.input_fields.items():
            topo_descriptor = TopologyDescriptor.build_descriptor(
                backend=Backend.HOST, operator=self, field=field, handle=topo_descriptor
            )
            self.input_fields[field] = topo_descriptor

        for field, topo_descriptor in self.output_fields.items():
            topo_descriptor = TopologyDescriptor.build_descriptor(
                backend=Backend.HOST, operator=self, field=field, handle=topo_descriptor
            )
            self.output_fields[field] = topo_descriptor

    @debug
    def get_field_requirements(self):
        """
        Called just after handle_method(), ie self.method has been set.
        Field requirements are:
            1) required local and global transposition state, if any.
            2) required memory ordering (either C or Fortran)
        Default is Backend.HOST, no min or max ghosts, MemoryOrdering.ANY
        and no specific default transposition state for each input and output variables.
        """

        # Create the topology descriptors
        self.create_topology_descriptors()

        # We use default DiscreteFieldRequirements (ie. no min ghosts, no max ghosts,
        # can_split set to True in all directions, all TranspositionStates
        # and C memory ordering).
        input_field_requirements = {}
        for field, topo_descriptor in self.input_fields.items():
            if topo_descriptor is None:
                req = None
            else:
                workdim = topo_descriptor.domain.dim
                req = DiscreteFieldRequirements(self, self.input_fields, field)
                req.axes = None
                req.memory_order = None
            input_field_requirements[field] = req

        output_field_requirements = {}
        for field, topo_descriptor in self.output_fields.items():
            if topo_descriptor is None:
                req = None
            else:
                workdim = topo_descriptor.domain.dim
                req = DiscreteFieldRequirements(self, self.output_fields, field)
                req.axes = None
                req.memory_order = MemoryOrdering.C_CONTIGUOUS
            output_field_requirements[field] = req

        requirements = super().get_field_requirements()
        requirements.update_inputs(input_field_requirements)
        requirements.update_outputs(output_field_requirements)
        return requirements

    def get_preserved_input_fields(self):
        """
        Declare fields that should not be invalidated as a set.
        You can only declare scalar fields that are input and output fields at the same time.
        For example, a redistribute operation does not invalidate input fields.
        """
        return set()

    @debug
    def get_node_requirements(self):
        """
        Called after get_field_requirements to get global operator requirements.

        By default we enforce unique:
            *transposition state
            *cartesian topology shape
            *memory order (either C or fortran)
        Across every fields.
        """
        from hysop.core.graph.node_requirements import OperatorRequirements

        reqs = OperatorRequirements(
            self,
            enforce_unique_transposition_state=True,
            enforce_unique_topology_shape=True,
            enforce_unique_memory_order=True,
            enforce_unique_ghosts=False,
        )
        return reqs

    @debug
    def _check_inout_topology_states(
        self, ifields, itopology_states, ofields, otopology_states
    ):
        if ifields != itopology_states:
            msg = "\nFATAL ERROR: {}::{}.handle_topologies()\n\n"
            msg = msg.format(type(self).__name__, self.name)
            msg += (
                "input_topology_states fields did not match operator's input Fields.\n"
            )
            if ifields - itopology_states:
                msg += "input_topology_states are missing the following Fields: {}\n"
                msg = msg.format(ifields - itopology_states)
            else:
                msg += "input_topology_states is providing useless extra Fields: {}\n"
                msg = msg.format(itopology_states - ifields)
            raise RuntimeError(msg)

        if ofields != otopology_states:
            msg = "\nFATAL ERROR: {}::{}.handle_topologies()\n\n"
            msg = msg.format(type(self).__name__, self.name)
            msg += "output_topology_states fields did not match operator's output Fields.\n"
            if ofields - otopology_states:
                msg += "output_topology_states are missing the following Fields: {}\n"
                msg = msg.format(ofields - otopology_states)
            else:
                msg += "output_topology_states is providing useless extra Fields: {}\n"
                msg = msg.format(otopology_states - ofields)
            raise RuntimeError(msg)

    @debug
    def handle_topologies(self, input_topology_states, output_topology_states):
        """
        Called just after all topologies have been set up by the graph builder.
        Once this has been called, an operator can retrieve its
        input and output topologies contained in self.input_fields and self.output_fields.

        ComputationalGraph creates the topologies we need for the current
        operator by using TopologyDescriptors and FieldRequirements returned by
        get_field_requirements().

        All topologies, input and output topology states have to comply with
        the operator field requirements obtained with self.get_field_requirements(),
        else this will raise a RuntimeError because it is an internal graph builder
        failure.

        Notes
        -----
        This function will take those generated topologies and build
        topology view on top of it to match input and output field requirements.

        Graph generated field topologies are available as values of
        self.input_fields and self.output_fields and are mapped by continuous Field.

        In addition input_topology_states and output_topology_states
        are passed as argument and contain input discrete topology states
        and output topology states that the graph builder determined.

        Once this has been checked, topologies are replaced with TopologyView instances
        (wich is a Topology associated to a TopologyState).
        A TopologyView is basically a read-only view on top of a Topology with is altered
        by a TopologyState.

        A TopologyState is a topology dependent state, and acts as a virtual state
        that determines how we should perceive raw mesh data.

        It may for example include a transposition state for CartesianTopology topologies,
        resulting in the automatic permutation of attributes when fetching attributes
        (global_resolution and ghosts will be transposed). It may alors contain MemoryOrdering
        information to tell us to see the data either as C_CONTIGUOUS or F_CONTIGUOUS.

        Thus self.input_fields and self.output_fields values will containi topology state
        dependent topologies after this call. This allows operator implementations that are
        state agnostic and thus much simpler to implement without errors.

        RedistributeInter may not have both input and output topology states. In that specific
        case, (in-|out-)topology can be None
        """
        super().handle_topologies(input_topology_states, output_topology_states)

        input_states_reports = []

        # check input - output topology states
        ifields = set(self.input_fields.keys())
        itopology_states = set(input_topology_states.keys())
        ofields = set(self.output_fields.keys())
        otopology_states = set(output_topology_states.keys())
        self._check_inout_topology_states(
            ifields=ifields,
            itopology_states=itopology_states,
            ofields=ofields,
            otopology_states=otopology_states,
        )

        for ifield in ifields:
            itopo = self.input_fields[ifield]
            if itopo is None:
                from hysop.core.mpi.redistribute import RedistributeInter

                assert isinstance(self, RedistributeInter)
                continue
            ireq = self.input_field_requirements[ifield].as_dfr()
            istate = input_topology_states[ifield]
            if ireq is None:
                continue

            try:
                ireq.check_topology(itopo)
            except RuntimeError:
                msg = "\nFATAL ERROR: {}::{}.handle_topologies() input topology mismatch.\n\n"
                msg = msg.format(type(self).__name__, self.name)
                msg += "Operator expected field {} topology to match the following requirements\n"
                msg = msg.format(ifield.name)
                msg += f"  {str(ireq)}\n"
                msg += "but input effective discrete field topology is\n"
                msg += f"  {itopo}\n"
                msg += "\n"
                print(msg)
                raise

            try:
                ireq.check_discrete_topology_state(istate)
            except RuntimeError:
                msg = "\nFATAL ERROR: {}::{}.handle_topologies() input state mismatch.\n\n"
                msg = msg.format(type(self).__name__, self.name)
                msg += "Operator expected field {} on topology id {} to match the following "
                msg += "requirements\n"
                msg = msg.format(ifield.name, itopo.id)
                msg += f"  {str(ireq)}\n"
                msg += "but input discrete topology state determined by the graph builder is\n"
                msg += f"  {istate}\n"
                msg += "\n"
                raise

            istate_report = f"Field {ifield.name}: {istate} (topo={itopo.id})"
            input_states_reports.append(istate_report)

        # check output topology states
        for ofield in ofields:
            otopo = self.output_fields[ofield]
            if otopo is None:
                from hysop.core.mpi.redistribute import RedistributeInter

                assert isinstance(self, RedistributeInter)
                continue
            oreq = self.output_field_requirements[ofield].as_dfr()
            ostate = output_topology_states[ofield]

            try:
                oreq.check_topology(otopo)
            except RuntimeError:
                msg = "\nFATAL ERROR: {}::{}.handle_topologies() output topology mismatch.\n\n"
                msg = msg.format(type(self).__name__, self.name)
                msg += "Operator expected field {} topology to match the following requirements\n"
                msg = msg.format(ofield.name)
                msg += f"  {oreq}\n"
                msg += "but output effective discrete field topology is\n"
                msg += f"  {otopo}\n"
                msg += "\n"
                print(msg)
                raise

            try:
                oreq.check_discrete_topology_state(ostate)
            except RuntimeError:
                msg = "\nFATAL ERROR: {}::{}.handle_topologies() output state mismatch.\n\n"
                msg = msg.format(type(self).__name__, self.name)
                msg += "Operator expected field {} on topology id {} to match the following "
                msg += "requirements\n"
                msg = msg.format(ofield.name, otopo.id)
                msg += f"  {oreq}\n"
                msg += "but output discrete topology state determined by the graph builder is\n"
                msg += f"  {ostate}\n"
                msg += "\nAll input states matched:\n"
                msg += "\n".join([f"  *{state}" for state in input_states_reports])
                msg += "\n"
                print(msg)
                raise

        # replace topologies by topology view using topology states
        input_fields = self.input_fields
        for field, topology in input_fields.items():
            if topology is not None:
                topology_state = input_topology_states[field].copy(is_read_only=True)
                input_fields[field] = topology.view(topology_state)

        output_fields = self.output_fields
        for field, topology in output_fields.items():
            if topology is not None:
                topology_state = output_topology_states[field].copy(is_read_only=False)
                output_fields[field] = topology.view(topology_state)

    @debug
    @initialized
    def discretize(self):
        """
        By default, an operator discretize all its variables.
        For each input continuous field that is also an output field,
        input topology may be different from the output topology.

        After this call, one can access self.input_discrete_fields
        and self.output_discrete_fields, which contains input and output
        dicretised fields mapped by continuous fields.

        self.discrete_fields will be a tuple containing all input and
        output discrete fields.

        Discrete tensor fields are built back from discretized scalar fields
        and are accessible from self.input_tensor_fields, self.output_tensor_fields
        and self.discrete_tensor_fields like their scalar counterpart.
        """
        if self.discretized:
            return
        if not self.topology_handled:
            msg = "\nTopologies in operator {} topologies have not been handled yet."
            msg += "\nThis may be the case if this operator has not been embedded into "
            msg += "a ComputationalGraph."
            msg = msg.format(self.name)
            raise RuntimeError(msg)

        self.input_discrete_fields = {}
        self.output_discrete_fields = {}
        self.input_discrete_tensor_fields = {}
        self.output_discrete_tensor_fields = {}

        super().discretize()

        # discretize ScalarFields
        for field, topology_view in self.input_fields.items():
            if topology_view is not None:
                topology, topology_state = (
                    topology_view.topology,
                    topology_view.topology_state,
                )
                self.input_discrete_fields[field] = field.discretize(
                    topology, topology_state
                )
        for field, topology_view in self.output_fields.items():
            if topology_view is not None:
                topology, topology_state = (
                    topology_view.topology,
                    topology_view.topology_state,
                )
                self.output_discrete_fields[field] = field.discretize(
                    topology, topology_state
                )

        # build back DiscreteTensorFields from DiscreteScalarFields
        from hysop.fields.discrete_field import DiscreteTensorField

        for tfield in self.input_tensor_fields:
            dfields = tfield.new_empty_array()
            for idx, field in tfield.nd_iter():
                dfield = self.input_discrete_fields[field]
                dfields[idx] = dfield
            tdfield = DiscreteTensorField(field=tfield, dfields=dfields)
            self.input_discrete_tensor_fields[tfield] = tdfield

        for tfield in self.output_tensor_fields:
            dfields = tfield.new_empty_array()
            for idx, field in tfield.nd_iter():
                dfield = self.output_discrete_fields[field]
                dfields[idx] = dfield
            tdfield = DiscreteTensorField(field=tfield, dfields=dfields)
            self.output_discrete_tensor_fields[tfield] = tdfield

        self.discrete_fields = tuple(
            set(self.input_discrete_fields.values()).union(
                self.output_discrete_fields.values()
            )
        )

        self.discrete_tensor_fields = tuple(
            set(self.input_discrete_tensor_fields.values()).union(
                self.output_discrete_tensor_fields.values()
            )
        )
        self.discretized = True

    @debug
    @discretized
    def get_work_properties(self):
        """
        Returns extra memory requirements of this operator.
        This allows operators to request for temporary buffers
        that will be shared between operators in a graph to reduce
        the memory footprint and the number of allocations.

        Returned memory is only usable during operator call (ie. in self.apply).
        Temporary buffers may be shared between different operators as determined
        by the graph builder.

        By default if there is no input nor output temprary fields, this returns no requests,
        meanning that this node requires no extra buffers.

        If temporary fields are present, their memory request are automatically
        computed and returned.
        """
        requests = OperatorMemoryRequests(self)
        delayed_requests = {}
        for dfield in self.discrete_fields:
            if dfield.is_tmp:
                if dfield.mem_tag is not None:
                    req_id = dfield.mem_tag
                else:
                    req_id = f"tmp_{dfield.name}_{dfield.tag}"
                try:
                    requests.push_mem_request(req_id, dfield.dfield.memory_request)
                except ValueError:
                    pass
        return requests

    @debug
    @discretized
    def setup(self, work):
        """
        Setup temporary buffer that have been requested in get_work_properties().
        This function may be used to execute post allocation routines.
        This sets self.ready flag to True.
        Once this flag is set one may call ComputationalGraphNode.apply() and
        ComputationalGraphNode.finalize().

        Automatically honour temporary field memory requests.
        """
        self.allocate_tmp_fields(work)
        super().setup(work)
        for f in self.input_fields:
            freq = self._field_requirements.get_input_requirement(f)[1]
            assert (
                freq.tstates is None
                or self.input_discrete_fields[f].topology_state.tstate in freq.tstates
            )
            assert (
                freq.tstates is None
                or self.input_discrete_fields[f].topology_state.axes in freq.axes
            )

    def allocate_tmp_fields(self, work):
        for dfield in self.discrete_fields:
            if dfield.is_tmp and (dfield._dfield._data is None):
                if dfield.mem_tag is not None:
                    req_id = dfield.mem_tag
                else:
                    req_id = f"tmp_{dfield.name}_{dfield.tag}"
                data = work.get_buffer(self, req_id)
                dfield.dfield.honor_memory_request(data)

    @classmethod
    def supported_backends(cls):
        """
        Return the backends that this operator's topologies can support as a set.
        By default all operators support only Backend.HOST.
        """
        return {Backend.HOST}

    @debug
    @initialized
    def check(self):
        """
        Check if node was correctly initialized.
        This calls ComputationalGraphNode.check() and then checks backend
        by comparing all variables topology backend to set.supported_backends().
        """
        super().check()
        self._check_backend()

    def output_topology_state(self, output_field, input_topology_states):
        """
        Determine a specific output discrete topology state given all
        input discrete topology states.

        Must be redefined to help correct computational graph generation.
        By default, just return first input state if all input states are all the same.

        If input_topology_states are different, raise a RuntimeError as default
        behaviour. Operators altering the state of their outputs *have* to
        override this method.

        The state may include transposition state, memory order and more.
        see hysop.topology.transposition_state.TranspositionState for the complete list.
        """

        from hysop.fields.continuous_field import Field
        from hysop.topology.topology import TopologyState

        check_instance(output_field, Field)
        check_instance(input_topology_states, dict, keys=Field, values=TopologyState)

        assert output_field in self.output_fields.keys()
        assert set(input_topology_states.keys()) == set(self.input_fields.keys())

        if input_topology_states:
            (ref_field, ref_state) = tuple(input_topology_states.items())[0]
            ref_topo = self.input_fields[ref_field]
        else:
            ref_state = self.output_fields[output_field].topology_state

        for ifield, istate in input_topology_states.items():
            itopo = self.input_fields[ifield]
            if not istate.match(ref_state):
                msg = "\nInput topology state for field {} defined on topology {} does "
                msg += "not match reference input topology state {} defined on topology {} "
                msg += "for operator {}.\n"
                msg += (
                    "ComputationalGraphOperator default behaviour is to raise an error "
                )
                msg += "when all input states do not match exactly.\n\n"
                msg += "Reference state: {}\n"
                msg += "Offending state: {}\n\n"
                msg += "This behaviour can be changed by overriding output_topology_state() for "
                msg += "your custom operator needs."
                msg = msg.format(
                    ifield.name,
                    itopo.tag,
                    ref_field.name,
                    ref_topo.tag,
                    self.name,
                    ref_state,
                    istate,
                )
                raise RuntimeError(msg)
        return ref_state.copy()

    def operators(self):
        """
        Return a list containing self and IO operators that should be included
        into the graph. This method is called by the graph builder and should
        not be called by user.
        """
        from hysop.operator.hdf_io import HDF_Writer

        ops = []

        for fields, io_params, op_kwds in self._input_fields_to_dump:
            input_fields, input_t_fields = self.input_fields, self.input_tensor_fields
            ifields = {}
            for f in fields:
                if f in input_t_fields:
                    ifields[f] = input_fields[f.fields[0]]
                else:
                    for fi in f.fields:
                        ifields[fi] = input_fields[fi]
            variables = {
                k: (v if v.backend.kind in HDF_Writer.supported_backends() else None)
                for (k, v) in ifields.items()
            }
            op = HDF_Writer(io_params=io_params, variables=variables, **op_kwds)
            op.initialize(topgraph_method=self.method)
            op.get_and_set_field_requirements()
            ops.append(op)

        ops.append(self)

        for fields, io_params, op_kwds in self._output_fields_to_dump:
            output_fields, output_t_fields = (
                self.output_fields,
                self.output_tensor_fields,
            )
            ofields = {}
            for f in fields:
                if f in output_t_fields:
                    ofields[f] = output_fields[f.fields[0]]
                else:
                    for fi in f.fields:
                        ofields[fi] = output_fields[fi]
            variables = {
                k: (v if v.backend.kind in HDF_Writer.supported_backends() else None)
                for (k, v) in ofields.items()
            }
            op = HDF_Writer(io_params=io_params, variables=variables, **op_kwds)
            op.initialize(topgraph_method=self.method)
            op.get_and_set_field_requirements()
            ops.append(op)

        return ops

    def checkpoint_required(self):
        """
        Should return True if this operator may export/import custom checkpoint data.
        Can be overriden to enable operator checkpointing.
        """
        return False

    def checkpoint_datagroup_key(self):
        """
        By default the checkpoint datagroup key is based on operator name.
        This can be overriden to generate custom keys.
        Note that all keys are post-processing by using CheckpointHandler._format_zarr_key.
        """
        return self.name

    def save_checkpoint(self, datagroup, mpi_params, io_params, compressor):
        """
        Save custom operator data to a checkpoint.

        Datagroup is a zarr.hierarchy.Datagroup object, see hysop.core.checkpoints.CheckpointHandler for example usage.
        Parameters mpi_params and io_params are MPIParams and IOParams coming from the CheckpointHandler.
        You can create numpy-like arrays with datagroup.create_dataset and subgroups with datagroup.create_group.
        Compressor is the compressor that should be used when creating arrays with datagroup.create_dataset.

        Each group or array can contain a json-serialisable dictionary of metadata.
        Metadata can be set as the following:
          group.attrs[key] = value
        or
          array.attrs[key] = value
        where key is a string that does not contain '\' or '/', see hysop.core.checkpoints.CheckpointHandler._format_zarr_key.

        Only io_leader should write metadata, io_leader can be determined as (mpi_params.rank == io_params.io_leader)
        Multiple processes array writes should be synchronized unless they write to different blocks of data.
        See https://zarr.readthedocs.io/en/stable/tutorial.html#parallel-computing-and-synchronization for more information.
        """
        if self.checkpoint_required():
            msg = "Operator {} does require checkpointing but {}.save_checkpoint() has not been overriden."
            raise NotImplementedError(msg.format(self.name, self.__class__.__name__))
        else:
            msg = "{}.load_checkpoint() called but operator {} does not seem to require a checkpoint..."
            raise RuntimeError(msg.format(self.__class__.__name__, self.name))

    def load_checkpoint(self, datagroup, mpi_params, io_params, relax_constraints):
        """
        Reload custom operator data from a checkpoint.

        Datagroup is a zarr.hierarchy.Datagroup object, see hysop.core.checkpoints.CheckpointHandler for example usage.
        Parameters mpi_params and io_params are MPIParams and IOParams coming from the CheckpointHandler.
        If relax_constraints is set, you can ignore data discreapencies such as datatype, else should an error should be raised.

        Data arrays or subgroups can be accessed with the dict-like datagroup[key] syntax.
        Group or array metadata can be retrieved by using the group.attrs[key] or array.attrs[key] syntax where key is a
        string that does not contain '\' or '/', see hysop.core.checkpoints.CheckpointHandler._format_zarr_key.

        As this operation read-only, there is no need to synchronize processes.
        Also note that metadata type is not always the same when deserialized (for example tuples become lists).
        """
        if self.checkpoint_required():
            msg = "Operator {} does require checkpointing but {}.load_checkpoint() has not been overriden."
            raise NotImplementedError(msg.format(self.name, self.__class__.__name__))
        else:
            msg = "{}.load_checkpoint() called but operator {} does not seem to require a checkpoint..."
            raise RuntimeError(msg.format(self.__class__.__name__, self.name))

    def _check_backend(self):
        """
        Checks backend support and topologies.
        """
        topologies_per_backend = self.get_topologies()
        supported_backends = self.supported_backends()
        for backend, topologies in topologies_per_backend.items():
            if backend.kind not in supported_backends:
                bad_fields = set()
                for field, topo in self.input_fields.items():
                    if topo and (topo.backend is backend):
                        bad_fields.add(field)
                for field, topo in self.output_fields.items():
                    if topo and (topo.backend is backend):
                        bad_fields.add(field)
                msg = f"\n\nOperator {self.node_tag} topology backend mismatch:"
                msg += "\n -> got topologies defined on backend {} which is of kind {}.".format(
                    backend.full_tag, backend.kind
                )
                msg += "\n     *bad topology tags were {}.".format(
                    ", ".join(t.tag for t in topologies)
                )
                msg += "\n -> this operator only supports the following backends:"
                msg += "\n     *" + "\n     *".join(
                    [str(b) for b in supported_backends]
                )
                msg += "\n -> bad fields were:"
                msg += "\n     *" + "\n     *".join([f.full_tag for f in bad_fields])
                print("\nFATAL ERROR: Topology backend mismatch.\n")
                print("Offending topologies were:")
                for t in topologies:
                    print("\n", t)
                raise RuntimeError(msg)

    def to_graph(self, name=None):
        """Convert this operator to a computational graph."""
        from hysop.core.graph.computational_graph import ComputationalGraph

        msg = f"Generating graph from computational operator {name}."
        dprint(msg)
        name = name or f"{self.name}_graph"
        graph = ComputationalGraph(name=name)
        graph.push_nodes(self.operators)
        return graph

    def build(self, name=None, outputs_are_inputs=False, **kwds):
        """Convert an operator to a graph and prepares it for apply."""
        return self.to_graph(name=name).build(
            outputs_are_inputs=outputs_are_inputs, **kwds
        )

    @classmethod
    def available_methods(cls):
        return dict()

    @classmethod
    def default_method(cls):
        return dict()

    @property
    def enable_opencl_host_buffer_mapping(self):
        return False
