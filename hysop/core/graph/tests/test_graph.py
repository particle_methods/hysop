# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import tempfile
from hysop.domain.box import Box
from hysop.topology.cartesian_topology import CartesianTopology
from hysop.tools.parameters import CartesianDiscretization
from hysop.fields.continuous_field import Field
from hysop.core.graph.all import ComputationalGraphOperator, ComputationalGraph


class _ComputationalGraph(ComputationalGraph):
    @classmethod
    def supports_multiple_topologies(cls):
        return True


class _ComputationalGraphOperator(ComputationalGraphOperator):
    def apply(self):
        pass

    @classmethod
    def supports_multiple_topologies(cls):
        return True


class TestGraph:

    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def setup_method(self, method):
        pass

    def teardown_method(self, method):
        pass

    def test_graph_build(self, display=False):
        box = Box(length=[1.0] * 3, origin=[0.0] * 3)
        Vg = Field(domain=box, name="Vg", is_vector=True)
        Vp = Field(domain=box, name="Vp", is_vector=True)
        Wg = Field(domain=box, name="Wg", is_vector=True)
        Wp = Field(domain=box, name="Wp", is_vector=True)
        rho0g = Field(domain=box, name="rho0g")
        rho0p = Field(domain=box, name="rho0p")
        rho1g = Field(domain=box, name="rho1g")
        rho1p = Field(domain=box, name="rho1p")

        d3d0 = CartesianDiscretization(
            resolution=(64, 64, 64), ghosts=None, default_boundaries=True
        )
        d3d1 = CartesianDiscretization(
            resolution=(128, 128, 128), ghosts=None, default_boundaries=True
        )
        t0 = CartesianTopology(domain=box, discretization=d3d0)
        t1 = CartesianTopology(domain=box, discretization=d3d1)

        ops = [
            ("copyW", [Wg], [Wp]),
            ("copyS0", [rho0g], [rho0p]),
            ("copyS1", [rho1g], [rho1p]),
            ("copyV", [Vg], [Vg]),
        ]
        g0 = _ComputationalGraph(name="g0")
        for opname, _in, _out in ops:
            invars = {}
            outvars = {}
            for var in _in:
                invars[var] = t0 if (var.name.find("rho")) else t1
            for var in _out:
                outvars[var] = t0 if (var.name.find("rho")) else t1
            g0.push_nodes(
                _ComputationalGraphOperator(
                    name=opname, input_fields=invars, output_fields=outvars
                )
            )

        ops = [
            ("advecWx", [Vg, Wp], [Wg]),
            ("stretchWx", [Vg, Wg], [Wg]),
            ("diffWx", [Wg], [Wg]),
            ("advecS0x", [Vg, rho0p], [rho0g]),
            ("advecS1x", [Vg, rho1p], [rho1g]),
            ("diffS0x", [rho0g], [rho0g]),
            ("diffS1x", [rho1g], [rho1g]),
            ("forceWx", [rho0g, rho1g, Wg], [Wg]),
        ]
        g1 = _ComputationalGraph(name="g1")
        for opname, _in, _out in ops:
            invars = {}
            outvars = {}
            for var in _in:
                invars[var] = t0 if (var.name.find("rho")) else t1
            for var in _out:
                outvars[var] = t0 if (var.name.find("rho")) else t1
            g1.push_nodes(
                _ComputationalGraphOperator(
                    name=opname, input_fields=invars, output_fields=outvars
                )
            )

        g = _ComputationalGraph(name="exec")
        g.push_nodes(g0)
        g.push_nodes(g1)

        g.initialize()
        g.discretize()
        g.setup(None)
        g.apply()

        with tempfile.NamedTemporaryFile(suffix=".html") as f:
            g.to_html(os.path.basename(f.name))

        if display:
            g.display()


if __name__ == "__main__":
    test = TestGraph()
    test.test_graph_build(display=False)
