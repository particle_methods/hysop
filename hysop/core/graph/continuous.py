# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Common interface for all continuous operators.

"""
import os
from abc import ABCMeta, abstractmethod

from hysop import __PROFILE__, vprint, dprint
from hysop.core.mpi import default_mpi_params
from hysop.tools.io_utils import IOParams, IO
from hysop.tools.parameters import MPIParams
from hysop.tools.decorators import debug
from hysop.tools.htypes import check_instance
from hysop.tools.profiler import Profiler
from hysop.tools.handle import TaggedObject
from hysop.fields.continuous_field import ScalarField, TensorField
from hysop.parameters.parameter import Parameter

import hysop.tools.io_utils as io


class OperatorBase(TaggedObject, metaclass=ABCMeta):
    """
    Abstract interface to continuous operators.
    """

    @debug
    def __new__(
        cls,
        name,
        fields,
        tensor_fields,
        parameters,
        mpi_params=None,
        io_params=False,
        **kwds,
    ):
        return super().__new__(cls, tagged_cls=OperatorBase, tag_prefix="node", **kwds)

    @debug
    def __init__(
        self,
        name,
        fields,
        tensor_fields,
        parameters,
        mpi_params=None,
        io_params=False,
        **kwds,
    ):
        """
        Parameters
        ----------
        fields: tuple of ScalarField
            ScalarFields on which this operator operates
        tensor_fields: tuple of TensorField
            TensorFields on which this operator operates.
            All contained ScalarField have to be contained in fields.
        parameters: tuple of Parameter
            Parameters on which this operator operates
        mpi_params : :class:`hysop.tools.parameters.MPIParams`
            Mpi config for the operator (comm, task ...)
        io_params : :class:`hysop.tools.io_utils.IOParams`
            File i/o config (filename, format ...)

        Attributes
        ----------
        fields: tuple of Field
            fields on which this operator operates
        parameters: tuple of Parameter
            Parameters on which this operator operates
        io_params: IOParams
            Mpi config for the operator (comm, task ...)
        mpi_params: MPIParams
            File i/o config (filename, format ...)
        """
        super().__init__(tagged_cls=OperatorBase, tag_prefix="node", **kwds)

        check_instance(fields, tuple, values=ScalarField)
        check_instance(tensor_fields, tuple, values=TensorField)
        check_instance(parameters, tuple, values=Parameter)
        check_instance(io_params, (IOParams, bool), allow_none=True)
        check_instance(mpi_params, MPIParams, allow_none=True)

        for tfield in tensor_fields:
            for field in tfield:
                assert field in fields

        self.name = name
        self.fields = fields
        self.tensor_fields = tensor_fields
        self.parameters = parameters
        self.io_params = io_params
        self.mpi_params = mpi_params

        self._set_domain_and_tasks()
        self._set_io()

    def _get_profiling_info(self):
        """Collect profiling informations of profiled attributes."""
        pass

    def profiler_report(self):
        """Update profiler statistics and print report."""
        if not __PROFILE__:
            return
        self._profiler.summarize()
        print(str(self._profiler))

    def _set_io(self):
        """
        Initialize the io params.

        Notes
        -----
        This function is private and must not be called by
        external object. It is usually called by operator
        during construction (__init__).

        """
        iopar = self.io_params
        # if iopar is not None (i.e. set in operator init)
        # and True or set as an IOParams , then build a writer
        if iopar is not None:
            if isinstance(iopar, bool):
                if iopar is True:
                    filename = os.path.join(IO.default_path(), self.name)
                    self.io_params = IOParams(filename, fileformat=IO.HDF5)
                else:
                    self.io_params = None
            elif isinstance(iopar, IOParams):
                pass
            else:
                raise TypeError("Error, wrong type for io_params.")

    def _set_domain_and_tasks(self):
        """
        Initialize the mpi context, depending on local fields, domain
        and so on.

        Notes
        -----
        This function is private and must not be called by
        external object. It is usually called by operator
        during construction (__init__).
        """
        if len(self.fields) > 0:
            self.domain = self.fields[0].domain

            # Check if all fields have the same domain
            for field in self.fields:
                assert (
                    field.domain is self.domain
                ), "All fields of the operator\
                must be defined on the same domain."

            # Set/check mpi context
            if self.mpi_params is None:
                self.mpi_params = MPIParams(
                    comm=self.domain.task_comm, task_id=self.domain.current_task()
                )

        else:
            if self.mpi_params is None:
                self.mpi_params = default_mpi_params()
            self.domain = None

        self._profiler = None
        if self.mpi_params.on_task:
            self._profiler = Profiler(obj=self)

    def short_description(self):
        return f"{self.full_tag}[name={self.name}]"

    @abstractmethod
    def long_description(self):
        pass

    def __eq__(self, other):
        return self is other

    def __ne__(self, other):
        return self is not other

    def __hash__(self):
        return id(self)

    def __str__(self):
        return self.short_description()
