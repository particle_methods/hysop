# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop import __DEBUG__, __VERBOSE__, __PROFILE__, vprint, dprint
from hysop.tools.decorators import debug, profile
from hysop.tools.htypes import to_list, to_set, to_tuple, first_not_None, check_instance
from hysop.tools.string_utils import framed_str, strlen, multiline_split
from hysop.tools.numpywrappers import npw
from hysop.core.graph.graph import (
    initialized,
    discretized,
    ready,
    graph_built,
    not_initialized,
)
from hysop.core.graph.graph import ComputationalGraphNodeData
from hysop.core.graph.computational_node import ComputationalGraphNode
from hysop.core.graph.computational_operator import ComputationalGraphOperator
from hysop.core.graph.node_generator import ComputationalGraphNodeGenerator
from hysop.core.graph.node_requirements import NodeRequirements, OperatorRequirements
from hysop.core.memory.memory_request import MultipleOperatorMemoryRequests
from hysop.fields.field_requirements import MultiFieldRequirements
from hysop.topology.topology import Topology
from hysop.core.mpi.redistribute import RedistributeInter

from abc import ABCMeta, abstractmethod


class ComputationalGraph(ComputationalGraphNode, metaclass=ABCMeta):
    """
    Interface of an abstract graph of continuous operators (ie. a computational graph).
    """

    __FORCE_REPORTS__ = False

    @debug
    def __new__(
        cls, candidate_input_tensors=None, candidate_output_tensors=None, **kwds
    ):
        if ("input_fields" in kwds.keys()) or ("output_fields" in kwds.keys()):
            msg = "input_fields or output_fields parameters should not be used in {}, they are \
                    deduced during graph construction (building step).".format(
                cls
            )
            raise ValueError(msg)
        if ("input_params" in kwds.keys()) or ("output_params" in kwds.keys()):
            msg = "input_params or output_params parameters should not be used in {}, they are \
                    deduced during graph construction (building step).".format(
                cls
            )
            raise ValueError(msg)
        return super().__new__(cls, input_fields=None, output_fields=None, **kwds)

    @debug
    def __init__(
        self, candidate_input_tensors=None, candidate_output_tensors=None, **kwds
    ):
        """
        Parameters
        ----------
        kwds: arguments for base classes.

        Notes
        -----
        The following base class variables cannot be specified during graph construction:
            variables, input_variables, output_variables
        Order of operation is: add_node, initialize, discretize,
                               get_work_properties, setup, apply, finalize.
        Nodes can also be added during pre initialization step.
        Graph building is done at the end of the initialization step, after all internal
        nodes have been initialized.
        """
        super().__init__(input_fields=None, output_fields=None, **kwds)

        self.nodes = []
        self.graph = None
        self.graph_built = False
        self.graph_is_rendering = False
        self._last_pushed_node_mpi_params = None
        self.candidate_input_tensors = set(first_not_None(candidate_input_tensors, ()))
        self.candidate_output_tensors = set(
            first_not_None(candidate_output_tensors, ())
        )

    @graph_built
    def _get_profiling_info(self):
        """Collect profiling informations of profiled attributes."""
        for node in self.nodes:
            self._profiler += node._profiler

    def node_requirements_report(self, requirements):
        values = [
            (
                "OPERATOR",
                "TOPOLOGY",
                "TSTATE",
                "GHOSTS",
                "MEMORY ORDER",
                "NODE.MRO[0]",
                "NODE.MRO[1]",
                "NODE.MRO[2]",
            )
        ]
        for node in self.nodes:
            reqs = node.get_node_requirements()
            if not isinstance(reqs, OperatorRequirements):
                continue
            opname = node.pretty_name
            optypes = type(node).__mro__
            n = len(optypes)
            optypes = tuple(_.__name__ for _ in optypes[: min(3, n)]) + ("",) * (3 - n)
            vals = (
                opname,
                reqs.enforce_unique_topology_shape,
                reqs.enforce_unique_transposition_state,
                reqs.enforce_unique_ghosts,
                reqs.enforce_unique_memory_order,
            ) + optypes
            vals = tuple(map(str, vals))
            values.append(vals)

        template = "\n   {:<{name_size}}   {:^{topology_size}}      {:^{tstates_size}}      {:^{ghosts_size}}      {:^{order_size}}      {:<{type_size0}}      {:<{type_size1}}      {:<{type_size2}}"
        name_size = max(strlen(s[0]) for s in values)
        topology_size = max(strlen(s[1]) for s in values)
        tstates_size = max(strlen(s[2]) for s in values)
        ghosts_size = max(strlen(s[3]) for s in values)
        order_size = max(strlen(s[4]) for s in values)
        type_size0 = max(strlen(s[5]) for s in values)
        type_size1 = max(strlen(s[6]) for s in values)
        type_size2 = max(strlen(s[7]) for s in values)

        ss = ""
        for (
            opname,
            enforce_unique_topology_shape,
            enforce_unique_transposition_state,
            enforce_unique_ghosts,
            enforce_unique_memory_order,
            optype0,
            optype1,
            optype2,
        ) in values:
            ss += template.format(
                opname,
                enforce_unique_topology_shape,
                enforce_unique_transposition_state,
                enforce_unique_ghosts,
                enforce_unique_memory_order,
                optype0,
                optype1,
                optype2,
                name_size=name_size,
                topology_size=topology_size,
                tstates_size=tstates_size,
                ghosts_size=ghosts_size,
                order_size=order_size,
                type_size0=type_size0,
                type_size1=type_size1,
                type_size2=type_size2,
            )

        title = "ComputationalGraph {} node requirements report ".format(
            self.pretty_name
        )
        return f"\n{framed_str(title=title, msg=ss[1:])}\n"

    def field_requirements_report(self, requirements):
        inputs, outputs = {}, {}
        sinputs, soutputs = {}, {}

        def sorted_reqs(reqs):
            return sorted(reqs, key=lambda x: f"{x.field.name}::{x.operator.name}")

        for field, mreqs in requirements.input_field_requirements.items():
            for td, reqs in mreqs.requirements.items():
                for req in reqs:
                    inputs.setdefault(td, {}).setdefault(field, []).append(req)
        for td, td_reqs in inputs.items():
            sin = sinputs.setdefault(td, [])
            for field, reqs in td_reqs.items():
                for req in sorted_reqs(reqs):
                    opname = getattr(req.operator, "pretty_name", "UnknownOperator")
                    fname = getattr(req.field, "pretty_name", "UnknownField")
                    min_ghosts = req.ghost_str(req.min_ghosts)
                    max_ghosts = req.ghost_str(req.max_ghosts + 1)
                    discr = str(req.operator.input_fields[field].grid_resolution)
                    ghosts = f"{min_ghosts}<=ghosts<{max_ghosts}"
                    can_split = req.can_split.view(npw.int8)
                    memory_order = f"{req.memory_order}" if req.memory_order else "ANY"
                    can_split = "[{}]".format(
                        ",".join("1" if cs else "0" for cs in req.can_split)
                    )
                    tstates = (
                        "{}".format(",".join(str(ts) for ts in req.tstates))
                        if req.tstates
                        else "ANY"
                    )
                    sin.append(
                        (opname, fname, discr, ghosts, memory_order, can_split, tstates)
                    )
        for field, mreqs in requirements.output_field_requirements.items():
            for td, reqs in mreqs.requirements.items():
                for req in reqs:
                    outputs.setdefault(td, {}).setdefault(field, []).append(req)
        for td, td_reqs in outputs.items():
            sout = soutputs.setdefault(td, [])
            for field, reqs in td_reqs.items():
                for req in sorted_reqs(reqs):
                    opname = getattr(req.operator, "pretty_name", "UnknownOperator")
                    fname = getattr(req.field, "pretty_name", "UnknownField")
                    min_ghosts = req.ghost_str(req.min_ghosts)
                    max_ghosts = req.ghost_str(req.max_ghosts + 1)
                    discr = str(req.operator.output_fields[field].grid_resolution)
                    ghosts = f"{min_ghosts}<=ghosts<{max_ghosts}"
                    can_split = req.can_split.view(npw.int8)
                    memory_order = f"{req.memory_order}" if req.memory_order else "ANY"
                    can_split = "[{}]".format(
                        ",".join("1" if cs else "0" for cs in req.can_split)
                    )
                    tstates = (
                        "{}".format(",".join(str(ts) for ts in req.tstates))
                        if req.tstates
                        else "ANY"
                    )
                    sout.append(
                        (opname, fname, discr, ghosts, memory_order, can_split, tstates)
                    )

        titles = [
            [
                (
                    "OPERATOR",
                    "FIELD",
                    "DISCRETIZATION",
                    "GHOSTS",
                    "MEMORY ORDER",
                    "CAN_SPLIT",
                    "TSTATES",
                )
            ]
        ]
        vals = tuple(sinputs.values()) + tuple(soutputs.values()) + tuple(titles)
        name_size = max(len(s[0]) for ss in vals for s in ss)
        field_size = max(len(s[1]) for ss in vals for s in ss)
        discr_size = max(len(s[2]) for ss in vals for s in ss)
        ghosts_size = max(len(s[3]) for ss in vals for s in ss)
        order_size = max(len(s[4]) for ss in vals for s in ss)
        cansplit_size = max(len(s[5]) for ss in vals for s in ss)
        tstates_size = max(len(s[6]) for ss in vals for s in ss)

        template = "\n   {:<{name_size}}   {:^{field_size}}     {:^{discr_size}}      {:^{ghosts_size}}      {:^{order_size}}      {:^{cansplit_size}}      {:^{tstates_size}}"

        ss = ">INPUTS:"
        if sinputs:
            for td, sreqs in sorted(sinputs.items(), key=lambda _: _[1][0]):
                if isinstance(td, Topology):
                    ss += f"\n {td.short_description()}"
                else:
                    ss += f"\n {td}"
                ss += template.format(
                    *titles[0][0],
                    name_size=name_size,
                    field_size=field_size,
                    discr_size=discr_size,
                    ghosts_size=ghosts_size,
                    order_size=order_size,
                    cansplit_size=cansplit_size,
                    tstates_size=tstates_size,
                )
                for opname, fname, discr, ghosts, order, can_split, tstates in sorted(
                    sreqs
                ):
                    ss += template.format(
                        opname,
                        fname,
                        discr,
                        ghosts,
                        order,
                        can_split,
                        tstates,
                        name_size=name_size,
                        field_size=field_size,
                        discr_size=discr_size,
                        ghosts_size=ghosts_size,
                        order_size=order_size,
                        cansplit_size=cansplit_size,
                        tstates_size=tstates_size,
                    )
        else:
            ss += " None"
        ss += "\n>OUTPUTS:"
        if soutputs:
            for td, sreqs in sorted(soutputs.items(), key=lambda _: _[1][0]):
                if isinstance(td, Topology):
                    ss += f"\n {td.short_description()}"
                else:
                    ss += f"\n {td}"
                ss += template.format(
                    *titles[0][0],
                    name_size=name_size,
                    field_size=field_size,
                    discr_size=discr_size,
                    ghosts_size=ghosts_size,
                    order_size=order_size,
                    cansplit_size=cansplit_size,
                    tstates_size=tstates_size,
                )
                for opname, fname, discr, ghosts, order, can_split, tstates in sorted(
                    sreqs
                ):
                    ss += template.format(
                        opname,
                        fname,
                        discr,
                        ghosts,
                        order,
                        can_split,
                        tstates,
                        name_size=name_size,
                        field_size=field_size,
                        discr_size=discr_size,
                        ghosts_size=ghosts_size,
                        order_size=order_size,
                        cansplit_size=cansplit_size,
                        tstates_size=tstates_size,
                    )
        else:
            ss += " None"

        title = "ComputationalGraph {} field requirements report ".format(
            self.pretty_name
        )
        return f"\n{framed_str(title=title, msg=ss)}\n"

    def domain_report(self):
        domains = self.get_domains()
        ops = {}

        maxlen = (None, 40, None, 40, None)
        split_sep = (None, ",", None, ",", None)
        newline_prefix = (None, " ", "", " ", None)
        replace = ("", "", "-", "", "")

        for domain, operators in domains.items():
            if domain is None:
                continue
            for op in sorted(operators, key=lambda x: x.pretty_name):
                finputs = ",".join(
                    sorted(
                        f.pretty_name
                        for f in op.iter_input_fields()
                        if f.domain is domain
                    )
                )
                foutputs = ",".join(
                    sorted(
                        f.pretty_name
                        for f in op.iter_output_fields()
                        if f.domain is domain
                    )
                )
                pinputs = ",".join(
                    sorted(p.pretty_name for p in op.input_params.keys())
                )
                poutputs = ",".join(
                    sorted(p.pretty_name for p in op.output_params.keys())
                )
                infields = f"[{finputs}]" if finputs else ""
                outfields = f"[{foutputs}]" if foutputs else ""
                inparams = f"[{pinputs}]" if pinputs else ""
                outparams = f"[{poutputs}]" if poutputs else ""

                inputs = "{}{}{}".format(
                    infields, "x" if infields and inparams else "", inparams
                )
                outputs = "{}{}{}".format(
                    outfields, "x" if outfields and outparams else "", outparams
                )

                if inputs == "":
                    inputs = "no inputs"
                if outputs == "":
                    outputs = "no outputs"
                if op.mpi_params.on_task:
                    opname = op.pretty_name
                    optype = type(op).__name__
                    strdata = (opname, inputs, "->", outputs, optype)

                    op_data = ops.setdefault(domain, [])
                    op_data += multiline_split(
                        strdata, maxlen, split_sep, replace, newline_prefix
                    )

        if None in domains:
            operators = domains[None]
            for op in sorted(operators, key=lambda x: x.pretty_name):
                pinputs = ",".join(
                    sorted(p.pretty_name for p in op.input_params.keys())
                )
                poutputs = ",".join(
                    sorted(p.pretty_name for p in op.output_params.keys())
                )
                inparams = f"[{pinputs}]" if pinputs else ""
                outparams = f"[{poutputs}]" if poutputs else ""

                inputs = f"{inparams}"
                outputs = f"{outparams}"
                if inputs == "":
                    inputs = "no inputs"
                if outputs == "":
                    outputs = "no outputs"
                if op.mpi_params.on_task:
                    opname = op.pretty_name
                    optype = type(op).__name__
                    strdata = (opname, inputs, "->", outputs, optype)

                    op_data = ops.setdefault(None, [])
                    op_data += multiline_split(
                        strdata, maxlen, split_sep, replace, newline_prefix
                    )

        title = f"ComputationalGraph {self.pretty_name} domain and operator report "
        if len(ops) == 0:
            return "\n{}\n".format(framed_str(title=title, msg="Empty"))

        name_size = max(strlen(s[0]) for ss in ops.values() for s in ss)
        in_size = max(strlen(s[1]) for ss in ops.values() for s in ss)
        arrow_size = max(strlen(s[2]) for ss in ops.values() for s in ss)
        out_size = max(strlen(s[3]) for ss in ops.values() for s in ss)
        type_size = max(strlen(s[4]) for ss in ops.values() for s in ss)

        ss = ""
        for domain, dops in ops.items():
            if domain is None:
                continue
            ss += f"\n>{domain.short_description()}"
            ss += "\n   {:<{name_size}}  {:<{in_size}}  {:<{arrow_size}}   {:<{out_size}}    {:<{type_size}}".format(
                "OPERATOR",
                "INPUTS",
                "",
                "OUTPUTS",
                "OPERATOR TYPE",
                name_size=name_size,
                in_size=in_size,
                arrow_size=arrow_size,
                out_size=out_size,
                type_size=type_size,
            )
            for opname, inputs, arrow, outputs, optype in dops:
                ss += "\n   {:<{name_size}}  {:<{in_size}}  {:<{arrow_size}}   {:<{out_size}}    {:<{type_size}}".format(
                    opname,
                    inputs,
                    arrow,
                    outputs,
                    optype,
                    name_size=name_size,
                    in_size=in_size,
                    arrow_size=arrow_size,
                    out_size=out_size,
                    type_size=type_size,
                )
        if None in ops:
            ss += "\n>Domainless operators:"
            ss += "\n   {:<{name_size}}  {:<{in_size}}  {:<{arrow_size}}   {:<{out_size}}    {:<{type_size}}".format(
                "OPERATOR",
                "INPUTS",
                "",
                "OUTPUTS",
                "OPERATOR TYPE",
                name_size=name_size,
                in_size=in_size,
                arrow_size=arrow_size,
                out_size=out_size,
                type_size=type_size,
            )
            for opname, inputs, arrow, outputs, optype in ops[None]:
                ss += "\n   {:<{name_size}}  {:<{in_size}}  {:<{arrow_size}}   {:<{out_size}}    {:<{type_size}}".format(
                    opname,
                    inputs,
                    arrow,
                    outputs,
                    optype,
                    name_size=name_size,
                    in_size=in_size,
                    arrow_size=arrow_size,
                    out_size=out_size,
                    type_size=type_size,
                )

        title = f"ComputationalGraph {self.pretty_name} domain and operator report "
        return f"\n{framed_str(title=title, msg=ss[1:])}\n"

    def topology_report(self):
        ss = ""
        for backend, topologies in self.get_topologies().items():
            ss += f"\n {backend.short_description()}:"
            ss += "\n  *" + "\n  *".join(
                t.short_description() for t in sorted(topologies, key=lambda x: x.id)
            )
        if ss == "":
            ss = " Empty"
        title = f"ComputationalGraph {self.pretty_name} topology report "
        return f"\n{framed_str(title=title, msg=ss[1:])}\n"

    def variable_report(self):
        fields = self.fields

        topologies = {}
        for field in self.fields:
            field_topologies = {}
            for i, node in enumerate(self.nodes):
                if field in node.input_fields:
                    topo = node.input_fields[field]
                    field_topologies.setdefault(topo, []).append(node)
                if field in node.output_fields:
                    topo = node.output_fields[field]
                    field_topologies.setdefault(topo, []).append(node)
            for topo in sorted(
                (_ for _ in field_topologies.keys() if not _ is None),
                key=lambda x: x.tag,
            ):
                pnames = {node.pretty_name for node in field_topologies[topo]}
                pnames = tuple(sorted(pnames))
                nbyline = 4
                nentries = len(pnames) // nbyline
                n0 = len(str(topo.backend.kind).lower())
                n1 = len(str(topo.tag))
                for i in range(nentries):
                    sops = ", ".join(pnames[nbyline * i : nbyline * (i + 1)])
                    if (i != nentries - 1) or (len(pnames) % nbyline != 0):
                        sops += ","
                    if i == 0:
                        entries = (str(topo.backend.kind).lower(), topo.tag, sops)
                    else:
                        entries = ("", "-" * n1, sops)
                    topologies.setdefault(field, []).append(entries)
                if len(pnames) % nbyline != 0:
                    sops = ", ".join(pnames[nbyline * nentries :])
                    if nentries == 0:
                        entries = (str(topo.backend.kind).lower(), topo.tag, sops)
                    else:
                        entries = ("", "-" * n1, sops)
                    topologies.setdefault(field, []).append(entries)

        titles = [[("BACKEND", "TOPOLOGY", "OPERATORS")]]
        vals = tuple(topologies.values()) + tuple(titles)
        backend_size = max(len(s[0]) for ss in vals for s in ss)
        topo_size = max(len(s[1]) for ss in vals for s in ss)
        template = "\n   {:<{backend_size}}   {:<{topo_size}}   {}"
        sizes = {"backend_size": backend_size, "topo_size": topo_size}

        ss = ""
        for field in sorted(self.fields, key=lambda x: x.name):
            ss += f"\n>FIELD {field.name}::{field.pretty_name}"
            ss += template.format(*titles[0][0], **sizes)
            field_topologies = topologies[field]
            for entries in field_topologies:
                ss += template.format(*entries, **sizes)

        if ss == "":
            ss = " Empty"
        title = "ComputationalGraph {} fields report ".format(self.pretty_name)
        ss = f"\n{framed_str(title=title, msg=ss[1:])}\n"
        return ss

    def operator_report(self):
        from hysop.problem import Problem

        maxlen = (None, None, None, 40, None, 40, None)
        split_sep = (None, None, None, ",", None, ",", None)
        newline_prefix = (None, None, None, " ", "", " ", None)
        replace = ("--", "", "", "", "-", "", "")

        def __iterate_operators(n, idprefix=""):
            for i, node in enumerate(n):
                handled_inputs, handled_outputs = (), ()
                finputs, foutputs = [], []
                for f in node.input_tensor_fields:
                    f0 = f.fields[0]
                    t0 = node.input_fields[f0]
                    if all((node.input_fields[fi] is t0) for fi in f.fields):
                        finputs.append(f"{f.pretty_name}.{t0.pretty_tag}")
                        handled_inputs += f.fields
                for f in node.output_tensor_fields:
                    f0 = f.fields[0]
                    t0 = node.output_fields[f0]
                    if all((node.output_fields[fi] is t0) for fi in f.fields):
                        foutputs.append(f"{f.pretty_name}.{t0.pretty_tag}")
                        handled_outputs += f.fields
                finputs += [
                    f"{f.pretty_name}.{t.pretty_tag}"
                    for (f, t) in node.input_fields.items()
                    if f not in handled_inputs and not t is None
                ]
                foutputs += [
                    f"{f.pretty_name}.{t.pretty_tag}"
                    for (f, t) in node.output_fields.items()
                    if f not in handled_outputs and not t is None
                ]
                finputs = ",".join(sorted(finputs))
                foutputs = ",".join(sorted(foutputs))

                pinputs = ",".join(
                    sorted(p.pretty_name for p in node.input_params.keys())
                )
                poutputs = ",".join(
                    sorted(p.pretty_name for p in node.output_params.keys())
                )

                infields = f"[{finputs}]" if finputs else ""
                outfields = f"[{foutputs}]" if foutputs else ""
                inparams = f"[{pinputs}]" if pinputs else ""
                outparams = f"[{poutputs}]" if poutputs else ""

                inputs = "{}{}{}".format(
                    infields, "x" if infields and inparams else "", inparams
                )
                outputs = "{}{}{}".format(
                    outfields, "x" if outfields and outparams else "", outparams
                )
                if inputs == "":
                    inputs = "no inputs"
                if outputs == "":
                    outputs = "no outputs"

                opname = node.pretty_name
                optype = type(node).__name__
                taskdata = node.mpi_params.task_id
                strdata = (
                    idprefix + str(i),
                    str(node.mpi_params.task_id),
                    opname,
                    inputs,
                    "->",
                    outputs,
                    optype,
                )
                yield (strdata, taskdata)
                if isinstance(node, Problem):
                    yield from __iterate_operators(
                        node.nodes, idprefix=strdata[0] + ":"
                    )

        ops, tasks = [], []
        for strdata, taskdata in __iterate_operators(self.nodes):
            tasks.append(taskdata)
            ops += multiline_split(strdata, maxlen, split_sep, replace, newline_prefix)
        hide_task_id = len(set(tasks)) == 1
        title = "ComputationalGraph {} discrete operator report (tasks:{})".format(
            self.pretty_name, tuple(set(tasks))
        )
        if len(ops) == 0:
            return "\n{}\n".format(framed_str(title=title, msg="Empty"))

        isize = max(strlen(s[0]) for s in ops)
        tasksize = max(max(strlen(s[1]) for s in ops), 6)
        name_size = max(strlen(s[2]) for s in ops)
        in_size = max(strlen(s[3]) for s in ops)
        arrow_size = max(strlen(s[4]) for s in ops)
        out_size = max(strlen(s[5]) for s in ops)
        type_size = max(strlen(s[6]) for s in ops)
        if hide_task_id:
            tasksize = 0

        ss = "  {:<{isize}}  {:<{tasksize}}  {:<{name_size}}  {:<{in_size}}  {:<{arrow_size}}   {:<{out_size}}    {:<{type_size}}".format(
            "ID",
            "" if hide_task_id else "TASKID",
            "OPERATOR",
            "INPUTS",
            "",
            "OUTPUTS",
            "OPERATOR TYPE",
            isize=isize,
            tasksize=tasksize,
            name_size=name_size,
            in_size=in_size,
            arrow_size=arrow_size,
            out_size=out_size,
            type_size=type_size,
        )
        for i, task_id, opname, inputs, arrow, outputs, optype in ops:
            ss += "\n  {:>{isize}}  {:<{tasksize}}  {:<{name_size}}  {:<{in_size}}  {:<{arrow_size}}   {:<{out_size}}    {:<{type_size}}".format(
                i,
                "" if hide_task_id else task_id,
                opname,
                inputs,
                arrow,
                outputs,
                optype,
                isize=isize,
                tasksize=tasksize,
                name_size=name_size,
                in_size=in_size,
                arrow_size=arrow_size,
                out_size=out_size,
                type_size=type_size,
            )
        return f"\n{framed_str(title=title, msg=ss[1:])}\n"

    def task_profiler_report(self):
        from hysop.problem import Problem

        if not __PROFILE__:
            return ""

        self._profiler.tasks_summarize()
        rk = self._profiler.get_comm().Get_rank()
        maxlen = (None,) * 11
        split_sep = (None,) * 11
        newline_prefix = (None,) * 11
        replace = ("--",) + ("",) * 10

        def __recurse_nodes(n, iprefix="", pnprefix="", nprefix=""):
            for i, op in enumerate(n):
                yield (
                    iprefix + str(i),
                    pnprefix + op.pretty_name,
                    nprefix + str(op),
                    op,
                )
                if isinstance(op, Problem):
                    yield from __recurse_nodes(
                        op.nodes,
                        iprefix + str(i) + ":",
                        pnprefix + op.pretty_name + ".",
                        nprefix + str(op) + ".",
                    )

        def __line(_i, _pn, _op, _k):
            strdata = (
                _i,
                str(_op.mpi_params.task_id),
                _pn,
                type(_op).__name__,
                _k.split(".")[-1],
            )
            values = tuple(
                ff.format(self._profiler.all_data[_k][_])
                for ff, _ in zip(
                    ("{:.5g}", "{}", "{:.5g}", "{:.5g}", "{:.5g}", "{}"),
                    (2, 1, 3, 4, 5, 0),
                )
            )
            return multiline_split(
                strdata + values, maxlen, split_sep, replace, newline_prefix
            )

        ops, already_printed = [], []
        ops += __line("", "MAIN", self, str(self) + ".apply")
        for i, pn, n, op in __recurse_nodes(self.nodes, nprefix=str(self) + "."):
            for k in self._profiler.all_data.keys():
                if n == ".".join(k.split(".")[:-1]) and not k in already_printed:
                    ops += __line(i, pn, op, k)
                    already_printed.append(k)

        isize = max(strlen(s[0]) for s in ops)
        tasksize = max(max(strlen(s[1]) for s in ops), 6)
        name_size = max(strlen(s[2]) for s in ops)
        type_size = max(strlen(s[3]) for s in ops)
        fn_size = max(strlen(s[4]) for s in ops)
        v_size = (max(strlen(s[5]) for s in ops) for _ in range(5, 11))
        notasks = len({s[1] for s in ops}) == 1
        if notasks:
            tasksize = 0
        ln = {
            "isize": isize,
            "tasksize": tasksize,
            "name_size": name_size,
            "type_size": type_size,
            "fn_size": fn_size,
        }
        ln.update({"vsize" + str(i): s for i, s in enumerate(v_size)})
        ss_fmt = "\n  {:<{isize}}  {:<{tasksize}}  {:<{name_size}}  {:<{type_size}}  {:<{fn_size}}"
        ss_fmt += " | {:<{vsize0}}  {:<{vsize1}} | {:<{vsize2}}  {:<{vsize3}}  {:<{vsize4}}  {:<{vsize5}}"
        ss = ss_fmt.format(
            "ID",
            "" if notasks else "TASKID",
            "OPERATOR",
            "OPERATOR TYPE",
            "FN",
            "SELF",
            "",
            "STATS",
            "",
            "",
            "",
            **ln,
        )
        ss += ss_fmt.format(
            "", "", "", "", "", "total", "nc", "mean", "min", "max", "nproc", **ln
        )
        for _ in ops:
            ss += ss_fmt.format(_[0], "" if notasks else _[1], *tuple(_[2:]), **ln)

        title = "ComputationalGraph {} task profiling report ".format(self.pretty_name)
        return f"\n{framed_str(title=title, msg=ss)}\n"

    def get_domains(self):
        domains = {}
        for node in self.nodes:
            for domain, ops in node.get_domains().items():
                domains.setdefault(domain, set()).update(ops)
        return domains

    @graph_built
    def get_topologies(self):
        topologies = {}
        for node in self.nodes:
            node_topologies = node.get_topologies()
            for backend, topos in node_topologies.items():
                topos = to_set(topos)
                if backend not in topologies:
                    topologies[backend] = set()
                topologies[backend].update(topos)
        return topologies

    @debug
    @not_initialized
    def push_nodes(self, *args):
        from hysop.problem import Problem

        nodes = ()
        for arg in args:
            if arg is None:
                continue
            nodes += to_tuple(arg)

        def _get_mpi_params(n):
            try:
                mpi_params = n.impl_kwds["mpi_params"]
            except (AttributeError, KeyError) as e:
                try:
                    mpi_params = n.mpi_params
                except AttributeError:
                    mpi_params = self.mpi_params
            return mpi_params

        def _hidden_node(node, mpi_params):
            """Add a utility node for handling inter-tasks communications."""
            from hysop.core.mpi.redistribute import RedistributeInter

            if isinstance(node, RedistributeInter):
                return None
            kw = {"variable": None, "source_topo": None, "target_topo": None}
            if not mpi_params.on_task:
                kw.update(
                    {
                        "mpi_params": mpi_params,
                    }
                )
            else:
                kw.update(
                    {
                        "mpi_params": self._last_pushed_node_mpi_params,
                    }
                )
            return RedistributeInter(**kw)

        # nodes iterator with neighbors
        for pcn_node in zip((None,) + nodes[:-1], nodes, nodes[1:] + (None,)):
            prev_node, node, next_node = pcn_node
            # Skip not on-task operators very early
            mpi_params = _get_mpi_params(node)

            if self._last_pushed_node_mpi_params is None:
                self._last_pushed_node_mpi_params = mpi_params
            # Check if there is a task change
            if (
                self._last_pushed_node_mpi_params
                and self._last_pushed_node_mpi_params.task_id != mpi_params.task_id
            ):
                if not (isinstance(node, Problem) and node.search_intertasks_ops):
                    self.nodes.append(_hidden_node(node, mpi_params))
            self._last_pushed_node_mpi_params = _get_mpi_params(node)
            if mpi_params and not mpi_params.on_task:
                continue
            if isinstance(node, ComputationalGraph):
                self.candidate_input_tensors.update(node.candidate_input_tensors)
                self.candidate_output_tensors.update(node.candidate_output_tensors)
                self.nodes.append(node)
            elif isinstance(node, ComputationalGraphNode):
                self.candidate_input_tensors.update(node.input_tensor_fields)
                self.candidate_output_tensors.update(node.output_tensor_fields)
                self.nodes.append(node)
            elif isinstance(node, ComputationalGraphNodeGenerator):
                self.candidate_input_tensors.update(node.candidate_input_tensors)
                self.candidate_output_tensors.update(node.candidate_output_tensors)
                nodes = node.generate()
                assert nodes is not None, node
                self.push_nodes(*nodes)
            else:
                msg = "Given node is not an instance of ComputationalGraphNode (got a {})."
                raise ValueError(msg.format(node.__class__))
        return self

    def available_methods(self):
        avail_methods = {}
        if not self.nodes:
            from hysop.problem import Problem

            msg = "No nodes present in ComputationalGraph {}.".format(self.pretty_name)
            if not isinstance(self, Problem):
                raise RuntimeError(msg)
        for node in self.nodes:
            for k, v in node.available_methods().items():
                v = to_set(v)
                if k in avail_methods:
                    avail_methods[k].update(v)
                else:
                    avail_methods[k] = v
        return avail_methods

    def default_method(self):
        return {}

    @debug
    def initialize(
        self,
        is_root=True,
        topgraph_method=None,
        outputs_are_inputs=False,
        search_intertasks_ops=False,
        **kwds,
    ):
        if self.initialized:
            return
        self.is_root = is_root

        if self.is_root:
            self.pre_initialize(**kwds)

        msg = "ComputationalGraph {} is empty."
        if len(self.nodes) == 0:
            vprint(msg.format(self.pretty_name))

        for node in self.nodes:
            node.pre_initialize(**kwds)

        method = self._setup_method(topgraph_method)
        self.handle_method(method)

        for node in self.nodes:
            node.initialize(
                is_root=False, outputs_are_inputs=False, topgraph_method=method, **kwds
            )

        for node in self.nodes:
            node.post_initialize(**kwds)
        if (self.is_root and __VERBOSE__) or __DEBUG__ or self.__FORCE_REPORTS__:
            print(self.domain_report())

        if self.is_root:
            field_requirements = self.get_and_set_field_requirements()
            field_requirements.build_topologies()

            self._build_graph(
                outputs_are_inputs=outputs_are_inputs,
                current_level=0,
                search_intertasks_ops=search_intertasks_ops,
            )

            # fix for auto generated nodes
            self._fix_auto_generated_nodes()

            # from now on, all nodes contained in self.nodes are ComputationalGraphOperator
            # (ordered for sequential execution)
            self.handle_topologies(
                self.input_topology_states, self.output_topology_states
            )
            self.check()

        self.initialized = True
        if self.is_root:
            self.post_initialize(**kwds)

    def _fix_auto_generated_nodes(self):
        for node in self.nodes:
            if isinstance(node, ComputationalGraph):
                node._fix_auto_generated_nodes()
            if node._field_requirements is None:
                node.get_and_set_field_requirements()
                assert node._field_requirements is not None

    @debug
    def check(self):
        super().check()
        reduced_graph = self.reduced_graph
        for node in self.nodes:
            node.check()

    @debug
    def get_field_requirements(self):
        requirements = super().get_field_requirements()
        if (self.is_root and __VERBOSE__) or __DEBUG__ or self.__FORCE_REPORTS__:
            print(self.node_requirements_report(requirements))
        for node in self.nodes:
            if node.mpi_params is None or node.mpi_params.on_task:
                node_requirements = node.get_and_set_field_requirements()
                requirements.update(node_requirements)
        if (self.is_root and __VERBOSE__) or __DEBUG__ or self.__FORCE_REPORTS__:
            print(self.field_requirements_report(requirements))
        return requirements

    @debug
    def handle_topologies(self, input_topology_states, output_topology_states):
        from hysop.problem import Problem

        # do not call super method
        for node in self.nodes:
            assert isinstance(node, ComputationalGraphOperator) or isinstance(
                node, Problem
            )
            assert node in input_topology_states
            assert node in output_topology_states
            assert node._field_requirements is not None
            if not isinstance(node, Problem):
                node.handle_topologies(
                    input_topology_states[node], output_topology_states[node]
                )
            else:
                node.handle_topologies(
                    node.input_topology_states, node.output_topology_states
                )
            node.input_topology_states = input_topology_states[node]
            node.output_topology_states = output_topology_states[node]
        self.topology_handled = True

    @debug
    def _build_graph(
        self,
        current_level,
        outputs_are_inputs=False,
        search_intertasks_ops=False,
        **kwds,
    ):
        if self.graph_built:
            return

        from hysop.core.graph.graph_builder import GraphBuilder

        builder = GraphBuilder(node=self)
        builder.configure(
            current_level=current_level,
            outputs_are_inputs=outputs_are_inputs,
            search_intertasks_ops=search_intertasks_ops,
            **kwds,
        )
        builder.build_graph()

        input_fields = builder.input_fields
        output_fields = builder.output_fields
        candidate_input_tensors = self.candidate_input_tensors
        candidate_output_tensors = self.candidate_output_tensors

        input_tensor_fields = ()
        output_tensor_fields = ()
        for itfield in candidate_input_tensors:
            if all((f in input_fields) for f in itfield):
                input_tensor_fields += (itfield,)
        for otfield in candidate_output_tensors:
            if all((f in output_fields) for f in otfield):
                output_tensor_fields += (otfield,)

        # keep variables
        self._init_base(
            input_fields=input_fields,
            output_fields=output_fields,
            input_tensor_fields=input_tensor_fields,
            output_tensor_fields=output_tensor_fields,
            input_params=builder.input_params,
            output_params=builder.output_params,
            is_root=self.is_root,
        )

        self.graph = builder.graph
        self.reduced_graph = builder.reduced_graph
        self.sorted_nodes = builder.sorted_nodes
        self.nodes = builder.nodes
        self.input_topology_states = builder.op_input_topology_states
        self.output_topology_states = builder.op_output_topology_states

        self.initial_input_topology_states = builder.input_topology_states
        self.final_output_topology_states = builder.output_topology_states

        self.level = current_level

        self.initialized = True
        self.graph_built = True

        if (self.is_root and __VERBOSE__) or __DEBUG__ or self.__FORCE_REPORTS__:
            print(self.topology_report())
            print(self.variable_report())
            print(self.operator_report())

    def display(self, visu_rank=0, show_buttons=False):
        """
        Display the reduced computational graph.
        """
        from hysop import main_rank

        if (visu_rank is None) or (main_rank != visu_rank):
            return

        net = self.to_pyvis()

        import tempfile

        with tempfile.NamedTemporaryFile(suffix=".html", delete=False) as f:
            net.show(f.name)

    def to_html(self, path, io_rank=0, show_buttons=False):
        """
        Generate an interactive computational graph in an html file.
        """
        from hysop import main_rank

        if (io_rank is None) or (main_rank != io_rank):
            return

        net = self.to_pyvis()
        if net is None:
            return

        html = net.generate_html()
        with open(path, "w+") as f:
            f.write(html)

    @graph_built
    def to_pyvis(self, width=None, height=None, with_custom_nodes=True):
        """
        Convert the graph to a pyvis network for vizualization.
        """
        try:
            import pyvis
            import matplotlib
        except ImportError:
            msg = "\nGraph vizualization requires pyvis and matplotlib, which are not present on your system.\n"
            print(msg)
            return

        width = first_not_None(width, 1920)
        height = first_not_None(height, 1080)

        graph = self.reduced_graph
        network = pyvis.network.Network(directed=True, width=width, height=height)
        known_nodes = set()

        def add_node(node):
            node_id = int(node)
            if node_id not in known_nodes:
                network.add_node(
                    node_id,
                    label=node.label,
                    title=node.title,
                    color=node.color,
                    shape=node.shape(with_custom_nodes),
                )
                known_nodes.add(node_id)

        def add_edge(from_node, to_node):
            from_node_id = int(from_node)
            to_node_id = int(to_node)
            edge = graph[from_node][to_node]
            network.add_edge(
                from_node_id, to_node_id, title=str(edge.get("data", "no edge data"))
            )

        for node in graph:
            add_node(node)
            for out_node in graph[node]:
                add_node(out_node)
                add_edge(node, out_node)

        return network

    @debug
    @graph_built
    def discretize(self):
        if self.discretized:
            return
        deffered_ops = []
        for node in self.nodes:
            if isinstance(node, RedistributeInter):
                deffered_ops.append(node)
                continue
            if not node.discretized:
                node.discretize()
        # Sorting the operators is mandatory for synchronization of
        # redistributesInter (local graphs are differents from a rank to another)
        for node in sorted(deffered_ops, key=lambda _: _.variable.name):
            node.discretize()

        input_discrete_fields = {}
        for field, topo in self.input_fields.items():
            istate = self.initial_input_topology_states[field][1]
            # problem inputs are writeable for initialization
            istate = istate.copy(is_read_only=False)
            dfield = field.discretize(topo, istate)
            input_discrete_fields[field] = dfield

        output_discrete_fields = {}
        for field, topo in self.output_fields.items():
            ostate = self.final_output_topology_states[field][1]
            dfield = field.discretize(topo, ostate)
            output_discrete_fields[field] = dfield

        # build back DiscreteTensorFields from DiscreteScalarFields
        from hysop.fields.discrete_field import DiscreteTensorField

        input_discrete_tensor_fields = {}
        for tfield in self.input_tensor_fields:
            dfields = tfield.new_empty_array()
            for idx, field in tfield.nd_iter():
                dfield = input_discrete_fields[field]
                dfields[idx] = dfield
            tdfield = DiscreteTensorField(field=tfield, dfields=dfields)
            input_discrete_tensor_fields[tfield] = tdfield

        output_discrete_tensor_fields = {}
        for tfield in self.output_tensor_fields:
            dfields = tfield.new_empty_array()
            for idx, field in tfield.nd_iter():
                dfield = output_discrete_fields[field]
                dfields[idx] = dfield
            tdfield = DiscreteTensorField(field=tfield, dfields=dfields)
            output_discrete_tensor_fields[tfield] = tdfield

        discrete_fields = tuple(
            set(input_discrete_fields.values()).union(output_discrete_fields.values())
        )

        discrete_tensor_fields = tuple(
            set(input_discrete_tensor_fields.values()).union(
                output_discrete_tensor_fields.values()
            )
        )

        self.input_discrete_fields = input_discrete_fields
        self.output_discrete_fields = output_discrete_fields
        self.input_discrete_tensor_fields = input_discrete_tensor_fields
        self.output_discrete_tensor_fields = output_discrete_tensor_fields
        self.discrete_fields = discrete_fields
        self.discrete_tensor_fields = discrete_tensor_fields

        self.discretized = True

    @debug
    @discretized
    def get_work_properties(self):
        requests = MultipleOperatorMemoryRequests()

        for node in self.nodes:
            if node not in requests.operators():
                wp = node.get_work_properties()
                requests += node.get_work_properties()
        if __DEBUG__ or (__VERBOSE__ and self.level == 0) or self.__FORCE_REPORTS__:
            srequests = requests.sreport()
            ss = srequests if (srequests != "") else " *no extra work requested*"
            title = "ComputationalGraph {} work properties report ".format(
                self.pretty_name
            )
            vprint(f"\n{framed_str(title=title, msg=ss)}\n")
        return requests

    @debug
    @discretized
    def setup(self, work=None, allow_subbuffers=False):
        if self.ready:
            return
        if work is None:
            work = self.get_work_properties()
            work.allocate(allow_subbuffers=allow_subbuffers)
        for node in self.nodes:
            if not node.ready:
                node.setup(work=work)
        self.ready = True

    def build(self, outputs_are_inputs=True, method=None, allow_subbuffers=False):
        """
        Shortcut for initialize(), discretize(), get_work_properties(), setup()
        for quick graph initialization.
        """
        self.initialize(outputs_are_inputs=outputs_are_inputs, topgraph_method=method)
        assert self.is_root, "Only root graph can be built."
        self.discretize()
        work = self.get_work_properties()
        work.allocate(allow_subbuffers=allow_subbuffers)
        self.setup(work)
        return self

    @debug
    @profile
    @ready
    def apply(self, **kwds):
        for node in self.nodes:
            if not node.to_be_skipped(node, **kwds):
                dprint(f"{node.name}.apply()")
                node.apply(**kwds)

    @debug
    @ready
    def finalize(self, **kwds):
        reduced_graph = self.reduced_graph
        for node in self.nodes:
            if node.ready:
                node.finalize(**kwds)
        self.ready = False

    @classmethod
    def supports_multiple_field_topologies(cls):
        return True

    @classmethod
    def supports_multiple_topologies(cls):
        return True

    @classmethod
    def supports_mpi(cls):
        return True
