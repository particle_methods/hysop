# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABCMeta, abstractmethod
import numpy as np

from hysop.tools.decorators import debug
from hysop.tools.htypes import check_instance
from hysop.core.arrays.all import ArrayBackend, HostArrayBackend, OpenClArrayBackend


class MemoryAllocator(metaclass=ABCMeta):

    @abstractmethod
    def handle_requests(cls, requests):
        pass


class StandardArrayAllocator(MemoryAllocator):

    def __init__(self, array_backend):
        check_instance(self, ArrayBackend)
        self.array_backend = array_backend

    def allocate(self, nbytes):
        npb = self.array_backend
        dtype = np.dtype(np.uint8)
        assert dtype.itemsize == 1
        return npb.empty(shape=(nbytes,), dtype=dtype)

    def handle_requests(self, requests):
        from hysop.core.memory.memory_request import MultipleOperatorMemoryRequests

        if not isinstance(requests, MultipleOperatorMemoryRequests):
            msg = "requests is not an instance of MultipleOperatorMemoryRequests (got a {})."
            raise ValueError(msg.format(requests.__class__))

        cls = self.__class__

        total_bytes = requests.min_bytes_to_allocate(self)
        if total_bytes == 0:
            return
        array = self.allocate(total_bytes)

        op_requests = requests._all_requests_per_allocator[self]
        views = requests._allocated_buffers
        self.build_array_views(array, op_requests, views)

    def build_array_views(self, array, op_requests, views):
        from hysop.core.memory.memory_request import OperatorMemoryRequests

        check_instance(op_requests, dict)
        ptr = array.ctypes.data
        for op, requests in op_requests.items():
            check_instance(requests, dict, values=OperatorMemoryRequests)
            start_idx = 0
            for req_id, req in requests.items():
                align_offset = -ptr % req.alignment
                start_idx += align_offset
                end_idx = start_idx + req.data_bytes()

                view = array[start_idx:end_idx].view(req.dtype).reshape(req.shape)
                if op not in views:
                    views[op] = {}
                views[op][req_id] = view

                start_idx = end_idx
                ptr += align_offset + req.data_bytes()
            assert end_idx <= requests.min_bytes_to_allocate(self)


NumpyMemoryRequestAllocator = StandardArrayAllocator(array_backend=HostArrayBackend)


def OpenClMemoryRequestAllocator(cl_env):
    return StandardArrayAllocator(array_backend=cl_env.array_backend())


if __name__ == "__main__":
    m0 = NumpyMemRequest(count=1, dtype=np.int32)
    m1 = NumpyMemRequest(shape=(2,), dtype=np.int32)
    m2 = NumpyMemRequest(shape=(2, 2), dtype=np.int32)
    m3 = NumpyMemRequest(
        shape=(
            2,
            2,
            2,
        ),
        dtype=np.int32,
        alignment=64,
    )

    m4 = NumpyMemRequest(count=3, dtype=np.int32)
    m5 = NumpyMemRequest(shape=(4,), dtype=np.int32)
    m6 = NumpyMemRequest(shape=(8,), dtype=np.int32)

    opm0 = OperatorMemoryRequests("opA")
    opm0.add_mem_request("reqA0", m0)
    opm0.add_mem_request("reqA1", m1)
    opm0.add_mem_request("reqA2", m2)
    opm0.add_mem_request("reqA3", m3)

    opm1 = OperatorMemoryRequests("opB")
    opm1.add_mem_request("reqB0", m4)
    opm1.add_mem_request("reqB1", m5)
    opm1.add_mem_request("reqB2", m6)

    all_reqs = MultipleOperatorMemoryRequests()
    all_reqs.add_mem_requests(opm0)
    all_reqs.add_mem_requests(opm1)

    all_reqs.allocate()

    m0.data[...] = 1
    m1.data[...] = 2
    m2.data[...] = 3
    m3.data[...] = 4

    print(m0.data)
    print(m1.data)
    print(m2.data)
    print(m3.data)
    print()
    print(m4.data)
    print(m5.data)
    print(m6.data)
    print()
    print(all_reqs.buffers[NumpyMemoryAllocator][0].dtype)
    print(all_reqs.buffers[NumpyMemoryAllocator][0].shape)
    print(all_reqs.buffers[NumpyMemoryAllocator][0].view(dtype=np.int32))

    assert m3.data.ctypes.data % 64 == 0
