# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abc import ABCMeta, abstractmethod
from hysop import dprint
from hysop.tools.decorators import debug
from hysop.tools.htypes import first_not_None
from hysop.core.graph.computational_node import ComputationalGraphNode
from hysop.core.mpi.redistribute import RedistributeInter


class ComputationalGraphNodeGenerator(metaclass=ABCMeta):
    """
    A class that can generate multiple hysop.core.graph.ComputationalGraphNode.
    """

    @debug
    def __new__(
        cls,
        candidate_input_tensors,
        candidate_output_tensors,
        name=None,
        pretty_name=None,
        **kwds,
    ):
        return super().__new__(cls, **kwds)

    @debug
    def __init__(
        self,
        candidate_input_tensors,
        candidate_output_tensors,
        name=None,
        pretty_name=None,
        **kwds,
    ):
        super().__init__(**kwds)
        candidate_input_tensors = first_not_None(candidate_input_tensors, ())
        candidate_output_tensors = first_not_None(candidate_output_tensors, ())

        self.name = name or self.__class__.__name__
        self.pretty_name = first_not_None(pretty_name, self.name)
        self.nodes = []
        self.generated = False
        self.candidate_input_tensors = set(
            filter(lambda x: x.is_tensor, candidate_input_tensors)
        )
        self.candidate_output_tensors = set(
            filter(lambda x: x.is_tensor, candidate_output_tensors)
        )

    @abstractmethod
    def _generate(self, **kargs):
        """
        Method that should return a list of generated nodes.
        """
        pass

    def _pre_generate(self, **kargs):
        """
        Optional method called before _generate.
        """
        pass

    def _post_generate(self, **kargs):
        """
        Optional method called after _generate.
        """
        pass

    @debug
    def generate(self, **kargs):
        """
        Recursively generate nodes.
        Do not override.
        """
        if not self.generated:
            self._pre_generate(**kargs)

            assert len(self.nodes) == 0
            try:
                for op in self._generate(**kargs):
                    if op is None:
                        continue
                    elif isinstance(op, RedistributeInter):
                        nodes = (op,)
                    elif isinstance(op, ComputationalGraphNode):
                        nodes = (op,)
                        self.candidate_input_tensors.update(op.input_tensor_fields)
                        self.candidate_output_tensors.update(op.output_tensor_fields)
                    elif isinstance(op, ComputationalGraphNodeGenerator):
                        nodes = op.generate(**kargs)
                        if nodes is None:
                            msg = "FATAL ERROR: {}::{}.generate() returned None, "
                            msg += "this is an implementation bug."
                            msg = msg.format(type(op), op.name)
                            raise RuntimeError(msg)
                        self.candidate_input_tensors.update(op.candidate_input_tensors)
                        self.candidate_output_tensors.update(
                            op.candidate_output_tensors
                        )
                    else:
                        msg = f"Unknown node type {op.__class__}."
                        raise TypeError(msg)
                    self.nodes += nodes
            except:
                msg = f"\nFailed to call generate() in class {self.__class__}.\n"
                print(msg)
                raise

            self.candidate_input_tensors = set(
                filter(lambda x: x.is_tensor, self.candidate_input_tensors)
            )
            self.candidate_output_tensors = set(
                filter(lambda x: x.is_tensor, self.candidate_output_tensors)
            )

            self._post_generate(**kargs)
            self.generated = True
        return self.nodes

    def to_graph(self, name=None, **kwds):
        """Generates and convert an computational node generator to a graph."""
        msg = f"Generating graph from computational node generator {name}."
        dprint(msg)

        from hysop.core.graph.computational_graph import ComputationalGraph

        name = name or f"{self.name}_graph"
        graph = ComputationalGraph(name=name, **kwds)
        graph.push_nodes(self)
        return graph

    def build(self, name=None, outputs_are_inputs=False, **kwds):
        """Convert a computational node generator to a graph and prepares it for apply."""
        return self.to_graph(name=name).build(
            outputs_are_inputs=outputs_are_inputs, **kwds
        )
