# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import functools
import shutil
import operator
import os
import warnings
import shutil
import tarfile
import uuid
import numpy as np
from hysop.tools.htypes import check_instance, first_not_None, to_tuple, to_list
from hysop.tools.units import bytes2str, time2str
from hysop.tools.io_utils import IOParams
from hysop.tools.numerics import default_invalid_value
from hysop.tools.string_utils import vprint_banner, vprint
from hysop.core.mpi import Wtime
from hysop.domain.box import Box
from hysop.parameters import ScalarParameter, TensorParameter, BufferParameter
from hysop.fields.cartesian_discrete_field import CartesianDiscreteScalarField


class CheckpointHandler:
    def __init__(
        self,
        load_checkpoint_path,
        save_checkpoint_path,
        compression_method,
        compression_level,
        io_params,
        relax_constraints,
    ):
        check_instance(load_checkpoint_path, str, allow_none=True)
        check_instance(save_checkpoint_path, str, allow_none=True)
        check_instance(compression_method, str, allow_none=True)
        check_instance(compression_level, int, allow_none=True)
        check_instance(io_params, IOParams, allow_none=True)
        check_instance(relax_constraints, bool)

        if compression_method is not None:
            from numcodecs import blosc

            available_compressors = blosc.list_compressors()
            if compression_method not in available_compressors:
                msg = "User specified compression method '{}' which is not supported by blosc. Available compressors are {}."
                raise RuntimeError(
                    msg.format(compression_method, ", ".join(available_compressors))
                )
        if compression_level is not None:
            if (compression_level < 0) or (compression_level > 9):
                msg = "User specified compression level {} that is not in valid range [0,9]."
                raise RuntimeError(msg.format(compression_level))

        self._load_checkpoint_path = load_checkpoint_path
        self._save_checkpoint_path = save_checkpoint_path
        self._compression_method = compression_method
        self._compression_level = compression_level
        self._io_params = io_params
        self._relax_constraints = relax_constraints

        self._checkpoint_template = None
        self._checkpoint_compressor = None

    @property
    def load_checkpoint_path(self):
        return self._load_checkpoint_path

    @property
    def save_checkpoint_path(self):
        return self._save_checkpoint_path

    @property
    def compression_method(self):
        return self._compression_method

    @property
    def compression_level(self):
        return self._compression_level

    @property
    def io_params(self):
        return self._io_params

    @property
    def relax_constraints(self):
        return self._relax_constraints

    def get_mpio_parameters(self, mpi_params):
        io_params = self.io_params
        comm = mpi_params.comm
        io_leader = io_params.io_leader
        is_io_leader = io_leader == mpi_params.rank
        return (io_params, mpi_params, comm, io_leader, is_io_leader)

    def is_io_leader(self, mpi_params):
        return self.io_params.io_leader == mpi_params.rank

    def finalize(self, mpi_params):
        (io_params, mpi_params, comm, io_leader, is_io_leader) = (
            self.get_mpio_parameters(mpi_params)
        )
        comm.Barrier()
        if (
            (self._checkpoint_template is not None)
            and os.path.exists(self._checkpoint_template)
            and self.is_io_leader(mpi_params)
        ):
            try:
                shutil.rmtree(self._checkpoint_template)
            except OSError:
                pass
        comm.Barrier()
        self._checkpoint_template = None
        self._checkpoint_compressor = None

    def load_checkpoint(self, problem, simulation):
        from hysop.problem import Problem
        from hysop.simulation import Simulation

        check_instance(problem, Problem)
        check_instance(simulation, Simulation)

        load_checkpoint_path = self.load_checkpoint_path
        if load_checkpoint_path is None:
            return

        vprint(
            "\n>Loading {}problem checkpoint from '{}'...".format(
                "relaxed" if self.relax_constraints else "", load_checkpoint_path
            )
        )
        if not os.path.exists(load_checkpoint_path):
            msg = "Failed to load checkpoint '{}' because the file does not exist."
            raise RuntimeError(msg.format(load_checkpoint_path))
        if self.io_params is None:
            msg = "Load checkpoint has been set to '{}' but checkpoint_io_params has not been specified."
            raise RuntimeError(msg.format(load_checkpoint_path))

        (io_params, mpi_params, comm, io_leader, is_io_leader) = (
            self.get_mpio_parameters(problem.mpi_params)
        )
        start = Wtime()

        # extract checkpoint to directory if required
        if os.path.isfile(load_checkpoint_path):
            if load_checkpoint_path.endswith(".tar"):
                if is_io_leader:
                    load_checkpoint_dir = os.path.join(
                        os.path.dirname(load_checkpoint_path),
                        os.path.basename(load_checkpoint_path).replace(".tar", ""),
                    )
                    while os.path.exists(load_checkpoint_dir):
                        # ok, use another directory name to avoid dataloss...
                        load_checkpoint_dir = os.path.join(
                            os.path.dirname(load_checkpoint_path), f"{uuid.uuid4().hex}"
                        )
                    with tarfile.open(load_checkpoint_path, mode="r") as tf:
                        tf.extractall(path=load_checkpoint_dir)
                else:
                    load_checkpoint_dir = None
                load_checkpoint_dir = comm.bcast(load_checkpoint_dir, root=io_leader)
                should_remove_dir = True
            else:
                msg = "Can only load checkpoint with tar extension, got {}."
                raise NotImplementedError(msg.format(load_checkpoint_path))
        elif os.path.isdir(load_checkpoint_path):
            load_checkpoint_dir = load_checkpoint_path
            should_remove_dir = False
        else:
            raise RuntimeError

        # import checkpoint data
        self._import_checkpoint(problem, simulation, load_checkpoint_dir)

        comm.Barrier()
        if is_io_leader and should_remove_dir:
            shutil.rmtree(load_checkpoint_dir)
        comm.Barrier()

        ellapsed = Wtime() - start
        msg = " > Successfully imported checkpoint in {}."
        vprint(msg.format(time2str(ellapsed)))

    def should_dump(self, simulation):
        io_params = self.io_params
        if self.save_checkpoint_path is None:
            return False
        if io_params is None:
            return False
        return io_params.should_dump(simulation)

    # Checkpoint is first exported as a directory containing a hierarchy of arrays (field and parameters data + metadata)
    # This folder is than tarred (without any form of compression) so that a checkpoint consists in a single movable file.
    # Data is already compressed during data export by the zarr module, using the blosc compressor (snappy, clevel=9).

    def save_checkpoint(self, problem, simulation):
        save_checkpoint_path = self.save_checkpoint_path
        if self.save_checkpoint_path is None:
            return

        if self.io_params is None:
            msg = "Load checkpoint has been set to '{}' but checkpoint io_params has not been specified."
            raise RuntimeError(msg.format(save_checkpoint_path))

        vprint(f">Exporting problem checkpoint to '{save_checkpoint_path}':")
        if not save_checkpoint_path.endswith(".tar"):
            msg = "Can only export checkpoint with tar extension, got {}."
            raise NotImplementedError(msg.format(save_checkpoint_path))
        save_checkpoint_tar = save_checkpoint_path

        (io_params, mpi_params, comm, io_leader, is_io_leader) = (
            self.get_mpio_parameters(problem.mpi_params)
        )
        start = Wtime()

        # create a backup of last checkpoint just in case things go wrong
        comm.Barrier()
        if is_io_leader and os.path.exists(save_checkpoint_tar):
            backup_checkpoint_tar = save_checkpoint_tar + ".bak"
            if os.path.exists(backup_checkpoint_tar):
                os.remove(backup_checkpoint_tar)
            os.rename(save_checkpoint_tar, backup_checkpoint_tar)
        else:
            backup_checkpoint_tar = None
        comm.Barrier()

        # determine checkpoint dump directory
        if is_io_leader:
            save_checkpoint_dir = os.path.join(
                os.path.dirname(save_checkpoint_tar),
                os.path.basename(save_checkpoint_tar).replace(".tar", ""),
            )
            while os.path.exists(save_checkpoint_dir):
                # ok, use another directory name to avoid dataloss...
                save_checkpoint_dir = os.path.join(
                    os.path.dirname(save_checkpoint_tar), f"{uuid.uuid4().hex}"
                )
        else:
            save_checkpoint_dir = None
        save_checkpoint_dir = mpi_params.comm.bcast(save_checkpoint_dir, root=io_leader)

        # try to create the checkpoint directory, this is a collective MPI operation
        try:
            success, reason, nbytes = self._export_checkpoint(
                problem, simulation, save_checkpoint_dir
            )
        except Exception as e:
            raise
            success = False
            reason = str(e)
        success = comm.allreduce(int(success)) == comm.size

        # Compress checkpoint directory to tar (easier to copy/move between clusters)
        # Note that there is no effective compression here, zarr already compressed field/param data
        if success and is_io_leader and os.path.isdir(save_checkpoint_dir):
            try:
                with tarfile.open(save_checkpoint_tar, "w") as tf:
                    for root, dirs, files in os.walk(save_checkpoint_dir):
                        for f in files:
                            fpath = os.path.join(root, f)
                            tf.add(
                                fpath,
                                arcname=fpath.replace(
                                    save_checkpoint_dir + os.path.sep, ""
                                ),
                            )

                if os.path.isfile(save_checkpoint_tar):
                    shutil.rmtree(save_checkpoint_dir)
                else:
                    raise RuntimeError("Could not tar checkpoint datadir.")

                ellapsed = Wtime() - start
                effective_nbytes = os.path.getsize(save_checkpoint_tar)
                compression_ratio = max(1.0, float(nbytes) / effective_nbytes)

                msg = " > Successfully exported checkpoint in {} with a compression ratio of {:.1f} ({})."
                vprint(
                    msg.format(
                        time2str(ellapsed),
                        compression_ratio,
                        bytes2str(effective_nbytes),
                    )
                )
            except Exception as e:
                success = False
                reason = str(e)
        success = comm.allreduce(int(success)) == comm.size

        if success:
            comm.Barrier()
            if (
                (backup_checkpoint_tar is not None)
                and os.path.isfile(backup_checkpoint_tar)
                and is_io_leader
            ):
                os.remove(backup_checkpoint_tar)
            comm.Barrier()
            return

        from hysop.tools.warning import HysopDumpWarning

        msg = f"Failed to export checkpoint because: {reason}."
        warnings.warn(msg, HysopDumpWarning)

        # Something went wrong (I/O error or other) so we rollback to previous checkpoint (if there is one)
        vprint(
            " | An error occured during checkpoint creation, rolling back to previous checkpoint..."
        )
        comm.Barrier()
        if is_io_leader:
            if os.path.exists(save_checkpoint_dir):
                shutil.rmtree(save_checkpoint_dir)
            if os.path.exists(save_checkpoint_tar):
                os.remove(save_checkpoint_tar)
            if (backup_checkpoint_tar is not None) and os.path.exists(
                backup_checkpoint_tar
            ):
                os.rename(backup_checkpoint_tar, save_checkpoint_tar)
        comm.Barrier()

    def create_checkpoint_template(self, problem, simulation):
        # Create groups of arrays on disk (only hierarchy and array metadata is stored in the template)
        # /!\ ZipStores are not safe from multiple processes so we use a DirectoryStore
        #      that can then be tarred manually by io_leader.

        save_checkpoint_path = self.save_checkpoint_path
        if save_checkpoint_path is None:
            return

        if not save_checkpoint_path.endswith(".tar"):
            msg = "Can only export checkpoint with tar extension, got {}."
            raise NotImplementedError(msg.format(save_checkpoint_path))

        (io_params, mpi_params, comm, io_leader, is_io_leader) = (
            self.get_mpio_parameters(problem.mpi_params)
        )

        # determine an empty directory for the template
        if is_io_leader:
            checkpoint_template = os.path.join(
                os.path.dirname(save_checkpoint_path),
                os.path.basename(save_checkpoint_path).replace(".tar", ".template"),
            )
            while os.path.exists(checkpoint_template):
                # ok, use another directory name to avoid dataloss...
                checkpoint_template = os.path.join(
                    os.path.dirname(save_checkpoint_path), f"{uuid.uuid4().hex}"
                )
        else:
            checkpoint_template = None
        checkpoint_template = comm.bcast(checkpoint_template, root=io_leader)
        self._checkpoint_template = checkpoint_template

        vprint(f"\n>Creating checkpoint template as '{checkpoint_template}'...")
        import zarr
        from numcodecs import blosc, Blosc

        blosc.use_threads = (
            mpi_params.size == 1
        )  # disable threads for multiple processes (can deadlock)

        # array data compressor
        self._compression_method = first_not_None(self._compression_method, "zstd")
        self._compression_level = first_not_None(self._compression_level, 6)
        compressor = Blosc(
            cname=self._compression_method,
            clevel=self._compression_level,
            shuffle=Blosc.SHUFFLE,
        )
        self._checkpoint_compressor = compressor

        # io_leader creates a directory layout on (hopefully) shared filesystem
        comm.Barrier()
        if is_io_leader:
            if os.path.exists(checkpoint_template):
                shutil.rmtree(checkpoint_template)
            store = zarr.DirectoryStore(path=checkpoint_template)
            root = zarr.open_group(store=store, mode="w", path="data")
            params_group = root.create_group("params")
            fields_group = root.create_group("fields")
            simu_group = root.create_group("simulation")
            operators_group = root.create_group("operators")
        else:
            store = None
            root = None
            params_group = None
            fields_group = None
            simu_group = None
        comm.Barrier()

        # count number of total data bytes without compression
        nbytes = 0
        fmt_key = self._format_zarr_key

        # operators
        for op in problem.nodes:
            if not op.checkpoint_required():
                continue
            key = fmt_key(op.checkpoint_datagroup_key())
            if key in operators_group:
                msg = (
                    "Operator checkpoint key '{}' has already been taken by another operator, "
                    "consider overriding {}.checkpoint_datagroup_key() or disable checkpointing "
                    "for one of the two operators by tweeking the checkpoint_required() method."
                )
                raise RuntimeError(msg.format(key, op.__class__.__name__))
            operators_group.create_group(key)

        # Generate parameter arrays
        # Here we expect that each process store parameters that are in sync
        # For each parameter we assume that the same values are broadcast to all processes
        # even if is not enforced by the library (should cover most current use cases...)
        for param in sorted(problem.parameters, key=operator.attrgetter("name")):
            if not is_io_leader:
                continue
            if isinstance(param, (ScalarParameter, TensorParameter, BufferParameter)):
                # all those parameters store their data in a numpy ndarray so we're good
                assert isinstance(param._value, np.ndarray), type(param._value)
                value = param._value
                array = params_group.create_dataset(
                    name=fmt_key(param.name),
                    overwrite=False,
                    data=None,
                    synchronizer=None,
                    compressor=compressor,
                    shape=value.shape,
                    chunks=None,
                    dtype=value.dtype,
                    fill_value=default_invalid_value(value.dtype),
                )
                array.attrs["kind"] = param.__class__.__name__
                nbytes += value.nbytes
            else:
                msg = f"Cannot export parameter of type {param.__class__.__name__}."
                raise NotImplementedError(msg)

        # Generate discrete field arrays
        # Here we assume that each process has a non-empty chunk of data
        for field in sorted(problem.fields, key=operator.attrgetter("name")):

            # we do not care about fields discretized only on temporary fields
            if all(df.is_tmp for df in field.discrete_fields.values()):
                continue

            if is_io_leader:
                field_group = fields_group.create_group(fmt_key(field.name))
            else:
                field_group = None

            dim = field.dim
            domain = field.domain._domain

            if isinstance(domain, Box):
                if field_group is not None:
                    field_group.attrs["domain"] = "Box"
                    field_group.attrs["dim"] = domain.dim
                    field_group.attrs["origin"] = to_tuple(domain.origin)
                    field_group.attrs["end"] = to_tuple(domain.end)
                    field_group.attrs["length"] = to_tuple(domain.length)
            else:
                # for now we just handle Boxed domains
                raise NotImplementedError

            for k, topo in enumerate(
                sorted(field.discrete_fields, key=operator.attrgetter("full_tag"))
            ):
                dfield = field.discrete_fields[topo]
                mesh = topo.mesh._mesh

                # we do not care about temporary fields
                if dfield.is_tmp:
                    continue

                if not isinstance(dfield, CartesianDiscreteScalarField):
                    # for now we just handle CartesianDiscreteScalarFields.
                    raise NotImplementedError

                global_resolution = topo.global_resolution  # logical grid size
                grid_resolution = topo.grid_resolution  # effective grid size
                ghosts = topo.ghosts

                # get local resolutions exluding ghosts
                compute_resolutions = comm.gather(
                    to_tuple(mesh.compute_resolution), root=io_leader
                )

                # is the current process handling a right boundary data block on a distributed axe ?
                is_at_right_boundary = (
                    mesh.is_at_right_boundary * (mesh.proc_shape > 1)
                ).any()
                is_at_right_boundary = np.asarray(
                    comm.gather(is_at_right_boundary, root=io_leader)
                )

                if not is_io_leader:
                    continue

                # io_leader can now determine wether the cartesian discretization is uniformly distributed
                # between processes or not
                inner_compute_resolutions = tuple(
                    compute_resolutions[i]
                    for i in range(len(compute_resolutions))
                    if not is_at_right_boundary[i]
                )
                grid_is_uniformly_distributed = all(
                    res == inner_compute_resolutions[0]
                    for res in inner_compute_resolutions
                )

                if grid_is_uniformly_distributed:
                    # We divide the array in 'compute_resolution' chunks, no sychronization is required.
                    # Here there is no need to use the process locker to write this array data.
                    # Each process writes its own independent block of data of size 'compute_resolution'.
                    should_sync = False
                    chunks = inner_compute_resolutions[0]
                else:
                    # We divide the array in >=1MB chunks (chunks are given in terms of elements)
                    # Array chunks may overlap different processes so we need interprocess sychronization (slow)
                    assert comm.size > 1
                    should_sync = True
                    if dim == 1:
                        chunks = 1024 * 1024  # at least 1MB / chunk
                    elif dim == 2:
                        chunks = (1024, 1024)  # at least 1MB / chunk
                    elif dim == 3:
                        chunks = (64, 128, 128)  # at least 1MB / chunk
                    else:
                        raise NotImplementedError(dim)

                if should_sync:
                    raise NotImplementedError

                # Create array (no memory is allocated here, even on disk because data blocks are empty)
                dtype = dfield.dtype
                shape = grid_resolution

                # We scale the keys up to 100 topologies, which seams to be a pretty decent upper limit
                # on a per field basis.
                array = field_group.create_dataset(
                    name=f"topo_{k:02d}",
                    overwrite=False,
                    data=None,
                    synchronizer=None,
                    compressor=compressor,
                    shape=shape,
                    chunks=chunks,
                    dtype=dtype,
                    fill_value=default_invalid_value(dtype),
                )
                array.attrs["should_sync"] = should_sync

                # We cannot rely on discrete mesh name because of topology names
                # so we save some field metadata to be able to differentiate between
                # discrete fields with the exact same grid resolution.
                # proc_shape and name are used in last resort to differentiate discrete fields.
                array.attrs["lboundaries"] = to_tuple(map(str, mesh.global_lboundaries))
                array.attrs["rboundaries"] = to_tuple(map(str, mesh.global_rboundaries))
                array.attrs["ghosts"] = to_tuple(mesh.ghosts)
                array.attrs["proc_shape"] = to_tuple(mesh.proc_shape)
                array.attrs["name"] = dfield.name

                nbytes += np.prod(shape, dtype=np.int64) * dtype.itemsize

        if root is not None:
            root.attrs["nbytes"] = int(nbytes)
            msg = (
                ">Maximum checkpoint size will be {}, without compression and metadata."
            )
            vprint(root.tree())
            vprint(msg.format(bytes2str(nbytes)))

        # some zarr store formats require a final close to flush data
        try:
            if root is not None:
                root.close()
        except AttributeError:
            pass

    def _export_checkpoint(self, problem, simulation, save_checkpoint_dir):
        # Given a template, fill field and parameters data from all processes.
        # returns (bool, msg) where bool is True on success
        (io_params, mpi_params, comm, io_leader, is_io_leader) = (
            self.get_mpio_parameters(problem.mpi_params)
        )

        # checkpoint template may have been deleted by user during simulation
        if (self._checkpoint_template is None) or (
            not os.path.isdir(self._checkpoint_template)
        ):
            self.create_checkpoint_template(problem, simulation)
        checkpoint_template = self._checkpoint_template
        checkpoint_compressor = self._checkpoint_compressor

        comm.Barrier()
        if is_io_leader:
            if os.path.exists(save_checkpoint_dir):
                shutil.rmtree(save_checkpoint_dir)
            shutil.copytree(checkpoint_template, save_checkpoint_dir)
        comm.Barrier()

        if not os.path.isdir(save_checkpoint_dir):
            msg = f"Could not find checkpoint directory '{save_checkpoint_dir}'. Are you using a network file system ?"
            raise RuntimeError(msg)

        # Every process now loads the same dataset template
        import zarr

        try:
            store = zarr.DirectoryStore(save_checkpoint_dir)
            root = zarr.open_group(
                store=store, mode="r+", synchronizer=None, path="data"
            )
            fields_group = root["fields"]
            params_group = root["params"]
            simu_group = root["simulation"]
            operators_group = root["operators"]
            nbytes = root.attrs["nbytes"]
        except:
            msg = "A fatal error occured during checkpoint export, checkpoint template may be illformed."
            vprint(msg)
            vprint()
            raise

        fmt_key = self._format_zarr_key

        # Export simulation data
        if is_io_leader:
            simulation.save_checkpoint(
                simu_group, mpi_params, io_params, checkpoint_compressor
            )

        # Export operator data
        for op in problem.nodes:
            if not op.checkpoint_required():
                continue
            key = fmt_key(op.checkpoint_datagroup_key())
            operator_group = operators_group[key]
            op.save_checkpoint(
                operator_group, mpi_params, io_params, checkpoint_compressor
            )

        # Currently there is no distributed parameter capabilities so io_leader has to dump all parameters
        if is_io_leader:
            msg = " | dumping parameters..."
            vprint(msg)
            for param in sorted(problem.parameters, key=operator.attrgetter("name")):
                if isinstance(
                    param, (ScalarParameter, TensorParameter, BufferParameter)
                ):
                    array = params_group[fmt_key(param.name)]
                    assert array.attrs["kind"] == param.__class__.__name__
                    assert array.dtype == param._value.dtype
                    assert array.shape == param._value.shape
                    array[...] = param._value
                else:
                    msg = f"Cannot dump parameter of type {param.__class__.__name__}."
                    raise NotImplementedError(msg)

        # Unlike parameter all processes participate for fields
        for field in sorted(problem.fields, key=operator.attrgetter("name")):

            # we do not care about fields discretized only on temporary fields
            if all(df.is_tmp for df in field.discrete_fields.values()):
                continue

            msg = f" | dumping field {field.pretty_name}..."
            vprint(msg)

            field_group = fields_group[fmt_key(field.name)]
            for k, topo in enumerate(
                sorted(field.discrete_fields, key=operator.attrgetter("full_tag"))
            ):
                dfield = field.discrete_fields[topo]
                mesh = topo.mesh._mesh

                # we do not care about temporary fields
                if dfield.is_tmp:
                    continue

                dataset = f"topo_{k:02d}"  # key has to match template
                array = field_group[dataset]
                should_sync = array.attrs["should_sync"]

                assert dfield.nb_components == 1
                assert (array.shape == mesh.grid_resolution).all(), (
                    array.shape,
                    mesh.grid_resolution,
                )
                assert array.dtype == dfield.dtype, (array.dtype, dfield.dtype)

                if should_sync:
                    # Should not be required untill we allow non-uniform discretizations
                    global_start = mesh.global_start
                    global_stop = mesh.global_stop
                    raise NotImplementedError(
                        "Synchronized multiprocess write has not been implemented yet."
                    )
                else:
                    assert (mesh.compute_resolution == array.chunks).all() or (
                        mesh.is_at_right_boundary * (mesh.proc_shape > 1)
                    ).any()
                    local_data = dfield.compute_data[0].get().handle
                    global_slices = mesh.global_compute_slices
                    array[global_slices] = (
                        local_data  # ok, every process writes to an independent data blocks
                    )

        # Some zarr store formats require a final close to flush data
        try:
            root.close()
        except AttributeError:
            pass

        return True, None, nbytes

    # On data import, there is no need to synchronize read-only arrays
    # so we are good with multiple processes reading overlapping data blocks
    def _import_checkpoint(self, problem, simulation, load_checkpoint_dir):

        (io_params, mpi_params, comm, io_leader, is_io_leader) = (
            self.get_mpio_parameters(problem.mpi_params)
        )
        mpi_params.comm.Barrier()

        if not os.path.isdir(load_checkpoint_dir):
            msg = f"Could not find checkpoint directory '{load_checkpoint_dir}'. Are you using a network file system ?"
            raise RuntimeError(msg)

        import zarr

        store = zarr.DirectoryStore(load_checkpoint_dir)
        try:
            root = zarr.open_group(
                store=store, mode="r", synchronizer=None, path="data"
            )
            params_group = root["params"]
            fields_group = root["fields"]
            simu_group = root["simulation"]
            operators_group = root["operators"]
        except:
            msg = "A fatal error occured during checkpoint import, checkpoint data may be illformed."
            vprint(msg)
            vprint()
            raise

        # Define helper functions
        relax_constraints = self.relax_constraints
        raise_error = self._raise_error
        if relax_constraints:
            raise_warning = self._raise_warning
        else:
            raise_warning = self._raise_error
        load_array_data = functools.partial(
            self._load_array_data, on_mismatch=raise_warning
        )
        fmt_key = self._format_zarr_key

        # Import simulation data after parameters are up to date
        msg = " | importing simulation..."
        vprint(msg)
        simulation.load_checkpoint(simu_group, mpi_params, io_params, relax_constraints)

        # Import operator data
        for op in problem.nodes:
            if not op.checkpoint_required():
                continue
            key = fmt_key(op.checkpoint_datagroup_key())
            if key not in operators_group:
                msg = "Could not find operator key '{}' in checkpoint."
                raise_warning(msg.format(key))
                continue
            operator_group = operators_group[key]
            op.load_checkpoint(operator_group, mpi_params, io_params, relax_constraints)

        # Import parameters, hopefully parameter names match the ones in the checkpoint
        msg = " | importing parameters..."
        vprint(msg)
        for param in sorted(problem.parameters, key=operator.attrgetter("name")):
            key = fmt_key(param.name)

            if key not in params_group:
                msg = "Checkpoint directory '{}' does not contain any data regarding to parameter {}"
                msg = msg.format(load_checkpoint_dir, param.name)
                raise_error(msg)

            array = params_group[key]

            if array.attrs["kind"] != param.__class__.__name__:
                msg = "Parameter kind do not match with checkpointed parameter {}, loaded kind {} but expected {}."
                msg = msg.format(
                    param.name, array.attrs["kind"], param.__class__.__name__
                )
                raise_error(msg)

            if isinstance(param, (ScalarParameter, TensorParameter, BufferParameter)):
                value = param._value

                if array.shape != value.shape:
                    msg = "Parameter shape does not match with checkpointed parameter {}, loaded shape {} but expected {}."
                    msg = msg.format(param.name, array.shape, value.shape)
                    raise_error(msg)

                if array.dtype != value.dtype:
                    msg = "Parameter datatype does not match with checkpointed parameter {}, loaded dtype {} but expected {}."
                    msg = msg.format(param.name, array.dtype, value.dtype)
                    raise_warning(msg)

                value[...] = array[...]
            else:
                msg = f"Cannot import parameter of type {param.__class__.__name__}."
                raise NotImplementedError(msg)

        # Import discrete fields, this is a bit more tricky because topologies or simply topology
        # names can change. Moreover there is currently no waranty that the same operator graph is
        # generated for the exact same problem configuration each time. We just emit user warnings
        # if we find a way to match topologies that do not match exactly checkpointed ones.
        for field in sorted(problem.fields, key=operator.attrgetter("name")):
            domain = field.domain._domain

            # we do not care about fields discretized only on temporary fields
            if all(df.is_tmp for df in field.discrete_fields.values()):
                continue

            msg = f" | importing field {field.pretty_name}..."
            vprint(msg)

            field_key = fmt_key(field.name)
            if field_key not in fields_group:
                msg = "Checkpoint directory '{}' does not contain any data regarding to field {}"
                msg = msg.format(load_checkpoint_dir, field.name)
                raise_error(msg)

            field_group = fields_group[field_key]

            # check that domain matches
            if field_group.attrs["domain"] != domain.__class__.__name__:
                msg = "Domain kind does not match with checkpointed field {}, loaded kind {} but expected {}."
                msg = msg.format(
                    field.name, field_group.attrs["domain"], domain.__class__.__name__
                )
                raise_error(msg)
            if field_group.attrs["dim"] != domain.dim:
                msg = "Domain dim does not match with checkpointed field {}, loaded dim {} but expected {}."
                msg = msg.format(field.name, field_group.attrs["dim"], domain.dim)
                raise_error(msg)
            if field_group.attrs["origin"] != to_list(domain.origin):
                msg = "Domain origin does not match with checkpointed field {}, loaded origin {} but expected {}."
                msg = msg.format(field.name, field_group.attrs["origin"], domain.origin)
                raise_error(msg)
            if field_group.attrs["end"] != to_list(domain.end):
                msg = "Domain end does not match with checkpointed field {}, loaded end {} but expected {}."
                msg = msg.format(field.name, field_group.attrs["end"], domain.end)
                raise_error(msg)
            if field_group.attrs["length"] != to_list(domain.length):
                msg = "Domain length does not match with checkpointed field {}, loaded length {} but expected {}."
                msg = msg.format(field.name, field_group.attrs["length"], domain.length)
                raise_error(msg)

            for k, topo in enumerate(
                sorted(field.discrete_fields, key=operator.attrgetter("full_tag"))
            ):
                dfield = field.discrete_fields[topo]
                mesh = topo.mesh._mesh

                # we do not care about temporary fields
                if dfield.is_tmp:
                    continue

                # for now we just handle CartesianDiscreteScalarFields.
                if not isinstance(dfield, CartesianDiscreteScalarField):
                    raise NotImplementedError

                # first we need to exactly match global grid resolution
                candidates = tuple(
                    filter(
                        lambda d: np.equal(d.shape, mesh.grid_resolution).all(),
                        field_group.values(),
                    )
                )
                if len(candidates) == 0:
                    msg = "Could not find any topology with shape {} for field {}, available discretizations are: {}."
                    msg = msg.format(
                        to_tuple(mesh.grid_resolution),
                        field.name,
                        ", ".join({str(d.shape) for d in field_group.values()}),
                    )
                    raise_error(msg)
                elif len(candidates) == 1:
                    load_array_data(candidates[0], dfield)
                    continue

                # Here multiple topologies have the extact same grid resolution so we try to match boundary conditions
                old_candidates = candidates
                candidates = tuple(
                    filter(
                        lambda d: d.attrs["lboundaries"]
                        == to_tuple(map(str, mesh.global_lboundaries)),
                        candidates,
                    )
                )
                candidates = tuple(
                    filter(
                        lambda d: d.attrs["rboundaries"]
                        == to_tuple(map(str, mesh.global_rboundaries)),
                        candidates,
                    )
                )
                if len(candidates) == 0:
                    # ok, the user changed the boundary conditions, we ignore boundary condition information
                    candidates = old_candidates
                elif len(candidates) == 1:
                    load_array_data(candidates[0], dfield)
                    continue

                # From now on multiple topologies have the same grid resolution and boundary conditions
                # We try to match exact ghost count, user did likely not change the order of the methods.
                old_candidates = candidates
                candidates = tuple(
                    filter(
                        lambda d: d.attrs["ghosts"] == to_tuple(mesh.ghosts), candidates
                    )
                )
                if len(candidates) == 0:
                    # ok, the user made a change that affected ghosts, we ignore the ghost condition
                    candidates = old_candidates
                elif len(candidates) == 1:
                    load_array_data(candidates[0], dfield)
                    continue

                # Now we try to differentiate by using zero ghost info (ghosts may change with method order, but zero-ghost is very specific)
                # Topology containing zero ghost layer usually target Fortran topologies for FFT operators or method that do not require any ghosts.
                old_candidates = candidates
                candidates = tuple(
                    filter(
                        lambda d: (
                            np.equal(d.attrs["ghosts"], 0) == (mesh.ghosts == 0)
                        ).all(),
                        candidates,
                    )
                )
                if len(candidates) == 0:
                    # ok, we ignore the zero-ghost condition
                    candidates = old_candidates
                elif len(candidates) == 1:
                    load_array_data(candidates[0], dfield)
                    continue

                # Now we try to match exact topology shape (the MPICart grid of processes)
                # We try this late because use may run the simulation again with a different number of processes.
                old_candidates = candidates
                candidates = tuple(
                    filter(
                        lambda d: d.attrs["proc_shape"] == to_tuple(mesh.proc_shape),
                        candidates,
                    )
                )
                if len(candidates) == 0:
                    # ok, we ignore the proc shape
                    candidates = old_candidates
                elif len(candidates) == 1:
                    load_array_data(candidates[0], dfield)
                    continue

                # Now we try to differentiate by using topo splitting info (axes on which data is distributed)
                # This again is very specific and can differentiate topologies used for spectral transforms.
                old_candidates = candidates
                candidates = tuple(
                    filter(
                        lambda d: (
                            np.greater(d.attrs["proc_shape"], 1)
                            == (mesh.proc_shape > 1)
                        ).all(),
                        candidates,
                    )
                )
                if len(candidates) == 0:
                    # ok, we ignore the MPI data splitting condition
                    candidates = old_candidates
                elif len(candidates) == 1:
                    load_array_data(candidates[0], dfield)
                    continue

                # Ok now, our last hope is to match the discrete field name
                old_candidates = candidates
                candidates = tuple(
                    filter(lambda d: d.attrs["name"] == dfield.name, candidates)
                )
                if len(candidates) == 0:
                    # ok, we ignore the name
                    candidates = old_candidates
                elif len(candidates) == 1:
                    load_array_data(candidates[0], dfield)
                    continue

                assert len(candidates) > 1, "Something went wrong."

                msg = "Could not discriminate checkpointed topologies for field {}, got {} candidates remaining."
                msg = msg.format(field.name, len(candidates))
                raise_error(msg)

    @staticmethod
    def _load_array_data(array, dfield, on_mismatch):
        mesh = dfield.mesh._mesh
        assert np.equal(array.shape, mesh.grid_resolution).all()

        # compare attributes but ignore name because this can be annoying
        attr_names = (
            "left boundaries",
            "right boundaries",
            "ghost layers",
            "process shape",
            "datatype",
        )
        array_attributes = (
            array.attrs["lboundaries"],
            array.attrs["rboundaries"],
            array.attrs["ghosts"],
            array.attrs["proc_shape"],
            array.dtype,
        )
        dfield_attributes = (
            list(map(str, mesh.global_lboundaries)),
            list(map(str, mesh.global_rboundaries)),
            list(mesh.ghosts),
            list(mesh.proc_shape),
        )

        for name, lhs, rhs in zip(attr_names, array_attributes, dfield_attributes):
            if lhs == rhs:
                continue
            msg = "{} do not match with checkpointed field {}, loaded {} {} but expected {}."
            msg = msg.format(name, dfield.field.name, name, lhs, rhs)
            on_mismatch(msg)

        global_slices = mesh.global_compute_slices
        data = np.asarray(array[global_slices], dtype=dfield.dtype)
        dfield.compute_data[0][...] = data
        dfield.exchange_ghosts()

    @staticmethod
    def _raise_error(msg):
        vprint(f" |   error: {msg}\n")
        vprint()
        err = "FATAL ERROR: Failed to import checkpoint, because the following error occured: {}."
        raise RuntimeError(err.format(msg))

    @staticmethod
    def _raise_warning(msg):
        msg = f" |   warning: {msg}"
        vprint(msg)

    @staticmethod
    def _format_zarr_key(k):
        # note keys that contains the special characters '/' and '\' do not work well with zarr
        # so we need to replace it by another character such as '_'.
        # We cannot use utf8 characters such as u+2215 (division slash).
        if k is None:
            return None
        return k.replace("/", "_").replace("\\", "_")
