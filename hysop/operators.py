# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Shortcuts for operators import

Allows things like:
from hysop.operators import DirectionalAdvection
"""

from hysop.operator.poisson import Poisson
from hysop.operator.poisson_curl import PoissonCurl
from hysop.operator.diffusion import Diffusion  # FFTW diffusion
from hysop.operator.advection import Advection  # Scales fortran advection
from hysop.operator.penalization import PenalizeVorticity, PenalizeVelocity
from hysop.operator.flowrate_correction import FlowRateCorrection
from hysop.operator.vorticity_absorption import VorticityAbsorption
from hysop.operator.transpose import Transpose
from hysop.operator.misc import Noop, ForceTopologyState

from hysop.operator.redistribute import Redistribute

from hysop.operator.analytic import AnalyticField
from hysop.operator.mean_field import ComputeMeanField
from hysop.operator.enstrophy import Enstrophy
from hysop.operator.kinetic_energy import KineticEnergy
from hysop.operator.adapt_timestep import AdaptiveTimeStep
from hysop.operator.hdf_io import HDF_Writer, HDF_Reader
from hysop.operator.custom_symbolic import CustomSymbolicOperator
from hysop.operator.plotters import ParameterPlotter, FieldPlotter2D
from hysop.operator.integrate import Integrate
from hysop.operator.penalization import PenalizeVorticity
from hysop.operator.flowrate_correction import FlowRateCorrection
from hysop.operator.vorticity_absorption import VorticityAbsorption
from hysop.operator.dummy import Dummy
from hysop.operator.custom import CustomOperator
from hysop.operator.convergence import Convergence
from hysop.operator.spatial_filtering import SpatialFilter
from hysop.operator.reductions import ParameterReduction

from hysop.operator.derivative import (
    SpaceDerivative,
    SpectralSpaceDerivative,
    FiniteDifferencesSpaceDerivative,
    MultiSpaceDerivatives,
)

from hysop.operator.min_max import (
    MinMaxFieldStatistics,
    MinMaxFiniteDifferencesDerivativeStatistics,
    MinMaxSpectralDerivativeStatistics,
)

from hysop.operator.gradient import Gradient, MinMaxGradientStatistics
from hysop.operator.curl import Curl, SpectralCurl
from hysop.operator.external_force import SpectralExternalForce

try:
    from hysop.backend.device.opencl.operator.external_force import (
        SymbolicExternalForce,
    )
except ImportError:
    SymbolicExternalForce = None

from hysop.numerics.splitting.strang import StrangSplitting
from hysop.operator.directional.symbolic_dir import DirectionalSymbolic
from hysop.operator.directional.advection_dir import DirectionalAdvection
from hysop.operator.directional.diffusion_dir import DirectionalDiffusion
from hysop.operator.directional.stretching_dir import DirectionalStretching
from hysop.operator.directional.stretching_dir import StaticDirectionalStretching
from hysop.operator.directional.stretching_diffusion_dir import (
    DirectionalStretchingDiffusion,
)
