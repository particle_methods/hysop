# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Parameters description.
* :class:`~hysop.parameters.parameter.Parameter`
"""

from abc import ABCMeta, abstractmethod
from hysop import dprint, vprint, __DEBUG__
from hysop.tools.htypes import check_instance, to_tuple, first_not_None
from hysop.tools.handle import TaggedObject
from hysop.tools.variable import Variable, VariableTag


class Parameter(TaggedObject, VariableTag, metaclass=ABCMeta):
    """
    A parameter is a value of a given type that may change value as simulation advances.
    Parameters are only available on the host backend.
    """

    def __new__(
        cls,
        name,
        parameter_types,
        initial_value=None,
        allow_None=False,
        quiet=False,
        const=False,
        pretty_name=None,
        var_name=None,
        is_view=False,
        **kwds,
    ):
        """
        Create or _get an existing Parameter with a specific name
        and type.

        Parameters
        ----------
        name : string
            A name for the parameter.
            A parameter is uniquely identified by its name.
        pretty_name : string
            A pretty name for the parameter.
        var_name : string
            A variable name for the parameter.
        parameter_types: type of array like of types
            Allowed types for the parameter.
            None is accepted only if allow_none is set to True.
        initial_value: optional, defaults to None
            Type is not checked at initialization to allow initial None.
        allow_None: bool, optional
            Set this to True to allow None as valid value.
        const: bool, optional
            Set this parameter constant (initial_value should be given).
        quiet: bool, optional
            Do not print parameter in value change.
            Defaults to False.
        is_view: bool
            Is this parameter a view on another parameter ?
        kwds: dict
            Base class arguments.

        Attributes
        ----------
        value:
            _get or set the current value of this Parameter object.
            Returned value is is a copy or a read-only reference to
            the current parameter value.
        parameter_types:
            Return allowed parameter types for this parameter.
        """
        check_instance(name, str)
        check_instance(pretty_name, str, allow_none=True)
        check_instance(var_name, str, allow_none=True)
        check_instance(allow_None, bool)
        check_instance(const, bool)
        if not isinstance(parameter_types, type):
            check_instance(parameter_types, tuple, values=type)
        else:
            parameter_types = to_tuple(parameter_types)

        if const and (initial_value is None):
            msg = "Constant parameter should be initialized."
            raise ValueError(msg)

        # register own class to authorized parameter types
        parameter_types += (cls,)

        if allow_None:
            parameter_types += (type(None),)

        parameter_types = tuple(set(parameter_types))

        obj = super().__new__(
            cls, tag_prefix="p", variable_kind=Variable.PARAMETER, **kwds
        )

        pretty_name = first_not_None(pretty_name, name)

        var_name = first_not_None(var_name, name)

        obj._name = name
        obj._pretty_name = pretty_name
        obj._var_name = var_name
        obj._parameter_types = parameter_types
        obj._const = False
        obj._value = initial_value
        obj._const = const
        obj._symbol = None
        obj._quiet = quiet
        obj._is_view = is_view

        return obj

    def __init__(
        self,
        name,
        parameter_types,
        initial_value=None,
        allow_None=False,
        quiet=False,
        const=False,
        pretty_name=None,
        var_name=None,
        is_view=False,
        **kwds,
    ):
        super().__init__(tag_prefix="p", variable_kind=Variable.PARAMETER, **kwds)

    def __eq__(self, other):
        return self is other

    def __ne__(self, other):
        return self is not other

    def __hash__(self):
        return id(self)

    def set_value(self, value):
        """Set the value of this Parameter object."""
        if self._const:
            msg = f"Cannot modify the value of constant parameter {self.pretty_name}."
            raise RuntimeError(msg)
        if not isinstance(value, self._parameter_types):
            msg = "Parameter.set_value() got a value of type {} but "
            msg += "only the following types are valid for parameter {}:\n  {}"
            msg = msg.format(type(value), self.pretty_name, self._parameter_types)
            raise ValueError(msg)
        if isinstance(value, self.__class__):
            value = value._get_value()
        self._set_value_impl(value)
        if not self.quiet or __DEBUG__:
            msg = ">Parameter {} set to {}.".format(self.pretty_name, value)
            vprint(msg)

    def _get_value(self):
        """
        _get the current value of this Parameter object.
        Returned value is not a reference to current parameter value.
        """
        return self._get_value_impl()

    def _get_name(self):
        """Return parameter name."""
        return self._name

    def _get_pretty_name(self):
        """Return parameter pretty name."""
        return self._pretty_name

    def _get_var_name(self):
        """Return parameter variable name."""
        return self._pretty_name

    def _get_const(self):
        """Return True if this parameter was set to be constant."""
        return self._const

    def _get_quiet(self):
        """Return True if this parameter was set to be quiet."""
        return self._quiet

    def _get_parameter_types(self):
        """Return allowed parameter types for this parameter."""
        return self._parameter_types

    def _get_symbol(self):
        """Return the symbolic variable associated to this parameter."""
        return self._symbol

    def _get_is_view(self):
        """Return True if this parameter is a view on another parameter."""
        return self._is_view

    @abstractmethod
    def iterviews(self):
        """Iterate over all parameters views to yield scalarparameters."""
        pass

    @abstractmethod
    def _get_value_impl(self):
        pass

    @abstractmethod
    def _set_value_impl(self, value):
        pass

    @abstractmethod
    def short_description(self):
        """Return a short description of this parameter as a string."""
        pass

    @abstractmethod
    def long_description(self):
        """Return a long description of this parameter as a string."""
        pass

    def __call__(self):
        """Alias to self.get_value()"""
        return self.value

    def __str__(self):
        return self.long_description()

    parameter_types = property(_get_parameter_types)
    name = property(_get_name)
    pretty_name = property(_get_pretty_name)
    var_name = property(_get_var_name)
    value = property(_get_value, set_value)
    symbol = property(_get_symbol)
    const = property(_get_const)
    quiet = property(_get_quiet)
    s = property(_get_symbol)
    is_view = property(_get_is_view)
