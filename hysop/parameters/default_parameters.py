# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from hysop.constants import HYSOP_REAL
from hysop.tools.htypes import first_not_None
from hysop.tools.sympy_utils import greak, Greak, subscripts
from hysop.parameters.tensor_parameter import TensorParameter
from hysop.parameters.scalar_parameter import ScalarParameter


def TimeParameters(dtype=None, **kwds):
    dtype = first_not_None(dtype, HYSOP_REAL)
    t = ScalarParameter("t", dtype=dtype, **kwds)
    dt = ScalarParameter("dt", dtype=dtype, **kwds)
    return (t, dt)


def ViscosityParameter(name=None, pretty_name=None, mu=False, **kwds):
    if mu:
        name = first_not_None(name, "mu")
        pretty_name = first_not_None(pretty_name, greak[11])
    else:
        name = first_not_None(name, "nu")
        pretty_name = first_not_None(pretty_name, greak[12])
    return ScalarParameter(name=name, pretty_name=pretty_name, **kwds)


def EnstrophyParameter(name=None, pretty_name=None, **kwds):
    name = first_not_None(name, "enstrophy")
    pretty_name = first_not_None(pretty_name, greak[5])
    return ScalarParameter(name=name, pretty_name=pretty_name, **kwds)


def KineticEnergyParameter(name=None, pretty_name=None, **kwds):
    name = first_not_None(name, "kinetic_energy")
    pretty_name = first_not_None(pretty_name, "Eₙ")
    return ScalarParameter(name=name, pretty_name=pretty_name, **kwds)


def VolumicIntegrationParameter(
    name=None, pretty_name=None, field=None, dtype=None, **kwds
):
    if field is not None:
        pretty_name = first_not_None(pretty_name, name, field.pretty_name)
        name = first_not_None(name, field.name)
        dtype = first_not_None(dtype, field.dtype)
    dtype = first_not_None(dtype, HYSOP_REAL)
    name += "_v"
    pretty_name += "ᵥ"
    if field.nb_components > 1:
        return TensorParameter(
            name=name,
            pretty_name=pretty_name,
            shape=(field.nb_components,),
            dtype=dtype,
            **kwds,
        )
    else:
        return ScalarParameter(name=name, pretty_name=pretty_name, dtype=dtype, **kwds)
