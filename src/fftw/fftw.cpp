/*
 * Copyright (c) HySoP 2011-2024
 *
 * This file is part of HySoP software.
 * See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
 * for further info.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "fftw.hpp"
#include <iostream>


namespace fftw
{

  FFTW_solver::FFTW_solver(int d):_dimension(d)
  {
    std::cout << "init fftw c++, dim =" << _dimension <<  std::endl;
  }

  int FFTW_solver::run()
  {
    std::cout << "run fftw c++" << std::endl;
    return 1;
  }


}
