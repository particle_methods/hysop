# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Example for fixed point iteration inside Simulation.

We consider steady state of heat equation with a varying source term.
Each time step, the source terms are computed analytically and then an
iterative method is used to compute  steady state solution.
"""
import numpy as np
import sympy as sp

pos = [0.5, 0.5, 0.5]
RADIUS = 0.2


def chi(x, y, z):
    return (
        np.sqrt((x - pos[0]) * (x - pos[0]) + (y - pos[1]) * (y - pos[1]) + 0.0 * z)
        <= RADIUS
    )


def schi(x, y, z):
    return (
        sp.sqrt((x - pos[0]) * (x - pos[0]) + (y - pos[1]) * (y - pos[1]) + 0.0 * z)
        <= RADIUS
    )


# Analytic operator for computing source term


def CS(data, coords, t, component):
    (x, y, z) = coords
    data[...] = 0.0
    data[chi(x, y, z)] = np.cos(t())


def init_u(data, coords, component):
    (x, y, z) = coords
    data[...] = 0.0


def compute(args):
    from hysop import (
        Box,
        Simulation,
        Problem,
        Field,
        MPIParams,
        IO,
        IOParams,
        main_rank,
    )
    from hysop.constants import Implementation, Backend, ResidualError
    from hysop.defaults import TimeParameters
    from hysop.parameters.scalar_parameter import ScalarParameter
    from hysop.topology.cartesian_topology import CartesianTopology
    from hysop.tools.parameters import CartesianDiscretization
    from hysop.symbolic.tmp import TmpScalar
    from hysop.operators import (
        StrangSplitting,
        DirectionalDiffusion,
        Diffusion,
        Convergence,
        AnalyticField,
        Dummy,
        CustomSymbolicOperator,
        HDF_Writer,
        CustomOperator,
    )
    from hysop.symbolic.relational import Assignment
    from hysop.symbolic.misc import Select
    from hysop.iterative_method import PseudoSimulation, IterativeMethod
    from hysop.simulation import eps

    # Define the domain
    dim = args.ndim
    npts = args.npts
    box = Box(origin=args.box_origin, length=args.box_length, dim=dim)

    # Get default MPI Parameters from domain (even for serial jobs)
    mpi_params = MPIParams(comm=box.task_comm, task_id=box.current_task())

    # Setup usual implementation specific variables
    impl = args.impl
    extra_op_kwds = {"mpi_params": mpi_params}
    if impl in (Implementation.PYTHON, Implementation.FORTRAN):
        backend = Backend.HOST
        method = {}
    elif impl is Implementation.OPENCL:
        # For the OpenCL implementation we need to setup the compute device
        # and configure how the code is generated and compiled at runtime.

        # Create an explicit OpenCL context from user parameters
        from hysop.backend.device.opencl.opencl_tools import (
            get_or_create_opencl_env,
            get_device_number,
        )

        cl_env = get_or_create_opencl_env(
            mpi_params=mpi_params,
            platform_id=args.cl_platform_id,
            device_id=(
                box.machine_rank % get_device_number()
                if args.cl_device_id is None
                else None
            ),
        )

        # Configure OpenCL kernel generation and tuning (already done by HysopArgParser)
        from hysop.methods import OpenClKernelConfig

        method = {OpenClKernelConfig: args.opencl_kernel_config}

        # Setup opencl specific extra operator keyword arguments
        extra_op_kwds["cl_env"] = cl_env
        backend = Backend.OPENCL
    else:
        msg = f"Unknown implementation '{impl}'."
        raise ValueError(msg)

    # Fields
    uSources = Field(domain=box, dtype=args.dtype, is_vector=False, name="uSources")
    u = Field(domain=box, dtype=args.dtype, is_vector=False, name="u")
    convergence = ScalarParameter(
        name="conv", dtype=args.dtype, quiet=True, initial_value=1e10
    )
    t, dt = TimeParameters(dtype=args.dtype)
    pseudo_dt = ScalarParameter(
        name="pseudo_dt", dtype=args.dtype, min_value=eps, initial_value=eps, quiet=True
    )

    # Operator for setting iterative method
    if impl is Implementation.OPENCL:
        (x0, x1, x2) = box.frame.coords
        utemp = TmpScalar(name="utemp", value=0.0, dtype=args.dtype)
        source = CustomSymbolicOperator(
            implementation=impl,
            name="BCAndSourceTerm",
            exprs=(
                # Source term
                Assignment(utemp, Select(u.s(), uSources.s(), schi(x0, x1, x2))),
                # BC enforcement
                Assignment(utemp, Select(utemp, 0.0, x0 < 0.1)),
                Assignment(utemp, Select(utemp, 0.0, x0 > 0.9)),
                Assignment(utemp, Select(utemp, 0.0, x1 < 0.1)),
                Assignment(utemp, Select(utemp, 0.0, x1 > 0.9)),
                Assignment(u.s(), utemp),
            ),
            variables={uSources: npts, u: npts},
            **extra_op_kwds,
        )
    else:

        def computeSRC(src_in, u_in, u_out):
            (x, y, z) = src_in.mesh_coords
            # Source term
            u_out.data[0][chi(x, y, z)] = src_in.data[0][chi(x, y, z)]
            # BC enforcment
            u_out.data[0][:, :, x[0, 0, :] < 0.1] = 0.0
            u_out.data[0][:, :, x[0, 0, :] > 0.9] = 0.0
            u_out.data[0][:, y[0, :, 0] < 0.1, :] = 0.0
            u_out.data[0][:, y[0, :, 0] > 0.9, :] = 0.0

        source = CustomOperator(
            implementation=Implementation.PYTHON,
            func=computeSRC,
            variables={uSources: npts, u: npts},
            invars=(uSources, u),
            outvars=(u,),
            **extra_op_kwds,
        )
    # Diffusion operator
    if impl is Implementation.FORTRAN or impl is Implementation.PYTHON:
        diffuse = Diffusion(
            implementation=impl,
            name="diffuse",
            Fin=u,
            Fout=u,
            nu=0.1,
            variables={u: npts},
            dt=pseudo_dt,
            **extra_op_kwds,
        )
    else:
        diffuse_op = DirectionalDiffusion(
            implementation=impl,
            name="diffuse",
            fields=(u),
            coeffs=(0.1,),
            variables={u: npts},
            dt=pseudo_dt,
            **extra_op_kwds,
        )
        diffuse = StrangSplitting(splitting_dim=dim, order=args.strang_order)
        diffuse.push_operators(diffuse_op)
    # Convergence operator with absolute error
    conv = Convergence(
        convergence=convergence,
        method={ResidualError: ResidualError.ABSOLUTE},
        variables={u: npts},
        name="convergence",
        implementation=Implementation.PYTHON,
        **extra_op_kwds,
    )
    # Dummy operators to fix in/out topologies
    fp_in = Dummy(
        implementation=Implementation.PYTHON,
        name="dummy_fixedpoint_input",
        variables={uSources: npts, u: npts},
        **extra_op_kwds,
    )
    fp_out = Dummy(
        implementation=Implementation.PYTHON,
        name="dummy_fixedpoint_output",
        variables={u: npts},
        **extra_op_kwds,
    )
    # Iterative method problem with convergence
    fixedPoint = IterativeMethod(
        stop_criteria=convergence,
        tolerance=1e-6,
        dt0=1e-3,
        dt=pseudo_dt,
        name="FixedPointIterations",
    )
    fixedPoint.insert(fp_in, diffuse, source, conv, fp_out)
    heat_sources = AnalyticField(
        name="heat_sources",
        field=uSources,
        formula=CS,
        variables={uSources: npts},
        implementation=Implementation.PYTHON,
        extra_input_kwds={"t": t},
        **extra_op_kwds,
    )
    # Dummy operators to fix in/out topologies
    fp_enter = Dummy(
        implementation=Implementation.PYTHON,
        name="dummy_fixedpoint_enter",
        variables={uSources: npts, u: npts},
        **extra_op_kwds,
    )
    fp_after = Dummy(
        implementation=Implementation.PYTHON,
        name="dummy_fixedpoint_after",
        variables={u: npts},
        **extra_op_kwds,
    )
    # > We ask to dump the outputs of this operator
    io_params = IOParams(filename="fields", frequency=args.dump_freq)
    dump_fields = HDF_Writer(
        name="fields", io_params=io_params, variables={u: npts}, **extra_op_kwds
    )

    # Main problem (time iterations)
    problem = Problem(name="MainProblem")
    problem.insert(heat_sources, fp_enter, fixedPoint, fp_after, dump_fields)
    problem.build()
    simu = Simulation(
        start=args.tstart,
        end=args.tend,
        nb_iter=args.nb_iter,
        max_iter=args.max_iter,
        dt0=args.dt,
        times_of_interest=args.times_of_interest,
        t=t,
        dt=dt,
    )
    simu.write_parameters(t, fixedPoint.it_num, filename="parameters.txt", precision=8)
    problem.initialize_field(u, formula=init_u)
    problem.solve(
        simu, dry_run=args.dry_run, checkpoint_handler=args.checkpoint_handler
    )
    problem.finalize()


if __name__ == "__main__":
    from hysop_examples.argparser import HysopArgParser, colors

    class IMArgParser(HysopArgParser):
        def __init__(self):
            prog_name = "iterative_method"
            default_dump_dir = "{}/hysop_examples/{}".format(
                HysopArgParser.tmp_dir(), prog_name
            )
            description = colors.color(
                "HySoP iterative method for heat equation Example.\n",
                fg="blue",
                style="bold",
            )
            super().__init__(
                prog_name=prog_name,
                description=description,
                default_dump_dir=default_dump_dir,
            )

    parser = IMArgParser()

    parser.set_defaults(
        impl="cl",
        ndim=3,
        npts=(32,),
        box_origin=(0.0,),
        box_length=(1.0,),
        tstart=0.0,
        tend=np.pi * 2,
        dt=1e-1,
        dump_freq=10,
    )

    parser.run(compute)
