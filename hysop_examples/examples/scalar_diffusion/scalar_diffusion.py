# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np

# Function to compute initial scalar values


def init_scalar(data, coords, component):
    assert component == 0, component
    data[...] = 0.0
    for x in coords:
        data[...] += (x - 0.5) * (x - 0.5)
    data[...] = np.exp(-np.sqrt(data))


def compute(args):
    from hysop import Field, Box, Simulation, Problem, ScalarParameter, MPIParams

    from hysop.constants import Implementation, ComputeGranularity
    from hysop.operator.directional.diffusion_dir import DirectionalDiffusion
    from hysop.operator.diffusion import Diffusion

    from hysop.methods import StrangOrder, TimeIntegrator, SpaceDiscretization
    from hysop.numerics.splitting.strang import StrangSplitting

    # Define domain
    dim = args.ndim
    npts = args.npts
    box = Box(origin=args.box_origin, length=args.box_length, dim=dim)

    # Get default MPI Parameters from domain (even for serial jobs)
    mpi_params = MPIParams(comm=box.task_comm, task_id=box.current_task())

    # Define parameters and field (time and analytic field)
    dt = ScalarParameter("dt", dtype=args.dtype)
    nu = ScalarParameter("nu", initial_value=args.nu, const=True, dtype=args.dtype)
    scalar = Field(domain=box, name="S0", nb_components=1, dtype=args.dtype)

    # Diffusion operator discretization parameters
    method = {
        ComputeGranularity: args.compute_granularity,
        SpaceDiscretization: args.fd_order,
        TimeIntegrator: args.time_integrator,
    }

    # Setup implementation specific variables
    impl = args.impl
    extra_op_kwds = {"mpi_params": mpi_params}

    # Create the problem we want to solve and insert our
    # directional splitting subgraph.
    # Add a writer of input field at given frequency.
    problem = Problem(method=method, mpi_params=mpi_params)

    if impl is Implementation.FORTRAN:
        # Build the directional diffusion operator
        diffusion = Diffusion(
            implementation=impl,
            name="diffusion",
            dt=dt,
            Fin=scalar,
            nu=nu,
            variables={scalar: npts},
            **extra_op_kwds,
        )
        problem.insert(diffusion)
    elif impl is Implementation.OPENCL:
        # For the OpenCL implementation we need to setup the compute device
        # and configure how the code is generated and compiled at runtime.

        # Create an explicit OpenCL context from user parameters
        from hysop.backend.device.opencl.opencl_tools import (
            get_or_create_opencl_env,
            get_device_number,
        )

        cl_env = get_or_create_opencl_env(
            mpi_params=mpi_params,
            platform_id=args.cl_platform_id,
            device_id=(
                box.machine_rank % get_device_number()
                if args.cl_device_id is None
                else None
            ),
        )

        # Configure OpenCL kernel generation and tuning (already done by HysopArgParser)
        from hysop.methods import OpenClKernelConfig

        method[OpenClKernelConfig] = args.opencl_kernel_config

        # Setup opencl specific extra operator keyword arguments
        extra_op_kwds["cl_env"] = cl_env

        # Build the directional diffusion operator
        diffusion = DirectionalDiffusion(
            implementation=impl,
            name="diffusion",
            dt=dt,
            fields=scalar,
            coeffs=nu,
            variables={scalar: npts},
            **extra_op_kwds,
        )

        # Build the directional splitting operator graph
        splitting = StrangSplitting(splitting_dim=dim, order=args.strang_order)
        splitting.push_operators(diffusion)

        problem.insert(splitting)
    else:
        msg = f"Unknown implementation '{impl}'."
        raise ValueError(msg)

    io_params = args.io_params.clone(filename="field")
    problem.dump_inputs(fields=scalar, io_params=io_params, **extra_op_kwds)
    problem.build(args)

    # If a visu_rank was provided, and show_graph was set,
    # display the graph on the given process rank.
    if args.display_graph:
        problem.display(args.visu_rank)

    # Initialize discrete scalar field
    problem.initialize_field(scalar, formula=init_scalar)

    # Create a simulation and solve the problem
    # (do not forget to specify the dt parameter here)
    simu = Simulation(
        start=args.tstart,
        end=args.tend,
        nb_iter=args.nb_iter,
        max_iter=args.max_iter,
        times_of_interest=args.times_of_interest,
        dt=dt,
        dt0=args.dt,
    )

    # Finally solve the problem
    problem.solve(
        simu,
        dry_run=args.dry_run,
        debug_dumper=args.debug_dumper,
        checkpoint_handler=args.checkpoint_handler,
    )

    # Finalize
    problem.finalize()


if __name__ == "__main__":
    from hysop_examples.argparser import HysopArgParser, colors

    class ScalarDiffusionArgParser(HysopArgParser):
        def __init__(self):
            prog_name = "scalar_diffusion"
            default_dump_dir = f"{HysopArgParser.tmp_dir()}/hysop_examples/{prog_name}"

            description = colors.color(
                "HySoP Scalar Diffusion Example: ", fg="blue", style="bold"
            )
            description += "Diffuse a scalar with a given diffusion coefficient. "
            description += (
                "\n\nThe diffusion operator is directionally splitted resulting "
            )
            description += (
                "in the use of one or more diffusion operators per direction."
            )

            super().__init__(
                prog_name=prog_name,
                description=description,
                default_dump_dir=default_dump_dir,
            )

        def _add_main_args(self):
            args = super()._add_main_args()
            args.add_argument(
                "-nu",
                "--diffusion-coeff",
                type=float,
                dest="nu",
                help="Diffusion coefficient.",
            )
            return args

        def _check_main_args(self, args):
            super()._check_main_args(args)
            self._check_default(args, "nu", float, allow_none=False)

    parser = ScalarDiffusionArgParser()

    parser.set_defaults(
        box_origin=(0.0,),
        box_length=(1.0,),
        tstart=0.0,
        tend=1.0,
        nb_iter=500,
        dump_freq=5,
        nu=0.01,
        impl="cl",
    )

    parser.run(compute)
