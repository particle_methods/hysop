#!/usr/bin/env python2
# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
import sympy as sm

TASK_A = 111
TASK_B = 222


def compute(args):
    """
    HySoP Tasks Example: Initialize a field with a space and time dependent analytic formula one task A, then compute on other task B
    """
    from hysop import (
        Field,
        Box,
        IOParams,
        MPIParams,
        Simulation,
        Problem,
        ScalarParameter,
    )
    from hysop.constants import HYSOP_DEFAULT_TASK_ID, Implementation
    from hysop.operators import CustomOperator, Dummy

    has_tasks = not args.proc_tasks is None
    try:
        has_tasks_overlapping = any(len(_) > 1 for _ in args.proc_tasks)
    except TypeError:
        has_tasks_overlapping = False

    # Define domain
    npts = args.npts
    box = Box(
        origin=args.box_origin,
        length=args.box_length,
        dim=args.ndim,
        proc_tasks=args.proc_tasks,
    )

    # Define parameters and field (time and analytic field)
    t = ScalarParameter("t", dtype=args.dtype, initial_value=0.0)
    p = ScalarParameter("p", dtype=args.dtype, initial_value=1.0, quiet=False)
    q = ScalarParameter("q", dtype=args.dtype, initial_value=1.0, quiet=False)
    A = Field(domain=box, name="A", dtype=args.dtype)
    B = Field(domain=box, name="B", dtype=args.dtype)
    C = Field(domain=box, name="C", dtype=args.dtype)
    D = Field(domain=box, name="D", dtype=args.dtype)
    if has_tasks_overlapping:
        Ap = Field(domain=box, name="Ap", dtype=args.dtype)
    else:
        Ap = A
    # Build the mpi_params for each task (keep in mind tha some procs can be defined on both tasks)
    if has_tasks:
        mpi_params = {TASK_A: None, TASK_B: None}
        for tk in (TASK_A, TASK_B):
            mpi_params[tk] = MPIParams(
                comm=box.get_task_comm(tk), task_id=tk, on_task=box.is_on_task(tk)
            )
    else:
        mpi_params = MPIParams(comm=box.task_comm, task_id=HYSOP_DEFAULT_TASK_ID)
    impl = args.impl

    # Setup implementation specific variables

    def _get_op_kwds(tk):
        if has_tasks:
            op_kwds = {"mpi_params": mpi_params[tk]}
        else:
            op_kwds = {"mpi_params": mpi_params}
        if (impl is Implementation.OPENCL) and box.is_on_task(tk):
            # For the OpenCL implementation we need to setup the compute device
            # and configure how the code is generated and compiled at runtime.

            # Create an explicit OpenCL context from user parameters
            from hysop.backend.device.opencl.opencl_tools import (
                get_or_create_opencl_env,
                get_device_number,
            )

            cl_env = get_or_create_opencl_env(
                mpi_params=mpi_params[tk],
                platform_id=args.cl_platform_id,
                device_id=(
                    box.machine_rank % get_device_number()
                    if args.cl_device_id is None
                    else None
                ),
            )

            # Configure OpenCL kernel generation and tuning (already done by HysopArgParser)
            from hysop.methods import OpenClKernelConfig

            method = {OpenClKernelConfig: args.opencl_kernel_config}

            # Setup opencl specific extra operator keyword arguments
            op_kwds["cl_env"] = cl_env
            op_kwds["method"] = method
        return op_kwds

    # Analytic initialization method depends on chosen implementation
    if impl is Implementation.PYTHON:

        def compute_scalar(data, coords, component):
            data[...] = sum(x * x for x in coords)

    elif impl is Implementation.OPENCL:
        compute_scalar = sum(xi * xi for xi in box.frame.coords)
    else:
        msg = f"Unknown implementation {impl}."

    def custom_func1(_Ai, _Ao, _p, _q):
        _p.value = args.dtype(np.min(_Ai.data[0]))
        _q.value = args.dtype(np.min(_Ai.data[0]))
        _Ao.data[0][...] = np.cos(_Ai.data[0])

    def custom_func2(_A, _B, _C):
        _B.data[0][...] = _A.data[0] ** 2
        _C.data[0][...] = 1.0

    def custom_func3(_B, _C, _D, _p, _q, _A):
        _A.data[0][...] = (
            _B.data[0] + (_C.data[0] + _D.data[0] - 1.0) ** 2 + _p() + _q()
        )

    # TaskA          |   TaskB
    # op1 A->A,p,q   |
    # op2 A->B,C     |
    #         `--B,C,p,q--,              // Inter-task communication step
    #                |  op3 p,q,B,C,D->A
    #         ,-----A---'               // Inter-task communication step
    # endA  A->A     |

    # Note : without endA operator, the second communication is not
    # automatically inserted because A field is an output for task A
    # and cannot be invalidated by op3 on othre task. Inter-task
    # invalidation in graph building is not yet implemented

    # Note this algorithm is not working with overlapping tasks
    # This is due to the fact that A is both input and output in taskA.
    # A workaround is to use an other field A' and endA is then a copy

    custom1 = CustomOperator(
        name="custom_op1",
        func=custom_func1,
        invars=(A,),
        outvars=(p, q, A),
        variables={
            A: npts,
        },
        implementation=impl,
        **_get_op_kwds(TASK_A),
    )
    custom2 = CustomOperator(
        name="custom_op2",
        func=custom_func2,
        invars=(A,),
        outvars=(B, C),
        variables={A: npts, B: npts, C: npts},
        implementation=impl,
        **_get_op_kwds(TASK_A),
    )
    custom3 = CustomOperator(
        name="custom_op3",
        func=custom_func3,
        invars=(p, q, B, C, D),
        outvars=(Ap,),
        variables={Ap: npts, B: npts, C: npts, D: npts},
        implementation=impl,
        **_get_op_kwds(TASK_B),
    )
    if has_tasks_overlapping:

        def copy(_Ap, _A):
            _A.data[0][...] = _Ap.data[0]

        endA = CustomOperator(
            name="endA",
            func=copy,
            invars=(Ap,),
            outvars=(A,),
            variables={Ap: npts, A: npts},
            implementation=impl,
            **_get_op_kwds(TASK_A),
        )
    else:
        endA = Dummy(
            name="endA",
            variables={
                A: npts,
            },
            implementation=impl,
            **_get_op_kwds(TASK_A),
        )

    # Create the problem we want to solve and insert our operator
    problem = Problem()
    problem.insert(custom1, custom2, custom3, endA)
    problem.build(args)

    # Create a simulation and solve the problem
    # (do not forget to specify the time parameter here)
    simu = Simulation(
        start=args.tstart,
        end=args.tend,
        nb_iter=args.nb_iter,
        dt0=args.dt,
        max_iter=args.max_iter,
        times_of_interest=args.dump_times,
        t=t,
    )

    def init_D(data, coords, component):
        data[...] = np.sin(sum(x * x for x in coords))

    # Finally solve the problem
    if not has_tasks or box.is_on_task(TASK_A):
        problem.initialize_field(A, formula=compute_scalar)
    if not has_tasks or box.is_on_task(TASK_B):
        problem.initialize_field(D, formula=init_D)
    problem.solve(simu, dry_run=args.dry_run)

    for dA in set(A.discrete_fields.values()).union(set(Ap.discrete_fields.values())):
        assert np.allclose(dA.data[0], 1.0), (np.min(dA.data[0]), np.max(dA.data[0]))

    # Finalize
    problem.finalize()


if __name__ == "__main__":
    import mpi4py.MPI as mpi
    from hysop_examples.argparser import HysopArgParser, colors

    class TasksHysopArgParser(HysopArgParser):
        def _add_main_args(self):
            args = super()._add_main_args()
            args.add_argument(
                "--proc-tasks",
                type=str,
                action=self.eval,
                container=tuple,
                append=False,
                dest="proc_tasks",
                help="Specify the tasks for each proc.",
            )

        def _check_main_args(self, args):
            super()._check_main_args(args)
            self._check_default(args, "proc_tasks", tuple, allow_none=False)

    prog_name = "tasks"
    default_dump_dir = f"{TasksHysopArgParser.tmp_dir()}/hysop_examples/{prog_name}"

    description = colors.color("HySoP Task Example: ", fg="blue", style="bold")
    description += "Initialize a field with a space and time dependent analytic formula one task A, then compute on other task B."

    parser = TasksHysopArgParser(
        prog_name=prog_name, description=description, default_dump_dir=default_dump_dir
    )

    proc_tasks = tuple(
        TASK_A if _ == 0 else TASK_B for _ in range(mpi.COMM_WORLD.Get_size())
    )
    parser.set_defaults(
        box_origin=(0.0,),
        box_length=(2 * np.pi,),
        tstart=0.0,
        tend=10.0,
        nb_iter=1,
        dump_freq=5,
        proc_tasks=proc_tasks,
    )
    parser.run(compute)
