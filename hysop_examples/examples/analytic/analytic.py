# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
import sympy as sm


def compute(args):
    """
    HySoP Analytic Example: Initialize a field with a space and time dependent analytic formula.
    """
    from hysop import (
        Field,
        Box,
        IOParams,
        MPIParams,
        Simulation,
        Problem,
        ScalarParameter,
    )
    from hysop.constants import Implementation
    from hysop.operators import AnalyticField, HDF_Writer

    # Define domain
    npts = args.npts
    box = Box(origin=args.box_origin, length=args.box_length, dim=args.ndim)

    # Define parameters and field (time and analytic field)
    t = ScalarParameter("t", dtype=args.dtype)
    scalar = Field(domain=box, name="S0", dtype=args.dtype)

    # We need to first get default MPI parameters (even for non MPI jobs)
    # so we use default domain communicator and task.
    mpi_params = MPIParams(comm=box.task_comm, task_id=box.current_task())

    # Setup implementation specific variables
    impl = args.impl
    op_kwds = {"mpi_params": mpi_params}
    if impl is Implementation.PYTHON:
        # Setup python specific extra operator keyword arguments
        # (mapping: variable name => variable value)
        pass
    elif impl is Implementation.OPENCL:
        # For the OpenCL implementation we need to setup the compute device
        # and configure how the code is generated and compiled at runtime.

        # Create an explicit OpenCL context from user parameters
        from hysop.backend.device.opencl.opencl_tools import (
            get_or_create_opencl_env,
            get_device_number,
        )

        cl_env = get_or_create_opencl_env(
            mpi_params=mpi_params,
            platform_id=args.cl_platform_id,
            device_id=(
                box.machine_rank % get_device_number()
                if args.cl_device_id is None
                else None
            ),
        )

        # Configure OpenCL kernel generation and tuning (already done by HysopArgParser)
        from hysop.methods import OpenClKernelConfig

        method = {OpenClKernelConfig: args.opencl_kernel_config}

        # Setup opencl specific extra operator keyword arguments
        op_kwds["cl_env"] = cl_env
        op_kwds["method"] = method
    else:
        msg = f"Unknown implementation '{impl}'."
        raise ValueError(msg)

    # Analytic initialization method depends on chosen implementation
    if impl is Implementation.PYTHON:
        # With the python implementation we can directly use a python method
        # (using numpy arrays). Here each field component is stored in the
        # tuple 'data'. Coordinates will be passed as a tuple as a second
        # argument. Finally extra arguments (here t) are passed last.
        # Note that t is a ScalarParameter, so we evaluate it to get its value.
        def compute_scalar(data, coords, component, t):
            data[...] = 1.0 / (1.0 + 0.1 * t())
            for x in coords:
                data[...] *= np.cos(x - t())

        extra_input_kwds = {"t": t}
    elif impl is Implementation.OPENCL:
        # With the opencl codegen implementation we use a symbolic expression
        # generated using sympy. OpenCL code will be automatically generated,
        # compiled and passed trough a runtime parameter autotuner to achieve
        # maximal performances on a variety of different devices.
        # For a vector field, multiple symbolic expressions should be given as a tuple.
        # Here x and ts are the symbolic variables representing coordinates and time.
        xs = box.frame.coords
        ts = t.s
        compute_scalar = 1 / (1 + 0.1 * ts)
        for xi in xs:
            compute_scalar *= sm.cos(xi - ts)
        extra_input_kwds = {}
    else:
        msg = f"Unknown implementation {impl}."

    # Finally build the operator
    analytic = AnalyticField(
        name="analytic",
        field=scalar,
        formula=compute_scalar,
        variables={scalar: npts},
        implementation=impl,
        extra_input_kwds=extra_input_kwds,
        **op_kwds,
    )

    # Write output field at given frequency
    io_params = IOParams(filename="analytic", frequency=args.dump_freq)
    df = HDF_Writer(name="S", io_params=io_params, variables={scalar: npts}, **op_kwds)

    # Create the problem we want to solve and insert our operator
    problem = Problem()
    problem.insert(analytic, df)
    problem.build(args)

    # If a visu_rank was provided, and show_graph was set,
    # display the graph on the given process rank.
    if args.display_graph:
        problem.display(args.visu_rank)

    # Create a simulation and solve the problem
    # (do not forget to specify the time parameter here)
    simu = Simulation(
        start=args.tstart,
        end=args.tend,
        nb_iter=args.nb_iter,
        dt0=args.dt,
        max_iter=args.max_iter,
        times_of_interest=args.times_of_interest,
        t=t,
    )

    # Finally solve the problem
    problem.solve(
        simu,
        dry_run=args.dry_run,
        debug_dumper=args.debug_dumper,
        checkpoint_handler=args.checkpoint_handler,
    )

    # Finalize
    problem.finalize()


if __name__ == "__main__":
    from hysop_examples.argparser import HysopArgParser, colors

    prog_name = "analytic"
    default_dump_dir = f"{HysopArgParser.tmp_dir()}/hysop_examples/{prog_name}"

    description = colors.color("HySoP Analytic Example: ", fg="blue", style="bold")
    description += (
        "Initialize a field with a space and time dependent analytic formula."
    )

    parser = HysopArgParser(
        prog_name=prog_name, description=description, default_dump_dir=default_dump_dir
    )

    parser.set_defaults(
        box_origin=(0.0,),
        box_length=(2 * np.pi,),
        tstart=0.0,
        tend=10.0,
        nb_iter=100,
        dump_freq=5,
    )

    parser.run(compute)
