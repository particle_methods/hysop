# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np


def compute(args):

    from hysop import (
        Field,
        Box,
        Simulation,
        Problem,
        ScalarParameter,
        MPIParams,
        IOParams,
        IO,
    )
    from hysop.constants import Implementation, BoxBoundaryCondition
    from hysop.operators import (
        Advection,
        DirectionalAdvection,
        StrangSplitting,
        SpatialFilter,
        HDF_Writer,
    )
    from hysop.methods import (
        Remesh,
        TimeIntegrator,
        Interpolation,
        FilteringMethod,
        PolynomialInterpolator,
    )

    # IO paths
    spectral_path = IO.default_path() + "/spectral"

    # Function to compute initial velocity values
    def init_velocity(data, coords, component):
        data[...] = args.velocity[::-1][component]

    # Function to compute initial scalar values
    def init_scalar(data, coords, component):
        data[...] = 1.0
        for x in coords:
            data[...] *= np.cos(x + component * (np.pi / 2))

    # Define domain
    dim = args.ndim
    npts = args.npts  # coarse resolution
    snpts = args.snpts  # fine resolution
    fnpts = tuple(3 * _ for _ in snpts)  # finest resolution
    cnpts = tuple(_ // 2 for _ in npts)  # coarsest resolution
    lboundaries = (BoxBoundaryCondition.PERIODIC,) * dim
    rboundaries = (BoxBoundaryCondition.PERIODIC,) * dim
    box = Box(
        origin=args.box_origin,
        length=args.box_length,
        dim=dim,
        lboundaries=lboundaries,
        rboundaries=rboundaries,
    )

    # Get default MPI Parameters from domain (even for serial jobs)
    mpi_params = MPIParams(comm=box.task_comm, task_id=box.current_task())

    # Define parameters and field (time and analytic field)
    dt = ScalarParameter("dt", dtype=args.dtype)
    velo = Field(domain=box, name="V", is_vector=True, dtype=args.dtype)
    scalar = Field(domain=box, name="S", nb_components=2, dtype=args.dtype)

    # Setup operator method dictionnary
    # Advection-Remesh operator discretization parameters
    method = {
        TimeIntegrator: args.time_integrator,
        Remesh: args.remesh_kernel,
        PolynomialInterpolator: args.polynomial_interpolator,
    }

    # Setup implementation specific variables
    impl = args.impl
    extra_op_kwds = {"mpi_params": mpi_params}
    if impl is Implementation.OPENCL:
        # For the OpenCL implementation we need to setup the compute device
        # and configure how the code is generated and compiled at runtime.

        # Create an explicit OpenCL context from user parameters
        from hysop.backend.device.opencl.opencl_tools import (
            get_or_create_opencl_env,
            get_device_number,
        )

        cl_env = get_or_create_opencl_env(
            mpi_params=mpi_params,
            platform_id=args.cl_platform_id,
            device_id=(
                box.machine_rank % get_device_number()
                if args.cl_device_id is None
                else None
            ),
        )

        # Configure OpenCL kernel generation and tuning method
        # (already done by HysopArgParser for simplicity)
        from hysop.methods import OpenClKernelConfig

        method[OpenClKernelConfig] = args.opencl_kernel_config

        # Setup opencl specific extra operator keyword arguments
        extra_op_kwds["cl_env"] = cl_env
    elif impl in (Implementation.PYTHON, Implementation.FORTRAN):
        pass
    else:
        msg = f"Unknown implementation '{impl}'."
        raise ValueError(msg)

    # Create the problem we want to solve
    problem = Problem(method=method)

    if (impl is Implementation.FORTRAN) or (
        (npts != snpts) and (impl is Implementation.PYTHON)
    ):
        # The fortran scales implementation is a special case.
        # Here directional advection is a black box.
        advec = Advection(
            implementation=Implementation.FORTRAN,
            name="advec",
            velocity=velo,
            advected_fields=(scalar,),
            variables={velo: npts, scalar: snpts},
            dt=dt,
            **extra_op_kwds,
        )

        # Finally insert our advection into the problem
        problem.insert(advec)
    else:
        # Build the directional advection operator
        # here the cfl determines the maximum number of ghosts
        advec = DirectionalAdvection(
            implementation=impl,
            name="advec",
            velocity=velo,
            velocity_cfl=args.cfl,
            advected_fields=(scalar,),
            variables={velo: npts, scalar: snpts},
            dt=dt,
            **extra_op_kwds,
        )

        # Build the directional splitting operator graph
        splitting = StrangSplitting(splitting_dim=dim, order=args.strang_order)
        splitting.push_operators(advec)

        # Finally insert our splitted advection into the problem
        problem.insert(splitting)

    # > Interpolation filter
    interpolation_filter = SpatialFilter(
        input_variables={scalar: snpts},
        output_variables={scalar: fnpts},
        filtering_method=args.interpolation_filter,
        implementation=impl,
        **extra_op_kwds,
    )
    # > Restriction filter
    restriction_filter = SpatialFilter(
        input_variables={scalar: npts},
        output_variables={scalar: cnpts},
        filtering_method=args.restriction_filter,
        implementation=impl,
        **extra_op_kwds,
    )

    # > Operators to dump all fields
    io_params = IOParams(filename="finest", frequency=args.dump_freq)
    df0 = HDF_Writer(
        name="S_finest", io_params=io_params, variables={scalar: fnpts}, **extra_op_kwds
    )
    io_params = IOParams(filename="fine", frequency=args.dump_freq)
    df1 = HDF_Writer(
        name="S_fine", io_params=io_params, variables={scalar: snpts}, **extra_op_kwds
    )
    io_params = IOParams(filename="coarse", frequency=args.dump_freq)
    df2 = HDF_Writer(
        name="S_coarse", io_params=io_params, variables={scalar: npts}, **extra_op_kwds
    )
    io_params = IOParams(filename="coarsest", frequency=args.dump_freq)
    df3 = HDF_Writer(
        name="S_coarsest",
        io_params=io_params,
        variables={scalar: cnpts},
        **extra_op_kwds,
    )

    # Add a writer of input field at given frequency.
    problem.insert(interpolation_filter, restriction_filter, df0, df1, df2, df3)
    problem.build(args)

    # If a visu_rank was provided, and show_graph was set,
    # display the graph on the given process rank.
    if args.display_graph:
        problem.display(args.visu_rank)

    # Initialize discrete velocity and scalar field
    problem.initialize_field(velo, formula=init_velocity)
    problem.initialize_field(scalar, formula=init_scalar)

    # Determine a timestep using the supplied CFL
    # (velocity is constant for the whole simulation)
    dx = problem.get_input_discrete_field(scalar).space_step.min()
    Vinf = max(abs(vi) for vi in args.velocity)
    dt0 = (args.cfl * dx) / Vinf
    if args.dt is not None:
        dt0 = min(args.dt, dt0)
    dt0 = 0.99 * dt0

    # Create a simulation and solve the problem
    # (do not forget to specify the dt parameter here)
    simu = Simulation(
        start=args.tstart,
        end=args.tend,
        nb_iter=args.nb_iter,
        max_iter=args.max_iter,
        times_of_interest=args.times_of_interest,
        dt=dt,
        dt0=dt0,
    )

    # Finally solve the problem
    problem.solve(
        simu,
        dry_run=args.dry_run,
        debug_dumper=args.debug_dumper,
        checkpoint_handler=args.checkpoint_handler,
    )

    # Finalize
    problem.finalize()


if __name__ == "__main__":
    from hysop_examples.argparser import HysopArgParser, colors

    class MultiResolutionScalarAdvectionArgParser(HysopArgParser):
        def __init__(self):
            prog_name = "multiresolution_scalar_advection"
            default_dump_dir = f"{HysopArgParser.tmp_dir()}/hysop_examples/{prog_name}"

            description = colors.color(
                "HySoP Scalar Advection Example: ", fg="blue", style="bold"
            )
            description += "Advect a scalar by a given constant velocity. "
            description += (
                "\n\nThe advection operator is directionally splitted resulting "
            )
            description += (
                "in the use of one or more advection-remesh operators per direction."
            )

            super().__init__(
                prog_name=prog_name,
                description=description,
                default_dump_dir=default_dump_dir,
            )

        def _add_main_args(self):
            args = super()._add_main_args()
            args.add_argument(
                "-vel",
                "--velocity",
                type=str,
                action=self.split,
                container=tuple,
                append=False,
                convert=float,
                dest="velocity",
                help="Velocity components.",
            )
            return args

        def _check_main_args(self, args):
            super()._check_main_args(args)
            self._check_default(args, "velocity", tuple, allow_none=False)

        def _setup_parameters(self, args):
            super()._setup_parameters(args)
            if len(args.velocity) == 1:
                args.velocity *= args.ndim

    parser = MultiResolutionScalarAdvectionArgParser()

    parser.set_defaults(
        box_origin=(0.0,),
        box_length=(2 * np.pi,),
        tstart=0.0,
        tend=2 * np.pi,
        npts=(16,),
        dump_freq=10,
        cfl=0.5,
        velocity=(1.0,),
        ndim=3,
        compute_precision="fp64",
    )

    parser.run(compute)
