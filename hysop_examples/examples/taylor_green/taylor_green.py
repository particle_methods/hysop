# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import numpy as np

pi = np.pi
cos = np.cos
sin = np.sin

# Function to compute initial vorticity


def init_vorticity(data, coords, component):
    # Ux = sin(x) * cos(y) * cos(z)
    # Uy = - cos(x) * sin(y) * cos(z)
    # Uz = 0
    # W = rot(U)
    (x, y, z) = coords
    if component == 0:
        data[...] = -cos(x) * sin(y) * sin(z)
    elif component == 1:
        data[...] = -sin(x) * cos(y) * sin(z)
    elif component == 2:
        data[...] = 2.0 * sin(x) * sin(y) * cos(z)
    else:
        raise NotImplementedError(component)
    # initial enstrophy is 6*pi^3
    # initial volume averaged enstrophy: 6*pi^3 / (2*pi)^3 = 0.75


def compute(args):
    from hysop import Box, Simulation, Problem, MPIParams, IO, IOParams, main_rank
    from hysop.defaults import (
        VelocityField,
        VorticityField,
        EnstrophyParameter,
        TimeParameters,
        ViscosityParameter,
    )
    from hysop.constants import (
        Implementation,
        AdvectionCriteria,
        StretchingCriteria,
        Backend,
    )

    from hysop.operators import (
        DirectionalAdvection,
        DirectionalStretching,
        StaticDirectionalStretching,
        Diffusion,
        PoissonCurl,
        AdaptiveTimeStep,
        HDF_Writer,
        Enstrophy,
        MinMaxFieldStatistics,
        StrangSplitting,
        ParameterPlotter,
        Advection,
        MinMaxGradientStatistics,
    )

    from hysop.methods import SpaceDiscretization, Remesh, TimeIntegrator, Interpolation

    # IO paths
    spectral_path = IO.default_path() + "/spectral"

    # Define the domain
    dim = args.ndim
    npts = args.npts
    box = Box(origin=args.box_origin, length=args.box_length, dim=dim)

    # Get default MPI Parameters from domain (even for serial jobs)
    mpi_params = MPIParams(comm=box.task_comm, task_id=box.current_task())

    # Setup usual implementation specific variables
    impl = args.impl
    extra_op_kwds = {"mpi_params": mpi_params}
    if impl in (Implementation.PYTHON, Implementation.FORTRAN):
        backend = Backend.HOST
        method = {}
    elif impl is Implementation.OPENCL:
        # For the OpenCL implementation we need to setup the compute device
        # and configure how the code is generated and compiled at runtime.

        # Create an explicit OpenCL context from user parameters
        from hysop.backend.device.opencl.opencl_tools import (
            get_or_create_opencl_env,
            get_device_number,
        )

        cl_env = get_or_create_opencl_env(
            mpi_params=mpi_params,
            platform_id=args.cl_platform_id,
            device_id=(
                box.machine_rank % get_device_number()
                if args.cl_device_id is None
                else None
            ),
        )

        # Configure OpenCL kernel generation and tuning (already done by HysopArgParser)
        from hysop.methods import OpenClKernelConfig

        method = {OpenClKernelConfig: args.opencl_kernel_config}

        # Setup opencl specific extra operator keyword arguments
        extra_op_kwds["cl_env"] = cl_env
        backend = Backend.OPENCL
    else:
        msg = f"Unknown implementation '{impl}'."
        raise ValueError(msg)

    # Define parameters and field (time, timestep, velocity, vorticity, enstrophy)
    t, dt = TimeParameters(dtype=args.dtype)
    velo = VelocityField(domain=box, dtype=args.dtype)
    vorti = VorticityField(velocity=velo)
    enstrophy = EnstrophyParameter(dtype=args.dtype)
    viscosity = ViscosityParameter(
        dtype=args.dtype, initial_value=(1.0 / args.Re), const=True
    )

    # Build the directional operators
    if impl is Implementation.FORTRAN:
        advec = Advection(
            implementation=impl,
            name="advec",
            velocity=velo,
            advected_fields=(vorti,),
            variables={velo: npts, vorti: npts},
            dt=dt,
            **extra_op_kwds,
        )
        advec_dir = None
    else:
        # > Directional advection
        advec = None
        advec_dir = DirectionalAdvection(
            implementation=impl,
            name="advec",
            velocity=velo,
            advected_fields=(vorti,),
            velocity_cfl=args.cfl,
            variables={velo: npts, vorti: npts},
            dt=dt,
            **extra_op_kwds,
        )
    # > Directional stretching
    if (impl is Implementation.PYTHON) or (impl is Implementation.FORTRAN):
        stretch_dir = StaticDirectionalStretching(
            implementation=Implementation.PYTHON,
            name="S",
            formulation=args.stretching_formulation,
            velocity=velo,
            vorticity=vorti,
            variables={velo: npts, vorti: npts},
            dt=dt,
            **extra_op_kwds,
        )
    else:
        stretch_dir = DirectionalStretching(
            implementation=impl,
            name="S",
            formulation=args.stretching_formulation,
            velocity=velo,
            vorticity=vorti,
            variables={velo: npts, vorti: npts},
            dt=dt,
            **extra_op_kwds,
        )

    # Build standard operators
    # > Poisson operator to recover the velocity from the vorticity
    poisson = PoissonCurl(
        name="poisson",
        velocity=velo,
        vorticity=vorti,
        variables={velo: npts, vorti: npts},
        projection=args.reprojection_frequency,
        diffusion=viscosity,
        dt=dt,
        # dump_energy=IOParams(filepath=spectral_path, filename='E_{fname}.txt', frequency=args.dump_freq),
        # plot_energy=IOParams(filepath=spectral_path, filename='E_{fname}_{ite}', frequency=args.dump_freq),
        # plot_input_vorticity_energy=-1,  # <= disable a specific plot
        # plot_output_vorticity_energy=-1, # <= disable a specific plot
        enforce_implementation=args.enforce_implementation,
        implementation=impl,
        **extra_op_kwds,
    )
    # > We ask to dump the outputs of this operator
    dump_fields = HDF_Writer(
        name="fields",
        io_params=args.io_params.clone(filename="fields"),
        force_backend=backend,
        variables={velo: npts, vorti: npts},
        **extra_op_kwds,
    )

    # > Operator to compute the infinite norm of the velocity
    if impl is Implementation.FORTRAN:
        impl = Implementation.PYTHON
    if args.variable_timestep:
        min_max_U = MinMaxFieldStatistics(
            name="min_max_U",
            field=velo,
            Finf=True,
            implementation=impl,
            variables={velo: npts},
            **extra_op_kwds,
        )
        min_max_gradU = MinMaxGradientStatistics(
            F=velo,
            Finf=True,
            implementation=impl,
            variables={velo: npts},
            **extra_op_kwds,
        )
        # > Operator to compute the infinite norm of the vorticity
        min_max_W = MinMaxFieldStatistics(
            name="min_max_W",
            field=vorti,
            Finf=True,
            implementation=impl,
            variables={vorti: npts},
            **extra_op_kwds,
        )
    else:
        min_max_U = min_max_gradU = min_max_W = None
    # > Operator to compute the enstrophy
    enstrophy_op = Enstrophy(
        name="enstrophy",
        vorticity=vorti,
        enstrophy=enstrophy,
        variables={vorti: npts},
        implementation=impl,
        **extra_op_kwds,
    )

    # > Directional splitting operator subgraph
    splitting = StrangSplitting(splitting_dim=dim, order=args.strang_order)
    splitting.push_operators(advec_dir, stretch_dir, min_max_gradU)

    # Adaptive timestep operator
    if args.variable_timestep:
        adapt_dt = AdaptiveTimeStep(dt, equivalent_CFL=True)
        dt_cfl = adapt_dt.push_cfl_criteria(
            cfl=args.cfl,
            dtype=dt.dtype,
            Fmin=min_max_U.Fmin,
            Fmax=min_max_U.Fmax,
            equivalent_CFL=True,
        )
        dt_stretch = adapt_dt.push_stretching_criteria(
            gradFinf=min_max_gradU.Finf,
            criteria=StretchingCriteria.GRAD_U,
            dtype=dt.dtype,
        )
        dt_lcfl0 = adapt_dt.push_advection_criteria(
            lcfl=args.lcfl,
            Finf=min_max_W.Finf,
            criteria=AdvectionCriteria.W_INF,
            name="LCFL0",
            dtype=dt.dtype,
        )
        dt_lcfl1 = adapt_dt.push_advection_criteria(
            lcfl=args.lcfl,
            gradFinf=min_max_gradU.Finf,
            criteria=AdvectionCriteria.GRAD_U,
            name="LCFL1",
            dtype=dt.dtype,
        )
        dt_lcfl2 = adapt_dt.push_advection_criteria(
            lcfl=args.lcfl,
            gradFinf=min_max_gradU.Finf,
            criteria=AdvectionCriteria.DEFORMATION,
            name="LCFL2",
            dtype=dt.dtype,
        )
    else:
        adapt_dt = None

    # > Custom operator to plot enstrophy
    if args.plot_enstrophy and (main_rank == args.visu_rank):

        class EnstrophyPlotter(ParameterPlotter):
            """Custom plotting operator for enstrophy."""

            def __init__(self, **kwds):
                import matplotlib.pyplot as plt

                if all(n == npts[0] for n in npts):
                    snpts = f"${npts[0]}^3$"
                else:
                    snpts = "x".join(str(n) for n in npts)
                tag = f"hysop-{snpts}"
                fig = plt.figure(figsize=(30, 18))
                axe0 = plt.subplot2grid((3, 2), (0, 0), rowspan=3, colspan=1)
                axe1 = plt.subplot2grid(
                    (3, 2), (0, 1), rowspan=2 + args.fixed_timestep, colspan=1
                )
                axe2 = (
                    plt.subplot2grid((3, 2), (2, 1), rowspan=1, colspan=1)
                    if args.variable_timestep
                    else None
                )
                axes = (axe0, axe1, axe2)
                parameters = {axe0: {tag: enstrophy}}
                if args.variable_timestep:
                    parameters[axe1] = {
                        dt_lcfl0.name: dt_lcfl0,
                        dt_lcfl1.name: dt_lcfl1,
                        dt_lcfl2.name: dt_lcfl2,
                        dt_cfl.name: dt_cfl,
                        dt_stretch.name: dt_stretch,
                    }
                    parameters[axe2] = {"CFL*": adapt_dt.equivalent_CFL}
                else:
                    parameters[axe1] = {dt.name: dt}

                super().__init__(
                    name="enstrophy_dt",
                    parameters=parameters,
                    fig=fig,
                    axes=axes,
                    **kwds,
                )
                config = "{}  {}  FD{}  PROJECTION_{}  {}".format(
                    args.time_integrator,
                    args.remesh_kernel,
                    args.fd_order,
                    args.reprojection_frequency,
                    args.strang_order,
                )
                fig = fig.suptitle(
                    "HySoP Taylor-Green Example {}\n{}".format(snpts, config),
                    fontweight="bold",
                )
                axe0.set_title("Integrated Enstrophy")
                axe0.set_xlabel("Non-dimensional time", fontweight="bold")
                axe0.set_ylabel(r"$\zeta$", rotation=0, fontweight="bold")
                axe0.set_xlim(args.tstart, args.tend)
                axe0.set_ylim(0, 26)
                if args.Re == 1600:
                    datadir = os.path.realpath(
                        os.path.join(os.getcwd(), os.path.dirname(__file__))
                    )
                    datadir += "/data"
                    for d in (512,):
                        reference = os.path.join(
                            datadir, "reference_{d}_{d}_{d}.txt".format(d=d)
                        )
                        data = np.loadtxt(reference, usecols=(0, 2), dtype=np.float32)
                        axe0.plot(
                            data[:, 0],
                            data[:, 1] * 2,
                            "--",
                            linewidth=1.0,
                            label="J.DeBonis-$512^3$",
                        )
                axe0.legend()
                axe1.set_title(f"Timesteps (CFL={args.cfl}, LCFL={args.lcfl})")
                axe1.set_xlabel("Non-dimensional time", fontweight="bold")
                axe1.set_ylabel("Non-dimensional time steps", fontweight="bold")
                axe1.set_xlim(args.tstart, args.tend)
                axe1.set_ylim(1e-4, 1e0)
                axe1.set_yscale("log")
                axe1.legend()
                if axe2:
                    axe2.set_title("Equivalent CFL")
                    axe2.set_xlabel("Non-dimensional time", fontweight="bold")
                    axe2.set_ylabel("CFL*", fontweight="bold")
                    axe2.set_xlim(args.tstart, args.tend)
                    axe2.axhline(y=args.cfl, color="r", linestyle="--")
                    axe2.set_ylim(0.0, 1.1 * args.cfl)

        plot = EnstrophyPlotter(
            update_frequency=args.plot_freq,
            visu_rank=args.visu_rank,
            io_params=args.io_params,
        )
    else:
        plot = None

    # Create the problem we want to solve and insert our
    # directional splitting subgraph and the standard operators.
    # The method dictionnary passed to this graph will be dispatched
    # accross all operators contained in the graph.
    method.update(
        {
            SpaceDiscretization: args.fd_order,
            TimeIntegrator: args.time_integrator,
            Remesh: args.remesh_kernel,
        }
    )
    problem = Problem(method=method)
    problem.insert(
        poisson,
        advec,
        splitting,
        min_max_U,
        min_max_W,
        enstrophy_op,
        adapt_dt,
        dump_fields,
        plot,
    )
    problem.build(args)

    # If a visu_rank was provided, and show_graph was set,
    # display the graph on the given process rank.
    if args.display_graph:
        problem.display(args.visu_rank)

    # Create a simulation
    # (do not forget to specify the t and dt parameters here)
    simu = Simulation(
        start=args.tstart,
        end=args.tend,
        nb_iter=args.nb_iter,
        max_iter=args.max_iter,
        dt0=args.dt,
        times_of_interest=args.times_of_interest,
        t=t,
        dt=dt,
    )
    params = (
        t,
        dt,
        enstrophy,
    )
    if args.variable_timestep:
        params += (
            dt_cfl,
            dt_stretch,
            dt_lcfl0,
            dt_lcfl1,
            dt_lcfl2,
            min_max_U.Finf,
            min_max_W.Finf,
            min_max_gradU.Finf,
            adapt_dt.equivalent_CFL,
        )
    simu.write_parameters(*params, filename="parameters.txt", precision=8)

    # Initialize only the vorticity
    problem.initialize_field(vorti, formula=init_vorticity)

    # Finally solve the problem
    problem.solve(
        simu,
        dry_run=args.dry_run,
        debug_dumper=args.debug_dumper,
        checkpoint_handler=args.checkpoint_handler,
    )

    # Finalize
    problem.finalize()


if __name__ == "__main__":
    from hysop_examples.argparser import HysopArgParser, colors

    class TaylorGreenArgParser(HysopArgParser):
        def __init__(self):
            prog_name = "taylor_green"
            default_dump_dir = "{}/hysop_examples/{}".format(
                HysopArgParser.tmp_dir(), prog_name
            )

            description = colors.color(
                "HySoP Taylor-Green Example: ", fg="blue", style="bold"
            )
            description += colors.color(
                "[Van Rees 2011] (first part)", fg="yellow", style="bold"
            )
            description += colors.color(
                "\nA comparison of vortex and pseudo-spectral methods "
                + "for the simulation of periodic vortical flows at high Reynolds numbers.",
                fg="yellow",
            )
            description += "\n"
            description += "\nThis example focuses on a validation study for the "
            description += "hybrid particle-mesh vortex method at Reynolds 1600 for "
            description += "the 3D Taylor-Green vortex."
            description += "\n"
            description += "\nSee the original paper at "
            description += (
                "http://vanreeslab.com/wp-content/papercite-data/pdf/rees-2011.pdf."
            )

            super().__init__(
                prog_name=prog_name,
                description=description,
                default_dump_dir=default_dump_dir,
            )

        def _add_main_args(self):
            args = super()._add_main_args()
            args.add_argument(
                "-Re",
                "--reynolds-number",
                type=float,
                dest="Re",
                help="Set the simulation Reynolds number.",
            )
            return args

        def _check_main_args(self, args):
            super()._check_main_args(args)
            self._check_default(args, "Re", float, allow_none=False)
            self._check_positive(args, "Re", strict=True, allow_none=False)

        def _add_graphical_io_args(self):
            graphical_io = super()._add_graphical_io_args()
            graphical_io.add_argument(
                "-pe",
                "--plot-enstrophy",
                action="store_true",
                dest="plot_enstrophy",
                help=(
                    "Plot the enstrophy component during simulation. "
                    + "Simulation will stop at each time of interest and "
                    + "the plot will be updated every specified freq iterations."
                ),
            )
            graphical_io.add_argument(
                "-pf",
                "--plot-freq",
                type=int,
                default=10,
                dest="plot_freq",
                help="Plotting update frequency in terms of iterations.",
            )

        def _check_file_io_args(self, args):
            super()._check_file_io_args(args)
            self._check_default(args, "plot_enstrophy", bool, allow_none=False)
            self._check_default(args, "plot_freq", int, allow_none=False)
            self._check_positive(args, "plot_freq", strict=True, allow_none=False)

        def _setup_parameters(self, args):
            super()._setup_parameters(args)
            if args.ndim != 3:
                msg = "This example only works for 3D domains."
                self.error(msg)

    parser = TaylorGreenArgParser()

    parser.set_defaults(
        impl="cl",
        ndim=3,
        npts=(64,),
        box_origin=(0.0,),
        box_length=(2 * pi,),
        tstart=0.0,
        tend=20.01,
        dt=1e-5,
        cfl=0.5,
        lcfl=0.125,
        dump_freq=100,
        dump_times=(),
        Re=1600.0,
    )

    parser.run(compute)
