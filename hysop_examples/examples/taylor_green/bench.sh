#!/usr/bin/env bash
##
## Copyright (c) HySoP 2011-2024
##
## This file is part of HySoP software.
## See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
## for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

EXAMPLE_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export MPLBACKEND='pdf'

echo "************************************"
echo "** Running Taylor-Green benchmark **"
echo "************************************"
echo
for d in 33 65 129 257; do
    echo "Discretization set to $dx$dx$d:"
    for integrator in "euler" "rk2" "rk4"; do
        echo " >integrator is $integrator"
        for order in 1 2; do
            echo "  *splitting order $order"
            dump_dir="$HOME/taylor_green_bench/taylor_green_$d/integrator_$integrator/splitting_$order"
            time_stats="$dump_dir/time.txt"
            time python "$EXAMPLE_ROOT/taylor_green.py" -VC -pe --discretization $d --time-integrator $integrator --directional-splitting-order $order --dump-dir="$dump_dir" --autotuner-verbose=5 --autotuner-max-candidates=32 --tee-ranks -1
            echo
        done
        echo
    done
    echo
done
