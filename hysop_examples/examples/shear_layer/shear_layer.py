# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sympy as sm
import numpy as np


def compute(args):
    from hysop import Box, Simulation, Problem, MPIParams, ScalarParameter
    from hysop.defaults import (
        VelocityField,
        VorticityField,
        TimeParameters,
        ViscosityParameter,
    )
    from hysop.constants import Implementation, AdvectionCriteria

    from hysop.operators import (
        DirectionalAdvection,
        Advection,
        PoissonCurl,
        AdaptiveTimeStep,
        FieldPlotter2D,
        MinMaxFieldStatistics,
        StrangSplitting,
    )

    from hysop.methods import (
        SpaceDiscretization,
        Remesh,
        TimeIntegrator,
        ComputeGranularity,
        Interpolation,
        Backend,
    )

    from hysop.symbolic import space_symbols

    msg = f"Parameters are: delta={args.delta}, rho={args.rho}, visco={args.nu}"

    # Define the domain
    dim = args.ndim
    npts = args.npts
    box = Box(origin=args.box_origin, length=args.box_length, dim=dim)

    # Get default MPI Parameters from domain (even for serial jobs)
    mpi_params = MPIParams(comm=box.task_comm, task_id=box.current_task())

    # Setup usual implementation specific variables
    impl = args.impl
    extra_op_kwds = {"mpi_params": mpi_params}
    if (impl is Implementation.PYTHON) or (impl is Implementation.FORTRAN):
        method = {}
    elif impl is Implementation.OPENCL:
        # For the OpenCL implementation we need to setup the compute device
        # and configure how the code is generated and compiled at runtime.

        # Create an explicit OpenCL context from user parameters
        from hysop.backend.device.opencl.opencl_tools import (
            get_or_create_opencl_env,
            get_device_number,
        )

        cl_env = get_or_create_opencl_env(
            mpi_params=mpi_params,
            platform_id=args.cl_platform_id,
            device_id=(
                box.machine_rank % get_device_number()
                if args.cl_device_id is None
                else None
            ),
        )

        # Configure OpenCL kernel generation and tuning (already done by HysopArgParser)
        from hysop.methods import OpenClKernelConfig

        method = {OpenClKernelConfig: args.opencl_kernel_config}

        # Setup opencl specific extra operator keyword arguments
        extra_op_kwds["cl_env"] = cl_env
    else:
        msg = f"Unknown implementation '{impl}'."
        raise ValueError(msg)

    # Get back user paramaters
    # rho:   thickness of the shear layers
    # delta: the strength of the initial perturbation
    rho = args.rho
    delta = args.delta

    # Compute initial vorticity fomula symbolically
    # and define the function to compute initial vorticity.
    (x, y) = space_symbols[:2]
    u = sm.tanh(rho * (0.25 - sm.sqrt((y - 0.5) * (y - 0.5))))
    v = delta * sm.sin(2 * sm.pi * x)
    w = v.diff(x) - u.diff(y)
    U0 = sm.utilities.lambdify((x, y), u)
    U1 = sm.utilities.lambdify((x, y), v)
    W0 = sm.utilities.lambdify((x, y), w)

    def init_velocity(data, coords, component):
        if component == 0:
            data[...] = U0(*coords)
        elif component == 1:
            data[...] = U1(*coords)
        else:
            raise NotImplementedError(component)

    def init_vorticity(data, coords, component):
        assert component == 0
        data[...] = W0(*coords)
        data[np.isnan(data)] = 0.0

    # Define parameters and field (time, timestep, viscosity, velocity, vorticity)
    t, dt = TimeParameters(dtype=args.dtype)
    velo = VelocityField(domain=box, dtype=args.dtype)
    vorti = VorticityField(velocity=velo)
    nu = ViscosityParameter(initial_value=args.nu, const=True, dtype=args.dtype)

    # Build the directional operators
    if (impl is Implementation.FORTRAN) and (dim == 3):
        # > Nd advection
        advec = Advection(
            implementation=impl,
            name="advec",
            velocity=velo,
            advected_fields=(vorti,),
            velocity_cfl=args.cfl,
            variables={velo: npts, vorti: npts},
            dt=dt,
            **extra_op_kwds,
        )
    else:
        # > Directional advection
        impl_advec = Implementation.PYTHON if (impl is Implementation.FORTRAN) else impl
        advec_dir = DirectionalAdvection(
            implementation=impl_advec,
            name="advection_remesh",
            velocity=velo,
            advected_fields=(vorti,),
            velocity_cfl=args.cfl,
            variables={velo: npts, vorti: npts},
            dt=dt,
            **extra_op_kwds,
        )
        # > Directional splitting operator subgraph
        advec = StrangSplitting(splitting_dim=dim, order=args.strang_order)
        advec.push_operators(advec_dir)

    # Build standard operators
    # > Poisson operator to recover the velocity from the vorticity
    poisson = PoissonCurl(
        name="poisson_curl",
        velocity=velo,
        vorticity=vorti,
        variables={velo: npts, vorti: npts},
        projection=args.reprojection_frequency,
        diffusion=nu,
        dt=dt,
        implementation=impl,
        enforce_implementation=args.enforce_implementation,
        **extra_op_kwds,
    )
    # > We ask to dump the inputs and the outputs of this operator
    poisson.dump_outputs(
        fields=(vorti,),
        io_params=args.io_params.clone(filename="vorti"),
        **extra_op_kwds,
    )
    poisson.dump_outputs(
        fields=(velo,), io_params=args.io_params.clone(filename="velo"), **extra_op_kwds
    )

    # > Operator to compute the infinite norm of the velocity
    if impl is Implementation.FORTRAN:
        impl = Implementation.PYTHON
    min_max_U = MinMaxFieldStatistics(
        name="min_max_U",
        field=velo,
        Finf=True,
        implementation=impl,
        variables={velo: npts},
        **extra_op_kwds,
    )
    # > Operator to compute the infinite norm of the vorticity
    min_max_W = MinMaxFieldStatistics(
        name="min_max_W",
        field=vorti,
        Finf=True,
        implementation=impl,
        variables={vorti: npts},
        **extra_op_kwds,
    )

    # Adaptive timestep operator
    adapt_dt = AdaptiveTimeStep(dt)
    adapt_dt.push_cfl_criteria(cfl=args.cfl, Finf=min_max_U.Finf)
    adapt_dt.push_advection_criteria(
        lcfl=args.lcfl, Finf=min_max_W.Finf, criteria=AdvectionCriteria.W_INF
    )

    # Field plotter
    if args.plot_vorticity:

        def fig_title(simulation):
            return f"Fields at t={simulation.time:2.2e}, iteration={simulation.current_iteration}, dt={simulation.time_step:2.2e}s"

        plot_fields = FieldPlotter2D(
            name="plot",
            figsize=(16, 6),
            shape=(1, 3),
            add_colorbars=True,
            symmetric_cbar=True,
            fields=(velo[0], velo[1], vorti),
            variables=npts,
            fig_title=fig_title,
            update_frequency=args.plot_freq,
            save_frequency=args.plot_freq,
            force_backend=Backend.OPENCL if impl is Implementation.OPENCL else None,
            **extra_op_kwds,
        )
    else:
        plot_fields = None

    # Create the problem we want to solve and insert our
    # directional splitting subgraph and the standard operators.
    # The method dictionnary passed to this graph will be dispatched
    # accross all operators contained in the graph.
    method.update(
        {
            ComputeGranularity: args.compute_granularity,
            SpaceDiscretization: args.fd_order,
            TimeIntegrator: args.time_integrator,
            Remesh: args.remesh_kernel,
        }
    )
    problem = Problem(method=method)
    problem.insert(poisson, advec, min_max_U, min_max_W, adapt_dt, plot_fields)
    problem.build(args)

    if plot_fields is not None:
        plot_fields.axes[0, 0].set_title("Velocity Ux")
        plot_fields.axes[0, 1].set_title("Velocity Uy")
        plot_fields.axes[0, 2].set_title("Vorticity ω")

    # If a visu_rank was provided, and show_graph was set,
    # display the graph on the given process rank.
    if args.display_graph:
        problem.display(args.visu_rank)

    # Create a simulation
    # (do not forget to specify the t and dt parameters here)
    simu = Simulation(
        start=args.tstart,
        end=args.tend,
        nb_iter=args.nb_iter,
        max_iter=args.max_iter,
        dt0=args.dt,
        times_of_interest=args.times_of_interest,
        t=t,
        dt=dt,
    )
    simu.write_parameters(t, dt, filename="parameters.txt", precision=4)

    # Initialize only the vorticity
    problem.initialize_field(velo, formula=init_velocity)
    problem.initialize_field(vorti, formula=init_vorticity)

    # Finally solve the problem
    problem.solve(
        simu,
        dry_run=args.dry_run,
        debug_dumper=args.debug_dumper,
        checkpoint_handler=args.checkpoint_handler,
        plot_freq=args.plot_freq,
    )

    # Finalize
    problem.finalize()


if __name__ == "__main__":
    from hysop_examples.argparser import HysopArgParser, colors

    class ShearLayerArgParser(HysopArgParser):
        def __init__(self):
            prog_name = "shear_layer"
            default_dump_dir = "{}/hysop_examples/{}".format(
                HysopArgParser.tmp_dir(), prog_name
            )

            description = colors.color(
                "HySoP Shear Layer Example: ", fg="blue", style="bold"
            )
            description += colors.color("[Brown 1995]", fg="yellow", style="bold")
            description += colors.color(
                "\nPerformance of under-resolved two dimensional "
                + "incrompressible flow simulation (Navier-Stokes 2D).",
                fg="yellow",
            )
            description += "\n"
            description += "\nThis example focuses on a doubly periodic shear layer to "
            description += "which a sinusoidal perturbation perpendicular to the "
            description += "orientation of the shear layers is made."
            description += "\n"
            description += "\nEach of the shear layers rolls up in a single vortex "
            description += "as the flow eolves."
            description += "\n"
            description += "\nDefault example parameters depends on the chosen case:"
            description += "\n  CASE     0        1        2"
            description += "\n  delta    0.5      0.5      0.5"
            description += "\n  rho      30       100      100"
            description += "\n  visco    1.0e-4   1.0e-4   0.5e-4"
            description += "\n  comment  thick    thin     thin"
            description += "\n"
            description += "\nSee the original paper at "
            description += "http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.27.7942&rep=rep1&type=pdf"

            super().__init__(
                prog_name=prog_name,
                description=description,
                default_dump_dir=default_dump_dir,
            )

        def _add_main_args(self):
            args = super()._add_main_args()
            args.add_argument(
                "-c",
                "--case",
                type=int,
                dest="case",
                help="Set (rho,delta,nu) to case-specific default values.",
            )
            args.add_argument(
                "-rho",
                "--layer-thickness",
                type=float,
                dest="rho",
                help="Initial thickness of the layers.",
            )
            args.add_argument(
                "-delta",
                "--initial-perturbation-stength",
                type=float,
                dest="delta",
                help="Initial perturbation strengh.",
            )
            args.add_argument(
                "-nu",
                "--viscosity",
                type=float,
                dest="nu",
                help="Viscosity coefficient.",
            )
            return args

        def _check_main_args(self, args):
            super()._check_main_args(args)
            self._check_default(args, "case", int, allow_none=False)
            self._check_default(args, "rho", float, allow_none=True)
            self._check_default(args, "delta", float, allow_none=True)
            self._check_default(args, "nu", float, allow_none=True)
            self._check_positive(
                args, ("rho", "delta", "nu"), strict=True, allow_none=True
            )

            if args.case not in (
                0,
                1,
                2,
            ):
                msg = "Case parameter should be 0, 1 or 2, got {}."
                msg = msg.format(args.case)
                self.error(msg)

        def _add_graphical_io_args(self):
            graphical_io = super()._add_graphical_io_args()
            graphical_io.add_argument(
                "-pw",
                "--plot-vorticity",
                action="store_true",
                dest="plot_vorticity",
                help=(
                    "Plot the vorticity component during simulation. "
                    + "Simulation will stop at each time of interest and "
                    + "the plot will be updated every specified freq iterations."
                ),
            )
            graphical_io.add_argument(
                "-pf",
                "--plot-freq",
                type=int,
                default=100,
                dest="plot_freq",
                help=(
                    "Plot frequency in terms of iterations."
                    + " Use 0 to disable frequency based plotting."
                ),
            )

        def _check_file_io_args(self, args):
            super()._check_file_io_args(args)
            self._check_default(args, "plot_vorticity", bool, allow_none=False)
            self._check_default(args, "plot_freq", int, allow_none=False)
            self._check_positive(args, "plot_freq", strict=False, allow_none=False)

        def _setup_parameters(self, args):
            super()._setup_parameters(args)
            from hysop.tools.htypes import first_not_None

            case = args.case

            delta_defaults = (0.5, 0.5, 0.5)
            rho_defaults = (30.0, 100.0, 100.0)
            nu_defaults = (1.0e-4, 0.5e-4, 0.25e-4)

            args.delta = first_not_None(args.delta, delta_defaults[case])
            args.rho = first_not_None(args.rho, rho_defaults[case])
            args.nu = first_not_None(args.nu, nu_defaults[case])

            self._check_positive(
                args, ("rho", "delta", "nu"), strict=True, allow_none=False
            )

            if args.ndim != 2:
                msg = "This example only works on 2D domains."
                self.error(msg)

    parser = ShearLayerArgParser()

    parser.set_defaults(
        impl="cl",
        ndim=2,
        npts=(256,),
        box_origin=(0.0,),
        box_length=(1.0,),
        tstart=0.0,
        tend=1.2,
        dt=1e-4,
        cfl=0.5,
        lcfl=0.125,
        case=0,
        dump_freq=0,
        dump_times=(0.8, 1.20),
    )

    parser.run(compute)
