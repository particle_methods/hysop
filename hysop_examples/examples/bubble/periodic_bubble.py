# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import numpy as np


def init_vorticity(data, **kwds):
    data[...] = 0.0


def init_velocity(data, **kwds):
    data[...] = 0.0


def init_rho(data, coords, Br, Bc, rho1, rho2, eps, component):
    assert component == 0
    # initialize density with the levelset
    init_phi(data=data, coords=coords, component=component, Br=Br, Bc=Bc)
    data[...] = regularize(data, rho1, rho2, eps)


def init_mu(data, coords, Br, Bc, mu1, mu2, eps, component):
    assert component == 0
    # initialize viscosity with the levelset
    init_phi(data=data, coords=coords, component=component, Br=Br, Bc=Bc)
    data[...] = regularize(data, mu1, mu2, eps)


def init_phi(data, coords, Br, Bc, component):
    assert component == 0
    assert len(Bc) == len(Br) >= 1
    phi = data
    phi[...] = np.inf
    Di = np.empty_like(phi)
    for C, R in zip(Bc, Br):
        Di[...] = 0
        for Xi, Ci in zip(coords, C):
            Li = 1.0
            Di += np.minimum((Xi - Ci - Li) ** 2, (Xi - Ci) ** 2, (Xi - Ci + Li) ** 2)
        Di -= R**2
        mask = np.abs(Di) < np.abs(phi)
        phi[mask] = Di[mask]
    assert np.isfinite(phi).all()


def regularize(x, y1, y2, eps):
    return y1 + (y2 - y1) * H_eps(x, eps)


def H_eps(x, eps):
    # regularized heavyside
    H = np.empty_like(x)
    H[np.where(x < -eps)] = 0.0
    H[np.where(x > +eps)] = 1.0

    ie = np.where(np.abs(x) <= eps)
    xe = x[ie]
    H[ie] = (xe + eps) / (2 * eps) + np.sin(np.pi * xe / eps) / (2 * np.pi)
    return H


def compute(args):
    from hysop import Box, Simulation, Problem, MPIParams, IOParams
    from hysop.defaults import (
        VelocityField,
        VorticityField,
        DensityField,
        ViscosityField,
        EnstrophyParameter,
        TimeParameters,
        VolumicIntegrationParameter,
    )
    from hysop.constants import Implementation, AdvectionCriteria

    from hysop.operators import (
        DirectionalAdvection,
        DirectionalDiffusion,
        DirectionalStretchingDiffusion,
        PoissonCurl,
        AdaptiveTimeStep,
        Enstrophy,
        MinMaxFieldStatistics,
        StrangSplitting,
        ParameterPlotter,
        Integrate,
        HDF_Writer,
        DirectionalSymbolic,
    )

    from hysop.methods import (
        SpaceDiscretization,
        Remesh,
        TimeIntegrator,
        ComputeGranularity,
        Interpolation,
    )

    # Define the domain
    dim = args.ndim
    npts = args.npts
    box = Box(origin=args.box_origin, length=args.box_length, dim=dim)

    # Get default MPI Parameters from domain (even for serial jobs)
    mpi_params = MPIParams(comm=box.task_comm, task_id=box.current_task())

    # Setup usual implementation specific variables
    impl = args.impl
    extra_op_kwds = {"mpi_params": mpi_params}
    if impl is Implementation.PYTHON:
        method = {}
    elif impl is Implementation.OPENCL:
        # For the OpenCL implementation we need to setup the compute device
        # and configure how the code is generated and compiled at runtime.

        # Create an explicit OpenCL context from user parameters
        from hysop.backend.device.opencl.opencl_tools import (
            get_or_create_opencl_env,
            get_device_number,
        )

        cl_env = get_or_create_opencl_env(
            mpi_params=mpi_params,
            platform_id=args.cl_platform_id,
            device_id=(
                box.machine_rank % get_device_number()
                if args.cl_device_id is None
                else None
            ),
        )

        # Configure OpenCL kernel generation and tuning (already done by HysopArgParser)
        from hysop.methods import OpenClKernelConfig

        method = {OpenClKernelConfig: args.opencl_kernel_config}

        # Setup opencl specific extra operator keyword arguments
        extra_op_kwds["cl_env"] = cl_env
    else:
        msg = f"Unknown implementation '{impl}'."
        raise ValueError(msg)

    # Define parameters and field (time, timestep, velocity, vorticity, enstrophy)
    t, dt = TimeParameters(dtype=args.dtype)
    velo = VelocityField(domain=box, dtype=args.dtype)
    vorti = VorticityField(velocity=velo)
    rho = DensityField(domain=box, dtype=args.dtype)
    mu = ViscosityField(domain=box, dtype=args.dtype, mu=True)

    enstrophy = EnstrophyParameter(dtype=args.dtype)
    rhov = VolumicIntegrationParameter(field=rho)
    muv = VolumicIntegrationParameter(field=mu)

    # Build the directional operators
    # > Directional advection
    advec = DirectionalAdvection(
        implementation=impl,
        name="advec",
        velocity=velo,
        advected_fields=(vorti, rho, mu),
        velocity_cfl=args.cfl,
        variables={velo: npts, vorti: npts, rho: npts, mu: npts},
        dt=dt,
        **extra_op_kwds,
    )
    # > Diffusion of rho and mu
    dx = 1.0 / float(max(npts))
    nu = 20 * dx**2
    nu = (nu, nu)
    diffuse = DirectionalDiffusion(
        implementation=impl,
        name="diffuse",
        pretty_name="diff",
        coeffs=nu,
        fields=(rho, mu),
        variables={rho: npts, mu: npts},
        dt=dt,
        **extra_op_kwds,
    )
    # > Directional stretching + diffusion
    if dim == 3:
        stretch_diffuse = DirectionalStretchingDiffusion(
            implementation=impl,
            name="stretch_diffuse",
            pretty_name="sdiff",
            formulation=args.stretching_formulation,
            viscosity=mu,
            velocity=velo,
            vorticity=vorti,
            variables={velo: npts, vorti: npts, mu: npts},
            dt=dt,
            **extra_op_kwds,
        )
    elif dim == 2:
        stretch_diffuse = DirectionalDiffusion(
            implementation=impl,
            name=f"diffuse_{vorti.name}",
            pretty_name=f"diff{vorti.pretty_name}",
            coeffs=mu,
            fields=vorti,
            variables={vorti: npts, mu: npts},
            dt=dt,
            **extra_op_kwds,
        )
    else:
        msg = f"Unsupported dimension {dim}."
        raise RuntimeError(msg)

    # > External force rot(-rho*g)
    from hysop.symbolic import space_symbols
    from hysop.symbolic.base import SymbolicTensor
    from hysop.symbolic.field import curl
    from hysop.symbolic.relational import Assignment

    frame = rho.domain.frame
    rhos = rho.s(*frame.vars)
    Ws = vorti.s(*frame.vars)
    Fext = np.zeros(shape=(dim,), dtype=object).view(SymbolicTensor)
    Fext[1] = -1.0  # -9.8196
    Fext *= rhos
    lhs = Ws.diff(frame.time)
    rhs = curl(Fext, frame)
    exprs = Assignment.assign(lhs, rhs)
    external_force = DirectionalSymbolic(
        name="Fext",
        implementation=impl,
        exprs=exprs,
        dt=dt,
        variables={vorti: npts, rho: npts},
        **extra_op_kwds,
    )

    # > Directional splitting operator subgraph
    splitting = StrangSplitting(splitting_dim=dim, order=args.strang_order)
    splitting.push_operators(advec, diffuse, stretch_diffuse, external_force)

    # Build standard operators
    # > Poisson operator to recover the velocity from the vorticity
    poisson = PoissonCurl(
        name="poisson",
        velocity=velo,
        vorticity=vorti,
        variables={velo: npts, vorti: npts},
        projection=args.reprojection_frequency,
        implementation=impl,
        **extra_op_kwds,
    )

    # > Operators to dump rho and mu
    io_params = IOParams(filename="fields", frequency=args.dump_freq)
    dump_fields = HDF_Writer(
        name="dump",
        io_params=io_params,
        variables={velo: npts, vorti: npts, rho: npts, mu: npts},
    )

    # > Operator to compute the infinite norm of the velocity
    min_max_U = MinMaxFieldStatistics(
        field=velo,
        Finf=True,
        implementation=impl,
        variables={velo: npts},
        **extra_op_kwds,
    )
    # > Operator to compute the infinite norm of the vorticity
    min_max_W = MinMaxFieldStatistics(
        field=vorti,
        Finf=True,
        implementation=impl,
        variables={vorti: npts},
        **extra_op_kwds,
    )

    # > Operator to track min and max density and viscosity
    min_max_rho = MinMaxFieldStatistics(
        field=rho,
        Fmin=True,
        Fmax=True,
        implementation=impl,
        variables={rho: npts},
        **extra_op_kwds,
    )
    min_max_mu = MinMaxFieldStatistics(
        field=mu,
        Fmin=True,
        Fmax=True,
        implementation=impl,
        variables={mu: npts},
        **extra_op_kwds,
    )

    # > Operators to compute the integrated enstrophy, density and viscosity
    integrate_enstrophy = Enstrophy(
        name="enstrophy",
        vorticity=vorti,
        enstrophy=enstrophy,
        variables={vorti: npts},
        implementation=impl,
        **extra_op_kwds,
    )
    integrate_rho = Integrate(
        name="integrate_rho",
        field=rho,
        variables={rho: npts},
        parameter=rhov,
        scaling="normalize",
        implementation=impl,
        **extra_op_kwds,
    )
    integrate_mu = Integrate(
        name="integrate_mu",
        field=mu,
        variables={mu: npts},
        parameter=muv,
        scaling="normalize",
        implementation=impl,
        **extra_op_kwds,
    )

    # Adaptive timestep operator
    adapt_dt = AdaptiveTimeStep(dt, equivalent_CFL=True, max_dt=1e-3)
    dt_cfl = adapt_dt.push_cfl_criteria(
        cfl=args.cfl, Finf=min_max_U.Finf, equivalent_CFL=True
    )
    dt_advec = adapt_dt.push_advection_criteria(
        lcfl=args.lcfl, Finf=min_max_W.Finf, criteria=AdvectionCriteria.W_INF
    )

    # Create the problem we want to solve and insert our
    # directional splitting subgraph and the standard operators.
    # The method dictionnary passed to this graph will be dispatched
    # accross all operators contained in the graph.
    method.update(
        {
            ComputeGranularity: args.compute_granularity,
            SpaceDiscretization: args.fd_order,
            TimeIntegrator: args.time_integrator,
            Remesh: args.remesh_kernel,
        }
    )
    problem = Problem(method=method)
    problem.insert(
        poisson,
        dump_fields,
        splitting,
        # integrate_enstrophy, integrate_rho, integrate_mu,
        min_max_rho,
        min_max_mu,
        min_max_U,
        min_max_W,
        adapt_dt,
    )
    problem.build(args)

    # If a visu_rank was provided, and show_graph was set,
    # display the graph on the given process rank.
    if args.display_graph:
        problem.display(args.visu_rank)

    # Create a simulation
    # (do not forget to specify the t and dt parameters here)
    simu = Simulation(
        start=args.tstart,
        end=args.tend,
        nb_iter=args.nb_iter,
        max_iter=args.max_iter,
        dt0=args.dt,
        times_of_interest=args.times_of_interest,
        t=t,
        dt=dt,
    )
    simu.write_parameters(
        t,
        dt_cfl,
        dt_advec,
        dt,
        enstrophy,
        rhov,
        muv,
        min_max_U.Finf,
        min_max_W.Finf,
        min_max_rho.Fmin,
        min_max_rho.Fmax,
        min_max_mu.Fmin,
        min_max_mu.Fmax,
        adapt_dt.equivalent_CFL,
        filename="parameters.txt",
        precision=8,
    )

    # Initialize vorticity, velocity, viscosity and density on all topologies
    Bc, Br = args.Bc, args.Br
    dx = np.max(np.divide(box.length, np.asarray(args.npts) - 1))
    eps = 2.5 * dx
    problem.initialize_field(field=velo, formula=init_velocity)
    problem.initialize_field(field=vorti, formula=init_vorticity)
    problem.initialize_field(
        field=rho,
        formula=init_rho,
        rho1=args.rho1,
        rho2=args.rho2,
        Bc=Bc,
        Br=Br,
        reorder="Bc",
        eps=eps,
    )
    problem.initialize_field(
        field=mu,
        formula=init_mu,
        mu1=args.mu1,
        mu2=args.mu2,
        Bc=Bc,
        Br=Br,
        reorder="Bc",
        eps=eps,
    )

    # Finally solve the problem
    problem.solve(
        simu,
        dry_run=args.dry_run,
        debug_dumper=args.debug_dumper,
        checkpoint_handler=args.checkpoint_handler,
    )

    # Finalize
    problem.finalize()


if __name__ == "__main__":
    from hysop_examples.argparser import HysopArgParser, colors

    class PeriodicBubbleArgParser(HysopArgParser):
        def __init__(self):
            prog_name = "periodic_bubble"
            default_dump_dir = "{}/hysop_examples/{}".format(
                HysopArgParser.tmp_dir(), prog_name
            )

            description = colors.color(
                "HySoP Periodic Bubble Example: ", fg="blue", style="bold"
            )
            description += colors.color(
                "[Osher 1995] (first part)", fg="yellow", style="bold"
            )
            description += colors.color(
                "\nA Level Set Formulation of Eulerian Interface Capturing Methods for "
                + "Incompressible Fluid flows.",
                fg="yellow",
            )
            description += "\n"
            description += "\nThis example focuses on a validation study for the "
            description += "hybrid particle-mesh vortex method for varying densities without using a levelset function."

            super().__init__(
                prog_name=prog_name,
                description=description,
                default_dump_dir=default_dump_dir,
            )

        def _add_main_args(self):
            args = super()._add_main_args()
            args.add_argument(
                "-rho1",
                "--bubble-density",
                type=float,
                dest="rho1",
                help="Set the density of the bubbles.",
            )
            args.add_argument(
                "-rho2",
                "--fluid-density",
                type=float,
                dest="rho2",
                help="Set the density of the fluid surrounding the bubbles.",
            )
            args.add_argument(
                "-mu1",
                "--bubble-viscosity",
                type=float,
                dest="mu1",
                help="Set the viscosity of the bubbles.",
            )
            args.add_argument(
                "-mu2",
                "--fluid-viscosity",
                type=float,
                dest="mu2",
                help="Set the viscosity of the fluid surrounding the bubbles.",
            )
            args.add_argument(
                "-bp",
                "--bubble-positions",
                type=float,
                dest="Bc",
                container=tuple,
                action=self.split,
                convert=tuple,
                help="Set the initial center of the bubbels.",
            )
            args.add_argument(
                "-br",
                "--bubble-radi",
                type=float,
                dest="Br",
                container=tuple,
                action=self.split,
                convert=float,
                help="Set the initial radius of the bubbels.",
            )
            return args

        def _check_main_args(self, args):
            super()._check_main_args(args)
            vars_ = ("rho1", "rho2", "mu1", "mu2")
            self._check_default(args, vars_, float, allow_none=False)
            self._check_positive(args, vars_, strict=False, allow_none=False)
            self._check_default(args, ("Bc", "Br"), tuple, allow_none=False)

            Bc, Br = args.Bc, args.Br
            if len(Bc) != len(Br):
                msg = "Specified {} bubble positions and {} bubble radi."
                self.error(msg)
            for bc, br in zip(Bc, Br):
                if not isinstance(bc, tuple):
                    msg = "Specified bubble center is not a tuple, got {}."
                    msg = msg.format(bc, type(bc).__name__)
                    self.error(msg)
                if not isinstance(br, float):
                    msg = "Specified bubble radius is not a float, got {} which is of type {}."
                    msg = msg.format(br, type(br).__name__)
                    self.error(msg)

        def _add_graphical_io_args(self):
            graphical_io = super()._add_graphical_io_args()
            graphical_io.add_argument(
                "-pp",
                "--plot-parameters",
                action="store_true",
                dest="plot_parameters",
                help=(
                    "Plot the density and viscosity integrals during simulation. "
                    + "Simulation will stop at each time of interest and "
                    + "the plot will be updated every specified freq iterations."
                ),
            )
            graphical_io.add_argument(
                "-pf",
                "--plot-freq",
                type=int,
                default=100,
                dest="plot_freq",
                help="Plotting update frequency in terms of iterations.",
            )

        def _check_file_io_args(self, args):
            super()._check_file_io_args(args)
            self._check_default(args, "plot_parameters", bool, allow_none=False)
            self._check_default(args, "plot_freq", int, allow_none=False)
            self._check_positive(args, "plot_freq", strict=True, allow_none=False)

        def _setup_parameters(self, args):
            super()._setup_parameters(args)
            dim = args.ndim
            if dim not in (2, 3):
                msg = "Domain should be 2D or 3D."
                self.error(msg)
            Bc = tuple(bc[:dim] for bc in args.Bc)
            args.Bc = Bc

    parser = PeriodicBubbleArgParser()

    parser.set_defaults(
        impl="cl",
        ndim=2,
        npts=(256,),
        box_origin=(0.0,),
        box_length=(1.0,),
        tstart=0.0,
        tend=0.51,
        dt=1e-5,
        cfl=0.5,
        lcfl=0.125,
        dump_freq=10,
        dump_times=(0.0, 0.1, 0.20, 0.30, 0.325, 0.4, 0.45, 0.50),
        rho1=1.0,
        rho2=10.0,
        mu1=0.00025,
        mu2=0.00050,
        Bc=(
            (0.5, 0.15, 0.5),
            (0.5, 0.45, 0.5),
        ),
        Br=(
            0.1,
            0.15,
        ),
    )

    parser.run(compute)
