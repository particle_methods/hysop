# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import numpy as np


def init_vorticity(data, **kwds):
    data[...] = 0.0


def init_velocity(data, **kwds):
    data[...] = 0.0


def init_rho(data, **kwds):
    data[...] = 0.0


def init_mu(data, **kwds):
    data[...] = 0.0


def init_phi(data, coords, Br, Bc, component):
    assert component == 0
    assert len(Bc) == len(Br) >= 1
    phi = data
    phi[...] = np.inf
    Di = np.empty_like(phi)
    for C, R in zip(Bc, Br):
        Di[...] = 0
        for Xi, Ci in zip(coords, C):
            Li = 1.0
            Di += np.minimum((Xi - Ci - Li) ** 2, (Xi - Ci) ** 2, (Xi - Ci + Li) ** 2)
        Di -= R**2
        mask = np.abs(Di) < np.abs(phi)
        phi[mask] = Di[mask]
    assert np.isfinite(phi).all()


def init_lambda(data, coords, component):
    assert component == 0
    X, Y = coords[:2]
    data[...] = abs(X - 0.5) < 0.1
    data[...] *= abs(Y - 0.7) < 0.2
    data[...] *= 1.0e8


def compute(args):
    from hysop import Box, Simulation, Problem, MPIParams, IOParams, vprint
    from hysop.defaults import (
        VelocityField,
        VorticityField,
        DensityField,
        ViscosityField,
        LevelSetField,
        PenalizationField,
        EnstrophyParameter,
        TimeParameters,
        VolumicIntegrationParameter,
    )
    from hysop.constants import Implementation, AdvectionCriteria

    from hysop.operators import (
        DirectionalAdvection,
        DirectionalDiffusion,
        DirectionalStretchingDiffusion,
        PoissonCurl,
        AdaptiveTimeStep,
        Enstrophy,
        MinMaxFieldStatistics,
        StrangSplitting,
        ParameterPlotter,
        Integrate,
        HDF_Writer,
        DirectionalSymbolic,
    )

    from hysop.methods import (
        SpaceDiscretization,
        Remesh,
        TimeIntegrator,
        ComputeGranularity,
        Interpolation,
    )
    from hysop.numerics.odesolvers.runge_kutta import Euler, RK2, RK3, RK4

    from hysop.symbolic import sm, space_symbols, local_indices_symbols
    from hysop.symbolic.base import SymbolicTensor
    from hysop.symbolic.field import curl
    from hysop.symbolic.relational import Assignment, LogicalGT, LogicalLT, LogicalAND
    from hysop.symbolic.misc import Select
    from hysop.symbolic.tmp import TmpScalar
    from hysop.tools.string_utils import framed_str

    # Define the domain
    dim = args.ndim
    npts = args.npts
    box = Box(origin=args.box_origin, length=args.box_length, dim=dim)

    # Get default MPI Parameters from domain (even for serial jobs)
    mpi_params = MPIParams(comm=box.task_comm, task_id=box.current_task())

    # Setup usual implementation specific variables
    impl = args.impl
    extra_op_kwds = {"mpi_params": mpi_params}
    if impl is Implementation.PYTHON:
        method = {}
    elif impl is Implementation.OPENCL:
        # For the OpenCL implementation we need to setup the compute device
        # and configure how the code is generated and compiled at runtime.

        # Create an explicit OpenCL context from user parameters
        from hysop.backend.device.opencl.opencl_tools import (
            get_or_create_opencl_env,
            get_device_number,
        )

        cl_env = get_or_create_opencl_env(
            mpi_params=mpi_params,
            platform_id=args.cl_platform_id,
            device_id=(
                box.machine_rank % get_device_number()
                if args.cl_device_id is None
                else None
            ),
        )

        # Configure OpenCL kernel generation and tuning (already done by HysopArgParser)
        from hysop.methods import OpenClKernelConfig

        method = {OpenClKernelConfig: args.opencl_kernel_config}

        # Setup opencl specific extra operator keyword arguments
        extra_op_kwds["cl_env"] = cl_env
    else:
        msg = f"Unknown implementation '{impl}'."
        raise ValueError(msg)

    # Define parameters and field (time, timestep, velocity, vorticity, enstrophy)
    t, dt = TimeParameters(dtype=args.dtype)
    velo = VelocityField(domain=box, dtype=args.dtype)
    vorti = VorticityField(velocity=velo)
    phi = LevelSetField(domain=box, dtype=args.dtype)
    _lambda = PenalizationField(domain=box, dtype=args.dtype)
    rho = DensityField(domain=box, dtype=args.dtype)
    mu = ViscosityField(domain=box, dtype=args.dtype, mu=True)

    enstrophy = EnstrophyParameter(dtype=args.dtype)
    rhov = VolumicIntegrationParameter(field=rho)
    muv = VolumicIntegrationParameter(field=mu)

    # Symbolic fields
    frame = rho.domain.frame
    phis = phi.s(*frame.vars)
    rhos = rho.s(*frame.vars)
    mus = mu.s(*frame.vars)
    Us = velo.s(*frame.vars)
    Ws = vorti.s(*frame.vars)
    lambdas = _lambda.s(*frame.vars)
    dts = dt.s

    # Build the directional operators
    # > Directional penalization
    penalization = -dts * lambdas * Us / (1 + lambdas * dts)
    penalization = penalization.freeze()
    lhs = Ws
    rhs = curl(penalization, frame)
    exprs = Assignment.assign(lhs, rhs)
    penalization = DirectionalSymbolic(
        name="penalization",
        implementation=impl,
        exprs=exprs,
        fixed_residue=Ws,
        variables={vorti: npts, velo: npts, _lambda: npts},
        method={TimeIntegrator: Euler},
        dt=dt,
        **extra_op_kwds,
    )
    # > Directional advection
    advec = DirectionalAdvection(
        implementation=impl,
        name="advection",
        pretty_name="Adv",
        velocity=velo,
        advected_fields=(vorti, phi),
        velocity_cfl=args.cfl,
        variables={velo: npts, vorti: npts, phi: npts},
        dt=dt,
        **extra_op_kwds,
    )
    # > Recompute density and viscosity from levelset
    dx = np.max(np.divide(box.length, np.asarray(args.npts) - 1))
    rho1, rho2 = args.rho1, args.rho2
    mu1, mu2 = args.mu1, args.mu2
    pi = TmpScalar(name="pi", dtype=args.dtype)
    eps = TmpScalar(name="eps", dtype=args.dtype)
    x = TmpScalar(name="x", dtype=args.dtype)
    H = TmpScalar(name="H", dtype=args.dtype)
    smooth_cond = LogicalLT(sm.Abs(x), eps)
    pos_cond = LogicalGT(x, 0)
    clamp = Select(0.0, 1.0, pos_cond)
    smooth = (x + eps) / (2 * eps) + sm.sin(pi * x / eps) / (2 * pi)
    H_eps = Select(clamp, smooth, smooth_cond)
    e0 = Assignment(pi, np.pi)
    e1 = Assignment(eps, 2.5 * dx)
    e2 = Assignment(x, phis)
    e3 = Assignment(H, H_eps)
    e4 = Assignment(rhos, rho1 + (rho2 - rho1) * H)
    e5 = Assignment(mus, mu1 + (mu2 - mu1) * H)
    exprs = (e0, e1, e2, e3, e4, e5)
    eval_fields = DirectionalSymbolic(
        name="eval_fields",
        pretty_name="{}({},{})".format(
            phi.pretty_name, rho.pretty_name, mu.pretty_name
        ),
        no_split=True,
        implementation=impl,
        exprs=exprs,
        dt=dt,
        variables={phi: npts, mu: npts, rho: npts},
        **extra_op_kwds,
    )
    # > Directional stretching + diffusion
    if dim == 3:
        stretch_diffuse = DirectionalStretchingDiffusion(
            implementation=impl,
            name="stretch_diffuse",
            pretty_name=f"SD{vorti.pretty_name}",
            formulation=args.stretching_formulation,
            viscosity=mu,
            velocity=velo,
            vorticity=vorti,
            variables={velo: npts, vorti: npts, mu: npts},
            dt=dt,
            **extra_op_kwds,
        )
    elif dim == 2:
        stretch_diffuse = DirectionalDiffusion(
            implementation=impl,
            name=f"diffuse_{vorti.name}",
            pretty_name=f"D{vorti.pretty_name}",
            coeffs=mu,
            fields=vorti,
            variables={vorti: npts, mu: npts},
            dt=dt,
            **extra_op_kwds,
        )
    else:
        msg = f"Unsupported dimension {dim}."
        raise RuntimeError(msg)

    # > External force rot(-rho*g)
    Fext = np.zeros(shape=(dim,), dtype=object).view(SymbolicTensor)
    Fext[1] = -1.0  # -9.8196
    Fext *= rhos
    lhs = Ws.diff(frame.time)
    rhs = curl(Fext, frame)
    exprs = Assignment.assign(lhs, rhs)
    external_force = DirectionalSymbolic(
        name="Fext",
        implementation=impl,
        exprs=exprs,
        dt=dt,
        variables={vorti: npts, rho: npts},
        **extra_op_kwds,
    )

    # > Directional splitting operator subgraph
    splitting = StrangSplitting(splitting_dim=dim, order=args.strang_order)
    splitting.push_operators(
        penalization, advec, eval_fields, stretch_diffuse, external_force
    )

    # Build standard operators
    # > Poisson operator to recover the velocity from the vorticity
    poisson = PoissonCurl(
        name="poisson",
        velocity=velo,
        vorticity=vorti,
        variables={velo: npts, vorti: npts},
        projection=args.reprojection_frequency,
        implementation=impl,
        **extra_op_kwds,
    )

    # > Operators to dump rho and mu
    io_params = IOParams(filename="fields", frequency=args.dump_freq)
    dump_fields = HDF_Writer(
        name="dump",
        io_params=io_params,
        variables={  # velo: npts,
            vorti: npts,
            # phi: npts,
            rho: npts,
            # mu: npts,
        },
    )
    io_params = IOParams(filename="lambda", frequency=0)
    dump_lambda = HDF_Writer(
        name="dump_lambda", io_params=io_params, variables={_lambda: npts}
    )

    # > Operator to compute the infinite norm of the velocity
    min_max_U = MinMaxFieldStatistics(
        field=velo,
        Finf=True,
        implementation=impl,
        variables={velo: npts},
        **extra_op_kwds,
    )
    # > Operator to compute the infinite norm of the vorticity
    min_max_W = MinMaxFieldStatistics(
        field=vorti,
        Finf=True,
        implementation=impl,
        variables={vorti: npts},
        **extra_op_kwds,
    )

    # > Operators to compute the integrated enstrophy, density and viscosity
    integrate_enstrophy = Enstrophy(
        vorticity=vorti,
        enstrophy=enstrophy,
        variables={vorti: npts},
        implementation=impl,
        **extra_op_kwds,
    )
    integrate_rho = Integrate(
        field=rho,
        variables={rho: npts},
        parameter=rhov,
        scaling="normalize",
        implementation=impl,
        **extra_op_kwds,
    )
    integrate_mu = Integrate(
        field=mu,
        variables={mu: npts},
        parameter=muv,
        scaling="normalize",
        implementation=impl,
        **extra_op_kwds,
    )

    # Adaptive timestep operator
    dx = np.min(np.divide(box.length, np.asarray(npts) - 1))
    W0_dt = 0.5 * (dx**2) / mu1
    W1_dt = 0.5 * (dx**2) / mu2
    msg = "W0_dt={}, W1_dt={}"
    msg = msg.format(W0_dt, W1_dt)
    msg = "\n" + framed_str(" DIFFUSION STABILITY CRITERIA ", msg)
    vprint(msg)
    max_dt = min(W0_dt, W1_dt)
    adapt_dt = AdaptiveTimeStep(
        dt,
        equivalent_CFL=True,
        max_dt=max_dt,
        name="merge_dt",
        pretty_name="dt",
    )
    dt_cfl = adapt_dt.push_cfl_criteria(
        cfl=args.cfl,
        Fmin=min_max_U.Fmin,
        Fmax=min_max_U.Fmax,
        equivalent_CFL=True,
        name="dt_cfl",
        pretty_name="CFL",
    )
    dt_advec = adapt_dt.push_advection_criteria(
        lcfl=args.lcfl,
        Finf=min_max_W.Finf,
        criteria=AdvectionCriteria.W_INF,
        name="dt_lcfl",
        pretty_name="LCFL",
    )

    # Create the problem we want to solve and insert our
    # directional splitting subgraph and the standard operators.
    # The method dictionnary passed to this graph will be dispatched
    # accross all operators contained in the graph.
    method.update(
        {
            ComputeGranularity: args.compute_granularity,
            SpaceDiscretization: args.fd_order,
            TimeIntegrator: args.time_integrator,
            Remesh: args.remesh_kernel,
        }
    )
    problem = Problem(method=method)
    problem.insert(
        poisson,
        splitting,
        dump_fields,
        dump_lambda,
        integrate_enstrophy,
        integrate_rho,
        integrate_mu,
        min_max_U,
        min_max_W,
        adapt_dt,
    )
    problem.build(args)

    # If a visu_rank was provided, and show_graph was set,
    # display the graph on the given process rank.
    if args.display_graph:
        problem.display(args.visu_rank)

    # Create a simulation
    # (do not forget to specify the t and dt parameters here)
    simu = Simulation(
        start=args.tstart,
        end=args.tend,
        nb_iter=args.nb_iter,
        max_iter=args.max_iter,
        dt0=args.dt,
        times_of_interest=args.times_of_interest,
        t=t,
        dt=dt,
    )
    simu.write_parameters(
        t,
        dt_cfl,
        dt_advec,
        dt,
        enstrophy,
        rhov,
        muv,
        min_max_U.Finf,
        min_max_W.Finf,
        adapt_dt.equivalent_CFL,
        filename="parameters.txt",
        precision=8,
    )

    # Initialize vorticity, velocity, viscosity and density on all topologies
    Bc, Br = args.Bc, args.Br
    problem.initialize_field(field=velo, formula=init_velocity)
    problem.initialize_field(field=vorti, formula=init_vorticity)
    problem.initialize_field(field=rho, formula=init_rho)
    problem.initialize_field(field=mu, formula=init_mu)
    problem.initialize_field(field=phi, formula=init_phi, Bc=Bc, Br=Br, reorder="Bc")
    problem.initialize_field(field=_lambda, formula=init_lambda)

    # Finally solve the problem
    problem.solve(
        simu,
        dry_run=args.dry_run,
        debug_dumper=args.debug_dumper,
        checkpoint_handler=args.checkpoint_handler,
    )

    # Finalize
    problem.finalize()


if __name__ == "__main__":
    from hysop_examples.argparser import HysopArgParser, colors

    class PeriodicBubbleArgParser(HysopArgParser):
        def __init__(self):
            prog_name = "periodic_bubble_levelset_penalization"
            default_dump_dir = "{}/hysop_examples/{}".format(
                HysopArgParser.tmp_dir(), prog_name
            )

            description = colors.color(
                "HySoP Periodic Bubble Example: ", fg="blue", style="bold"
            )
            description += colors.color(
                "[Osher 1995] (first part)", fg="yellow", style="bold"
            )
            description += colors.color(
                "\nA Level Set Formulation of Eulerian Interface Capturing Methods for "
                + "Incompressible Fluid flows.",
                fg="yellow",
            )
            description += "\n"
            description += "\nThis example focuses on a validation study for the "
            description += "hybrid particle-mesh vortex method for varying densities without using a levelset function."

            super().__init__(
                prog_name=prog_name,
                description=description,
                default_dump_dir=default_dump_dir,
            )

        def _add_main_args(self):
            args = super()._add_main_args()
            args.add_argument(
                "-rho1",
                "--bubble-density",
                type=float,
                dest="rho1",
                help="Set the density of the bubbles.",
            )
            args.add_argument(
                "-rho2",
                "--fluid-density",
                type=float,
                dest="rho2",
                help="Set the density of the fluid surrounding the bubbles.",
            )
            args.add_argument(
                "-mu1",
                "--bubble-viscosity",
                type=float,
                dest="mu1",
                help="Set the viscosity of the bubbles.",
            )
            args.add_argument(
                "-mu2",
                "--fluid-viscosity",
                type=float,
                dest="mu2",
                help="Set the viscosity of the fluid surrounding the bubbles.",
            )
            args.add_argument(
                "-bp",
                "--bubble-positions",
                type=float,
                dest="Bc",
                container=tuple,
                action=self.split,
                convert=tuple,
                help="Set the initial center of the bubbels.",
            )
            args.add_argument(
                "-br",
                "--bubble-radi",
                type=float,
                dest="Br",
                container=tuple,
                action=self.split,
                convert=float,
                help="Set the initial radius of the bubbels.",
            )
            return args

        def _check_main_args(self, args):
            super()._check_main_args(args)
            vars_ = ("rho1", "rho2", "mu1", "mu2")
            self._check_default(args, vars_, float, allow_none=False)
            self._check_positive(args, vars_, strict=False, allow_none=False)
            self._check_default(args, ("Bc", "Br"), tuple, allow_none=False)

            Bc, Br = args.Bc, args.Br
            if len(Bc) != len(Br):
                msg = "Specified {} bubble positions and {} bubble radi."
                self.error(msg)
            for bc, br in zip(Bc, Br):
                if not isinstance(bc, tuple):
                    msg = "Specified bubble center is not a tuple, got {}."
                    msg = msg.format(bc, type(bc).__name__)
                    self.error(msg)
                if not isinstance(br, float):
                    msg = "Specified bubble radius is not a float, got {} which is of type {}."
                    msg = msg.format(br, type(br).__name__)
                    self.error(msg)

        def _add_graphical_io_args(self):
            graphical_io = super()._add_graphical_io_args()
            graphical_io.add_argument(
                "-pp",
                "--plot-parameters",
                action="store_true",
                dest="plot_parameters",
                help=(
                    "Plot the density and viscosity integrals during simulation. "
                    + "Simulation will stop at each time of interest and "
                    + "the plot will be updated every specified freq iterations."
                ),
            )
            graphical_io.add_argument(
                "-pf",
                "--plot-freq",
                type=int,
                default=100,
                dest="plot_freq",
                help="Plotting update frequency in terms of iterations.",
            )

        def _check_file_io_args(self, args):
            super()._check_file_io_args(args)
            self._check_default(args, "plot_parameters", bool, allow_none=False)
            self._check_default(args, "plot_freq", int, allow_none=False)
            self._check_positive(args, "plot_freq", strict=True, allow_none=False)

        def _setup_parameters(self, args):
            super()._setup_parameters(args)
            dim = args.ndim
            if dim not in (2, 3):
                msg = "Domain should be 2D or 3D."
                self.error(msg)
            Bc = tuple(bc[:dim] for bc in args.Bc)
            args.Bc = Bc

    parser = PeriodicBubbleArgParser()

    parser.set_defaults(
        impl="cl",
        ndim=2,
        npts=(256,),
        box_origin=(0.0,),
        box_length=(1.0,),
        tstart=0.0,
        tend=1.75,
        dt=1e-6,
        cfl=0.5,
        lcfl=0.125,
        dump_freq=25,
        dump_times=(0.0, 0.1, 0.20, 0.30, 0.325, 0.4, 0.45, 0.50),
        rho1=1.0,
        rho2=10.0,
        mu1=0.00025,
        mu2=0.00050,
        Bc=((0.5, 0.15, 0.5),),
        Br=(0.1,),
    )

    parser.run(compute)
