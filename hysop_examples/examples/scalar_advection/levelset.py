# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
import sympy as sm

pi = np.pi


def compute(args):
    from hysop import (
        Field,
        Box,
        Simulation,
        Problem,
        IOParams,
        vprint,
        ScalarParameter,
        MPIParams,
        CartesianDiscretization,
        CartesianTopology,
    )
    from hysop.constants import Implementation, Backend
    from hysop.operators import (
        DirectionalAdvection,
        StrangSplitting,
        Integrate,
        AnalyticField,
        Advection,
        HDF_Writer,
    )
    from hysop.methods import (
        Remesh,
        TimeIntegrator,
        ComputeGranularity,
        Interpolation,
        StrangOrder,
    )
    import hysop.numerics.odesolvers.runge_kutta as rk
    from hysop.defaults import TimeParameters

    # Function to compute initial scalar values
    def init_scalar(data, coords, component):
        assert component == 0
        dim = len(coords)
        if dim == 3:
            (x, y, z) = coords
            rr = np.sqrt((x - 0.35) ** 2 + (y - 0.35) ** 2 + (z - 0.35) ** 2)
        else:
            (x, y) = coords
            rr = np.sqrt((x - 0.5) ** 2 + (y - 0.75) ** 2)
        data[...] = 0.0
        data[rr < 0.15] = 1.0
        rr = np.sqrt((x - 0.75) ** 2 + (y - 0.75) ** 2 + (z - 0.75) ** 2)
        data[rr < 0.1] += 1.0

    # Define domain
    impl = args.impl
    dim = args.ndim
    npts = args.npts
    snpts = args.snpts
    box = Box(origin=args.box_origin, length=args.box_length, dim=dim)
    if dim == 3:
        dt0 = 0.35 / (4.0 * pi)
    else:
        dt0 = 0.35 / (2.0 * pi)
    cfl = 1.0 * dt0 * max(npts)
    vprint(f"\nCFL is {cfl}")

    # Get default MPI Parameters from domain (even for serial jobs)
    mpi_params = MPIParams(comm=box.task_comm, task_id=box.current_task())

    vprint("Default I/O configuration:")
    vprint(args.io_params.to_string("  "))
    vprint()

    method = {}
    extra_op_kwds = {"mpi_params": mpi_params}
    if impl is Implementation.OPENCL:
        from hysop.methods import OpenClKernelConfig
        from hysop.backend.device.opencl.opencl_tools import (
            get_or_create_opencl_env,
            get_device_number,
        )

        cl_env = get_or_create_opencl_env(
            mpi_params=mpi_params,
            platform_id=args.cl_platform_id,
            device_id=(
                box.machine_rank % get_device_number()
                if args.cl_device_id is None
                else None
            ),
        )
        extra_op_kwds["cl_env"] = cl_env
        method[OpenClKernelConfig] = args.opencl_kernel_config
        backend = Backend.OPENCL
    elif impl in (Implementation.PYTHON, Implementation.FORTRAN):
        backend = Backend.HOST
    else:
        msg = f"Unknown implementation '{impl}'."
        raise ValueError(msg)

    # Define parameters and field (time and analytic field)
    t, dt = TimeParameters(dtype=args.dtype)
    velo = Field(domain=box, name="V", nb_components=dim, dtype=args.dtype)
    scalar = Field(domain=box, name="S", nb_components=1, dtype=args.dtype)
    vol = ScalarParameter("volume", dtype=args.dtype)

    if snpts == npts:
        mask = velo[1]
    else:
        mask = scalar.field_like("mask")

    # Setup operator method dictionnary
    method.update(
        {
            TimeIntegrator: args.time_integrator,
            Remesh: args.remesh_kernel,
            Interpolation: args.advection_interpolator,
        }
    )

    # Create a simulation and solve the problem
    # (do not forget to specify the dt parameter here)
    simu = Simulation(
        start=args.tstart,
        end=args.tend,
        nb_iter=args.nb_iter,
        max_iter=args.max_iter,
        times_of_interest=args.times_of_interest,
        t=t,
        dt=dt,
        dt0=dt0,
    )
    simu.write_parameters(simu.t, vol, filename="volume.txt", precision=12)

    # Setup implementation specific variables
    if (impl is Implementation.PYTHON) or (impl is Implementation.FORTRAN):
        sin, cos = np.sin, np.cos

        def fcompute_velocity(data, coords, component, t):
            (x, y, z) = coords
            fn = [
                +2.0
                * sin(pi * x) ** 2
                * sin(2 * pi * y)
                * sin(2.0 * pi * z)
                * cos(t() * pi / 3.0),
                -sin(2.0 * pi * x)
                * sin(pi * y) ** 2
                * sin(2.0 * pi * z)
                * cos(t() * pi / 3.0),
                -sin(2.0 * pi * x)
                * sin(2.0 * pi * y)
                * sin(pi * z) ** 2
                * cos(t() * pi / 3.0),
            ]
            data[...] = fn[component]

        def fcompute_norm(data, coords, component):
            data[...] = np.sqrt(data[0] ** 2 + data[1] ** 2 + data[2] ** 2)

        def fcompute_volume(data, coords, component, S):
            data[...] = S[0] <= 0.5

    elif impl is Implementation.OPENCL:
        from hysop.symbolic.relational import LogicalLE

        sin, cos = sm.sin, sm.cos
        Vs = velo.s()
        Ss = scalar.s()
        if dim == 3:
            x, y, z = box.frame.coords
            formula = (
                2.0
                * sin(pi * x) ** 2
                * sin(2.0 * pi * y)
                * sin(2.0 * pi * z)
                * cos(t.s * pi / 3.0),
                -sin(2.0 * pi * x)
                * sin(pi * y) ** 2
                * sin(2.0 * pi * z)
                * cos(t.s * pi / 3.0),
                -sin(2.0 * pi * x)
                * sin(2.0 * pi * y)
                * sin(pi * z) ** 2
                * cos(t.s * pi / 3.0),
            )
        else:
            x, y = box.frame.coords
            formula = (
                -sin(x * pi) ** 2 * sin(y * pi * 2) * cos(t.s * pi / 3.0),
                sin(y * pi) ** 2 * sin(x * pi * 2) * cos(t.s * pi / 3.0),
            )
        fcompute_velocity = formula
        fcompute_norm = (sm.sqrt(np.dot(Vs, Vs)), None, None)
        fcompute_volume = LogicalLE(Ss, 0.5)
    else:
        msg = f"Unknown implementation '{impl}'."
        raise ValueError(msg)

    if impl is Implementation.FORTRAN:
        advec = Advection(
            name="advec",
            velocity=velo,
            advected_fields=(scalar,),
            dt=dt,
            variables={velo: npts, scalar: snpts},
            implementation=impl,
            **extra_op_kwds,
        )
    else:
        advec_dir = DirectionalAdvection(
            name="advec",
            velocity=velo,
            velocity_cfl=cfl,
            advected_fields=(scalar,),
            dt=dt,
            variables={velo: npts, scalar: snpts},
            implementation=impl,
            **extra_op_kwds,
        )
        advec = StrangSplitting(splitting_dim=dim, order=args.strang_order)
        advec.push_operators(advec_dir)

    compute_velocity = AnalyticField(
        name="compute_velocity",
        pretty_name="V(t)",
        field=velo,
        formula=fcompute_velocity,
        variables={velo: npts},
        extra_input_kwds={"t": simu.t},
        implementation=(
            Implementation.PYTHON if (impl is Implementation.FORTRAN) else impl
        ),
        **extra_op_kwds,
    )

    compute_norm = AnalyticField(
        name="compute_norm",
        pretty_name="||V||",
        field=velo,
        formula=fcompute_norm,
        variables={velo: npts},
        implementation=(
            Implementation.PYTHON if (impl is Implementation.FORTRAN) else impl
        ),
        **extra_op_kwds,
    )

    compute_volume = AnalyticField(
        name="compute_volume",
        pretty_name="S<0.5",
        field=mask,
        formula=fcompute_volume,
        variables={mask: snpts, scalar: snpts},
        extra_input_kwds={"S": scalar},
        implementation=(
            Implementation.PYTHON if (impl is Implementation.FORTRAN) else impl
        ),
        **extra_op_kwds,
    )

    # compute the volume (where S<=0.5), use Vy as tmp buffer when possible (ie. npts == snpts)
    volume = Integrate(
        name="volume",
        field=mask,
        variables={mask: snpts},
        parameter=vol,
        implementation=impl,
        **extra_op_kwds,
    )

    # dump scalar and velocity norm
    if npts == snpts:
        io_params = args.io_params.clone(filename="fields")
        dump_fields = HDF_Writer(
            name="fields",
            io_params=io_params,
            force_backend=backend,
            variables={velo[0]: snpts, scalar: snpts},
            **extra_op_kwds,
        )
        dumpers = (dump_fields,)
    else:
        io_params = args.io_params.clone(filename="scalar")
        dump_scalar = HDF_Writer(
            name="scalar",
            io_params=io_params,
            force_backend=backend,
            variables={scalar: snpts},
            **extra_op_kwds,
        )

        io_params = args.io_params.clone(filename="vnorm")
        dump_velocity_norm = HDF_Writer(
            name="Vnorm",
            io_params=io_params,
            force_backend=backend,
            variables={velo[0]: npts},
            **extra_op_kwds,
        )
        dumpers = (dump_scalar, dump_velocity_norm)

    problem = Problem(method=method)
    problem.insert(compute_velocity, advec)
    if not args.bench_mode:
        problem.insert(compute_norm, *dumpers)
        problem.insert(compute_volume, volume)
    problem.build(args=args)

    # If a visu_rank was provided, and show_graph was set,
    # display the graph on the given process rank.
    if args.display_graph:
        problem.display(args.visu_rank)

    # Initialize scalar
    problem.initialize_field(scalar, formula=init_scalar)

    # Finally solve the problem
    problem.solve(
        simu,
        dry_run=args.dry_run,
        debug_dumper=args.debug_dumper,
        checkpoint_handler=args.checkpoint_handler,
    )

    problem.finalize()


if __name__ == "__main__":
    from hysop_examples.argparser import HysopArgParser, colors

    class LevelsetArgParser(HysopArgParser):
        def __init__(self):
            prog_name = "levelset"
            default_dump_dir = f"{HysopArgParser.tmp_dir()}/hysop_examples/{prog_name}"

            description = colors.color(
                "HySoP Levelset Example: ", fg="blue", style="bold"
            )
            description += "Advect a scalar by a time dependent analytic velocity. "

            super().__init__(
                prog_name=prog_name,
                description=description,
                default_dump_dir=default_dump_dir,
            )

        def _add_main_args(self):
            args = super()._add_main_args()
            args.add_argument(
                "-b",
                "--bench",
                action="store_true",
                dest="bench_mode",
                help="Disable volume computation, bench only advection.",
            )
            return args

    parser = LevelsetArgParser()

    parser.set_defaults(
        box_origin=(0.0,),
        box_length=(1.0,),
        ndim=3,
        tstart=0.0,
        tend=12.0,
        npts=(128,),
        snpts=None,
        grid_ratio=1,
        dump_period=0.1,
        dump_freq=0,
    )
    parser.run(compute)
