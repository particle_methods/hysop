# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# HySoP Example: Turbulent scalar advection-diffusion
# Example from JM Etancelin PhD (section 6.3)
import os
import numpy as np
import mpi4py.MPI as MPI

pi = np.pi
cos = np.cos
sin = np.sin

TASK_UW = 111
TASK_SCALAR = 222
if MPI.COMM_WORLD.Get_size() == 1:
    TASK_UW = 999
    TASK_SCALAR = 999


def compute(args):
    from hysop.backend.device.opencl.opencl_tools import (
        get_or_create_opencl_env,
        get_device_number,
    )
    from hysop.tools.parameters import CartesianDiscretization
    from hysop.core.mpi import MPI, main_size, main_rank, main_comm
    from hysop.methods import (
        SpaceDiscretization,
        Remesh,
        TimeIntegrator,
        ComputeGranularity,
        Interpolation,
        StrangOrder,
    )
    from hysop.numerics.odesolvers.runge_kutta import RK2
    from hysop.operators import (
        Advection,
        StaticDirectionalStretching,
        Diffusion,
        PoissonCurl,
        AdaptiveTimeStep,
        DirectionalDiffusion,
        Enstrophy,
        MinMaxFieldStatistics,
        StrangSplitting,
        ParameterPlotter,
        DirectionalAdvection,
        HDF_Writer,
    )
    from hysop.topology.cartesian_topology import CartesianTopology
    from hysop.constants import (
        Implementation,
        AdvectionCriteria,
        HYSOP_REAL,
        StretchingFormulation,
        HYSOP_DEFAULT_TASK_ID,
    )
    from hysop.defaults import (
        VelocityField,
        VorticityField,
        EnstrophyParameter,
        TimeParameters,
    )
    from hysop import Box, Simulation, Problem, MPIParams, Field, IOParams

    width = 0.01
    ampl3 = 0.3
    ampl = 0.05

    # Function to compute initial vorticity
    def init_velocity(data, coords, component):
        (x, y, z) = coords
        yy = (0.1 - 2.0 * np.abs(y - 0.5)) / (4.0 * width)
        strg = np.exp(-np.abs(yy**2)) * np.random.random(size=data.shape)
        if component == 0:
            data[...] = 0.5 * (1.0 + np.tanh(yy))
            data[...] *= 1.0 + ampl3 * np.sin(8.0 * np.pi * x)
            data[...] *= 1.0 + ampl * strg
        else:
            data[...] = ampl * strg
        return data

    # Function to compute initial vorticity

    def init_vorticity(data, coords, component):
        (x, y, z) = coords
        if component == 2:
            xx = -(0.3 * np.sin(8.0 * np.pi * x) + 1.0)
            yp = 25.0 * np.tanh(-50.0 * y[y > 0.5] + 27.5) ** 2 - 25.0
            ym = -25.0 * np.tanh(50.0 * y[y < 0.5] - 22.5) ** 2 + 25.0
            data[:, y[0, :, 0] > 0.5, :] = yp[np.newaxis, :, np.newaxis]
            data[:, y[0, :, 0] == 0.5, :] = 0.0
            data[:, y[0, :, 0] < 0.5, :] = ym[np.newaxis, :, np.newaxis]
            data[...] *= xx
        else:
            data[...] = 0.0

    def init_scal(data, coords, component):
        assert component == 0
        (x, y, z) = coords
        yy = abs(y - 0.5)
        aux = (0.1 - 2.0 * yy) / (4.0 * width)
        data[...] = 0.5 * (1.0 + np.tanh(aux))
        data[...] *= 1.0 + ampl3 * np.sin(8.0 * pi * x)

    # Define the domain
    dim = args.ndim
    npts_uw = args.npts
    npts_s = args.snpts
    box = Box(
        origin=args.box_origin,
        length=args.box_length,
        dim=dim,
        proc_tasks=args.proc_tasks,
    )

    # Physical parameters:
    # Flow viscosity
    VISCOSITY = 1e-4
    # Schmidt number
    SC = ((1.0 * npts_s[0] - 1.0) / (1.0 * npts_uw[0] - 1.0)) ** 2
    # Scalar diffusivity
    DIFF_COEFF_SCAL = VISCOSITY / SC
    dt_max = 0.012
    cfl = 1.5
    lcfl = 0.15
    dump_freq = args.dump_freq

    # Get the mpi parameters
    has_tasks = not args.proc_tasks is None
    if has_tasks:
        mpi_params = {TASK_UW: None, TASK_SCALAR: None, HYSOP_DEFAULT_TASK_ID: None}
        for tk in (TASK_UW, TASK_SCALAR, HYSOP_DEFAULT_TASK_ID):
            mpi_params[tk] = MPIParams(
                comm=box.get_task_comm(tk), task_id=tk, on_task=box.is_on_task(tk)
            )
    else:
        mpi_params = MPIParams(comm=box.task_comm, task_id=HYSOP_DEFAULT_TASK_ID)
    cl_env = None
    if box.is_on_task(TASK_SCALAR):
        # Create an explicit OpenCL context from user parameters
        cl_env = get_or_create_opencl_env(
            mpi_params=mpi_params[TASK_SCALAR],
            platform_id=0,
            device_id=box.machine_rank % get_device_number(),
        )

    # Setup usual implementation specific variables
    impl = None
    method = {}

    # Define parameters and field (time, timestep, velocity, vorticity, enstrophy)
    t, dt = TimeParameters(dtype=HYSOP_REAL)
    velo = VelocityField(domain=box, dtype=HYSOP_REAL)
    vorti = VorticityField(velocity=velo, dtype=HYSOP_REAL)
    enstrophy = EnstrophyParameter(dtype=HYSOP_REAL)
    wdotw = Field(domain=box, dtype=HYSOP_REAL, is_vector=False, name="WdotW")
    scal = Field(domain=box, name="Scalar", is_vector=False)

    # Build the directional operators
    # > Directional advection
    advec_scal = DirectionalAdvection(
        implementation=Implementation.OPENCL,
        name="advec_scal",
        velocity=velo,
        velocity_cfl=dt_max * 1.2 * (npts_uw[0] - 1),
        advected_fields=(scal,),
        variables={velo: npts_uw, scal: npts_s},
        dt=dt,
        mpi_params=mpi_params[TASK_SCALAR],
        cl_env=cl_env,
    )
    diffuse_scal = DirectionalDiffusion(
        implementation=Implementation.OPENCL,
        name="diffuse_scal",
        fields=(scal,),
        coeffs=DIFF_COEFF_SCAL,
        variables={scal: npts_s},
        dt=dt,
        mpi_params=mpi_params[TASK_SCALAR],
        cl_env=cl_env,
    )
    splitting_scal = StrangSplitting(
        splitting_dim=dim, order=StrangOrder.STRANG_FIRST_ORDER
    )
    splitting_scal.push_operators(advec_scal)
    splitting_scal.push_operators(diffuse_scal)
    # > Directional advection
    advec = Advection(
        implementation=Implementation.FORTRAN,
        name="advec",
        velocity=velo,
        advected_fields=(vorti,),
        variables={velo: npts_uw, vorti: npts_uw},
        dt=dt,
        mpi_params=mpi_params[TASK_UW],
    )
    # > Directional stretching
    stretch = StaticDirectionalStretching(
        implementation=Implementation.PYTHON,
        name="stretch",
        formulation=StretchingFormulation.CONSERVATIVE,
        velocity=velo,
        vorticity=vorti,
        variables={velo: npts_uw, vorti: npts_uw},
        dt=dt,
        mpi_params=mpi_params[TASK_UW],
    )
    # > Directional splitting operator subgraph
    splitting = StrangSplitting(
        splitting_dim=dim, order=StrangOrder.STRANG_SECOND_ORDER
    )
    splitting.push_operators(stretch)
    # > Diffusion
    diffuse = Diffusion(
        implementation=Implementation.FORTRAN,
        name="diffuse",
        nu=VISCOSITY,
        Fin=vorti,
        variables={vorti: npts_uw},  # topo_nogh},
        dt=dt,
        mpi_params=mpi_params[TASK_UW],
    )
    # > Poisson operator to recover the velocity from the vorticity
    poisson = PoissonCurl(
        implementation=Implementation.FORTRAN,
        name="poisson",
        velocity=velo,
        vorticity=vorti,
        variables={velo: npts_uw, vorti: npts_uw},
        projection=None,
        mpi_params=mpi_params[TASK_UW],
    )
    # > Operator to compute the infinite norm of the velocity
    min_max_U = MinMaxFieldStatistics(
        name="min_max_U",
        field=velo,
        Finf=True,
        implementation=Implementation.PYTHON,
        variables={velo: npts_uw},
        mpi_params=mpi_params[TASK_UW],
    )
    # > Operator to compute the infinite norm of the vorticity
    min_max_W = MinMaxFieldStatistics(
        name="min_max_W",
        field=vorti,
        Finf=True,
        implementation=Implementation.PYTHON,
        variables={vorti: npts_uw},
        mpi_params=mpi_params[TASK_UW],
    )
    # > Operator to compute the enstrophy
    enstrophy_op = Enstrophy(
        name="enstrophy",
        vorticity=vorti,
        enstrophy=enstrophy,
        WdotW=wdotw,
        variables={vorti: npts_uw, wdotw: npts_uw},
        implementation=Implementation.PYTHON,
        mpi_params=mpi_params[TASK_UW],
    )

    # Adaptive timestep operator
    adapt_dt = AdaptiveTimeStep(
        dt, max_dt=dt_max, equivalent_CFL=True, mpi_params=mpi_params[TASK_UW]
    )
    dt_cfl = adapt_dt.push_cfl_criteria(
        cfl=cfl,
        Finf=min_max_U.Finf,
        equivalent_CFL=True,
        mpi_params=mpi_params[TASK_UW],
    )
    dt_advec = adapt_dt.push_advection_criteria(
        lcfl=lcfl,
        Finf=min_max_W.Finf,
        criteria=AdvectionCriteria.W_INF,
        mpi_params=mpi_params[TASK_UW],
    )

    # > Outputs
    dumpU = HDF_Writer(
        name="dumpU",
        io_params=IOParams(
            filename="U",
            frequency=args.dump_freq,
            dump_times=args.dump_times,
            with_last=True,
        ),
        variables={velo: npts_uw},
        var_names={velo[0]: "UX", velo[1]: "UY", velo[2]: "UZ"},
        mpi_params=mpi_params[TASK_UW],
    )
    dumpS = HDF_Writer(
        name="dumpS",
        io_params=IOParams(
            filename="S",
            frequency=args.dump_freq,
            dump_times=args.dump_times,
            with_last=True,
        ),
        variables={scal: npts_s},
        var_names={scal: "S"},
        mpi_params=mpi_params[TASK_SCALAR],
    )

    # Create the problem we want to solve and insert our
    # directional splitting subgraph and the standard operators.
    # The method dictionnary passed to this graph will be dispatched
    # accross all operators contained in the graph.
    method.update(
        {
            ComputeGranularity: 0,
            SpaceDiscretization: 4,
            TimeIntegrator: RK2,
            Remesh: Remesh.L4_2,
            Interpolation: Interpolation.LINEAR,
        }
    )
    problem = Problem(method=method, mpi_params=mpi_params[TASK_UW])
    problem.insert(
        advec,
        splitting,
        diffuse,
        poisson,
        splitting_scal,  # task_scal
        dumpS,  # task_scal
        dumpU,
        enstrophy_op,
        min_max_U,
        min_max_W,
        adapt_dt,
    )
    problem.build()

    # Create a simulation
    # (do not forget to specify the t and dt parameters here)
    simu = Simulation(
        start=args.tstart,
        end=args.tend,
        nb_iter=args.nb_iter,
        max_iter=args.max_iter,
        dt0=1e-5,
        times_of_interest=args.times_of_interest,
        t=t,
        dt=dt,
    )
    simu.write_parameters(
        t,
        dt_cfl,
        dt_advec,
        dt,
        enstrophy,
        min_max_U.Finf,
        min_max_W.Finf,
        adapt_dt.equivalent_CFL,
        filename="parameters.txt",
        precision=8,
    )

    # Initialize fields on tasks where needed
    if box.is_on_task(TASK_UW):
        problem.initialize_field(velo, formula=init_velocity)
        problem.initialize_field(vorti, formula=init_vorticity)
    if box.is_on_task(TASK_SCALAR):
        problem.initialize_field(scal, formula=init_scal)

    # Finally solve the problem
    problem.solve(simu)

    # Finalize
    problem.finalize()


if __name__ == "__main__":
    from hysop_examples.argparser import HysopArgParser, colors
    from hysop.core.mpi import MPI, main_size, main_rank, main_comm

    class TurbulentScalarAdvectionArgParser(HysopArgParser):
        def __init__(self):
            prog_name = "turbulent_scalar_advection"
            default_dump_dir = f"{HysopArgParser.tmp_dir()}/hysop_examples/{prog_name}"

            description = colors.color(
                "HySoP Turbulent scalar transport Example: ", fg="blue", style="bold"
            )
            description += "Advect a scalar by a flow velocity. "

            super().__init__(
                prog_name=prog_name,
                description=description,
                default_dump_dir=default_dump_dir,
            )

        def _add_main_args(self):
            args = super()._add_main_args()
            args.add_argument(
                "--proc-tasks",
                type=str,
                action=self.eval,
                container=tuple,
                append=False,
                dest="proc_tasks",
                help="Specify the tasks for each proc.",
            )
            return args

    parser = TurbulentScalarAdvectionArgParser()
    parser.set_defaults(
        box_origin=(0.0, 0.0, 0.0),
        box_length=(1.0, 1.0, 2.0),
        ndim=3,
        tstart=0.0,
        tend=12.0,
        times_of_interest=(4.5,),
        npts=(64, 64, 128),
        snpts=(128, 128, 256),
        dump_period=1.0,
        dump_freq=100,
        proc_tasks=tuple(
            (TASK_SCALAR, TASK_UW) if _ % main_size == 0 else (TASK_UW,)
            for _ in range(main_size)
        ),
    )
    parser.run(compute)
