# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import numpy as np


pi = np.pi
cos = np.cos
sin = np.sin


def compute(args):
    from hysop import Box, Simulation, Problem, MPIParams, Field, IOParams
    from hysop.defaults import (
        VelocityField,
        VorticityField,
        EnstrophyParameter,
        TimeParameters,
    )
    from hysop.parameters.tensor_parameter import TensorParameter
    from hysop.constants import (
        Implementation,
        AdvectionCriteria,
        HYSOP_REAL,
        StretchingFormulation,
        StretchingCriteria,
        Backend,
    )
    from hysop.operators import (
        Advection,
        StaticDirectionalStretching,
        Diffusion,
        PoissonCurl,
        AdaptiveTimeStep,
        HDF_Writer,
        Enstrophy,
        MinMaxFieldStatistics,
        StrangSplitting,
        ParameterPlotter,
        PenalizeVorticity,
        FlowRateCorrection,
        VorticityAbsorption,
        CustomOperator,
        DirectionalAdvection,
        DirectionalStretching,
    )
    from hysop.numerics.odesolvers.runge_kutta import RK2
    from hysop.methods import (
        SpaceDiscretization,
        Remesh,
        TimeIntegrator,
        ComputeGranularity,
        Interpolation,
        StrangOrder,
    )
    from hysop.topology.cartesian_topology import CartesianTopology
    from hysop.tools.parameters import CartesianDiscretization

    # Define the domain
    dim = args.ndim
    assert dim == 3
    npts = args.npts
    box = Box(dim=dim, origin=args.box_origin, length=args.box_length)
    cfl = args.cfl
    lcfl = args.lcfl
    uinf = 1.0
    viscosity = 1.0 / 250
    outfreq = args.dump_freq
    dt0 = args.dt

    # Get default MPI Parameters from domain (even for serial jobs)
    mpi_params = MPIParams(comm=box.task_comm, task_id=box.current_task())

    # Setup usual implementation specific variables
    impl = args.impl
    extra_op_kwds = {"mpi_params": mpi_params}
    implIsFortran = impl is Implementation.FORTRAN
    backend = Backend.HOST
    if impl is Implementation.PYTHON or implIsFortran:
        method = {}
    elif impl is Implementation.OPENCL:
        # For the OpenCL implementation we need to setup the compute device
        # and configure how the code is generated and compiled at runtime.

        # Create an explicit OpenCL context from user parameters
        from hysop.backend.device.opencl.opencl_tools import (
            get_or_create_opencl_env,
            get_device_number,
        )

        cl_env = get_or_create_opencl_env(
            mpi_params=mpi_params,
            platform_id=args.cl_platform_id,
            device_id=(
                box.machine_rank % get_device_number()
                if args.cl_device_id is None
                else None
            ),
        )

        # Configure OpenCL kernel generation and tuning (already done by HysopArgParser)
        from hysop.methods import OpenClKernelConfig

        method = {OpenClKernelConfig: args.opencl_kernel_config}

        # Setup opencl specific extra operator keyword arguments
        extra_op_kwds["cl_env"] = cl_env
    else:
        msg = f"Unknown implementation '{impl}'."
        raise ValueError(msg)

    # ====== Sphere inside the domain ======
    RADIUS = 0.5
    pos = [0.0, 0.0, 0.0]

    def computeSphere(data, coords, component):
        assert component == 0
        (x, y, z) = coords
        dx = x[0, 0, 1] - x[0, 0, 0]
        dy = y[0, 1, 0] - y[0, 0, 0]
        dz = z[1, 0, 0] - z[0, 0, 0]
        data[...] = 0.0

        def chi(x, y, z):
            return (
                np.sqrt(
                    (x - pos[0]) * (x - pos[0])
                    + (y - pos[1]) * (y - pos[1])
                    + (z - pos[2]) * (z - pos[2])
                )
                <= RADIUS
            )

        data[chi(x, y, z)] = 1.0

        # Smooth the sphere surface with a Volume-of-fluid fraction
        vof = 5  # number of points in the subgrid
        front_z = np.where(np.abs(data[0:-1, :, :] - data[1:, :, :]) > 0.1)
        front_z = (
            np.concatenate((front_z[0], front_z[0] + 1)),
            np.concatenate((front_z[1], front_z[1])),
            np.concatenate((front_z[2], front_z[2])),
        )
        front_y = np.where(np.abs(data[:, 0:-1, :] - data[:, 1:, :]) > 0.1)
        front_y = (
            np.concatenate((front_y[0], front_y[0])),
            np.concatenate((front_y[1], front_y[1] + 1)),
            np.concatenate((front_y[2], front_y[2])),
        )
        front = (
            np.concatenate((front_z[0], front_y[0])),
            np.concatenate((front_z[1], front_y[1])),
            np.concatenate((front_z[2], front_y[2])),
        )
        front_x = np.where(np.abs(data[:, :, 0:-1] - data[:, :, 1:]) > 0.1)
        front_x = (
            np.concatenate((front_x[0], front_x[0])),
            np.concatenate((front_x[1], front_x[1])),
            np.concatenate((front_x[2], front_x[2] + 1)),
        )
        front = (
            np.concatenate((front[0], front_x[0])),
            np.concatenate((front[1], front_x[1])),
            np.concatenate((front[2], front_x[2])),
        )

        for k, j, i in zip(*front):
            sx = np.linspace(x[0, 0, i] - dx / 2, x[0, 0, i] + dx / 2, vof)[
                np.newaxis, np.newaxis, :
            ]
            sy = np.linspace(y[0, j, 0] - dy / 2, y[0, j, 0] + dy / 2, vof)[
                np.newaxis, :, np.newaxis
            ]
            sz = np.linspace(z[k, 0, 0] - dz / 2, z[k, 0, 0] + dz / 2, vof)[
                :, np.newaxis, np.newaxis
            ]
            data[k, j, i] = 1.0 * (np.sum(chi(sx, sy, sz)) / (1.0 * vof**3))

    # ======= Function to compute initial velocity  =======

    def computeVel(data, coords, component):
        data[...] = uinf if (component == 0) else 0.0

    # ======= Function to compute initial vorticity =======
    def computeVort(data, coords, component):
        data[...] = 0.0

    def computeFlowrate(t, flowrate):
        fr = np.zeros_like(flowrate.value)
        fr[-1] = uinf * box.length[1] * box.length[0]
        Tstart = 3.0
        if t() >= Tstart and t() <= Tstart + 1.0:
            fr[1] = sin(pi * (t() - Tstart)) * box.length[1] * box.length[0]
        flowrate.value = fr

    method = {}

    # Define parameters and field (time, timestep, velocity, vorticity, enstrophy)
    t, dt = TimeParameters(dtype=args.dtype)
    velo = VelocityField(domain=box, dtype=args.dtype)
    vorti = VorticityField(velocity=velo, dtype=args.dtype)
    sphere = Field(domain=box, name="Sphere", is_vector=False, dtype=args.dtype)
    wdotw = Field(domain=box, dtype=args.dtype, is_vector=False, name="WdotW")
    enstrophy = EnstrophyParameter(dtype=args.dtype)
    flowrate = TensorParameter(
        name="flowrate",
        dtype=args.dtype,
        shape=(3,),
        initial_value=[0.0, 0.0, uinf * box.length[1] * box.length[0]],
    )

    # # Topologies
    # topo_nogh = CartesianTopology(domain=box,
    #                               discretization=CartesianDiscretization(npts,
    #                                   default_boundaries=True),
    #                               mpi_params=mpi_params,
    #                               cutdirs=[True, False, False])
    # topo_gh = CartesianTopology(domain=box,
    #                             discretization=CartesianDiscretization(npts,
    #                                 ghosts=(4, 4, 4), default_boundaries=True),
    #                             mpi_params=mpi_params,
    #                             cutdirs=[True, False, False])

    # Build the directional operators
    # > Directional advection
    if implIsFortran:
        advec = Advection(
            implementation=impl,
            name="advec",
            velocity=velo,
            advected_fields=(vorti,),
            variables={velo: npts, vorti: npts},
            dt=dt,
            **extra_op_kwds,
        )
    else:
        advec_dir = DirectionalAdvection(
            implementation=impl,
            name="advec",
            velocity=velo,
            advected_fields=(vorti,),
            velocity_cfl=args.cfl,
            variables={velo: npts, vorti: npts},
            dt=dt,
            **extra_op_kwds,
        )
    # > Directional stretching + diffusion
    if impl is Implementation.OPENCL:
        StretchOp = DirectionalStretching
    else:
        StretchOp = StaticDirectionalStretching
    stretch = StretchOp(
        implementation=Implementation.PYTHON if implIsFortran else impl,
        name="stretch",
        formulation=StretchingFormulation.CONSERVATIVE,
        velocity=velo,
        vorticity=vorti,
        variables={velo: npts, vorti: npts},
        dt=dt,
        **extra_op_kwds,
    )
    # > Directional splitting operator subgraph
    splitting = StrangSplitting(splitting_dim=dim, order=StrangOrder.STRANG_FIRST_ORDER)
    if not implIsFortran:
        splitting.push_operators(advec_dir)
    splitting.push_operators(stretch)
    # > Penalization
    penal = PenalizeVorticity(
        implementation=Implementation.PYTHON,
        name="penalization",
        velocity=velo,
        vorticity=vorti,
        variables={velo: npts, vorti: npts, sphere: npts},
        obstacles=[
            sphere,
        ],
        coeff=1e8,
        dt=dt,
        **extra_op_kwds,
    )
    # > Diffusion operator
    diffuse = Diffusion(
        implementation=impl,
        name="diffuse",
        nu=viscosity,
        Fin=vorti,
        variables={vorti: npts},
        dt=dt,
        **extra_op_kwds,
    )
    # > Vorticity absorption
    absorption = VorticityAbsorption(
        implementation=Implementation.PYTHON,
        velocity=velo,
        vorticity=vorti,
        start_coord=6.68,
        flowrate=flowrate,
        name="absorption",
        variables={velo: npts, vorti: npts},
        dt=dt,
        **extra_op_kwds,
    )
    # > Poisson operator to recover the velocity from the vorticity
    poisson = PoissonCurl(
        implementation=impl,
        name="poisson",
        velocity=velo,
        vorticity=vorti,
        variables={velo: npts, vorti: npts},
        projection=None,
        **extra_op_kwds,
    )
    # > Flowrate correction operator to adjust velocity with prescribed flowrate
    computeFlowrate = CustomOperator(
        func=computeFlowrate, invars=(t,), outvars=(flowrate,)
    )
    correctFlowrate = FlowRateCorrection(
        implementation=Implementation.PYTHON,
        name="flowrate_correction",
        velocity=velo,
        vorticity=vorti,
        flowrate=flowrate,
        dt=dt,
        variables={velo: npts, vorti: npts},
        **extra_op_kwds,
    )

    # > outputs
    io_params = IOParams(filename="fields", frequency=args.dump_freq)
    dump_fields = HDF_Writer(
        name="fields",
        io_params=io_params,
        force_backend=backend,
        variables={velo: npts, vorti: npts},
        **extra_op_kwds,
    )

    # > Operator to compute the infinite norm of the velocity
    min_max_U = MinMaxFieldStatistics(
        name="min_max_U",
        field=velo,
        Finf=True,
        implementation=Implementation.PYTHON if implIsFortran else impl,
        variables={velo: npts},
        **extra_op_kwds,
    )
    # > Operator to compute the infinite norm of the vorticity
    min_max_W = MinMaxFieldStatistics(
        name="min_max_W",
        field=vorti,
        Finf=True,
        implementation=Implementation.PYTHON if implIsFortran else impl,
        variables={vorti: npts},
        **extra_op_kwds,
    )
    # > Operator to compute the enstrophy
    enstrophy_op = Enstrophy(
        name="enstrophy",
        vorticity=vorti,
        enstrophy=enstrophy,
        WdotW=wdotw,
        variables={vorti: npts, wdotw: npts},
        implementation=Implementation.PYTHON if implIsFortran else impl,
        **extra_op_kwds,
    )

    # Adaptive timestep operator
    # TODO: move advection to GRAD_U
    # TODO: add stretching criteria, based on a gradient
    adapt_dt = AdaptiveTimeStep(
        dt, equivalent_CFL=True, start_time=10 * dt0
    )  # start adapting timestep at t=10*dt0
    dt_cfl = adapt_dt.push_cfl_criteria(
        cfl=cfl, Finf=min_max_U.Finf, equivalent_CFL=True
    )
    dt_advec = adapt_dt.push_advection_criteria(
        lcfl=lcfl, Finf=min_max_W.Finf, criteria=AdvectionCriteria.W_INF
    )
    # criteria=AdvectionCriteria.GRAD_U)
    # dt_advec = adapt_dt.push_stretching_criteria(lcfl=lcfl,
    #                                              gradFinf=grad_W.Finf,
    #                                              criteria=StretchingCriteria.GRAD_U)

    # Create the problem we want to solve and insert our
    # directional splitting subgraph and the standard operators.
    # The method dictionnary passed to this graph will be dispatched
    # accross all operators contained in the graph.
    method.update(
        {
            SpaceDiscretization: args.fd_order,
            TimeIntegrator: args.time_integrator,
            Remesh: args.remesh_kernel,
            Interpolation: Interpolation.LINEAR,
        }
    )
    problem = Problem(method=method)
    problem.insert(computeFlowrate, penal, splitting, diffuse)
    if implIsFortran:
        problem.insert(advec)
    problem.insert(
        absorption,
        poisson,
        correctFlowrate,
        enstrophy_op,
        min_max_U,
        min_max_W,
        dump_fields,
        adapt_dt,
    )
    problem.build()

    # Create a simulation
    # (do not forget to specify the t and dt parameters here)
    simu = Simulation(
        start=args.tstart,
        end=args.tend,
        nb_iter=args.nb_iter,
        max_iter=args.max_iter,
        dt0=args.dt,
        times_of_interest=args.times_of_interest,
        t=t,
        dt=dt,
    )
    simu.write_parameters(
        t,
        dt_cfl,
        dt_advec,
        dt,
        enstrophy,
        flowrate,
        min_max_U.Finf,
        min_max_W.Finf,
        adapt_dt.equivalent_CFL,
        filename="parameters.txt",
        precision=8,
    )

    problem.initialize_field(vorti, formula=computeVort)
    problem.initialize_field(velo, formula=computeVel)
    problem.initialize_field(sphere, formula=computeSphere)

    # Finally solve the problem
    problem.solve(
        simu,
        dry_run=args.dry_run,
        debug_dumper=args.debug_dumper,
        checkpoint_handler=args.checkpoint_handler,
    )

    # Finalize
    problem.finalize()


if __name__ == "__main__":
    from hysop_examples.argparser import HysopArgParser, colors

    parser = HysopArgParser(
        prog_name="FlowAroundSphere",
        description="""HySoP flow around a sphere.\n""",
        default_dump_dir="{}/hysop_examples/FlowAroundSphere".format(
            HysopArgParser.tmp_dir()
        ),
    )
    parser.set_defaults(
        impl="cl",
        ndim=3,
        npts=(32, 32, 64),
        box_origin=(-2.56, -2.56, -2.56),
        box_length=(5.12, 5.12, 10.24),
        tstart=0.0,
        tend=10.0,
        dt=0.0125,
        cfl=0.5,
        lcfl=0.125,
        dump_freq=100,
        dump_times=(10.0,),
    )
    parser.run(compute)
