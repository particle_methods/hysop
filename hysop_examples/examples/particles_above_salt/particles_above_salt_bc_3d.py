# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
import scipy as sp
import sympy as sm

# initialize vorticity


def init_vorticity(data, coords, component):
    # the flow is initially quiescent
    data[...] = 0.0


# initialize velocity


def init_velocity(data, coords, component):
    # the flow is initially quiescent
    data[...] = 0.0


# initialize sediment concentration and salinity


def delta(*coords):
    d = np.prod(*coords)
    return np.zeros_like(d)


def delta(Ys, l0):
    Y0 = 1
    for Yi in Ys:
        Y0 = Y0 * Yi
    return 0.1 * l0 * (np.random.rand(*Y0.shape) - 0.5)


def init_concentration(data, coords, l0, component):
    assert component == 0
    X = coords[0]
    Ys = coords[0:]
    data[...] = 0.5 * (1.0 + sp.special.erf((X - delta(Ys, l0)) / l0))


def init_salinity(data, coords, l0, component):
    assert component == 0
    init_concentration(data=data, coords=coords, l0=l0, component=component)
    data[...] = 1.0 - data[...]


def compute(args):
    from hysop import (
        Field,
        Box,
        Simulation,
        Problem,
        MPIParams,
        IOParams,
        vprint,
        ScalarParameter,
        IO,
    )
    from hysop.defaults import (
        VelocityField,
        VorticityField,
        DensityField,
        ViscosityField,
        LevelSetField,
        PenalizationField,
        EnstrophyParameter,
        TimeParameters,
        VolumicIntegrationParameter,
    )
    from hysop.constants import (
        Implementation,
        AdvectionCriteria,
        BoxBoundaryCondition,
        BoundaryCondition,
        Backend,
    )

    from hysop.operators import (
        DirectionalAdvection,
        DirectionalStretching,
        Diffusion,
        ComputeMeanField,
        PoissonCurl,
        AdaptiveTimeStep,
        Enstrophy,
        MinMaxFieldStatistics,
        StrangSplitting,
        ParameterPlotter,
        Integrate,
        HDF_Writer,
        CustomSymbolicOperator,
        DirectionalSymbolic,
    )

    from hysop.methods import (
        SpaceDiscretization,
        Remesh,
        TimeIntegrator,
        ComputeGranularity,
        Interpolation,
    )

    from hysop.symbolic import sm, space_symbols, local_indices_symbols
    from hysop.symbolic.base import SymbolicTensor
    from hysop.symbolic.field import curl
    from hysop.symbolic.relational import Assignment, LogicalLE, LogicalGE
    from hysop.symbolic.misc import Select
    from hysop.symbolic.tmp import TmpScalar
    from hysop.tools.string_utils import framed_str

    # IO paths
    spectral_path = IO.default_path() + "/spectral"
    dump_energy_ioparams = IOParams(
        filepath=spectral_path, filename="E_{fname}", frequency=args.dump_freq
    )

    # Constants
    l0 = 1.5  # initial thickness of the profile
    dim = args.ndim
    if dim == 3:
        (Sc, tau, Vp, Rs, Xo, Xn, N) = (
            7.00,
            25,
            0.04,
            2.0,
            (-110, 0, 0),
            (65, 100, 100),
            (3073, 1024, 1024),
        )
        Sc = args.schmidt
        tau = args.tau
        Vp = args.Vp
        Rs = args.Rs
        n = args.npts[0]
        N = (3 * n + 1, n, n)
        vprint(
            f"Example configuration is Sc={Sc}, Tau={tau}, Vp={Vp}, Rs={Rs}, n={n}, N={N}"
        )
        vprint()
    else:
        raise NotImplementedError

    nu_S = ScalarParameter(
        name="nu_S", dtype=args.dtype, const=True, initial_value=1.0 / Sc
    )
    nu_C = ScalarParameter(
        name="nu_C", dtype=args.dtype, const=True, initial_value=1.0 / (tau * Sc)
    )
    nu_W = ScalarParameter(name="nu_W", dtype=args.dtype, const=True, initial_value=1.0)

    # Define the domain
    npts = N[::-1]
    Xo = Xo[::-1]
    Xn = Xn[::-1]

    lboundaries = (BoxBoundaryCondition.PERIODIC,) * (dim - 1) + (
        BoxBoundaryCondition.SYMMETRIC,
    )
    rboundaries = (BoxBoundaryCondition.PERIODIC,) * (dim - 1) + (
        BoxBoundaryCondition.SYMMETRIC,
    )

    S_lboundaries = (BoundaryCondition.PERIODIC,) * (dim - 1) + (
        BoundaryCondition.HOMOGENEOUS_NEUMANN,
    )
    S_rboundaries = (BoundaryCondition.PERIODIC,) * (dim - 1) + (
        BoundaryCondition.HOMOGENEOUS_DIRICHLET,
    )
    C_lboundaries = (BoundaryCondition.PERIODIC,) * (dim - 1) + (
        BoundaryCondition.HOMOGENEOUS_DIRICHLET,
    )
    C_rboundaries = (BoundaryCondition.PERIODIC,) * (dim - 1) + (
        BoundaryCondition.HOMOGENEOUS_NEUMANN,
    )

    box = Box(
        origin=Xo,
        length=np.subtract(Xn, Xo),
        lboundaries=lboundaries,
        rboundaries=rboundaries,
    )

    # Get default MPI Parameters from domain (even for serial jobs)
    mpi_params = MPIParams(comm=box.task_comm, task_id=box.current_task())

    # Setup usual implementation specific variables
    impl = args.impl
    enforce_implementation = args.enforce_implementation

    extra_op_kwds = {"mpi_params": mpi_params}
    if impl is Implementation.PYTHON:
        method = {}
    elif impl is Implementation.OPENCL:
        # For the OpenCL implementation we need to setup the compute device
        # and configure how the code is generated and compiled at runtime.

        # Create an explicit OpenCL context from user parameters
        from hysop.backend.device.opencl.opencl_tools import get_or_create_opencl_env

        cl_env = get_or_create_opencl_env(
            mpi_params=mpi_params,
            platform_id=args.cl_platform_id,
            device_id=args.cl_device_id,
        )

        # Configure OpenCL kernel generation and tuning (already done by HysopArgParser)
        from hysop.methods import OpenClKernelConfig

        method = {OpenClKernelConfig: args.opencl_kernel_config}

        # Setup opencl specific extra operator keyword arguments
        extra_op_kwds["cl_env"] = cl_env
    else:
        msg = f"Unknown implementation '{impl}'."
        raise ValueError(msg)

    # Define parameters and field (time, timestep, velocity, vorticity, enstrophy)
    t, dt = TimeParameters(dtype=args.dtype)
    velo = VelocityField(domain=box, dtype=args.dtype)
    vorti = VorticityField(velocity=velo)
    C = Field(
        domain=box,
        name="C",
        dtype=args.dtype,
        lboundaries=C_lboundaries,
        rboundaries=C_rboundaries,
    )
    S = Field(
        domain=box,
        name="S",
        dtype=args.dtype,
        lboundaries=S_lboundaries,
        rboundaries=S_rboundaries,
    )

    # Symbolic fields
    frame = velo.domain.frame
    Us = velo.s(*frame.vars)
    Ws = vorti.s(*frame.vars)
    Cs = C.s(*frame.vars)
    Ss = S.s(*frame.vars)
    dts = dt.s

    # Build the directional operators
    # > Directional advection
    advec = DirectionalAdvection(
        implementation=impl,
        name="advec",
        velocity=velo,
        advected_fields=(vorti, S),
        velocity_cfl=args.cfl,
        variables={velo: npts, vorti: npts, S: npts},
        dt=dt,
        **extra_op_kwds,
    )

    V0 = [0] * dim
    VP = [0] * dim
    VP[0] = Vp
    advec_C = DirectionalAdvection(
        implementation=impl,
        name="advec_C",
        velocity=velo,
        advected_fields=(C,),
        relative_velocity=VP,
        velocity_cfl=args.cfl,
        variables={velo: npts, C: npts},
        dt=dt,
        **extra_op_kwds,
    )

    # > Stretch vorticity
    if dim == 3:
        stretch = DirectionalStretching(
            implementation=impl,
            name="stretch",
            pretty_name="stretch",
            formulation=args.stretching_formulation,
            velocity=velo,
            vorticity=vorti,
            variables={velo: npts, vorti: npts},
            dt=dt,
            **extra_op_kwds,
        )
    elif dim == 2:
        stretch = None
    else:
        msg = f"Unsupported dimension {dim}."
        raise RuntimeError(msg)

    # > Diffusion of vorticity, S and C
    diffuse_S = Diffusion(
        implementation=impl,
        enforce_implementation=enforce_implementation,
        name="diffuse_S",
        pretty_name="diffS",
        nu=nu_S,
        Fin=S,
        variables={S: npts},
        dt=dt,
        dump_energy=dump_energy_ioparams,
        plot_inout_energy=IOParams(
            filepath=spectral_path,
            filename="E_S_diffusion_{ite}",
            frequency=args.dump_freq,
        ),
        **extra_op_kwds,
    )
    diffuse_C = Diffusion(
        implementation=impl,
        enforce_implementation=enforce_implementation,
        name="diffuse_C",
        pretty_name="diffC",
        nu=nu_C,
        Fin=C,
        variables={C: npts},
        dt=dt,
        dump_energy=dump_energy_ioparams,
        plot_inout_energy=IOParams(
            filepath=spectral_path,
            filename="E_C_diffusion_{ite}",
            frequency=args.dump_freq,
        ),
        **extra_op_kwds,
    )

    # > External force rot(-rho*g) = rot(Rs*S + C)
    Fext = np.zeros(shape=(dim,), dtype=object).view(SymbolicTensor)
    fext = -(Rs * Ss + Cs)
    Fext[0] = fext
    lhs = Ws.diff(frame.time)
    rhs = curl(Fext, frame)
    exprs = Assignment.assign(lhs, rhs)
    external_force = DirectionalSymbolic(
        name="Fext",
        implementation=impl,
        exprs=exprs,
        dt=dt,
        variables={vorti: npts, S: npts, C: npts},
        **extra_op_kwds,
    )

    splitting = StrangSplitting(splitting_dim=dim, order=args.strang_order)
    splitting.push_operators(advec, advec_C, stretch, external_force)

    # Build standard operators
    # > Poisson operator to recover the velocity from the vorticity
    poisson = PoissonCurl(
        name="poisson",
        velocity=velo,
        vorticity=vorti,
        variables={velo: npts, vorti: npts},
        diffusion=nu_W,
        dt=dt,
        implementation=impl,
        enforce_implementation=enforce_implementation,
        dump_energy=dump_energy_ioparams,
        plot_velocity_energy=IOParams(
            filepath=spectral_path,
            filename="E_velocity_{ite}",
            frequency=args.dump_freq,
        ),
        plot_inout_vorticity_energy=IOParams(
            filepath=spectral_path,
            filename="E_vorticity_{ite}",
            frequency=args.dump_freq,
        ),
        **extra_op_kwds,
    )

    # > Operator to compute the infinite norm of the velocity
    min_max_U = MinMaxFieldStatistics(
        name="min_max_U",
        field=velo,
        Finf=True,
        implementation=impl,
        variables={velo: npts},
        **extra_op_kwds,
    )
    # > Operator to compute the infinite norm of the vorticity
    min_max_W = MinMaxFieldStatistics(
        field=vorti,
        Finf=True,
        implementation=impl,
        variables={vorti: npts},
        **extra_op_kwds,
    )

    # > Operators to dump all fields
    io_params = IOParams(filename="fields", frequency=args.dump_freq)
    dump_fields = HDF_Writer(
        name="dump",
        io_params=io_params,
        force_backend=Backend.OPENCL,
        variables={vorti: npts, velo: npts, C: npts, S: npts},
        **extra_op_kwds,
    )

    # Adaptive timestep operator
    adapt_dt = AdaptiveTimeStep(
        dt, equivalent_CFL=True, name="merge_dt", pretty_name="dt", max_dt=1.0
    )
    dt_cfl = adapt_dt.push_cfl_criteria(
        cfl=args.cfl,
        Fmin=min_max_U.Fmin,
        Fmax=min_max_U.Fmax,
        equivalent_CFL=True,
        relative_velocities=[V0, VP],
        name="dt_cfl",
        pretty_name="CFL",
    )
    dt_advec = adapt_dt.push_advection_criteria(
        lcfl=args.lcfl,
        Finf=min_max_W.Finf,
        criteria=AdvectionCriteria.W_INF,
        name="dt_lcfl",
        pretty_name="LCFL",
    )

    # Create the problem we want to solve and insert our
    # directional splitting subgraph and the standard operators.
    # The method dictionnary passed to this graph will be dispatched
    # accross all operators contained in the graph.
    method.update(
        {
            ComputeGranularity: args.compute_granularity,
            SpaceDiscretization: args.fd_order,
            TimeIntegrator: args.time_integrator,
            Remesh: args.remesh_kernel,
        }
    )

    problem = Problem(method=method)
    problem.insert(
        poisson,
        diffuse_S,
        diffuse_C,
        # dump_fields,
        splitting,
        min_max_U,
        min_max_W,
        adapt_dt,
    )
    problem.build(args)

    # If a visu_rank was provided, and show_graph was set,
    # display the graph on the given process rank.
    if args.display_graph:
        problem.display(args.visu_rank)

    # Create a simulation
    # (do not forget to specify the t and dt parameters here)
    simu = Simulation(
        start=args.tstart,
        end=args.tend,
        nb_iter=args.nb_iter,
        max_iter=args.max_iter,
        dt0=args.dt,
        times_of_interest=args.times_of_interest,
        t=t,
        dt=dt,
    )
    simu.write_parameters(
        t,
        dt_cfl,
        dt_advec,
        dt,
        min_max_U.Finf,
        min_max_W.Finf,
        adapt_dt.equivalent_CFL,
        filename="parameters.txt",
        precision=8,
    )

    # Initialize vorticity, velocity, S and C on all topologies
    problem.initialize_field(field=velo, formula=init_velocity)
    problem.initialize_field(field=vorti, formula=init_vorticity)
    problem.initialize_field(field=C, formula=init_concentration, l0=l0)
    problem.initialize_field(field=S, formula=init_salinity, l0=l0)

    # Finally solve the problem
    problem.solve(
        simu,
        dry_run=args.dry_run,
        debug_dumper=args.debug_dumper,
        checkpoint_handler=args.checkpoint_handler,
    )

    # Finalize
    problem.finalize()


if __name__ == "__main__":
    from hysop_examples.argparser import HysopArgParser, colors

    class ParticleAboveSaltArgParser(HysopArgParser):
        def __init__(self):
            prog_name = "particle_above_salt_bc_3d"
            default_dump_dir = "{}/hysop_examples/{}".format(
                HysopArgParser.tmp_dir(), prog_name
            )

            description = colors.color(
                "HySoP Particles Above Salt Example: ", fg="blue", style="bold"
            )
            description += colors.color("[Meiburg 2014]", fg="yellow", style="bold")
            description += colors.color(
                "\nSediment-laden fresh water above salt water.", fg="yellow"
            )
            description += "\n"
            description += "\nThis example focuses on a validation study for the "
            description += (
                "hybrid particle-mesh vortex method in the Boussinesq approximation."
            )

            super().__init__(
                prog_name=prog_name,
                description=description,
                default_dump_dir=default_dump_dir,
            )

        def _add_main_args(self):
            args = super()._add_main_args()
            args.add_argument(
                "-Sc",
                "--schmidt",
                type=float,
                dest="schmidt",
                help="Schmidt number of salt.",
            )
            args.add_argument(
                "-tau",
                "--schmidt-ratio",
                type=float,
                dest="tau",
                help="Ratio between salt and sediment schmidt number",
            )
            args.add_argument(
                "-Vp",
                "--stokes-velocity",
                type=float,
                dest="Vp",
                help="Falling sediment imposed velocity.",
            )
            args.add_argument(
                "-Rs",
                "--density-expension-factor",
                type=float,
                dest="Rs",
                help="Density expension factor.",
            )
            return args

        def _check_main_args(self, args):
            super()._check_main_args(args)
            self._check_default(
                args, ("schmidt", "tau", "Vp", "Rs"), float, allow_none=False
            )
            self._check_positive(args, ("schmidt", "tau", "Vp", "Rs"))

        def _setup_parameters(self, args):
            super()._setup_parameters(args)
            dim = args.ndim
            if dim not in (2, 3):
                msg = "Domain should be 2D or 3D."
                self.error(msg)

    parser = ParticleAboveSaltArgParser()

    parser.set_defaults(
        impl="cl",
        ndim=3,
        npts=(64,),
        box_origin=(0.0,),
        box_length=(1.0,),
        tstart=0.0,
        tend=201.0,
        dt=1e-6,
        cfl=12.00,
        lcfl=0.95,
        dump_times=(25.0, 50.0, 75.0, 100.0, 125.0, 150.0, 175.0, 200.0),
        dump_freq=0,
        schmidt=7.0,
        tau=25.0,
        Vp=0.04,
        Rs=2.0,
    )

    parser.run(compute)
