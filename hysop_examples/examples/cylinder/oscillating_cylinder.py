# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# HySoP Example: Oscillating cylinder
# Quentin Desbonnets
# PhD: Methode d'homogeneisation pour la vibration de faisceaux de tubes en presence de fluide
# Example for only one cylinder

import os
import numpy as np


def init_vorticity(data, **kwds):
    data[...] = 0.0


def init_velocity(data, **kwds):
    data[...] = 0.0


def init_lambda(data, **kwds):
    data[...] = 0.0


def compute(args):
    from hysop import (
        Box,
        Simulation,
        Problem,
        MPIParams,
        IOParams,
        vprint,
        ScalarParameter,
    )
    from hysop.defaults import (
        VelocityField,
        VorticityField,
        DensityField,
        ViscosityField,
        LevelSetField,
        PenalizationField,
        EnstrophyParameter,
        TimeParameters,
        VolumicIntegrationParameter,
    )
    from hysop.constants import Implementation, AdvectionCriteria

    from hysop.operators import (
        DirectionalAdvection,
        DirectionalDiffusion,
        DirectionalStretching,
        PoissonCurl,
        AdaptiveTimeStep,
        Enstrophy,
        MinMaxFieldStatistics,
        StrangSplitting,
        ParameterPlotter,
        HDF_Writer,
        DirectionalSymbolic,
        AnalyticField,
    )

    from hysop.methods import (
        SpaceDiscretization,
        Remesh,
        TimeIntegrator,
        ComputeGranularity,
        Interpolation,
    )
    from hysop.numerics.odesolvers.runge_kutta import Euler, RK2, RK3, RK4

    from hysop.symbolic import sm, space_symbols, local_indices_symbols
    from hysop.symbolic.base import SymbolicTensor
    from hysop.symbolic.field import curl
    from hysop.symbolic.relational import Assignment, LogicalGT, LogicalLT, LogicalAND
    from hysop.symbolic.misc import Select
    from hysop.symbolic.tmp import TmpScalar
    from hysop.tools.string_utils import framed_str

    Kc = 5
    Re = 250

    T = 1.0
    D = 1.0
    E = 1.7

    H = 2 * E
    L = 2 * H
    N = 1024

    Vc = Kc * D / T
    mu = Vc * D / Re
    mu = ScalarParameter(name="mu", dtype=args.dtype, const=True, initial_value=mu)

    # Define the domain
    dim = 2
    npts = (int(H / E) * N, int(L / E) * N)
    box = Box(origin=(-H / 2, -L / 2), length=(H, L), dim=dim)

    # Get default MPI Parameters from domain (even for serial jobs)
    mpi_params = MPIParams(comm=box.task_comm, task_id=box.current_task())

    # Setup usual implementation specific variables
    impl = args.impl
    extra_op_kwds = {"mpi_params": mpi_params}
    if impl is Implementation.PYTHON:
        method = {}
    elif impl is Implementation.OPENCL:
        # For the OpenCL implementation we need to setup the compute device
        # and configure how the code is generated and compiled at runtime.

        # Create an explicit OpenCL context from user parameters
        from hysop.backend.device.opencl.opencl_tools import (
            get_or_create_opencl_env,
            get_device_number,
        )

        cl_env = get_or_create_opencl_env(
            mpi_params=mpi_params,
            platform_id=args.cl_platform_id,
            device_id=(
                box.machine_rank % get_device_number()
                if args.cl_device_id is None
                else None
            ),
        )

        # Configure OpenCL kernel generation and tuning (already done by HysopArgParser)
        from hysop.methods import OpenClKernelConfig

        method = {OpenClKernelConfig: args.opencl_kernel_config}

        # Setup opencl specific extra operator keyword arguments
        extra_op_kwds["cl_env"] = cl_env
    else:
        msg = f"Unknown implementation '{impl}'."
        raise ValueError(msg)

    # Define parameters and field (time, timestep, velocity, vorticity, enstrophy)
    t, dt = TimeParameters(dtype=args.dtype)
    velo = VelocityField(domain=box, dtype=args.dtype)
    vorti = VorticityField(velocity=velo)
    _lambda = PenalizationField(domain=box, dtype=args.dtype)

    # Symbolic fields
    frame = velo.domain.frame
    Us = velo.s(*frame.vars)
    Ws = vorti.s(*frame.vars)
    lambdas = _lambda.s(*frame.vars)
    ts = t.s
    dts = dt.s

    # Cylinder configuration
    X, Y = space_symbols[:2]
    Xc = (Kc / (2 * np.pi) * D) * sm.cos(2 * np.pi * ts / T)
    Yc = 0.0
    Uc = np.asarray([Xc.diff(ts), 0.0])
    Xs = LogicalLT((Xc - X) ** 2 + (Yc - Y) ** 2, D**2 / 4)

    compute_lambda = 1e8 * Xs
    cylinder = AnalyticField(
        name="cylinder",
        field=_lambda,
        formula=compute_lambda,
        variables={_lambda: npts},
        implementation=impl,
        **extra_op_kwds,
    )

    # Build the directional operators
    # > Directional penalization
    penalization = +dts * lambdas * (Uc - Us) / (1 + lambdas * dts)
    penalization = penalization.freeze()
    lhs = Ws
    rhs = curl(penalization, frame)
    exprs = Assignment.assign(lhs, rhs)
    penalization = DirectionalSymbolic(
        name="penalization",
        implementation=impl,
        exprs=exprs,
        fixed_residue=Ws,
        variables={vorti: npts, velo: npts, _lambda: npts},
        method={TimeIntegrator: Euler},
        dt=dt,
        **extra_op_kwds,
    )
    # > Directional advection
    advec = DirectionalAdvection(
        implementation=impl,
        name="advection",
        pretty_name="Adv",
        velocity=velo,
        advected_fields=(vorti,),
        velocity_cfl=args.cfl,
        variables={velo: npts, vorti: npts},
        dt=dt,
        **extra_op_kwds,
    )

    # > Directional stretching + diffusion
    if dim == 3:
        stretch = DirectionalStretching(
            implementation=impl,
            name="S",
            pretty_name="S",
            formulation=args.stretching_formulation,
            velocity=velo,
            vorticity=vorti,
            variables={velo: npts, vorti: npts},
            dt=dt,
            **extra_op_kwds,
        )
    else:
        stretch = None

    # > Directional splitting operator subgraph
    splitting = StrangSplitting(splitting_dim=dim, order=args.strang_order)
    splitting.push_operators(penalization, advec, stretch)

    # Build standard operators
    # > Poisson operator to recover the velocity from the vorticity
    poisson = PoissonCurl(
        name="poisson",
        velocity=velo,
        vorticity=vorti,
        variables={velo: npts, vorti: npts},
        diffusion=mu,
        dt=dt,
        projection=args.reprojection_frequency,
        implementation=impl,
        **extra_op_kwds,
    )

    # > Operators to dump rho and mu
    io_params = IOParams(filename="fields", frequency=args.dump_freq)
    dump_fields = HDF_Writer(
        name="dump",
        io_params=io_params,
        variables={velo: npts, vorti: npts, _lambda: npts},
    )

    # > Operator to compute the infinite norm of the velocity
    min_max_U = MinMaxFieldStatistics(
        field=velo,
        Finf=True,
        implementation=impl,
        variables={velo: npts},
        **extra_op_kwds,
    )
    # > Operator to compute the infinite norm of the vorticity
    min_max_W = MinMaxFieldStatistics(
        field=vorti,
        Finf=True,
        implementation=impl,
        variables={vorti: npts},
        **extra_op_kwds,
    )

    # Adaptive timestep operator
    dx = np.min(np.divide(box.length, np.asarray(npts) - 1))
    CFL_dt = (args.cfl * dx) / Kc
    msg = "CFL_dt={}"
    msg = msg.format(CFL_dt)
    msg = "\n" + framed_str(" CYLINDER EVOLUTION STABILITY CRITERIA ", msg)
    vprint(msg)
    max_dt = CFL_dt
    adapt_dt = AdaptiveTimeStep(
        dt,
        equivalent_CFL=True,
        max_dt=max_dt,
        name="merge_dt",
        pretty_name="dt",
    )
    dt_cfl = adapt_dt.push_cfl_criteria(
        cfl=args.cfl,
        Fmin=min_max_U.Fmin,
        Fmax=min_max_U.Fmax,
        equivalent_CFL=True,
        name="dt_cfl",
        pretty_name="CFL",
    )
    dt_advec = adapt_dt.push_advection_criteria(
        lcfl=args.lcfl,
        Finf=min_max_W.Finf,
        criteria=AdvectionCriteria.W_INF,
        name="dt_lcfl",
        pretty_name="LCFL",
    )

    # Create the problem we want to solve and insert our
    # directional splitting subgraph and the standard operators.
    # The method dictionnary passed to this graph will be dispatched
    # accross all operators contained in the graph.
    method.update(
        {
            ComputeGranularity: args.compute_granularity,
            SpaceDiscretization: args.fd_order,
            TimeIntegrator: args.time_integrator,
            Remesh: args.remesh_kernel,
        }
    )
    problem = Problem(method=method)
    problem.insert(
        cylinder, poisson, splitting, dump_fields, min_max_U, min_max_W, adapt_dt
    )
    problem.build(args)

    # If a visu_rank was provided, and show_graph was set,
    # display the graph on the given process rank.
    if args.display_graph:
        problem.display(args.visu_rank)

    # Create a simulation
    # (do not forget to specify the t and dt parameters here)
    simu = Simulation(
        start=args.tstart,
        end=args.tend,
        nb_iter=args.nb_iter,
        max_iter=args.max_iter,
        dt0=args.dt,
        times_of_interest=args.times_of_interest,
        t=t,
        dt=dt,
    )
    simu.write_parameters(
        t,
        dt_cfl,
        dt_advec,
        dt,
        min_max_U.Finf,
        min_max_W.Finf,
        adapt_dt.equivalent_CFL,
        filename="parameters.txt",
        precision=8,
    )

    # Initialize vorticity, velocity, viscosity and density on all topologies
    problem.initialize_field(field=velo, formula=init_velocity)
    problem.initialize_field(field=vorti, formula=init_vorticity)
    problem.initialize_field(field=_lambda, formula=init_lambda)

    # Finally solve the problem
    problem.solve(
        simu,
        dry_run=args.dry_run,
        debug_dumper=args.debug_dumper,
        checkpoint_handler=args.checkpoint_handler,
    )

    # Finalize
    problem.finalize()


if __name__ == "__main__":
    from hysop_examples.argparser import HysopArgParser, colors

    class OscillatingCylinderArgParser(HysopArgParser):
        def __init__(self):
            prog_name = "oscillating_cylinder"
            default_dump_dir = "{}/hysop_examples/{}".format(
                HysopArgParser.tmp_dir(), prog_name
            )

            description = colors.color(
                "HySoP Oscillating Cylinder Example: ", fg="blue", style="bold"
            )
            description += "\n"
            description += "\nThis example focuses on a validation study for the "
            description += "penalization with a immersed moving cylinder boundary."

            super().__init__(
                prog_name=prog_name,
                description=description,
                default_dump_dir=default_dump_dir,
            )

        def _setup_parameters(self, args):
            super()._setup_parameters(args)
            dim = args.ndim
            if dim not in (2, 3):
                msg = "Domain should be 2D or 3D."
                self.error(msg)

    parser = OscillatingCylinderArgParser()

    toi = tuple(np.linspace(0.0, 20.0, 20 * 24).tolist())

    parser.set_defaults(
        impl="cl",
        ndim=2,
        tstart=0.0,
        tend=20.1,
        dt=1e-6,
        cfl=0.5,
        lcfl=0.95,
        dump_freq=0,
        dump_times=toi,
    )

    parser.run(compute)
