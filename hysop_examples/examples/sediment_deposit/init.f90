	subroutine init(xp1,yp1,om,npart)


        include 'param.i'
        include 'param.h'
        include 'arrays.h'

      dimension xp1(*),yp1(*)
      dimension om(*)
	dimension xb(npg,npg),yb(npg,npg)

	pi2=2.*3.1415926
	ampl=0.05
        eps=dx2

	alambda=float(nb)


	pi=3.1415926
	spi=sqrt(pi)
	pi2=2.*pi

	npart=0
        do i=1,nx1
        do j=1,ny1
        xx=(float(i)-1.)*dx1
        yy=(float(j)-1.)*dx1
	strength=0.
	omg(i,j)=strength
	vxg(i,j)=0.
	vyg(i,j)=0.
	strg1(i,j)=0.
	strg2(i,j)=0.
        npart=npart+1
        xp1(npart)=xx
        yp1(npart)=yy
        om(npart)=strength
        enddo
        enddo

	ugmax=-1.
	ugmin=1.
c	ug > 0 dans les grains, <0 entre les grains
        do j=1,ny2
        yy=abs(float(j-1)*dx2)*alambda
	phase=rand()
	phase=0.
        do i=1,nx2
        xx=(abs(float(i-1)*dx2)+phase)*alambda
        yy=abs(float(j-1)*dx2)*alambda
	ug(i,j)=sin(pi2*xx)*sin(pi2*yy)
c	cas ou on advecte la densite
c        ug(i,j)=0.+
c     1           0.5*drho*(1.+tanh(ug(i,j)/(eps)))
	ug_init(i,j)=ug(i,j)
	ugmax=amax1(ugmax,ug(i,j))
	ugmin=amin1(ugmin,ug(i,j))
	enddo
	enddo

	print*,'ugmin et ugmax a init ',ugmin,ugmax

	return
	end
