# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
import scipy as sp
import sympy as sm
import numba as nb

TANK_RATIO = 3
SEDIMENT_COUNT = 2048 * TANK_RATIO
SEDIMENT_RADIUS = 0.5e-2
DISCRETIZATION = 512

# initialize vorticity


def init_vorticity(data, coords, component=None):
    # the flow is initially quiescent
    for d in data:
        d[...] = 0.0


# initialize velocity


def init_velocity(data, coords, component=None):
    # the flow is initially quiescent
    for d in data:
        d[...] = 0.0


def init_sediment(data, coords, nblobs, rblob):
    from hysop import vprint

    data = data[0]
    coords = coords[0]
    X, Y = coords
    R2 = rblob * rblob

    cache_file = "/tmp/C_init_{}_{}".format(
        "_".join(str(x) for x in data.shape),
        str(abs(hash((TANK_RATIO, nblobs, rblob)))),
    )
    try:
        D = np.load(file=cache_file + ".npz")
        vprint(f'  *Initializing sediments from cache: "{cache_file}.npz".')
        data[...] = D["data"]
    except:
        X, Y = X.ravel(), Y.ravel()
        dx, dy = X[1] - X[0], Y[1] - Y[0]
        Nx, Ny = X.size, Y.size
        Rx, Ry = 2 + int(rblob / dx), 2 + int(rblob / dy)
        assert rblob >= dx, "Sediment radius < dx."
        assert rblob >= dy, "Sediment radius < dy."

        Bx = 1 * np.random.rand(nblobs)
        By = 1 * np.random.rand(nblobs)
        Ix = np.floor(Bx / dx).astype(np.int32)
        Iy = np.floor(By / dy).astype(np.int32)
        Px = Bx - Ix * dx
        Py = By - Iy * dy

        from hysop.tools.numba_utils import make_numba_signature

        args = (Ix, Iy, Bx, By, data)
        signature, _ = make_numba_signature(*args)

        @nb.guvectorize(
            [signature],
            "(n),(n),(n),(n),(n0,n1)",
            target="parallel",
            nopython=True,
            cache=True,
        )
        def iter_blobs(Ix, Iy, Bx, By, data):
            for k in range(nblobs):
                ix, iy = Ix[k], Iy[k]
                px, py = Px[k], Py[k]
                for i in range(-Ry, +Ry):
                    ii = iy + i
                    if (ii < 0) or (ii >= Ny):
                        continue
                    dy2 = (py + i * dy) ** 2
                    for j in range(-Rx, +Rx):
                        jj = ix + j
                        if (jj < 0) or (jj >= Nx):
                            continue
                        dx2 = (px - j * dx) ** 2
                        d = dx2 + dy2
                        if d < R2:
                            data[ii, jj] = 0.5

        vprint(
            f"  *Initializing sediments of radius {rblob} with {nblobs} random blobs."
        )
        data[...] = 0.0
        iter_blobs(*args)

        # we cache initialization
        np.savez_compressed(file=cache_file, data=data)
        vprint(f'  *Caching data to "{cache_file}.npz".')


def compute(args):
    from hysop import (
        Field,
        Box,
        Simulation,
        Problem,
        MPIParams,
        IOParams,
        vprint,
        ScalarParameter,
    )
    from hysop.defaults import (
        VelocityField,
        VorticityField,
        DensityField,
        ViscosityField,
        LevelSetField,
        PenalizationField,
        EnstrophyParameter,
        TimeParameters,
        VolumicIntegrationParameter,
    )
    from hysop.constants import (
        Implementation,
        AdvectionCriteria,
        BoxBoundaryCondition,
        BoundaryCondition,
        Backend,
    )

    from hysop.operators import (
        DirectionalAdvection,
        DirectionalStretching,
        Diffusion,
        ComputeMeanField,
        PoissonCurl,
        AdaptiveTimeStep,
        Enstrophy,
        MinMaxFieldStatistics,
        StrangSplitting,
        ParameterPlotter,
        Integrate,
        HDF_Writer,
        CustomSymbolicOperator,
        DirectionalSymbolic,
        SpectralExternalForce,
        SymbolicExternalForce,
    )

    from hysop.methods import (
        SpaceDiscretization,
        Remesh,
        TimeIntegrator,
        ComputeGranularity,
        Interpolation,
    )

    from hysop.numerics.odesolvers.runge_kutta import Euler, RK2, RK3, RK4
    from hysop.symbolic import sm, space_symbols, local_indices_symbols
    from hysop.symbolic.base import SymbolicTensor
    from hysop.symbolic.field import curl
    from hysop.symbolic.relational import (
        Assignment,
        LogicalLE,
        LogicalGE,
        LogicalLT,
        LogicalGT,
    )
    from hysop.symbolic.misc import Select
    from hysop.symbolic.tmp import TmpScalar
    from hysop.tools.string_utils import framed_str

    # Constants
    dim = args.ndim
    if dim == 2:
        Xo = (0.0,) * dim
        Xn = (float(TANK_RATIO), 1.0)
        nblobs = SEDIMENT_COUNT
        rblob = SEDIMENT_RADIUS
        npts = args.npts
    else:
        msg = f"The {dim}D has not been implemented yet."
        raise NotImplementedError(msg)

    nu_S = ScalarParameter(
        name="nu_S", dtype=args.dtype, const=True, initial_value=1e-10
    )
    nu_W = ScalarParameter(
        name="nu_W", dtype=args.dtype, const=True, initial_value=1e-2
    )

    lboundaries = (BoxBoundaryCondition.SYMMETRIC, BoxBoundaryCondition.SYMMETRIC)
    rboundaries = (BoxBoundaryCondition.SYMMETRIC, BoxBoundaryCondition.SYMMETRIC)

    S_lboundaries = (
        BoundaryCondition.HOMOGENEOUS_NEUMANN,
        BoundaryCondition.HOMOGENEOUS_NEUMANN,
    )
    S_rboundaries = (
        BoundaryCondition.HOMOGENEOUS_NEUMANN,
        BoundaryCondition.HOMOGENEOUS_NEUMANN,
    )

    box = Box(
        origin=Xo,
        length=np.subtract(Xn, Xo),
        lboundaries=lboundaries,
        rboundaries=rboundaries,
    )

    # Get default MPI Parameters from domain (even for serial jobs)
    mpi_params = MPIParams(comm=box.task_comm, task_id=box.current_task())

    # Setup usual implementation specific variables
    impl = args.impl
    enforce_implementation = args.enforce_implementation
    extra_op_kwds = {"mpi_params": mpi_params}
    if impl is Implementation.PYTHON:
        method = {}
    elif impl is Implementation.OPENCL:
        # For the OpenCL implementation we need to setup the compute device
        # and configure how the code is generated and compiled at runtime.

        # Create an explicit OpenCL context from user parameters
        from hysop.backend.device.opencl.opencl_tools import (
            get_or_create_opencl_env,
            get_device_number,
        )

        cl_env = get_or_create_opencl_env(
            mpi_params=mpi_params,
            platform_id=args.cl_platform_id,
            device_id=(
                box.machine_rank % get_device_number()
                if args.cl_device_id is None
                else None
            ),
        )

        # Configure OpenCL kernel generation and tuning (already done by HysopArgParser)
        from hysop.methods import OpenClKernelConfig

        method = {OpenClKernelConfig: args.opencl_kernel_config}

        # Setup opencl specific extra operator keyword arguments
        extra_op_kwds["cl_env"] = cl_env
    else:
        msg = f"Unknown implementation '{impl}'."
        raise ValueError(msg)

    # Define parameters and field (time, timestep, velocity, vorticity, enstrophy)
    t, dt = TimeParameters(dtype=args.dtype)
    velo = VelocityField(domain=box, dtype=args.dtype)
    vorti = VorticityField(velocity=velo)
    S = Field(domain=box, name="S", dtype=args.dtype)
    # lboundaries=S_lboundaries, rboundaries=S_rboundaries)

    # Symbolic fields
    frame = velo.domain.frame
    Us = velo.s(*frame.vars)
    Ws = vorti.s(*frame.vars)
    Ss = S.s(*frame.vars)
    dts = dt.s

    # Build the directional operators
    # > Directional advection
    advec = DirectionalAdvection(
        implementation=impl,
        name="advec",
        velocity=velo,
        advected_fields=(vorti, S),
        velocity_cfl=args.cfl,
        variables={velo: npts, vorti: npts, S: npts},
        dt=dt,
        **extra_op_kwds,
    )

    # > Stretch vorticity
    if dim == 3:
        stretch = DirectionalStretching(
            implementation=impl,
            name="S",
            pretty_name="S",
            formulation=args.stretching_formulation,
            velocity=velo,
            vorticity=vorti,
            variables={velo: npts, vorti: npts},
            dt=dt,
            **extra_op_kwds,
        )
    elif dim == 2:
        stretch = None
    else:
        msg = f"Unsupported dimension {dim}."
        raise RuntimeError(msg)

    splitting = StrangSplitting(splitting_dim=dim, order=args.strang_order)
    splitting.push_operators(advec, stretch)

    # Build standard operators
    # > Poisson operator to recover the velocity from the vorticity
    poisson = PoissonCurl(
        name="poisson",
        velocity=velo,
        vorticity=vorti,
        variables={velo: npts, vorti: npts},
        diffusion=nu_W,
        dt=dt,
        implementation=impl,
        enforce_implementation=enforce_implementation,
        **extra_op_kwds,
    )

    # > External force rot(-rho*g) = rot(-(1+S)) = rot(-S)
    g = 9.81
    Fext = SymbolicExternalForce(name="S", Fext=(0, -g * Ss), diffusion={S: nu_S})
    external_force = SpectralExternalForce(
        name="Fext",
        vorticity=vorti,
        dt=dt,
        Fext=Fext,
        Finf=True,
        implementation=impl,
        variables={vorti: npts, S: npts},
        **extra_op_kwds,
    )

    # > Operator to compute the infinite norm of the velocity
    min_max_U = MinMaxFieldStatistics(
        name="min_max_U",
        field=velo,
        Finf=True,
        implementation=impl,
        variables={velo: npts},
        **extra_op_kwds,
    )
    # > Operator to compute the infinite norm of the vorticity
    min_max_W = MinMaxFieldStatistics(
        field=vorti,
        Finf=True,
        implementation=impl,
        variables={vorti: npts},
        **extra_op_kwds,
    )

    # > Operators to dump all fields
    io_params = IOParams(filename="fields", frequency=args.dump_freq)
    dump_fields = HDF_Writer(
        name="dump",
        io_params=io_params,
        force_backend=Backend.OPENCL,
        variables={velo: npts, vorti: npts, S: npts},
        **extra_op_kwds,
    )

    # > Operator to compute and save mean fields
    axes = list(range(1, dim))
    view = [
        slice(None, None, None),
    ] * dim
    view = tuple(view)
    io_params = IOParams(filename="horizontally_averaged_profiles", frequency=0)
    compute_mean_fields = ComputeMeanField(
        name="mean", fields={S: (view, axes)}, variables={S: npts}, io_params=io_params
    )

    # Adaptive timestep operator
    adapt_dt = AdaptiveTimeStep(
        dt, equivalent_CFL=True, name="merge_dt", pretty_name="dt"
    )
    dt_cfl = adapt_dt.push_cfl_criteria(
        cfl=args.cfl,
        Finf=min_max_U.Finf,
        equivalent_CFL=True,
        name="dt_cfl",
        pretty_name="CFL",
    )
    dt_advec = adapt_dt.push_advection_criteria(
        lcfl=args.lcfl,
        Finf=min_max_W.Finf,
        criteria=AdvectionCriteria.W_INF,
        name="dt_lcfl",
        pretty_name="LCFL",
    )
    dt_force = adapt_dt.push_cst_criteria(
        cst=10000, Finf=external_force.Finf, name="dt_force", pretty_name="FEXT"
    )

    # Create the problem we want to solve and insert our
    # directional splitting subgraph and the standard operators.
    # The method dictionnary passed to this graph will be dispatched
    # accross all operators contained in the graph.
    method.update(
        {
            ComputeGranularity: args.compute_granularity,
            SpaceDiscretization: args.fd_order,
            TimeIntegrator: args.time_integrator,
            Remesh: args.remesh_kernel,
        }
    )

    problem = Problem(method=method)
    problem.insert(
        poisson,
        dump_fields,
        min_max_U,
        min_max_W,
        adapt_dt,
        splitting,
        compute_mean_fields,
        external_force,
    )
    problem.build(args)

    # If a visu_rank was provided, and show_graph was set,
    # display the graph on the given process rank.
    if args.display_graph:
        problem.display(args.visu_rank)

    # Create a simulation
    # (do not forget to specify the t and dt parameters here)
    simu = Simulation(
        start=args.tstart,
        end=args.tend,
        nb_iter=args.nb_iter,
        max_iter=args.max_iter,
        dt0=args.dt,
        times_of_interest=args.times_of_interest,
        t=t,
        dt=dt,
    )
    simu.write_parameters(
        t,
        dt_cfl,
        dt_advec,
        dt,
        min_max_U.Finf,
        min_max_W.Finf,
        adapt_dt.equivalent_CFL,
        filename="parameters.txt",
        precision=8,
    )

    # Initialize vorticity, velocity, S on all topologies
    problem.initialize_field(field=velo, formula=init_velocity)
    problem.initialize_field(field=vorti, formula=init_vorticity)
    problem.initialize_field(
        field=S, formula=init_sediment, nblobs=nblobs, rblob=rblob, without_ghosts=True
    )

    # Finally solve the problem
    problem.solve(
        simu,
        dry_run=args.dry_run,
        debug_dumper=args.debug_dumper,
        checkpoint_handler=args.checkpoint_handler,
    )

    # Finalize
    problem.finalize()


if __name__ == "__main__":
    from hysop_examples.argparser import HysopArgParser, colors

    class ParticleAboveSaltArgParser(HysopArgParser):
        def __init__(self):
            prog_name = "sediment_deposit"
            default_dump_dir = "{}/hysop_examples/{}".format(
                HysopArgParser.tmp_dir(), prog_name
            )

            description = colors.color(
                "HySoP Sediment Deposit Example: ", fg="blue", style="bold"
            )
            description += "\n"
            description += "\nThis example focuses on a validation study for the "
            description += "hybrid particle-mesh vortex method for sediment deposit."

            super().__init__(
                prog_name=prog_name,
                description=description,
                default_dump_dir=default_dump_dir,
            )

        def _setup_parameters(self, args):
            super()._setup_parameters(args)
            dim = args.ndim
            if dim not in (2, 3):
                msg = "Domain should be 2D or 3D."
                self.error(msg)

    parser = ParticleAboveSaltArgParser()

    parser.set_defaults(
        impl="cl",
        ndim=2,
        npts=(TANK_RATIO * DISCRETIZATION + 1, DISCRETIZATION + 1),
        box_origin=(0.0,),
        box_length=(1.0,),
        tstart=0.0,
        tend=20.0,
        dt=1e-6,
        cfl=32.0,
        lcfl=0.90,
        # dump_times=tuple(float(x) for x in range(0,100000,1000)),
        dump_freq=10,
    )

    parser.run(compute)
