# Copyright (c) HySoP 2011-2024
#
# This file is part of HySoP software.
# See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
# for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import argparse
import tempfile
import colors
import textwrap
import warnings
import contextlib
import tee
import re
import errno
import shutil
import psutil
import sys
import functools
import atexit
from argparse_color_formatter import ColorHelpFormatter
import numpy as np

# Fix a bug in the tee module #########


class FixTee:
    def flush(self):
        if (self.stream is not None):
            self.stream.flush()
        if (self.fp is not None):
            self.fp.flush()
            os.fsync(self.fp.fileno())


class StdoutTee(FixTee, tee.StdoutTee):
    pass


class StderrTee(FixTee, tee.StderrTee):
    pass
########################################


class HysopArgumentWarning(UserWarning):
    pass


class SplitAppendAction(argparse._AppendAction):
    def __init__(self, *args, **kwds):
        separator = kwds.pop('sep', ',')
        container = kwds.pop('container', tuple)
        convert = kwds.pop('convert', lambda x: x)
        append = kwds.pop('append', False)
        super().__init__(*args, **kwds)
        self._separator = separator
        self._container = container
        self._convert = convert
        self._append = append

    def __call__(self, parser, namespace, values, option_string=None):
        if isinstance(values, str):
            for c in ('(', '{', '[', ']', '}', ')'):
                values = values.replace(c, '')
            try:
                values = tuple(self._convert(v) for v in values.split(self._separator))
            except:
                msg = 'Failed to convert \'{}\' to {} of {}s for parameter {}.'
                msg = msg.format(values, self._container.__name__, self._convert.__name__,
                                 self.dest)
                parser.error(msg)
        else:
            try:
                values = tuple(values)
            except:
                msg = f'Could not convert values \'{values}\' to tuple for parameter {self.dest}.'
                parser.error(msg)
        if self._append:
            items = getattr(namespace, self.dest, None)
            items = self._container() if (items is None) else items
        else:
            items = self._container()
        if (self._container is list):
            items.append(values)
        elif (self._container is tuple):
            items += values
        elif (self._container is set):
            items.update(values)
        else:
            msg = f'Unknown container type {self._container}.'
            raise TypeError(msg)
        setattr(namespace, self.dest, items)


class EvalAction(argparse._AppendAction):
    def __init__(self, *args, **kwds):
        container = kwds.pop('container', tuple)
        append = kwds.pop('append', False)
        super().__init__(*args, **kwds)
        self._container = container
        self._append = append

    def __call__(self, parser, namespace, values, option_string=None):
        assert isinstance(values, str), type(values)
        try:
            values = eval(values)
        except:
            msg = f"Failed to convert \'{values}\' using eval function"
            parser.error(msg)
        if self._append:
            items = argparse._ensure_value(namespace, self.dest, self._container())
        else:
            items = self._container()
        if (self._container is list):
            items.append(values)
        elif (self._container is tuple):
            items += values
        elif (self._container is set):
            items.update(values)
        else:
            msg = f'Unknown container type {self._container}.'
            raise TypeError(msg)
        setattr(namespace, self.dest, items)


class HysopArgParser(argparse.ArgumentParser):

    @classmethod
    def set_tmp_dir(cls, tempdir):
        # default tmp dir is in order:
        # TMPDIR, TEMP, TMP, default system tmp, current working directory
        tempfile.tempdir = tempdir

    @classmethod
    def tmp_dir(cls):
        return tempfile.gettempdir()

    @classmethod
    def get_fs_type(cls, path):
        import subprocess
        cmd = ['stat', '-f', '-c', '%T', path]
        fs_type = subprocess.check_output(cmd)
        return fs_type.replace('\n', '')

    @classmethod
    def is_shared_fs(cls, path):
        return (cls.get_fs_type(path) in ('nfs',))

    @classmethod
    def set_env(cls, target, value, hysop=True):
        if hysop:
            target = f'HYSOP_{target}'
        if (value is None):
            pass
        elif (value is True):
            os.environ[target] = '1'
        elif (value is False):
            os.environ[target] = '0'
        elif isinstance(value, str):
            os.environ[target] = value
        elif isinstance(value, int):
            os.environ[target] = str(value)
        else:
            msg = f'Invalid value of type {type(value)}.'
            raise TypeError(msg)

    # custom actions
    split = SplitAppendAction
    eval = EvalAction

    def _get_formatter(self):
        # try to find out terminal width (max=120 characters)
        try:
            TERMINAL_WIDTH = min(int(os.popen('stty size', 'r').read().split()[1]), 120)
        except:
            TERMINAL_WIDTH = min(os.environ.get('COLUMNS', 80), 120)
        formatter_opts = {'prog': self.prog, 'width': TERMINAL_WIDTH}
        return self.formatter_class(**formatter_opts)

    def __init__(self, prog_name, description,
                 domain=None, default_dump_dir=None,
                 generate_io_params=None,
                 **kwds):

        prog = prog_name
        epilog = colors.color('[ADDITIONAL NOTES]', fg='yellow', style='bold')
        epilog += '\nOpenCL and Autotuner parameters only have an effect on the OpenCL backend.'
        epilog += '\nWhen default arguments are not specified, hysop defaults are used.'

        super().__init__(prog=prog, description=description,
                                             formatter_class=HysopHelpFormatter, epilog=epilog,
                                             add_help=False,
                                             **kwds)

        if (domain is None):
            domain = 'box'
        if (default_dump_dir is None):
            default_dump_dir = f'{self.tmp_dir()}/hysop/{prog_name}'
        if (generate_io_params is None):
            generate_io_params = ()
        generate_io_params += ('checkpoint',)

        self.default_dump_dir = default_dump_dir

        self._domain = domain

        self._add_positional_args()
        self._add_main_args()
        self._add_domain_args()
        self._add_simu_args()
        self._add_problem_args()
        self._add_method_args()
        self._add_threading_args()
        self._add_opencl_args()
        self._add_autotuner_args()
        self._add_graphical_io_args()
        self._add_file_io_args(default_dump_dir, generate_io_params)
        self._add_term_io_args()
        self._add_misc_args()

    def pre_process_args(self, args):
        pass

    def run(self, program, **kwds):
        args = self.parse_args()
        args.__class__ = HysopNamespace

        # SETUP I/O
        from mpi4py import MPI
        size = MPI.COMM_WORLD.Get_size()
        rank = MPI.COMM_WORLD.Get_rank()
        self.size = size
        self.rank = rank

        self.default_dump_dir = self._fmt_filename(self.default_dump_dir, rank, size,
                                                   args.dump_dir, self.default_dump_dir)

        self.pre_process_args(args)

        self._check_term_io_args(args)
        self._check_file_io_args(args)

        args.stdout = self._fmt_filename(args.stdout, rank, size, args.dump_dir, self.default_dump_dir)
        args.stderr = self._fmt_filename(args.stderr, rank, size, args.dump_dir, self.default_dump_dir)
        self._rmfile(args.stdout)
        self._rmfile(args.stderr)

        if args.clean:
            dump_dirs = {args.dump_dir, args.autotuner_dump_dir}
            dump_dirs.update(getattr(args, f'{pname}_dump_dir')
                             for pname in self.generate_io_params)
            dump_dirs = filter(lambda ddir: isinstance(ddir, str), dump_dirs)
            dump_dirs = map(lambda ddir: os.path.abspath(ddir), dump_dirs)
            dump_dirs = filter(lambda ddir: os.path.isdir(ddir) and
                               (ddir not in ('/', '/home', '~', os.path.expanduser('~'))), dump_dirs)
            dump_dirs = tuple(dump_dirs)
            if args.no_interactive:
                confirm_deletion = True
            else:
                msg = 'HySoP will clean the following directories prior to launch:'
                for ddir in dump_dirs:
                    msg += f'\n  {ddir}'
                print(msg)
                valid = {"yes": True, "y": True,
                         "no": False, "n": False,
                         '': True}
                confirm_deletion = None
                while (confirm_deletion is None):
                    prompt = 'Please confirm this action [Y/n]: '
                    sys.stdout.write(prompt)
                    choice = raw_input().lower().strip()
                    if (choice in valid):
                        confirm_deletion = valid[choice]

            assert isinstance(confirm_deletion, bool)
            if confirm_deletion:
                for ddir in dump_dirs:
                    # tar should be kept for checkpoint dumps
                    self._rmfiles(ddir, 'txt')
                    self._rmfiles(ddir, 'out')
                    self._rmfiles(ddir, 'log')
                    self._rmfiles(ddir, 'png')
                    self._rmfiles(ddir, 'jpg')
                    self._rmfiles(ddir, 'eps')
                    self._rmfiles(ddir, 'pdf')
                    self._rmfiles(ddir, 'xml')
                    self._rmfiles(ddir, 'json')
                    self._rmfiles(ddir, 'h5')
                    self._rmfiles(ddir, 'xmf')
                    self._rmfiles(ddir, 'cl')
                    self._rmfiles(ddir, 'sim')
                    self._rmfiles(ddir, 'npy')
                    self._rmfiles(ddir, 'npz')
                    self._rmfiles(ddir, 'pklz')
                    self._rmdir(ddir, 'generated_kernels', force=True)
                    self._rmdir(ddir, 'spectral', force=True)
            else:
                print('Deletion skipped by user.')

        MPI.COMM_WORLD.Barrier()

        with self.redirect_stdout(rank, size, args), self.redirect_stderr(rank, size, args):
            # Build hysop environment (without importing hysop)
            self._check_threading_args(args)
            self._setup_hysop_env(args)

            # /!\ only import hysop from there on /!\
            # (env. variables have been correctly set)
            import hysop
            from hysop import vprint

            # register exit handler
            def at_exit(rank, size, args):
                if (rank in args.tee_ranks):
                    vprint(self._rank_filter(f'Logs have been dumped to \'{args.stdout}\'.',
                                             rank=rank, size=size))
                MPI.COMM_WORLD.Barrier()
                if (size > 1) and (rank == args.tee_ranks[0]):
                    vprint()
            atexit.register(at_exit, rank=rank, size=size, args=args)

            # check remaining arguments
            self._check_positional_args(args)
            self._check_main_args(args)
            self._check_domain_args(args)
            self._check_problem_args(args)
            self._check_simu_args(args)
            self._check_method_args(args)
            self._check_opencl_args(args)
            self._check_autotuner_args(args)
            self._check_graphical_io_args(args)
            self._check_misc_args(args)

            # setup arguments
            self._setup_parameters(args)
            self._setup_implementation(args)

            MPI.COMM_WORLD.Barrier()

            # filter numpy arrays and run program
            from hysop.tools.contexts import printoptions
            with printoptions(threshold=10000, linewidth=240,
                              nanstr='nan', infstr='inf',
                              formatter={'float': lambda x: f'{x:>6.2f}'}):
                program(args, **kwds)

    @staticmethod
    def _fmt_filename(filename, rank, size, dump_dir, default_dump_dir, **kwds):
        return filename.format(rank=rank, size=size,
                               dump_dir=dump_dir,
                               default_dump_dir=default_dump_dir,
                               **kwds)

    @staticmethod
    def _color_filter(msg):
        return re.sub(r'(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]', "", msg)

    @staticmethod
    def _null_filter(msg):
        return None

    @staticmethod
    def _rank_filter(msg, rank, size):
        if (size > 1):
            prefix = f'\n[P{rank}]  '
            return msg.replace('\n', prefix)
        else:
            return msg

    @staticmethod
    def _mkdir(path, dirname=True):
        path = os.path.realpath(path)
        if dirname:
            path = os.path.dirname(path)
        try:
            os.makedirs(path)
        except OSError as e:
            if (e.errno != errno.EEXIST):
                raise

    @staticmethod
    def _rmfile(path):
        try:
            os.remove(path)
        except OSError as e:
            if (e.errno != errno.ENOENT):
                raise

    @classmethod
    def _rmfiles(cls, path, ext):
        ext = f'.{ext}'
        if os.path.exists(path):
            for fname in os.listdir(path):
                if fname.endswith(ext):
                    fname = f'{path}/{fname}'
                    cls._rmfile(fname)

    @classmethod
    def _rmdir(cls, path, subdir, force=False):
        path += '/' + subdir
        assert path not in ('/', '/tmp')
        if os.path.exists(path):
            msg = 'Are you sure you want to delete {} y/n ?'
            msg = msg.format(path)
            remove = 'u'
            try:
                if force:
                    shutil.rmtree(path)
                else:
                    while(remove not in ('y', 'n')):
                        remove = raw_input(msg)
                    if (remove == 'y'):
                        shutil.rmtree(path)
            except OSError as e:
                if (e.errno != errno.ENOENT):
                    raise

    @contextlib.contextmanager
    def redirect_stdout(self, rank, size, args):
        redirect_to_terminal = (rank in args.tee_ranks)

        file_filters = [self._color_filter]
        if redirect_to_terminal:
            stream_filters = [functools.partial(self._rank_filter, rank=rank, size=size)]
        else:
            stream_filters = [self._null_filter]

        self._mkdir(args.stdout)
        with StdoutTee(args.stdout, mode='a', buff=-1,
                       file_filters=file_filters,
                       stream_filters=stream_filters):
            yield

    @contextlib.contextmanager
    def redirect_stderr(self, rank, size, args):
        redirect_to_terminal = (rank in args.tee_ranks)

        file_filters = [self._color_filter]
        if redirect_to_terminal:
            stream_filters = [functools.partial(self._rank_filter, rank=rank, size=size)]
        else:
            stream_filters = [self._null_filter]

        self._mkdir(args.stderr)
        with StderrTee(args.stderr, mode='a', buff=-1,
                       file_filters=file_filters,
                       stream_filters=stream_filters):
            yield

    def _add_positional_args(self):
        pass

    def _check_positional_args(self, args):
        pass

    def _add_main_args(self):
        args = self.add_argument_group('Main parameters')
        args.add_argument('-impl', '--implementation', type=str, default='python',
                          dest='impl',
                          help='Backend implementation (either python, fortran or opencl).')
        args.add_argument('-cp', '--compute-precision', type=str, default='fp32',
                          dest='compute_precision',
                          help='Floating-point precision used to discretize the parameters and fields.')
        args.add_argument('-ei', '--enforce-implementation', type=str, default='true',
                          dest='enforce_implementation',
                          help='If set to false, the library may use another implementation than user specified one for some operators.')
        return args

    def _check_main_args(self, args):
        self._check_default(args, ('impl', 'compute_precision'), str, allow_none=False)
        args.impl = self._convert_implementation('impl', args.impl)
        args.compute_precision = self._convert_precision('compute_precision',
                                                         args.compute_precision)
        args.dtype = self._precision_to_dtype('compute_precision', args.compute_precision)
        args.enforce_implementation = self._convert_bool('enforce_implementation', args.enforce_implementation)

    def _add_domain_args(self):
        discretization = self.add_argument_group('Discretization parameters')
        discretization.add_argument('-dim', '--ndim', type=int, default=2,
                                    dest='ndim',
                                    help='Number of dimensions.')
        if (self._domain == 'box'):
            discretization.add_argument('-d', '--discretization', type=str, default=(64,),
                                        action=self.split, container=tuple, convert=int, append=False,
                                        dest='npts',
                                        help='Cartesian discretization, number of points in each direction.')
            discretization.add_argument('-sd', '--scalar-discretization', type=str, default=None,
                                        action=self.split, container=tuple, convert=int, append=False,
                                        dest='snpts',
                                        help='Cartesian discretization, number of points in each direction for scalars.')
            discretization.add_argument('-gr', '--grid-ratio', type=int, default=1,
                                        dest='grid_ratio',
                                        help='Cartesian discretization, number of points in each direction for scalars compared to base velocity and vorticity grid.')
            discretization.add_argument('-bo', '--box-origin', type=str, default=(0.0,),
                                        action=self.split, container=tuple, convert=float, append=False,
                                        dest='box_origin',
                                        help='Physical coordinates of left-most corner of the domain.')
            discretization.add_argument('-bl', '--box-length', type=str, default=(1.0,),
                                        action=self.split, container=tuple, convert=float, append=False,
                                        dest='box_length',
                                        help='Physical length of the box.')
            return discretization
        else:
            msg = f'Unknown domain value {self._domain}.'
            raise ValueError(msg)

    def _check_domain_args(self, args):
        dim = args.ndim
        if (dim <= 0):
            msg = f'Negative or zero dimension, got {dim}.'
            self.error(msg)
        if (self._domain == 'box'):
            npts = args.npts
            snpts = args.snpts
            box_origin = args.box_origin
            box_length = args.box_length
            self._check_default(args, ('npts', 'box_origin', 'box_length'), tuple)
            if len(npts) == 1:
                npts *= dim
            if len(npts) != dim:
                msg = 'Discretization should be of the same size as the dimension.'
                msg += f'\nGot {npts} but ndim={dim}.'
                self.error(msg)
            if any(x <= 0 for x in npts):
                msg = 'Negative discretization encountered.'
                msg += f'\nGot {npts}.'
                self.error(msg)
            if len(box_origin) == 1:
                box_origin *= dim
            if len(box_origin) != dim:
                msg = 'Box start should be of the same size as the dimension.'
                msg += f'\nGot {box_origin} but ndim={dim}.'
                self.error(msg)
            if len(box_length) == 1:
                box_length *= dim
            if len(box_length) != dim:
                msg = 'Box length should be of the same size as the dimension.'
                msg += f'\nGot {box_length} but ndim={dim}.'
                self.error(msg)
            if any(x <= 0 for x in box_length):
                msg = 'Negative box length encountered.'
                msg += f'\nGot {box_length}.'
                self.error(msg)
            if snpts:
                if len(snpts) == 1:
                    snpts *= dim
                if len(snpts) != dim:
                    msg = 'Discretization should be of the same size as the dimension.'
                    msg += f'\nGot {snpts} but ndim={dim}.'
                    self.error(msg)
                if any(x <= 0 for x in snpts):
                    msg = 'Negative discretization encountered.'
                    msg += f'\nGot {snpts}.'
                    self.error(msg)
            elif args.grid_ratio:
                assert args.grid_ratio >= 1
                gr = args.grid_ratio
                snpts = tuple(di*gr for di in npts)
            else:
                snpts = npts[:]
            args.npts = npts
            args.snpts = snpts
            args.box_origin = box_origin
            args.box_length = box_length
        else:
            msg = f'Unknown domain value {self._domain}.'
            raise ValueError(msg)

    def _add_simu_args(self):
        simu = self.add_argument_group('Simulation parameters')
        simu.add_argument('-ts', '--tstart', type=float, default=0.0,
                          dest='tstart',
                          help='Set simulation initial time.')
        simu.add_argument('-te', '--tend', type=float, default=10.0,
                          dest='tend',
                          help='Set simulation end time.')
        simu.add_argument('-niter', '--nb-iterations', type=int, default=None,
                          dest='nb_iter',
                          help='Number of iterations.')
        simu.add_argument('-maxiter', '--max-iterations', type=int, default=None,
                          dest='max_iter',
                          help=('Maximal number of iterations at witch the simulation will stop ' +
                                '[default=no limit].'))
        simu.add_argument('-dt', '--timestep', type=float, default=None,
                          dest='dt',
                          help=('Specify timestep instead of a number of iterations ' +
                                '(has priority over number of iterations).' +
                                ' This will be the initial timestep when using adaptive timestep.'))
        simu.add_argument('-fts', '--fixed-timestep', default=False, action='store_true',
                          dest='fixed_timestep',
                          help='Disable variable timestepping. In this case, timestep has to be specified with -dt or -nb_iter.')
        simu.add_argument('-mindt', '--min-timestep', type=float,
                          default=np.finfo(np.float64).eps, dest='min_dt',
                          help='Enforce a minimal timestep.')
        simu.add_argument('-maxdt', '--max-timestep', type=float, default=np.inf,
                          dest='max_dt',
                          help='Enforce a maximal timestep.')
        simu.add_argument('-cfl', '--cfl', type=float, default=None,
                          dest='cfl',
                          help='Specify CFL for adaptive time stepping.')
        simu.add_argument('-lcfl', '--lagrangian-cfl', type=float, default=None,
                          dest='lcfl',
                          help='Specify LCFL for adaptive time stepping.')

    def _add_problem_args(self):
        problem = self.add_argument_group('Problem parameters')
        problem.add_argument('-stopi', '--stop-at-initialization', default=False, action='store_true',
                             dest='stop_at_initialization',
                             help='Stop execution before problem initialization.')
        problem.add_argument('-stopd', '--stop-at-discretization', default=False, action='store_true',
                             dest='stop_at_discretization',
                             help='Stop execution before problem discretization.')
        problem.add_argument('-stopwp', '--stop-at-work-properties', default=False, action='store_true',
                             dest='stop_at_work_properties',
                             help='Stop execution before problem work properties retrieval.')
        problem.add_argument('-stopwa', '--stop-at-work-allocation', default=False, action='store_true',
                             dest='stop_at_work_allocation',
                             help='Stop execution before problem work properties allocation.')
        problem.add_argument('-stops', '--stop-at-setup', default=False, action='store_true',
                             dest='stop_at_setup',
                             help='Stop execution before problem setup.')
        problem.add_argument('-stopb', '--stop-at-build', default=False, action='store_true',
                             dest='stop_at_build',
                             help='Stop execution once the problem has been built.')
        problem.add_argument('-dr', '--dry-run', default=False, action='store_true',
                             dest='dry_run',
                             help='Stop execution before the first simulation iteration.')
        return problem

    def _check_problem_args(self, args):
        self._check_default(args, ('stop_at_initialization', 'stop_at_discretization', 'stop_at_setup',
                                   'stop_at_work_properties', 'stop_at_work_allocation', 'stop_at_build'), bool, allow_none=False)

    def _check_simu_args(self, args):
        self._check_default(args, ('tstart', 'tend'), float)
        self._check_default(args, 'dt', float, allow_none=True)
        self._check_default(args, 'nb_iter', int, allow_none=True)
        self._check_default(args, ('dry_run', 'fixed_timestep'), bool, allow_none=False)
        self._check_positive(args, ('dt', 'min_dt', 'max_dt'), strict=True, allow_none=True)
        self._check_positive(args, 'nb_iter', strict=True, allow_none=True)
        self._check_positive(args, 'max_iter', strict=True, allow_none=True)
        self._check_positive(args, 'cfl', strict=True, allow_none=True)
        self._check_positive(args, 'lcfl', strict=True, allow_none=True)
        if ((args.nb_iter is not None) and (args.dt is not None)):
            msg = ('\nTimestep and number of iterations specified in the same time, '
                   + 'using timestep.')
            warnings.warn(msg, HysopArgumentWarning)
        if ((args.min_dt is not None) and (args.max_dt is not None)
                and (args.min_dt > args.max_dt)):
            msg = '\nmin_dt > max_dt'
            self.error(msg)
        if args.fixed_timestep and (args.dt is None) and (args.nb_iter is None):
            msg = 'Fixed timestep requires a timestep or a number of iterations to be specified.'
            self.error(msg)
        args.variable_timestep = not args.fixed_timestep

    def _add_method_args(self):
        method = self.add_argument_group('Method parameters')
        method.add_argument('-cg', '--compute-granularity', type=int,
                            default=0,
                            dest='compute_granularity',
                            help='Set default operator compute granularity.')
        method.add_argument('-fdo', '--finite-differences-order', type=int,
                            default=4,
                            dest='fd_order',
                            help='Set default order of finite differences stencils.')
        method.add_argument('-ti', '--time-integrator', type=str,
                            default='RK2',
                            dest='time_integrator',
                            help='Set the default time integrator.')
        method.add_argument('-rk', '--remesh-kernel', type=str,
                            default='L4_2',
                            dest='remesh_kernel',
                            help='Set the default remeshing formula for advection-remeshing.')
        method.add_argument('-interp', '--interpolation-filter', type=str,
                            default='polynomial',
                            dest='interpolation_filter',
                            help='Set the default interpolation formula to compute subgrid field values.')
        method.add_argument('-restrict', '--restriction-filter', type=str,
                            default='polynomial',
                            dest='restriction_filter',
                            help='Set the default restriction formula to pass from fine grids to coarse ones.')
        method.add_argument('-pi', '--polynomial-interpolator', type=str,
                            default='LINEAR',
                            dest='polynomial_interpolator',
                            help='Set the default polynomial interpolator for polynomial interpolation or restriction methods.')
        method.add_argument('-ai', '--advection-interpolator', type=str,
                            default='LEGACY',
                            dest='advection_interpolator',
                            help='Set the default polynomial interpolator for bilevel advection. Legacy interpolator uses linear interpolation, else specify a custom polynomial interpolator.')
        method.add_argument('-sf', '--stretching-formulation', type=str,
                            default='conservative',
                            dest='stretching_formulation',
                            help='Set the default stretching formulation.')
        method.add_argument('-rf', '--reprojection-frequency', type=int,
                            default=0,
                            dest='reprojection_frequency',
                            help='Set the solenoidal reprojection frequency of the vorticity.')
        method.add_argument('-dso', '--directional-splitting-order', type=int,
                            default=2,
                            dest='strang_order',
                            help='Set the default directional splitting order.')
        method.add_argument('-sdm', '--scalars-diffusion-mode', type=str,
                            default='spectral', dest='scalars_diffusion_mode',
                            help='Enforce either spectral or directional diffusion with finite differences for scalars. Vorticity diffusion mode may not be enforced by this parameter, see --vorticity-diffusion-mode. The default value is spectral diffusion mode.')
        method.add_argument('-vdm', '--vorticity-diffusion-mode', type=str,
                            default='spectral', dest='vorticity_diffusion_mode',
                            help='Enforce either spectral or directional diffusion with finite differences for vorticity. Vorticity is a special case for diffusion because vorticity diffusion can be computed at the same time as the Poisson operator which recovers the velocity. Spectral diffusion of vorticity when divergence-free field projection is enabled is virtually free. The default value is spectral diffusion mode.')
        method.add_argument('--enable-diffusion-substepping',
                            dest='enable_diffusion_substepping', default=False, action='store_true',
                            help='Do not restrict timestep because of finite difference directional diffusion CFL but enforce substepping inside the operator depending on current timestep.')

        return method

    def _check_method_args(self, args):
        self._check_default(args, 'enable_diffusion_substepping', bool, allow_none=False)
        self._check_default(args, ('compute_granularity', 'fd_order', 'strang_order',
                                   'reprojection_frequency'), int, allow_none=False)
        self._check_default(args, ('time_integrator', 'remesh_kernel', 'interpolation_filter',
                                   'restriction_filter', 'stretching_formulation', 'polynomial_interpolator',
                                   'vorticity_diffusion_mode', 'scalars_diffusion_mode'),
                            str, allow_none=False)
        self._check_positive(args, 'compute_granularity', strict=False, allow_none=False)
        self._check_positive(args, 'fd_order', strict=True, allow_none=False)
        self._check_positive(args, 'strang_order', strict=True, allow_none=False)
        self._check_positive(args, 'reprojection_frequency', strict=False, allow_none=False)

        args.strang_order = self._convert_strang_order('strang_order',
                                                       args.strang_order)
        args.time_integrator = self._convert_time_integrator('time_integrator',
                                                             args.time_integrator)
        args.remesh_kernel = self._convert_remesh_kernel('remesh_kernel',
                                                         args.remesh_kernel)
        args.stretching_formulation = self._convert_stretching_formulation(
            'stretching_formulation', args.stretching_formulation)
        args.interpolation_filter = \
            self._convert_filtering_method('interpolation_filter',  args.interpolation_filter,
                                           allow_subgrid=True)
        args.restriction_filter = \
            self._convert_filtering_method('restriction_filter', args.restriction_filter,
                                           allow_subgrid=False)
        args.polynomial_interpolator = \
            self._convert_polynomial_interpolation('polynomial_interpolator', args.polynomial_interpolator)
        args.advection_interpolator = \
            self._convert_advection_interpolation('advection_interpolator', args.advection_interpolator)
        self._check_and_set_diffusion_mode('vorticity_diffusion_mode', args)
        self._check_and_set_diffusion_mode('scalars_diffusion_mode', args)

    def _add_threading_args(self):
        threading = self.add_argument_group('threading parameters')
        msg = "Enable threads for backends that supports it (Numba and FFTW) by setting HYSOP_ENABLE_THREADS. "
        msg += "Disabling threading will limit all threading backends to one thread and set numba default backend to 'cpu' instead of 'parallel'."
        threading.add_argument('--enable-threading', type=str, default='1',
                               dest='enable_threading',
                               help=msg)
        msg = 'Set the default maximum usable threads for threading backends (OpenMP, MKL) and operator backends using threads (Numba, FFTW). '
        msg += 'This parameter will set HYSOP_MAX_THREADS and does not affect the OpenCL backend. '
        msg += "If this parameter is set to 'physical', the maximum number of threads will be set to the number of physical cores available to the process (taking into account the cpuset). "
        msg += "If set to 'logical', logical cores will be chosen instead. Else this parameter expects a positive integer. "
        msg += 'If --enable-threads is set to False, this parameter is ignored and HYSOP_MAX_THREADS will be set to 1.'
        threading.add_argument('--max-threads', type=str, default='physical',
                               dest='max_threads',
                               help=msg)
        threading.add_argument('--openmp-threads', type=str, default=None,
                               dest='openmp_threads',
                               help='This parameter will set OMP_NUM_THREADS to a custom value (overrides --max-threads).')
        threading.add_argument('--mkl-threads', type=str, default=None,
                               dest='mkl_threads',
                               help='This parameter will set MKL_NUM_THREADS to a custom value (overrides --max-threads).')
        threading.add_argument('--mkl-domain-threads', type=str, default=None,
                               dest='mkl_domain_threads',
                               help='This parameter will set MKL_DOMAIN_NUM_THREADS to a custom value (overrides --max-threads).')
        threading.add_argument('--mkl-threading-layer', type=str, default='TBB',
                               dest='mkl_threading_layer',
                               help="This parameter will set MKL_THREADING_LAYER to a custom value ('TBB', 'GNU', 'INTEL', 'SEQUENTIAL').")
        threading.add_argument('--numba-threads', type=str, default=None,
                               dest='numba_threads',
                               help='This parameter will set NUMBA_NUM_THREADS to a custom value (overrides --max-threads).')
        threading.add_argument('--numba-threading-layer', type=str, default='workqueue',
                               dest='numba_threading_layer',
                               help="This parameter will set NUMBA_THREADING_LAYER to a custom value ('workqueue' is available on all platforms, but not 'omp' and 'tbb'). Use 'numba -s' to list available numba threading layers.")
        threading.add_argument('--fftw-threads', type=str, default=None,
                               dest='fftw_threads',
                               help='This parameter will set HYSOP_FFTW_NUM_THREADS to a custom value (overrides --max-threads).')
        threading.add_argument('--fftw-planner-effort', type=str, default='estimate',
                               dest='fftw_planner_effort',
                               help='Set default planning effort for FFTW plans. The actual number of threads used by FFTW may depend on the planning step. This parameter will set HYSOP_FFTW_PLANNER_EFFORT.')
        threading.add_argument('--fftw-planner-timelimit', type=str, default='-1',
                               dest='fftw_planner_timelimit',
                               help='Set an approximate upper bound in seconds for FFTW planning. This parameter will set HYSOP_FFTW_PLANNER_TIMELIMIT.')
        return threading

    def _add_opencl_args(self):
        opencl = self.add_argument_group('OpenCL parameters')
        opencl.add_argument('--opencl-platform-id', type=int, default=None,
                            dest='cl_platform_id',
                            help=('R|OpenCL platform id for the OpenCL implementation.'
                                  + '\nDefaults to hysop.__DEFAULT_PLATFORM_ID__.'))
        opencl.add_argument('--opencl-device-id', type=int, default=None,
                            dest='cl_device_id',
                            help=('R|OpenCL device id for the OpenCL implementation.'
                                  + '\nDefaults to hysop.__DEFAULT_DEVICE_ID__.'))
        opencl.add_argument('--opencl-device-type', type=str, default=None,
                            dest='cl_device_type',
                            help='OpenCL device type for the OpenCL implementation.')
        opencl.add_argument('--opencl-build-options', type=str, default=[],
                            action=self.split, container=list, append=True,
                            dest='cl_build_opts',
                            help='Extra OpenCL build options.')
        opencl.add_argument('--opencl-defines', type=int, default=[],
                            action=self.split, container=list, append=True,
                            dest='cl_defines',
                            help='Extra OpenCL defines.')
        opencl.add_argument('--opencl-fp-precision', type=str, default=None,
                            dest='cl_fp_precision',
                            help=('Force OpenCL kernels floating-point precision.'
                                  + '\nEither fp16, fp32 or fp64.'))
        opencl.add_argument('--opencl-fp-dump-mode', type=str, default=None,
                            dest='cl_fp_dump_mode',
                            help=('OpenCL floating-point dumping format. ' +
                                  'Either dec (decimal) or hex (hexadecimal).'))
        opencl.add_argument('--opencl-enable-short-circuit', type=str, default=None,
                            dest='cl_enable_short_circuit',
                            help='Enable short circuits for code-generated OpenCL kernels.')
        opencl.add_argument('--opencl-enable-loop-unrolling', type=str, default=None,
                            dest='cl_enable_loop_unrolling',
                            help='Enable loop unrolling for code-generated OpenCL kernels.')
        return opencl

    def _check_threading_args(self, args):
        self._check_default(args, ('enable_threading', 'max_threads', 'mkl_threading_layer', 'numba_threading_layer',
                                   'fftw_planner_effort', 'fftw_planner_timelimit'), str, allow_none=False)
        self._check_default(args, ('openmp_threads', 'mkl_threads', 'mkl_domain_threads', 'numba_threads', 'fftw_threads'),
                            str, allow_none=True)

        args.enable_threading = self._convert_bool('enable_threading', args.enable_threading)
        if args.enable_threading:
            args.max_threads = self._convert_threads('max_threads', args.max_threads, default=None)
        else:
            args.max_threads = 1
        for argname in ('openmp_threads', 'mkl_threads', 'numba_threads', 'fftw_threads'):
            setattr(args, argname, self._convert_threads(argname, getattr(args, argname),
                                                         default=args.max_threads))
        args.mkl_threading_layer = self._convert_mkl_threading_layer('mkl_threading_layer',
                                                                     args.mkl_threading_layer)
        args.numba_threading_layer = self._convert_numba_threading_layer('numba_threading_layer',
                                                                         args.numba_threading_layer)
        args.fftw_planner_effort = self._convert_fftw_planner_effort('fftw_planner_effort',
                                                                     args.fftw_planner_effort)

    def _check_opencl_args(self, args):
        self._check_default(args, ('cl_platform_id', 'cl_device_id'), int, allow_none=True)
        self._check_default(args, ('cl_build_opts', 'cl_defines'), list, allow_none=False)
        self._check_default(args, ('cl_device_type', 'cl_fp_precision', 'cl_fp_dump_mode'),
                            str, allow_none=True)
        self._check_default(args, ('cl_enable_short_circuit', 'cl_enable_loop_unrolling'),
                            str, allow_none=True)

        self._check_positive(args, ('cl_platform_id', 'cl_device_id'), allow_none=True)

        args.cl_device_type = self._convert_device_type('cl_device_type', args.cl_device_type)
        args.cl_fp_precision = self._convert_precision('cl_fp_precision', args.cl_fp_precision)
        args.cl_fp_dump_mode = self._convert_fp_dump_mode('cl_fp_dump_mode',
                                                          args.cl_fp_dump_mode)
        args.cl_enable_short_circuit = self._convert_bool('cl_enable_short_circuit',
                                                          args.cl_enable_short_circuit)
        args.cl_enable_loop_unrolling = self._convert_bool('cl_enable_loop_unrolling',
                                                           args.cl_enable_loop_unrolling)

    def _build_opencl_kernel_config(self, args, autotuner_config):
        from hysop.methods import OpenClKernelConfig
        kernel_config = OpenClKernelConfig(autotuner_config=autotuner_config,
                                           user_build_options=args.cl_build_opts,
                                           user_size_constants=args.cl_defines,
                                           precision=args.cl_fp_precision,
                                           float_dump_mode=args.cl_fp_dump_mode,
                                           use_short_circuit_ops=args.cl_enable_short_circuit,
                                           unroll_loops=args.cl_enable_loop_unrolling)
        return kernel_config

    def _add_autotuner_args(self):
        autotuner = self.add_argument_group('Kernel autotuner parameters')
        autotuner.add_argument('--autotuner-dump-dir', type=str, default=None,
                               dest='autotuner_dump_dir',
                               help='Configure kernel autotuner dump directory.')
        autotuner.add_argument('--autotuner-cache-override',
                               default=False, action='store_true',
                               dest='autotuner_cache_override',
                               help=('Override kernel autotuner cached data. Best kernels candidates will be stored in '
                                     + 'a temporary directory instead of persistant system-wide cache directory.'))
        autotuner.add_argument('--autotuner-flag', type=str, default=None,
                               dest='autotuner_flag',
                               help=('Configure kernel autotuner rigor flag'
                                     + ' (either estimate,measure,patient or exhaustive).'))
        autotuner.add_argument('--autotuner-nruns', type=int, default=None,
                               dest='autotuner_nruns',
                               help='Configure kernel autotuner initial kernel profiling runs.')
        autotuner.add_argument('--autotuner-prune-threshold', type=float, default=None,
                               dest='autotuner_prune_threshold',
                               help='Configure kernel autotuner kernel pruning threshold.')
        autotuner.add_argument('--autotuner-max-candidates', type=int, default=None,
                               dest='autotuner_max_candidates',
                               help='Configure kernel autotuner maximal number of candidate kernels.')
        autotuner.add_argument('--autotuner-verbose', type=int, default=None,
                               dest='autotuner_verbose',
                               help='Configure kernel autotuner kernel verbosity (0 to 5).')
        autotuner.add_argument('--autotuner-debug',
                               default=False, action='store_true',
                               dest='autotuner_debug',
                               help='Configure kernel autotuner kernel debug flag.')
        autotuner.add_argument('--autotuner-dump-kernels',
                               default=False, action='store_true',
                               dest='autotuner_dump_kernels',
                               help='Configure kernel autotuner kernel source dumping.')
        autotuner.add_argument('--autotuner-dump-isolation',
                               default=False, action='store_true',
                               dest='autotuner_dump_isolation',
                               help='Configure kernel autotuner to generate oclgrind kernel isolation files for each optimal kernel.')
        autotuner.add_argument('--autotuner-dump-hash-logs',
                               default=False, action='store_true',
                               dest='autotuner_dump_hash_logs',
                               help=('Configure kernel autotuner to generate kernel extra keywords hash logs '
                                     + 'for kernel caching debugging purposes.'))
        autotuner.add_argument('--autotuner-filter-statistics',
                               type=str, default='.*',
                               dest='autotuner_filter_statistics',
                               help=('Space separated list of regular expressions to match against kernel names. '
                                     + 'A kernel that matches becomes candidate for statistics, postprocessing, source and isolation dump, if enabled. '
                                     + 'If not specified, all kernels are considered by default by using the generic \'.*\' pattern.'))
        autotuner.add_argument('--autotuner-plot-statistics',
                               default=False, action='store_true',
                               dest='autotuner_plot_statistics',
                               help='Compute and plot tuning statistics for all tuned kernels.')
        autotuner.add_argument('--autotuner-bench-kernels',
                               default=False, action='store_true',
                               dest='autotuner_bench_kernels',
                               help=('Enable standard bench mode for kernels: search without max candidates '
                                     + 'at maximum verbosity with cache override and nruns=8. '
                                     + 'Prune threshold and autotuner flag are however not modified.'))
        autotuner.add_argument('--autotuner-postprocess-kernels', type=str, default=None,
                               dest='autotuner_postprocess_kernels',
                               help=('Run a custom command after each final generated kernel: '
                                     + 'command  FILE_BASENAME  FROM_CACHE  AUTOTUNER_DUMP_DIR  AUTOTUNER_NAME  KERNEL_NAME  '
                                     + 'MEAN_EXECUTION_TIME_NS  MIN_EXECUTION_TIME_NS  MAX_EXECUTION_TIME_NS  '
                                     + 'KERNEL_SOURCE_FILE  KERNEL_ISOLATION_FILE  KERNEL_HASH_LOGS_FILE  '
                                     + 'VENDOR_NAME  DEVICE_NAME  WORK_SIZE  WORK_LOAD  GLOBAL_WORK_SIZE  '
                                     + 'LOCAL_WORK_SIZE  EXTRA_PARAMETERS  EXTRA_KWDS_HASH  SRC_HASH. '
                                     + 'See hysop/tools/postprocess_kernel.sh for an example of post processing script.'))
        autotuner.add_argument('--autotuner-postprocess-nruns', type=int, default=16,
                               dest='autotuner_postprocess_nruns',
                               help='Number of time to run the best obtained kernel with autotuning_mode set to False.')

        return autotuner

    def _check_autotuner_args(self, args):
        self._check_default(args, ('autotuner_flag', 'autotuner_dump_dir',
                                   'autotuner_postprocess_kernels', 'autotuner_filter_statistics'),
                            str, allow_none=True)
        self._check_default(args, ('autotuner_nruns', 'autotuner_max_candidates',
                                   'autotuner_verbose', 'autotuner_postprocess_nruns'), int, allow_none=True)
        self._check_default(args, ('autotuner_dump_kernels',
                                   'autotuner_dump_isolation',
                                   'autotuner_dump_hash_logs',
                                   'autotuner_bench_kernels',
                                   'autotuner_plot_statistics'),
                            bool, allow_none=True)
        self._check_default(args, 'autotuner_prune_threshold', float, allow_none=True)

        self._check_positive(args, ('autotuner_nruns', 'autotuner_max_candidates',
                                    'autotuner_postprocess_nruns'),
                             strict=True, allow_none=True)
        self._check_positive(args, 'autotuner_verbose', strict=False, allow_none=True)
        self._check_range(args, 'autotuner_prune_threshold', 1.0, 5.0, allow_none=True)
        self._check_dir(args, 'autotuner_dump_dir', allow_shared=True, allow_none=True)

        args.autotuner_flag = self._convert_autotuner_flag('autotuner_flag',
                                                           args.autotuner_flag)

        patterns = filter(lambda x: len(x),  args.autotuner_filter_statistics.split(' '))
        patterns = tuple(re.compile(e) for e in patterns)

        def filter_statistics(kernel_name, patterns=patterns):
            return any(pat.match(kernel_name) for pat in patterns)
        args.autotuner_filter_statistics = filter_statistics

        if args.autotuner_bench_kernels:
            args.autotuner_nruns = 8
            args.autotuner_max_candidates = np.iinfo(np.int64).max
            args.autotuner_verbose = np.iinfo(np.int64).max
            args.autotuner_cache_override = True

    def _build_autotuner_config(self, args):
        from hysop.methods import OpenClKernelAutotunerConfig
        if (args.autotuner_cache_override is None):
            override_cache = args.override_cache
        else:
            override_cache = args.autotuner_cache_override
        autotuner_config = OpenClKernelAutotunerConfig(
            autotuner_flag=args.autotuner_flag,
            nruns=args.autotuner_nruns,
            prune_threshold=args.autotuner_prune_threshold,
            max_candidates=args.autotuner_max_candidates,
            verbose=args.autotuner_verbose,
            debug=args.autotuner_debug,
            dump_kernels=args.autotuner_dump_kernels,
            dump_hash_logs=args.autotuner_dump_hash_logs,
            generate_isolation_file=args.autotuner_dump_isolation,
            plot_statistics=args.autotuner_plot_statistics,
            override_cache=override_cache,
            dump_folder=args.autotuner_dump_dir,
            postprocess_kernels=args.autotuner_postprocess_kernels,
            postprocess_nruns=args.autotuner_postprocess_nruns,
            filter_statistics=args.autotuner_filter_statistics)
        return autotuner_config

    def _add_file_io_args(self, default_dump_dir, generate_io_params):

        file_io = self.add_argument_group('File I/O')

        file_io.add_argument('--dump-dir', type=str, default=default_dump_dir,
                             dest='dump_dir',
                             help=('Global output directory for all IO params.'
                                   + f' Overrides HYSOP_DUMP_DIR.'))
        file_io.add_argument('--dump-freq', type=int, default=0,
                             dest='dump_freq',
                             help=('Global output frequency in terms of number of iterations for all IO params.'
                                   + ' Use 0 to disable frequency based dumping.'))
        file_io.add_argument('--dump-period', type=float, default=0.0,
                             dest='dump_period',
                             help=('Global output frequency in terms of period for all IO params.'
                                   + ' This will append linspace(tstart, tend, int((tend-tstart)/dump_period)) to times of interest.'
                                   + ' Use 0.0 to disable period based dumping.'))
        file_io.add_argument('--dump-times', type=str, default=None, convert=float, nargs='?', const=tuple(),
                             action=self.split, container=tuple, append=False,
                             dest='dump_times',
                             help='Global comma delimited list of additional output times of interest for all io_params.')
        file_io.add_argument('--dump-tstart', type=float, default=None,
                             dest='dump_tstart',
                             help='Set global starting time at which output are dumped for all IO params. Defaults to simulation start.')
        file_io.add_argument('--dump-tend', type=float, default=None,
                             dest='dump_tend',
                             help='Set global end time at which output are dumped for all IO params. Defaults to simulation end.')
        file_io.add_argument('--dump-last', action='store_true', dest='dump_last',
                             help='If set, always dump on last simulation iteration.')
        file_io.add_argument('--dump-postprocess', type=str, default=None,
                             dest='postprocess_dump',
                             help=('Run a custom command after I/O dump: '
                                   + 'command FILENAME\n'
                                   + 'See hysop/tools/postprocess_dump.sh for an example of post processing script.\n'
                                   + 'I/O can be postprocessed directly from RAM by setting --enable-ram-fs.'))
        file_io.add_argument('--dump-is-temporary', default=False, action='store_true',
                             dest='dump_is_temporary',
                             help='Delete dumped data files after callback has been executed. Best used with --enable-ram-fs and --dump-post-process.')
        file_io.add_argument('--enable-ram-fs', default=False, action='store_true',
                             dest='enable_ram_fs',
                             help='Dump I/O directly into RAM (if possible), else fallback to --dump-dir unless --force-ram-fs has been set.')
        file_io.add_argument('--force-ram-fs', default=False, action='store_true',
                             dest='force_ram_fs',
                             help='Dump I/O directly into RAM. Raises an EnvironmentError when no ramfs is available. Implies --enable-ram-fs. When enabled --dump-dir is ignored.')
        file_io.add_argument('--hdf5-disable-compression', default=False, action='store_true',
                             dest='hdf5_disable_compression',
                             help='Disable compression for HDF5 outputs (when available).')
        file_io.add_argument('--hdf5-disable-slicing', default=False, action='store_true',
                             dest='hdf5_disable_slicing',
                             help=('Disable HDF5 slicing that is obtained with XDMF JOIN. '
                                   'May reduce performances when HDF5 slicing applies (<= 16 processes slab topologies).'
                                   'Enabling this option guarantees a single HDF5 file for all processes per dump.'))
        
        file_io.add_argument('--disable-file-locks', default=False, action='store_true',
                 dest='disable_file_locks',
                 help=('Disable file locking mechanism which restricts access to hysop cache files among multiple processes. '
                       'This may be necessary on some platforms or when running in containers like singularity.'))

        # list of additional named io_params to be generated
        assert (generate_io_params is not None), generate_io_params
        assert 'checkpoint' in generate_io_params, generate_io_params

        self.generate_io_params = generate_io_params
        for pname in generate_io_params:
            if (pname == 'checkpoint'):
                description = ('Configure problem checkpoints I/O parameters, dumped checkpoints represent simulation states '
                               'that can be loaded back to continue the simulation later on.')
                pargs = self.add_argument_group(f'{pname.upper()} I/O', description=description)
                pargs.add_argument('-L', '--load-checkpoint', default=None, const='checkpoint.tar', nargs='?', type=str, dest='load_checkpoint_path',
                                   help=('Begin simulation from this checkpoint. Can be given as fullpath or as a filename relative to --checkpoint-dump-dir. '
                                         'The given checkpoint has to be compatible with the problem it will be loaded to. '
                                         'This will only work if parameter names, variable names, operator names, discretization and global topology information remain unchanged. '
                                         'Operator ordering, boundary conditions, data ordering, data permutation and MPI layouts may be however be changed. '
                                         'Defaults to {checkpoint_output_dir}/checkpoint.tar if no filename is specified.'))
                pargs.add_argument('-S', '--save-checkpoint', default=None, const='checkpoint.tar', nargs='?', type=str, dest='save_checkpoint_path',
                                   help=('Enable simulation checkpoints to be able to restart simulations from a specific point later on. '
                                         'Can be given as fullpath or as a filename relative to --checkpoint-dump-dir. '
                                         'Frequency or time of interests for checkpoints can be configured by using global FILE I/O parameters or '
                                         'specific --checkpoint-dump-* arguments which takes priority over global ones. '
                                         'Should not be to frequent for efficiency reasons. May be used in conjunction with --load-checkpoint, '
                                         'in which case the starting checkpoint may be overwritten in the case the same path are given. '
                                         'Defaults to {checkpoint_output_dir}/checkpoint.tar if no filename is specified.'))
                pargs.add_argument('--checkpoint-compression-method', type=str, default=None, dest='checkpoint_compression_method',
                                   help='Set the compression method used by the Blosc meta-compressor for checkpoint array data. Defaults to zstd.')
                pargs.add_argument('--checkpoint-compression-level', type=int, default=None, dest='checkpoint_compression_level',
                                   help='Set the compression level used by the Blosc meta-compressor for checkpoint array data, from 0 (no compression) to 9 (maximum compression). Defaults to 6.')
                pargs.add_argument('--checkpoint-relax-constraints', action='store_true', dest='checkpoint_relax_constraints',
                                   help=('Relax field/parameter checks when loading a checkpoint. This allows for a change in datatype, '
                                         'boundary conditions, ghost count and topology shape when reloading a checkpoint. '
                                         'Useful to continue a simulation with a different precision, different compute backend, '
                                         'different boundary conditions or with a different number of processes.'))
            else:
                pargs = self.add_argument_group(f'{pname.upper()} I/O')

            assert isinstance(pname, str), pname
            pargs.add_argument(f'--{pname}-dump-dir',
                               type=str, default=None,
                               dest=f'{pname}_dump_dir',
                               help=f'Custom output directory for custom IO parameter \'{pname}.\'')
            pargs.add_argument(f'--{pname}-dump-freq',
                               type=int, default=None,
                               dest=f'{pname}_dump_freq',
                               help=f'Custom output frequency in terms of number of iterations for IO parameter \'{pname}.\'')
            pargs.add_argument(f'--{pname}-dump-period',
                               type=float, default=None,
                               dest=f'{pname}_dump_period',
                               help=f'Custom output frequency in terms of period for IO parameter \'{pname}.\'')
            pargs.add_argument(f'--{pname}-dump-times', nargs='?', const=tuple(),
                               action=self.split, container=tuple, append=False,
                               type=str, default=None, convert=float,
                               dest=f'{pname}_dump_times',
                               help=f'Comma delimited list of additional output times of interest for IO parameter \'{pname}.\'')
            pargs.add_argument(f'--{pname}-dump-tstart',
                               type=float, default=None,
                               dest=f'{pname}_dump_tstart',
                               help=f'Set starting time at which output are dumped for IO parameter \'{pname}\'.')
            pargs.add_argument(f'--{pname}-dump-tend',
                               type=float, default=None,
                               dest=f'{pname}_dump_tend',
                               help=f'Set end time at which output are dumped for IO parameter \'{pname}\'.')
            pargs.add_argument(f'--{pname}-dump-last', action='store_true',
                               dest=f'{pname}_dump_last',
                               help=f'If set, always dump on last simulation iteration for IO parameter \'{pname}\'')
            pargs.add_argument(f'--{pname}-dump-postprocess', type=str, default=None,
                               dest=f'{pname}_postprocess_dump',
                               help=(f'Run a custom command after {pname} I/O dump: '
                                     + 'command FILENAME\n'
                                     + 'See hysop/tools/postprocess_dump.sh for an example of post processing script.\n'
                                     + f'{pname} I/O can be postprocessed directly from RAM by setting --enable-ram-fs.'))
            pargs.add_argument(f'--{pname}-dump-is-temporary', default=None, action='store_true',
                               dest=f'{pname}_dump_is_temporary',
                               help=f'Delete {pname} data files after callback has been executed. Best used with --enable-ram-fs and --dump-post-process.')
            pargs.add_argument(f'--{pname}-enable-ram-fs', default=None, action='store_true',
                               dest=f'{pname}_enable_ram_fs',
                               help='Dump I/O directly into RAM (if possible), else fallback to --dump-dir unless --force-ram-fs has been set.')
            pargs.add_argument(f'--{pname}-force-ram-fs', default=None, action='store_true',
                               dest=f'{pname}_force_ram_fs',
                               help=f'Dump {pname} I/O directly into RAM (if possible), else raise an EnvironmentError. Implies --enable-ram-fs. When enabled --dump-dir is ignored.')
            pargs.add_argument(f'--{pname}-hdf5-disable-compression', default=None, action='store_true',
                               dest=f'{pname}_hdf5_disable_compression',
                               help=f'Disable compression for {pname} HDF5 outputs (when available).')
            pargs.add_argument(f'--{pname}-hdf5-disable-slicing', default=False, action='store_true',
                               dest=f'{pname}_hdf5_disable_slicing',
                               help=f'Disable HDF5 slicing that is obtained with XDMF JOIN for {pname}.')
            setattr(file_io, f'{pname}_io', pargs)

        file_io.add_argument('--cache-dir', type=str, default=None,
                             dest='cache_dir',
                             help=('Specify an alternative HySoP caching directory.'
                                   + ' Overrides HYSOP_CACHE_DIR.'))
        file_io.add_argument('--override-cache', default=None, action='store_true',
                             dest='override_cache',
                             help='Ignore cached data.')
        file_io.add_argument('--debug-dump-dir', type=str,
                             default=None,
                             dest='debug_dump_dir',
                             help=('Target root directory for debug dumps. Debug dumps will appear into <dump dir>/<target>. Defaults to global hysop dump_dir.'))
        file_io.add_argument('--debug-dump-target', type=str, default=None,
                             dest='debug_dump_target',
                             help=('Tag for field debug dumps. Debug dumps will appear into <dump dir>/<target>.'))
        file_io.add_argument('-C', '--clean', action='store_true', default=False,
                             dest='clean',
                             help=('Clean the dump_folders (default dump, autotuner dump and extra_io_params directories) prior to launch. '
                                   + 'Remove all files matching the following extensions: '
                                   + 'txt, out, log, png, jpg, eps, pdf, xml, json, h5, xmf, cl, sim, npz, pklz. '
                                   + 'The user will be prompted to confirm action prior to cleaning unless --no-interactive or -N is passed.'))

        return file_io

    def _check_file_io_args(self, args):
        pnames = self.generate_io_params

        self._check_default(args, ('debug_dump_dir', 'debug_dump_target'), str, allow_none=True)
        self._check_default(args, 'override_cache', bool, allow_none=True)
        self._check_default(args, 'clean', bool, allow_none=True)
        self._check_default(args, ('cache_dir', 'postprocess_dump'), str, allow_none=True)
        self._check_dir(args, 'cache_dir', allow_shared=False, allow_none=True)
        self._check_default(args, ('no_interactive', 'dump_is_temporary',
                                   'enable_ram_fs', 'force_ram_fs', 
                                   'hdf5_disable_compression', 'hdf5_disable_slicing',
                                   'disable_file_locks'),
                            bool, allow_none=False)

        self._check_default(args,  'dump_dir',    str,        allow_none=False)
        self._check_default(args,  'dump_freq',   int,        allow_none=True)
        self._check_default(args,  'dump_period', float,      allow_none=True)
        self._check_default(args,  'dump_times',  tuple,      allow_none=True)
        self._check_default(args,  'dump_last',   bool,       allow_none=False)
        self._check_positive(args, 'dump_freq', strict=False, allow_none=False)
        self._check_dir(args, 'dump_dir', allow_shared=True, allow_none=True)

        self._check_default(args, ('tstart', 'tend'), float, allow_none=False)
        self._check_default(args, ('dump_tstart', 'dump_tend'), float, allow_none=True)
        args.dump_tstart = args.dump_tstart if (args.dump_tstart is not None) else args.tstart
        args.dump_tend = args.dump_tend if (args.dump_tend is not None) else args.tend
        if (args.tstart >= args.tend):
            msg = 'Invalid time range for the simulation: tstart={}, tend={}.'
            msg = msg.format(args.tstart, args.tend)
            self.error(msg)
        if (args.dump_tstart >= args.dump_tend):
            msg = 'Invalid time range for the dumping: tstart={}, tend={}.'
            msg = msg.format(args.dump_tstart, args.dump_tend)
            self.error(msg)
        if (args.dump_tstart < args.tstart):
            msg = 'Cannot dump before the simulation starts: simulation.tstart={}, dump.tstart={}.'
            msg = msg.format(args.tstart, args.dump_tstart)
            self.error(msg)
        if (args.dump_tend > args.tend):
            msg = 'Cannot dump after the simulation ends: simulation.tend={}, dump.tend={}.'
            msg = msg.format(args.tend, args.dump_tend)
            self.error(msg)

        times_of_interest = set()
        tstart = args.dump_tstart
        tend = args.dump_tend
        T = tend-tstart
        dt = args.dump_period

        if (args.dump_times is not None) or ((args.dump_period is not None) and (args.dump_period > 0.0)):
            args.dump_times = set() if (args.dump_times is None) else set(args.dump_times)
            if (args.dump_period is not None) and (args.dump_period > 0.0):
                ndumps = int(np.floor(T/dt)) + 1
                toi = tstart + np.arange(ndumps)*dt
                args.dump_times.update(toi)
            args.dump_times = filter(lambda t: (t >= tstart) & (t <= tend), args.dump_times)
            args.dump_times = tuple(sorted(args.dump_times))
            times_of_interest.update(args.dump_times)

        if args.force_ram_fs:
            args.enable_ram_fs = True

        if args.dump_is_temporary and (args.postprocess_dump is None):
            msg = 'Dump is temporary but no postprocessing script has been supplied.'
            self.error(msg)

        for pname in pnames:
            def _set_arg(args, argname, pname, prefix=''):
                bname = f'{prefix}{argname}'
                vname = f'{pname}_{bname}'
                default_value = getattr(args, bname)
                actual_value = getattr(args, vname)
                if (actual_value is None):
                    setattr(args, vname, default_value)
                value = getattr(args, vname)
                return value
            for argname in ('dir', 'freq', 'period', 'last', 'times', 'tstart', 'tend', 'is_temporary'):
                _set_arg(args, argname, pname, prefix='dump_')
            for argname in ('enable_ram_fs', 'force_ram_fs', 'hdf5_disable_compression', 'hdf5_disable_slicing', 'postprocess_dump'):
                _set_arg(args, argname, pname)
            if getattr(args, f'{pname}_force_ram_fs'):
                setattr(args, f'{pname}_enable_ram_fs', True)
            if getattr(args, f'{pname}_dump_is_temporary'):
                pd = getattr(args, f'{pname}_postprocess_dump')
                if (pd is None):
                    msg = f'{pname} dump is temporary but no postprocessing script has been supplied'
                    self.error(msg)

            bname = f'{pname}_dump'
            self._check_default(args,  f'{bname}_dir',    str,        allow_none=False)
            self._check_default(args,  f'{bname}_freq',   int,        allow_none=False)
            self._check_default(args,  f'{bname}_period', float,      allow_none=True)
            self._check_default(args,  f'{bname}_times',  tuple,      allow_none=True)
            self._check_default(args,  f'{bname}_last',    bool,      allow_none=True)
            self._check_default(args,  f'{bname}_tstart', float,      allow_none=False)
            self._check_default(args,  f'{bname}_tend',   float,      allow_none=False)
            self._check_positive(args, f'{bname}_freq', strict=False, allow_none=False)
            self._check_default(args,
                                tuple(map(lambda k: '{}_{}'.format(f'{pname}', k), ('dump_is_temporary',
                                                                                            'enable_ram_fs', 'force_ram_fs', 'hdf5_disable_compression', 'hdf5_disable_slicing'))),
                                bool, allow_none=False)
            self._check_dir(args,  f'{bname}_dir', allow_shared=True, allow_none=False)

            ststart = f'{bname}_tstart'
            stend = f'{bname}_tend'
            tstart = getattr(args, ststart)
            tend = getattr(args, stend)
            T = tend-tstart
            if (tstart >= tend):
                msg = 'Invalid time range for the dumping of IO parameter {}: {}={}, {}={}.'
                msg = msg.format(pname, ststart, tstart, stend, tend)
                self.error(msg)
            if (tstart < args.tstart):
                msg = 'Cannot dump before the simulation starts for IO parameter {}: simulation.tstart={}, {}={}.'
                msg = msg.format(pname, args.tstart, ststart, tstart)
                self.error(msg)
            if (tend > args.tend):
                msg = 'Cannot dump after the simulation ends for IO parameter {}: simulation.tend={}, {}={}.'
                msg = msg.format(pname, args.tend, stend, tend)
                self.error(msg)

            dump_times = getattr(args, f'{bname}_times')
            dump_period = getattr(args, f'{bname}_period')

            dump_times = set() if (dump_times is None) else set(dump_times)
            if (dump_period is not None) and (dump_period > 0.0):
                dt = dump_period
                ndumps = int(np.floor(T/dt)) + 1
                toi = tstart + np.arange(ndumps)*dt
                dump_times.update(toi)
            dump_times = tuple(filter(lambda t: (t >= tstart) & (t <= tend), dump_times))

            setattr(args, f'{bname}_times', tuple(sorted(dump_times)))
            times_of_interest.update(dump_times)

        msg = f'args.times_of_interest = {times_of_interest}\n'

        times_of_interest = tuple(sorted(times_of_interest))
        if (len(times_of_interest) > 0) and (times_of_interest[-1] == args.tend):
            args.tend += np.finfo(np.float32).eps

        for dp in times_of_interest[::-1]:
            if not isinstance(dp, float):
                msg = f'Dump time {dp} is not a float but a {type(dp)}.'
                self.error(msg)
            elif (dp < args.tstart):
                msg += 'Dump times of interest t={} happens before tstart={}.'
                msg = msg.format(dp, args.tstart)
                self.error(msg)
            elif (dp == args.tend):
                msg += 'Dump times of interest t={} happens exactly at tend={}.'
                msg = msg.format(dp, args.tend)
                self.error(msg)
            elif (dp >= args.tend):
                msg += 'Dump times of interest t={} happens after tend={}.'
                msg = msg.format(dp, args.tend)
                self.error(msg)

        args.times_of_interest = times_of_interest

        # extra checkpoints arguments
        self._check_default(args, 'load_checkpoint_path', str, allow_none=True)
        self._check_default(args, 'save_checkpoint_path', str, allow_none=True)
        self._check_default(args, 'checkpoint_relax_constraints', bool, allow_none=False)
        self._check_default(args, 'checkpoint_compression_method', str, allow_none=True)
        self._check_default(args, 'checkpoint_compression_level', int, allow_none=True)

    def _add_graphical_io_args(self):
        graphical_io = self.add_argument_group('Graphical I/O')
        if 'DISPLAY' in os.environ:
            default_visu_rank = 0
        else:
            default_visu_rank = None
        graphical_io.add_argument('--visu-rank', type=int, default=default_visu_rank,
                                  dest='visu_rank',
                                  help='Specify a MPI rank where the graphical outputs will occur.')
        graphical_io.add_argument('-dg', '--display-graph', action='store_true',
                                  dest='display_graph',
                                  help='Stop execution before first simulation iteration.')
        return graphical_io

    def _check_graphical_io_args(self, args):
        if (args.visu_rank is not None) and (args.visu_rank < 0):
            args.visu_rank = None
        self._check_default(args, 'visu_rank', int, allow_none=True)
        self._check_positive(args, 'visu_rank', strict=False, allow_none=True)
        self._check_default(args, 'display_graph', bool, allow_none=False)

    def _add_term_io_args(self):
        term_io = self.add_argument_group('Terminal I/O')
        msg0 = ('All {rank} and {size} occurences are replaced by the MPI rank and '
                + 'MPI communicator size. {default_dump_dir} is replaced by default output path.'
                + '{dump_dir} is replaced by user defined dump_dir.')
        # +'{program} and {hostname} are replaced by actual program and host name.')
        term_io.add_argument('-stdout', '--std-out',
                             type=str, default='{dump_dir}/{rank}.out',
                             dest='stdout',
                             help='Redirect stdout to this file. ' + msg0)
        term_io.add_argument('-stderr', '--std-err',
                             type=str, default='{dump_dir}/{rank}.out',
                             dest='stderr',
                             help='Redirect stderr to this file. '+msg0)
        term_io.add_argument('-tee', '--tee-ranks', type=str, default=(0,),
                             action=self.split, container=tuple, append=False, convert=int,
                             dest='tee_ranks',
                             help='Tee stdout and stderr of specified MPI ranks to terminal.')
        term_io.add_argument('-N', '--no-interactive', action='store_true',
                             dest='no_interactive',
                             help=('Disable user interactivity.'))
        term_io.add_argument('-V', '--verbose', action='store_true', default=None,
                             dest='verbose',
                             help='Enable verbosity. Overrides HYSOP_VERBOSE.')
        term_io.add_argument('-D', '--debug', action='store_true', default=None,
                             dest='debug',
                             help='Enable debugging informations. Overrides HYSOP_DEBUG.')
        term_io.add_argument('-K', '--kernel-debug', action='store_true', default=None,
                             dest='kernel_debug',
                             help='Enable kernel debugging informations. Overrides HYSOP_KERNEL_DEBUG.')
        term_io.add_argument('-P', '--profile', action='store_true', default=None,
                             dest='profile',
                             help='Enable profiling informations. Overrides HYSOP_PROFILE.')
        term_io.add_argument('-T', '--trace', type=str, default=None,
                             action=self.split, container=set, append=True,
                             dest='trace',
                             help=textwrap.dedent('R|'
                                                  + 'Enable trace informations.'
                                                  + '\nComma delimited list of modules to trace:'
                                                  + '\n  [calls]      all python calls'
                                                  + '\n  [warnings]   warnings traceback'
                                                  + '\n  [memallocs]  memory allocations'
                                                  + '\n  [bigallocs]  fat memory allocations'
                                                  + '\n  [kernels]    opencl and cuda kernel calls'
                                                  + '\n  [nocopy]     disable trace of opencl and cuda copy kernel calls'
                                                  + '\n  [noacc]      disable trace of opencl and cuda accumulate kernel calls'
                                                  + '\n  [all]        enable [warnings,memallocs,kernels]'
                                                  + '\nOverrides HYSOP_TRACE_{CALLS,WARNINGS,MEMALLOCS,KERNELS,NOCOPY,NOACCUMULATE}.'))
        return term_io

    def _check_term_io_args(self, args):
        self._check_default(args, ('stdout', 'stderr'), str, allow_none=False)
        self._check_default(args, ('tee_ranks'), tuple, allow_none=False)
        self._check_default(args, ('verbose', 'debug', 'profile', 'kernel_debug'),
                            bool, allow_none=True)
        self._check_default(args, 'trace', set, allow_none=True)

    def _add_misc_args(self):
        class _HysopVersion(argparse._VersionAction):
            def __call__(self, parser, namespace, values, option_string=None):
                HysopArgParser.set_env('VERBOSE', False)
                from hysop import version
                version = f'HySoP {version}'
                formatter = parser._get_formatter()
                formatter.add_text(version)
                parser.exit(message=formatter.format_help())

        class _HwInfo(argparse._StoreAction):
            def __call__(self, parser, namespace, values, option_strings=None):
                HysopArgParser.set_env('VERBOSE', False)
                from hysop.backend.hardware.hwinfo import Topology, PCIIds
                pciids = PCIIds(values)
                topo = Topology.parse(pciids)
                parser.exit(message=str(topo))

        class _HwSummary(argparse._StoreAction):
            def __call__(self, parser, namespace, values, option_strings=None):
                HysopArgParser.set_env('VERBOSE', False)
                from hysop.tools.hysop_ls import run
                args = []
                if (values is not None):
                    args = ['--pci-ids', values]
                run(args)
                parser.exit(message=None)

        misc = self.add_argument_group('Miscellaneous')
        misc.add_argument('-h', '--help', action='help', default=argparse.SUPPRESS,
                          help='Show this help message and exit')
        misc.add_argument('-v', '--version', action=_HysopVersion,
                          help='Show hysop version and exit.')
        misc.add_argument('-hwinfo', '--hardware-info', action=_HwInfo,
                          nargs='?', default=None, metavar='pci.ids',
                          help=('R|Show hardware information and exit.'
                                + '\nBench all OpenCL devices on first run.'))
        misc.add_argument('-hwstats', '--hardware-statistics', action=_HwSummary,
                          nargs='?', default=None, metavar='pci.ids',
                          help=('R|Show localhost hardware statistics and exit.'
                                + '\nBench all OpenCL devices on first run.'))
        return misc

    def _check_misc_args(self, args):
        pass

    def _check_default(self, args, argnames, types, allow_none=False):
        if not isinstance(argnames, tuple):
            argnames = (argnames,)
        assert all(isinstance(a, str) for a in argnames), argnames
        if not isinstance(types, tuple):
            types = (types,)
        assert all(isinstance(a, type) for a in types)
        if allow_none:
            types += (type(None),)
        for argname in argnames:
            if not hasattr(args, argname):
                msg = f'Unknown argument name \'{argname}\'.'
                raise ValueError(msg)
            argvalue = getattr(args, argname)
            if not isinstance(argvalue, types):
                msg = '\nBad initialization type for argument \'{}\''
                msg += '\n >Expected one of the following type:\n  *{}'
                msg += '\n >Got value {} of type {}.'
                msg = msg.format(argname, '\n  *'.join(x.__name__ for x in types),
                                 argvalue, type(argvalue).__name__)
                raise TypeError(msg)

    def _check_positive(self, args, argnames, strict=False, allow_none=False):
        if not isinstance(argnames, tuple):
            argnames = (argnames,)
        for argname in argnames:
            if not hasattr(args, argname):
                msg = f'Unknown argument name \'{argname}\'.'
                raise ValueError(msg)
            argvalue = getattr(args, argname)
            if (argvalue is None):
                if allow_none:
                    continue
                msg = '\nGot invalid None value for parameter \'{}\', positive value expected.'
                msg = msg.format(argname)
                self.error(msg)
            if (argvalue < 0) or (strict and (argvalue == 0)):
                if strict:
                    msg = 'Strictly positive'
                else:
                    msg = 'Positive'
                msg += ' value expected for parameter {} but got value {}.'
                msg = msg.format(argname, argvalue)
                self.error(msg)

    def _check_range(self, args, argnames, fmin, fmax, allow_none=False):
        if not isinstance(argnames, tuple):
            argnames = (argnames,)
        for argname in argnames:
            if not hasattr(args, argname):
                msg = f'Unknown argument name \'{argname}\'.'
                raise ValueError(msg)
            argvalue = getattr(args, argname)
            if (argvalue is None):
                if allow_none:
                    continue
                msg = 'Got invalid None value for parameter \'{}\'.'
                msg = msg.format(argname)
                self.error(msg)
            if (fmin is not None) and (fmax is not None) and not (fmin <= argvalue <= fmax):
                msg = 'Value {} is not in range [{}, {}] for parameter {}.'
                msg = msg.format(argvalue, fmin, fmax, argname)
                self.error(msg)
            elif (fmin is not None) and (argvalue < fmin):
                msg = 'Value {} is bellow minimum value {} for parameter {}. '
                msg = msg.format(argvalue, fmin, argname)
                self.error(msg)
            elif (fmin is not None) and (argvalue > fmax):
                msg = 'Value {} is beyond maximal value {} for parameter {}. '
                msg = msg.format(argvalue, fmax, argname)
                self.error(msg)

    def _check_dir(self, args, argnames, allow_shared=False, allow_none=False,
                   enforce_shared=False, assert_root_folder_exists=True):
        if not isinstance(argnames, tuple):
            argnames = (argnames,)
        for argname in argnames:
            if not hasattr(args, argname):
                msg = f'Unknown argument name \'{argname}\'.'
                raise ValueError(msg)
            argvalue = getattr(args, argname)
            if (argvalue is None):
                if allow_none:
                    continue
                msg = 'Got invalid None value for parameter \'{}\'.'
                msg = msg.format(argname)
                self.error(msg)

            argvalue = self._fmt_filename(argvalue,
                                          self.rank, self.size,
                                          args.dump_dir,
                                          self.default_dump_dir)

            path = argvalue
            i = 0
            while not os.path.isdir(path):
                path = os.path.realpath(path+'/..')
                if path == '/':
                    break
                i += 1
                if (i == 256):
                    msg = 'Achieved more than 256 subfolders, {} is not a valid path !'
                    msg = msg.format(path)
                    self.error(msg)

            if (not allow_shared) and self.is_shared_fs(path):
                msg = '{} directory \'{}\' cannot be stored on a shared network file system.'
                msg = msg.format(argname, argvalue)
                self.error(msg)

            if enforce_shared and not self.is_shared_fs(path):
                msg = '{} directory \'{}\' has to be stored on a shared network file system.'
                msg = msg.format(argname, argvalue)
                self.error(msg)

            self._mkdir(argvalue, dirname=False)
            setattr(args, argname, os.path.realpath(argvalue))

    def _setup_hysop_env(self, args):
        self.set_env('VERBOSE',   args.verbose)
        self.set_env('DEBUG',     args.debug)
        self.set_env('KERNEL_DEBUG', args.kernel_debug)
        self.set_env('PROFILE',   args.profile)
        self.set_env('DUMP_DIR',  args.dump_dir)
        self.set_env('CACHE_DIR', args.cache_dir)

        if (args.trace is not None):
            for module in args.trace:
                module = module.lower()
                if module == 'calls':
                    self.set_env('TRACE_CALLS', '1')
                elif module == 'nocopy':
                    self.set_env('TRACE_NOCOPY', '1')
                elif module == 'noacc':
                    self.set_env('TRACE_NOACCUMULATE', '1')
                elif module == 'bigallocs':
                    self.set_env('BACKTRACE_BIG_MEMALLOCS', '1')
                elif module in ('all', 'warnings'):
                    self.set_env('TRACE_WARNINGS', '1')
                elif module in ('all', 'memallocs'):
                    self.set_env('TRACE_MEMALLOCS', '1')
                elif module in ('all', 'kernels'):
                    self.set_env('TRACE_KERNELS', '1')
                else:
                    msg = f'Unknown tracing module \'{module}\'.'
                    self.error(msg)

        self.set_env('ENABLE_THREADING',       args.enable_threading,          True)
        self.set_env('MAX_THREADS',            args.max_threads,               True)
        self.set_env('FFTW_NUM_THREADS',       args.fftw_threads,              True)
        self.set_env('FFTW_PLANNER_EFFORT',    args.fftw_planner_effort,       True)
        self.set_env('FFTW_PLANNER_TIMELIMIT', args.fftw_planner_timelimit,    True)
        self.set_env('ENABLE_FILELOCKS', not args.disable_file_locks, True)

        # those environment variables are not part of HySoP
        self.set_env('OMP_NUM_THREADS',        args.openmp_threads,            False)
        self.set_env('MKL_NUM_THREADS',        args.mkl_threads,               False)
        self.set_env('MKL_DOMAIN_NUM_THREADS', args.mkl_domain_threads,        False)
        self.set_env('MKL_THREADING_LAYER',    args.mkl_threading_layer,       False)
        self.set_env('NUMBA_NUM_THREADS',      args.numba_threads,             False)
        self.set_env('NUMBA_THREADING_LAYER',  args.numba_threading_layer,     False)

    def _setup_parameters(self, args):
        from hysop import IO, IOParams
        from hysop.core.checkpoints import CheckpointHandler
        from hysop.tools.debug_dumper import DebugDumper

        args.io_params = IOParams(filename=None, filepath=args.dump_dir,
                                  frequency=args.dump_freq, dump_times=args.dump_times,
                                  dump_tstart=args.dump_tstart, dump_tend=args.dump_tend, dump_last=args.dump_last,
                                  enable_ram_fs=args.enable_ram_fs, force_ram_fs=args.force_ram_fs,
                                  dump_is_temporary=args.dump_is_temporary,
                                  postprocess_dump=args.postprocess_dump,
                                  hdf5_disable_compression=args.hdf5_disable_compression,
                                  hdf5_disable_slicing=args.hdf5_disable_slicing)

        for pname in self.generate_io_params:
            iop = IOParams(filename=None,
                           filepath=getattr(args, f'{pname}_dump_dir'),
                           frequency=getattr(args, f'{pname}_dump_freq'),
                           dump_times=getattr(args, f'{pname}_dump_times'),
                           with_last=getattr(args, f'{pname}_dump_last') or args.dump_last,
                           dump_tstart=getattr(args, f'{pname}_dump_tstart'),
                           dump_tend=getattr(args, f'{pname}_dump_tend'),
                           enable_ram_fs=getattr(args, f'{pname}_enable_ram_fs'),
                           force_ram_fs=getattr(args, f'{pname}_force_ram_fs'),
                           dump_is_temporary=getattr(args, f'{pname}_dump_is_temporary'),
                           postprocess_dump=getattr(args, f'{pname}_postprocess_dump'),
                           hdf5_disable_compression=getattr(args, f'{pname}_hdf5_disable_compression'),
                           hdf5_disable_slicing=getattr(args, f'{pname}_hdf5_disable_slicing'))
            setattr(args, f'{pname}_io_params', iop)

        load_checkpoint_path = args.load_checkpoint_path
        if (load_checkpoint_path is not None):
            if not load_checkpoint_path.endswith('.tar'):
                msg = 'Load checkpoint filename has to end with .tar, got \'{}\'.'
                self.error(msg.format(load_checkpoint_path))
            if (os.path.sep not in load_checkpoint_path):
                load_checkpoint_path = os.path.join(args.checkpoint_dump_dir, load_checkpoint_path)
            if not os.path.isfile(load_checkpoint_path):
                msg = 'Cannot load checkpoint \'{}\' because the file does not exist.'
                self.error(msg.format(load_checkpoint_path))
            load_checkpoint_path = os.path.abspath(load_checkpoint_path)
            args.load_checkpoint_path = load_checkpoint_path

        save_checkpoint_path = args.save_checkpoint_path
        if (save_checkpoint_path is not None):
            if not save_checkpoint_path.endswith('.tar'):
                msg = 'Save checkpoint filename has to end with .tar, got \'{}\'.'
                self.error(msg.format(save_checkpoint_path))
            if (os.path.sep not in save_checkpoint_path):
                save_checkpoint_path = os.path.join(args.checkpoint_dump_dir, save_checkpoint_path)
            save_checkpoint_path = os.path.abspath(save_checkpoint_path)
            args.checkpoint_dump_dir = os.path.dirname(save_checkpoint_path)
            args.save_checkpoint_path = save_checkpoint_path

        args.checkpoint_handler = CheckpointHandler(args.load_checkpoint_path, args.save_checkpoint_path,
                                                    args.checkpoint_compression_method, args.checkpoint_compression_level,
                                                    args.checkpoint_io_params, args.checkpoint_relax_constraints)

        # debug dumps
        if (args.debug_dump_dir is None):
            args.debug_dump_dir = args.dump_dir
        if args.debug_dump_target:
            debug_dumper = DebugDumper(
                path=args.debug_dump_dir,
                name=args.debug_dump_target,
                force_overwrite=True, enable_on_op_apply=True)
        else:
            debug_dumper = None
        args.debug_dumper = debug_dumper

    def _setup_implementation(self, args):
        from hysop.constants import Implementation
        impl = args.impl

        autotuner_config = None
        kernel_config = None

        if impl in (Implementation.PYTHON, Implementation.FORTRAN):
            pass
        elif impl is Implementation.OPENCL:
            # check and configure OpenCL kernel generation and tuning
            from hysop.backend import __HAS_OPENCL_BACKEND__
            if not __HAS_OPENCL_BACKEND__:
                msg = 'No OpenCL backend found (pyopencl package not found).'
                self.error(msg)
            autotuner_config = self._build_autotuner_config(args)
            kernel_config = self._build_opencl_kernel_config(args, autotuner_config)
        else:
            msg = f'Unknown implementation \'{impl}\'.'
            raise ValueError(msg)
        args.impl = impl
        args.autotuner_config = autotuner_config
        args.opencl_kernel_config = kernel_config

    def _check_convert(self, argname, argvalue, values):
        if (argvalue is None):
            return None
        if isinstance(argvalue, str):
            argvalue = argvalue.strip().lower()
        if (argvalue in values):
            return values[argvalue]
        msg = f'Failed to convert argument {argname}: {argvalue}'
        msg += '\nPossible values are:\n  *'
        msg += '\n  *'.join(f'{k}: {v}' for (k, v) in values.items())
        self.error(msg)

    def _check_and_set_diffusion_mode(self, argname, args):
        dm = getattr(args, argname, None)
        modes = ('spectral', 'directional')
        if (dm is None):
            msg = 'Diffusion mode for parameter \'{}\' has not been set.'
            msg = msg.format(dm, argname)
            self.error(msg)
        if (dm not in modes):
            msg = 'Unknown diffusion mode \'{}\' for parameter \'{}\'. Available modes are {}.'
            msg = msg.format(dm, argname, modes)
            self.error(msg)
        target = argname.split('_')[0]
        for mode in modes:
            varname = f'use_{target}_{mode}_diffusion'
            setattr(args, varname, dm == mode)

    def _convert_bool(self, argname, val):
        values = {
            None:   None,
            'none':  None,
            'false': False,
            'true':  True,
            '0':     False,
            '1':     True,
        }
        return self._check_convert(argname, val, values)

    def _convert_threads(self, argname, val, default):
        if (val == 'physical'):
            val = psutil.cpu_count(logical=False)
        elif (val == 'logical'):
            val = psutil.cpu_count(logical=True)
        elif (val is None):
            if (default is None):
                msg = "'Parameter '{}' has been set to None and no default value has been set."
                msg = msg.format(argname)
                self.error(msg)
            else:
                val = default
        val = int(val)
        if not (val > 0):
            msg = "'Parameter '{}' has been set to an invalid number of threads {}."
            msg = msg.format(argname, val)
            self.error(msg)
        return val

    def _convert_fftw_planner_effort(self, argname, val):
        values = {
            'estimate':   'FFTW_ESTIMATE',
            'measure':    'FFTW_MEASURE',
            'patient':    'FFTW_PATIENT',
            'exhaustive': 'FFTW_EXHAUSTIVE',
        }
        return self._check_convert(argname, val, values)

    def _convert_mkl_threading_layer(self, argname, val):
        values = {
            'seq': 'SEQUENTIAL',
            'omp': 'OMP',
            'tbb': 'TBB',
            'intel': 'INTEL'
        }
        return self._check_convert(argname, val, values)

    def _convert_numba_threading_layer(self, argname, val):
        values = {
            'workqueue': 'workqueue',
            'omp':       'omp',
            'tbb':       'tbb'
        }
        return self._check_convert(argname, val, values)

    def _convert_implementation(self, argname, impl):
        from hysop.constants import Implementation
        implementations = {
            'python':  Implementation.PYTHON,
            'fortran': Implementation.FORTRAN,
            'cpp':     Implementation.CPP,
            'opencl':  Implementation.OPENCL,
            'cl':      Implementation.OPENCL
        }
        return self._check_convert(argname, impl, implementations)

    def _convert_autotuner_flag(self, argname, flag):
        from hysop.constants import AutotunerFlags
        flags = {
            'estimate':   AutotunerFlags.ESTIMATE,
            'measure':    AutotunerFlags.MEASURE,
            'patient':    AutotunerFlags.PATIENT,
            'exhaustive': AutotunerFlags.EXHAUSTIVE
        }
        return self._check_convert(argname, flag, flags)

    def _convert_precision(self, argname, precision):
        from hysop.constants import Precision
        if (precision is None):
            return None
        precisions = {
            'default':      Precision.DEFAULT,
            'same':         Precision.SAME,
            'fp16':         Precision.HALF,
            'fp32':         Precision.FLOAT,
            'fp64':         Precision.DOUBLE,
            'fp80':         Precision.LONG_DOUBLE,
            'fp128':        Precision.QUAD,
            'half':         Precision.HALF,
            'float':        Precision.FLOAT,
            'double':       Precision.DOUBLE,
            'long double':  Precision.LONG_DOUBLE,
            '__float128':   Precision.QUAD,
        }
        return self._check_convert(argname, precision, precisions)

    def _precision_to_dtype(self, argname, precision):
        import numpy as np
        from hysop.constants import Precision, HYSOP_REAL
        if (precision is None):
            return None
        precisions = {
            Precision.DEFAULT:     HYSOP_REAL,
            Precision.HALF:        np.float16,
            Precision.FLOAT:       np.float32,
            Precision.DOUBLE:      np.float64,
            Precision.LONG_DOUBLE: np.float128
        }
        return self._check_convert(argname, precision, precisions)

    def _convert_device_type(self, argname, device_type):
        from hysop.constants import DeviceType
        device_types = {
            'all':     DeviceType.ALL,
            'acc':     DeviceType.ACCELERATOR,
            'cpu':     DeviceType.CPU,
            'custom':  DeviceType.CUSTOM,
            'gpu':     DeviceType.GPU,
            'default': DeviceType.DEFAULT
        }
        return self._check_convert(argname, device_type, device_types)

    def _convert_fp_dump_mode(self, argname, fp_dump_mode):
        fp_dump_modes = {
            'decimal':     'dec',
            'hexadecimal': 'hex',
            'dec':         'dec',
            'hex':         'hex'
        }
        return self._check_convert(argname, fp_dump_mode, fp_dump_modes)

    def _convert_strang_order(self, argname, strang_order):
        from hysop.methods import StrangOrder
        strang_orders = {
            1: StrangOrder.STRANG_FIRST_ORDER,
            2: StrangOrder.STRANG_SECOND_ORDER,
        }
        return self._check_convert(argname, strang_order, strang_orders)

    def _convert_time_integrator(self, argname, time_integrator):
        from hysop.numerics.odesolvers.runge_kutta import Euler, RK2, RK3, RK4, RK4_38
        time_integrators = {
            'euler': Euler,
            'rk1':   Euler,
            'rk2':   RK2,
            'rk3':   RK3,
            'rk4':   RK4,
            'rk4_38': RK4_38
        }
        return self._check_convert(argname, time_integrator, time_integrators)

    def _convert_remesh_kernel(self, argname, remesh_kernel):
        from hysop.methods import Remesh
        remesh_kernels = {
            'l2_1':  Remesh.L2_1,
            'l2_2':  Remesh.L2_2,
            'l4_2':  Remesh.L4_2,
            'l4_4':  Remesh.L4_4,
            'l6_4':  Remesh.L6_4,
            'l6_6':  Remesh.L6_6,
            'l8_4':  Remesh.L8_4,
            'l2_1s': Remesh.L2_1s,
            'l2_2s': Remesh.L2_2s,
            'l4_2s': Remesh.L4_2s,
            'l4_4s': Remesh.L4_4s,
            'l6_4s': Remesh.L6_4s,
            'l6_6s': Remesh.L6_6s,
            'l8_4s': Remesh.L8_4s,
        }
        return self._check_convert(argname, remesh_kernel, remesh_kernels)

    def _convert_polynomial_interpolation(self, argname, pi):
        from hysop.numerics.interpolation.polynomial import PolynomialInterpolation
        polynomial_interpolations = {
            'linear':         PolynomialInterpolation.LINEAR,
            'cubic':          PolynomialInterpolation.CUBIC,
            'quintic':        PolynomialInterpolation.QUINTIC,
            'septic':         PolynomialInterpolation.SEPTIC,
            'nonic':          PolynomialInterpolation.NONIC,
            'cubic_fdc2':     PolynomialInterpolation.CUBIC_FDC2,
            'cubic_fdc4':     PolynomialInterpolation.CUBIC_FDC4,
            'cubic_fdc6':     PolynomialInterpolation.CUBIC_FDC6,
            'quintic_fdc2':   PolynomialInterpolation.QUINTIC_FDC2,
            'quintic_fdc4':   PolynomialInterpolation.QUINTIC_FDC4,
            'quintic_fdc6':   PolynomialInterpolation.QUINTIC_FDC6,
            'septic_fdc2':    PolynomialInterpolation.SEPTIC_FDC2,
            'septic_fdc4':    PolynomialInterpolation.SEPTIC_FDC4,
            'septic_fdc6':    PolynomialInterpolation.SEPTIC_FDC6,
            'nonic_fdc2':     PolynomialInterpolation.NONIC_FDC2,
            'nonic_fdc4':     PolynomialInterpolation.NONIC_FDC4,
            'nonic_fdc6':     PolynomialInterpolation.NONIC_FDC6,
        }
        return self._check_convert(argname, pi, polynomial_interpolations)

    def _convert_advection_interpolation(self, argname, ai):
        from hysop.numerics.interpolation.interpolation import Interpolation
        from hysop.numerics.interpolation.polynomial import PolynomialInterpolation
        advection_interpolations = {
            'legacy':         Interpolation.LINEAR,
            'linear':         PolynomialInterpolation.LINEAR,
            'cubic_fdc2':     PolynomialInterpolation.CUBIC_FDC2,
            'cubic_fdc4':     PolynomialInterpolation.CUBIC_FDC4,
            'cubic_fdc6':     PolynomialInterpolation.CUBIC_FDC6,
            'quintic_fdc2':   PolynomialInterpolation.QUINTIC_FDC2,
            'quintic_fdc4':   PolynomialInterpolation.QUINTIC_FDC4,
            'quintic_fdc6':   PolynomialInterpolation.QUINTIC_FDC6,
            'septic_fdc2':    PolynomialInterpolation.SEPTIC_FDC2,
            'septic_fdc4':    PolynomialInterpolation.SEPTIC_FDC4,
            'septic_fdc6':    PolynomialInterpolation.SEPTIC_FDC6,
            'nonic_fdc2':     PolynomialInterpolation.NONIC_FDC2,
            'nonic_fdc4':     PolynomialInterpolation.NONIC_FDC4,
            'nonic_fdc6':     PolynomialInterpolation.NONIC_FDC6,
        }
        return self._check_convert(argname, ai, advection_interpolations)

    def _convert_stretching_formulation(self, argname, stretching_formulation):
        from hysop.constants import StretchingFormulation
        stretching_formulations = {
            'grad_uw':       StretchingFormulation.GRAD_UW,
            'grad_uw_t':     StretchingFormulation.GRAD_UW_T,
            'mixed_grad_uw': StretchingFormulation.MIXED_GRAD_UW,
            'conservative':  StretchingFormulation.CONSERVATIVE
        }
        return self._check_convert(argname, stretching_formulation, stretching_formulations)

    def _convert_filtering_method(self, argname, fm, allow_subgrid=True):
        from hysop.methods import FilteringMethod
        filtering_methods = {
            'spectral':   FilteringMethod.SPECTRAL,
            'remesh':     FilteringMethod.REMESH,
            'polynomial': FilteringMethod.POLYNOMIAL,
            'subgrid':    FilteringMethod.SUBGRID,
        }
        if allow_subgrid:
            filtering_methods['subgrid'] = FilteringMethod.SUBGRID
        return self._check_convert(argname, fm, filtering_methods)


class HysopHelpFormatter(ColorHelpFormatter):
    def _format_args(self, *args, **kwds):
        args = super()._format_args(*args, **kwds)
        return colors.color(args, fg='green', style='bold')

    def _format_action(self, *args, **kwds):
        args = super()._format_action(*args, **kwds)
        args = args.split(' ')
        args = (colors.color(arg, fg='red', style='bold') if arg and (arg[0] == '-')
                else arg for arg in args)
        return ' '.join(args)

    def _fill_text(self, text, width, indent):
        return ''.join([indent + line for line in text.splitlines(True)])

    def _format_usage(self, usage, actions, groups, prefix):
        def predicate(action):
            blacklist = ('-tee', '-stdout', '-stderr', '-V', '-D', '-K', '-P', '-T', '-h',
                         '--version', '--hardware-info', '--hardware-statistics')
            p = not action.option_strings[0].startswith('--opencl')
            p &= not action.option_strings[0].startswith('--autotuner')
            p &= not action.option_strings[0].startswith('--fftw')
            p &= 'thread' not in action.option_strings[0]
            p &= 'stop' not in action.option_strings[0]
            p &= (action.option_strings[0] not in blacklist)
            p &= (len(action.option_strings) < 2) or (action.option_strings[1] not in blacklist)
            return p
        actions = filter(predicate, actions)
        usage = self._format_usage_color_help_formatter(usage=usage, actions=actions, groups=groups, prefix=prefix)
        usage = usage.rstrip()
        opencl_parameters = colors.color('OPENCL_PARAMETERS', fg='green', style='bold')
        autotuner_parameters = colors.color('AUTOTUNER_PARAMETERS', fg='green', style='bold')
        trace = colors.color('TRACE',  fg='green', style='bold')
        tee = colors.color('RANKS',  fg='green', style='bold')
        stdout = colors.color('STDOUT', fg='green', style='bold')
        stderr = colors.color('STDERR', fg='green', style='bold')

        s = ' '*(8+len(self._prog))

        usage += '\n{s}[--opencl-{{...}} {}]\n{s}[--autotuner-{{...}} {}]'.format(
            opencl_parameters, autotuner_parameters, s=s)
        usage += f'\n{s}[-tee {tee}] [-stdout {stdout}] [-stderr {stderr}]'
        usage += f'\n{s}[-V] [-D] [-K] [-P] [-T {trace}]'
        usage += f'\n{s}[--help] [--version] [--hardware-info] [--hardware-summary]'
        usage += '\n\n'
        return usage

    def _format_usage_color_help_formatter(self, usage, actions, groups, prefix):
        from gettext import gettext as _
        from colors import strip_color
        import re as _re
        if prefix is None:
            prefix = _('usage: ')

        # if usage is specified, use that
        if usage is not None:
            usage = usage % dict(prog=self._prog)

        # if no optionals or positionals are available, usage is just prog
        elif usage is None and not actions:
            usage = '%(prog)s' % dict(prog=self._prog)

        # if optionals and positionals are available, calculate usage
        elif usage is None:
            prog = '%(prog)s' % dict(prog=self._prog)

            # split optionals from positionals
            optionals = []
            positionals = []
            for action in actions:
                if action.option_strings:
                    optionals.append(action)
                else:
                    positionals.append(action)

            # build full usage string
            format = self._format_actions_usage
            action_usage = format(optionals + positionals, groups)
            usage = ' '.join([s for s in [prog, action_usage] if s])

            # wrap the usage parts if it's too long
            text_width = self._width - self._current_indent
            if len(prefix) + len(strip_color(usage)) > text_width:

                # break usage into wrappable parts
                part_regexp = r'\(.*?\)+|\[.*?\]+|\S+'
                opt_usage = format(optionals, groups)
                pos_usage = format(positionals, groups)
                opt_parts = _re.findall(part_regexp, opt_usage)
                pos_parts = _re.findall(part_regexp, pos_usage)

                # helper for wrapping lines
                def get_lines(parts, indent, prefix=None):
                    lines = []
                    line = []
                    if prefix is not None:
                        line_len = len(prefix) - 1
                    else:
                        line_len = len(indent) - 1
                    for part in parts:
                        if line_len + 1 + len(strip_color(part)) > text_width and line:
                            lines.append(indent + ' '.join(line))
                            line = []
                            line_len = len(indent) - 1
                        line.append(part)
                        line_len += len(strip_color(part)) + 1
                    if line:
                        lines.append(indent + ' '.join(line))
                    if prefix is not None:
                        lines[0] = lines[0][len(indent):]
                    return lines

                # if prog is short, follow it with optionals or positionals
                len_prog = len(strip_color(prog))
                if len(prefix) + len_prog <= 0.75 * text_width:
                    indent = ' ' * (len(prefix) + len_prog + 1)
                    if opt_parts:
                        lines = get_lines([prog] + opt_parts, indent, prefix)
                        lines.extend(get_lines(pos_parts, indent))
                    elif pos_parts:
                        lines = get_lines([prog] + pos_parts, indent, prefix)
                    else:
                        lines = [prog]

                # if prog is long, put it on its own line
                else:
                    indent = ' ' * len(prefix)
                    parts = opt_parts + pos_parts
                    lines = get_lines(parts, indent)
                    if len(lines) > 1:
                        lines = []
                        lines.extend(get_lines(opt_parts, indent))
                        lines.extend(get_lines(pos_parts, indent))
                    lines = [prog] + lines

                # join lines into usage
                usage = '\n'.join(lines)

        # prefix with 'usage:'
        return f'{prefix}{usage}\n\n'

    def start_section(self, heading):
        heading = colors.color(f'[{heading.upper()}]', fg='yellow', style='bold')
        return super().start_section(heading)

    def _get_help_string(self, action):
        help = action.help
        if '%(default)' not in action.help:
            if action.default not in (argparse.SUPPRESS, None):
                defaulting_nargs = [argparse.OPTIONAL, argparse.ZERO_OR_MORE]
                if action.option_strings or action.nargs in defaulting_nargs:
                    help += ' [default: %(default)s]'
        return help

    def _split_lines(self, text, width):
        if text.startswith('R|'):
            return text[2:].splitlines()
        return super()._split_lines(text, width)


class HysopNamespace(argparse.Namespace):
    pass


if __name__ == '__main__':
    parser = HysopArgParser(prog_name='example_demo', description='This is an HySoP example demo.')
    parser.print_help()
