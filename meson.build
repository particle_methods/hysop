#
# ./run_docker_image.sh  (original ubuntu_jammy_meson docker image)
#
# export HYSOP=hysop
# export TMP_DIR='/tmp'
# export BUILD_DIR=builddir
#
# cd ${TMP_DIR}; \rm -r ${HYSOP}
# cd ${TMP_DIR}; cp -r ../hysop ${HYSOP}; cd ${TMP_DIR}/${HYSOP}
#
# pip3 install .
#
# - OR -
#
# --prefix is used to set the directory where hysop will be installed!
# meson setup         ${BUILD_DIR} --prefix ${PWD}/build-install
# meson configure     ${BUILD_DIR} -Dpython.install_env=prefix
# meson compile -v -C ${BUILD_DIR}
# meson install    -C ${BUILD_DIR}
#
# To test if everything is ok before to submit meson tests.
# export PYTHONPATH=${PWD}/build-install/lib/python3/dist-packages:${PYTHONPATH}
# or !!
# export PYTHONPATH=${PWD}/build-install/lib/python3.11/site-packages:${PYTHONPATH}
# python3 -c "import hysop; print(hysop); print(dir(hysop))"
# python3 -c "from hysop import f2hysop; print(f2hysop);print(dir(f2hysop))"
#
# meson test    -C ${BUILD_DIR}
#
# https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop
# Nicolas.Grima@univ-pau.fr
# january/february 2024
#

#
# - Hysop version is in hysop_version.txt texte file.
#/site-packages: - Meson set the version variable used dynamically by pyproject.toml.
# - (Warning: only one line in hysop_version.txt)
#
# - Main Project parameters - #
project('hysop',
        'c',
        # hysop version is set in hysop_version.txt file
        version : files('hysop_version.txt'),
        license: 'Apache-2.0',
        meson_version: '>=1.3.0',
        # Strongly inspired by the scipy meson.build configuration
        default_options: [
          'buildtype=debugoptimized',  # equivalent to CMAKE_BUILD_TYPE
          'optimization=2',
          'b_ndebug=if-release',
          # 'fortran_std=legacy',
          'python.install_env=auto'
        ],
       )

# - Read Hysop OPTIONS in meson_options.txt
verbose_mode                  = get_option('VERBOSE_MODE')
with_scales                   = get_option('WITH_SCALES')
with_fftw                     = get_option('WITH_FFTW')
with_opencl                   = get_option('WITH_OPENCL')
with_tests                    = get_option('WITH_TESTS')
doubleprec                    = get_option('DOUBLEPREC')
use_mpi                       = get_option('USE_MPI')
build_shared_libs             = get_option('BUILD_SHARED_LIBS')
with_parallel_compressed_hdf5 = get_option('WITH_PARALLEL_COMPRESSED_HDF5')
with_extras                   = get_option('WITH_EXTRAS')
profile                       = get_option('PROFILE')
verbose                       = get_option('VERBOSE')
debug                         = get_option('DEBUG')
full_test                     = get_option('FULL_TEST')
optim                         = get_option('OPTIM')
with_mpi_test                 = get_option('WITH_MPI_TESTS')
fortran_layout                = get_option('FORTRAN_LAYOUT')
enable_long_tests             = get_option('ENABLE_LONG_TESTS')
dev_mode                      = get_option('DEV_MODE')

if use_mpi != 'ON'
  message('Non-mpi version of hysop has been deprecated!')
  message('USE_MPI is forced to ON')
  use_mpi='ON'
endif

# Fortran interface is set only if scales and/or fftw are up.
use_fortran = 'OFF'
if with_fftw == 'ON' or with_scales == 'ON' or with_extras == 'ON'
  use_fortran = 'ON'
  message('use_fortran is forced to ON')
  # - Add FORTRAN language
  add_languages('fortran', native: false, required: true)
  ff = meson.get_compiler('fortran')
endif

# - To get absolute project directory path
project_dir = meson.current_source_dir()
message('Project directory:', project_dir)

# - To get absolute meson build directory
meson_build_dir = meson.current_build_dir()
message('Meson build directory:', meson_build_dir)

# - Get hysop project version
hysop_version = meson.project_version()
version       = meson.project_version() # for pyproject.toml
message('hysop_version:', hysop_version)

# - FS (FilesSystem) is a Meson tool to manipulate files
fs = import('fs')

# - Meson test options/parameters
test_is_parallel = true
test_env         = environment()
test_env.set('MALLOC_PERTURB_','0')

# - Import python and get dependencies and hysop/f2hysop install path
py_mod  = import('python', required: true)
py      = py_mod.find_installation(pure: false)
py_dep  = py.dependency()

pytestexe =find_program('pytest')

# - OpenCL check platform and device IDs
if with_opencl == 'ON'
#  dep_opencl = dependency('OpenCL') # search openCL lib
  src_py_opencl = files('hysop/tools/opencl_explore.py')
  ocl_explore_out = run_command(py, src_py_opencl, check: true).stdout().strip().split()
  platform_id = ocl_explore_out[0]
  device_id   = ocl_explore_out[1]
  message('OpenCL -> Platform_ID:', platform_id)
  message('OpenCL -> Device_ID  :',   device_id)
else
  platform_id = '-1'
  device_id   = '-1'
endif

# - To set where hysop python files will installed
# - use meson "--prefix" option to force an install directory.
# - Otherwise, by default, hysop python files will be
# - install in to the python virtual environment.
# - (It's strongly recommended to use a virtual env!)
if verbose == 'ON'
  message('py.get_path(purelib):', py.get_path('purelib'))
  message('hysop_pythonpath = py.get_install_dir()', py.get_install_dir())
  # meson.build_options() give a string
  # with meson setup options, it is particular usefull
  # to know if --prefix option was used or not !
  message('meson.build_options():', meson.build_options())
endif

# - Hysop python path is defined using "prefix" value sets during 'meson setup'
# - or if you submit 'pip3 install .' using a virtual env the path
# - will be something like: path/venv/lib/python3.xx/site-packages
hysop_pythonpath = py.get_install_dir()

message('******************')
message('Hysop python path:', hysop_pythonpath)
message('******************')

# - used by tests !
test_env.set('PYTHONPATH', py.get_install_dir())

# - Hysop fortran source codes for hysop_fortran.a static library
src_f90=[]
# - Hysop fortran source codes for f2hysop dynamic library (f2py)
# - These fortran source codes are interface bettween hyspo_fortran.a lib
# - and python routines.
src_f90_4_f2py=[]
# - Hysop python source codes
src_python=[]
# - check meson.build in hysop and hysop directories
subdir('hysop')
subdir('hysop_examples')
# - check meson.build in src directory
subdir('src')

# ============= Summary =============
#if verbose == 'ON'
  message('====================== End of configuration process ======================')
  message('Summary: ')
  message(' Python executable   :', py.full_path())
  message(' Python version      :', py.version())
  message(' Python dist-packages:', py.get_path('purelib'))

  message(' Hysop install dir   :', hysop_pythonpath)

  if use_fortran == 'ON'
    message(' Compiler family     :', ff.get_id())
  else
    message('''
            You deactivate fortran to python interface generation.
            This will disable the fortran interface,
            including fftw and scales fonctionnalities.
            ''')
  endif
  message(' Sources are in      :', project_dir)
  message(' Build is done in    :', meson_build_dir)
  message(' Project uses MPI    :', use_mpi)
  message(' Project uses Scales :', with_scales)
  message(' Project uses FFTW   :', with_fftw)
  if with_opencl == 'ON'
    message(' Project uses OpenCL :', with_opencl)
  else
    message('''
            You deactivate OpenCL. This will disable
            the OpenCL backend for both GPU and
            CPU architecture.
            ''')
  endif
  message(' //  HDF5 interface  :',
          h5py_parallel_compression_enabled)
  message(' Profile mode        :', profile)
  message(' Debug   mode        :', debug)
  message(' Enable -OO run?     :', optim)
  if doubleprec == 'ON'
      message(' Default real numbers precision : DOUBLE.')
  else
      message(' Default real numbers precision : SINGLE.')
  endif
  message('')
  message('====================== MESON ======================')
  message('Try :')
  message(' export BUILD_DIR='+meson_build_dir)
  message(' \'meson compile -C ${BUILD_DIR}\' to build the project.')
  message(' \'meson install -C ${BUILD_DIR}\' to install python modules and their dependencies. ')
  message(' \'meson test    -C ${BUILD_DIR}\' to run some tests.')
  message(' \'meson dist    -C ${BUILD_DIR}\' generate release archive.')
  message('''
          /!\ WARNING /!\:
          depending on your python environment configuration,
          you may need to set PYTHONPATH.
          Try to run python -c 'import hysop'
          If it fails, export PYTHONPATH environment variable as:''')
  message(' export PYTHONPATH='+hysop_pythonpath+':${PYTHONPATH}')
#endif
