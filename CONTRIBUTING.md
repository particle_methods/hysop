<!--
Copyright (c) HySoP 2011-2024

This file is part of HySoP software.
See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
for further info.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
Oftentimes open source projects place a CONTRIBUTING file in the root directory.

It explains how a participant should do things like format code, test fixes, and submit patches.

Here is a fine example from puppet and another one from factory_girl_rails.

From a maintainer’s point of view, the document succinctly communicates how best to collaborate. And for a contributor, one quick check of this file verifies their submission follows the maintainer’s guidelines.

As a maintainer, all you have to do is add a CONTRIBUTING file (or CONTRIBUTING.md if you’re using Markdown) to the root of your repository.
Then we will add a link to your file when a contributor creates an Issue or opens a Pull Request.
