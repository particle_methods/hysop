# CI configuration for Hysop project


Req:
- keep only latest tags
- exception: releases


Steps: 

[[_TOC_]]



## Create and save docker images

**What:**

- build docker images with all the dependencies required to config and build hysop
- save those images in hysop registries

**Images:**

- ci_cpu_intel: OpenCL/Intel, for an host without GPUs  --> [saved in hysop registry as ci_cpu_intel](https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop/container_registry/1813)
- ci_gpu_nvidia: OpenCL/NVIDIA, for an host with GPUs   --> [saved in hysop registry as ci_cpu_nvidia](https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop/container_registry/1814)
- ci_cpu_intel_doc: same as ci_cpu_intel but with extra-deps required to build the documentation (project [hysop-doc](https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop-doc))  --> [saved in hysop-doc registry](https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop-doc/container_registry/1903)


**When:** only if the last commit message contains [docker-build]

**Requirement**: the same docker images must be generated, whatever the branch is (i.e. ci directory and .gitlab-ci.yml must be properly synchronized between all branches)


## Configure hysop

- Run meson setup on OpenCL/Intel and OpenCL/NVIDIA images
- Keep artifacts (build-dir) for build job

## Build hysop

- Run meson compile on OpenCL/Intel and OpenCL/NVIDIA images
- Keep artifacts (build-dir) for tests and install and examples jobs
 
## Install hysop

- Run meson install on OpenCL/Intel and OpenCL/NVIDIA images
- Try to import hysop and hysop fortran packages

## Tests

Two jobs to:
- run meson tests on OpenCL/Intel and OpenCL/NVIDIA images
- generate artifacts only if tests fail.

Two jobs to:
- run examples (run_examples.sh)  on OpenCL/Intel and OpenCL/NVIDIA images
- generate artifacts only if tests fail.

## Create 'read-to-use' images

Build and save docker images with a fully functionnal hysop install

- hysopbinderlab, based on 'ci_cpu_intel', source image to create binder repo.
  Saved in [project hysop-binder](https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop_binder)

  
- hysoplab-cpu-intel-<branch-name> master based on 'ci_cpu_intel, jupyter lab for hysop (no GPUs host)
  [Saved in hysop registry](https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop/container_registry)
  
  usage: 

  ```
  docker run -p 8888:8888  --rm -ti gricad-registry.univ-grenoble-alpes.fr/particle_methods/hysop/hysoplab-cpu-intel-master
  ```

- hysoplab-gpu-nvidia-<branch-name> based on 'ci_gpu_nvidia, jupyter lab for hysop (GPUs host)
  [Saved in hysop registry](https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop/container_registry)

  usage: 

  ```
  docker run -p 8888:8888  --runtime=nvidia --gpus all --rm -ti gricad-registry.univ-grenoble-alpes.fr/particle_methods/hysop/hysoplab-gpu-nvidia-master
  ```

## Build doc

A job to trigger doc. generation in project [hysop-doc](https://gricad-gitlab.univ-grenoble-alpes.fr/particle_methods/hysop-doc)


## More

- A job to automatically create a release when a tag is pushed.
