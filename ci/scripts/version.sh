#!/usr/bin/env bash
##
## Copyright (c) HySoP 2011-2024
##
## This file is part of HySoP software.
## See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
## for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -feu -o pipefail
echo "HOST"
uname -a
echo
echo "BUILD TOOLS:"
echo $(meson --version | head -1)
echo $(gcc --version | head -1)
echo "f2py version $(f2py -v)"
echo
echo "PYTHON PACKAGES:"
PYTHON_EXECUTABLE=${PYTHON_EXECUTABLE:-"$(which python3)"}
${PYTHON_EXECUTABLE} -m pip list --format=columns
echo
echo "CL_INFO"
[[ $(which clinfo) ]] && clinfo
echo
exit 0
