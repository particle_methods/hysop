#!/usr/bin/env bash
##
## Copyright (c) HySoP 2011-2024
##
## This file is part of HySoP software.
## See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
## for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -feu -o pipefail

# Ensure that required variables are set.
: ${EXAMPLE_DIR:?"Please set environment variable EXAMPLE_DIR to the required build path."}


PYTHON_EXECUTABLE=${PYTHON_EXECUTABLE:-"$(which python3)"}

COMMON_EXAMPLE_OPTIONS='-VNC -d16 -cp float -maxit 2 --autotuner-max-candidates 1 --save-checkpoint --checkpoint-dump-freq 0 --checkpoint-dump-period 0 --checkpoint-dump-last --checkpoint-dump-times'

example_test() {
     test=$1
     echo
     echo "EXAMPLE $1"
     echo "========$(printf '=%.0s' `seq ${#1}`)"
     ${PYTHON_EXECUTABLE} -Wd "${EXAMPLE_DIR}/${1}" ${COMMON_EXAMPLE_OPTIONS}
     echo
}


example_test "analytic/analytic.py"
example_test "scalar_diffusion/scalar_diffusion.py"
example_test "scalar_advection/scalar_advection.py"
example_test "scalar_advection/levelset.py"
example_test "multiresolution/scalar_advection.py"
example_test "shear_layer/shear_layer.py"
example_test "taylor_green/taylor_green.py" '-impl python'
example_test "taylor_green/taylor_green.py" '-impl opencl'
example_test "bubble/periodic_bubble.py"
example_test "bubble/periodic_bubble_levelset.py"
example_test "bubble/periodic_bubble_levelset_penalization.py" #LLVM bug for DP
example_test "bubble/periodic_jet_levelset.py"
example_test "particles_above_salt/particles_above_salt_periodic.py"
example_test "particles_above_salt/particles_above_salt_symmetrized.py"


# Tasks examples (parallel)
MPIRUN_EXECUTABLE=${MPIRUN_EXECUTABLE:-mpirun}
MPIRUN_TASKS_OPTION='-np'
if [ "${MPIRUN_EXECUTABLE}" = "srun" ]; then MPIRUN_TASKS_OPTION='-n'; fi
if [[ ${MPIRUN_EXECUTABLE} == *"mpirun"* ]]; then MPIRUN_TASKS_OPTION='--oversubscribe '${MPIRUN_TASKS_OPTION}; fi
MPIRUN_FAIL_EARLY="-mca orte_abort_on_non_zero_status 1"
MPIRUN_ARGS="${MPIRUN_FAIL_EARLY} ${MPIRUN_TASKS_OPTION} 2"
COMMON_EXAMPLE_OPTIONS='-VNC -cp float -maxit 2 --autotuner-max-candidates 1 --save-checkpoint --checkpoint-dump-freq 0 --checkpoint-dump-period 0 --checkpoint-dump-last --checkpoint-dump-times'
example_test() {
     test=$1
     echo
     echo "EXAMPLE $1"
     echo "========$(printf '=%.0s' `seq ${#1}`)"
     ${PYTHON_EXECUTABLE} -Wd "${EXAMPLE_DIR}/${1}" -d16,16,32 -sd 32,32,64 ${COMMON_EXAMPLE_OPTIONS}
     ${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} ${PYTHON_EXECUTABLE} -Wd "${EXAMPLE_DIR}/${1}" -d16,16,32 -sd 32,32,64 --proc-tasks '(111,222),(111,)'  ${COMMON_EXAMPLE_OPTIONS}
     ${MPIRUN_EXECUTABLE} ${MPIRUN_ARGS} ${PYTHON_EXECUTABLE} -Wd "${EXAMPLE_DIR}/${1}" -d16,16,32 -sd 32,32,64 --proc-tasks '111,222' ${COMMON_EXAMPLE_OPTIONS}
     echo
}
example_test "scalar_advection/turbulent_scalar_advection.py"
