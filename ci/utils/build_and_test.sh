#!/usr/bin/env bash
##
## Copyright (c) HySoP 2011-2024
##
## This file is part of HySoP software.
## See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
## for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -feu -o pipefail

# /hysop should be mounted as read only by run_tests_in_docker.sh
if [[ ! -d '/home/hysop-user/shared' ]]; then
    echo "This script should not be called from host, but from within a docker image."
    echo " => hysop has not been mounted (see hysop/ci/utils/run_docker_image.sh)."
    exit 1
fi

HYSOP_DIR='/tmp/hysop'
HYSOP_BUILD_DIR="builddir"
SCRIPT_DIR="${HYSOP_DIR}/ci/scripts"
export CI_PROJECT_DIR=${HYSOP_DIR}
export BUILD_DIR=${HYSOP_DIR}/${HYSOP_BUILD_DIR}

\rm -fr ${HYSOP_DIR}
cp -r /home/hysop-user/shared "${HYSOP_DIR}"

cd "${HYSOP_DIR}"
${SCRIPT_DIR}/version.sh
rm -rf ${HYSOP_BUILD_DIR}
meson setup      ${HYSOP_BUILD_DIR}
meson compile -C ${HYSOP_BUILD_DIR}
meson install -C ${HYSOP_BUILD_DIR}
meson test    -C ${HYSOP_BUILD_DIR}

# clean everything because image may be commited to retain hysop cache
cd
rm -rf "${HYSOP_DIR}"
