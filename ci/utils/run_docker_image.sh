#!/usr/bin/env bash
##
## Copyright (c) HySoP 2011-2024
##
## This file is part of HySoP software.
## See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
## for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -feu -o pipefail
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DOCKER_IMAGE_TAG=${1:-ci_cpu_intel}
HOST_DIRECTORY=${2:-$(realpath ${SCRIPT_DIR}/../..)}
HYSOP_REGISTRY_URL='gricad-registry.univ-grenoble-alpes.fr'

if [[ ${DOCKER_IMAGE_TAG} == *_nvidia* ]]; then
    EXTRA_ARGS="--runtime=nvidia --gpus all"
else
    EXTRA_ARGS=""
fi

docker run --rm --cap-add=SYS_PTRACE ${EXTRA_ARGS} -it -v "${HOST_DIRECTORY}:/home/hysop-user/shared" "${HYSOP_REGISTRY_URL}/particle_methods/hysop/${DOCKER_IMAGE_TAG}:latest"
