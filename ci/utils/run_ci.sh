II#!/usr/bin/env bash
##
## Copyright (c) HySoP 2011-2024
##
## This file is part of HySoP software.
## See "https://particle_methods.gricad-pages.univ-grenoble-alpes.fr/hysop-doc/"
## for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -feu -o pipefail
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DOCKER_IMAGE_TAG=${1:-ci_cpu_intel}
HYSOP_REGISTRY_URL='gricad-registry.univ-grenoble-alpes.fr'
CONTAINER_ID='hysop_build_and_test'

if [[ ${DOCKER_IMAGE_TAG} == *_nvidia* ]]; then
    EXTRA_ARGS="--runtime=nvidia --gpus all"
else
    EXTRA_ARGS=""
fi

function remove_img() {
    docker stop "${CONTAINER_ID}" || true
    docker rm "${CONTAINER_ID}" || true
}
trap remove_img INT TERM EXIT KILL

docker create --cap-add=SYS_PTRACE ${EXTRA_ARGS} --name="${CONTAINER_ID}" -it -v "${SCRIPT_DIR}/../..:/home/hysop-user/shared:ro" "${HYSOP_REGISTRY_URL}/particle_methods/hysop/${DOCKER_IMAGE_TAG}:latest"
docker start "${CONTAINER_ID}"
# Interactive shell is asked to activate micromamba in shell. Interactivity is not needed
docker exec -it "${CONTAINER_ID}" bash -i -c "MESON_TESTTHREADS=1 /home/hysop-user/shared/ci/utils/build_and_test.sh"
docker exec -it "${CONTAINER_ID}" bash -i -c "MESON_TESTTHREADS=1 EXAMPLE_DIR=/home/hysop-user/shared/hysop_examples/examples /home/hysop-user/shared/ci/scripts/run_examples.sh"
